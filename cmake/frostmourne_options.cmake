# Copyright (C) 2008-2015 Frostmourne <http://www.frostmourne.eu/>
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

option(FROSTMOURNE_LOG_TRACE                       "Enable Trace Logging in core"                      0)
option(FROSTMOURNE_LOG_DEBUG                       "Enable Debug Logging in core"                      0)

option(FROSTMOURNE_PACKET_LOG                      "Enable PacketLog in core"                          0)

option(FROSTMOURNE_WITH_ACCOUNT_SCRIPT             "Use AccountScript in core"                         0)
option(FROSTMOURNE_WITH_AUCTIONHOUSE_SCRIPT        "Use AuctionHouseScript in core"                    0)
option(FROSTMOURNE_WITH_BATTLEGROUND_SCRIPT        "Use BattlegroundScript in core"                    0)
option(FROSTMOURNE_WITH_BATTLEGROUNDMAP_SCRIPT     "Use BattlegroundMapScript in core"                 0)
option(FROSTMOURNE_WITH_DYNAMICOBJECT_SCRIPT       "Use DynamicObjectScript in core"                   0)
option(FROSTMOURNE_WITH_FORMULA_SCRIPT             "Use FormulaScript in core"                         0)
option(FROSTMOURNE_WITH_GUILD_SCRIPT               "Use GuildScript in core"                           0)
option(FROSTMOURNE_WITH_SERVER_SCRIPT              "Use ServerScript in core"                          0)
option(FROSTMOURNE_WITH_TRANSPORT_SCRIPT           "Use TransportScript in core"                       0)
option(FROSTMOURNE_WITH_VEHICLE_SCRIPT             "Use VehicleScript in core"                         0)
option(FROSTMOURNE_WITH_WEATHER_SCRIPT             "Use WeatherScript in core"                         0)
option(FROSTMOURNE_WITH_WORLD_SCRIPT               "Use WorldScript in core"                           0)
option(FROSTMOURNE_WITH_WORLDMAP_SCRIPT            "Use WorldMapScript in core"                        0)
