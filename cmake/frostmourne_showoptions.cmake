# Copyright (C) 2008-2015 Frostmourne <http://www.frostmourne.eu/>
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Show infomation about the frostmourne specific options selected during configuration

message("*** Frostmourne specific settings ***")
message("")

# FROSTMOURNE_LOG_TRACE

if( FROSTMOURNE_LOG_TRACE )
  message("* Enable Trace Logging in core                  : Yes")
  add_definitions(-DFROSTMOURNE_LOG_TRACE)
else()
  message("* Enable Trace Logging in core                  : No  (default)")
endif()

# FROSTMOURNE_LOG_DEBUG

if( FROSTMOURNE_LOG_DEBUG )
  message("* Enable Debug Logging in core                  : Yes")
  add_definitions(-DFROSTMOURNE_LOG_DEBUG)
else()
  message("* Enable Debug Logging in core                  : No  (default)")
endif()

# FROSTMOURNE_PACKET_LOG

if( FROSTMOURNE_PACKET_LOG )
  message("* Enable PacketLog in core                      : Yes")
  add_definitions(-DFROSTMOURNE_PACKET_LOG)
else()
  message("* Enable PacketLog in core                      : No  (default)")
endif()

# FROSTMOURNE_WITH_ACCOUNT_SCRIPT

if( FROSTMOURNE_WITH_ACCOUNT_SCRIPT )
  message("* Use AccountScript in core                     : Yes")
  add_definitions(-DWITH_ACCOUNT_SCRIPT)
else()
  message("* Use AccountScript in core                     : No  (default)")
endif()

# FROSTMOURNE_WITH_AUCTIONHOUSE_SCRIPT

if( FROSTMOURNE_WITH_AUCTIONHOUSE_SCRIPT )
  message("* Use AuctionHouseScript in core                : Yes")
  add_definitions(-DWITH_AUCTIONHOUSE_SCRIPT)
else()
  message("* Use AuctionHouseScript in core                : No  (default)")
endif()

# FROSTMOURNE_WITH_BATTLEGROUND_SCRIPT

if( FROSTMOURNE_WITH_BATTLEGROUND_SCRIPT )
  message("* Use BattlegroundScript in core                : Yes")
  add_definitions(-DWITH_BATTLEGROUND_SCRIPT)
else()
  message("* Use BattlegroundScript in core                : No  (default)")
endif()

# FROSTMOURNE_WITH_BATTLEGROUNDMAP_SCRIPT

if( FROSTMOURNE_WITH_BATTLEGROUNDMAP_SCRIPT )
  message("* Use BattlegroundMapScript in core             : Yes")
  add_definitions(-DWITH_BATTLEGROUNDMAP_SCRIPT)
else()
  message("* Use BattlegroundMapScript in core             : No  (default)")
endif()

# FROSTMOURNE_WITH_DYNAMICOBJECT_SCRIPT

if( FROSTMOURNE_WITH_DYNAMICOBJECT_SCRIPT )
  message("* Use DynamicObjectScript in core               : Yes")
  add_definitions(-DWITH_DYNAMICOBJECT_SCRIPT)
else()
  message("* Use DynamicObjectScript in core               : No  (default)")
endif()

# FROSTMOURNE_WITH_FORMULA_SCRIPT

if( FROSTMOURNE_WITH_FORMULA_SCRIPT )
  message("* Use FormulaScript in core                     : Yes")
  add_definitions(-DWITH_FORMULA_SCRIPT)
else()
  message("* Use FormulaScript in core                     : No  (default)")
endif()

# FROSTMOURNE_WITH_GUILD_SCRIPT

if( FROSTMOURNE_WITH_GUILD_SCRIPT )
  message("* Use GuildScript in core                       : Yes")
  add_definitions(-DWITH_GUILD_SCRIPT)
else()
  message("* Use GuildScript in core                       : No  (default)")
endif()

# FROSTMOURNE_WITH_SERVER_SCRIPT

if( FROSTMOURNE_WITH_SERVER_SCRIPT )
  message("* Use ServerScript in core                      : Yes")
  add_definitions(-DWITH_SERVER_SCRIPT)
else()
  message("* Use ServerScript in core                      : No  (default)")
endif()

# FROSTMOURNE_WITH_TRANSPORT_SCRIPT

if( FROSTMOURNE_WITH_TRANSPORT_SCRIPT )
  message("* Use TransportScript in core                   : Yes")
  add_definitions(-DWITH_TRANSPORT_SCRIPT)
else()
  message("* Use TransportScript in core                   : No  (default)")
endif()

# FROSTMOURNE_WITH_VEHICLE_SCRIPT

if( FROSTMOURNE_WITH_VEHICLE_SCRIPT )
  message("* Use VehicleScript in core                     : Yes")
  add_definitions(-DWITH_VEHICLE_SCRIPT)
else()
  message("* Use VehicleScript in core                     : No  (default)")
endif()

# FROSTMOURNE_WITH_WEATHER_SCRIPT

if( FROSTMOURNE_WITH_WEATHER_SCRIPT )
  message("* Use WeatherScript in core                     : Yes")
  add_definitions(-DWITH_WEATHER_SCRIPT)
else()
  message("* Use WeatherScript in core                     : No  (default)")
endif()

# FROSTMOURNE_WITH_WORLD_SCRIPT

if( FROSTMOURNE_WITH_WORLD_SCRIPT )
  message("* Use WorldScript in core                       : Yes")
  add_definitions(-DWITH_WORLD_SCRIPT)
else()
  message("* Use WorldScript in core                       : No  (default)")
endif()

# FROSTMOURNE_WITH_WORLDMAP_SCRIPT

if( FROSTMOURNE_WITH_WORLDMAP_SCRIPT )
  message("* Use WorldMapScript in core                    : Yes")
  add_definitions(-DWITH_WORLDMAP_SCRIPT)
else()
  message("* Use WorldMapScript in core                    : No  (default)")
endif()

message("")
