DELETE FROM `spell_effect_scaling` WHERE `ID` IN (3155, 3156);
INSERT INTO `spell_effect_scaling` (`ID`, `Coefficient`, `SpellEffectID`) VALUES 
(3155, 14.131, 128396), -- this hotfix removes the variance value from db2 which is incorrect
(3156, 9.529, 128397); -- this hotfix removes the variance value from db2 which is incorrect