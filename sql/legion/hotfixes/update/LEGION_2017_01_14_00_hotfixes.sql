--
-- Table structure for table `pvp_talent`
--

DROP TABLE IF EXISTS `pvp_talent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvp_talent` (
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SpellID` int(10) unsigned NOT NULL DEFAULT '0',
  `OverridesSpellID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` text,
  `TierID` int(10) unsigned NOT NULL DEFAULT '0',
  `ColumnIndex` int(10) unsigned NOT NULL DEFAULT '0',
  `Unk1` int(10) unsigned NOT NULL DEFAULT '0',
  `ClassID` int(10) unsigned NOT NULL DEFAULT '0',
  `SpecID` int(10) unsigned NOT NULL DEFAULT '0',
  `Unk2` int(10) unsigned NOT NULL DEFAULT '0',
  `VerifiedBuild` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvp_talent`
--

LOCK TABLES `pvp_talent` WRITE;
/*!40000 ALTER TABLE `pvp_talent` DISABLE KEYS */;
/*!40000 ALTER TABLE `pvp_talent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pvp_talent_locale`
--

DROP TABLE IF EXISTS `pvp_talent_locale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvp_talent_locale` (
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `locale` varchar(4) NOT NULL,
  `Description_lang` text,
  `VerifiedBuild` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`,`locale`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvp_talent_locale`
--

LOCK TABLES `pvp_talent_locale` WRITE;
/*!40000 ALTER TABLE `pvp_talent_locale` DISABLE KEYS */;
/*!40000 ALTER TABLE `pvp_talent_locale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pvp_talent_unlock`
--

DROP TABLE IF EXISTS `pvp_talent_unlock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvp_talent_unlock` (
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `TierID` int(10) unsigned NOT NULL DEFAULT '0',
  `ColumnIndex` int(10) unsigned NOT NULL DEFAULT '0',
  `HonorLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `VerifiedBuild` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvp_talent_unlock`
--

LOCK TABLES `pvp_talent_unlock` WRITE;
/*!40000 ALTER TABLE `pvp_talent_unlock` DISABLE KEYS */;
/*!40000 ALTER TABLE `pvp_talent_unlock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_pack_x_currency_type`
--

DROP TABLE IF EXISTS `reward_pack_x_currency_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_pack_x_currency_type` (
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `RewardPackID` int(10) unsigned NOT NULL DEFAULT '0',
  `CurrencyID` int(10) unsigned NOT NULL DEFAULT '0',
  `Amount` int(10) unsigned NOT NULL DEFAULT '0',
  `VerifiedBuild` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_pack_x_currency_type`
--

LOCK TABLES `reward_pack_x_currency_type` WRITE;
/*!40000 ALTER TABLE `reward_pack_x_currency_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward_pack_x_currency_type` ENABLE KEYS */;
UNLOCK TABLES;
