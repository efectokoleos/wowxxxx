DELETE FROM `spell` WHERE `ID` = 300001;
INSERT INTO `spell` (`Name`, `MiscID`, `ID`) VALUES 
('combo point holder (5cp) target enemy (100ms duration)', 500002, 300001);

DELETE FROM `spell_misc` WHERE `ID` = 500002;
INSERT INTO `spell_misc` (`ID`, `Attributes`, `AttributesEx`, `AttributesExB`, `AttributesExC`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500002, 67108864, 268436480, 4, 269484032, 1, 407, 5); 

DELETE FROM `spell_effect` WHERE `ID` = 500002;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `EffectBasePoints`, `ImplicitTarget1`, `SpellID`) VALUES 
(500002, 6, 4, 5, 6, 300001);
