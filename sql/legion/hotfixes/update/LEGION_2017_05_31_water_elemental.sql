DELETE FROM `spell_effect_scaling` WHERE `ID` = 2910;
INSERT INTO `spell_effect_scaling` (`ID`, `Coefficient`, `SpellEffectID`) VALUES 
(2910, 0.5, 21340); -- this hotfix removes the variance value from db2 which is incorrect
