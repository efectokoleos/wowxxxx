-- Hotfixes database update data
TRUNCATE TABLE `updates_include`;
INSERT INTO `updates_include` (`path`, `state`) VALUES
('$/sql/updates/hotfixes', 'RELEASED'),
('$/sql/custom/hotfixes', 'RELEASED'),
('$/sql/legion/hotfixes/archive', 'ARCHIVED'),
('$/sql/legion/hotfixes/update', 'RELEASED'),
('$/sql/old/6.x/hotfixes', 'ARCHIVED'),
('$/sql/old/7/hotfixes','RELEASED');
