UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94429);

DELETE FROM `creature_queststarter` WHERE `quest`=39276;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94429, 39276);

DELETE FROM `creature_questender` WHERE `quest`=39276;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95002, 39276);

DELETE FROM `quest_template_addon` WHERE `ID`=39276;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39276, 39054, 39055);
