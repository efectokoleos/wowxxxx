UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92218);

DELETE FROM `creature_queststarter` WHERE `quest`=43560;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92218, 43560);

DELETE FROM `creature_questender` WHERE `quest`=43560;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110990, 43560);

DELETE FROM `quest_template_addon` WHERE `ID`=43560;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43560, 43558);
