UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91131);

DELETE FROM `creature_queststarter` WHERE `quest`=42756;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91131, 42756);

DELETE FROM `creature_questender` WHERE `quest`=42756;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100482, 42756);

DELETE FROM `quest_template_addon` WHERE `ID`=42756;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42756, 42567);
