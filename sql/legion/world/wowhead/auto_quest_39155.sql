UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95611);

DELETE FROM `creature_queststarter` WHERE `quest`=39155;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95611, 39155);

DELETE FROM `creature_questender` WHERE `quest`=39155;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94228, 39155);

DELETE FROM `quest_template_addon` WHERE `ID`=39155;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39155, 38878, 38882);
