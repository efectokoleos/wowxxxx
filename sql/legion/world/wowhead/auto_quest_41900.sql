UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(42465);

DELETE FROM `creature_queststarter` WHERE `quest`=41900;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(42465, 41900);

DELETE FROM `creature_questender` WHERE `quest`=41900;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96746, 41900);

DELETE FROM `quest_template_addon` WHERE `ID`=41900;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41900, 41772);
