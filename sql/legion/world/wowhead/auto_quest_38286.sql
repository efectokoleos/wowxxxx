UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91403);

DELETE FROM `creature_queststarter` WHERE `quest`=38286;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91403, 38286);

DELETE FROM `creature_questender` WHERE `quest`=38286;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106780, 38286);

DELETE FROM `quest_template_addon` WHERE `ID`=38286;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38286, 37470, 42213);
