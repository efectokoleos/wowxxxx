UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102600);

DELETE FROM `creature_queststarter` WHERE `quest`=41028;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102600, 41028);

DELETE FROM `creature_questender` WHERE `quest`=41028;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102600, 41028);

DELETE FROM `quest_template_addon` WHERE `ID`=41028;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41028, 40010);
