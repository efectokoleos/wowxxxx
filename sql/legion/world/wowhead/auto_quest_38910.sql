UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93841);

DELETE FROM `creature_queststarter` WHERE `quest`=38910;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93841, 38910);

DELETE FROM `creature_questender` WHERE `quest`=38910;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95410, 38910);

DELETE FROM `quest_template_addon` WHERE `ID`=38910;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38910, 39318, 39321);
