UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90383);

DELETE FROM `creature_queststarter` WHERE `quest`=37857;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90383, 37857);

DELETE FROM `creature_questender` WHERE `quest`=37857;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89978, 37857);

DELETE FROM `quest_template_addon` WHERE `ID`=37857;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37857, 37960);
