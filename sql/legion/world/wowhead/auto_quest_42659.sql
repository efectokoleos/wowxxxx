UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107973);

DELETE FROM `creature_queststarter` WHERE `quest`=42659;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107973, 42659);

DELETE FROM `creature_questender` WHERE `quest`=42659;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107973, 42659);

DELETE FROM `quest_template_addon` WHERE `ID`=42659;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42659, 42133);
