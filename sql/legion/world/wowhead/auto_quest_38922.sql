UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93890);

DELETE FROM `creature_queststarter` WHERE `quest`=38922;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93890, 38922);

DELETE FROM `creature_questender` WHERE `quest`=38922;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93581, 38922);

DELETE FROM `quest_template_addon` WHERE `ID`=38922;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38922, 38246);
