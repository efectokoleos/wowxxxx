UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(6294);

DELETE FROM `creature_queststarter` WHERE `quest`=38402;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(6294, 38402);

DELETE FROM `creature_questender` WHERE `quest`=38402;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(6294, 38402);

DELETE FROM `quest_template_addon` WHERE `ID`=38402;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38402, 38396);
