UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115499);

DELETE FROM `creature_queststarter` WHERE `quest`=44790;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115499, 44790);

DELETE FROM `creature_questender` WHERE `quest`=44790;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115499, 44790);

DELETE FROM `quest_template_addon` WHERE `ID`=44790;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44790, 44830);
