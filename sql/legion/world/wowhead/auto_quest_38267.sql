UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91290);

DELETE FROM `creature_queststarter` WHERE `quest`=38267;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91290, 38267);

DELETE FROM `creature_questender` WHERE `quest`=38267;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91291, 38267);

DELETE FROM `quest_template_addon` WHERE `ID`=38267;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38267, 37687);
