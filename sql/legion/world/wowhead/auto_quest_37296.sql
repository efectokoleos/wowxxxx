UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(83463);

DELETE FROM `creature_queststarter` WHERE `quest`=37296;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(83463, 37296);

DELETE FROM `creature_questender` WHERE `quest`=37296;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(83463, 37296);

DELETE FROM `quest_template_addon` WHERE `ID`=37296;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37296, 35697);
