UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92218);

DELETE FROM `creature_queststarter` WHERE `quest`=43558;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92218, 43558);

DELETE FROM `creature_questender` WHERE `quest`=43558;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92218, 43558);

DELETE FROM `quest_template_addon` WHERE `ID`=43558;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43558, 43560);
