UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(76508);

DELETE FROM `creature_queststarter` WHERE `quest`=37516;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(76508, 37516);

DELETE FROM `creature_questender` WHERE `quest`=37516;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88892, 37516);

DELETE FROM `quest_template_addon` WHERE `ID`=37516;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37516, 35973);
