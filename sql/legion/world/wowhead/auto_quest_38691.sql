UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92620);

DELETE FROM `creature_queststarter` WHERE `quest`=38691;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92620, 38691);

DELETE FROM `creature_questender` WHERE `quest`=38691;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92842, 38691);

DELETE FROM `quest_template_addon` WHERE `ID`=38691;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38691, 38718);
