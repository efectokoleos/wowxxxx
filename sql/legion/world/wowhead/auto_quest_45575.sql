UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117249);

DELETE FROM `creature_queststarter` WHERE `quest`=45575;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117249, 45575);

DELETE FROM `creature_questender` WHERE `quest`=45575;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117292, 45575);

DELETE FROM `quest_template_addon` WHERE `ID`=45575;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45575, 45726, 45587);
