UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92242);

DELETE FROM `creature_queststarter` WHERE `quest`=38532;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92242, 38532);

DELETE FROM `creature_questender` WHERE `quest`=38532;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92242, 38532);

DELETE FROM `quest_template_addon` WHERE `ID`=38532;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38532, 38559);
