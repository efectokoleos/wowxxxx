UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105816);

DELETE FROM `creature_queststarter` WHERE `quest`=42691;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105816, 42691);

DELETE FROM `creature_questender` WHERE `quest`=42691;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105816, 42691);

DELETE FROM `quest_template_addon` WHERE `ID`=42691;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42691, 42689, 42406);
