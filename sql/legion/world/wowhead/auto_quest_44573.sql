UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114491);

DELETE FROM `creature_queststarter` WHERE `quest`=44573;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114491, 44573);

DELETE FROM `gameobject_questender` WHERE `quest`=44573;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259864, 44573);
