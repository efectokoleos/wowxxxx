UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98794);

DELETE FROM `creature_queststarter` WHERE `quest`=39988;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98794, 39988);

DELETE FROM `creature_questender` WHERE `quest`=39988;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97892, 39988);

DELETE FROM `quest_template_addon` WHERE `ID`=39988;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39988, 40112, 39990);
