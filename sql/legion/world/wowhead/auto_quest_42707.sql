UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107351);

DELETE FROM `creature_queststarter` WHERE `quest`=42707;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107351, 42707);

DELETE FROM `creature_questender` WHERE `quest`=42707;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107351, 42707);

DELETE FROM `quest_template_addon` WHERE `ID`=42707;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42707, 42520, 42734);
