UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92569);

DELETE FROM `creature_queststarter` WHERE `quest`=38618;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92569, 38618);

DELETE FROM `creature_questender` WHERE `quest`=38618;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91249, 38618);

DELETE FROM `quest_template_addon` WHERE `ID`=38618;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38618, 38342, 38413);
