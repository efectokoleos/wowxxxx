UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107351);

DELETE FROM `creature_queststarter` WHERE `quest`=42959;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107351, 42959);

DELETE FROM `creature_questender` WHERE `quest`=42959;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109307, 42959);

DELETE FROM `quest_template_addon` WHERE `ID`=42959;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42959, 42956);
