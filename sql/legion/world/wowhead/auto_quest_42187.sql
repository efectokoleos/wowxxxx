UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=42187;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 42187);

DELETE FROM `creature_questender` WHERE `quest`=42187;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99179, 42187);

DELETE FROM `quest_template_addon` WHERE `ID`=42187;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42187, 42186);
