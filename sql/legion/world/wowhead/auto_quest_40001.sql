UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92539);

DELETE FROM `creature_queststarter` WHERE `quest`=40001;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92539, 40001);

DELETE FROM `creature_questender` WHERE `quest`=40001;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97979, 40001);

DELETE FROM `quest_template_addon` WHERE `ID`=40001;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40001, 40078, 40002);
