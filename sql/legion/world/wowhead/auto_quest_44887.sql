UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=44887;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 44887);

DELETE FROM `creature_questender` WHERE `quest`=44887;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 44887);

DELETE FROM `quest_template_addon` WHERE `ID`=44887;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44887, 44886, 44556);
