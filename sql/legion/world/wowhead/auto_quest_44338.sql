UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113686);

DELETE FROM `creature_queststarter` WHERE `quest`=44338;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113686, 44338);

DELETE FROM `creature_questender` WHERE `quest`=44338;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 44338);

DELETE FROM `quest_template_addon` WHERE `ID`=44338;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44338, 44448);
