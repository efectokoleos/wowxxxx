UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114263);

DELETE FROM `creature_queststarter` WHERE `quest`=44868;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114263, 44868);

DELETE FROM `creature_questender` WHERE `quest`=44868;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116760, 44868);

DELETE FROM `quest_template_addon` WHERE `ID`=44868;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44868, 44729);
