UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117471);

DELETE FROM `creature_queststarter` WHERE `quest`=45647;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117471, 45647);

DELETE FROM `creature_questender` WHERE `quest`=45647;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117471, 45647);

DELETE FROM `quest_template_addon` WHERE `ID`=45647;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45647, 45632);
