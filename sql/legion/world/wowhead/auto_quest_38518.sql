UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92242);

DELETE FROM `creature_queststarter` WHERE `quest`=38518;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92242, 38518);

DELETE FROM `creature_questender` WHERE `quest`=38518;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 38518);

DELETE FROM `quest_template_addon` WHERE `ID`=38518;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38518, 39680, 38522);
