UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92842);

DELETE FROM `creature_queststarter` WHERE `quest`=38718;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92842, 38718);

DELETE FROM `creature_questender` WHERE `quest`=38718;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92850, 38718);

DELETE FROM `quest_template_addon` WHERE `ID`=38718;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38718, 38691);
