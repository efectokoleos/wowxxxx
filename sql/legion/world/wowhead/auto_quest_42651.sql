UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106720);

DELETE FROM `creature_queststarter` WHERE `quest`=42651;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106720, 42651);

DELETE FROM `creature_questender` WHERE `quest`=42651;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107987, 42651);
