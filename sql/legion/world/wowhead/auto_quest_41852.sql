UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105072);

DELETE FROM `creature_queststarter` WHERE `quest`=41852;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105072, 41852);

DELETE FROM `creature_questender` WHERE `quest`=41852;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105072, 41852);

DELETE FROM `quest_template_addon` WHERE `ID`=41852;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41852, 41850);
