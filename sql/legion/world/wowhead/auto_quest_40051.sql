UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99045);

DELETE FROM `creature_queststarter` WHERE `quest`=40051;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99045, 40051);

DELETE FROM `creature_questender` WHERE `quest`=40051;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93127, 40051);

DELETE FROM `quest_template_addon` WHERE `ID`=40051;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40051, 40222);
