UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97296);

DELETE FROM `creature_queststarter` WHERE `quest`=40976;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97296, 40976);

DELETE FROM `creature_questender` WHERE `quest`=40976;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100873, 40976);

DELETE FROM `quest_template_addon` WHERE `ID`=40976;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40976, 40518, 40760);
