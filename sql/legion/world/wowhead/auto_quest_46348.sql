UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120722);

DELETE FROM `creature_queststarter` WHERE `quest`=46348;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120722, 46348);

DELETE FROM `creature_questender` WHERE `quest`=46348;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120726, 46348);

DELETE FROM `quest_template_addon` WHERE `ID`=46348;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46348, 46347, 46349);
