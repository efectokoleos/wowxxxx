DELETE FROM `gameobject_queststarter` WHERE `quest`=42454;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(250548, 42454);

DELETE FROM `gameobject_questender` WHERE `quest`=42454;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(246463, 42454);

DELETE FROM `quest_template_addon` WHERE `ID`=42454;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42454, 39781);
