UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107317);

DELETE FROM `creature_queststarter` WHERE `quest`=42385;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107317, 42385);

DELETE FROM `creature_questender` WHERE `quest`=42385;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107372, 42385);

DELETE FROM `quest_template_addon` WHERE `ID`=42385;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42385, 42386);
