UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98964);

DELETE FROM `creature_queststarter` WHERE `quest`=40210;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98964, 40210);

DELETE FROM `creature_questender` WHERE `quest`=40210;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93523, 40210);

DELETE FROM `quest_template_addon` WHERE `ID`=40210;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40210, 40203);
