DELETE FROM `creature_questender` WHERE `quest`=41702;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104618, 41702);

DELETE FROM `gameobject_queststarter` WHERE `quest`=41702;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(247694, 41702);

DELETE FROM `quest_template_addon` WHERE `ID`=41702;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41702, 40326, 41704);
