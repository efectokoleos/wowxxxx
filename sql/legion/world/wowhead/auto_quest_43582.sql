UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(111019);

DELETE FROM `creature_queststarter` WHERE `quest`=43582;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(111019, 43582);

DELETE FROM `creature_questender` WHERE `quest`=43582;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110987, 43582);

DELETE FROM `quest_template_addon` WHERE `ID`=43582;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43582, 41231);
