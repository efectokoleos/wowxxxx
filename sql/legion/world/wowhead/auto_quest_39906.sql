UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98159);

DELETE FROM `creature_queststarter` WHERE `quest`=39906;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98159, 39906);

DELETE FROM `creature_questender` WHERE `quest`=39906;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98159, 39906);

DELETE FROM `quest_template_addon` WHERE `ID`=39906;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39906, 39914);
