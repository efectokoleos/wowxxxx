UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101195);

DELETE FROM `creature_queststarter` WHERE `quest`=41468;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101195, 41468);

DELETE FROM `creature_questender` WHERE `quest`=41468;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101259, 41468);

DELETE FROM `quest_template_addon` WHERE `ID`=41468;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41468, 41782);
