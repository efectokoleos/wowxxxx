UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100866);

DELETE FROM `creature_queststarter` WHERE `quest`=40760;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100866, 40760);

DELETE FROM `creature_questender` WHERE `quest`=40760;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100873, 40760);

DELETE FROM `quest_template_addon` WHERE `ID`=40760;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40760, 39691, 40593);
