UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104732);

DELETE FROM `creature_queststarter` WHERE `quest`=42103;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104732, 42103);

DELETE FROM `creature_questender` WHERE `quest`=42103;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104732, 42103);
