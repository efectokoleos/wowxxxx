UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113911);

DELETE FROM `creature_queststarter` WHERE `quest`=38317;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113911, 38317);

DELETE FROM `creature_questender` WHERE `quest`=38317;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97250, 38317);

DELETE FROM `quest_template_addon` WHERE `ID`=38317;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38317, 38308);
