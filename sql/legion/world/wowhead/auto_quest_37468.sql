UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88890);

DELETE FROM `creature_queststarter` WHERE `quest`=37468;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88890, 37468);

DELETE FROM `creature_questender` WHERE `quest`=37468;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89660, 37468);

DELETE FROM `quest_template_addon` WHERE `ID`=37468;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37468, 37467, 37736);
