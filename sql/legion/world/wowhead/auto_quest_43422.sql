UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113362);

DELETE FROM `creature_queststarter` WHERE `quest`=43422;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113362, 43422);

DELETE FROM `creature_questender` WHERE `quest`=43422;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113362, 43422);

DELETE FROM `quest_template_addon` WHERE `ID`=43422;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43422, 44215);
