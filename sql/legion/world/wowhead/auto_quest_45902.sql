UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90418);

DELETE FROM `creature_queststarter` WHERE `quest`=45902;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90418, 45902);

DELETE FROM `creature_questender` WHERE `quest`=45902;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 45902);

DELETE FROM `quest_template_addon` WHERE `ID`=45902;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45902, 46065);
