UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96254);

DELETE FROM `creature_queststarter` WHERE `quest`=38611;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96254, 38611);

DELETE FROM `creature_questender` WHERE `quest`=38611;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92561, 38611);

DELETE FROM `quest_template_addon` WHERE `ID`=38611;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38611, 39597, 38312);
