UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(112688);

DELETE FROM `creature_queststarter` WHERE `quest`=42175;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(112688, 42175);

DELETE FROM `creature_questender` WHERE `quest`=42175;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102700, 42175);

DELETE FROM `quest_template_addon` WHERE `ID`=42175;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42175, 42663);
