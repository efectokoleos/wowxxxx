UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=42139;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 42139);

DELETE FROM `creature_questender` WHERE `quest`=42139;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98092, 42139);

DELETE FROM `quest_template_addon` WHERE `ID`=42139;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42139, 43007, 42140);
