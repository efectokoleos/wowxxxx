UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105081);

DELETE FROM `creature_queststarter` WHERE `quest`=42006;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105081, 42006);

DELETE FROM `gameobject_questender` WHERE `quest`=42006;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(248398, 42006);

DELETE FROM `quest_template_addon` WHERE `ID`=42006;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42006, 42001, 42007);
