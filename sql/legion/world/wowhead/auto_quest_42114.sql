UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106316);

DELETE FROM `creature_queststarter` WHERE `quest`=42114;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106316, 42114);

DELETE FROM `creature_questender` WHERE `quest`=42114;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106291, 42114);

DELETE FROM `quest_template_addon` WHERE `ID`=42114;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42114, 42188);
