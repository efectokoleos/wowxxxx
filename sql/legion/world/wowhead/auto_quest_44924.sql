UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115791);

DELETE FROM `creature_queststarter` WHERE `quest`=44924;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115791, 44924);

DELETE FROM `creature_questender` WHERE `quest`=44924;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116048, 44924);

DELETE FROM `quest_template_addon` WHERE `ID`=44924;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44924, 44920);
