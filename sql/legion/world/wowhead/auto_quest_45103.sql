UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116128);

DELETE FROM `creature_queststarter` WHERE `quest`=45103;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116128, 45103);

DELETE FROM `creature_questender` WHERE `quest`=45103;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113695, 45103);
