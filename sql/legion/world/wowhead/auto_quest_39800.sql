UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96663);

DELETE FROM `creature_queststarter` WHERE `quest`=39800;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96663, 39800);

DELETE FROM `creature_questender` WHERE `quest`=39800;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90749, 39800);

DELETE FROM `quest_template_addon` WHERE `ID`=39800;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39800, 38206, 38052);
