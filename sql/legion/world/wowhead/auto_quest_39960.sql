UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97766);

DELETE FROM `creature_queststarter` WHERE `quest`=39960;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97766, 39960);

DELETE FROM `creature_questender` WHERE `quest`=39960;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97766, 39960);

DELETE FROM `quest_template_addon` WHERE `ID`=39960;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39960, 39959);
