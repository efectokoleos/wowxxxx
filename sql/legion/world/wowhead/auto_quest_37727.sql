UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89341);

DELETE FROM `creature_queststarter` WHERE `quest`=37727;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89341, 37727);

DELETE FROM `creature_questender` WHERE `quest`=37727;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89341, 37727);

DELETE FROM `quest_template_addon` WHERE `ID`=37727;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37727, 37728);
