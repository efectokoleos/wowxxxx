UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117042);

DELETE FROM `creature_queststarter` WHERE `quest`=45415;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117042, 45415);

DELETE FROM `creature_questender` WHERE `quest`=45415;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116880, 45415);

DELETE FROM `quest_template_addon` WHERE `ID`=45415;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45415, 45414);
