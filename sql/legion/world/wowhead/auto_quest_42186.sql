UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(112696);

DELETE FROM `creature_queststarter` WHERE `quest`=42186;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(112696, 42186);

DELETE FROM `creature_questender` WHERE `quest`=42186;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 42186);

DELETE FROM `quest_template_addon` WHERE `ID`=42186;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42186, 42187);
