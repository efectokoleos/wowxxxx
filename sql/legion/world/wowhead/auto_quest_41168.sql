UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103484);

DELETE FROM `creature_queststarter` WHERE `quest`=41168;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103484, 41168);

DELETE FROM `quest_template_addon` WHERE `ID`=41168;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41168, 41158, 41157);
