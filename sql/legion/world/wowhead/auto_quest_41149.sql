UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109022);

DELETE FROM `creature_queststarter` WHERE `quest`=41149;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109022, 41149);

DELETE FROM `gameobject_questender` WHERE `quest`=41149;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(247694, 41149);
