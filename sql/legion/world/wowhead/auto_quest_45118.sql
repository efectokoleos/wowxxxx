UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116193);

DELETE FROM `creature_queststarter` WHERE `quest`=45118;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116193, 45118);

DELETE FROM `creature_questender` WHERE `quest`=45118;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116193, 45118);

DELETE FROM `quest_template_addon` WHERE `ID`=45118;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45118, 44889);
