UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92718);

DELETE FROM `creature_queststarter` WHERE `quest`=38723;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92718, 38723);

DELETE FROM `creature_questender` WHERE `quest`=38723;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96665, 38723);

DELETE FROM `quest_template_addon` WHERE `ID`=38723;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38723, 39682);
