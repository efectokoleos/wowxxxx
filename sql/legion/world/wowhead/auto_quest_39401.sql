UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94789);

DELETE FROM `creature_queststarter` WHERE `quest`=39401;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94789, 39401);

DELETE FROM `creature_questender` WHERE `quest`=39401;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94789, 39401);

UPDATE `quest_template_addon` SET `PrevQuestID`=39243 WHERE `ID`=39401;
