UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113655);

DELETE FROM `creature_queststarter` WHERE `quest`=42970;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113655, 42970);

DELETE FROM `creature_questender` WHERE `quest`=42970;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101547, 42970);

DELETE FROM `quest_template_addon` WHERE `ID`=42970;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42970, 40755);
