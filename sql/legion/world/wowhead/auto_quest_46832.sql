UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120183);

DELETE FROM `creature_queststarter` WHERE `quest`=46832;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120183, 46832);

DELETE FROM `creature_questender` WHERE `quest`=46832;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120118, 46832);

DELETE FROM `quest_template_addon` WHERE `ID`=46832;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46832, 46845);
