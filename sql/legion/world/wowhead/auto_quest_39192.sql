UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96586);

DELETE FROM `creature_queststarter` WHERE `quest`=39192;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96586, 39192);

DELETE FROM `creature_questender` WHERE `quest`=39192;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96586, 39192);

DELETE FROM `quest_template_addon` WHERE `ID`=39192;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39192, 39530);
