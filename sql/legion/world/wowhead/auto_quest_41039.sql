UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102843);

DELETE FROM `creature_queststarter` WHERE `quest`=41039;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102843, 41039);

DELETE FROM `creature_questender` WHERE `quest`=41039;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102996, 41039);

DELETE FROM `quest_template_addon` WHERE `ID`=41039;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41039, 41038);
