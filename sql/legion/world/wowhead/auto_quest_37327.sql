UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(81929);

DELETE FROM `creature_queststarter` WHERE `quest`=37327;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(81929, 37327);

DELETE FROM `creature_questender` WHERE `quest`=37327;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(83463, 37327);

DELETE FROM `quest_template_addon` WHERE `ID`=37327;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37327, 37296);
