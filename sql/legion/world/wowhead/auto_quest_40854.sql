UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102193);

DELETE FROM `creature_queststarter` WHERE `quest`=40854;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102193, 40854);

DELETE FROM `creature_questender` WHERE `quest`=40854;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93520, 40854);

DELETE FROM `quest_template_addon` WHERE `ID`=40854;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40854, 40545);
