UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89023);

DELETE FROM `creature_queststarter` WHERE `quest`=37733;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89023, 37733);

DELETE FROM `creature_questender` WHERE `quest`=37733;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89023, 37733);

DELETE FROM `quest_template_addon` WHERE `ID`=37733;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37733, 37256);
