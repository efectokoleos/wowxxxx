UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117871);

DELETE FROM `creature_queststarter` WHERE `quest`=46035;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117871, 46035);

DELETE FROM `creature_questender` WHERE `quest`=46035;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 46035);

DELETE FROM `quest_template_addon` WHERE `ID`=46035;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46035, 45906);
