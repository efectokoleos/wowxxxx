UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90431);

DELETE FROM `creature_queststarter` WHERE `quest`=47022;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90431, 47022);

DELETE FROM `creature_questender` WHERE `quest`=47022;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116880, 47022);

DELETE FROM `quest_template_addon` WHERE `ID`=47022;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(47022, 47030, 45413);
