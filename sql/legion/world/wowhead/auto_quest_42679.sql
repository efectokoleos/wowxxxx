UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103761);

DELETE FROM `creature_queststarter` WHERE `quest`=42679;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103761, 42679);

DELETE FROM `creature_questender` WHERE `quest`=42679;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103761, 42679);

DELETE FROM `quest_template_addon` WHERE `ID`=42679;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42679, 42677, 42681);
