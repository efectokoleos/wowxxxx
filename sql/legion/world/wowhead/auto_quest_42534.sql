UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107806);

DELETE FROM `creature_queststarter` WHERE `quest`=42534;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107806, 42534);

DELETE FROM `creature_questender` WHERE `quest`=42534;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107806, 42534);

DELETE FROM `quest_template_addon` WHERE `ID`=42534;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42534, 42533);
