UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114489);

DELETE FROM `creature_queststarter` WHERE `quest`=44571;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114489, 44571);

DELETE FROM `gameobject_questender` WHERE `quest`=44571;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259862, 44571);
