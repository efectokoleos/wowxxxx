UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93538);

DELETE FROM `creature_queststarter` WHERE `quest`=41164;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93538, 41164);

DELETE FROM `creature_questender` WHERE `quest`=41164;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103484, 41164);

DELETE FROM `quest_template_addon` WHERE `ID`=41164;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41164, 40857);
