UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93231);

DELETE FROM `creature_queststarter` WHERE `quest`=38778;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93231, 38778);

DELETE FROM `creature_questender` WHERE `quest`=38778;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93231, 38778);

DELETE FROM `quest_template_addon` WHERE `ID`=38778;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38778, 39796, 38808);
