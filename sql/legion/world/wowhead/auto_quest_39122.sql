UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95804);

DELETE FROM `creature_queststarter` WHERE `quest`=39122;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95804, 39122);

DELETE FROM `creature_questender` WHERE `quest`=39122;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95932, 39122);

DELETE FROM `quest_template_addon` WHERE `ID`=39122;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39122, 39092);
