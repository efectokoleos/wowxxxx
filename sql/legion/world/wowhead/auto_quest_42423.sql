UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90431);

DELETE FROM `creature_queststarter` WHERE `quest`=42423;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90431, 42423);

DELETE FROM `creature_questender` WHERE `quest`=42423;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107423, 42423);

DELETE FROM `quest_template_addon` WHERE `ID`=42423;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42423, 42416, 42424);
