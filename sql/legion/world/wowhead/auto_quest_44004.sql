UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(112130);

DELETE FROM `creature_queststarter` WHERE `quest`=44004;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(112130, 44004);

DELETE FROM `creature_questender` WHERE `quest`=44004;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 44004);

DELETE FROM `quest_template_addon` WHERE `ID`=44004;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44004, 44153);
