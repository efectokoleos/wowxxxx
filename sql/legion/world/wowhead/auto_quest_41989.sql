UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105342);

DELETE FROM `creature_queststarter` WHERE `quest`=41989;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105342, 41989);

DELETE FROM `creature_questender` WHERE `quest`=41989;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105342, 41989);

DELETE FROM `quest_template_addon` WHERE `ID`=41989;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41989, 41834);
