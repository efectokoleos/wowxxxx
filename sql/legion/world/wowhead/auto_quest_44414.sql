UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113361);

DELETE FROM `creature_queststarter` WHERE `quest`=44414;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113361, 44414);

DELETE FROM `creature_questender` WHERE `quest`=44414;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113361, 44414);

DELETE FROM `quest_template_addon` WHERE `ID`=44414;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44414, 44415);
