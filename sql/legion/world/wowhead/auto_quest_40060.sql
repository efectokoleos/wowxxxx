UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97736);

DELETE FROM `creature_queststarter` WHERE `quest`=40060;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97736, 40060);

DELETE FROM `creature_questender` WHERE `quest`=40060;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98026, 40060);

DELETE FROM `quest_template_addon` WHERE `ID`=40060;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40060, 39937, 39943);
