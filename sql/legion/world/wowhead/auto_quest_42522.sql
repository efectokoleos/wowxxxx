UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107723);

DELETE FROM `creature_queststarter` WHERE `quest`=42522;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107723, 42522);

DELETE FROM `creature_questender` WHERE `quest`=42522;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108571, 42522);

DELETE FROM `quest_template_addon` WHERE `ID`=42522;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42522, 42510);
