UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98675);

DELETE FROM `creature_queststarter` WHERE `quest`=40130;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98675, 40130);

DELETE FROM `creature_questender` WHERE `quest`=40130;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98698, 40130);

DELETE FROM `quest_template_addon` WHERE `ID`=40130;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40130, 39916, 39918);
