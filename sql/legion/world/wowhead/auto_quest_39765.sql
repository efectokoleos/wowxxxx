UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96984);

DELETE FROM `creature_queststarter` WHERE `quest`=39765;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96984, 39765);

DELETE FROM `creature_questender` WHERE `quest`=39765;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96984, 39765);

DELETE FROM `quest_template_addon` WHERE `ID`=39765;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39765, 39769);
