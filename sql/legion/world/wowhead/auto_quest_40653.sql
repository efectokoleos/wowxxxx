UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98002);

DELETE FROM `creature_queststarter` WHERE `quest`=40653;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98002, 40653);

DELETE FROM `creature_questender` WHERE `quest`=40653;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98002, 40653);

DELETE FROM `quest_template_addon` WHERE `ID`=40653;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40653, 40652);
