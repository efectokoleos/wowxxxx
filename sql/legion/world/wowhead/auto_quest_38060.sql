UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90866);

DELETE FROM `creature_queststarter` WHERE `quest`=38060;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90866, 38060);

DELETE FROM `creature_questender` WHERE `quest`=38060;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90749, 38060);

DELETE FROM `quest_template_addon` WHERE `ID`=38060;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38060, 38058);
