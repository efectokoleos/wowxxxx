UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98092);

DELETE FROM `creature_queststarter` WHERE `quest`=43013;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98092, 43013);

DELETE FROM `creature_questender` WHERE `quest`=43013;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98092, 43013);

DELETE FROM `quest_template_addon` WHERE `ID`=43013;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43013, 42140, 43014);
