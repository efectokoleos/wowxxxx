UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115802);

DELETE FROM `creature_queststarter` WHERE `quest`=45532;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115802, 45532);

DELETE FROM `creature_questender` WHERE `quest`=45532;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119982, 45532);
