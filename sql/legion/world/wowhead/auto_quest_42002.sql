UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105689);

DELETE FROM `creature_queststarter` WHERE `quest`=42002;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105689, 42002);

DELETE FROM `creature_questender` WHERE `quest`=42002;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105724, 42002);

DELETE FROM `quest_template_addon` WHERE `ID`=42002;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42002, 42000);
