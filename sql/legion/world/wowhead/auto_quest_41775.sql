UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96746);

DELETE FROM `creature_queststarter` WHERE `quest`=41775;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96746, 41775);

DELETE FROM `creature_questender` WHERE `quest`=41775;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(42465, 41775);

DELETE FROM `quest_template_addon` WHERE `ID`=41775;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41775, 42200, 42068);
