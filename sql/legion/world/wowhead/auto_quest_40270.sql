UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99398);

DELETE FROM `creature_queststarter` WHERE `quest`=40270;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99398, 40270);

DELETE FROM `creature_questender` WHERE `quest`=40270;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99415, 40270);

DELETE FROM `quest_template_addon` WHERE `ID`=40270;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40270, 40267);
