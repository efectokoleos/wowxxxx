UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(111553);

DELETE FROM `creature_queststarter` WHERE `quest`=42521;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(111553, 42521);

DELETE FROM `creature_questender` WHERE `quest`=42521;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105917, 42521);

DELETE FROM `quest_template_addon` WHERE `ID`=42521;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42521, 42508);
