UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93531);

DELETE FROM `creature_queststarter` WHERE `quest`=39903;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93531, 39903);

DELETE FROM `creature_questender` WHERE `quest`=39903;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98367, 39903);

DELETE FROM `quest_template_addon` WHERE `ID`=39903;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39903, 39904);
