UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100635);

DELETE FROM `creature_queststarter` WHERE `quest`=43750;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100635, 43750);

DELETE FROM `creature_questender` WHERE `quest`=43750;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107987, 43750);

DELETE FROM `quest_template_addon` WHERE `ID`=43750;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43750, 42193);
