UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96644);

DELETE FROM `creature_queststarter` WHERE `quest`=38206;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96644, 38206);

DELETE FROM `creature_questender` WHERE `quest`=38206;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96663, 38206);

DELETE FROM `quest_template_addon` WHERE `ID`=38206;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38206, 38035, 39800);
