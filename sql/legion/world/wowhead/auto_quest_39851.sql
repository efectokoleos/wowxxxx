UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97480);

DELETE FROM `creature_queststarter` WHERE `quest`=39851;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97480, 39851);

DELETE FROM `creature_questender` WHERE `quest`=39851;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97480, 39851);

DELETE FROM `quest_template_addon` WHERE `ID`=39851;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39851, 39849, 39853);
