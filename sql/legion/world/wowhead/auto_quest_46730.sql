UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120215);

DELETE FROM `creature_queststarter` WHERE `quest`=46730;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120215, 46730);

DELETE FROM `creature_questender` WHERE `quest`=46730;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120215, 46730);

DELETE FROM `quest_template_addon` WHERE `ID`=46730;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46730, 46734);
