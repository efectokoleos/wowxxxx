UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104744);

DELETE FROM `creature_queststarter` WHERE `quest`=41733;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104744, 41733);

DELETE FROM `creature_questender` WHERE `quest`=41733;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 41733);

DELETE FROM `quest_template_addon` WHERE `ID`=41733;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41733, 41732);
