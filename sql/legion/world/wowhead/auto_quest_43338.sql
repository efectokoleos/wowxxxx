UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(60968);

DELETE FROM `creature_queststarter` WHERE `quest`=43338;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(60968, 43338);

DELETE FROM `creature_questender` WHERE `quest`=43338;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96541, 43338);

DELETE FROM `quest_template_addon` WHERE `ID`=43338;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43338, 43334, 39771);
