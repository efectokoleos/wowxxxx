UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107460);

DELETE FROM `creature_queststarter` WHERE `quest`=44700;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107460, 44700);

DELETE FROM `creature_questender` WHERE `quest`=44700;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96644, 44700);

DELETE FROM `quest_template_addon` WHERE `ID`=44700;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44700, 38035);
