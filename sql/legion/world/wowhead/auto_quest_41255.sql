UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101195);

DELETE FROM `creature_queststarter` WHERE `quest`=41255;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101195, 41255);

DELETE FROM `creature_questender` WHERE `quest`=41255;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97923, 41255);

DELETE FROM `quest_template_addon` WHERE `ID`=41255;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41255, 40651);
