UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90383);

DELETE FROM `creature_queststarter` WHERE `quest`=37860;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90383, 37860);

DELETE FROM `quest_template_addon` WHERE `ID`=37860;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37860, 37959);
