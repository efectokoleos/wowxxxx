UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93523);

DELETE FROM `creature_queststarter` WHERE `quest`=40183;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93523, 40183);

DELETE FROM `creature_questender` WHERE `quest`=40183;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93523, 40183);

DELETE FROM `quest_template_addon` WHERE `ID`=40183;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40183, 39958);
