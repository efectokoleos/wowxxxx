UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106001);

DELETE FROM `creature_queststarter` WHERE `quest`=42065;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106001, 42065);

DELETE FROM `creature_questender` WHERE `quest`=42065;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(42465, 42065);

DELETE FROM `quest_template_addon` WHERE `ID`=42065;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42065, 41898);
