UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100031);

DELETE FROM `creature_queststarter` WHERE `quest`=38566;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100031, 38566);

DELETE FROM `creature_questender` WHERE `quest`=38566;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91866, 38566);

DELETE FROM `quest_template_addon` WHERE `ID`=38566;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38566, 38576, 39722);
