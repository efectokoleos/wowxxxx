UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91158);

DELETE FROM `creature_queststarter` WHERE `quest`=38362;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91158, 38362);

DELETE FROM `creature_questender` WHERE `quest`=38362;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91158, 38362);

DELETE FROM `quest_template_addon` WHERE `ID`=38362;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38362, 38360);
