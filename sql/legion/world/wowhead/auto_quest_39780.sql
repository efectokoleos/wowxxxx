UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97407);

DELETE FROM `creature_queststarter` WHERE `quest`=39780;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97407, 39780);

DELETE FROM `creature_questender` WHERE `quest`=39780;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93846, 39780);

DELETE FROM `quest_template_addon` WHERE `ID`=39780;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39780, 39043, 38909);
