UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93822);

DELETE FROM `creature_queststarter` WHERE `quest`=39404;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93822, 39404);

DELETE FROM `creature_questender` WHERE `quest`=39404;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93822, 39404);

UPDATE `quest_template_addon` SET `PrevQuestID`=39056, `NextQuestID`=39655 WHERE `ID`=39404;
