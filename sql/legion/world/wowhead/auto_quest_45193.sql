UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116175);

DELETE FROM `creature_queststarter` WHERE `quest`=45193;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116175, 45193);

DELETE FROM `creature_questender` WHERE `quest`=45193;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 45193);
