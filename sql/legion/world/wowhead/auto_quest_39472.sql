DELETE FROM `creature_questender` WHERE `quest`=39472;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94318, 39472);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39472;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(243700, 39472);

DELETE FROM `quest_template_addon` WHERE `ID`=39472;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39472, 39059, 39062);
