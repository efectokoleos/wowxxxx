UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92400);

DELETE FROM `creature_queststarter` WHERE `quest`=38568;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92400, 38568);

DELETE FROM `creature_questender` WHERE `quest`=38568;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89937, 38568);

DELETE FROM `quest_template_addon` WHERE `ID`=38568;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38568, 38567, 38570);
