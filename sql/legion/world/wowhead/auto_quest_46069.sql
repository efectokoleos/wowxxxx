UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118376);

DELETE FROM `creature_queststarter` WHERE `quest`=46069;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118376, 46069);

DELETE FROM `creature_questender` WHERE `quest`=46069;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90250, 46069);

DELETE FROM `quest_template_addon` WHERE `ID`=46069;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46069, 46070);
