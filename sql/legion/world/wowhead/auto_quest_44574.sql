UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114492);

DELETE FROM `creature_queststarter` WHERE `quest`=44574;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114492, 44574);

DELETE FROM `gameobject_questender` WHERE `quest`=44574;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259865, 44574);
