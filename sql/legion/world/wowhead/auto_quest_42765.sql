UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109187);

DELETE FROM `creature_queststarter` WHERE `quest`=42765;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109187, 42765);

DELETE FROM `creature_questender` WHERE `quest`=42765;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 42765);

DELETE FROM `quest_template_addon` WHERE `ID`=42765;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42765, 42868);
