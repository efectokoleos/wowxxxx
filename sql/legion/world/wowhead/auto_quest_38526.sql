UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92264);

DELETE FROM `creature_queststarter` WHERE `quest`=38526;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92264, 38526);

DELETE FROM `creature_questender` WHERE `quest`=38526;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92264, 38526);

DELETE FROM `quest_template_addon` WHERE `ID`=38526;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38526, 38525, 38527);
