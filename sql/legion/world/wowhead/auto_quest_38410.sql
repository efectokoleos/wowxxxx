UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91519);

DELETE FROM `creature_queststarter` WHERE `quest`=38410;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91519, 38410);

DELETE FROM `creature_questender` WHERE `quest`=38410;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91553, 38410);

DELETE FROM `quest_template_addon` WHERE `ID`=38410;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38410, 38318, 38342);
