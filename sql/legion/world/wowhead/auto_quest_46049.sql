UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118595);

DELETE FROM `creature_queststarter` WHERE `quest`=46049;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118595, 46049);

DELETE FROM `creature_questender` WHERE `quest`=46049;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118571, 46049);
