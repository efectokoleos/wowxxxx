DELETE FROM `creature_questender` WHERE `quest`=39667;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96444, 39667);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39667;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(243899, 39667);

DELETE FROM `quest_template_addon` WHERE `ID`=39667;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39667, 39605, 38965);
