DELETE FROM `creature_questender` WHERE `quest`=37979;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(81492, 37979);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37979;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37979);

DELETE FROM `quest_template_addon` WHERE `ID`=37979;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37979, 37978);
