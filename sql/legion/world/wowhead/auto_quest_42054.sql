UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107132);

DELETE FROM `creature_queststarter` WHERE `quest`=42054;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107132, 42054);

DELETE FROM `creature_questender` WHERE `quest`=42054;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106905, 42054);
