UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97072);

DELETE FROM `creature_queststarter` WHERE `quest`=39832;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97072, 39832);

DELETE FROM `creature_questender` WHERE `quest`=39832;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93568, 39832);

DELETE FROM `quest_template_addon` WHERE `ID`=39832;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39832, 39761);
