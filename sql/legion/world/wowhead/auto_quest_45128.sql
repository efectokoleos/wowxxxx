UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116193);

DELETE FROM `creature_queststarter` WHERE `quest`=45128;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116193, 45128);

DELETE FROM `creature_questender` WHERE `quest`=45128;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96469, 45128);

DELETE FROM `quest_template_addon` WHERE `ID`=45128;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45128, 44889);
