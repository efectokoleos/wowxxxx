UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101492);

DELETE FROM `creature_queststarter` WHERE `quest`=39333;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101492, 39333);

DELETE FROM `creature_questender` WHERE `quest`=39333;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92458, 39333);

DELETE FROM `quest_template_addon` WHERE `ID`=39333;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39333, 39342);
