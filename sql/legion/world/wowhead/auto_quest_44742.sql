UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=44742;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 44742);

DELETE FROM `creature_questender` WHERE `quest`=44742;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115247, 44742);

DELETE FROM `quest_template_addon` WHERE `ID`=44742;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44742, 45262);
