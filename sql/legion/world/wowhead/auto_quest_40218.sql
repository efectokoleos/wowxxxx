UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105816);

DELETE FROM `creature_queststarter` WHERE `quest`=40218;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105816, 40218);

DELETE FROM `creature_questender` WHERE `quest`=40218;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105018, 40218);

DELETE FROM `quest_template_addon` WHERE `ID`=40218;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40218, 39142, 41767);
