UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97140);

DELETE FROM `creature_queststarter` WHERE `quest`=40325;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97140, 40325);

DELETE FROM `creature_questender` WHERE `quest`=40325;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97140, 40325);

DELETE FROM `quest_template_addon` WHERE `ID`=40325;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40325, 40324, 42224);
