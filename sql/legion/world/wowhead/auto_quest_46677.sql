UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120084);

DELETE FROM `creature_queststarter` WHERE `quest`=46677;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120084, 46677);

DELETE FROM `creature_questender` WHERE `quest`=46677;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120084, 46677);

DELETE FROM `quest_template_addon` WHERE `ID`=46677;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46677, 46675, 45425);
