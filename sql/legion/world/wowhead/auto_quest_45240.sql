UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113695);

DELETE FROM `creature_queststarter` WHERE `quest`=45240;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113695, 45240);

DELETE FROM `creature_questender` WHERE `quest`=45240;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119539, 45240);

DELETE FROM `quest_template_addon` WHERE `ID`=45240;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45240, 45398);
