UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97258);

DELETE FROM `creature_queststarter` WHERE `quest`=39789;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97258, 39789);

DELETE FROM `creature_questender` WHERE `quest`=39789;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107675, 39789);

DELETE FROM `quest_template_addon` WHERE `ID`=39789;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39789, 39793);
