UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102700);

DELETE FROM `creature_queststarter` WHERE `quest`=40267;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102700, 40267);

DELETE FROM `creature_questender` WHERE `quest`=40267;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99398, 40267);

DELETE FROM `quest_template_addon` WHERE `ID`=40267;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40267, 40270);
