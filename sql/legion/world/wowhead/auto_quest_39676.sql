UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94801);

DELETE FROM `creature_queststarter` WHERE `quest`=39676;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94801, 39676);

DELETE FROM `creature_questender` WHERE `quest`=39676;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94789, 39676);

UPDATE `quest_template_addon` SET `PrevQuestID`=39675 WHERE `ID`=39676;
