DELETE FROM `creature_questender` WHERE `quest`=38823;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97270, 38823);

DELETE FROM `gameobject_queststarter` WHERE `quest`=38823;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(241840, 38823);

DELETE FROM `quest_template_addon` WHERE `ID`=38823;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38823, 38811, 38815);
