UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93603);

DELETE FROM `creature_queststarter` WHERE `quest`=39154;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93603, 39154);

DELETE FROM `creature_questender` WHERE `quest`=39154;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94227, 39154);

DELETE FROM `quest_template_addon` WHERE `ID`=39154;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39154, 38873, 38878);
