UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97979);

DELETE FROM `creature_queststarter` WHERE `quest`=40003;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97979, 40003);

DELETE FROM `creature_questender` WHERE `quest`=40003;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97986, 40003);

DELETE FROM `quest_template_addon` WHERE `ID`=40003;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40003, 40002);
