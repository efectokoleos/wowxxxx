UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98102);

DELETE FROM `creature_queststarter` WHERE `quest`=43253;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98102, 43253);

DELETE FROM `creature_questender` WHERE `quest`=43253;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98102, 43253);

DELETE FROM `quest_template_addon` WHERE `ID`=43253;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43253, 42684, 43249);
