UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107675);

DELETE FROM `creature_queststarter` WHERE `quest`=39793;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107675, 39793);

DELETE FROM `creature_questender` WHERE `quest`=39793;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107675, 39793);

DELETE FROM `quest_template_addon` WHERE `ID`=39793;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39793, 39789);
