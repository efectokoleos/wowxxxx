UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104784);

DELETE FROM `creature_queststarter` WHERE `quest`=41730;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104784, 41730);

DELETE FROM `creature_questender` WHERE `quest`=41730;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104850, 41730);

DELETE FROM `quest_template_addon` WHERE `ID`=41730;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41730, 41728, 41732);
