UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103761);

DELETE FROM `creature_queststarter` WHERE `quest`=44161;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103761, 44161);

DELETE FROM `creature_questender` WHERE `quest`=44161;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103761, 44161);

DELETE FROM `quest_template_addon` WHERE `ID`=44161;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44161, 42670, 42679);
