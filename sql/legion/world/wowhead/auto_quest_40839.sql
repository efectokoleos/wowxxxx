UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=40839;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 40839);

DELETE FROM `creature_questender` WHERE `quest`=40839;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101513, 40839);

DELETE FROM `quest_template_addon` WHERE `ID`=40839;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40839, 40832);
