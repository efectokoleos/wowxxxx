UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98159);

DELETE FROM `creature_queststarter` WHERE `quest`=39914;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98159, 39914);

DELETE FROM `creature_questender` WHERE `quest`=39914;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98159, 39914);

DELETE FROM `quest_template_addon` WHERE `ID`=39914;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39914, 39906);
