UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117810);

DELETE FROM `creature_queststarter` WHERE `quest`=46336;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117810, 46336);

DELETE FROM `creature_questender` WHERE `quest`=46336;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119943, 46336);

DELETE FROM `quest_template_addon` WHERE `ID`=46336;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46336, 46337);
