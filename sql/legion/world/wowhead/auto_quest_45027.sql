UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105926);

DELETE FROM `creature_queststarter` WHERE `quest`=45027;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105926, 45027);

DELETE FROM `creature_questender` WHERE `quest`=45027;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115863, 45027);

DELETE FROM `quest_template_addon` WHERE `ID`=45027;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45027, 45794);
