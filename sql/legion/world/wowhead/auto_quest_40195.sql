UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98964);

DELETE FROM `creature_queststarter` WHERE `quest`=40195;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98964, 40195);

DELETE FROM `creature_questender` WHERE `quest`=40195;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99689, 40195);

DELETE FROM `quest_template_addon` WHERE `ID`=40195;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40195, 40327);
