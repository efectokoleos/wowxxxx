UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=44743;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 44743);

DELETE FROM `creature_questender` WHERE `quest`=44743;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(114838, 44743);

DELETE FROM `quest_template_addon` WHERE `ID`=44743;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44743, 44858);
