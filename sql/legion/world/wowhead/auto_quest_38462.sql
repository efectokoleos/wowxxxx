UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92055);

DELETE FROM `creature_queststarter` WHERE `quest`=38462;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92055, 38462);

DELETE FROM `creature_questender` WHERE `quest`=38462;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92120, 38462);

DELETE FROM `quest_template_addon` WHERE `ID`=38462;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38462, 38458, 38463);
