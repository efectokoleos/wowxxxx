UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92183);

DELETE FROM `creature_queststarter` WHERE `quest`=38564;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92183, 38564);

DELETE FROM `creature_questender` WHERE `quest`=38564;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 38564);

DELETE FROM `quest_template_addon` WHERE `ID`=38564;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38564, 38518, 38523);
