UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116880);

DELETE FROM `creature_queststarter` WHERE `quest`=45413;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116880, 45413);

DELETE FROM `creature_questender` WHERE `quest`=45413;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116880, 45413);

DELETE FROM `quest_template_addon` WHERE `ID`=45413;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45413, 45412, 45414);
