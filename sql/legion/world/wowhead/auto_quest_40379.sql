UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100982);

DELETE FROM `creature_queststarter` WHERE `quest`=40379;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100982, 40379);

DELETE FROM `creature_questender` WHERE `quest`=40379;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93759, 40379);

DELETE FROM `quest_template_addon` WHERE `ID`=40379;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40379, 39050);
