UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(78466);

DELETE FROM `creature_queststarter` WHERE `quest`=39175;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(78466, 39175);

DELETE FROM `creature_questender` WHERE `quest`=39175;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94686, 39175);

DELETE FROM `quest_template_addon` WHERE `ID`=39175;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39175, 39176);
