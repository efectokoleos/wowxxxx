UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104855);

DELETE FROM `creature_queststarter` WHERE `quest`=41793;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104855, 41793);

DELETE FROM `creature_questender` WHERE `quest`=41793;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105815, 41793);

DELETE FROM `quest_template_addon` WHERE `ID`=41793;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41793, 41787, 41795);
