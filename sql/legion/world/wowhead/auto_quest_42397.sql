UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107973);

DELETE FROM `creature_queststarter` WHERE `quest`=42397;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107973, 42397);

DELETE FROM `creature_questender` WHERE `quest`=42397;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107981, 42397);

DELETE FROM `quest_template_addon` WHERE `ID`=42397;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42397, 42398);
