UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102740);

DELETE FROM `creature_queststarter` WHERE `quest`=41110;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102740, 41110);

DELETE FROM `quest_template_addon` WHERE `ID`=41110;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41110, 40968, 41108);
