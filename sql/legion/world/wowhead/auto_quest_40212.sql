UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98969);

DELETE FROM `creature_queststarter` WHERE `quest`=40212;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98969, 40212);

DELETE FROM `creature_questender` WHERE `quest`=40212;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98969, 40212);

DELETE FROM `quest_template_addon` WHERE `ID`=40212;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40212, 40214);
