UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105102);

DELETE FROM `creature_queststarter` WHERE `quest`=42102;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105102, 42102);

DELETE FROM `creature_questender` WHERE `quest`=42102;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105102, 42102);

DELETE FROM `quest_template_addon` WHERE `ID`=42102;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42102, 41785);
