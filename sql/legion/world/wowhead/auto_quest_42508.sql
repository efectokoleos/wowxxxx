UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107465);

DELETE FROM `creature_queststarter` WHERE `quest`=42508;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107465, 42508);

DELETE FROM `creature_questender` WHERE `quest`=42508;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111553, 42508);

DELETE FROM `quest_template_addon` WHERE `ID`=42508;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42508, 42451, 42521);
