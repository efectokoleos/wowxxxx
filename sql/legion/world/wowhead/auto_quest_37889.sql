UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94429);

DELETE FROM `creature_queststarter` WHERE `quest`=37889;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94429, 37889);

DELETE FROM `creature_questender` WHERE `quest`=37889;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89822, 37889);

DELETE FROM `quest_template_addon` WHERE `ID`=37889;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37889, 39242, 37890);
