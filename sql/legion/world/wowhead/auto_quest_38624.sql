UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92218);

DELETE FROM `creature_queststarter` WHERE `quest`=38624;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92218, 38624);

DELETE FROM `creature_questender` WHERE `quest`=38624;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97074, 38624);

DELETE FROM `quest_template_addon` WHERE `ID`=38624;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38624, 39652, 39803);
