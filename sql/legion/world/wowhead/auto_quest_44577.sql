UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114496);

DELETE FROM `creature_queststarter` WHERE `quest`=44577;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114496, 44577);

DELETE FROM `gameobject_questender` WHERE `quest`=44577;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259868, 44577);
