UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97359);

DELETE FROM `creature_queststarter` WHERE `quest`=39950;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97359, 39950);

DELETE FROM `creature_questender` WHERE `quest`=39950;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92195, 39950);

DELETE FROM `quest_template_addon` WHERE `ID`=39950;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39950, 39949);
