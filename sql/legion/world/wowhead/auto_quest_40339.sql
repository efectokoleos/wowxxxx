UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97130);

DELETE FROM `creature_queststarter` WHERE `quest`=40339;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97130, 40339);

DELETE FROM `creature_questender` WHERE `quest`=40339;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97130, 40339);

DELETE FROM `quest_template_addon` WHERE `ID`=40339;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40339, 39770);
