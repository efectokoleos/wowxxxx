UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102546);

DELETE FROM `creature_queststarter` WHERE `quest`=40989;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102546, 40989);

DELETE FROM `creature_questender` WHERE `quest`=40989;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101846, 40989);

DELETE FROM `quest_template_addon` WHERE `ID`=40989;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40989, 40990);
