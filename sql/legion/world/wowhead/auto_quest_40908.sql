UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99948);

DELETE FROM `creature_queststarter` WHERE `quest`=40908;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99948, 40908);

DELETE FROM `creature_questender` WHERE `quest`=40908;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99559, 40908);
