UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110482);

DELETE FROM `creature_queststarter` WHERE `quest`=43412;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110482, 43412);

DELETE FROM `creature_questender` WHERE `quest`=43412;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110482, 43412);

DELETE FROM `quest_template_addon` WHERE `ID`=43412;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43412, 44214);
