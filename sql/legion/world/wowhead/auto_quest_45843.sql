UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116880);

DELETE FROM `creature_queststarter` WHERE `quest`=45843;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116880, 45843);

DELETE FROM `creature_questender` WHERE `quest`=45843;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 45843);
