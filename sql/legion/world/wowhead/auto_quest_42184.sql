UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96746);

DELETE FROM `creature_queststarter` WHERE `quest`=42184;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96746, 42184);

DELETE FROM `creature_questender` WHERE `quest`=42184;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96746, 42184);

DELETE FROM `quest_template_addon` WHERE `ID`=42184;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42184, 41770);
