UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97988);

DELETE FROM `creature_queststarter` WHERE `quest`=40072;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97988, 40072);

DELETE FROM `gameobject_questender` WHERE `quest`=40072;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(251991, 40072);

DELETE FROM `quest_template_addon` WHERE `ID`=40072;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40072, 40005);
