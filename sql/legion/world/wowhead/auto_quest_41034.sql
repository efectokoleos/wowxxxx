UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102381);

DELETE FROM `creature_queststarter` WHERE `quest`=41034;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102381, 41034);

DELETE FROM `creature_questender` WHERE `quest`=41034;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102845, 41034);
