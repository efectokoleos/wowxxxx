UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103778);

DELETE FROM `creature_queststarter` WHERE `quest`=40783;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103778, 40783);

DELETE FROM `creature_questender` WHERE `quest`=40783;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101656, 40783);

DELETE FROM `quest_template_addon` WHERE `ID`=40783;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40783, 40784);
