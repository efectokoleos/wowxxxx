UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116189);

DELETE FROM `creature_queststarter` WHERE `quest`=45126;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116189, 45126);

DELETE FROM `creature_questender` WHERE `quest`=45126;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116189, 45126);

DELETE FROM `quest_template_addon` WHERE `ID`=45126;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45126, 45125);
