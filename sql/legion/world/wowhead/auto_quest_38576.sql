UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106209);

DELETE FROM `creature_queststarter` WHERE `quest`=38576;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106209, 38576);

DELETE FROM `creature_questender` WHERE `quest`=38576;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100031, 38576);

DELETE FROM `quest_template_addon` WHERE `ID`=38576;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38576, 38376, 38566);
