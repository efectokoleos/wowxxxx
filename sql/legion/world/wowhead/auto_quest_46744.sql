UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=46744;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 46744);

DELETE FROM `creature_questender` WHERE `quest`=46744;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116302, 46744);

DELETE FROM `quest_template_addon` WHERE `ID`=46744;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46744, 46765);
