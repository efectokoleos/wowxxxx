UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98102);

DELETE FROM `creature_queststarter` WHERE `quest`=42730;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98102, 42730);

DELETE FROM `creature_questender` WHERE `quest`=42730;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102594, 42730);
