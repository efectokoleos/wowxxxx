DELETE FROM `gameobject_queststarter` WHERE `quest`=40326;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(245328, 40326);

DELETE FROM `gameobject_questender` WHERE `quest`=40326;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(247694, 40326);

DELETE FROM `quest_template_addon` WHERE `ID`=40326;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40326, 40012, 41702);
