UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107392);

DELETE FROM `creature_queststarter` WHERE `quest`=42440;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107392, 42440);

DELETE FROM `creature_questender` WHERE `quest`=42440;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107392, 42440);

DELETE FROM `quest_template_addon` WHERE `ID`=42440;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42440, 42430);
