UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99948);

DELETE FROM `creature_queststarter` WHERE `quest`=40927;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99948, 40927);

DELETE FROM `creature_questender` WHERE `quest`=40927;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102390, 40927);

DELETE FROM `quest_template_addon` WHERE `ID`=40927;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40927, 41426);
