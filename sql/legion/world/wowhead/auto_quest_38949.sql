UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93967);

DELETE FROM `creature_queststarter` WHERE `quest`=38949;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93967, 38949);

DELETE FROM `creature_questender` WHERE `quest`=38949;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93967, 38949);

DELETE FROM `quest_template_addon` WHERE `ID`=38949;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38949, 38947, 38950);
