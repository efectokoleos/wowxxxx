UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90463);

DELETE FROM `creature_queststarter` WHERE `quest`=45182;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90463, 45182);

DELETE FROM `creature_questender` WHERE `quest`=45182;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116175, 45182);
