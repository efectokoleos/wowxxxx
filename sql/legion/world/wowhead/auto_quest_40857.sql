UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93538);

DELETE FROM `creature_queststarter` WHERE `quest`=40857;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93538, 40857);

DELETE FROM `creature_questender` WHERE `quest`=40857;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103484, 40857);

DELETE FROM `quest_template_addon` WHERE `ID`=40857;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40857, 41158, 41157);
