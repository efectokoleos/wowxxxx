UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93826);

DELETE FROM `creature_queststarter` WHERE `quest`=40515;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93826, 40515);

DELETE FROM `creature_questender` WHERE `quest`=40515;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98825, 40515);

DELETE FROM `quest_template_addon` WHERE `ID`=40515;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40515, 40167);
