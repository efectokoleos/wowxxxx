UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90553);

DELETE FROM `creature_queststarter` WHERE `quest`=38001;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90553, 38001);

DELETE FROM `creature_questender` WHERE `quest`=38001;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90481, 38001);

DELETE FROM `quest_template_addon` WHERE `ID`=38001;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38001, 38577);
