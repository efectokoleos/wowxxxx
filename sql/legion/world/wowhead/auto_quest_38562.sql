UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91751);

DELETE FROM `creature_queststarter` WHERE `quest`=38562;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91751, 38562);

DELETE FROM `creature_questender` WHERE `quest`=38562;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91751, 38562);

DELETE FROM `quest_template_addon` WHERE `ID`=38562;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38562, 38421);
