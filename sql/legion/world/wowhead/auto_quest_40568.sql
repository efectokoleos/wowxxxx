UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91249);

DELETE FROM `creature_queststarter` WHERE `quest`=40568;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91249, 40568);

DELETE FROM `creature_questender` WHERE `quest`=40568;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96465, 40568);

DELETE FROM `quest_template_addon` WHERE `ID`=40568;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40568, 38412, 39652);
