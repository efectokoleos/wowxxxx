UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96083);

DELETE FROM `creature_queststarter` WHERE `quest`=39323;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96083, 39323);

DELETE FROM `creature_questender` WHERE `quest`=39323;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95130, 39323);

DELETE FROM `quest_template_addon` WHERE `ID`=39323;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39323, 39572);
