UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117305);

DELETE FROM `creature_queststarter` WHERE `quest`=46320;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117305, 46320);

DELETE FROM `creature_questender` WHERE `quest`=46320;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119765, 46320);

DELETE FROM `quest_template_addon` WHERE `ID`=46320;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46320, 45442);
