UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108782);

DELETE FROM `creature_queststarter` WHERE `quest`=42775;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108782, 42775);

DELETE FROM `creature_questender` WHERE `quest`=42775;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108571, 42775);

DELETE FROM `quest_template_addon` WHERE `ID`=42775;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42775, 42752, 42776);
