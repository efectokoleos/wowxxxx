UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100190);

DELETE FROM `creature_queststarter` WHERE `quest`=40419;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100190, 40419);

DELETE FROM `creature_questender` WHERE `quest`=40419;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102570, 40419);

DELETE FROM `quest_template_addon` WHERE `ID`=40419;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40419, 40400, 40952);
