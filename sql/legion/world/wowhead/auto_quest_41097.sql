UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99890);

DELETE FROM `creature_queststarter` WHERE `quest`=41097;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99890, 41097);

DELETE FROM `gameobject_questender` WHERE `quest`=41097;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(246884, 41097);
