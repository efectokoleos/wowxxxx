UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93542);

DELETE FROM `creature_queststarter` WHERE `quest`=38964;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93542, 38964);

DELETE FROM `quest_template_addon` WHERE `ID`=38964;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38964, 38961, 39602);
