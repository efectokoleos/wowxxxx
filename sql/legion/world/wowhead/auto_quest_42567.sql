UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90738);

DELETE FROM `creature_queststarter` WHERE `quest`=42567;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90738, 42567);

DELETE FROM `creature_questender` WHERE `quest`=42567;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91131, 42567);

DELETE FROM `quest_template_addon` WHERE `ID`=42567;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42567, 42756);
