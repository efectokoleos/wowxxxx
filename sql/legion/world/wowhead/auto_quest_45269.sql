UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=45269;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 45269);

DELETE FROM `creature_questender` WHERE `quest`=45269;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 45269);

DELETE FROM `quest_template_addon` WHERE `ID`=45269;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45269, 45287);
