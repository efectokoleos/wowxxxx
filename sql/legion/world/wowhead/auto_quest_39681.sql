UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92183);

DELETE FROM `creature_queststarter` WHERE `quest`=39681;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92183, 39681);

DELETE FROM `creature_questender` WHERE `quest`=39681;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97261, 39681);

DELETE FROM `quest_template_addon` WHERE `ID`=39681;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39681, 38499);
