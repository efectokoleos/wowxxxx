UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88798);

DELETE FROM `creature_queststarter` WHERE `quest`=37496;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88798, 37496);

DELETE FROM `creature_questender` WHERE `quest`=37496;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88873, 37496);

DELETE FROM `quest_template_addon` WHERE `ID`=37496;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37496, 38407, 37507);
