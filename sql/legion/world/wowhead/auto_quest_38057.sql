UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90783);

DELETE FROM `creature_queststarter` WHERE `quest`=38057;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90783, 38057);

DELETE FROM `creature_questender` WHERE `quest`=38057;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92414, 38057);

DELETE FROM `quest_template_addon` WHERE `ID`=38057;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38057, 38058);
