UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103023);

DELETE FROM `creature_queststarter` WHERE `quest`=40959;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103023, 40959);

DELETE FROM `creature_questender` WHERE `quest`=40959;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103023, 40959);

DELETE FROM `quest_template_addon` WHERE `ID`=40959;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40959, 40958);
