UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93759);

DELETE FROM `creature_queststarter` WHERE `quest`=38766;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93759, 38766);

DELETE FROM `creature_questender` WHERE `quest`=38766;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93127, 38766);

DELETE FROM `quest_template_addon` WHERE `ID`=38766;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38766, 38813);
