UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97734);

DELETE FROM `creature_queststarter` WHERE `quest`=39943;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97734, 39943);

DELETE FROM `creature_questender` WHERE `quest`=39943;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92195, 39943);

DELETE FROM `quest_template_addon` WHERE `ID`=39943;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39943, 39938);
