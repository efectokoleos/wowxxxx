UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98725);

DELETE FROM `creature_queststarter` WHERE `quest`=45540;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98725, 45540);

DELETE FROM `creature_questender` WHERE `quest`=45540;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116781, 45540);

DELETE FROM `quest_template_addon` WHERE `ID`=45540;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45540, 45423);
