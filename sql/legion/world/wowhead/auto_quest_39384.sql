UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95396);

DELETE FROM `creature_queststarter` WHERE `quest`=39384;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95396, 39384);

DELETE FROM `creature_questender` WHERE `quest`=39384;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91650, 39384);

DELETE FROM `quest_template_addon` WHERE `ID`=39384;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39384, 39383);
