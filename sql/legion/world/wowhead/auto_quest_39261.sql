UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99343);

DELETE FROM `creature_queststarter` WHERE `quest`=39261;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99343, 39261);

DELETE FROM `creature_questender` WHERE `quest`=39261;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99247, 39261);
