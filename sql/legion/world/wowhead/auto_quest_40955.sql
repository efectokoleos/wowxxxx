UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102574);

DELETE FROM `creature_queststarter` WHERE `quest`=40955;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102574, 40955);

DELETE FROM `creature_questender` WHERE `quest`=40955;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102574, 40955);

DELETE FROM `quest_template_addon` WHERE `ID`=40955;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40955, 40954);
