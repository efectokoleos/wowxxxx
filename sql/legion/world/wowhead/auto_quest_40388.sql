UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97892);

DELETE FROM `creature_queststarter` WHERE `quest`=40388;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97892, 40388);

DELETE FROM `creature_questender` WHERE `quest`=40388;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97903, 40388);

DELETE FROM `quest_template_addon` WHERE `ID`=40388;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40388, 39990, 39992);
