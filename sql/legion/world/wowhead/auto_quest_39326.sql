UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92458);

DELETE FROM `creature_queststarter` WHERE `quest`=39326;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92458, 39326);

DELETE FROM `creature_questender` WHERE `quest`=39326;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92458, 39326);

DELETE FROM `quest_template_addon` WHERE `ID`=39326;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39326, 39325);
