UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102940);

DELETE FROM `creature_queststarter` WHERE `quest`=42519;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102940, 42519);

DELETE FROM `creature_questender` WHERE `quest`=42519;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103023, 42519);
