UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=47052;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 47052);

DELETE FROM `creature_questender` WHERE `quest`=47052;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116568, 47052);
