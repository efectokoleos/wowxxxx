UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92264);

DELETE FROM `creature_queststarter` WHERE `quest`=38527;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92264, 38527);

DELETE FROM `creature_questender` WHERE `quest`=38527;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92264, 38527);

DELETE FROM `quest_template_addon` WHERE `ID`=38527;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38527, 38526, 38528);
