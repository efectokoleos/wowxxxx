UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93805);

DELETE FROM `creature_queststarter` WHERE `quest`=38909;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93805, 38909);

DELETE FROM `creature_questender` WHERE `quest`=38909;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93826, 38909);

DELETE FROM `quest_template_addon` WHERE `ID`=38909;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38909, 39027, 38912);
