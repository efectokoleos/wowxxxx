UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98017);

DELETE FROM `creature_queststarter` WHERE `quest`=39879;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98017, 39879);

DELETE FROM `creature_questender` WHERE `quest`=39879;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98017, 39879);

DELETE FROM `quest_template_addon` WHERE `ID`=39879;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39879, 39883);
