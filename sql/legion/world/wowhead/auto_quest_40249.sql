UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99247);

DELETE FROM `creature_queststarter` WHERE `quest`=40249;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99247, 40249);

DELETE FROM `creature_questender` WHERE `quest`=40249;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102799, 40249);
