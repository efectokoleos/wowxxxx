UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89398);

DELETE FROM `creature_queststarter` WHERE `quest`=40057;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89398, 40057);

DELETE FROM `creature_questender` WHERE `quest`=40057;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89398, 40057);

DELETE FROM `quest_template_addon` WHERE `ID`=40057;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40057, 40056);
