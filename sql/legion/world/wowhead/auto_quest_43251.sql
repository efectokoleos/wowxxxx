UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110164);

DELETE FROM `creature_queststarter` WHERE `quest`=43251;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110164, 43251);

DELETE FROM `creature_questender` WHERE `quest`=43251;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110164, 43251);

DELETE FROM `quest_template_addon` WHERE `ID`=43251;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43251, 42678);
