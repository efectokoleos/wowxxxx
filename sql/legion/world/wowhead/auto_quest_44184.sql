UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114562);

DELETE FROM `creature_queststarter` WHERE `quest`=44184;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114562, 44184);

DELETE FROM `creature_questender` WHERE `quest`=44184;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111109, 44184);

DELETE FROM `quest_template_addon` WHERE `ID`=44184;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44184, 44500);
