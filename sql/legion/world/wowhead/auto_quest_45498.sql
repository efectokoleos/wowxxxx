UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120032);

DELETE FROM `creature_queststarter` WHERE `quest`=45498;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120032, 45498);

DELETE FROM `creature_questender` WHERE `quest`=45498;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120041, 45498);

DELETE FROM `quest_template_addon` WHERE `ID`=45498;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45498, 45426);
