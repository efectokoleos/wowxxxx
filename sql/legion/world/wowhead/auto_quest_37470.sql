UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89009);

DELETE FROM `creature_queststarter` WHERE `quest`=37470;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89009, 37470);

DELETE FROM `creature_questender` WHERE `quest`=37470;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91403, 37470);

DELETE FROM `quest_template_addon` WHERE `ID`=37470;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37470, 38286);
