UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96254);

DELETE FROM `creature_queststarter` WHERE `quest`=38473;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96254, 38473);

DELETE FROM `creature_questender` WHERE `quest`=38473;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91222, 38473);

DELETE FROM `quest_template_addon` WHERE `ID`=38473;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38473, 39597, 38312);
