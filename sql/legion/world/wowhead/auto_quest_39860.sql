UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93836);

DELETE FROM `creature_queststarter` WHERE `quest`=39860;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93836, 39860);

DELETE FROM `creature_questender` WHERE `quest`=39860;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96286, 39860);

DELETE FROM `quest_template_addon` WHERE `ID`=39860;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39860, 39455, 39381);
