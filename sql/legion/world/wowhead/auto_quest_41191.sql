UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103485);

DELETE FROM `creature_queststarter` WHERE `quest`=41191;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103485, 41191);

DELETE FROM `creature_questender` WHERE `quest`=41191;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103485, 41191);

DELETE FROM `quest_template_addon` WHERE `ID`=41191;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41191, 41190);
