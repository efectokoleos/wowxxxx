UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97480);

DELETE FROM `creature_queststarter` WHERE `quest`=39850;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97480, 39850);

DELETE FROM `creature_questender` WHERE `quest`=39850;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97480, 39850);

DELETE FROM `quest_template_addon` WHERE `ID`=39850;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39850, 39849, 39853);
