UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105045);

DELETE FROM `creature_queststarter` WHERE `quest`=41850;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105045, 41850);

DELETE FROM `creature_questender` WHERE `quest`=41850;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105072, 41850);

DELETE FROM `quest_template_addon` WHERE `ID`=41850;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41850, 41849, 41852);
