UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118947);

DELETE FROM `creature_queststarter` WHERE `quest`=46106;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118947, 46106);

DELETE FROM `creature_questender` WHERE `quest`=46106;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118954, 46106);

DELETE FROM `quest_template_addon` WHERE `ID`=46106;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46106, 46107);
