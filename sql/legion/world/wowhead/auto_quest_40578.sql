UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99483);

DELETE FROM `creature_queststarter` WHERE `quest`=40578;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99483, 40578);

DELETE FROM `creature_questender` WHERE `quest`=40578;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99575, 40578);
