UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97261);

DELETE FROM `creature_queststarter` WHERE `quest`=38501;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97261, 38501);

DELETE FROM `creature_questender` WHERE `quest`=38501;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 38501);

DELETE FROM `quest_template_addon` WHERE `ID`=38501;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38501, 38505);
