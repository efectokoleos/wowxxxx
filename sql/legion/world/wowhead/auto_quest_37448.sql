UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110953);

DELETE FROM `creature_queststarter` WHERE `quest`=37448;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110953, 37448);

DELETE FROM `creature_questender` WHERE `quest`=37448;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98100, 37448);

DELETE FROM `quest_template_addon` WHERE `ID`=37448;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37448, 37666, 37494);
