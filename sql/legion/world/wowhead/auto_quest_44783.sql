UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116737);

DELETE FROM `creature_queststarter` WHERE `quest`=44783;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116737, 44783);

DELETE FROM `creature_questender` WHERE `quest`=44783;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116737, 44783);

DELETE FROM `quest_template_addon` WHERE `ID`=44783;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44783, 44775);
