UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106001);

DELETE FROM `creature_queststarter` WHERE `quest`=41898;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106001, 41898);

DELETE FROM `creature_questender` WHERE `quest`=41898;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106001, 41898);

DELETE FROM `quest_template_addon` WHERE `ID`=41898;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41898, 41777, 42065);
