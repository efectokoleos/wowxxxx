UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102425);

DELETE FROM `creature_queststarter` WHERE `quest`=40968;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102425, 40968);

DELETE FROM `quest_template_addon` WHERE `ID`=40968;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40968, 41109);
