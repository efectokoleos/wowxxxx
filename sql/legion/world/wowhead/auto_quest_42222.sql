UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106530);

DELETE FROM `creature_queststarter` WHERE `quest`=42222;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106530, 42222);

DELETE FROM `creature_questender` WHERE `quest`=42222;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107351, 42222);

DELETE FROM `quest_template_addon` WHERE `ID`=42222;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42222, 42171, 42416);
