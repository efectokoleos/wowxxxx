UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97667);

DELETE FROM `creature_queststarter` WHERE `quest`=39873;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97667, 39873);

DELETE FROM `gameobject_questender` WHERE `quest`=39873;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(243402, 39873);
