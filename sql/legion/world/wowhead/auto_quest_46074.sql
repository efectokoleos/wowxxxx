UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92183);

DELETE FROM `creature_queststarter` WHERE `quest`=46074;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92183, 46074);

DELETE FROM `creature_questender` WHERE `quest`=46074;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90250, 46074);

DELETE FROM `quest_template_addon` WHERE `ID`=46074;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46074, 46083, 45770);
