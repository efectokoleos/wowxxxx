DELETE FROM `creature_questender` WHERE `quest`=39592;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96257, 39592);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39592;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(243836, 39592);

DELETE FROM `quest_template_addon` WHERE `ID`=39592;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39592, 39594);
