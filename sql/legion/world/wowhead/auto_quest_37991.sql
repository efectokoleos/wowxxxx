UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89795);

DELETE FROM `creature_queststarter` WHERE `quest`=37991;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89795, 37991);

DELETE FROM `creature_questender` WHERE `quest`=37991;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90543, 37991);

DELETE FROM `quest_template_addon` WHERE `ID`=37991;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37991, 42271);
