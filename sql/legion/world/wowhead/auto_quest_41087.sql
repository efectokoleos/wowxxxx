UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=41087;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 41087);

DELETE FROM `creature_questender` WHERE `quest`=41087;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109823, 41087);

DELETE FROM `quest_template_addon` WHERE `ID`=41087;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41087, 32442);
