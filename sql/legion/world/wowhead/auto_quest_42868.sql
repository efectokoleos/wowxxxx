UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109187);

DELETE FROM `creature_queststarter` WHERE `quest`=42868;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109187, 42868);

DELETE FROM `creature_questender` WHERE `quest`=42868;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109187, 42868);

DELETE FROM `quest_template_addon` WHERE `ID`=42868;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42868, 42957, 42765);
