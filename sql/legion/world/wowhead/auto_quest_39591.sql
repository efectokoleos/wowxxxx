DELETE FROM `creature_questender` WHERE `quest`=39591;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96257, 39591);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39591;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(243836, 39591);

DELETE FROM `quest_template_addon` WHERE `ID`=39591;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39591, 39590);
