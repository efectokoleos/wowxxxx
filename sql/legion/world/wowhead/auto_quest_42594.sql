UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107968);

DELETE FROM `creature_queststarter` WHERE `quest`=42594;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107968, 42594);

DELETE FROM `creature_questender` WHERE `quest`=42594;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108782, 42594);

DELETE FROM `quest_template_addon` WHERE `ID`=42594;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42594, 42593, 42801);
