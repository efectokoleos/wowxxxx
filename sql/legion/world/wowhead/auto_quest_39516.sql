UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93127);

DELETE FROM `creature_queststarter` WHERE `quest`=39516;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93127, 39516);

DELETE FROM `creature_questender` WHERE `quest`=39516;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93127, 39516);

DELETE FROM `quest_template_addon` WHERE `ID`=39516;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39516, 39495, 39663);
