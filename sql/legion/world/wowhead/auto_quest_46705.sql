UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(121357);

DELETE FROM `creature_queststarter` WHERE `quest`=46705;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(121357, 46705);

DELETE FROM `creature_questender` WHERE `quest`=46705;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(121358, 46705);

DELETE FROM `quest_template_addon` WHERE `ID`=46705;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46705, 45207);
