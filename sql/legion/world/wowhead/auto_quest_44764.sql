UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114631);

DELETE FROM `creature_queststarter` WHERE `quest`=44764;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114631, 44764);

DELETE FROM `creature_questender` WHERE `quest`=44764;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(114310, 44764);

DELETE FROM `quest_template_addon` WHERE `ID`=44764;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44764, 44733);
