UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100323);

DELETE FROM `creature_queststarter` WHERE `quest`=40834;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100323, 40834);

DELETE FROM `creature_questender` WHERE `quest`=40834;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100729, 40834);

DELETE FROM `quest_template_addon` WHERE `ID`=40834;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40834, 40785, 40835);
