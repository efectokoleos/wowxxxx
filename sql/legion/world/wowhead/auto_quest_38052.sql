UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90749);

DELETE FROM `creature_queststarter` WHERE `quest`=38052;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90749, 38052);

DELETE FROM `creature_questender` WHERE `quest`=38052;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90749, 38052);

DELETE FROM `quest_template_addon` WHERE `ID`=38052;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38052, 39800);
