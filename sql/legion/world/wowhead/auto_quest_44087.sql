UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(112666);

DELETE FROM `creature_queststarter` WHERE `quest`=44087;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(112666, 44087);

DELETE FROM `creature_questender` WHERE `quest`=44087;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103025, 44087);

DELETE FROM `quest_template_addon` WHERE `ID`=44087;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44087, 42670);
