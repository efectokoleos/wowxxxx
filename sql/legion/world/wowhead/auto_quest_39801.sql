UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96686);

DELETE FROM `creature_queststarter` WHERE `quest`=39801;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96686, 39801);

DELETE FROM `creature_questender` WHERE `quest`=39801;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91473, 39801);

DELETE FROM `quest_template_addon` WHERE `ID`=39801;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39801, 39698);
