UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(77209);

DELETE FROM `creature_queststarter` WHERE `quest`=39195;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(77209, 39195);

DELETE FROM `creature_questender` WHERE `quest`=39195;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94686, 39195);

DELETE FROM `quest_template_addon` WHERE `ID`=39195;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39195, 39176);
