UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114494);

DELETE FROM `creature_queststarter` WHERE `quest`=44576;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114494, 44576);

DELETE FROM `gameobject_questender` WHERE `quest`=44576;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259867, 44576);
