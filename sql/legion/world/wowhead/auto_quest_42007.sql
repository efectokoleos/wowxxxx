DELETE FROM `gameobject_queststarter` WHERE `quest`=42007;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(248398, 42007);

DELETE FROM `quest_template_addon` WHERE `ID`=42007;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42007, 42006, 42008);
