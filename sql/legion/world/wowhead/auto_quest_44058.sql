UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=44058;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 44058);

DELETE FROM `quest_template_addon` WHERE `ID`=44058;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44058, 44057);
