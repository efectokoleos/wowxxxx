UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97140);

DELETE FROM `creature_queststarter` WHERE `quest`=40012;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97140, 40012);

DELETE FROM `gameobject_questender` WHERE `quest`=40012;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(245328, 40012);

DELETE FROM `quest_template_addon` WHERE `ID`=40012;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40012, 40326);
