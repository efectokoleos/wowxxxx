UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89660);

DELETE FROM `creature_queststarter` WHERE `quest`=37469;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89660, 37469);

DELETE FROM `gameobject_questender` WHERE `quest`=37469;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(240552, 37469);

DELETE FROM `quest_template_addon` WHERE `ID`=37469;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37469, 37730);
