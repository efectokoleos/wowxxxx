UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90180);

DELETE FROM `creature_queststarter` WHERE `quest`=38254;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90180, 38254);

DELETE FROM `creature_questender` WHERE `quest`=38254;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90177, 38254);

DELETE FROM `quest_template_addon` WHERE `ID`=38254;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38254, 38257, 38255);
