UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100786);

DELETE FROM `creature_queststarter` WHERE `quest`=40384;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100786, 40384);

DELETE FROM `creature_questender` WHERE `quest`=40384;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102478, 40384);

DELETE FROM `quest_template_addon` WHERE `ID`=40384;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40384, 40618);
