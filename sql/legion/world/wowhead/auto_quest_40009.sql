UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98312);

DELETE FROM `creature_queststarter` WHERE `quest`=40009;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98312, 40009);

DELETE FROM `creature_questender` WHERE `quest`=40009;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98312, 40009);

DELETE FROM `quest_template_addon` WHERE `ID`=40009;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40009, 40123);
