UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107302);

DELETE FROM `creature_queststarter` WHERE `quest`=42728;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107302, 42728);

DELETE FROM `creature_questender` WHERE `quest`=42728;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108389, 42728);

DELETE FROM `quest_template_addon` WHERE `ID`=42728;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42728, 41143);
