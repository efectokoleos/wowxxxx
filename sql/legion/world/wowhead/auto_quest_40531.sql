UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93526);

DELETE FROM `creature_queststarter` WHERE `quest`=40531;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93526, 40531);

DELETE FROM `creature_questender` WHERE `quest`=40531;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93526, 40531);

DELETE FROM `quest_template_addon` WHERE `ID`=40531;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40531, 40530);
