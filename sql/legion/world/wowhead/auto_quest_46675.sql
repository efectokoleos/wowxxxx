UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120070);

DELETE FROM `creature_queststarter` WHERE `quest`=46675;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120070, 46675);

DELETE FROM `creature_questender` WHERE `quest`=46675;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120084, 46675);

DELETE FROM `quest_template_addon` WHERE `ID`=46675;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46675, 46677);
