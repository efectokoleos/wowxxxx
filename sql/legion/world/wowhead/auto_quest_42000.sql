UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90259);

DELETE FROM `creature_queststarter` WHERE `quest`=42000;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90259, 42000);

DELETE FROM `creature_questender` WHERE `quest`=42000;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105689, 42000);

DELETE FROM `quest_template_addon` WHERE `ID`=42000;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42000, 42002);
