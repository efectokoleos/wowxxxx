UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101097);

DELETE FROM `creature_queststarter` WHERE `quest`=42128;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101097, 42128);

DELETE FROM `creature_questender` WHERE `quest`=42128;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106610, 42128);

DELETE FROM `quest_template_addon` WHERE `ID`=42128;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42128, 40606, 40623);
