UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98002);

DELETE FROM `creature_queststarter` WHERE `quest`=42031;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98002, 42031);

DELETE FROM `creature_questender` WHERE `quest`=42031;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101195, 42031);

DELETE FROM `quest_template_addon` WHERE `ID`=42031;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42031, 42032, 42033);
