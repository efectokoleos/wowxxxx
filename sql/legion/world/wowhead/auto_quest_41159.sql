UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103484);

DELETE FROM `creature_queststarter` WHERE `quest`=41159;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103484, 41159);

DELETE FROM `creature_questender` WHERE `quest`=41159;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103484, 41159);

DELETE FROM `quest_template_addon` WHERE `ID`=41159;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41159, 41158, 41157);
