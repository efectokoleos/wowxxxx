UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107482);

DELETE FROM `creature_queststarter` WHERE `quest`=42433;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107482, 42433);

DELETE FROM `creature_questender` WHERE `quest`=42433;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108515, 42433);

DELETE FROM `quest_template_addon` WHERE `ID`=42433;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42433, 42418);
