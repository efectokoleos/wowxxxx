UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106521);

DELETE FROM `creature_queststarter` WHERE `quest`=46792;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106521, 46792);

DELETE FROM `creature_questender` WHERE `quest`=46792;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106521, 46792);

DELETE FROM `quest_template_addon` WHERE `ID`=46792;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46792, 46791);
