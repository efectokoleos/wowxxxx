UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107966);

DELETE FROM `creature_queststarter` WHERE `quest`=42401;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107966, 42401);

DELETE FROM `creature_questender` WHERE `quest`=42401;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108089, 42401);

DELETE FROM `quest_template_addon` WHERE `ID`=42401;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42401, 42400, 42404);
