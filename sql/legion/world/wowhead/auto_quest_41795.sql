UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105815);

DELETE FROM `creature_queststarter` WHERE `quest`=41795;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105815, 41795);

DELETE FROM `creature_questender` WHERE `quest`=41795;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105951, 41795);

DELETE FROM `quest_template_addon` WHERE `ID`=41795;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41795, 41793, 41796);
