UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96420);

DELETE FROM `creature_queststarter` WHERE `quest`=38819;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96420, 38819);

DELETE FROM `creature_questender` WHERE `quest`=38819;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93127, 38819);

DELETE FROM `quest_template_addon` WHERE `ID`=38819;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38819, 39495, 39663);
