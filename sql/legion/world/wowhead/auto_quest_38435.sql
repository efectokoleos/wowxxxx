UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95002);

DELETE FROM `creature_queststarter` WHERE `quest`=38435;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95002, 38435);

DELETE FROM `creature_questender` WHERE `quest`=38435;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91913, 38435);

DELETE FROM `quest_template_addon` WHERE `ID`=38435;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38435, 39055, 38436);
