UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100812);

DELETE FROM `creature_queststarter` WHERE `quest`=40623;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100812, 40623);

DELETE FROM `creature_questender` WHERE `quest`=40623;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101282, 40623);

DELETE FROM `quest_template_addon` WHERE `ID`=40623;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40623, 40611, 40712);
