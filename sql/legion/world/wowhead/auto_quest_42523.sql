UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103023);

DELETE FROM `creature_queststarter` WHERE `quest`=42523;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103023, 42523);

DELETE FROM `creature_questender` WHERE `quest`=42523;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103023, 42523);

DELETE FROM `quest_template_addon` WHERE `ID`=42523;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42523, 42524);
