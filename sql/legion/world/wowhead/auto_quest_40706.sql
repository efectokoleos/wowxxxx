UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101314);

DELETE FROM `creature_queststarter` WHERE `quest`=40706;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101314, 40706);

DELETE FROM `creature_questender` WHERE `quest`=40706;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101314, 40706);

DELETE FROM `quest_template_addon` WHERE `ID`=40706;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40706, 40705);
