UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118214);

DELETE FROM `creature_queststarter` WHERE `quest`=46353;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118214, 46353);

DELETE FROM `creature_questender` WHERE `quest`=46353;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120758, 46353);

DELETE FROM `quest_template_addon` WHERE `ID`=46353;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46353, 46341);
