UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113695);

DELETE FROM `creature_queststarter` WHERE `quest`=44775;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113695, 44775);

DELETE FROM `creature_questender` WHERE `quest`=44775;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115360, 44775);

DELETE FROM `quest_template_addon` WHERE `ID`=44775;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44775, 44783);
