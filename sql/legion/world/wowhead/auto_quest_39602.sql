UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93542);

DELETE FROM `creature_queststarter` WHERE `quest`=39602;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93542, 39602);

DELETE FROM `quest_template_addon` WHERE `ID`=39602;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39602, 38964, 39605);
