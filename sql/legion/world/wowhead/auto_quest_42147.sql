UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105342);

DELETE FROM `creature_queststarter` WHERE `quest`=42147;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105342, 42147);

DELETE FROM `creature_questender` WHERE `quest`=42147;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97140, 42147);

DELETE FROM `quest_template_addon` WHERE `ID`=42147;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42147, 42079);
