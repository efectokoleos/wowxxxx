DELETE FROM `creature_questender` WHERE `quest`=37852;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(81492, 37852);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37852;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37852);

DELETE FROM `quest_template_addon` WHERE `ID`=37852;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37852, 37851);
