UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106530);

DELETE FROM `creature_queststarter` WHERE `quest`=42166;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106530, 42166);

DELETE FROM `creature_questender` WHERE `quest`=42166;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106656, 42166);

DELETE FROM `quest_template_addon` WHERE `ID`=42166;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42166, 42435);
