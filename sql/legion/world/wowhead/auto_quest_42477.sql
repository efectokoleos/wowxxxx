UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102700);

DELETE FROM `creature_queststarter` WHERE `quest`=42477;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102700, 42477);

DELETE FROM `creature_questender` WHERE `quest`=42477;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102700, 42477);

DELETE FROM `quest_template_addon` WHERE `ID`=42477;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42477, 42479);
