UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117871);

DELETE FROM `creature_queststarter` WHERE `quest`=45627;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117871, 45627);

DELETE FROM `creature_questender` WHERE `quest`=45627;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 45627);

DELETE FROM `quest_template_addon` WHERE `ID`=45627;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45627, 45909);
