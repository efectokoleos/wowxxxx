UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91923);

DELETE FROM `creature_queststarter` WHERE `quest`=38445;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91923, 38445);

DELETE FROM `creature_questender` WHERE `quest`=38445;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90309, 38445);

DELETE FROM `quest_template_addon` WHERE `ID`=38445;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38445, 38444);
