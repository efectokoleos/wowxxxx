UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90177);

DELETE FROM `creature_queststarter` WHERE `quest`=38255;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90177, 38255);

DELETE FROM `creature_questender` WHERE `quest`=38255;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91242, 38255);

DELETE FROM `quest_template_addon` WHERE `ID`=38255;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38255, 38254, 38256);
