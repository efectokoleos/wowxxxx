UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90259);

DELETE FROM `creature_queststarter` WHERE `quest`=42919;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90259, 42919);

DELETE FROM `creature_questender` WHERE `quest`=42919;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90259, 42919);

DELETE FROM `quest_template_addon` WHERE `ID`=42919;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42919, 42867);
