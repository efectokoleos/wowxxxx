UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93541);

DELETE FROM `creature_queststarter` WHERE `quest`=40152;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93541, 40152);

DELETE FROM `creature_questender` WHERE `quest`=40152;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98720, 40152);

DELETE FROM `quest_template_addon` WHERE `ID`=40152;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40152, 40151);
