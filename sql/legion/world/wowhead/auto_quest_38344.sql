UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91589);

DELETE FROM `creature_queststarter` WHERE `quest`=38344;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91589, 38344);

DELETE FROM `creature_questender` WHERE `quest`=38344;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(6294, 38344);

DELETE FROM `quest_template_addon` WHERE `ID`=38344;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38344, 38345);
