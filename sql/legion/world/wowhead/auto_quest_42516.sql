UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(112689);

DELETE FROM `creature_queststarter` WHERE `quest`=42516;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(112689, 42516);

DELETE FROM `creature_questender` WHERE `quest`=42516;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101195, 42516);

DELETE FROM `quest_template_addon` WHERE `ID`=42516;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42516, 42583);
