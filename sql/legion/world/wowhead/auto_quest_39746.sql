UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91731);

DELETE FROM `creature_queststarter` WHERE `quest`=39746;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91731, 39746);

DELETE FROM `creature_questender` WHERE `quest`=39746;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96527, 39746);

DELETE FROM `quest_template_addon` WHERE `ID`=39746;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39746, 41335);
