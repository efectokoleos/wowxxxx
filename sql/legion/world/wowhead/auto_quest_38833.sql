UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92243);

DELETE FROM `creature_queststarter` WHERE `quest`=38833;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92243, 38833);

DELETE FROM `creature_questender` WHERE `quest`=38833;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92243, 38833);

DELETE FROM `quest_template_addon` WHERE `ID`=38833;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38833, 38559, 38533);
