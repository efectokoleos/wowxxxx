UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98229);

DELETE FROM `creature_queststarter` WHERE `quest`=40378;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98229, 40378);

DELETE FROM `creature_questender` WHERE `quest`=40378;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94410, 40378);

DELETE FROM `quest_template_addon` WHERE `ID`=40378;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40378, 40077);
