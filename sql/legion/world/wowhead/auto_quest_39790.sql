UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93189);

DELETE FROM `creature_queststarter` WHERE `quest`=39790;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93189, 39790);

DELETE FROM `creature_questender` WHERE `quest`=39790;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97360, 39790);

DELETE FROM `quest_template_addon` WHERE `ID`=39790;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39790, 38807, 39763);
