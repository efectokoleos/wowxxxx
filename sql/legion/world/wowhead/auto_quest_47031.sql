UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116302);

DELETE FROM `creature_queststarter` WHERE `quest`=47031;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116302, 47031);

DELETE FROM `creature_questender` WHERE `quest`=47031;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 47031);

DELETE FROM `quest_template_addon` WHERE `ID`=47031;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(47031, 45560);
