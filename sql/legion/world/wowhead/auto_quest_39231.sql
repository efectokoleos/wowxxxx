UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89753);

DELETE FROM `creature_queststarter` WHERE `quest`=39231;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89753, 39231);

DELETE FROM `creature_questender` WHERE `quest`=39231;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(78487, 39231);
