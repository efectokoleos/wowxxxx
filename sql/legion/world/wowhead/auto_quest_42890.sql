UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109434);

DELETE FROM `creature_queststarter` WHERE `quest`=42890;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109434, 42890);

DELETE FROM `creature_questender` WHERE `quest`=42890;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94175, 42890);

DELETE FROM `quest_template_addon` WHERE `ID`=42890;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42890, 42887);
