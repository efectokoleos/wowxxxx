UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88500);

DELETE FROM `creature_queststarter` WHERE `quest`=37318;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88500, 37318);

DELETE FROM `gameobject_questender` WHERE `quest`=37318;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(237705, 37318);
