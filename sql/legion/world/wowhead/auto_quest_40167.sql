UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98825);

DELETE FROM `creature_queststarter` WHERE `quest`=40167;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98825, 40167);

DELETE FROM `creature_questender` WHERE `quest`=40167;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98825, 40167);

DELETE FROM `quest_template_addon` WHERE `ID`=40167;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40167, 40515, 40520);
