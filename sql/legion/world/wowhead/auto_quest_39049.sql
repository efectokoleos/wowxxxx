UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94410);

DELETE FROM `creature_queststarter` WHERE `quest`=39049;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94410, 39049);

DELETE FROM `creature_questender` WHERE `quest`=39049;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93759, 39049);

DELETE FROM `quest_template_addon` WHERE `ID`=39049;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39049, 39050);
