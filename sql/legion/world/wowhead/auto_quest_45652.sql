UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96746);

DELETE FROM `creature_queststarter` WHERE `quest`=45652;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96746, 45652);

DELETE FROM `creature_questender` WHERE `quest`=45652;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117482, 45652);

DELETE FROM `quest_template_addon` WHERE `ID`=45652;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45652, 45706);
