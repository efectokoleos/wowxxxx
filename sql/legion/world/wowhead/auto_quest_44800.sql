UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120244);

DELETE FROM `creature_queststarter` WHERE `quest`=44800;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120244, 44800);

DELETE FROM `creature_questender` WHERE `quest`=44800;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106519, 44800);

DELETE FROM `quest_template_addon` WHERE `ID`=44800;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44800, 45724);
