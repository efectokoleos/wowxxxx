UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92243);

DELETE FROM `creature_queststarter` WHERE `quest`=38533;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92243, 38533);

DELETE FROM `creature_questender` WHERE `quest`=38533;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92264, 38533);

DELETE FROM `quest_template_addon` WHERE `ID`=38533;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38533, 38833);
