UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100675);

DELETE FROM `creature_queststarter` WHERE `quest`=40593;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100675, 40593);

DELETE FROM `creature_questender` WHERE `quest`=40593;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100973, 40593);

DELETE FROM `quest_template_addon` WHERE `ID`=40593;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40593, 40760, 40605);
