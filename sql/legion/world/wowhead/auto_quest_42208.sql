UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96746);

DELETE FROM `creature_queststarter` WHERE `quest`=42208;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96746, 42208);

DELETE FROM `creature_questender` WHERE `quest`=42208;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96746, 42208);

DELETE FROM `quest_template_addon` WHERE `ID`=42208;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42208, 41772);
