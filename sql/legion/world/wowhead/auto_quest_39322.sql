UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95410);

DELETE FROM `creature_queststarter` WHERE `quest`=39322;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95410, 39322);

DELETE FROM `creature_questender` WHERE `quest`=39322;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95410, 39322);

DELETE FROM `quest_template_addon` WHERE `ID`=39322;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39322, 39387);
