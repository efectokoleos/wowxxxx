UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107425);

DELETE FROM `creature_queststarter` WHERE `quest`=43335;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107425, 43335);

DELETE FROM `creature_questender` WHERE `quest`=43335;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107425, 43335);

DELETE FROM `quest_template_addon` WHERE `ID`=43335;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43335, 42392);
