UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118242);

DELETE FROM `creature_queststarter` WHERE `quest`=46290;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118242, 46290);

DELETE FROM `creature_questender` WHERE `quest`=46290;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109222, 46290);

DELETE FROM `quest_template_addon` WHERE `ID`=46290;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46290, 46000);
