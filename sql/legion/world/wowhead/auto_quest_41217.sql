UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(42396);

DELETE FROM `creature_queststarter` WHERE `quest`=41217;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(42396, 41217);

DELETE FROM `creature_questender` WHERE `quest`=41217;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103614, 41217);

UPDATE `quest_template_addon` SET `NextQuestID`=26421 WHERE `ID`=41217;
