UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93581);

DELETE FROM `creature_queststarter` WHERE `quest`=38246;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93581, 38246);

DELETE FROM `creature_questender` WHERE `quest`=38246;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93890, 38246);

DELETE FROM `quest_template_addon` WHERE `ID`=38246;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38246, 38922);
