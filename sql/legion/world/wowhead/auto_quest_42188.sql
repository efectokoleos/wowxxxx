UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96746);

DELETE FROM `creature_queststarter` WHERE `quest`=42188;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96746, 42188);

DELETE FROM `creature_questender` WHERE `quest`=42188;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106316, 42188);

DELETE FROM `quest_template_addon` WHERE `ID`=42188;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42188, 42114);
