UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104788);

DELETE FROM `creature_queststarter` WHERE `quest`=41901;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104788, 41901);

DELETE FROM `creature_questender` WHERE `quest`=41901;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96746, 41901);

DELETE FROM `quest_template_addon` WHERE `ID`=41901;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41901, 42200);
