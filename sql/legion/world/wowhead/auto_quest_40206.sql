UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98931);

DELETE FROM `creature_queststarter` WHERE `quest`=40206;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98931, 40206);

DELETE FROM `creature_questender` WHERE `quest`=40206;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98964, 40206);

DELETE FROM `quest_template_addon` WHERE `ID`=40206;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40206, 40199);
