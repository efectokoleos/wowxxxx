UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(27856);

DELETE FROM `creature_queststarter` WHERE `quest`=47543;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(27856, 47543);

DELETE FROM `creature_questender` WHERE `quest`=47543;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(27856, 47543);

DELETE FROM `quest_template_addon` WHERE `ID`=47543;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(47543, 47545);
