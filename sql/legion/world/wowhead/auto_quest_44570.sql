UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114488);

DELETE FROM `creature_queststarter` WHERE `quest`=44570;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114488, 44570);

DELETE FROM `gameobject_questender` WHERE `quest`=44570;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259861, 44570);
