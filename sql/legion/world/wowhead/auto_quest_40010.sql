UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97140);

DELETE FROM `creature_queststarter` WHERE `quest`=40010;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97140, 40010);

DELETE FROM `creature_questender` WHERE `quest`=40010;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102600, 40010);

DELETE FROM `quest_template_addon` WHERE `ID`=40010;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40010, 41028);
