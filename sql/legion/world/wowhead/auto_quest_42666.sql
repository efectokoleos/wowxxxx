UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(112666);

DELETE FROM `creature_queststarter` WHERE `quest`=42666;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(112666, 42666);

DELETE FROM `creature_questender` WHERE `quest`=42666;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103025, 42666);

DELETE FROM `quest_template_addon` WHERE `ID`=42666;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42666, 42670);
