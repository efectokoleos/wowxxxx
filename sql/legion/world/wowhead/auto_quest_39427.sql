UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104382);

DELETE FROM `creature_queststarter` WHERE `quest`=39427;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104382, 39427);

DELETE FROM `creature_questender` WHERE `quest`=39427;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110821, 39427);

DELETE FROM `quest_template_addon` WHERE `ID`=39427;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39427, 41542, 40385);
