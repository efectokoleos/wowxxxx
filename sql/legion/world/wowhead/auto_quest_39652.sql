UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96465);

DELETE FROM `creature_queststarter` WHERE `quest`=39652;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96465, 39652);

DELETE FROM `creature_questender` WHERE `quest`=39652;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92218, 39652);

DELETE FROM `quest_template_addon` WHERE `ID`=39652;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39652, 38413, 38624);
