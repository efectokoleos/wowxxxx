UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119821);

DELETE FROM `creature_queststarter` WHERE `quest`=44758;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119821, 44758);

DELETE FROM `creature_questender` WHERE `quest`=44758;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119821, 44758);

DELETE FROM `quest_template_addon` WHERE `ID`=44758;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44758, 45833);
