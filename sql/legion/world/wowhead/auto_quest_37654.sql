UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91419);

DELETE FROM `creature_queststarter` WHERE `quest`=37654;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91419, 37654);

DELETE FROM `creature_questender` WHERE `quest`=37654;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106881, 37654);

DELETE FROM `quest_template_addon` WHERE `ID`=37654;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37654, 38857, 37659);
