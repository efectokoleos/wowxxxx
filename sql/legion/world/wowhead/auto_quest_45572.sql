UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119944);

DELETE FROM `creature_queststarter` WHERE `quest`=45572;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119944, 45572);

DELETE FROM `creature_questender` WHERE `quest`=45572;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119676, 45572);

DELETE FROM `quest_template_addon` WHERE `ID`=45572;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45572, 45840, 46182);
