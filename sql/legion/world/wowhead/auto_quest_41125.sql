UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102700);

DELETE FROM `creature_queststarter` WHERE `quest`=41125;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102700, 41125);

DELETE FROM `creature_questender` WHERE `quest`=41125;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102700, 41125);

DELETE FROM `quest_template_addon` WHERE `ID`=41125;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41125, 41114, 41112);
