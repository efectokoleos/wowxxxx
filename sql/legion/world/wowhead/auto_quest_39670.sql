UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96513);

DELETE FROM `creature_queststarter` WHERE `quest`=39670;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96513, 39670);

DELETE FROM `creature_questender` WHERE `quest`=39670;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96513, 39670);

DELETE FROM `quest_template_addon` WHERE `ID`=39670;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39670, 39386, 39656);
