UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106199);

DELETE FROM `creature_queststarter` WHERE `quest`=42602;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106199, 42602);

DELETE FROM `creature_questender` WHERE `quest`=42602;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106199, 42602);

DELETE FROM `quest_template_addon` WHERE `ID`=42602;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42602, 41797, 42601);
