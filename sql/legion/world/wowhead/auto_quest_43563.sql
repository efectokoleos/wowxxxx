UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103155);

DELETE FROM `creature_queststarter` WHERE `quest`=43563;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103155, 43563);

DELETE FROM `creature_questender` WHERE `quest`=43563;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103155, 43563);

DELETE FROM `quest_template_addon` WHERE `ID`=43563;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43563, 43562);
