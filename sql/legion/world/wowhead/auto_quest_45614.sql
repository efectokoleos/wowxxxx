UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117331);

DELETE FROM `creature_queststarter` WHERE `quest`=45614;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117331, 45614);

DELETE FROM `creature_questender` WHERE `quest`=45614;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119001, 45614);

DELETE FROM `quest_template_addon` WHERE `ID`=45614;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45614, 45251);
