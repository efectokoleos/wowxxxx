UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92718);

DELETE FROM `creature_queststarter` WHERE `quest`=38672;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92718, 38672);

DELETE FROM `gameobject_questender` WHERE `quest`=38672;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(244923, 38672);

DELETE FROM `quest_template_addon` WHERE `ID`=38672;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38672, 38669);
