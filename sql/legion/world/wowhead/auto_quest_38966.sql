UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93542);

DELETE FROM `creature_queststarter` WHERE `quest`=38966;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93542, 38966);

DELETE FROM `creature_questender` WHERE `quest`=38966;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93542, 38966);

DELETE FROM `quest_template_addon` WHERE `ID`=38966;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38966, 38965, 38962);
