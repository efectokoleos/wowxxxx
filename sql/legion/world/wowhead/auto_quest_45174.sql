UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113857);

DELETE FROM `creature_queststarter` WHERE `quest`=45174;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113857, 45174);

DELETE FROM `creature_questender` WHERE `quest`=45174;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89398, 45174);

DELETE FROM `quest_template_addon` WHERE `ID`=45174;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45174, 45175);
