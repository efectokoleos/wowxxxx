UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117774);

DELETE FROM `creature_queststarter` WHERE `quest`=46205;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117774, 46205);

DELETE FROM `creature_questender` WHERE `quest`=46205;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119002, 46205);

DELETE FROM `quest_template_addon` WHERE `ID`=46205;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46205, 45795, 46199);
