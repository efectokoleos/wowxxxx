UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98632);

DELETE FROM `creature_queststarter` WHERE `quest`=42593;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98632, 42593);

DELETE FROM `creature_questender` WHERE `quest`=42593;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107968, 42593);

DELETE FROM `quest_template_addon` WHERE `ID`=42593;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42593, 42594);
