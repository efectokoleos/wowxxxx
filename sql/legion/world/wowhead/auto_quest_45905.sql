UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90431);

DELETE FROM `creature_queststarter` WHERE `quest`=45905;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90431, 45905);

DELETE FROM `creature_questender` WHERE `quest`=45905;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90431, 45905);

DELETE FROM `quest_template_addon` WHERE `ID`=45905;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45905, 45416);
