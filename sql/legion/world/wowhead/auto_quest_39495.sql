UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96436);

DELETE FROM `creature_queststarter` WHERE `quest`=39495;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96436, 39495);

DELETE FROM `creature_questender` WHERE `quest`=39495;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93127, 39495);

DELETE FROM `quest_template_addon` WHERE `ID`=39495;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39495, 39262, 38727);
