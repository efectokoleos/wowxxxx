UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110821);

DELETE FROM `creature_queststarter` WHERE `quest`=40385;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110821, 40385);

DELETE FROM `creature_questender` WHERE `quest`=40385;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102478, 40385);

DELETE FROM `quest_template_addon` WHERE `ID`=40385;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40385, 39427);
