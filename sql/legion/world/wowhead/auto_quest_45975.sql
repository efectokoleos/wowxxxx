UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90259);

DELETE FROM `creature_queststarter` WHERE `quest`=45975;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90259, 45975);

DELETE FROM `creature_questender` WHERE `quest`=45975;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90259, 45975);

DELETE FROM `quest_template_addon` WHERE `ID`=45975;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45975, 45974);
