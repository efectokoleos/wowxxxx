UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110564);

DELETE FROM `creature_queststarter` WHERE `quest`=43273;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110564, 43273);

DELETE FROM `creature_questender` WHERE `quest`=43273;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110564, 43273);

DELETE FROM `quest_template_addon` WHERE `ID`=43273;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43273, 43270, 43275);
