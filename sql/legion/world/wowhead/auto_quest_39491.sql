UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93833);

DELETE FROM `creature_queststarter` WHERE `quest`=39491;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93833, 39491);

DELETE FROM `creature_questender` WHERE `quest`=39491;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93833, 39491);

DELETE FROM `quest_template_addon` WHERE `ID`=39491;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39491, 38911, 39496);
