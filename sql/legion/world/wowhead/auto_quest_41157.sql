UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103484);

DELETE FROM `creature_queststarter` WHERE `quest`=41157;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103484, 41157);

DELETE FROM `creature_questender` WHERE `quest`=41157;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103484, 41157);

DELETE FROM `quest_template_addon` WHERE `ID`=41157;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41157, 40857, 41170);
