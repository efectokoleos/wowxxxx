UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=45263;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 45263);

DELETE FROM `creature_questender` WHERE `quest`=45263;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 45263);

DELETE FROM `quest_template_addon` WHERE `ID`=45263;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45263, 45278);
