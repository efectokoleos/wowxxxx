UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92195);

DELETE FROM `creature_queststarter` WHERE `quest`=39934;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92195, 39934);

DELETE FROM `creature_questender` WHERE `quest`=39934;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89398, 39934);

DELETE FROM `quest_template_addon` WHERE `ID`=39934;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39934, 39935);
