UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113547);

DELETE FROM `creature_queststarter` WHERE `quest`=40518;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113547, 40518);

DELETE FROM `creature_questender` WHERE `quest`=40518;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100453, 40518);

DELETE FROM `quest_template_addon` WHERE `ID`=40518;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40518, 44281, 39691);
