DELETE FROM `gameobject_queststarter` WHERE `quest`=41627;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(248398, 41627);

DELETE FROM `quest_template_addon` WHERE `ID`=41627;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41627, 41626, 41628);
