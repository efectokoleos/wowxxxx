UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93530);

DELETE FROM `creature_queststarter` WHERE `quest`=39875;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93530, 39875);

DELETE FROM `creature_questender` WHERE `quest`=39875;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93530, 39875);

DELETE FROM `quest_template_addon` WHERE `ID`=39875;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39875, 39874, 39876);
