UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100185);

DELETE FROM `creature_queststarter` WHERE `quest`=40424;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100185, 40424);

DELETE FROM `creature_questender` WHERE `quest`=40424;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100301, 40424);

DELETE FROM `quest_template_addon` WHERE `ID`=40424;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40424, 40469);
