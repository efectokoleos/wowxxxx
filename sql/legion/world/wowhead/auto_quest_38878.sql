UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94227);

DELETE FROM `creature_queststarter` WHERE `quest`=38878;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94227, 38878);

DELETE FROM `creature_questender` WHERE `quest`=38878;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95611, 38878);

DELETE FROM `quest_template_addon` WHERE `ID`=38878;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38878, 39154, 39155);
