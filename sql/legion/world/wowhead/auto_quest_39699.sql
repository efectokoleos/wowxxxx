UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92242);

DELETE FROM `creature_queststarter` WHERE `quest`=39699;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92242, 39699);

DELETE FROM `creature_questender` WHERE `quest`=39699;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92242, 39699);

DELETE FROM `quest_template_addon` WHERE `ID`=39699;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39699, 38514);
