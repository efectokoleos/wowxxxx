UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92219);

DELETE FROM `creature_queststarter` WHERE `quest`=38257;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92219, 38257);

DELETE FROM `creature_questender` WHERE `quest`=38257;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90180, 38257);

DELETE FROM `quest_template_addon` WHERE `ID`=38257;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38257, 38253, 38254);
