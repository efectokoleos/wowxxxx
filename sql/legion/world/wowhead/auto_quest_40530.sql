UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93526);

DELETE FROM `creature_queststarter` WHERE `quest`=40530;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93526, 40530);

DELETE FROM `creature_questender` WHERE `quest`=40530;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93526, 40530);

DELETE FROM `quest_template_addon` WHERE `ID`=40530;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40530, 40529, 40531);
