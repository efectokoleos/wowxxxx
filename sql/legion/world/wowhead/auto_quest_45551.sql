UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108455);

DELETE FROM `creature_queststarter` WHERE `quest`=45551;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108455, 45551);

DELETE FROM `creature_questender` WHERE `quest`=45551;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117308, 45551);
