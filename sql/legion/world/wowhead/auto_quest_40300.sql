UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99093);

DELETE FROM `creature_queststarter` WHERE `quest`=40300;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99093, 40300);

DELETE FROM `creature_questender` WHERE `quest`=40300;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99483, 40300);
