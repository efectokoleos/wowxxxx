UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109049);

DELETE FROM `creature_queststarter` WHERE `quest`=42957;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109049, 42957);

DELETE FROM `creature_questender` WHERE `quest`=42957;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109187, 42957);

DELETE FROM `quest_template_addon` WHERE `ID`=42957;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42957, 42766, 42868);
