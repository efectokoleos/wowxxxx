UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120041);

DELETE FROM `creature_queststarter` WHERE `quest`=46924;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120041, 46924);

DELETE FROM `creature_questender` WHERE `quest`=46924;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115750, 46924);

DELETE FROM `quest_template_addon` WHERE `ID`=46924;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46924, 45498, 46674);
