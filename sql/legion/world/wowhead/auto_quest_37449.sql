UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90474);

DELETE FROM `creature_queststarter` WHERE `quest`=37449;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90474, 37449);

DELETE FROM `creature_questender` WHERE `quest`=37449;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89362, 37449);

DELETE FROM `quest_template_addon` WHERE `ID`=37449;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37449, 37450);
