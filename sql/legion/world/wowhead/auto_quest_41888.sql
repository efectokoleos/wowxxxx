UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105120);

DELETE FROM `creature_queststarter` WHERE `quest`=41888;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105120, 41888);

DELETE FROM `creature_questender` WHERE `quest`=41888;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96746, 41888);

DELETE FROM `quest_template_addon` WHERE `ID`=41888;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41888, 41934);
