UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101314);

DELETE FROM `creature_queststarter` WHERE `quest`=41625;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101314, 41625);

DELETE FROM `creature_questender` WHERE `quest`=41625;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105081, 41625);

DELETE FROM `quest_template_addon` WHERE `ID`=41625;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41625, 41626);
