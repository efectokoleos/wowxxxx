UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103092);

DELETE FROM `creature_queststarter` WHERE `quest`=41141;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103092, 41141);

DELETE FROM `creature_questender` WHERE `quest`=41141;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103092, 41141);

DELETE FROM `quest_template_addon` WHERE `ID`=41141;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41141, 41124);
