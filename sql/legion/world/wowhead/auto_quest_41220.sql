UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(86563);

DELETE FROM `creature_queststarter` WHERE `quest`=41220;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(86563, 41220);

DELETE FROM `creature_questender` WHERE `quest`=41220;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93326, 41220);

DELETE FROM `quest_template_addon` WHERE `ID`=41220;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41220, 39718);
