UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97986);

DELETE FROM `creature_queststarter` WHERE `quest`=40005;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97986, 40005);

DELETE FROM `creature_questender` WHERE `quest`=40005;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97988, 40005);

DELETE FROM `quest_template_addon` WHERE `ID`=40005;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40005, 40072);
