UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97748);

DELETE FROM `creature_queststarter` WHERE `quest`=40052;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97748, 40052);

DELETE FROM `creature_questender` WHERE `quest`=40052;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92195, 40052);

DELETE FROM `quest_template_addon` WHERE `ID`=40052;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40052, 39947, 39951);
