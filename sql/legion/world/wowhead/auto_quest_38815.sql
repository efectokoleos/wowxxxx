UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97270);

DELETE FROM `creature_queststarter` WHERE `quest`=38815;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97270, 38815);

DELETE FROM `creature_questender` WHERE `quest`=38815;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93234, 38815);

DELETE FROM `quest_template_addon` WHERE `ID`=38815;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38815, 38816, 38818);
