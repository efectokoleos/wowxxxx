UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96257);

DELETE FROM `creature_queststarter` WHERE `quest`=39594;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96257, 39594);

DELETE FROM `creature_questender` WHERE `quest`=39594;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96258, 39594);

DELETE FROM `quest_template_addon` WHERE `ID`=39594;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39594, 39592, 39597);
