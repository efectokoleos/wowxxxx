UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93326);

DELETE FROM `creature_queststarter` WHERE `quest`=38443;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93326, 38443);

DELETE FROM `creature_questender` WHERE `quest`=38443;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93354, 38443);

DELETE FROM `quest_template_addon` WHERE `ID`=38443;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38443, 37853);
