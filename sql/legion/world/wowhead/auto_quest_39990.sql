UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97892);

DELETE FROM `creature_queststarter` WHERE `quest`=39990;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97892, 39990);

DELETE FROM `creature_questender` WHERE `quest`=39990;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97892, 39990);

DELETE FROM `quest_template_addon` WHERE `ID`=39990;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39990, 39988, 39991);
