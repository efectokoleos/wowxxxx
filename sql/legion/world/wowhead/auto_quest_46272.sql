UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120281);

DELETE FROM `creature_queststarter` WHERE `quest`=46272;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120281, 46272);

DELETE FROM `creature_questender` WHERE `quest`=46272;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119338, 46272);

DELETE FROM `quest_template_addon` WHERE `ID`=46272;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46272, 46274);
