UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(83823);

DELETE FROM `creature_queststarter` WHERE `quest`=37834;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(83823, 37834);

DELETE FROM `creature_questender` WHERE `quest`=37834;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(83823, 37834);

DELETE FROM `quest_template_addon` WHERE `ID`=37834;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37834, 36017, 37836);
