UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97407);

DELETE FROM `creature_queststarter` WHERE `quest`=40594;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97407, 40594);

DELETE FROM `creature_questender` WHERE `quest`=40594;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97407, 40594);

DELETE FROM `quest_template_addon` WHERE `ID`=40594;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40594, 39025, 39027);
