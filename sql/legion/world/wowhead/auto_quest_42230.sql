UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108101);

DELETE FROM `creature_queststarter` WHERE `quest`=42230;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108101, 42230);

DELETE FROM `gameobject_questender` WHERE `quest`=42230;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(265435, 42230);

DELETE FROM `quest_template_addon` WHERE `ID`=42230;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42230, 42228);
