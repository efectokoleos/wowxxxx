UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93231);

DELETE FROM `creature_queststarter` WHERE `quest`=39791;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93231, 39791);

DELETE FROM `creature_questender` WHERE `quest`=39791;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93446, 39791);

DELETE FROM `quest_template_addon` WHERE `ID`=39791;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39791, 38808, 38816);
