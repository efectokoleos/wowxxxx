UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93526);

DELETE FROM `creature_queststarter` WHERE `quest`=40535;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93526, 40535);

DELETE FROM `creature_questender` WHERE `quest`=40535;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100776, 40535);

DELETE FROM `quest_template_addon` WHERE `ID`=40535;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40535, 40536);
