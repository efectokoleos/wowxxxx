DELETE FROM `creature_questender` WHERE `quest`=37993;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(78487, 37993);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37993;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37993);

DELETE FROM `quest_template_addon` WHERE `ID`=37993;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37993, 37851);
