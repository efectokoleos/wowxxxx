UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102700);

DELETE FROM `creature_queststarter` WHERE `quest`=42476;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102700, 42476);

DELETE FROM `creature_questender` WHERE `quest`=42476;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102700, 42476);

DELETE FROM `quest_template_addon` WHERE `ID`=42476;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42476, 42479);
