UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108072);

DELETE FROM `creature_queststarter` WHERE `quest`=42641;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108072, 42641);

DELETE FROM `creature_questender` WHERE `quest`=42641;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108072, 42641);

DELETE FROM `quest_template_addon` WHERE `ID`=42641;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42641, 42635);
