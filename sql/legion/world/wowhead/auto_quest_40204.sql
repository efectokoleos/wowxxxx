UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98948);

DELETE FROM `creature_queststarter` WHERE `quest`=40204;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98948, 40204);

DELETE FROM `creature_questender` WHERE `quest`=40204;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98948, 40204);

DELETE FROM `quest_template_addon` WHERE `ID`=40204;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40204, 40210);
