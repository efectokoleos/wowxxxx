UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95421);

DELETE FROM `creature_queststarter` WHERE `quest`=39588;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95421, 39588);

DELETE FROM `creature_questender` WHERE `quest`=39588;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95421, 39588);

DELETE FROM `quest_template_addon` WHERE `ID`=39588;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39588, 39381, 39426);
