UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=44782;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 44782);

DELETE FROM `creature_questender` WHERE `quest`=44782;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115465, 44782);

DELETE FROM `quest_template_addon` WHERE `ID`=44782;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44782, 44781, 44821);
