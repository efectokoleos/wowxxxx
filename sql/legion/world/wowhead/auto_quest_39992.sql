UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97903);

DELETE FROM `creature_queststarter` WHERE `quest`=39992;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97903, 39992);

DELETE FROM `creature_questender` WHERE `quest`=39992;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98825, 39992);

DELETE FROM `quest_template_addon` WHERE `ID`=39992;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39992, 39991);
