UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90543);

DELETE FROM `creature_queststarter` WHERE `quest`=42271;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90543, 42271);

DELETE FROM `creature_questender` WHERE `quest`=42271;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89975, 42271);

DELETE FROM `quest_template_addon` WHERE `ID`=42271;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42271, 37991);
