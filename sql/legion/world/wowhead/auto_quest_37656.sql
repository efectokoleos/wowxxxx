UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90474);

DELETE FROM `creature_queststarter` WHERE `quest`=37656;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90474, 37656);

DELETE FROM `creature_questender` WHERE `quest`=37656;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90474, 37656);

DELETE FROM `quest_template_addon` WHERE `ID`=37656;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37656, 37449);
