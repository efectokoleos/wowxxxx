UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101282);

DELETE FROM `creature_queststarter` WHERE `quest`=40987;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101282, 40987);

DELETE FROM `creature_questender` WHERE `quest`=40987;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93437, 40987);

DELETE FROM `quest_template_addon` WHERE `ID`=40987;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40987, 40986);
