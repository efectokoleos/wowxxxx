UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105018);

DELETE FROM `creature_queststarter` WHERE `quest`=41768;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105018, 41768);

DELETE FROM `creature_questender` WHERE `quest`=41768;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105102, 41768);

DELETE FROM `quest_template_addon` WHERE `ID`=41768;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41768, 41769);
