UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96469);

DELETE FROM `creature_queststarter` WHERE `quest`=39530;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96469, 39530);

DELETE FROM `creature_questender` WHERE `quest`=39530;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96586, 39530);

DELETE FROM `quest_template_addon` WHERE `ID`=39530;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39530, 39192);
