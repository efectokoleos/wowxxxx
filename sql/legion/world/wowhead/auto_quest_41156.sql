UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101282);

DELETE FROM `creature_queststarter` WHERE `quest`=41156;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101282, 41156);

DELETE FROM `creature_questender` WHERE `quest`=41156;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101097, 41156);

DELETE FROM `quest_template_addon` WHERE `ID`=41156;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41156, 40623, 40731);
