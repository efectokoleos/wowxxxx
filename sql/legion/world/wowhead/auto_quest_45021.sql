UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105926);

DELETE FROM `creature_queststarter` WHERE `quest`=45021;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105926, 45021);

DELETE FROM `creature_questender` WHERE `quest`=45021;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115863, 45021);

DELETE FROM `quest_template_addon` WHERE `ID`=45021;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45021, 45024);
