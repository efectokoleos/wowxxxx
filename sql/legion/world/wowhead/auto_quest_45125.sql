UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116714);

DELETE FROM `creature_queststarter` WHERE `quest`=45125;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116714, 45125);

DELETE FROM `creature_questender` WHERE `quest`=45125;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116714, 45125);

DELETE FROM `quest_template_addon` WHERE `ID`=45125;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45125, 45126);
