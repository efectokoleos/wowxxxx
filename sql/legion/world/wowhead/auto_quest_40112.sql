UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97891);

DELETE FROM `creature_queststarter` WHERE `quest`=40112;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97891, 40112);

DELETE FROM `creature_questender` WHERE `quest`=40112;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98794, 40112);

DELETE FROM `quest_template_addon` WHERE `ID`=40112;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40112, 39983, 39988);
