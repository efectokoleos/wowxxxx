UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114909);

DELETE FROM `creature_queststarter` WHERE `quest`=44738;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114909, 44738);

DELETE FROM `creature_questender` WHERE `quest`=44738;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115039, 44738);

DELETE FROM `quest_template_addon` WHERE `ID`=44738;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44738, 44739, 44740);
