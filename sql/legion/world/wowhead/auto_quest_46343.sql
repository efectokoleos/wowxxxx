UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120722);

DELETE FROM `creature_queststarter` WHERE `quest`=46343;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120722, 46343);

DELETE FROM `creature_questender` WHERE `quest`=46343;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120722, 46343);

DELETE FROM `quest_template_addon` WHERE `ID`=46343;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46343, 46342);
