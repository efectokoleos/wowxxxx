UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97979);

DELETE FROM `creature_queststarter` WHERE `quest`=40002;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97979, 40002);

DELETE FROM `creature_questender` WHERE `quest`=40002;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97979, 40002);

DELETE FROM `quest_template_addon` WHERE `ID`=40002;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40002, 40001, 40003);
