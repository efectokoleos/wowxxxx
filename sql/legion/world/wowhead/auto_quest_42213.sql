UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106780);

DELETE FROM `creature_queststarter` WHERE `quest`=42213;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106780, 42213);

DELETE FROM `gameobject_questender` WHERE `quest`=42213;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(246465, 42213);

DELETE FROM `quest_template_addon` WHERE `ID`=42213;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42213, 38286);
