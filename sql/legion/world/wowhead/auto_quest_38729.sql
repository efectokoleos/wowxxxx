UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97303);

DELETE FROM `creature_queststarter` WHERE `quest`=38729;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97303, 38729);

DELETE FROM `gameobject_questender` WHERE `quest`=38729;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(244466, 38729);
