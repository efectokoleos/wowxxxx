UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115248);

DELETE FROM `creature_queststarter` WHERE `quest`=44753;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115248, 44753);

DELETE FROM `creature_questender` WHERE `quest`=44753;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115557, 44753);
