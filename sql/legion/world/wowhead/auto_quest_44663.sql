UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114562);

DELETE FROM `creature_queststarter` WHERE `quest`=44663;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114562, 44663);

DELETE FROM `creature_questender` WHERE `quest`=44663;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111109, 44663);

DELETE FROM `quest_template_addon` WHERE `ID`=44663;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44663, 40605);
