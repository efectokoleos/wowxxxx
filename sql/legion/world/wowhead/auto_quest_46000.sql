UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119001);

DELETE FROM `creature_queststarter` WHERE `quest`=46000;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119001, 46000);

DELETE FROM `creature_questender` WHERE `quest`=46000;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118242, 46000);

DELETE FROM `quest_template_addon` WHERE `ID`=46000;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46000, 45586, 46290);
