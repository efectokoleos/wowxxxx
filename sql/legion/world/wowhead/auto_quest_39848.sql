UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97419);

DELETE FROM `creature_queststarter` WHERE `quest`=39848;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97419, 39848);

DELETE FROM `gameobject_questender` WHERE `quest`=39848;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(244559, 39848);

DELETE FROM `quest_template_addon` WHERE `ID`=39848;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39848, 38347, 39857);
