UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102578);

DELETE FROM `creature_queststarter` WHERE `quest`=40954;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102578, 40954);

DELETE FROM `creature_questender` WHERE `quest`=40954;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102574, 40954);

DELETE FROM `quest_template_addon` WHERE `ID`=40954;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40954, 40953, 40955);
