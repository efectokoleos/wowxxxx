UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104850);

DELETE FROM `creature_queststarter` WHERE `quest`=41732;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104850, 41732);

DELETE FROM `creature_questender` WHERE `quest`=41732;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104744, 41732);

DELETE FROM `quest_template_addon` WHERE `ID`=41732;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41732, 41729, 41733);
