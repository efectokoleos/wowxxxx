UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107981);

DELETE FROM `creature_queststarter` WHERE `quest`=42399;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107981, 42399);

DELETE FROM `creature_questender` WHERE `quest`=42399;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107973, 42399);

DELETE FROM `quest_template_addon` WHERE `ID`=42399;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42399, 42400);
