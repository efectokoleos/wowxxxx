UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117715);

DELETE FROM `creature_queststarter` WHERE `quest`=45767;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117715, 45767);

DELETE FROM `creature_questender` WHERE `quest`=45767;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117715, 45767);

DELETE FROM `quest_template_addon` WHERE `ID`=45767;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45767, 45971);
