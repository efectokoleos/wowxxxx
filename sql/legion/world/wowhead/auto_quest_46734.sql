UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120215);

DELETE FROM `creature_queststarter` WHERE `quest`=46734;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120215, 46734);

DELETE FROM `creature_questender` WHERE `quest`=46734;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116302, 46734);

DELETE FROM `quest_template_addon` WHERE `ID`=46734;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46734, 46730);
