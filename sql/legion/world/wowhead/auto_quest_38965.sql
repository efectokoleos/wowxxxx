UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96444);

DELETE FROM `creature_queststarter` WHERE `quest`=38965;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96444, 38965);

DELETE FROM `creature_questender` WHERE `quest`=38965;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93542, 38965);

DELETE FROM `quest_template_addon` WHERE `ID`=38965;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38965, 39667, 38966);
