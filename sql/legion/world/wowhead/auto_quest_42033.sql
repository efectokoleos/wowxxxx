UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101195);

DELETE FROM `creature_queststarter` WHERE `quest`=42033;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101195, 42033);

DELETE FROM `creature_questender` WHERE `quest`=42033;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105903, 42033);

DELETE FROM `quest_template_addon` WHERE `ID`=42033;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42033, 42031);
