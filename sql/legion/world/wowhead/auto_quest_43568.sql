UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107126);

DELETE FROM `creature_queststarter` WHERE `quest`=43568;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107126, 43568);

DELETE FROM `creature_questender` WHERE `quest`=43568;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(112146, 43568);

DELETE FROM `quest_template_addon` WHERE `ID`=43568;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43568, 43567);
