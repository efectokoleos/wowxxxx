UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110714);

DELETE FROM `creature_queststarter` WHERE `quest`=43508;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110714, 43508);

DELETE FROM `creature_questender` WHERE `quest`=43508;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110793, 43508);

DELETE FROM `quest_template_addon` WHERE `ID`=43508;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43508, 37666);
