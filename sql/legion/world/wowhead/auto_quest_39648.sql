UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96451);

DELETE FROM `creature_queststarter` WHERE `quest`=39648;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96451, 39648);

DELETE FROM `creature_questender` WHERE `quest`=39648;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96362, 39648);
