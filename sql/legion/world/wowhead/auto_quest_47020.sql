UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90418);

DELETE FROM `creature_queststarter` WHERE `quest`=47020;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90418, 47020);

DELETE FROM `creature_questender` WHERE `quest`=47020;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117225, 47020);

DELETE FROM `quest_template_addon` WHERE `ID`=47020;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(47020, 47031, 45564);
