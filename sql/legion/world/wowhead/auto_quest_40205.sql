UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98948);

DELETE FROM `creature_queststarter` WHERE `quest`=40205;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98948, 40205);

DELETE FROM `creature_questender` WHERE `quest`=40205;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98948, 40205);

DELETE FROM `quest_template_addon` WHERE `ID`=40205;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40205, 40210);
