UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106914);

DELETE FROM `creature_queststarter` WHERE `quest`=42238;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106914, 42238);

DELETE FROM `creature_questender` WHERE `quest`=42238;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91166, 42238);

DELETE FROM `quest_template_addon` WHERE `ID`=42238;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42238, 38460);
