UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104797);

DELETE FROM `creature_queststarter` WHERE `quest`=41771;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104797, 41771);

DELETE FROM `creature_questender` WHERE `quest`=41771;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113340, 41771);

DELETE FROM `quest_template_addon` WHERE `ID`=41771;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41771, 41770);
