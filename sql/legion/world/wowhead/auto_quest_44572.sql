UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114490);

DELETE FROM `creature_queststarter` WHERE `quest`=44572;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114490, 44572);

DELETE FROM `gameobject_questender` WHERE `quest`=44572;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259863, 44572);
