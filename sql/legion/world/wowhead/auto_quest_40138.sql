UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98720);

DELETE FROM `creature_queststarter` WHERE `quest`=40138;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98720, 40138);

DELETE FROM `creature_questender` WHERE `quest`=40138;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98720, 40138);

DELETE FROM `quest_template_addon` WHERE `ID`=40138;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40138, 40140);
