UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105917);

DELETE FROM `creature_queststarter` WHERE `quest`=42493;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105917, 42493);

DELETE FROM `creature_questender` WHERE `quest`=42493;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108085, 42493);

DELETE FROM `quest_template_addon` WHERE `ID`=42493;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42493, 42520);
