DELETE FROM `creature_questender` WHERE `quest`=37885;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(81492, 37885);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37885;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37885);

DELETE FROM `quest_template_addon` WHERE `ID`=37885;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37885, 37884);
