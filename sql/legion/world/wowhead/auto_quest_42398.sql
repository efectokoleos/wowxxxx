UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107981);

DELETE FROM `creature_queststarter` WHERE `quest`=42398;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107981, 42398);

DELETE FROM `creature_questender` WHERE `quest`=42398;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107981, 42398);

DELETE FROM `quest_template_addon` WHERE `ID`=42398;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42398, 42397);
