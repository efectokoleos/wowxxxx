UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=44739;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 44739);

DELETE FROM `creature_questender` WHERE `quest`=44739;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(114909, 44739);

DELETE FROM `quest_template_addon` WHERE `ID`=44739;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44739, 45266, 44738);
