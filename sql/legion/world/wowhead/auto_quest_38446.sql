UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91935);

DELETE FROM `creature_queststarter` WHERE `quest`=38446;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91935, 38446);

DELETE FROM `creature_questender` WHERE `quest`=38446;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90309, 38446);

DELETE FROM `quest_template_addon` WHERE `ID`=38446;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38446, 38581);
