UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108089);

DELETE FROM `creature_queststarter` WHERE `quest`=42404;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108089, 42404);

DELETE FROM `creature_questender` WHERE `quest`=42404;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107973, 42404);

DELETE FROM `quest_template_addon` WHERE `ID`=42404;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42404, 42401, 42689);
