UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93531);

DELETE FROM `creature_queststarter` WHERE `quest`=39891;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93531, 39891);

DELETE FROM `creature_questender` WHERE `quest`=39891;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107139, 39891);

DELETE FROM `quest_template_addon` WHERE `ID`=39891;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39891, 40169);
