UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115693);

DELETE FROM `creature_queststarter` WHERE `quest`=44874;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115693, 44874);

DELETE FROM `creature_questender` WHERE `quest`=44874;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115693, 44874);

DELETE FROM `quest_template_addon` WHERE `ID`=44874;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44874, 44875);
