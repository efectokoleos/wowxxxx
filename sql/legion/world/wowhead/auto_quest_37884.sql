DELETE FROM `gameobject_queststarter` WHERE `quest`=37884;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37884);

DELETE FROM `quest_template_addon` WHERE `ID`=37884;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37884, 37883, 37885);
