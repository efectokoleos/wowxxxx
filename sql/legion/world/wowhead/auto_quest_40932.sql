UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100729);

DELETE FROM `creature_queststarter` WHERE `quest`=40932;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100729, 40932);

DELETE FROM `gameobject_questender` WHERE `quest`=40932;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(245793, 40932);

DELETE FROM `quest_template_addon` WHERE `ID`=40932;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40932, 40931);
