UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96763);

DELETE FROM `creature_queststarter` WHERE `quest`=39729;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96763, 39729);

DELETE FROM `creature_questender` WHERE `quest`=39729;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 39729);

DELETE FROM `quest_template_addon` WHERE `ID`=39729;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39729, 39680, 38522);
