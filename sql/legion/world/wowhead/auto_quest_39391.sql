UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95421);

DELETE FROM `creature_queststarter` WHERE `quest`=39391;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95421, 39391);

DELETE FROM `creature_questender` WHERE `quest`=39391;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95421, 39391);

DELETE FROM `quest_template_addon` WHERE `ID`=39391;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39391, 39381, 39426);
