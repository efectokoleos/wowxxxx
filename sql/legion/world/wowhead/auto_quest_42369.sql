UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107244);

DELETE FROM `creature_queststarter` WHERE `quest`=42369;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107244, 42369);

DELETE FROM `creature_questender` WHERE `quest`=42369;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107244, 42369);

DELETE FROM `quest_template_addon` WHERE `ID`=42369;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42369, 42375);
