UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90238);

DELETE FROM `creature_queststarter` WHERE `quest`=37935;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90238, 37935);

DELETE FROM `creature_questender` WHERE `quest`=37935;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90481, 37935);

DELETE FROM `quest_template_addon` WHERE `ID`=37935;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37935, 37934);
