UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108782);

DELETE FROM `creature_queststarter` WHERE `quest`=42787;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108782, 42787);

DELETE FROM `creature_questender` WHERE `quest`=42787;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108782, 42787);

DELETE FROM `quest_template_addon` WHERE `ID`=42787;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42787, 42731, 42735);
