UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103415);

DELETE FROM `creature_queststarter` WHERE `quest`=40392;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103415, 40392);

DELETE FROM `creature_questender` WHERE `quest`=40392;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100190, 40392);

DELETE FROM `quest_template_addon` WHERE `ID`=40392;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40392, 41540);
