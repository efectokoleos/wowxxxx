UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90431);

DELETE FROM `creature_queststarter` WHERE `quest`=45412;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90431, 45412);

DELETE FROM `creature_questender` WHERE `quest`=45412;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116880, 45412);

DELETE FROM `quest_template_addon` WHERE `ID`=45412;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45412, 47030, 45413);
