UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92264);

DELETE FROM `creature_queststarter` WHERE `quest`=38528;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92264, 38528);

DELETE FROM `creature_questender` WHERE `quest`=38528;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92264, 38528);

DELETE FROM `quest_template_addon` WHERE `ID`=38528;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38528, 38527, 38530);
