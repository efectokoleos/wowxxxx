DELETE FROM `creature_questender` WHERE `quest`=40519;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107934, 40519);

DELETE FROM `gameobject_queststarter` WHERE `quest`=40519;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(206111, 40519);

DELETE FROM `quest_template_addon` WHERE `ID`=40519;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40519, 42782);
