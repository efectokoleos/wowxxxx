UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94789);

DELETE FROM `creature_queststarter` WHERE `quest`=39243;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94789, 39243);

DELETE FROM `creature_questender` WHERE `quest`=39243;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94789, 39243);

DELETE FROM `quest_template_addon` WHERE `ID`=39243;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39243, 39401);
