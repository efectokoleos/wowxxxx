UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89661);

DELETE FROM `creature_queststarter` WHERE `quest`=42370;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89661, 42370);

DELETE FROM `creature_questender` WHERE `quest`=42370;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89661, 42370);

DELETE FROM `quest_template_addon` WHERE `ID`=42370;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42370, 37518);
