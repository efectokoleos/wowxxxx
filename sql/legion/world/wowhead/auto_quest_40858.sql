UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102195);

DELETE FROM `creature_queststarter` WHERE `quest`=40858;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102195, 40858);

DELETE FROM `creature_questender` WHERE `quest`=40858;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93520, 40858);

DELETE FROM `quest_template_addon` WHERE `ID`=40858;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40858, 40856, 40863);
