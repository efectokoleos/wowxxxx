UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120118);

DELETE FROM `creature_queststarter` WHERE `quest`=46845;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120118, 46845);

DELETE FROM `creature_questender` WHERE `quest`=46845;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111323, 46845);

DELETE FROM `quest_template_addon` WHERE `ID`=46845;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46845, 46832);
