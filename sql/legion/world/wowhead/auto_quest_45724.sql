UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120244);

DELETE FROM `creature_queststarter` WHERE `quest`=45724;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120244, 45724);

DELETE FROM `creature_questender` WHERE `quest`=45724;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120244, 45724);

DELETE FROM `quest_template_addon` WHERE `ID`=45724;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45724, 45706, 44800);
