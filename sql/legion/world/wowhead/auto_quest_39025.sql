UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93805);

DELETE FROM `creature_queststarter` WHERE `quest`=39025;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93805, 39025);

DELETE FROM `creature_questender` WHERE `quest`=39025;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97553, 39025);

DELETE FROM `quest_template_addon` WHERE `ID`=39025;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39025, 42088, 39043);
