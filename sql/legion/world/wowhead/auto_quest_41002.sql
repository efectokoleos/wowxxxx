UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101035);

DELETE FROM `creature_queststarter` WHERE `quest`=41002;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101035, 41002);

DELETE FROM `creature_questender` WHERE `quest`=41002;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95234, 41002);

DELETE FROM `quest_template_addon` WHERE `ID`=41002;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41002, 40593, 44663);
