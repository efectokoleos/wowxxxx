UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119209);

DELETE FROM `creature_queststarter` WHERE `quest`=45145;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119209, 45145);

DELETE FROM `creature_questender` WHERE `quest`=45145;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109102, 45145);

DELETE FROM `quest_template_addon` WHERE `ID`=45145;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45145, 46259);
