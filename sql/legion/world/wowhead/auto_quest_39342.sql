UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101492);

DELETE FROM `creature_queststarter` WHERE `quest`=39342;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101492, 39342);

DELETE FROM `creature_questender` WHERE `quest`=39342;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101492, 39342);

DELETE FROM `quest_template_addon` WHERE `ID`=39342;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39342, 39333);
