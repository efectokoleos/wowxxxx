UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108961);

DELETE FROM `creature_queststarter` WHERE `quest`=42814;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108961, 42814);

DELETE FROM `creature_questender` WHERE `quest`=42814;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96183, 42814);

DELETE FROM `quest_template_addon` WHERE `ID`=42814;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42814, 38904);
