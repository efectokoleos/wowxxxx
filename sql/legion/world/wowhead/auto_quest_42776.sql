UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108571);

DELETE FROM `creature_queststarter` WHERE `quest`=42776;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108571, 42776);

DELETE FROM `creature_questender` WHERE `quest`=42776;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98648, 42776);

DELETE FROM `quest_template_addon` WHERE `ID`=42776;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42776, 42775, 42669);
