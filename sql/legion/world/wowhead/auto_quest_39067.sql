UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93822);

DELETE FROM `creature_queststarter` WHERE `quest`=39067;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93822, 39067);

DELETE FROM `creature_questender` WHERE `quest`=39067;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93822, 39067);

DELETE FROM `quest_template_addon` WHERE `ID`=39067;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39067, 39068);
