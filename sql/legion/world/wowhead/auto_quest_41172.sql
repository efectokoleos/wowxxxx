UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103482);

DELETE FROM `creature_queststarter` WHERE `quest`=41172;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103482, 41172);

DELETE FROM `creature_questender` WHERE `quest`=41172;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103482, 41172);

DELETE FROM `quest_template_addon` WHERE `ID`=41172;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41172, 41171, 41173);
