UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107934);

DELETE FROM `creature_queststarter` WHERE `quest`=42782;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107934, 42782);

DELETE FROM `creature_questender` WHERE `quest`=42782;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108916, 42782);

DELETE FROM `quest_template_addon` WHERE `ID`=42782;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42782, 40519, 42740);
