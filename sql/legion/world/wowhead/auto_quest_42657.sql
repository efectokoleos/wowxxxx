UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108455);

DELETE FROM `creature_queststarter` WHERE `quest`=42657;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108455, 42657);

DELETE FROM `creature_questender` WHERE `quest`=42657;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108492, 42657);

DELETE FROM `quest_template_addon` WHERE `ID`=42657;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42657, 42658);
