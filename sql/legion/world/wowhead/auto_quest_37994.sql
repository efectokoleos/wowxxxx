DELETE FROM `creature_questender` WHERE `quest`=37994;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(78487, 37994);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37994;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37994);

DELETE FROM `quest_template_addon` WHERE `ID`=37994;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37994, 37884);
