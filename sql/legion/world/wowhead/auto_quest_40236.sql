UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99181);

DELETE FROM `creature_queststarter` WHERE `quest`=40236;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99181, 40236);

DELETE FROM `creature_questender` WHERE `quest`=40236;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 40236);

DELETE FROM `quest_template_addon` WHERE `ID`=40236;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40236, 40636);
