UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106250);

DELETE FROM `creature_queststarter` WHERE `quest`=42046;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106250, 42046);

DELETE FROM `creature_questender` WHERE `quest`=42046;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101195, 42046);
