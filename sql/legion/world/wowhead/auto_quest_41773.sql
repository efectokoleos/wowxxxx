UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105055);

DELETE FROM `creature_queststarter` WHERE `quest`=41773;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105055, 41773);

DELETE FROM `creature_questender` WHERE `quest`=41773;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105120, 41773);

DELETE FROM `quest_template_addon` WHERE `ID`=41773;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41773, 41934);
