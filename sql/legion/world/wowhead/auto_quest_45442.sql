UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119765);

DELETE FROM `creature_queststarter` WHERE `quest`=45442;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119765, 45442);

DELETE FROM `creature_questender` WHERE `quest`=45442;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119765, 45442);

DELETE FROM `quest_template_addon` WHERE `ID`=45442;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45442, 46320);
