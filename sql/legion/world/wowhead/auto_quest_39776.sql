UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93846);

DELETE FROM `creature_queststarter` WHERE `quest`=39776;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93846, 39776);

DELETE FROM `creature_questender` WHERE `quest`=39776;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93846, 39776);

DELETE FROM `quest_template_addon` WHERE `ID`=39776;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39776, 38915, 42088);
