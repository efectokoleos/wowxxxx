UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119539);

DELETE FROM `creature_queststarter` WHERE `quest`=45398;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119539, 45398);

DELETE FROM `creature_questender` WHERE `quest`=45398;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119539, 45398);

DELETE FROM `quest_template_addon` WHERE `ID`=45398;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45398, 45240);
