UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91866);

DELETE FROM `creature_queststarter` WHERE `quest`=38933;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91866, 38933);

DELETE FROM `creature_questender` WHERE `quest`=38933;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90250, 38933);

DELETE FROM `quest_template_addon` WHERE `ID`=38933;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38933, 39722, 39756);
