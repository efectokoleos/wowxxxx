UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98002);

DELETE FROM `creature_queststarter` WHERE `quest`=42032;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98002, 42032);

DELETE FROM `creature_questender` WHERE `quest`=42032;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98002, 42032);

DELETE FROM `quest_template_addon` WHERE `ID`=42032;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42032, 42031);
