UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=42886;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 42886);

DELETE FROM `creature_questender` WHERE `quest`=42886;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109356, 42886);

DELETE FROM `quest_template_addon` WHERE `ID`=42886;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42886, 42887);
