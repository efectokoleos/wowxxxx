UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=45833;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 45833);

DELETE FROM `creature_questender` WHERE `quest`=45833;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119821, 45833);

DELETE FROM `quest_template_addon` WHERE `ID`=45833;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45833, 44758);
