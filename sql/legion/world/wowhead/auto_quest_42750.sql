UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108358);

DELETE FROM `creature_queststarter` WHERE `quest`=42750;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108358, 42750);

DELETE FROM `creature_questender` WHERE `quest`=42750;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108304, 42750);

DELETE FROM `quest_template_addon` WHERE `ID`=42750;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42750, 42751);
