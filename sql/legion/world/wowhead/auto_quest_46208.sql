UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119527);

DELETE FROM `creature_queststarter` WHERE `quest`=46208;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119527, 46208);

DELETE FROM `creature_questender` WHERE `quest`=46208;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96469, 46208);

DELETE FROM `quest_template_addon` WHERE `ID`=46208;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46208, 46207);
