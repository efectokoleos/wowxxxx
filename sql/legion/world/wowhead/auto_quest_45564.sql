UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117225);

DELETE FROM `creature_queststarter` WHERE `quest`=45564;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117225, 45564);

DELETE FROM `creature_questender` WHERE `quest`=45564;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117249, 45564);

DELETE FROM `quest_template_addon` WHERE `ID`=45564;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45564, 45560, 45726);
