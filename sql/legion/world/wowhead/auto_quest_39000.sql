UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92213);

DELETE FROM `creature_queststarter` WHERE `quest`=39000;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92213, 39000);

DELETE FROM `creature_questender` WHERE `quest`=39000;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92213, 39000);

DELETE FROM `quest_template_addon` WHERE `ID`=39000;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39000, 39024, 39003);
