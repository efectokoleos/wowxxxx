UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92458);

DELETE FROM `creature_queststarter` WHERE `quest`=39337;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92458, 39337);

DELETE FROM `gameobject_questender` WHERE `quest`=39337;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(249211, 39337);

DELETE FROM `quest_template_addon` WHERE `ID`=39337;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39337, 39431);
