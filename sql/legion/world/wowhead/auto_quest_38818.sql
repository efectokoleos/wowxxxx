UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93234);

DELETE FROM `creature_queststarter` WHERE `quest`=38818;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93234, 38818);

DELETE FROM `creature_questender` WHERE `quest`=38818;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97319, 38818);

DELETE FROM `quest_template_addon` WHERE `ID`=38818;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38818, 38815);
