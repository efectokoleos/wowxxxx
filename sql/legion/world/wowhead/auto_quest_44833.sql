UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115524);

DELETE FROM `creature_queststarter` WHERE `quest`=44833;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115524, 44833);

DELETE FROM `creature_questender` WHERE `quest`=44833;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 44833);

DELETE FROM `quest_template_addon` WHERE `ID`=44833;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44833, 44832);
