UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102655);

DELETE FROM `creature_queststarter` WHERE `quest`=41015;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102655, 41015);

DELETE FROM `creature_questender` WHERE `quest`=41015;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102709, 41015);

DELETE FROM `quest_template_addon` WHERE `ID`=41015;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41015, 40938, 41017);
