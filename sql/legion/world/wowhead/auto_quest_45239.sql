UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93538);

DELETE FROM `creature_queststarter` WHERE `quest`=45239;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93538, 45239);

DELETE FROM `creature_questender` WHERE `quest`=45239;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116396, 45239);

DELETE FROM `quest_template_addon` WHERE `ID`=45239;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45239, 45238);
