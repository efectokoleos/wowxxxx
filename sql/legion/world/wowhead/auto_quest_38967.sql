UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93542);

DELETE FROM `creature_queststarter` WHERE `quest`=38967;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93542, 38967);

DELETE FROM `creature_questender` WHERE `quest`=38967;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93969, 38967);

DELETE FROM `quest_template_addon` WHERE `ID`=38967;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38967, 38962, 38968);
