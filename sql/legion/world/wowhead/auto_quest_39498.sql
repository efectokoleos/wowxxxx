UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96038);

DELETE FROM `creature_queststarter` WHERE `quest`=39498;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96038, 39498);

DELETE FROM `creature_questender` WHERE `quest`=39498;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97662, 39498);

DELETE FROM `quest_template_addon` WHERE `ID`=39498;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39498, 39487, 42104);
