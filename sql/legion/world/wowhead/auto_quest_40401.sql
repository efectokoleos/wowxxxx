UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100185);

DELETE FROM `creature_queststarter` WHERE `quest`=40401;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100185, 40401);

DELETE FROM `creature_questender` WHERE `quest`=40401;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100192, 40401);

DELETE FROM `quest_template_addon` WHERE `ID`=40401;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40401, 40469);
