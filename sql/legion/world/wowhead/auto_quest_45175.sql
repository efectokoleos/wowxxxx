UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89398);

DELETE FROM `creature_queststarter` WHERE `quest`=45175;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89398, 45175);

DELETE FROM `creature_questender` WHERE `quest`=45175;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89398, 45175);

DELETE FROM `quest_template_addon` WHERE `ID`=45175;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45175, 45174, 45176);
