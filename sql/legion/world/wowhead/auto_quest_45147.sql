UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109102);

DELETE FROM `creature_queststarter` WHERE `quest`=45147;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109102, 45147);

DELETE FROM `creature_questender` WHERE `quest`=45147;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119545, 45147);

DELETE FROM `quest_template_addon` WHERE `ID`=45147;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45147, 45148);
