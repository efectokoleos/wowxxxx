UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91223);

DELETE FROM `creature_queststarter` WHERE `quest`=38148;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91223, 38148);

DELETE FROM `creature_questender` WHERE `quest`=38148;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91109, 38148);

DELETE FROM `quest_template_addon` WHERE `ID`=38148;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38148, 40573, 38377);
