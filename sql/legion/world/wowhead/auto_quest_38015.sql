UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90738);

DELETE FROM `creature_queststarter` WHERE `quest`=38015;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90738, 38015);

DELETE FROM `creature_questender` WHERE `quest`=38015;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90738, 38015);

DELETE FROM `quest_template_addon` WHERE `ID`=38015;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38015, 37862);
