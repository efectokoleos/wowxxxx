UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89822);

DELETE FROM `creature_queststarter` WHERE `quest`=37890;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89822, 37890);

DELETE FROM `creature_questender` WHERE `quest`=37890;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90238, 37890);

DELETE FROM `quest_template_addon` WHERE `ID`=37890;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37890, 37889, 37934);
