UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106881);

DELETE FROM `creature_queststarter` WHERE `quest`=37659;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106881, 37659);

DELETE FROM `gameobject_questender` WHERE `quest`=37659;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(239328, 37659);

DELETE FROM `quest_template_addon` WHERE `ID`=37659;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37659, 37654);
