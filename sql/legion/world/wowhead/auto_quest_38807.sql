UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93189);

DELETE FROM `creature_queststarter` WHERE `quest`=38807;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93189, 38807);

DELETE FROM `creature_questender` WHERE `quest`=38807;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93189, 38807);

DELETE FROM `quest_template_addon` WHERE `ID`=38807;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38807, 39790);
