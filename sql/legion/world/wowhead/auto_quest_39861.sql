UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91172);

DELETE FROM `creature_queststarter` WHERE `quest`=39861;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91172, 39861);

DELETE FROM `creature_questender` WHERE `quest`=39861;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91462, 39861);

DELETE FROM `quest_template_addon` WHERE `ID`=39861;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39861, 39731, 40122);
