UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91242);

DELETE FROM `creature_queststarter` WHERE `quest`=38572;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91242, 38572);

DELETE FROM `creature_questender` WHERE `quest`=38572;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91242, 38572);

DELETE FROM `quest_template_addon` WHERE `ID`=38572;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38572, 38571, 38573);
