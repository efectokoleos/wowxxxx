UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114493);

DELETE FROM `creature_queststarter` WHERE `quest`=44575;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114493, 44575);

DELETE FROM `gameobject_questender` WHERE `quest`=44575;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(259866, 44575);
