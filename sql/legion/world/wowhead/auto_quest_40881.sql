DELETE FROM `creature_questender` WHERE `quest`=40881;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93520, 40881);

DELETE FROM `gameobject_queststarter` WHERE `quest`=40881;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(246438, 40881);

DELETE FROM `quest_template_addon` WHERE `ID`=40881;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40881, 40880);
