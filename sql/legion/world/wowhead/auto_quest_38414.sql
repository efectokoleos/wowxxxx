UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91249);

DELETE FROM `creature_queststarter` WHERE `quest`=38414;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91249, 38414);

DELETE FROM `creature_questender` WHERE `quest`=38414;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96465, 38414);

DELETE FROM `quest_template_addon` WHERE `ID`=38414;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38414, 38412, 39652);
