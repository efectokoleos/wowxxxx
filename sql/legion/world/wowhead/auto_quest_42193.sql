UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107987);

DELETE FROM `creature_queststarter` WHERE `quest`=42193;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107987, 42193);

DELETE FROM `creature_questender` WHERE `quest`=42193;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106720, 42193);

DELETE FROM `quest_template_addon` WHERE `ID`=42193;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42193, 43750);
