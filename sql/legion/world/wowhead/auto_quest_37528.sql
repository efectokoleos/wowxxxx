UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88873);

DELETE FROM `creature_queststarter` WHERE `quest`=37528;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88873, 37528);

DELETE FROM `creature_questender` WHERE `quest`=37528;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88873, 37528);

DELETE FROM `quest_template_addon` WHERE `ID`=37528;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37528, 37510);
