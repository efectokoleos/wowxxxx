UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98017);

DELETE FROM `creature_queststarter` WHERE `quest`=39880;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98017, 39880);

DELETE FROM `creature_questender` WHERE `quest`=39880;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98017, 39880);

DELETE FROM `quest_template_addon` WHERE `ID`=39880;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39880, 39883);
