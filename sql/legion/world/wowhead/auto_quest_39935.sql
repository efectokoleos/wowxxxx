UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89398);

DELETE FROM `creature_queststarter` WHERE `quest`=39935;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89398, 39935);

DELETE FROM `creature_questender` WHERE `quest`=39935;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89398, 39935);

DELETE FROM `quest_template_addon` WHERE `ID`=39935;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39935, 39934);
