UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91072);

DELETE FROM `creature_queststarter` WHERE `quest`=38306;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91072, 38306);

DELETE FROM `creature_questender` WHERE `quest`=38306;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(6566, 38306);

DELETE FROM `quest_template_addon` WHERE `ID`=38306;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38306, 38346);
