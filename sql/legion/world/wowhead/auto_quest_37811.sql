DELETE FROM `creature_questender` WHERE `quest`=37811;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(81492, 37811);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37811;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37811);

DELETE FROM `quest_template_addon` WHERE `ID`=37811;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37811, 37799);
