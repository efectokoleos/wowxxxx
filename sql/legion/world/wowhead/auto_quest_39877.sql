UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90317);

DELETE FROM `creature_queststarter` WHERE `quest`=39877;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90317, 39877);

DELETE FROM `creature_questender` WHERE `quest`=39877;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90317, 39877);

DELETE FROM `quest_template_addon` WHERE `ID`=39877;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39877, 39876);
