UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120758);

DELETE FROM `creature_queststarter` WHERE `quest`=46342;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120758, 46342);

DELETE FROM `creature_questender` WHERE `quest`=46342;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120722, 46342);

DELETE FROM `quest_template_addon` WHERE `ID`=46342;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46342, 46341, 46343);
