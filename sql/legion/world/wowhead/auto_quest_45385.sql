UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108311);

DELETE FROM `creature_queststarter` WHERE `quest`=45385;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108311, 45385);

DELETE FROM `creature_questender` WHERE `quest`=45385;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117709, 45385);

DELETE FROM `quest_template_addon` WHERE `ID`=45385;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45385, 45764);
