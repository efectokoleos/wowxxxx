UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102996);

DELETE FROM `creature_queststarter` WHERE `quest`=41040;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102996, 41040);

DELETE FROM `creature_questender` WHERE `quest`=41040;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103049, 41040);

DELETE FROM `quest_template_addon` WHERE `ID`=41040;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41040, 32442);
