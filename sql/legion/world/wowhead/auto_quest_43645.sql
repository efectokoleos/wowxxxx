UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96530);

DELETE FROM `creature_queststarter` WHERE `quest`=43645;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96530, 43645);

DELETE FROM `creature_questender` WHERE `quest`=43645;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96530, 43645);

DELETE FROM `quest_template_addon` WHERE `ID`=43645;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43645, 40341);
