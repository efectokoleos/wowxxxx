UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105071);

DELETE FROM `creature_queststarter` WHERE `quest`=41834;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105071, 41834);

DELETE FROM `creature_questender` WHERE `quest`=41834;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105342, 41834);

DELETE FROM `quest_template_addon` WHERE `ID`=41834;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41834, 41762, 41989);
