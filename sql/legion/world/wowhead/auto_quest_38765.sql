UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93759);

DELETE FROM `creature_queststarter` WHERE `quest`=38765;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93759, 38765);

DELETE FROM `creature_questender` WHERE `quest`=38765;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93127, 38765);

DELETE FROM `quest_template_addon` WHERE `ID`=38765;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38765, 38813);
