UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108069);

DELETE FROM `creature_queststarter` WHERE `quest`=39578;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108069, 39578);

DELETE FROM `creature_questender` WHERE `quest`=39578;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99190, 39578);

DELETE FROM `quest_template_addon` WHERE `ID`=39578;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39578, 40219, 39577);
