UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117482);

DELETE FROM `creature_queststarter` WHERE `quest`=45706;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117482, 45706);

DELETE FROM `creature_questender` WHERE `quest`=45706;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120244, 45706);

DELETE FROM `quest_template_addon` WHERE `ID`=45706;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45706, 45652, 45724);
