UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119097);

DELETE FROM `creature_queststarter` WHERE `quest`=45557;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119097, 45557);

DELETE FROM `creature_questender` WHERE `quest`=45557;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119097, 45557);

DELETE FROM `quest_template_addon` WHERE `ID`=45557;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45557, 45556);
