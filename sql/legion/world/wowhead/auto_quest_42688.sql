UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107302);

DELETE FROM `creature_queststarter` WHERE `quest`=42688;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107302, 42688);

DELETE FROM `creature_questender` WHERE `quest`=42688;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107302, 42688);

DELETE FROM `quest_template_addon` WHERE `ID`=42688;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42688, 42396);
