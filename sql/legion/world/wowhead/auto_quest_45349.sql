UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110564);

DELETE FROM `creature_queststarter` WHERE `quest`=45349;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110564, 45349);

DELETE FROM `creature_questender` WHERE `quest`=45349;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118528, 45349);

DELETE FROM `quest_template_addon` WHERE `ID`=45349;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45349, 45350);
