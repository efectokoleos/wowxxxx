UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107995);

DELETE FROM `creature_queststarter` WHERE `quest`=37862;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107995, 37862);

DELETE FROM `creature_questender` WHERE `quest`=37862;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90738, 37862);

DELETE FROM `quest_template_addon` WHERE `ID`=37862;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37862, 37861, 38015);
