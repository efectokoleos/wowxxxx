UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99905);

DELETE FROM `creature_queststarter` WHERE `quest`=40217;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99905, 40217);

DELETE FROM `creature_questender` WHERE `quest`=40217;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94409, 40217);

DELETE FROM `quest_template_addon` WHERE `ID`=40217;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40217, 39859);
