UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96746);

DELETE FROM `creature_queststarter` WHERE `quest`=42200;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96746, 42200);

DELETE FROM `creature_questender` WHERE `quest`=42200;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96746, 42200);

DELETE FROM `quest_template_addon` WHERE `ID`=42200;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42200, 41901, 41775);
