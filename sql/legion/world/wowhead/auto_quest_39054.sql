UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94429);

DELETE FROM `creature_queststarter` WHERE `quest`=39054;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94429, 39054);

DELETE FROM `creature_questender` WHERE `quest`=39054;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94429, 39054);

UPDATE `quest_template_addon` SET `PrevQuestID`=39082, `NextQuestID`=39276 WHERE `ID`=39054;
