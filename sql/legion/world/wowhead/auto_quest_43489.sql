UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=43489;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 43489);

DELETE FROM `gameobject_questender` WHERE `quest`=43489;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(252232, 43489);

DELETE FROM `quest_template_addon` WHERE `ID`=43489;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43489, 43493);
