UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115367);

DELETE FROM `creature_queststarter` WHERE `quest`=45417;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115367, 45417);

DELETE FROM `creature_questender` WHERE `quest`=45417;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106522, 45417);

DELETE FROM `quest_template_addon` WHERE `ID`=45417;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45417, 44719, 45420);
