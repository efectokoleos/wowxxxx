UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93520);

DELETE FROM `creature_queststarter` WHERE `quest`=40880;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93520, 40880);

DELETE FROM `gameobject_questender` WHERE `quest`=40880;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(246438, 40880);

DELETE FROM `quest_template_addon` WHERE `ID`=40880;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40880, 40881);
