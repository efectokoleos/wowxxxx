UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99065);

DELETE FROM `creature_queststarter` WHERE `quest`=40266;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99065, 40266);

DELETE FROM `creature_questender` WHERE `quest`=40266;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99093, 40266);
