UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97736);

DELETE FROM `creature_queststarter` WHERE `quest`=39938;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97736, 39938);

DELETE FROM `creature_questender` WHERE `quest`=39938;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98026, 39938);

DELETE FROM `quest_template_addon` WHERE `ID`=39938;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39938, 39937, 39943);
