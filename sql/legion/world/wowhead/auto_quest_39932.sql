UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92195);

DELETE FROM `creature_queststarter` WHERE `quest`=39932;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92195, 39932);

DELETE FROM `creature_questender` WHERE `quest`=39932;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92195, 39932);

DELETE FROM `quest_template_addon` WHERE `ID`=39932;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39932, 39933);
