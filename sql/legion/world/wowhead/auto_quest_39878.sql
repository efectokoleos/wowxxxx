UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93531);

DELETE FROM `creature_queststarter` WHERE `quest`=39878;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93531, 39878);

DELETE FROM `creature_questender` WHERE `quest`=39878;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98017, 39878);

DELETE FROM `quest_template_addon` WHERE `ID`=39878;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39878, 40048);
