UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117252);

DELETE FROM `creature_queststarter` WHERE `quest`=45570;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117252, 45570);

DELETE FROM `creature_questender` WHERE `quest`=45570;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105081, 45570);
