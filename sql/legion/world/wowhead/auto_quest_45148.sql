UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119545);

DELETE FROM `creature_queststarter` WHERE `quest`=45148;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119545, 45148);

DELETE FROM `creature_questender` WHERE `quest`=45148;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119545, 45148);

DELETE FROM `quest_template_addon` WHERE `ID`=45148;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45148, 45147);
