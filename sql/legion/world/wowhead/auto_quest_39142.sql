UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105816);

DELETE FROM `creature_queststarter` WHERE `quest`=39142;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105816, 39142);

DELETE FROM `creature_questender` WHERE `quest`=39142;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105816, 39142);

DELETE FROM `quest_template_addon` WHERE `ID`=39142;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39142, 39389, 40218);
