UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97553);

DELETE FROM `creature_queststarter` WHERE `quest`=39043;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97553, 39043);

DELETE FROM `creature_questender` WHERE `quest`=39043;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97553, 39043);

DELETE FROM `quest_template_addon` WHERE `ID`=39043;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39043, 39025, 39027);
