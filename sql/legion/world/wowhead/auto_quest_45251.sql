UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118242);

DELETE FROM `creature_queststarter` WHERE `quest`=45251;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118242, 45251);

DELETE FROM `creature_questender` WHERE `quest`=45251;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117331, 45251);

DELETE FROM `quest_template_addon` WHERE `ID`=45251;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45251, 45614);
