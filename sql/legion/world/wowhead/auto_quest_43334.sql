UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96541);

DELETE FROM `creature_queststarter` WHERE `quest`=43334;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96541, 43334);

DELETE FROM `creature_questender` WHERE `quest`=43334;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(60968, 43334);

DELETE FROM `quest_template_addon` WHERE `ID`=43334;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43334, 43338);
