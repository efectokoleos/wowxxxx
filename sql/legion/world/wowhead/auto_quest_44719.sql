UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115710);

DELETE FROM `creature_queststarter` WHERE `quest`=44719;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115710, 44719);

DELETE FROM `creature_questender` WHERE `quest`=44719;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115367, 44719);

DELETE FROM `quest_template_addon` WHERE `ID`=44719;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44719, 45417);
