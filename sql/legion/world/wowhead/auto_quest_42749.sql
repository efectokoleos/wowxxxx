UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108576);

DELETE FROM `creature_queststarter` WHERE `quest`=42749;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108576, 42749);

DELETE FROM `quest_template_addon` WHERE `ID`=42749;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42749, 42735, 42752);
