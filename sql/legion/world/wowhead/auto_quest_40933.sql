UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100729);

DELETE FROM `creature_queststarter` WHERE `quest`=40933;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100729, 40933);

DELETE FROM `creature_questender` WHERE `quest`=40933;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100812, 40933);

DELETE FROM `quest_template_addon` WHERE `ID`=40933;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40933, 40934);
