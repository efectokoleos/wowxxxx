UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90418);

DELETE FROM `creature_queststarter` WHERE `quest`=45560;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90418, 45560);

DELETE FROM `creature_questender` WHERE `quest`=45560;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117225, 45560);

DELETE FROM `quest_template_addon` WHERE `ID`=45560;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45560, 47031, 45564);
