UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(81920);

DELETE FROM `creature_queststarter` WHERE `quest`=37326;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(81920, 37326);

DELETE FROM `creature_questender` WHERE `quest`=37326;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(83463, 37326);

DELETE FROM `quest_template_addon` WHERE `ID`=37326;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37326, 37296);
