UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92242);

DELETE FROM `creature_queststarter` WHERE `quest`=39334;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92242, 39334);

DELETE FROM `creature_questender` WHERE `quest`=39334;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92458, 39334);

DELETE FROM `quest_template_addon` WHERE `ID`=39334;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39334, 39430);
