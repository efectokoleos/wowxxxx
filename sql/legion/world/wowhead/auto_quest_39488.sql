UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96520);

DELETE FROM `creature_queststarter` WHERE `quest`=39488;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96520, 39488);

DELETE FROM `creature_questender` WHERE `quest`=39488;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96038, 39488);

DELETE FROM `quest_template_addon` WHERE `ID`=39488;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39488, 39277, 39498);
