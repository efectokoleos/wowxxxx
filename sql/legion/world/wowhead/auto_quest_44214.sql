UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108571);

DELETE FROM `creature_queststarter` WHERE `quest`=44214;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108571, 44214);

DELETE FROM `creature_questender` WHERE `quest`=44214;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110482, 44214);

DELETE FROM `quest_template_addon` WHERE `ID`=44214;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44214, 43412);
