UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114838);

DELETE FROM `creature_queststarter` WHERE `quest`=44861;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114838, 44861);

DELETE FROM `creature_questender` WHERE `quest`=44861;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116256, 44861);

DELETE FROM `quest_template_addon` WHERE `ID`=44861;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44861, 44858, 44827);
