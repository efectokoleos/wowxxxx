UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90369);

DELETE FROM `creature_queststarter` WHERE `quest`=38376;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90369, 38376);

DELETE FROM `creature_questender` WHERE `quest`=38376;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91144, 38376);

DELETE FROM `quest_template_addon` WHERE `ID`=38376;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38376, 38576);
