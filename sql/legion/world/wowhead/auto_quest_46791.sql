UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117888);

DELETE FROM `creature_queststarter` WHERE `quest`=46791;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117888, 46791);

DELETE FROM `creature_questender` WHERE `quest`=46791;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106521, 46791);

DELETE FROM `quest_template_addon` WHERE `ID`=46791;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46791, 46792);
