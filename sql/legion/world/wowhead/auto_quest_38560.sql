UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90309);

DELETE FROM `creature_queststarter` WHERE `quest`=38560;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90309, 38560);

DELETE FROM `quest_template_addon` WHERE `ID`=38560;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38560, 38270);
