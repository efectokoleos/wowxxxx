UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92213);

DELETE FROM `creature_queststarter` WHERE `quest`=39057;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92213, 39057);

DELETE FROM `creature_questender` WHERE `quest`=39057;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92213, 39057);

DELETE FROM `quest_template_addon` WHERE `ID`=39057;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39057, 39697);
