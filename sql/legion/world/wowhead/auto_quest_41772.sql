UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96746);

DELETE FROM `creature_queststarter` WHERE `quest`=41772;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96746, 41772);

DELETE FROM `creature_questender` WHERE `quest`=41772;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105207, 41772);

DELETE FROM `quest_template_addon` WHERE `ID`=41772;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41772, 41900);
