UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93522);

DELETE FROM `creature_queststarter` WHERE `quest`=40202;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93522, 40202);

DELETE FROM `creature_questender` WHERE `quest`=40202;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98948, 40202);

DELETE FROM `quest_template_addon` WHERE `ID`=40202;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40202, 40198);
