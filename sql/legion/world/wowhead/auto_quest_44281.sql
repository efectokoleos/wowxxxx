UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(4311);

DELETE FROM `creature_queststarter` WHERE `quest`=44281;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(4311, 44281);

DELETE FROM `creature_questender` WHERE `quest`=44281;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113547, 44281);

DELETE FROM `quest_template_addon` WHERE `ID`=44281;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44281, 43926, 40518);
