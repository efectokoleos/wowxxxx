UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103175);

DELETE FROM `creature_queststarter` WHERE `quest`=41123;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103175, 41123);

DELETE FROM `creature_questender` WHERE `quest`=41123;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103175, 41123);

DELETE FROM `quest_template_addon` WHERE `ID`=41123;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41123, 41307);
