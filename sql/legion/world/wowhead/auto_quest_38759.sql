UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94410);

DELETE FROM `creature_queststarter` WHERE `quest`=38759;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94410, 38759);

DELETE FROM `creature_questender` WHERE `quest`=38759;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93759, 38759);

DELETE FROM `quest_template_addon` WHERE `ID`=38759;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38759, 39050);
