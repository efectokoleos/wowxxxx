UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92909);

DELETE FROM `creature_queststarter` WHERE `quest`=38710;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92909, 38710);

DELETE FROM `creature_questender` WHERE `quest`=38710;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90369, 38710);

DELETE FROM `quest_template_addon` WHERE `ID`=38710;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38710, 40408);
