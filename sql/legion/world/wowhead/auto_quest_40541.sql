UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100499);

DELETE FROM `creature_queststarter` WHERE `quest`=40541;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100499, 40541);

DELETE FROM `creature_questender` WHERE `quest`=40541;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100499, 40541);

DELETE FROM `quest_template_addon` WHERE `ID`=40541;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40541, 40540);
