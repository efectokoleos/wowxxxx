UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94902);

DELETE FROM `creature_queststarter` WHERE `quest`=39247;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94902, 39247);

DELETE FROM `creature_questender` WHERE `quest`=39247;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102799, 39247);
