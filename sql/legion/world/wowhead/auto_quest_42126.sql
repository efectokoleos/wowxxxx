UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108515);

DELETE FROM `creature_queststarter` WHERE `quest`=42126;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108515, 42126);

DELETE FROM `creature_questender` WHERE `quest`=42126;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108515, 42126);

DELETE FROM `quest_template_addon` WHERE `ID`=42126;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42126, 42703, 42127);
