UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107351);

DELETE FROM `creature_queststarter` WHERE `quest`=42734;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107351, 42734);

DELETE FROM `creature_questender` WHERE `quest`=42734;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107351, 42734);

DELETE FROM `quest_template_addon` WHERE `ID`=42734;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42734, 42707);
