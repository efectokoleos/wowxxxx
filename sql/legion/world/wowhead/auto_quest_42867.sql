UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=42867;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 42867);

DELETE FROM `creature_questender` WHERE `quest`=42867;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90259, 42867);

DELETE FROM `quest_template_addon` WHERE `ID`=42867;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42867, 42919);
