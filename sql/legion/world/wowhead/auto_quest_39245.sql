UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94789);

DELETE FROM `creature_queststarter` WHERE `quest`=39245;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94789, 39245);

DELETE FROM `creature_questender` WHERE `quest`=39245;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94789, 39245);

DELETE FROM `quest_template_addon` WHERE `ID`=39245;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39245, 39246);
