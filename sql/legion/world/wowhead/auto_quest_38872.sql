UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93624);

DELETE FROM `creature_queststarter` WHERE `quest`=38872;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93624, 38872);

DELETE FROM `creature_questender` WHERE `quest`=38872;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93603, 38872);

DELETE FROM `quest_template_addon` WHERE `ID`=38872;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38872, 38873);
