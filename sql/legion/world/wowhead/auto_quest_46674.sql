UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101195);

DELETE FROM `creature_queststarter` WHERE `quest`=46674;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101195, 46674);

DELETE FROM `creature_questender` WHERE `quest`=46674;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120070, 46674);

DELETE FROM `quest_template_addon` WHERE `ID`=46674;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46674, 45426);
