UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109384);

DELETE FROM `creature_queststarter` WHERE `quest`=42888;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109384, 42888);

DELETE FROM `creature_questender` WHERE `quest`=42888;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109434, 42888);

DELETE FROM `quest_template_addon` WHERE `ID`=42888;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42888, 42886, 42890);
