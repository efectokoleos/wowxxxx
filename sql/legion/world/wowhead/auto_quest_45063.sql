UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=45063;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 45063);

DELETE FROM `creature_questender` WHERE `quest`=45063;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 45063);

DELETE FROM `quest_template_addon` WHERE `ID`=45063;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45063, 45062);
