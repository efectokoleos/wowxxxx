UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93437);

DELETE FROM `creature_queststarter` WHERE `quest`=42449;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93437, 42449);

DELETE FROM `creature_questender` WHERE `quest`=42449;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107554, 42449);

DELETE FROM `quest_template_addon` WHERE `ID`=42449;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42449, 43539);
