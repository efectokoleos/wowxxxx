UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90309);

DELETE FROM `creature_queststarter` WHERE `quest`=38581;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90309, 38581);

DELETE FROM `creature_questender` WHERE `quest`=38581;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91935, 38581);

DELETE FROM `quest_template_addon` WHERE `ID`=38581;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38581, 38446);
