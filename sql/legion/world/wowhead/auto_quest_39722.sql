UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91866);

DELETE FROM `creature_queststarter` WHERE `quest`=39722;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91866, 39722);

DELETE FROM `creature_questender` WHERE `quest`=39722;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91866, 39722);

DELETE FROM `quest_template_addon` WHERE `ID`=39722;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39722, 38566, 38933);
