UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117249);

DELETE FROM `creature_queststarter` WHERE `quest`=45726;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117249, 45726);

DELETE FROM `creature_questender` WHERE `quest`=45726;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117249, 45726);

DELETE FROM `quest_template_addon` WHERE `ID`=45726;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45726, 45564, 45575);
