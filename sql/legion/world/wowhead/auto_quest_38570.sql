UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89937);

DELETE FROM `creature_queststarter` WHERE `quest`=38570;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89937, 38570);

DELETE FROM `creature_questender` WHERE `quest`=38570;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92401, 38570);

DELETE FROM `quest_template_addon` WHERE `ID`=38570;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38570, 38568, 38571);
