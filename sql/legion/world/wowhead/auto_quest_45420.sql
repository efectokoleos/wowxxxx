UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106522);

DELETE FROM `creature_queststarter` WHERE `quest`=45420;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106522, 45420);

DELETE FROM `creature_questender` WHERE `quest`=45420;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 45420);

DELETE FROM `quest_template_addon` WHERE `ID`=45420;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45420, 45417);
