UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91481);

DELETE FROM `creature_queststarter` WHERE `quest`=38405;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91481, 38405);

DELETE FROM `creature_questender` WHERE `quest`=38405;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91519, 38405);

DELETE FROM `quest_template_addon` WHERE `ID`=38405;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38405, 38312, 38410);
