UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101499);

DELETE FROM `creature_queststarter` WHERE `quest`=40747;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101499, 40747);

DELETE FROM `creature_questender` WHERE `quest`=40747;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101499, 40747);

DELETE FROM `quest_template_addon` WHERE `ID`=40747;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40747, 40011, 40748);
