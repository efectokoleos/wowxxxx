UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90481);

DELETE FROM `creature_queststarter` WHERE `quest`=38415;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90481, 38415);

DELETE FROM `creature_questender` WHERE `quest`=38415;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91750, 38415);

DELETE FROM `quest_template_addon` WHERE `ID`=38415;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38415, 38416);
