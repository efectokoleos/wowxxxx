UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98632);

DELETE FROM `creature_queststarter` WHERE `quest`=42682;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98632, 42682);

DELETE FROM `creature_questender` WHERE `quest`=42682;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103761, 42682);

DELETE FROM `quest_template_addon` WHERE `ID`=42682;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42682, 42683, 37447);
