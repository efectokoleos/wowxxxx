UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116321);

DELETE FROM `creature_queststarter` WHERE `quest`=44770;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116321, 44770);

DELETE FROM `creature_questender` WHERE `quest`=44770;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116321, 44770);

DELETE FROM `quest_template_addon` WHERE `ID`=44770;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44770, 46351);
