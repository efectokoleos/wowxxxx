UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95921);

DELETE FROM `creature_queststarter` WHERE `quest`=39092;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95921, 39092);

DELETE FROM `creature_questender` WHERE `quest`=39092;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95804, 39092);

DELETE FROM `quest_template_addon` WHERE `ID`=39092;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39092, 39063, 39122);
