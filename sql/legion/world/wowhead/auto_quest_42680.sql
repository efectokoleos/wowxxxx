UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98102);

DELETE FROM `creature_queststarter` WHERE `quest`=42680;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98102, 42680);

DELETE FROM `creature_questender` WHERE `quest`=42680;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98100, 42680);

DELETE FROM `quest_template_addon` WHERE `ID`=42680;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42680, 42678);
