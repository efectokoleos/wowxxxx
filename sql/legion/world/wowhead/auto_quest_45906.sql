UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90418);

DELETE FROM `creature_queststarter` WHERE `quest`=45906;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90418, 45906);

DELETE FROM `creature_questender` WHERE `quest`=45906;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 45906);

DELETE FROM `quest_template_addon` WHERE `ID`=45906;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45906, 46035);
