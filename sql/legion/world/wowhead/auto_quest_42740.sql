UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108916);

DELETE FROM `creature_queststarter` WHERE `quest`=42740;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108916, 42740);

DELETE FROM `creature_questender` WHERE `quest`=42740;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100395, 42740);

DELETE FROM `quest_template_addon` WHERE `ID`=42740;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42740, 42782, 40517);
