UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108304);

DELETE FROM `creature_queststarter` WHERE `quest`=42751;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108304, 42751);

DELETE FROM `creature_questender` WHERE `quest`=42751;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108887, 42751);

DELETE FROM `quest_template_addon` WHERE `ID`=42751;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42751, 42747);
