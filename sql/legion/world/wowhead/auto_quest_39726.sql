UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96763);

DELETE FROM `creature_queststarter` WHERE `quest`=39726;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96763, 39726);

DELETE FROM `creature_questender` WHERE `quest`=39726;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96763, 39726);

DELETE FROM `quest_template_addon` WHERE `ID`=39726;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39726, 38518);
