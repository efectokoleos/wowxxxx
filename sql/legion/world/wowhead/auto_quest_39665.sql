UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93812);

DELETE FROM `creature_queststarter` WHERE `quest`=39665;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93812, 39665);

DELETE FROM `creature_questender` WHERE `quest`=39665;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93822, 39665);

UPDATE `quest_template_addon` SET `PrevQuestID`=39666 WHERE `ID`=39665;
