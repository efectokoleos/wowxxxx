UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100521);

DELETE FROM `creature_queststarter` WHERE `quest`=40547;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100521, 40547);

DELETE FROM `creature_questender` WHERE `quest`=40547;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100500, 40547);

DELETE FROM `quest_template_addon` WHERE `ID`=40547;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40547, 40556);
