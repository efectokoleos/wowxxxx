UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90866);

DELETE FROM `creature_queststarter` WHERE `quest`=38053;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90866, 38053);

DELETE FROM `creature_questender` WHERE `quest`=38053;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90866, 38053);

DELETE FROM `quest_template_addon` WHERE `ID`=38053;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38053, 38058);
