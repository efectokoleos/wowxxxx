UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110018);

DELETE FROM `creature_queststarter` WHERE `quest`=43331;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110018, 43331);

DELETE FROM `gameobject_questender` WHERE `quest`=43331;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(251870, 43331);
