UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92195);

DELETE FROM `creature_queststarter` WHERE `quest`=40056;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92195, 40056);

DELETE FROM `creature_questender` WHERE `quest`=40056;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89398, 40056);

DELETE FROM `quest_template_addon` WHERE `ID`=40056;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40056, 40057);
