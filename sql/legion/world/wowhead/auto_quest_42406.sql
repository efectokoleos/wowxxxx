UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105816);

DELETE FROM `creature_queststarter` WHERE `quest`=42406;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105816, 42406);

DELETE FROM `creature_questender` WHERE `quest`=42406;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105816, 42406);

DELETE FROM `quest_template_addon` WHERE `ID`=42406;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42406, 42691);
