UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93231);

DELETE FROM `creature_queststarter` WHERE `quest`=39788;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93231, 39788);

DELETE FROM `creature_questender` WHERE `quest`=39788;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93231, 39788);

DELETE FROM `quest_template_addon` WHERE `ID`=39788;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39788, 39796, 38808);
