UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104824);

DELETE FROM `creature_queststarter` WHERE `quest`=41784;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104824, 41784);

DELETE FROM `creature_questender` WHERE `quest`=41784;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105102, 41784);

DELETE FROM `quest_template_addon` WHERE `ID`=41784;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41784, 41780);
