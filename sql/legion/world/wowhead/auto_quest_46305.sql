UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116737);

DELETE FROM `creature_queststarter` WHERE `quest`=46305;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116737, 46305);

DELETE FROM `creature_questender` WHERE `quest`=46305;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116737, 46305);

DELETE FROM `quest_template_addon` WHERE `ID`=46305;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46305, 44787);
