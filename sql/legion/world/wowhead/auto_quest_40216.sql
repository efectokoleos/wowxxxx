UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94409);

DELETE FROM `creature_queststarter` WHERE `quest`=40216;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94409, 40216);

DELETE FROM `creature_questender` WHERE `quest`=40216;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94409, 40216);

DELETE FROM `quest_template_addon` WHERE `ID`=40216;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40216, 39859);
