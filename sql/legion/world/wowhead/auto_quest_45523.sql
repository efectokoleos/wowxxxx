UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116568);

DELETE FROM `creature_queststarter` WHERE `quest`=45523;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116568, 45523);

DELETE FROM `creature_questender` WHERE `quest`=45523;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117227, 45523);
