UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107598);

DELETE FROM `creature_queststarter` WHERE `quest`=44052;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107598, 44052);

DELETE FROM `creature_questender` WHERE `quest`=44052;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97140, 44052);
