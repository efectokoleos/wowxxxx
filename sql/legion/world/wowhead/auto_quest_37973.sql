UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89793);

DELETE FROM `creature_queststarter` WHERE `quest`=37973;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89793, 37973);

DELETE FROM `quest_template_addon` WHERE `ID`=37973;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37973, 37976);
