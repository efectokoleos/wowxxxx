UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=38649;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 38649);

DELETE FROM `creature_questender` WHERE `quest`=38649;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115710, 38649);

DELETE FROM `quest_template_addon` WHERE `ID`=38649;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38649, 45260, 38695);
