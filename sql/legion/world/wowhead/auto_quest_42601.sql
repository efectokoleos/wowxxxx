UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106199);

DELETE FROM `creature_queststarter` WHERE `quest`=42601;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106199, 42601);

DELETE FROM `creature_questender` WHERE `quest`=42601;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106199, 42601);

DELETE FROM `quest_template_addon` WHERE `ID`=42601;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42601, 42602);
