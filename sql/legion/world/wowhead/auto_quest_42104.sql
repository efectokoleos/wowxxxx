UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97662);

DELETE FROM `creature_queststarter` WHERE `quest`=42104;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97662, 42104);

DELETE FROM `creature_questender` WHERE `quest`=42104;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93805, 42104);

DELETE FROM `quest_template_addon` WHERE `ID`=42104;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42104, 39498);
