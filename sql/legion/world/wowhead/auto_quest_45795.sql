UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118942);

DELETE FROM `creature_queststarter` WHERE `quest`=45795;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118942, 45795);

DELETE FROM `creature_questender` WHERE `quest`=45795;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117774, 45795);

DELETE FROM `quest_template_addon` WHERE `ID`=45795;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45795, 45838, 46205);
