UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=45482;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 45482);

DELETE FROM `creature_questender` WHERE `quest`=45482;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116568, 45482);

DELETE FROM `quest_template_addon` WHERE `ID`=45482;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45482, 47033);
