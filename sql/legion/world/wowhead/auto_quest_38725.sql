UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96655);

DELETE FROM `creature_queststarter` WHERE `quest`=38725;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96655, 38725);

DELETE FROM `creature_questender` WHERE `quest`=38725;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98711, 38725);

DELETE FROM `quest_template_addon` WHERE `ID`=38725;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38725, 40222);
