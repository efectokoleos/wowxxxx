UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105951);

DELETE FROM `creature_queststarter` WHERE `quest`=41796;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105951, 41796);

DELETE FROM `creature_questender` WHERE `quest`=41796;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105951, 41796);

DELETE FROM `quest_template_addon` WHERE `ID`=41796;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41796, 41795);
