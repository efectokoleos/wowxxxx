UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92195);

DELETE FROM `creature_queststarter` WHERE `quest`=39955;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92195, 39955);

DELETE FROM `creature_questender` WHERE `quest`=39955;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97762, 39955);

DELETE FROM `quest_template_addon` WHERE `ID`=39955;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39955, 39954, 39956);
