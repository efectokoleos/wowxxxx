UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108389);

DELETE FROM `creature_queststarter` WHERE `quest`=41143;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108389, 41143);

DELETE FROM `creature_questender` WHERE `quest`=41143;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108390, 41143);

DELETE FROM `quest_template_addon` WHERE `ID`=41143;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41143, 42728);
