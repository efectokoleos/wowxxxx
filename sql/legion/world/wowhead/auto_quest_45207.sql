UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118242);

DELETE FROM `creature_queststarter` WHERE `quest`=45207;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118242, 45207);

DELETE FROM `creature_questender` WHERE `quest`=45207;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(121357, 45207);

DELETE FROM `quest_template_addon` WHERE `ID`=45207;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45207, 46338, 46705);
