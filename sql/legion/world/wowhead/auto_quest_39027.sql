UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97553);

DELETE FROM `creature_queststarter` WHERE `quest`=39027;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97553, 39027);

DELETE FROM `creature_questender` WHERE `quest`=39027;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93805, 39027);

DELETE FROM `quest_template_addon` WHERE `ID`=39027;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39027, 39043, 38909);
