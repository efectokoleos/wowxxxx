UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93538);

DELETE FROM `creature_queststarter` WHERE `quest`=45238;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93538, 45238);

DELETE FROM `creature_questender` WHERE `quest`=45238;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93538, 45238);

DELETE FROM `quest_template_addon` WHERE `ID`=45238;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45238, 45160, 45239);
