UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93538);

DELETE FROM `creature_queststarter` WHERE `quest`=41186;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93538, 41186);

DELETE FROM `creature_questender` WHERE `quest`=41186;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103485, 41186);

DELETE FROM `quest_template_addon` WHERE `ID`=41186;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41186, 41187);
