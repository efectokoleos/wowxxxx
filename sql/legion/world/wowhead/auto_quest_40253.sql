UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92718);

DELETE FROM `creature_queststarter` WHERE `quest`=40253;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92718, 40253);

DELETE FROM `creature_questender` WHERE `quest`=40253;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96665, 40253);

DELETE FROM `quest_template_addon` WHERE `ID`=40253;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40253, 39682);
