UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93568);

DELETE FROM `creature_queststarter` WHERE `quest`=43539;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93568, 43539);

DELETE FROM `creature_questender` WHERE `quest`=43539;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93568, 43539);

DELETE FROM `quest_template_addon` WHERE `ID`=43539;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43539, 42449);
