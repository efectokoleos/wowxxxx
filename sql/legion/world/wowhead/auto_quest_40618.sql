UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102478);

DELETE FROM `creature_queststarter` WHERE `quest`=40618;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102478, 40618);

DELETE FROM `creature_questender` WHERE `quest`=40618;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102478, 40618);

DELETE FROM `quest_template_addon` WHERE `ID`=40618;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40618, 40384);
