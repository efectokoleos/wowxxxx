UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116868);

DELETE FROM `creature_queststarter` WHERE `quest`=45406;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116868, 45406);

DELETE FROM `creature_questender` WHERE `quest`=45406;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118778, 45406);

DELETE FROM `quest_template_addon` WHERE `ID`=45406;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45406, 45839);
