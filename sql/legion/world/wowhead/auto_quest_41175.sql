UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103482);

DELETE FROM `creature_queststarter` WHERE `quest`=41175;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103482, 41175);

DELETE FROM `creature_questender` WHERE `quest`=41175;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103482, 41175);

DELETE FROM `quest_template_addon` WHERE `ID`=41175;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41175, 41174, 41176);
