UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101064);

DELETE FROM `creature_queststarter` WHERE `quest`=40644;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101064, 40644);

DELETE FROM `creature_questender` WHERE `quest`=40644;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103875, 40644);

DELETE FROM `quest_template_addon` WHERE `ID`=40644;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40644, 41106, 40645);
