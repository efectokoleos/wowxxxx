UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93189);

DELETE FROM `creature_queststarter` WHERE `quest`=38888;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93189, 38888);

DELETE FROM `creature_questender` WHERE `quest`=38888;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93691, 38888);

DELETE FROM `quest_template_addon` WHERE `ID`=38888;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38888, 38786);
