UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103485);

DELETE FROM `creature_queststarter` WHERE `quest`=41190;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103485, 41190);

DELETE FROM `creature_questender` WHERE `quest`=41190;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103485, 41190);

DELETE FROM `quest_template_addon` WHERE `ID`=41190;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41190, 41189, 41191);
