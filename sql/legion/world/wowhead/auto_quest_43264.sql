UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93437);

DELETE FROM `creature_queststarter` WHERE `quest`=43264;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93437, 43264);

DELETE FROM `creature_questender` WHERE `quest`=43264;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93568, 43264);

DELETE FROM `quest_template_addon` WHERE `ID`=43264;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43264, 43265);
