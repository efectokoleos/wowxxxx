UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93555);

DELETE FROM `creature_queststarter` WHERE `quest`=46720;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93555, 46720);

DELETE FROM `quest_template_addon` WHERE `ID`=46720;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46720, 46719, 46812);
