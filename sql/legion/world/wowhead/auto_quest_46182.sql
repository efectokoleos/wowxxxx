UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119676);

DELETE FROM `creature_queststarter` WHERE `quest`=46182;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119676, 46182);

DELETE FROM `creature_questender` WHERE `quest`=46182;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119944, 46182);

DELETE FROM `quest_template_addon` WHERE `ID`=46182;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46182, 45572);
