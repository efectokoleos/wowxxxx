UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102198);

DELETE FROM `creature_queststarter` WHERE `quest`=40867;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102198, 40867);

DELETE FROM `creature_questender` WHERE `quest`=40867;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102198, 40867);

DELETE FROM `quest_template_addon` WHERE `ID`=40867;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40867, 40868);
