UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99575);

DELETE FROM `creature_queststarter` WHERE `quest`=40321;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99575, 40321);

DELETE FROM `creature_questender` WHERE `quest`=40321;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100779, 40321);
