UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91290);

DELETE FROM `creature_queststarter` WHERE `quest`=37687;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91290, 37687);

DELETE FROM `creature_questender` WHERE `quest`=37687;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91290, 37687);

UPDATE `quest_template_addon` SET `NextQuestID`=38267 WHERE `ID`=37687;
