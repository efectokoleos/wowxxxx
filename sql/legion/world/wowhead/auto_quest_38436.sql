UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91913);

DELETE FROM `creature_queststarter` WHERE `quest`=38436;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91913, 38436);

DELETE FROM `creature_questender` WHERE `quest`=38436;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91923, 38436);

DELETE FROM `quest_template_addon` WHERE `ID`=38436;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38436, 38435, 38444);
