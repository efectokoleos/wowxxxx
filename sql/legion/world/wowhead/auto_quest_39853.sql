UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97480);

DELETE FROM `creature_queststarter` WHERE `quest`=39853;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97480, 39853);

DELETE FROM `creature_questender` WHERE `quest`=39853;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97558, 39853);

DELETE FROM `quest_template_addon` WHERE `ID`=39853;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39853, 39850, 39855);
