UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91066);

DELETE FROM `creature_queststarter` WHERE `quest`=38322;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91066, 38322);

DELETE FROM `creature_questender` WHERE `quest`=38322;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91109, 38322);

DELETE FROM `quest_template_addon` WHERE `ID`=38322;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38322, 40573, 38377);
