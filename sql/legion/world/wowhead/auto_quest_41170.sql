UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103484);

DELETE FROM `creature_queststarter` WHERE `quest`=41170;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103484, 41170);

DELETE FROM `creature_questender` WHERE `quest`=41170;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103484, 41170);

DELETE FROM `quest_template_addon` WHERE `ID`=41170;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41170, 41157);
