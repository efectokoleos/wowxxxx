UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101097);

DELETE FROM `creature_queststarter` WHERE `quest`=43100;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101097, 43100);

DELETE FROM `creature_questender` WHERE `quest`=43100;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109698, 43100);

DELETE FROM `quest_template_addon` WHERE `ID`=43100;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43100, 43153);
