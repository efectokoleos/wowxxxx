UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97072);

DELETE FROM `creature_queststarter` WHERE `quest`=39761;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97072, 39761);

DELETE FROM `creature_questender` WHERE `quest`=39761;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97072, 39761);

DELETE FROM `quest_template_addon` WHERE `ID`=39761;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39761, 39757, 39832);
