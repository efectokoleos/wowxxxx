UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98100);

DELETE FROM `creature_queststarter` WHERE `quest`=43479;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98100, 43479);

DELETE FROM `creature_questender` WHERE `quest`=43479;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98100, 43479);

DELETE FROM `quest_template_addon` WHERE `ID`=43479;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43479, 43470, 43485);
