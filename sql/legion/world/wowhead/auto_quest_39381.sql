UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96286);

DELETE FROM `creature_queststarter` WHERE `quest`=39381;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96286, 39381);

DELETE FROM `creature_questender` WHERE `quest`=39381;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95421, 39381);

DELETE FROM `quest_template_addon` WHERE `ID`=39381;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39381, 39860, 39391);
