UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90418);

DELETE FROM `creature_queststarter` WHERE `quest`=45909;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90418, 45909);

DELETE FROM `creature_questender` WHERE `quest`=45909;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 45909);

DELETE FROM `quest_template_addon` WHERE `ID`=45909;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45909, 45627);
