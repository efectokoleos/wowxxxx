UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96270);

DELETE FROM `creature_queststarter` WHERE `quest`=39579;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96270, 39579);

DELETE FROM `creature_questender` WHERE `quest`=39579;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108305, 39579);

DELETE FROM `quest_template_addon` WHERE `ID`=39579;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39579, 39577, 39580);
