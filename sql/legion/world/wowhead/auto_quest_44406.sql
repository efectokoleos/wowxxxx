UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96528);

DELETE FROM `creature_queststarter` WHERE `quest`=44406;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96528, 44406);

DELETE FROM `creature_questender` WHERE `quest`=44406;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99531, 44406);

DELETE FROM `quest_template_addon` WHERE `ID`=44406;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44406, 43945);
