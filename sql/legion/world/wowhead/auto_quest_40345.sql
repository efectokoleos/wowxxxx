UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97130);

DELETE FROM `creature_queststarter` WHERE `quest`=40345;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97130, 40345);

DELETE FROM `creature_questender` WHERE `quest`=40345;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97130, 40345);

DELETE FROM `quest_template_addon` WHERE `ID`=40345;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40345, 40339, 39772);
