UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98646);

DELETE FROM `creature_queststarter` WHERE `quest`=42754;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98646, 42754);

DELETE FROM `creature_questender` WHERE `quest`=42754;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98646, 42754);

DELETE FROM `quest_template_addon` WHERE `ID`=42754;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42754, 42732, 42810);
