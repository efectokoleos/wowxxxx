UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117263);

DELETE FROM `creature_queststarter` WHERE `quest`=45629;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117263, 45629);

DELETE FROM `creature_questender` WHERE `quest`=45629;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117259, 45629);

DELETE FROM `quest_template_addon` WHERE `ID`=45629;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45629, 46260);
