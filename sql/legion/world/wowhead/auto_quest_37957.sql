UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90065);

DELETE FROM `creature_queststarter` WHERE `quest`=37957;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90065, 37957);

DELETE FROM `creature_questender` WHERE `quest`=37957;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89978, 37957);

DELETE FROM `quest_template_addon` WHERE `ID`=37957;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37957, 37858);
