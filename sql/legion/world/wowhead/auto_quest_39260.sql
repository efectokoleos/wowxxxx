UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89763);

DELETE FROM `creature_queststarter` WHERE `quest`=39260;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89763, 39260);

DELETE FROM `creature_questender` WHERE `quest`=39260;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(81492, 39260);
