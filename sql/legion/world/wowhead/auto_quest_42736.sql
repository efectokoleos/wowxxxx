UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108576);

DELETE FROM `creature_queststarter` WHERE `quest`=42736;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108576, 42736);

DELETE FROM `creature_questender` WHERE `quest`=42736;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108576, 42736);

DELETE FROM `quest_template_addon` WHERE `ID`=42736;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42736, 42787, 42749);
