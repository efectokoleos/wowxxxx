UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116193);

DELETE FROM `creature_queststarter` WHERE `quest`=45834;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116193, 45834);

DELETE FROM `creature_questender` WHERE `quest`=45834;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116193, 45834);

DELETE FROM `quest_template_addon` WHERE `ID`=45834;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45834, 44849);
