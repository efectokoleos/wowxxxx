UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91291);

DELETE FROM `creature_queststarter` WHERE `quest`=38213;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91291, 38213);

DELETE FROM `gameobject_questender` WHERE `quest`=38213;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(240317, 38213);
