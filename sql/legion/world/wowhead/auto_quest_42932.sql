UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96527);

DELETE FROM `creature_queststarter` WHERE `quest`=42932;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96527, 42932);

DELETE FROM `creature_questender` WHERE `quest`=42932;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(42465, 42932);

DELETE FROM `quest_template_addon` WHERE `ID`=42932;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42932, 42931, 42933);
