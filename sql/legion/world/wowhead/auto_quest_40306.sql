UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99483);

DELETE FROM `creature_queststarter` WHERE `quest`=40306;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99483, 40306);

DELETE FROM `creature_questender` WHERE `quest`=40306;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99575, 40306);
