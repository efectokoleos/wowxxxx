UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89793);

DELETE FROM `creature_queststarter` WHERE `quest`=37941;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89793, 37941);

DELETE FROM `quest_template_addon` WHERE `ID`=37941;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37941, 37942);
