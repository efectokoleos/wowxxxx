UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93826);

DELETE FROM `creature_queststarter` WHERE `quest`=38911;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93826, 38911);

DELETE FROM `creature_questender` WHERE `quest`=38911;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93833, 38911);

DELETE FROM `quest_template_addon` WHERE `ID`=38911;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38911, 38907, 39491);
