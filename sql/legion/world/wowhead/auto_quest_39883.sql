UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98017);

DELETE FROM `creature_queststarter` WHERE `quest`=39883;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98017, 39883);

DELETE FROM `creature_questender` WHERE `quest`=39883;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93531, 39883);

DELETE FROM `quest_template_addon` WHERE `ID`=39883;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39883, 39879, 39884);
