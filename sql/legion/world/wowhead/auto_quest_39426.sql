UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95421);

DELETE FROM `creature_queststarter` WHERE `quest`=39426;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95421, 39426);

DELETE FROM `creature_questender` WHERE `quest`=39426;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96453, 39426);

DELETE FROM `quest_template_addon` WHERE `ID`=39426;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39426, 39391, 40229);
