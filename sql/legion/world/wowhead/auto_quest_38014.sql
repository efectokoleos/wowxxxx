UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102988);

DELETE FROM `creature_queststarter` WHERE `quest`=38014;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102988, 38014);

DELETE FROM `creature_questender` WHERE `quest`=38014;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102988, 38014);

DELETE FROM `quest_template_addon` WHERE `ID`=38014;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38014, 37861, 38015);
