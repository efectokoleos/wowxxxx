UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116189);

DELETE FROM `creature_queststarter` WHERE `quest`=45127;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116189, 45127);

DELETE FROM `creature_questender` WHERE `quest`=45127;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 45127);

DELETE FROM `quest_template_addon` WHERE `ID`=45127;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45127, 45861);
