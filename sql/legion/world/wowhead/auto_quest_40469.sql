UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100192);

DELETE FROM `creature_queststarter` WHERE `quest`=40469;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100192, 40469);

DELETE FROM `creature_questender` WHERE `quest`=40469;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100185, 40469);

DELETE FROM `quest_template_addon` WHERE `ID`=40469;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40469, 40401, 40424);
