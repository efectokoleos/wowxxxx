UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117573);

DELETE FROM `creature_queststarter` WHERE `quest`=46719;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117573, 46719);

DELETE FROM `creature_questender` WHERE `quest`=46719;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93555, 46719);

DELETE FROM `quest_template_addon` WHERE `ID`=46719;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46719, 46720);
