UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98825);

DELETE FROM `creature_queststarter` WHERE `quest`=40520;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98825, 40520);

DELETE FROM `creature_questender` WHERE `quest`=40520;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98825, 40520);

DELETE FROM `quest_template_addon` WHERE `ID`=40520;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40520, 40167, 39983);
