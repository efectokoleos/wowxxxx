UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91166);

DELETE FROM `creature_queststarter` WHERE `quest`=38232;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91166, 38232);

DELETE FROM `creature_questender` WHERE `quest`=38232;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91165, 38232);

DELETE FROM `quest_template_addon` WHERE `ID`=38232;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38232, 38460, 38237);
