UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(112679);

DELETE FROM `creature_queststarter` WHERE `quest`=43007;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(112679, 43007);

DELETE FROM `creature_questender` WHERE `quest`=43007;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101513, 43007);

DELETE FROM `quest_template_addon` WHERE `ID`=43007;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43007, 42139);
