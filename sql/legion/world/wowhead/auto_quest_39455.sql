UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95256);

DELETE FROM `creature_queststarter` WHERE `quest`=39455;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95256, 39455);

DELETE FROM `creature_questender` WHERE `quest`=39455;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93836, 39455);

DELETE FROM `quest_template_addon` WHERE `ID`=39455;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39455, 39374, 39860);
