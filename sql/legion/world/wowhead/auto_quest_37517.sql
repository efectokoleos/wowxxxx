UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(76508);

DELETE FROM `creature_queststarter` WHERE `quest`=37517;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(76508, 37517);

DELETE FROM `creature_questender` WHERE `quest`=37517;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88892, 37517);

DELETE FROM `quest_template_addon` WHERE `ID`=37517;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37517, 35973);
