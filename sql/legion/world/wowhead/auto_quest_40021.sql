UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98135);

DELETE FROM `creature_queststarter` WHERE `quest`=40021;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98135, 40021);

DELETE FROM `creature_questender` WHERE `quest`=40021;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98135, 40021);

DELETE FROM `quest_template_addon` WHERE `ID`=40021;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40021, 40020);
