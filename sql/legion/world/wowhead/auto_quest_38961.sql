UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93967);

DELETE FROM `creature_queststarter` WHERE `quest`=38961;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93967, 38961);

DELETE FROM `creature_questender` WHERE `quest`=38961;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93542, 38961);

DELETE FROM `quest_template_addon` WHERE `ID`=38961;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38961, 38963, 38964);
