UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108072);

DELETE FROM `creature_queststarter` WHERE `quest`=42635;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108072, 42635);

DELETE FROM `creature_questender` WHERE `quest`=42635;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108072, 42635);

DELETE FROM `quest_template_addon` WHERE `ID`=42635;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42635, 42641);
