UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95392);

DELETE FROM `creature_queststarter` WHERE `quest`=39456;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95392, 39456);

DELETE FROM `creature_questender` WHERE `quest`=39456;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93826, 39456);

DELETE FROM `quest_template_addon` WHERE `ID`=39456;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39456, 40229);
