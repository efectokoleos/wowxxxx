UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99531);

DELETE FROM `creature_queststarter` WHERE `quest`=43945;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99531, 43945);

DELETE FROM `creature_questender` WHERE `quest`=43945;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99531, 43945);

DELETE FROM `quest_template_addon` WHERE `ID`=43945;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43945, 44406);
