UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120221);

DELETE FROM `creature_queststarter` WHERE `quest`=46693;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120221, 46693);

DELETE FROM `creature_questender` WHERE `quest`=46693;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120221, 46693);

DELETE FROM `quest_template_addon` WHERE `ID`=46693;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46693, 46695);
