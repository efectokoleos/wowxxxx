UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93841);

DELETE FROM `creature_queststarter` WHERE `quest`=39318;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93841, 39318);

DELETE FROM `creature_questender` WHERE `quest`=39318;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93841, 39318);

DELETE FROM `quest_template_addon` WHERE `ID`=39318;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39318, 38913, 38910);
