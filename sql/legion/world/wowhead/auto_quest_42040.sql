UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103832);

DELETE FROM `creature_queststarter` WHERE `quest`=42040;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103832, 42040);

DELETE FROM `creature_questender` WHERE `quest`=42040;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106204, 42040);
