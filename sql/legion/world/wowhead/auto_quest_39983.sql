UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98825);

DELETE FROM `creature_queststarter` WHERE `quest`=39983;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98825, 39983);

DELETE FROM `creature_questender` WHERE `quest`=39983;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97891, 39983);

DELETE FROM `quest_template_addon` WHERE `ID`=39983;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39983, 40520, 40112);
