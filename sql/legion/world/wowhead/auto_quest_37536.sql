UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88923);

DELETE FROM `creature_queststarter` WHERE `quest`=37536;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88923, 37536);

DELETE FROM `creature_questender` WHERE `quest`=37536;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89048, 37536);

DELETE FROM `quest_template_addon` WHERE `ID`=37536;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37536, 37510, 37538);
