UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92458);

DELETE FROM `creature_queststarter` WHERE `quest`=39345;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92458, 39345);

DELETE FROM `creature_questender` WHERE `quest`=39345;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92458, 39345);

DELETE FROM `quest_template_addon` WHERE `ID`=39345;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39345, 39346);
