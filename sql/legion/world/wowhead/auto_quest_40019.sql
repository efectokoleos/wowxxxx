UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92464);

DELETE FROM `creature_queststarter` WHERE `quest`=40019;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92464, 40019);

DELETE FROM `creature_questender` WHERE `quest`=40019;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92464, 40019);

DELETE FROM `quest_template_addon` WHERE `ID`=40019;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40019, 40018);
