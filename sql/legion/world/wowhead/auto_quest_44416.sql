UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113355);

DELETE FROM `creature_queststarter` WHERE `quest`=44416;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113355, 44416);

DELETE FROM `creature_questender` WHERE `quest`=44416;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113355, 44416);

DELETE FROM `quest_template_addon` WHERE `ID`=44416;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44416, 44415);
