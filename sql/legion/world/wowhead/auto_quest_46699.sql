UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120221);

DELETE FROM `creature_queststarter` WHERE `quest`=46699;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120221, 46699);

DELETE FROM `creature_questender` WHERE `quest`=46699;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120221, 46699);

DELETE FROM `quest_template_addon` WHERE `ID`=46699;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46699, 46701);
