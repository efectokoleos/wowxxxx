UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107317);

DELETE FROM `creature_queststarter` WHERE `quest`=42390;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107317, 42390);

DELETE FROM `creature_questender` WHERE `quest`=42390;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107425, 42390);

DELETE FROM `quest_template_addon` WHERE `ID`=42390;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42390, 42392);
