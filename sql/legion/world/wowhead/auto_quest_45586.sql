UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117331);

DELETE FROM `creature_queststarter` WHERE `quest`=45586;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117331, 45586);

DELETE FROM `creature_questender` WHERE `quest`=45586;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119001, 45586);

DELETE FROM `quest_template_addon` WHERE `ID`=45586;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45586, 46000);
