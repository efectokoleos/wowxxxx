UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90463);

DELETE FROM `creature_queststarter` WHERE `quest`=45910;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90463, 45910);

DELETE FROM `creature_questender` WHERE `quest`=45910;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90463, 45910);

DELETE FROM `quest_template_addon` WHERE `ID`=45910;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45910, 46127);
