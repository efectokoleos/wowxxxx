UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(83869);

DELETE FROM `creature_queststarter` WHERE `quest`=38924;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(83869, 38924);

DELETE FROM `creature_questender` WHERE `quest`=38924;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(83869, 38924);

DELETE FROM `quest_template_addon` WHERE `ID`=38924;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38924, 39096);
