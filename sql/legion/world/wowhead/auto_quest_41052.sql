UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93775);

DELETE FROM `creature_queststarter` WHERE `quest`=41052;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93775, 41052);

DELETE FROM `creature_questender` WHERE `quest`=41052;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93773, 41052);

DELETE FROM `quest_template_addon` WHERE `ID`=41052;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41052, 38904);
