UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(18255);

DELETE FROM `creature_queststarter` WHERE `quest`=45295;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(18255, 45295);

DELETE FROM `creature_questender` WHERE `quest`=45295;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(18255, 45295);

DELETE FROM `quest_template_addon` WHERE `ID`=45295;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45295, 45294);
