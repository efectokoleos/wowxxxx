UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89978);

DELETE FROM `creature_queststarter` WHERE `quest`=37861;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89978, 37861);

DELETE FROM `creature_questender` WHERE `quest`=37861;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107995, 37861);

DELETE FROM `quest_template_addon` WHERE `ID`=37861;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37861, 37960, 37862);
