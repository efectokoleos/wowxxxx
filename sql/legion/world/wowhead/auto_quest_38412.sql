UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91558);

DELETE FROM `creature_queststarter` WHERE `quest`=38412;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91558, 38412);

DELETE FROM `creature_questender` WHERE `quest`=38412;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91249, 38412);

DELETE FROM `quest_template_addon` WHERE `ID`=38412;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38412, 38342, 38413);
