UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93846);

DELETE FROM `creature_queststarter` WHERE `quest`=42512;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93846, 42512);

DELETE FROM `creature_questender` WHERE `quest`=42512;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97407, 42512);

DELETE FROM `quest_template_addon` WHERE `ID`=42512;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42512, 42088, 39043);
