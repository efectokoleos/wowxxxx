UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(121237);

DELETE FROM `creature_queststarter` WHERE `quest`=46282;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(121237, 46282);

DELETE FROM `creature_questender` WHERE `quest`=46282;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120288, 46282);

DELETE FROM `quest_template_addon` WHERE `ID`=46282;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46282, 46275);
