UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119765);

DELETE FROM `creature_queststarter` WHERE `quest`=45459;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119765, 45459);

DELETE FROM `creature_questender` WHERE `quest`=45459;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117305, 45459);

DELETE FROM `quest_template_addon` WHERE `ID`=45459;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45459, 45404, 45574);
