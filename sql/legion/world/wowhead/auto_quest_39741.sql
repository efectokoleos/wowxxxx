UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108571);

DELETE FROM `creature_queststarter` WHERE `quest`=39741;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108571, 39741);

DELETE FROM `creature_questender` WHERE `quest`=39741;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98632, 39741);

DELETE FROM `quest_template_addon` WHERE `ID`=39741;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39741, 42634);
