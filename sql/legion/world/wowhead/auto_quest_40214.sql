UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98969);

DELETE FROM `creature_queststarter` WHERE `quest`=40214;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98969, 40214);

DELETE FROM `creature_questender` WHERE `quest`=40214;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98969, 40214);

DELETE FROM `quest_template_addon` WHERE `ID`=40214;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40214, 40212);
