UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92183);

DELETE FROM `creature_queststarter` WHERE `quest`=46083;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92183, 46083);

DELETE FROM `creature_questender` WHERE `quest`=46083;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 46083);

DELETE FROM `quest_template_addon` WHERE `ID`=46083;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46083, 46071, 46074);
