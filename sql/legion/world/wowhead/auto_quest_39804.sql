UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92539);

DELETE FROM `creature_queststarter` WHERE `quest`=39804;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92539, 39804);

DELETE FROM `creature_questender` WHERE `quest`=39804;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92539, 39804);

DELETE FROM `quest_template_addon` WHERE `ID`=39804;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39804, 39803, 39796);
