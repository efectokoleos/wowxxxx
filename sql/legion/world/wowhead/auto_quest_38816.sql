UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93446);

DELETE FROM `creature_queststarter` WHERE `quest`=38816;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93446, 38816);

DELETE FROM `creature_questender` WHERE `quest`=38816;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97270, 38816);

DELETE FROM `quest_template_addon` WHERE `ID`=38816;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38816, 38811, 38815);
