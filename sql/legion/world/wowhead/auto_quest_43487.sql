UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110571);

DELETE FROM `creature_queststarter` WHERE `quest`=43487;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110571, 43487);

DELETE FROM `creature_questender` WHERE `quest`=43487;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110571, 43487);

DELETE FROM `quest_template_addon` WHERE `ID`=43487;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43487, 43486);
