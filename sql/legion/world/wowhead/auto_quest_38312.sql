UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91222);

DELETE FROM `creature_queststarter` WHERE `quest`=38312;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91222, 38312);

DELETE FROM `creature_questender` WHERE `quest`=38312;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91481, 38312);

DELETE FROM `quest_template_addon` WHERE `ID`=38312;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38312, 38473, 38318);
