UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103023);

DELETE FROM `creature_queststarter` WHERE `quest`=42384;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103023, 42384);

DELETE FROM `creature_questender` WHERE `quest`=42384;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107317, 42384);

DELETE FROM `quest_template_addon` WHERE `ID`=42384;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42384, 42526);
