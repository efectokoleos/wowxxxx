UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97558);

DELETE FROM `creature_queststarter` WHERE `quest`=39855;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97558, 39855);

DELETE FROM `creature_questender` WHERE `quest`=39855;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92539, 39855);

DELETE FROM `quest_template_addon` WHERE `ID`=39855;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39855, 39853);
