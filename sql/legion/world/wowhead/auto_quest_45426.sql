UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115750);

DELETE FROM `creature_queststarter` WHERE `quest`=45426;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115750, 45426);

DELETE FROM `creature_questender` WHERE `quest`=45426;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101195, 45426);

DELETE FROM `quest_template_addon` WHERE `ID`=45426;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45426, 45498, 46674);
