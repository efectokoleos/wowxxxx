UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98367);

DELETE FROM `creature_queststarter` WHERE `quest`=39904;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98367, 39904);

DELETE FROM `creature_questender` WHERE `quest`=39904;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98367, 39904);

DELETE FROM `quest_template_addon` WHERE `ID`=39904;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39904, 39903);
