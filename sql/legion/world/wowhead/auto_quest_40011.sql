UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97140);

DELETE FROM `creature_queststarter` WHERE `quest`=40011;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97140, 40011);

DELETE FROM `creature_questender` WHERE `quest`=40011;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101499, 40011);

DELETE FROM `quest_template_addon` WHERE `ID`=40011;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40011, 40747);
