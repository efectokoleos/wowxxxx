UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97360);

DELETE FROM `creature_queststarter` WHERE `quest`=39763;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97360, 39763);

DELETE FROM `creature_questender` WHERE `quest`=39763;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97360, 39763);

DELETE FROM `quest_template_addon` WHERE `ID`=39763;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39763, 39790, 39817);
