UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97762);

DELETE FROM `creature_queststarter` WHERE `quest`=39956;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97762, 39956);

DELETE FROM `creature_questender` WHERE `quest`=39956;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97762, 39956);

DELETE FROM `quest_template_addon` WHERE `ID`=39956;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39956, 39955);
