UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92567);

DELETE FROM `creature_queststarter` WHERE `quest`=38615;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92567, 38615);

DELETE FROM `creature_questender` WHERE `quest`=38615;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92573, 38615);

DELETE FROM `quest_template_addon` WHERE `ID`=38615;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38615, 38318, 38342);
