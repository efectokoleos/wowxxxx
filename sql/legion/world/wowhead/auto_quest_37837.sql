UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90233);

DELETE FROM `creature_queststarter` WHERE `quest`=37837;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90233, 37837);

DELETE FROM `creature_questender` WHERE `quest`=37837;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90233, 37837);

DELETE FROM `quest_template_addon` WHERE `ID`=37837;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37837, 37964, 37841);
