DELETE FROM `creature_questender` WHERE `quest`=43926;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(4311, 43926);

DELETE FROM `gameobject_queststarter` WHERE `quest`=43926;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(206109, 43926);

DELETE FROM `quest_template_addon` WHERE `ID`=43926;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43926, 44281);
