UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91144);

DELETE FROM `creature_queststarter` WHERE `quest`=42811;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91144, 42811);

DELETE FROM `creature_questender` WHERE `quest`=42811;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100031, 42811);

DELETE FROM `quest_template_addon` WHERE `ID`=42811;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42811, 38376, 38566);
