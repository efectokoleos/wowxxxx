UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118954);

DELETE FROM `creature_queststarter` WHERE `quest`=46200;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118954, 46200);

DELETE FROM `creature_questender` WHERE `quest`=46200;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 46200);

DELETE FROM `quest_template_addon` WHERE `ID`=46200;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46200, 45864);
