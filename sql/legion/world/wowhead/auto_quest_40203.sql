UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98948);

DELETE FROM `creature_queststarter` WHERE `quest`=40203;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98948, 40203);

DELETE FROM `creature_questender` WHERE `quest`=40203;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98948, 40203);

DELETE FROM `quest_template_addon` WHERE `ID`=40203;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40203, 40210);
