UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104795);

DELETE FROM `creature_queststarter` WHERE `quest`=41759;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104795, 41759);

DELETE FROM `creature_questender` WHERE `quest`=41759;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105816, 41759);

DELETE FROM `quest_template_addon` WHERE `ID`=41759;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41759, 39179);
