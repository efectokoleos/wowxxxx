UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89978);

DELETE FROM `creature_queststarter` WHERE `quest`=37858;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89978, 37858);

DELETE FROM `creature_questender` WHERE `quest`=37858;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90065, 37858);

DELETE FROM `quest_template_addon` WHERE `ID`=37858;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37858, 37957);
