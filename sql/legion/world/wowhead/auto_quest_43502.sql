UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107126);

DELETE FROM `creature_queststarter` WHERE `quest`=43502;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107126, 43502);

DELETE FROM `creature_questender` WHERE `quest`=43502;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103155, 43502);

DELETE FROM `quest_template_addon` WHERE `ID`=43502;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43502, 43562);
