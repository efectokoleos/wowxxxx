UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90866);

DELETE FROM `creature_queststarter` WHERE `quest`=38058;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90866, 38058);

DELETE FROM `quest_template_addon` WHERE `ID`=38058;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38058, 38053, 38060);
