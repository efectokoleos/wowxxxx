UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115517);

DELETE FROM `creature_queststarter` WHERE `quest`=44842;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115517, 44842);

DELETE FROM `creature_questender` WHERE `quest`=44842;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115517, 44842);

DELETE FROM `quest_template_addon` WHERE `ID`=44842;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44842, 44834);
