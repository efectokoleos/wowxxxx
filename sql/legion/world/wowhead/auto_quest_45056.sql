UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101848);

DELETE FROM `creature_queststarter` WHERE `quest`=45056;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101848, 45056);

DELETE FROM `creature_questender` WHERE `quest`=45056;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101848, 45056);
