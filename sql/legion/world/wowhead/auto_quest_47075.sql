UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116302);

DELETE FROM `creature_queststarter` WHERE `quest`=47075;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116302, 47075);

DELETE FROM `creature_questender` WHERE `quest`=47075;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111737, 47075);

DELETE FROM `quest_template_addon` WHERE `ID`=47075;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(47075, 47067, 46940);
