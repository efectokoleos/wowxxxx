UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93354);

DELETE FROM `creature_queststarter` WHERE `quest`=37853;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93354, 37853);

DELETE FROM `creature_questender` WHERE `quest`=37853;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89794, 37853);

DELETE FROM `quest_template_addon` WHERE `ID`=37853;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37853, 38443);
