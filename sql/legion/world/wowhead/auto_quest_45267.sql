UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=45267;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 45267);

DELETE FROM `creature_questender` WHERE `quest`=45267;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 45267);

DELETE FROM `quest_template_addon` WHERE `ID`=45267;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45267, 45282, 44736);
