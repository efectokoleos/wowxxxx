UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120722);

DELETE FROM `creature_queststarter` WHERE `quest`=46347;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120722, 46347);

DELETE FROM `creature_questender` WHERE `quest`=46347;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120722, 46347);

DELETE FROM `quest_template_addon` WHERE `ID`=46347;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46347, 46346, 46348);
