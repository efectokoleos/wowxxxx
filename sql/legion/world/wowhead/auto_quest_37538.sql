UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89048);

DELETE FROM `creature_queststarter` WHERE `quest`=37538;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89048, 37538);

DELETE FROM `gameobject_questender` WHERE `quest`=37538;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(239120, 37538);

DELETE FROM `quest_template_addon` WHERE `ID`=37538;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37538, 37536);
