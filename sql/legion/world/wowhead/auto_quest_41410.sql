UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104586);

DELETE FROM `creature_queststarter` WHERE `quest`=41410;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104586, 41410);

DELETE FROM `creature_questender` WHERE `quest`=41410;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104586, 41410);

DELETE FROM `quest_template_addon` WHERE `ID`=41410;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41410, 41409);
