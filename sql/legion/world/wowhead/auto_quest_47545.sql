UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(27856);

DELETE FROM `creature_queststarter` WHERE `quest`=47545;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(27856, 47545);

DELETE FROM `creature_questender` WHERE `quest`=47545;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(27856, 47545);

DELETE FROM `quest_template_addon` WHERE `ID`=47545;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(47545, 47543, 47550);
