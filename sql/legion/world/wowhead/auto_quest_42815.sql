UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96183);

DELETE FROM `creature_queststarter` WHERE `quest`=42815;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96183, 42815);

DELETE FROM `creature_questender` WHERE `quest`=42815;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93823, 42815);

DELETE FROM `quest_template_addon` WHERE `ID`=42815;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42815, 41052, 39654);
