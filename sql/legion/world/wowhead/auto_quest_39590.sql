UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96254);

DELETE FROM `creature_queststarter` WHERE `quest`=39590;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96254, 39590);

DELETE FROM `gameobject_questender` WHERE `quest`=39590;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(243836, 39590);

DELETE FROM `quest_template_addon` WHERE `ID`=39590;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39590, 38331, 39591);
