UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102570);

DELETE FROM `creature_queststarter` WHERE `quest`=40952;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102570, 40952);

DELETE FROM `creature_questender` WHERE `quest`=40952;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102478, 40952);

DELETE FROM `quest_template_addon` WHERE `ID`=40952;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40952, 40419, 40953);
