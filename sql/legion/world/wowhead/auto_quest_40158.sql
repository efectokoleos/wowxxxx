UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98720);

DELETE FROM `creature_queststarter` WHERE `quest`=40158;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98720, 40158);

DELETE FROM `creature_questender` WHERE `quest`=40158;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98720, 40158);

DELETE FROM `quest_template_addon` WHERE `ID`=40158;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40158, 40157);
