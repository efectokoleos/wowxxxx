UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=44886;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 44886);

DELETE FROM `creature_questender` WHERE `quest`=44886;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 44886);

DELETE FROM `quest_template_addon` WHERE `ID`=44886;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44886, 45422, 44887);
