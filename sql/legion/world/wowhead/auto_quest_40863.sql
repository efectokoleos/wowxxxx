UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93520);

DELETE FROM `creature_queststarter` WHERE `quest`=40863;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93520, 40863);

DELETE FROM `creature_questender` WHERE `quest`=40863;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93520, 40863);

DELETE FROM `quest_template_addon` WHERE `ID`=40863;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40863, 40858, 40864);
