UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93523);

DELETE FROM `creature_queststarter` WHERE `quest`=40196;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93523, 40196);

DELETE FROM `creature_questender` WHERE `quest`=40196;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93523, 40196);

DELETE FROM `quest_template_addon` WHERE `ID`=40196;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40196, 40197);
