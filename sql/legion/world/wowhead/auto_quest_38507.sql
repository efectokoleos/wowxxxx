UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92195);

DELETE FROM `creature_queststarter` WHERE `quest`=38507;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92195, 38507);

DELETE FROM `creature_questender` WHERE `quest`=38507;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 38507);

DELETE FROM `quest_template_addon` WHERE `ID`=38507;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38507, 38506, 38500);
