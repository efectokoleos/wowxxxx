UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106519);

DELETE FROM `creature_queststarter` WHERE `quest`=46258;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106519, 46258);

DELETE FROM `creature_questender` WHERE `quest`=46258;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115572, 46258);

DELETE FROM `quest_template_addon` WHERE `ID`=46258;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46258, 45769);
