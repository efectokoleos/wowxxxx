UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97923);

DELETE FROM `creature_queststarter` WHERE `quest`=41332;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97923, 41332);

DELETE FROM `creature_questender` WHERE `quest`=41332;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104241, 41332);

DELETE FROM `quest_template_addon` WHERE `ID`=41332;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41332, 40651, 40652);
