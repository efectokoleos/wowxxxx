UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(42465);

DELETE FROM `creature_queststarter` WHERE `quest`=42937;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(42465, 42937);

DELETE FROM `creature_questender` WHERE `quest`=42937;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(42465, 42937);

DELETE FROM `quest_template_addon` WHERE `ID`=42937;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42937, 42933);
