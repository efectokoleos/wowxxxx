UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97140);

DELETE FROM `creature_queststarter` WHERE `quest`=42224;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97140, 42224);

DELETE FROM `gameobject_questender` WHERE `quest`=42224;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(250383, 42224);

DELETE FROM `quest_template_addon` WHERE `ID`=42224;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42224, 40325, 42225);
