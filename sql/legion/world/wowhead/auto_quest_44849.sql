UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96469);

DELETE FROM `creature_queststarter` WHERE `quest`=44849;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96469, 44849);

DELETE FROM `creature_questender` WHERE `quest`=44849;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116193, 44849);

DELETE FROM `quest_template_addon` WHERE `ID`=44849;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44849, 46173, 45834);
