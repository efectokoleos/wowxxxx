UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93437);

DELETE FROM `creature_queststarter` WHERE `quest`=42533;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93437, 42533);

DELETE FROM `creature_questender` WHERE `quest`=42533;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107806, 42533);

DELETE FROM `quest_template_addon` WHERE `ID`=42533;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42533, 42534);
