UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106001);

DELETE FROM `creature_queststarter` WHERE `quest`=41777;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106001, 41777);

DELETE FROM `creature_questender` WHERE `quest`=41777;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106001, 41777);

DELETE FROM `quest_template_addon` WHERE `ID`=41777;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41777, 41898);
