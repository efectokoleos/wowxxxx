UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116302);

DELETE FROM `creature_queststarter` WHERE `quest`=47073;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116302, 47073);

DELETE FROM `creature_questender` WHERE `quest`=47073;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98737, 47073);

DELETE FROM `quest_template_addon` WHERE `ID`=47073;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(47073, 47067, 46940);
