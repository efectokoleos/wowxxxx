DELETE FROM `creature_questender` WHERE `quest`=39857;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97480, 39857);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39857;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(244559, 39857);

DELETE FROM `quest_template_addon` WHERE `ID`=39857;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39857, 39848, 39849);
