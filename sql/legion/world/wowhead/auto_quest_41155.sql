UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100812);

DELETE FROM `creature_queststarter` WHERE `quest`=41155;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100812, 41155);

DELETE FROM `creature_questender` WHERE `quest`=41155;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101282, 41155);

DELETE FROM `quest_template_addon` WHERE `ID`=41155;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41155, 40611, 40712);
