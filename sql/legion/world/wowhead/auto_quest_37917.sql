DELETE FROM `gameobject_queststarter` WHERE `quest`=37917;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37917);

DELETE FROM `quest_template_addon` WHERE `ID`=37917;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37917, 37916, 37918);
