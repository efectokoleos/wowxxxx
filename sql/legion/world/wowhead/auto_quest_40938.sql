UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101313);

DELETE FROM `creature_queststarter` WHERE `quest`=40938;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101313, 40938);

DELETE FROM `creature_questender` WHERE `quest`=40938;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102655, 40938);

DELETE FROM `quest_template_addon` WHERE `ID`=40938;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40938, 41015);
