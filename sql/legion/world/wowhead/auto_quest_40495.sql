UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101097);

DELETE FROM `creature_queststarter` WHERE `quest`=40495;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101097, 40495);

DELETE FROM `creature_questender` WHERE `quest`=40495;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100323, 40495);

DELETE FROM `quest_template_addon` WHERE `ID`=40495;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40495, 42939, 40588);
