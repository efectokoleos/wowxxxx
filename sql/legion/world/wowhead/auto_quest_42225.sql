DELETE FROM `gameobject_queststarter` WHERE `quest`=42225;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(250383, 42225);

DELETE FROM `gameobject_questender` WHERE `quest`=42225;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(250383, 42225);

DELETE FROM `quest_template_addon` WHERE `ID`=42225;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42225, 42224);
