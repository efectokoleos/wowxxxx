UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(42465);

DELETE FROM `creature_queststarter` WHERE `quest`=42935;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(42465, 42935);

DELETE FROM `creature_questender` WHERE `quest`=42935;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(42465, 42935);

DELETE FROM `quest_template_addon` WHERE `ID`=42935;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42935, 42932, 42937);
