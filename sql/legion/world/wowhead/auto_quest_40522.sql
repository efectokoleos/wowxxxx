UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100453);

DELETE FROM `creature_queststarter` WHERE `quest`=40522;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100453, 40522);

DELETE FROM `creature_questender` WHERE `quest`=40522;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100866, 40522);

DELETE FROM `quest_template_addon` WHERE `ID`=40522;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40522, 40518, 40760);
