UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116302);

DELETE FROM `creature_queststarter` WHERE `quest`=46765;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116302, 46765);

DELETE FROM `creature_questender` WHERE `quest`=46765;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116302, 46765);

DELETE FROM `quest_template_addon` WHERE `ID`=46765;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46765, 46744, 44781);
