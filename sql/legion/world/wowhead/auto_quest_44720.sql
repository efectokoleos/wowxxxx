UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114946);

DELETE FROM `creature_queststarter` WHERE `quest`=44720;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114946, 44720);

DELETE FROM `creature_questender` WHERE `quest`=44720;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92539, 44720);

DELETE FROM `quest_template_addon` WHERE `ID`=44720;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44720, 44771);
