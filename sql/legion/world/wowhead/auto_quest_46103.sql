UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119259);

DELETE FROM `creature_queststarter` WHERE `quest`=46103;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119259, 46103);

DELETE FROM `creature_questender` WHERE `quest`=46103;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98099, 46103);
