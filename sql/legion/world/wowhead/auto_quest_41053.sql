UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102574);

DELETE FROM `creature_queststarter` WHERE `quest`=41053;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102574, 41053);

DELETE FROM `creature_questender` WHERE `quest`=41053;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102940, 41053);

DELETE FROM `quest_template_addon` WHERE `ID`=41053;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41053, 41047);
