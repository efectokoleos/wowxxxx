UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=40698;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 40698);

DELETE FROM `creature_questender` WHERE `quest`=40698;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 40698);

DELETE FROM `quest_template_addon` WHERE `ID`=40698;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40698, 40793);
