UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92194);

DELETE FROM `creature_queststarter` WHERE `quest`=38506;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92194, 38506);

DELETE FROM `creature_questender` WHERE `quest`=38506;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92195, 38506);

DELETE FROM `quest_template_addon` WHERE `ID`=38506;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38506, 38505, 38507);
