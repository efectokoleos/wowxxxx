UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93779);

DELETE FROM `creature_queststarter` WHERE `quest`=39063;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93779, 39063);

DELETE FROM `creature_questender` WHERE `quest`=39063;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95921, 39063);

DELETE FROM `quest_template_addon` WHERE `ID`=39063;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39063, 39062, 39092);
