UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101547);

DELETE FROM `creature_queststarter` WHERE `quest`=40755;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101547, 40755);

DELETE FROM `creature_questender` WHERE `quest`=40755;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113836, 40755);

DELETE FROM `quest_template_addon` WHERE `ID`=40755;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40755, 42970);
