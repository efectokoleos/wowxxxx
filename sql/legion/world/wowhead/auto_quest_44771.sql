UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92539);

DELETE FROM `creature_queststarter` WHERE `quest`=44771;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92539, 44771);

DELETE FROM `creature_questender` WHERE `quest`=44771;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92539, 44771);

DELETE FROM `quest_template_addon` WHERE `ID`=44771;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44771, 44720, 44721);
