UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96469);

DELETE FROM `creature_queststarter` WHERE `quest`=44889;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96469, 44889);

DELETE FROM `creature_questender` WHERE `quest`=44889;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116858, 44889);

DELETE FROM `quest_template_addon` WHERE `ID`=44889;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44889, 45118, 45634);
