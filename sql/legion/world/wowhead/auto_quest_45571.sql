UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=45571;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 45571);

DELETE FROM `creature_questender` WHERE `quest`=45571;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117259, 45571);
