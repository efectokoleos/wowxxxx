UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96665);

DELETE FROM `creature_queststarter` WHERE `quest`=39682;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96665, 39682);

DELETE FROM `creature_questender` WHERE `quest`=39682;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97643, 39682);

DELETE FROM `quest_template_addon` WHERE `ID`=39682;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39682, 38723);
