UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107423);

DELETE FROM `creature_queststarter` WHERE `quest`=42424;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107423, 42424);

DELETE FROM `creature_questender` WHERE `quest`=42424;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108515, 42424);

DELETE FROM `quest_template_addon` WHERE `ID`=42424;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42424, 42423, 42451);
