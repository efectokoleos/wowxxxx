UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98698);

DELETE FROM `creature_queststarter` WHERE `quest`=39918;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98698, 39918);

DELETE FROM `creature_questender` WHERE `quest`=39918;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107139, 39918);

DELETE FROM `quest_template_addon` WHERE `ID`=39918;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39918, 40130, 39907);
