UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107973);

DELETE FROM `creature_queststarter` WHERE `quest`=42402;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107973, 42402);

DELETE FROM `creature_questender` WHERE `quest`=42402;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107973, 42402);

DELETE FROM `quest_template_addon` WHERE `ID`=42402;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42402, 42407, 42405);
