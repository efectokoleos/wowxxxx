UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89398);

DELETE FROM `creature_queststarter` WHERE `quest`=39936;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89398, 39936);

DELETE FROM `creature_questender` WHERE `quest`=39936;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97736, 39936);

DELETE FROM `quest_template_addon` WHERE `ID`=39936;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39936, 39937);
