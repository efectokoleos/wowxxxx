UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93522);

DELETE FROM `creature_queststarter` WHERE `quest`=40198;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93522, 40198);

DELETE FROM `creature_questender` WHERE `quest`=40198;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93522, 40198);

DELETE FROM `quest_template_addon` WHERE `ID`=40198;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40198, 40202);
