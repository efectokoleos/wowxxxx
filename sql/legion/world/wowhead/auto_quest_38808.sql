UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93231);

DELETE FROM `creature_queststarter` WHERE `quest`=38808;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93231, 38808);

DELETE FROM `creature_questender` WHERE `quest`=38808;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93231, 38808);

DELETE FROM `quest_template_addon` WHERE `ID`=38808;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38808, 38778, 38811);
