UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96528);

DELETE FROM `creature_queststarter` WHERE `quest`=40276;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96528, 40276);

DELETE FROM `creature_questender` WHERE `quest`=40276;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96528, 40276);

DELETE FROM `quest_template_addon` WHERE `ID`=40276;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40276, 41510);
