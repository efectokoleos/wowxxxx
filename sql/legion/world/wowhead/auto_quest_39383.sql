UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95395);

DELETE FROM `creature_queststarter` WHERE `quest`=39383;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95395, 39383);

DELETE FROM `creature_questender` WHERE `quest`=39383;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95396, 39383);

DELETE FROM `quest_template_addon` WHERE `ID`=39383;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39383, 38382, 39384);
