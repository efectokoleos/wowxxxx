UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98646);

DELETE FROM `creature_queststarter` WHERE `quest`=42920;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98646, 42920);

DELETE FROM `creature_questender` WHERE `quest`=42920;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108572, 42920);

DELETE FROM `quest_template_addon` WHERE `ID`=42920;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42920, 42132);
