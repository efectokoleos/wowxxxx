UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115492);

DELETE FROM `creature_queststarter` WHERE `quest`=45769;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115492, 45769);

DELETE FROM `creature_questender` WHERE `quest`=45769;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106519, 45769);

DELETE FROM `quest_template_addon` WHERE `ID`=45769;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45769, 45883, 46258);
