UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107372);

DELETE FROM `creature_queststarter` WHERE `quest`=42386;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107372, 42386);

DELETE FROM `creature_questender` WHERE `quest`=42386;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107372, 42386);

DELETE FROM `quest_template_addon` WHERE `ID`=42386;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42386, 42385);
