UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101344);

DELETE FROM `creature_queststarter` WHERE `quest`=40705;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101344, 40705);

DELETE FROM `creature_questender` WHERE `quest`=40705;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101314, 40705);

DELETE FROM `quest_template_addon` WHERE `ID`=40705;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40705, 40706);
