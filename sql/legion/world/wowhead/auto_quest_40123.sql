UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98312);

DELETE FROM `creature_queststarter` WHERE `quest`=40123;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98312, 40123);

DELETE FROM `creature_questender` WHERE `quest`=40123;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98312, 40123);

DELETE FROM `quest_template_addon` WHERE `ID`=40123;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40123, 39987, 40009);
