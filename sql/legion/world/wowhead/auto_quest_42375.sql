UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107244);

DELETE FROM `creature_queststarter` WHERE `quest`=42375;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107244, 42375);

DELETE FROM `creature_questender` WHERE `quest`=42375;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107244, 42375);

DELETE FROM `quest_template_addon` WHERE `ID`=42375;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42375, 42372, 42369);
