UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96984);

DELETE FROM `creature_queststarter` WHERE `quest`=39769;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96984, 39769);

DELETE FROM `creature_questender` WHERE `quest`=39769;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97130, 39769);

DELETE FROM `quest_template_addon` WHERE `ID`=39769;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39769, 39765);
