UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98711);

DELETE FROM `creature_queststarter` WHERE `quest`=40222;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98711, 40222);

DELETE FROM `creature_questender` WHERE `quest`=40222;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99045, 40222);

DELETE FROM `quest_template_addon` WHERE `ID`=40222;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40222, 38725, 40051);
