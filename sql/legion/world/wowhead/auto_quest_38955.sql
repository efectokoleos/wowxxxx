UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93967);

DELETE FROM `creature_queststarter` WHERE `quest`=38955;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93967, 38955);

DELETE FROM `creature_questender` WHERE `quest`=38955;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93967, 38955);

DELETE FROM `quest_template_addon` WHERE `ID`=38955;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38955, 38954);
