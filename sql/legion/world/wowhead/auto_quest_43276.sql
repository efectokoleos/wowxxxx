UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110564);

DELETE FROM `creature_queststarter` WHERE `quest`=43276;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110564, 43276);

DELETE FROM `creature_questender` WHERE `quest`=43276;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110564, 43276);

DELETE FROM `quest_template_addon` WHERE `ID`=43276;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43276, 43277);
