UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92458);

DELETE FROM `creature_queststarter` WHERE `quest`=39329;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92458, 39329);

DELETE FROM `creature_questender` WHERE `quest`=39329;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92458, 39329);

DELETE FROM `quest_template_addon` WHERE `ID`=39329;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39329, 39328, 39330);
