UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93520);

DELETE FROM `creature_queststarter` WHERE `quest`=40864;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93520, 40864);

DELETE FROM `creature_questender` WHERE `quest`=40864;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93520, 40864);

DELETE FROM `quest_template_addon` WHERE `ID`=40864;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40864, 40863);
