UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114561);

DELETE FROM `creature_queststarter` WHERE `quest`=44182;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114561, 44182);

DELETE FROM `creature_questender` WHERE `quest`=44182;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113986, 44182);

DELETE FROM `quest_template_addon` WHERE `ID`=44182;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44182, 44421);
