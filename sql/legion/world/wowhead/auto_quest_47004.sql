UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90418);

DELETE FROM `creature_queststarter` WHERE `quest`=47004;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90418, 47004);

DELETE FROM `creature_questender` WHERE `quest`=47004;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118752, 47004);

DELETE FROM `quest_template_addon` WHERE `ID`=47004;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(47004, 47027, 46079);
