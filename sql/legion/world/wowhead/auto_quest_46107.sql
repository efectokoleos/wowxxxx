UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118954);

DELETE FROM `creature_queststarter` WHERE `quest`=46107;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118954, 46107);

DELETE FROM `creature_questender` WHERE `quest`=46107;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118954, 46107);

DELETE FROM `quest_template_addon` WHERE `ID`=46107;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46107, 46106);
