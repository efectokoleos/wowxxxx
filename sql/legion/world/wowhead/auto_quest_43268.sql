UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93568);

DELETE FROM `creature_queststarter` WHERE `quest`=43268;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93568, 43268);

DELETE FROM `creature_questender` WHERE `quest`=43268;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93568, 43268);

DELETE FROM `quest_template_addon` WHERE `ID`=43268;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43268, 43267);
