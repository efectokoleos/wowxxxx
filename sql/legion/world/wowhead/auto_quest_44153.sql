UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=44153;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 44153);

DELETE FROM `creature_questender` WHERE `quest`=44153;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(112953, 44153);

DELETE FROM `quest_template_addon` WHERE `ID`=44153;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44153, 44004);
