UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93523);

DELETE FROM `creature_queststarter` WHERE `quest`=40197;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93523, 40197);

DELETE FROM `creature_questender` WHERE `quest`=40197;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93523, 40197);

DELETE FROM `quest_template_addon` WHERE `ID`=40197;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40197, 40196, 41889);
