UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104630);

DELETE FROM `creature_queststarter` WHERE `quest`=41606;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104630, 41606);

DELETE FROM `creature_questender` WHERE `quest`=41606;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104406, 41606);

DELETE FROM `quest_template_addon` WHERE `ID`=41606;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41606, 40364);
