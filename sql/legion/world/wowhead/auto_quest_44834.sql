UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115517);

DELETE FROM `creature_queststarter` WHERE `quest`=44834;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115517, 44834);

DELETE FROM `creature_questender` WHERE `quest`=44834;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115517, 44834);

DELETE FROM `quest_template_addon` WHERE `ID`=44834;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44834, 44842);
