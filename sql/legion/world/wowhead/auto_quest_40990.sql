UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101846);

DELETE FROM `creature_queststarter` WHERE `quest`=40990;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101846, 40990);

DELETE FROM `creature_questender` WHERE `quest`=40990;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101846, 40990);

DELETE FROM `quest_template_addon` WHERE `ID`=40990;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40990, 40988, 40991);
