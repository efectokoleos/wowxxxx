UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94605);

DELETE FROM `creature_queststarter` WHERE `quest`=39177;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94605, 39177);

DELETE FROM `creature_questender` WHERE `quest`=39177;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94686, 39177);

DELETE FROM `quest_template_addon` WHERE `ID`=39177;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39177, 39176);
