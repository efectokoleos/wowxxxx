UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99190);

DELETE FROM `creature_queststarter` WHERE `quest`=39577;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99190, 39577);

DELETE FROM `creature_questender` WHERE `quest`=39577;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96270, 39577);

DELETE FROM `quest_template_addon` WHERE `ID`=39577;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39577, 39578, 39579);
