UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92183);

DELETE FROM `creature_queststarter` WHERE `quest`=38522;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92183, 38522);

DELETE FROM `creature_questender` WHERE `quest`=38522;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 38522);

DELETE FROM `quest_template_addon` WHERE `ID`=38522;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38522, 38518, 38523);
