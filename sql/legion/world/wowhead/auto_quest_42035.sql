UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105948);

DELETE FROM `creature_queststarter` WHERE `quest`=42035;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105948, 42035);

DELETE FROM `creature_questender` WHERE `quest`=42035;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106883, 42035);

DELETE FROM `quest_template_addon` WHERE `ID`=42035;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42035, 42034);
