UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117475);

DELETE FROM `creature_queststarter` WHERE `quest`=45649;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117475, 45649);

DELETE FROM `creature_questender` WHERE `quest`=45649;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117475, 45649);

DELETE FROM `quest_template_addon` WHERE `ID`=45649;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45649, 45648);
