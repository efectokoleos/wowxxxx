UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96686);

DELETE FROM `creature_queststarter` WHERE `quest`=39698;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96686, 39698);

DELETE FROM `creature_questender` WHERE `quest`=39698;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96686, 39698);

DELETE FROM `quest_template_addon` WHERE `ID`=39698;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39698, 39801);
