UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=39987;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 39987);

DELETE FROM `creature_questender` WHERE `quest`=39987;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98312, 39987);

DELETE FROM `quest_template_addon` WHERE `ID`=39987;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39987, 39986, 40123);
