UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=43829;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 43829);

DELETE FROM `creature_questender` WHERE `quest`=43829;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101513, 43829);

DELETE FROM `quest_template_addon` WHERE `ID`=43829;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43829, 43958, 44041);
