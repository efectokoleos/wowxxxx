UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102120);

DELETE FROM `creature_queststarter` WHERE `quest`=40849;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102120, 40849);

DELETE FROM `creature_questender` WHERE `quest`=40849;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94159, 40849);
