UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=44041;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 44041);

DELETE FROM `creature_questender` WHERE `quest`=44041;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101513, 44041);

DELETE FROM `quest_template_addon` WHERE `ID`=44041;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44041, 43829);
