UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(6566);

DELETE FROM `creature_queststarter` WHERE `quest`=38404;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(6566, 38404);

DELETE FROM `creature_questender` WHERE `quest`=38404;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(6566, 38404);

DELETE FROM `quest_template_addon` WHERE `ID`=38404;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38404, 38397);
