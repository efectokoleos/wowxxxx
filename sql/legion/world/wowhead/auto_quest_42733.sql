UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98648);

DELETE FROM `creature_queststarter` WHERE `quest`=42733;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98648, 42733);

DELETE FROM `creature_questender` WHERE `quest`=42733;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98646, 42733);

DELETE FROM `quest_template_addon` WHERE `ID`=42733;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42733, 42754);
