UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101097);

DELETE FROM `creature_queststarter` WHERE `quest`=42608;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101097, 42608);

DELETE FROM `creature_questender` WHERE `quest`=42608;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106199, 42608);

DELETE FROM `quest_template_addon` WHERE `ID`=42608;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42608, 42603);
