UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91109);

DELETE FROM `creature_queststarter` WHERE `quest`=38377;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91109, 38377);

DELETE FROM `creature_questender` WHERE `quest`=38377;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100387, 38377);

DELETE FROM `quest_template_addon` WHERE `ID`=38377;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38377, 38148);
