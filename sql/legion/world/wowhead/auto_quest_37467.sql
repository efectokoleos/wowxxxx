UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88867);

DELETE FROM `creature_queststarter` WHERE `quest`=37467;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88867, 37467);

DELETE FROM `creature_questender` WHERE `quest`=37467;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88890, 37467);

DELETE FROM `quest_template_addon` WHERE `ID`=37467;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37467, 37486, 37468);
