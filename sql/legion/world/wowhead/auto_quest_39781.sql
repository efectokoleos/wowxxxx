UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93846);

DELETE FROM `creature_queststarter` WHERE `quest`=39781;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93846, 39781);

DELETE FROM `gameobject_questender` WHERE `quest`=39781;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(250548, 39781);

DELETE FROM `quest_template_addon` WHERE `ID`=39781;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39781, 42454);
