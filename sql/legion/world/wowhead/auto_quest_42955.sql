UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109307);

DELETE FROM `creature_queststarter` WHERE `quest`=42955;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109307, 42955);

DELETE FROM `creature_questender` WHERE `quest`=42955;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109307, 42955);

DELETE FROM `quest_template_addon` WHERE `ID`=42955;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42955, 42954, 42956);
