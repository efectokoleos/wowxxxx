UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97359);

DELETE FROM `creature_queststarter` WHERE `quest`=39949;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97359, 39949);

DELETE FROM `creature_questender` WHERE `quest`=39949;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97359, 39949);

DELETE FROM `quest_template_addon` WHERE `ID`=39949;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39949, 39948, 39950);
