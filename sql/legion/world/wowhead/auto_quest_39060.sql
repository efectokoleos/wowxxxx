UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94318);

DELETE FROM `creature_queststarter` WHERE `quest`=39060;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94318, 39060);

DELETE FROM `creature_questender` WHERE `quest`=39060;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94318, 39060);

DELETE FROM `quest_template_addon` WHERE `ID`=39060;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39060, 39059, 39062);
