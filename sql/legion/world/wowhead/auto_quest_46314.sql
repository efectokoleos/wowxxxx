UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90431);

DELETE FROM `creature_queststarter` WHERE `quest`=46314;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90431, 46314);

DELETE FROM `creature_questender` WHERE `quest`=46314;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116880, 46314);

DELETE FROM `quest_template_addon` WHERE `ID`=46314;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46314, 47030, 45413);
