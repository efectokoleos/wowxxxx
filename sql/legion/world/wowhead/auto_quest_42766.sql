UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108700);

DELETE FROM `creature_queststarter` WHERE `quest`=42766;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108700, 42766);

DELETE FROM `creature_questender` WHERE `quest`=42766;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109049, 42766);

DELETE FROM `quest_template_addon` WHERE `ID`=42766;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42766, 42762, 42957);
