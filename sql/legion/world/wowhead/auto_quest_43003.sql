UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(42470);

DELETE FROM `creature_queststarter` WHERE `quest`=43003;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(42470, 43003);

DELETE FROM `creature_questender` WHERE `quest`=43003;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109464, 43003);
