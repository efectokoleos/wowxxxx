UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106905);

DELETE FROM `creature_queststarter` WHERE `quest`=42055;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106905, 42055);

DELETE FROM `creature_questender` WHERE `quest`=42055;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103832, 42055);
