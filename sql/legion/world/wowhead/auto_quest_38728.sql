UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97297);

DELETE FROM `creature_queststarter` WHERE `quest`=38728;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97297, 38728);

DELETE FROM `creature_questender` WHERE `quest`=38728;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97303, 38728);

DELETE FROM `quest_template_addon` WHERE `ID`=38728;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38728, 39663);
