UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108782);

DELETE FROM `creature_queststarter` WHERE `quest`=42808;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108782, 42808);

DELETE FROM `creature_questender` WHERE `quest`=42808;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108782, 42808);

DELETE FROM `quest_template_addon` WHERE `ID`=42808;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42808, 42802);
