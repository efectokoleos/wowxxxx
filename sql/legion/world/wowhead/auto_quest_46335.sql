UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108247);

DELETE FROM `creature_queststarter` WHERE `quest`=46335;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108247, 46335);

DELETE FROM `creature_questender` WHERE `quest`=46335;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108247, 46335);

DELETE FROM `quest_template_addon` WHERE `ID`=46335;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46335, 44766, 46338);
