DELETE FROM `creature_questender` WHERE `quest`=42226;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107126, 42226);

DELETE FROM `gameobject_queststarter` WHERE `quest`=42226;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(250383, 42226);

DELETE FROM `quest_template_addon` WHERE `ID`=42226;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42226, 42227);
