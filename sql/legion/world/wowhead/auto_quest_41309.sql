UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103437);

DELETE FROM `creature_queststarter` WHERE `quest`=41309;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103437, 41309);

DELETE FROM `creature_questender` WHERE `quest`=41309;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104161, 41309);

DELETE FROM `quest_template_addon` WHERE `ID`=41309;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41309, 41214);
