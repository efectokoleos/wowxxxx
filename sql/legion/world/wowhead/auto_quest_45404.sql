UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119664);

DELETE FROM `creature_queststarter` WHERE `quest`=45404;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119664, 45404);

DELETE FROM `creature_questender` WHERE `quest`=45404;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119765, 45404);

DELETE FROM `quest_template_addon` WHERE `ID`=45404;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45404, 45459);
