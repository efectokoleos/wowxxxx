UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(111019);

DELETE FROM `creature_queststarter` WHERE `quest`=41231;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(111019, 41231);

DELETE FROM `creature_questender` WHERE `quest`=41231;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111019, 41231);

DELETE FROM `quest_template_addon` WHERE `ID`=41231;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41231, 43582);
