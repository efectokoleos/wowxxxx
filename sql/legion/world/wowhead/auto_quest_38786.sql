UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93691);

DELETE FROM `creature_queststarter` WHERE `quest`=38786;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93691, 38786);

DELETE FROM `creature_questender` WHERE `quest`=38786;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93691, 38786);

DELETE FROM `quest_template_addon` WHERE `ID`=38786;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38786, 38888, 38787);
