UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92400);

DELETE FROM `creature_queststarter` WHERE `quest`=38574;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92400, 38574);

DELETE FROM `creature_questender` WHERE `quest`=38574;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91242, 38574);

DELETE FROM `quest_template_addon` WHERE `ID`=38574;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38574, 38573);
