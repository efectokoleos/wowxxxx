UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103437);

DELETE FROM `creature_queststarter` WHERE `quest`=41222;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103437, 41222);

DELETE FROM `creature_questender` WHERE `quest`=41222;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103437, 41222);

DELETE FROM `quest_template_addon` WHERE `ID`=41222;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41222, 41214);
