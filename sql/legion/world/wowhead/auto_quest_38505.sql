UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92183);

DELETE FROM `creature_queststarter` WHERE `quest`=38505;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92183, 38505);

DELETE FROM `creature_questender` WHERE `quest`=38505;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92194, 38505);

DELETE FROM `quest_template_addon` WHERE `ID`=38505;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38505, 38501, 38506);
