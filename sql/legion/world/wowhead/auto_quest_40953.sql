UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102478);

DELETE FROM `creature_queststarter` WHERE `quest`=40953;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102478, 40953);

DELETE FROM `creature_questender` WHERE `quest`=40953;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102578, 40953);

DELETE FROM `quest_template_addon` WHERE `ID`=40953;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40953, 40952, 40954);
