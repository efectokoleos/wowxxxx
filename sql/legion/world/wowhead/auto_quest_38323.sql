UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100573);

DELETE FROM `creature_queststarter` WHERE `quest`=38323;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100573, 38323);

DELETE FROM `creature_questender` WHERE `quest`=38323;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91109, 38323);

DELETE FROM `quest_template_addon` WHERE `ID`=38323;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38323, 40573, 38377);
