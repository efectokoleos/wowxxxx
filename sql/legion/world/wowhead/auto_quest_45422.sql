UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=45422;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 45422);

DELETE FROM `quest_template_addon` WHERE `ID`=45422;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45422, 44886);
