UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101767);

DELETE FROM `creature_queststarter` WHERE `quest`=40963;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101767, 40963);

DELETE FROM `creature_questender` WHERE `quest`=40963;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102425, 40963);
