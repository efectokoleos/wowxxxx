UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98102);

DELETE FROM `creature_queststarter` WHERE `quest`=43250;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98102, 43250);

DELETE FROM `creature_questender` WHERE `quest`=43250;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110164, 43250);

DELETE FROM `quest_template_addon` WHERE `ID`=43250;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43250, 43249);
