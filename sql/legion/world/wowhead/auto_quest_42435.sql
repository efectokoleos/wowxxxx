UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107532);

DELETE FROM `creature_queststarter` WHERE `quest`=42435;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107532, 42435);

DELETE FROM `creature_questender` WHERE `quest`=42435;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106530, 42435);

DELETE FROM `quest_template_addon` WHERE `ID`=42435;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42435, 42434, 42166);
