UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106879);

DELETE FROM `creature_queststarter` WHERE `quest`=41009;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106879, 41009);

DELETE FROM `creature_questender` WHERE `quest`=41009;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102478, 41009);

DELETE FROM `quest_template_addon` WHERE `ID`=41009;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41009, 40419, 40953);
