UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98100);

DELETE FROM `creature_queststarter` WHERE `quest`=43485;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98100, 43485);

DELETE FROM `creature_questender` WHERE `quest`=43485;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110714, 43485);

DELETE FROM `quest_template_addon` WHERE `ID`=43485;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43485, 43469);
