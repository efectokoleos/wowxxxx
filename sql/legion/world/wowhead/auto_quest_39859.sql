UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94434);

DELETE FROM `creature_queststarter` WHERE `quest`=39859;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94434, 39859);

DELETE FROM `creature_questender` WHERE `quest`=39859;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94409, 39859);

DELETE FROM `quest_template_addon` WHERE `ID`=39859;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39859, 39417, 40216);
