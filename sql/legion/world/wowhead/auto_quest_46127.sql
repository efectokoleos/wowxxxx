UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117871);

DELETE FROM `creature_queststarter` WHERE `quest`=46127;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117871, 46127);

DELETE FROM `creature_questender` WHERE `quest`=46127;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90463, 46127);

DELETE FROM `quest_template_addon` WHERE `ID`=46127;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46127, 45910);
