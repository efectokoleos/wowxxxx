UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93530);

DELETE FROM `creature_queststarter` WHERE `quest`=39876;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93530, 39876);

DELETE FROM `creature_questender` WHERE `quest`=39876;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90317, 39876);

DELETE FROM `quest_template_addon` WHERE `ID`=39876;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39876, 39875, 39877);
