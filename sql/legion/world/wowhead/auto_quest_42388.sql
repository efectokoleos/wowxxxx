UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113325);

DELETE FROM `creature_queststarter` WHERE `quest`=42388;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113325, 42388);

DELETE FROM `creature_questender` WHERE `quest`=42388;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107317, 42388);

DELETE FROM `quest_template_addon` WHERE `ID`=42388;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42388, 42389);
