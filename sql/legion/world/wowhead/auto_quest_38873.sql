UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93603);

DELETE FROM `creature_queststarter` WHERE `quest`=38873;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93603, 38873);

DELETE FROM `creature_questender` WHERE `quest`=38873;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93603, 38873);

DELETE FROM `quest_template_addon` WHERE `ID`=38873;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38873, 38872, 39154);
