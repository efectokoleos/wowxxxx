UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118536);

DELETE FROM `creature_queststarter` WHERE `quest`=45788;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118536, 45788);

DELETE FROM `creature_questender` WHERE `quest`=45788;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102709, 45788);

DELETE FROM `quest_template_addon` WHERE `ID`=45788;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45788, 45789);
