UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94346);

DELETE FROM `creature_queststarter` WHERE `quest`=39059;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94346, 39059);

DELETE FROM `creature_questender` WHERE `quest`=39059;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94318, 39059);

DELETE FROM `quest_template_addon` WHERE `ID`=39059;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39059, 39060);
