UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=44215;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 44215);

DELETE FROM `creature_questender` WHERE `quest`=44215;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113362, 44215);

DELETE FROM `quest_template_addon` WHERE `ID`=44215;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44215, 43422);
