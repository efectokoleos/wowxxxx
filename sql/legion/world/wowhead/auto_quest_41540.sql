UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102478);

DELETE FROM `creature_queststarter` WHERE `quest`=41540;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102478, 41540);

DELETE FROM `creature_questender` WHERE `quest`=41540;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103741, 41540);

DELETE FROM `quest_template_addon` WHERE `ID`=41540;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41540, 40392);
