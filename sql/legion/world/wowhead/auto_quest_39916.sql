UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98675);

DELETE FROM `creature_queststarter` WHERE `quest`=39916;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98675, 39916);

DELETE FROM `creature_questender` WHERE `quest`=39916;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98675, 39916);

DELETE FROM `quest_template_addon` WHERE `ID`=39916;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39916, 40169, 40130);
