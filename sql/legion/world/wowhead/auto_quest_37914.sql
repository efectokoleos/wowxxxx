UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89793);

DELETE FROM `creature_queststarter` WHERE `quest`=37914;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89793, 37914);

DELETE FROM `quest_template_addon` WHERE `ID`=37914;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37914, 37916);
