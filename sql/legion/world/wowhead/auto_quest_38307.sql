UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96683);

DELETE FROM `creature_queststarter` WHERE `quest`=38307;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96683, 38307);

DELETE FROM `creature_questender` WHERE `quest`=38307;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96683, 38307);

DELETE FROM `quest_template_addon` WHERE `ID`=38307;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38307, 39864);
