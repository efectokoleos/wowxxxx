DELETE FROM `creature_questender` WHERE `quest`=39431;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93974, 39431);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39431;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(249211, 39431);

DELETE FROM `quest_template_addon` WHERE `ID`=39431;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39431, 39337, 44112);
