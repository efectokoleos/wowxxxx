UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93523);

DELETE FROM `creature_queststarter` WHERE `quest`=39958;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93523, 39958);

DELETE FROM `creature_questender` WHERE `quest`=39958;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93523, 39958);

DELETE FROM `quest_template_addon` WHERE `ID`=39958;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39958, 40183);
