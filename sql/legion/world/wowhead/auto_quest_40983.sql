UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100873);

DELETE FROM `creature_queststarter` WHERE `quest`=40983;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100873, 40983);

DELETE FROM `creature_questender` WHERE `quest`=40983;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101035, 40983);

DELETE FROM `quest_template_addon` WHERE `ID`=40983;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40983, 40760, 40605);
