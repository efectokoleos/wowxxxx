UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119357);

DELETE FROM `creature_queststarter` WHERE `quest`=46275;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119357, 46275);

DELETE FROM `creature_questender` WHERE `quest`=46275;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120268, 46275);

DELETE FROM `quest_template_addon` WHERE `ID`=46275;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46275, 46274, 46282);
