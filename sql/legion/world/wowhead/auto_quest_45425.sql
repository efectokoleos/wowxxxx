UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120084);

DELETE FROM `creature_queststarter` WHERE `quest`=45425;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120084, 45425);

DELETE FROM `creature_questender` WHERE `quest`=45425;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101195, 45425);

DELETE FROM `quest_template_addon` WHERE `ID`=45425;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45425, 46677);
