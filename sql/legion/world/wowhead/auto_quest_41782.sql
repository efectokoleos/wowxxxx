UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101259);

DELETE FROM `creature_queststarter` WHERE `quest`=41782;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101259, 41782);

DELETE FROM `creature_questender` WHERE `quest`=41782;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104535, 41782);

DELETE FROM `quest_template_addon` WHERE `ID`=41782;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41782, 41468, 41783);
