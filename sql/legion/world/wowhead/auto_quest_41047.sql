UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102940);

DELETE FROM `creature_queststarter` WHERE `quest`=41047;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102940, 41047);

DELETE FROM `creature_questender` WHERE `quest`=41047;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102940, 41047);

DELETE FROM `quest_template_addon` WHERE `ID`=41047;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41047, 41053, 40958);
