UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105102);

DELETE FROM `creature_queststarter` WHERE `quest`=41785;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105102, 41785);

DELETE FROM `creature_questender` WHERE `quest`=41785;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104855, 41785);

DELETE FROM `quest_template_addon` WHERE `ID`=41785;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41785, 42102, 41787);
