UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99153);

DELETE FROM `creature_queststarter` WHERE `quest`=40219;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99153, 40219);

DELETE FROM `creature_questender` WHERE `quest`=40219;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108069, 40219);

DELETE FROM `quest_template_addon` WHERE `ID`=40219;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40219, 39575, 39578);
