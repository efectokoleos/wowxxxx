UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104824);

DELETE FROM `creature_queststarter` WHERE `quest`=41780;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104824, 41780);

DELETE FROM `creature_questender` WHERE `quest`=41780;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104824, 41780);

DELETE FROM `quest_template_addon` WHERE `ID`=41780;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41780, 41781, 41784);
