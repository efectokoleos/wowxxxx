UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107723);

DELETE FROM `creature_queststarter` WHERE `quest`=42510;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107723, 42510);

DELETE FROM `creature_questender` WHERE `quest`=42510;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107723, 42510);

DELETE FROM `quest_template_addon` WHERE `ID`=42510;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42510, 37447, 42522);
