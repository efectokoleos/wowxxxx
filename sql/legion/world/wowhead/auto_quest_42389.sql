UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107317);

DELETE FROM `creature_queststarter` WHERE `quest`=42389;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107317, 42389);

DELETE FROM `creature_questender` WHERE `quest`=42389;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107316, 42389);

DELETE FROM `quest_template_addon` WHERE `ID`=42389;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42389, 42388, 42391);
