UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105342);

DELETE FROM `creature_queststarter` WHERE `quest`=42079;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105342, 42079);

DELETE FROM `creature_questender` WHERE `quest`=42079;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105342, 42079);

DELETE FROM `quest_template_addon` WHERE `ID`=42079;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42079, 42147);
