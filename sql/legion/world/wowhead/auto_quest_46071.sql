UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=46071;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 46071);

DELETE FROM `creature_questender` WHERE `quest`=46071;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 46071);

DELETE FROM `quest_template_addon` WHERE `ID`=46071;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46071, 46070, 46083);
