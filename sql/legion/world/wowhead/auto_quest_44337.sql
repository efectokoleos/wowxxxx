UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113686);

DELETE FROM `creature_queststarter` WHERE `quest`=44337;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113686, 44337);

DELETE FROM `creature_questender` WHERE `quest`=44337;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 44337);

DELETE FROM `quest_template_addon` WHERE `ID`=44337;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44337, 44448);
