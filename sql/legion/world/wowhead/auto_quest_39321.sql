UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95410);

DELETE FROM `creature_queststarter` WHERE `quest`=39321;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95410, 39321);

DELETE FROM `creature_questender` WHERE `quest`=39321;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95410, 39321);

DELETE FROM `quest_template_addon` WHERE `ID`=39321;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39321, 38910);
