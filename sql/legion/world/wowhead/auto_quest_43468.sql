UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98102);

DELETE FROM `creature_queststarter` WHERE `quest`=43468;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98102, 43468);

DELETE FROM `creature_questender` WHERE `quest`=43468;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98102, 43468);

DELETE FROM `quest_template_addon` WHERE `ID`=43468;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43468, 43253);
