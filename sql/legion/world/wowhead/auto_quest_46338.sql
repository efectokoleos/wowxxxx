UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108247);

DELETE FROM `creature_queststarter` WHERE `quest`=46338;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108247, 46338);

DELETE FROM `creature_questender` WHERE `quest`=46338;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118242, 46338);

DELETE FROM `quest_template_addon` WHERE `ID`=46338;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46338, 46335, 45207);
