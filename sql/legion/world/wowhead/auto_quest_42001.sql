UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102700);

DELETE FROM `creature_queststarter` WHERE `quest`=42001;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102700, 42001);

DELETE FROM `creature_questender` WHERE `quest`=42001;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105081, 42001);

DELETE FROM `quest_template_addon` WHERE `ID`=42001;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42001, 42006);
