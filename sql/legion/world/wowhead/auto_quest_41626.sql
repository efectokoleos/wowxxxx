UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105081);

DELETE FROM `creature_queststarter` WHERE `quest`=41626;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105081, 41626);

DELETE FROM `gameobject_questender` WHERE `quest`=41626;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(248398, 41626);

DELETE FROM `quest_template_addon` WHERE `ID`=41626;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41626, 41625, 41627);
