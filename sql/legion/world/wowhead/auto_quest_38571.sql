UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92401);

DELETE FROM `creature_queststarter` WHERE `quest`=38571;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92401, 38571);

DELETE FROM `creature_questender` WHERE `quest`=38571;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91242, 38571);

DELETE FROM `quest_template_addon` WHERE `ID`=38571;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38571, 38570, 38572);
