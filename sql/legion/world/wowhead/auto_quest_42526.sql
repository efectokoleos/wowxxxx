UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103023);

DELETE FROM `creature_queststarter` WHERE `quest`=42526;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103023, 42526);

DELETE FROM `creature_questender` WHERE `quest`=42526;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103023, 42526);

DELETE FROM `quest_template_addon` WHERE `ID`=42526;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42526, 42525, 42384);
