DELETE FROM `creature_questender` WHERE `quest`=39385;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93603, 39385);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39385;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(243454, 39385);

DELETE FROM `quest_template_addon` WHERE `ID`=39385;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39385, 38872, 39154);
