UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93823);

DELETE FROM `creature_queststarter` WHERE `quest`=39654;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93823, 39654);

DELETE FROM `creature_questender` WHERE `quest`=39654;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96469, 39654);

DELETE FROM `quest_template_addon` WHERE `ID`=39654;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39654, 38904);
