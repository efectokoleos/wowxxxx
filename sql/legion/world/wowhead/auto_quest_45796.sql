UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117361);

DELETE FROM `creature_queststarter` WHERE `quest`=45796;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117361, 45796);

DELETE FROM `creature_questender` WHERE `quest`=45796;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117361, 45796);

DELETE FROM `quest_template_addon` WHERE `ID`=45796;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45796, 45587);
