DELETE FROM `creature_questender` WHERE `quest`=37945;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(81492, 37945);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37945;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37945);

DELETE FROM `quest_template_addon` WHERE `ID`=37945;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37945, 37944);
