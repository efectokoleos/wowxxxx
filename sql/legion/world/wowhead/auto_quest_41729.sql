UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104745);

DELETE FROM `creature_queststarter` WHERE `quest`=41729;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104745, 41729);

DELETE FROM `creature_questender` WHERE `quest`=41729;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104850, 41729);

DELETE FROM `quest_template_addon` WHERE `ID`=41729;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41729, 41728, 41732);
