UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98721);

DELETE FROM `creature_queststarter` WHERE `quest`=40149;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98721, 40149);

DELETE FROM `creature_questender` WHERE `quest`=40149;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98721, 40149);

DELETE FROM `quest_template_addon` WHERE `ID`=40149;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40149, 40147);
