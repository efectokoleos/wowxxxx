UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109384);

DELETE FROM `creature_queststarter` WHERE `quest`=43462;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109384, 43462);

DELETE FROM `creature_questender` WHERE `quest`=43462;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109434, 43462);

DELETE FROM `quest_template_addon` WHERE `ID`=43462;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43462, 42886, 42890);
