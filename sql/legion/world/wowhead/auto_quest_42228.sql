UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107126);

DELETE FROM `creature_queststarter` WHERE `quest`=42228;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107126, 42228);

DELETE FROM `creature_questender` WHERE `quest`=42228;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108101, 42228);

DELETE FROM `quest_template_addon` WHERE `ID`=42228;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42228, 42227, 42230);
