UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(121358);

DELETE FROM `creature_queststarter` WHERE `quest`=46345;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(121358, 46345);

DELETE FROM `creature_questender` WHERE `quest`=46345;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116321, 46345);

DELETE FROM `quest_template_addon` WHERE `ID`=46345;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46345, 46339, 44768);
