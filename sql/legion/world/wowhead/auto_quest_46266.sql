UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117709);

DELETE FROM `creature_queststarter` WHERE `quest`=46266;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117709, 46266);

DELETE FROM `creature_questender` WHERE `quest`=46266;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108311, 46266);

DELETE FROM `quest_template_addon` WHERE `ID`=46266;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46266, 45798);
