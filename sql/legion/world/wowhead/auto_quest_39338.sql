UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93974);

DELETE FROM `creature_queststarter` WHERE `quest`=39338;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93974, 39338);

DELETE FROM `creature_questender` WHERE `quest`=39338;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92458, 39338);

DELETE FROM `quest_template_addon` WHERE `ID`=39338;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39338, 44112);
