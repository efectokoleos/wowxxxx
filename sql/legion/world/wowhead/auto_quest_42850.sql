UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=42850;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 42850);

DELETE FROM `creature_questender` WHERE `quest`=42850;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90250, 42850);

DELETE FROM `quest_template_addon` WHERE `ID`=42850;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42850, 42849);
