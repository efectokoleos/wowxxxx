UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=45770;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 45770);

DELETE FROM `creature_questender` WHERE `quest`=45770;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90250, 45770);

DELETE FROM `quest_template_addon` WHERE `ID`=45770;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45770, 46074);
