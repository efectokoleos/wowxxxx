UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96254);

DELETE FROM `creature_queststarter` WHERE `quest`=38331;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96254, 38331);

DELETE FROM `creature_questender` WHERE `quest`=38331;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96254, 38331);

DELETE FROM `quest_template_addon` WHERE `ID`=38331;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38331, 39590);
