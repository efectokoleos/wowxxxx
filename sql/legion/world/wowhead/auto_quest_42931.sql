UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96539);

DELETE FROM `creature_queststarter` WHERE `quest`=42931;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96539, 42931);

DELETE FROM `creature_questender` WHERE `quest`=42931;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96527, 42931);

DELETE FROM `quest_template_addon` WHERE `ID`=42931;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42931, 42932);
