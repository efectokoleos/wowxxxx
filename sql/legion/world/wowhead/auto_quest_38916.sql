UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98825);

DELETE FROM `creature_queststarter` WHERE `quest`=38916;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98825, 38916);

DELETE FROM `creature_questender` WHERE `quest`=38916;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94571, 38916);

DELETE FROM `quest_template_addon` WHERE `ID`=38916;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38916, 39575);
