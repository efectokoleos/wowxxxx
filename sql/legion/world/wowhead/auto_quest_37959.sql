UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90383);

DELETE FROM `creature_queststarter` WHERE `quest`=37959;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90383, 37959);

DELETE FROM `creature_questender` WHERE `quest`=37959;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90383, 37959);

DELETE FROM `quest_template_addon` WHERE `ID`=37959;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37959, 37860);
