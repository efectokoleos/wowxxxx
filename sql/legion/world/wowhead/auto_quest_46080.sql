UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118954);

DELETE FROM `creature_queststarter` WHERE `quest`=46080;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118954, 46080);

DELETE FROM `creature_questender` WHERE `quest`=46080;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118954, 46080);

DELETE FROM `quest_template_addon` WHERE `ID`=46080;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46080, 46078, 46082);
