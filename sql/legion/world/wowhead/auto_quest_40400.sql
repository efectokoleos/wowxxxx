UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100190);

DELETE FROM `creature_queststarter` WHERE `quest`=40400;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100190, 40400);

DELETE FROM `creature_questender` WHERE `quest`=40400;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100190, 40400);

DELETE FROM `quest_template_addon` WHERE `ID`=40400;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40400, 40419);
