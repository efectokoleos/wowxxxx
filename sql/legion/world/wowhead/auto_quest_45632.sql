UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116858);

DELETE FROM `creature_queststarter` WHERE `quest`=45632;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116858, 45632);

DELETE FROM `creature_questender` WHERE `quest`=45632;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117471, 45632);

DELETE FROM `quest_template_addon` WHERE `ID`=45632;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45632, 45647);
