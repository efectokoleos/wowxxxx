UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=41728;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 41728);

DELETE FROM `creature_questender` WHERE `quest`=41728;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104784, 41728);

DELETE FROM `quest_template_addon` WHERE `ID`=41728;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41728, 41729);
