UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98100);

DELETE FROM `creature_queststarter` WHERE `quest`=37494;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98100, 37494);

DELETE FROM `creature_questender` WHERE `quest`=37494;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111049, 37494);

DELETE FROM `quest_template_addon` WHERE `ID`=37494;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37494, 37448);
