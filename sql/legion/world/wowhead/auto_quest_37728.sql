UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89341);

DELETE FROM `creature_queststarter` WHERE `quest`=37728;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89341, 37728);

DELETE FROM `creature_questender` WHERE `quest`=37728;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89341, 37728);

DELETE FROM `quest_template_addon` WHERE `ID`=37728;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37728, 37727);
