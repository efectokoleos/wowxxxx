UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101061);

DELETE FROM `creature_queststarter` WHERE `quest`=40643;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101061, 40643);

DELETE FROM `creature_questender` WHERE `quest`=40643;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101064, 40643);

DELETE FROM `quest_template_addon` WHERE `ID`=40643;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40643, 41106);
