UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116256);

DELETE FROM `creature_queststarter` WHERE `quest`=44827;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116256, 44827);

DELETE FROM `creature_questender` WHERE `quest`=44827;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116256, 44827);

DELETE FROM `quest_template_addon` WHERE `ID`=44827;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44827, 44861);
