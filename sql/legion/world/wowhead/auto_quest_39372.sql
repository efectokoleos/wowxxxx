UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95256);

DELETE FROM `creature_queststarter` WHERE `quest`=39372;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95256, 39372);

DELETE FROM `creature_questender` WHERE `quest`=39372;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95256, 39372);

DELETE FROM `quest_template_addon` WHERE `ID`=39372;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39372, 38912, 39374);
