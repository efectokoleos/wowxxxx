DELETE FROM `creature_questender` WHERE `quest`=37565;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89051, 37565);

DELETE FROM `gameobject_queststarter` WHERE `quest`=37565;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239120, 37565);

DELETE FROM `quest_template_addon` WHERE `ID`=37565;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37565, 37566);
