UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103023);

DELETE FROM `creature_queststarter` WHERE `quest`=42134;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103023, 42134);

DELETE FROM `creature_questender` WHERE `quest`=42134;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106446, 42134);
