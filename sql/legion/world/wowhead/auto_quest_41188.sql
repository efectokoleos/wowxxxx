UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103485);

DELETE FROM `creature_queststarter` WHERE `quest`=41188;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103485, 41188);

DELETE FROM `creature_questender` WHERE `quest`=41188;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103485, 41188);

DELETE FROM `quest_template_addon` WHERE `ID`=41188;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41188, 41187);
