UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94429);

DELETE FROM `creature_queststarter` WHERE `quest`=39241;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94429, 39241);

DELETE FROM `creature_questender` WHERE `quest`=39241;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94429, 39241);

UPDATE `quest_template_addon` SET `PrevQuestID`=39236, `NextQuestID`=39242 WHERE `ID`=39241;
