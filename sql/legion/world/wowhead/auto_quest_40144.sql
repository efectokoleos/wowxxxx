UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93541);

DELETE FROM `creature_queststarter` WHERE `quest`=40144;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93541, 40144);

DELETE FROM `creature_questender` WHERE `quest`=40144;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98791, 40144);

DELETE FROM `quest_template_addon` WHERE `ID`=40144;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40144, 40145);
