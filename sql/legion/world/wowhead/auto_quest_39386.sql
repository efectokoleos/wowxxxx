UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94409);

DELETE FROM `creature_queststarter` WHERE `quest`=39386;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94409, 39386);

DELETE FROM `creature_questender` WHERE `quest`=39386;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96513, 39386);

DELETE FROM `quest_template_addon` WHERE `ID`=39386;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39386, 39670);
