UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101195);

DELETE FROM `creature_queststarter` WHERE `quest`=42428;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101195, 42428);

DELETE FROM `creature_questender` WHERE `quest`=42428;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107392, 42428);

DELETE FROM `quest_template_addon` WHERE `ID`=42428;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42428, 42439);
