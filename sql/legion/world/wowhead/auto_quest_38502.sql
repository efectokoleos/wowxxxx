UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97261);

DELETE FROM `creature_queststarter` WHERE `quest`=38502;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97261, 38502);

DELETE FROM `creature_questender` WHERE `quest`=38502;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 38502);

DELETE FROM `quest_template_addon` WHERE `ID`=38502;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38502, 38505);
