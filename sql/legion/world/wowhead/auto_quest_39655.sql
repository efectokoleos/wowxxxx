UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93822);

DELETE FROM `creature_queststarter` WHERE `quest`=39655;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93822, 39655);

DELETE FROM `creature_questender` WHERE `quest`=39655;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93822, 39655);

DELETE FROM `quest_template_addon` WHERE `ID`=39655;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39655, 39404, 39675);
