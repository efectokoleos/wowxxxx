UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117262);

DELETE FROM `creature_queststarter` WHERE `quest`=45628;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117262, 45628);

DELETE FROM `creature_questender` WHERE `quest`=45628;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117259, 45628);

DELETE FROM `quest_template_addon` WHERE `ID`=45628;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45628, 46260);
