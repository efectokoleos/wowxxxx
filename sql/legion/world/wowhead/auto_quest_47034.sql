UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116302);

DELETE FROM `creature_queststarter` WHERE `quest`=47034;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116302, 47034);

DELETE FROM `creature_questender` WHERE `quest`=47034;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90463, 47034);

DELETE FROM `quest_template_addon` WHERE `ID`=47034;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(47034, 45482);
