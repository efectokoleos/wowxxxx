UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100776);

DELETE FROM `creature_queststarter` WHERE `quest`=40536;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100776, 40536);

DELETE FROM `creature_questender` WHERE `quest`=40536;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100776, 40536);

DELETE FROM `quest_template_addon` WHERE `ID`=40536;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40536, 40535);
