UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98632);

DELETE FROM `creature_queststarter` WHERE `quest`=42131;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98632, 42131);

DELETE FROM `creature_questender` WHERE `quest`=42131;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108782, 42131);

DELETE FROM `quest_template_addon` WHERE `ID`=42131;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42131, 42731);
