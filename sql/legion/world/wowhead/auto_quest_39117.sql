UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94117);

DELETE FROM `creature_queststarter` WHERE `quest`=39117;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94117, 39117);

DELETE FROM `creature_questender` WHERE `quest`=39117;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94117, 39117);
