UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98100);

DELETE FROM `creature_queststarter` WHERE `quest`=43470;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98100, 43470);

DELETE FROM `creature_questender` WHERE `quest`=43470;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98100, 43470);

DELETE FROM `quest_template_addon` WHERE `ID`=43470;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43470, 43469);
