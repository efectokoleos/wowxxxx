UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93531);

DELETE FROM `creature_queststarter` WHERE `quest`=39920;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93531, 39920);

DELETE FROM `creature_questender` WHERE `quest`=39920;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99420, 39920);

DELETE FROM `quest_template_addon` WHERE `ID`=39920;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39920, 39907, 39921);
