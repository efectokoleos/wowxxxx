UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(109698);

DELETE FROM `creature_queststarter` WHERE `quest`=43153;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(109698, 43153);

DELETE FROM `creature_questender` WHERE `quest`=43153;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109838, 43153);

DELETE FROM `quest_template_addon` WHERE `ID`=43153;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43153, 43100);
