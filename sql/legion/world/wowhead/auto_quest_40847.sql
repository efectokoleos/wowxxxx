UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94159);

DELETE FROM `creature_queststarter` WHERE `quest`=40847;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94159, 40847);

DELETE FROM `creature_questender` WHERE `quest`=40847;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102120, 40847);
