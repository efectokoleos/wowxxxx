UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=45261;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 45261);

DELETE FROM `creature_questender` WHERE `quest`=45261;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 45261);

DELETE FROM `quest_template_addon` WHERE `ID`=45261;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45261, 45272);
