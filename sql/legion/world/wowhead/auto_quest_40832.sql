UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102018);

DELETE FROM `creature_queststarter` WHERE `quest`=40832;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102018, 40832);

DELETE FROM `creature_questender` WHERE `quest`=40832;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101513, 40832);

DELETE FROM `quest_template_addon` WHERE `ID`=40832;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40832, 40839);
