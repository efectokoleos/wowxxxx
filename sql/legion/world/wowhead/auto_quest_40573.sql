UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91650);

DELETE FROM `creature_queststarter` WHERE `quest`=40573;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91650, 40573);

DELETE FROM `creature_questender` WHERE `quest`=40573;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100573, 40573);

DELETE FROM `quest_template_addon` WHERE `ID`=40573;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40573, 38148);
