UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108515);

DELETE FROM `creature_queststarter` WHERE `quest`=42451;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108515, 42451);

DELETE FROM `creature_questender` WHERE `quest`=42451;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107465, 42451);

DELETE FROM `quest_template_addon` WHERE `ID`=42451;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42451, 42424, 42508);
