UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100190);

DELETE FROM `creature_queststarter` WHERE `quest`=40402;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100190, 40402);

DELETE FROM `creature_questender` WHERE `quest`=40402;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100190, 40402);

DELETE FROM `quest_template_addon` WHERE `ID`=40402;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40402, 40419);
