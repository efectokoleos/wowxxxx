UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102390);

DELETE FROM `creature_queststarter` WHERE `quest`=41426;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102390, 41426);

DELETE FROM `creature_questender` WHERE `quest`=41426;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102410, 41426);

DELETE FROM `quest_template_addon` WHERE `ID`=41426;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41426, 40927);
