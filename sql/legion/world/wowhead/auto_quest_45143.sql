UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90249);

DELETE FROM `creature_queststarter` WHERE `quest`=45143;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90249, 45143);

DELETE FROM `creature_questender` WHERE `quest`=45143;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109102, 45143);
