UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102585);

DELETE FROM `creature_queststarter` WHERE `quest`=44463;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102585, 44463);

DELETE FROM `creature_questender` WHERE `quest`=44463;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100973, 44463);

DELETE FROM `quest_template_addon` WHERE `ID`=44463;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44463, 40760, 40605);
