UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118105);

DELETE FROM `creature_queststarter` WHERE `quest`=46317;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118105, 46317);

DELETE FROM `creature_questender` WHERE `quest`=46317;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106299, 46317);

DELETE FROM `quest_template_addon` WHERE `ID`=46317;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46317, 46318);
