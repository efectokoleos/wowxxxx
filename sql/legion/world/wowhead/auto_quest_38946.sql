UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93542);

DELETE FROM `creature_queststarter` WHERE `quest`=38946;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93542, 38946);

DELETE FROM `creature_questender` WHERE `quest`=38946;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93967, 38946);

DELETE FROM `quest_template_addon` WHERE `ID`=38946;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38946, 38945);
