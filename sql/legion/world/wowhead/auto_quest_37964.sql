UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(83823);

DELETE FROM `creature_queststarter` WHERE `quest`=37964;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(83823, 37964);

DELETE FROM `creature_questender` WHERE `quest`=37964;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90233, 37964);

DELETE FROM `quest_template_addon` WHERE `ID`=37964;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37964, 37836, 37837);
