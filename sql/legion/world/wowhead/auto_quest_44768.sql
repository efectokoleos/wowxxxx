UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116321);

DELETE FROM `creature_queststarter` WHERE `quest`=44768;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116321, 44768);

DELETE FROM `creature_questender` WHERE `quest`=44768;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116321, 44768);

DELETE FROM `quest_template_addon` WHERE `ID`=44768;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44768, 46345);
