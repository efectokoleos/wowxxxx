UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105120);

DELETE FROM `creature_queststarter` WHERE `quest`=41934;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105120, 41934);

DELETE FROM `creature_questender` WHERE `quest`=41934;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105120, 41934);

DELETE FROM `quest_template_addon` WHERE `ID`=41934;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41934, 41773, 41888);
