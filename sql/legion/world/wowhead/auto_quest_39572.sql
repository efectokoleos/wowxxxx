UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95130);

DELETE FROM `creature_queststarter` WHERE `quest`=39572;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95130, 39572);

DELETE FROM `creature_questender` WHERE `quest`=39572;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96084, 39572);

DELETE FROM `quest_template_addon` WHERE `ID`=39572;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39572, 39323);
