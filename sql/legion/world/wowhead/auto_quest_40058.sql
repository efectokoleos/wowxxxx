UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89398);

DELETE FROM `creature_queststarter` WHERE `quest`=40058;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89398, 40058);

DELETE FROM `creature_questender` WHERE `quest`=40058;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97736, 40058);

DELETE FROM `quest_template_addon` WHERE `ID`=40058;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40058, 39937);
