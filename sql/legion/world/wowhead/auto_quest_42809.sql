UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98646);

DELETE FROM `creature_queststarter` WHERE `quest`=42809;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98646, 42809);

DELETE FROM `creature_questender` WHERE `quest`=42809;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108571, 42809);

DELETE FROM `quest_template_addon` WHERE `ID`=42809;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42809, 42132);
