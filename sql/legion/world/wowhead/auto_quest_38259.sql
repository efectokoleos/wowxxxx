UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92219);

DELETE FROM `creature_queststarter` WHERE `quest`=38259;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92219, 38259);

DELETE FROM `creature_questender` WHERE `quest`=38259;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91242, 38259);

DELETE FROM `quest_template_addon` WHERE `ID`=38259;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38259, 38258);
