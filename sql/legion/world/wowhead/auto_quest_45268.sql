UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=45268;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 45268);

DELETE FROM `creature_questender` WHERE `quest`=45268;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 45268);

DELETE FROM `quest_template_addon` WHERE `ID`=45268;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45268, 45285, 44918);
