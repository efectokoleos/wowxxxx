UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91923);

DELETE FROM `creature_queststarter` WHERE `quest`=38444;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91923, 38444);

DELETE FROM `creature_questender` WHERE `quest`=38444;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91923, 38444);

DELETE FROM `quest_template_addon` WHERE `ID`=38444;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38444, 38436, 38445);
