UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95002);

DELETE FROM `creature_queststarter` WHERE `quest`=39055;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95002, 39055);

DELETE FROM `creature_questender` WHERE `quest`=39055;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95002, 39055);

DELETE FROM `quest_template_addon` WHERE `ID`=39055;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39055, 39276, 38435);
