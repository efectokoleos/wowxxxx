UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=42762;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 42762);

DELETE FROM `creature_questender` WHERE `quest`=42762;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108700, 42762);

DELETE FROM `quest_template_addon` WHERE `ID`=42762;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42762, 42766);
