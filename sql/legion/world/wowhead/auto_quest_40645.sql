UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103875);

DELETE FROM `creature_queststarter` WHERE `quest`=40645;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103875, 40645);

DELETE FROM `creature_questender` WHERE `quest`=40645;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101195, 40645);

DELETE FROM `quest_template_addon` WHERE `ID`=40645;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40645, 40644, 40646);
