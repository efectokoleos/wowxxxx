UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=41905;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 41905);

DELETE FROM `creature_questender` WHERE `quest`=41905;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 41905);

DELETE FROM `quest_template_addon` WHERE `ID`=41905;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41905, 42191);
