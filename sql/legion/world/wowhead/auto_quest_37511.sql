UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(83734);

DELETE FROM `creature_queststarter` WHERE `quest`=37511;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(83734, 37511);

DELETE FROM `creature_questender` WHERE `quest`=37511;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(76508, 37511);

DELETE FROM `quest_template_addon` WHERE `ID`=37511;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37511, 35972, 35973);
