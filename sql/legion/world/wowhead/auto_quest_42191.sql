UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99179);

DELETE FROM `creature_queststarter` WHERE `quest`=42191;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99179, 42191);

DELETE FROM `creature_questender` WHERE `quest`=42191;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 42191);

DELETE FROM `quest_template_addon` WHERE `ID`=42191;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42191, 41905);
