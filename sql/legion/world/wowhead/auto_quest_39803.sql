UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97074);

DELETE FROM `creature_queststarter` WHERE `quest`=39803;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97074, 39803);

DELETE FROM `creature_questender` WHERE `quest`=39803;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92539, 39803);

DELETE FROM `quest_template_addon` WHERE `ID`=39803;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39803, 38624, 39804);
