UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90481);

DELETE FROM `creature_queststarter` WHERE `quest`=38453;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90481, 38453);

DELETE FROM `quest_template_addon` WHERE `ID`=38453;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38453, 38270);
