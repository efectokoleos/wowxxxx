UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88115);

DELETE FROM `creature_queststarter` WHERE `quest`=37497;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88115, 37497);

DELETE FROM `creature_questender` WHERE `quest`=37497;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88867, 37497);

DELETE FROM `quest_template_addon` WHERE `ID`=37497;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37497, 37486);
