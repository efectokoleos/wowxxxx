UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118183);

DELETE FROM `creature_queststarter` WHERE `quest`=44789;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118183, 44789);

DELETE FROM `creature_questender` WHERE `quest`=44789;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118250, 44789);

DELETE FROM `quest_template_addon` WHERE `ID`=44789;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44789, 45812, 45856);
