UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95956);

DELETE FROM `creature_queststarter` WHERE `quest`=39277;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95956, 39277);

DELETE FROM `creature_questender` WHERE `quest`=39277;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95956, 39277);

DELETE FROM `quest_template_addon` WHERE `ID`=39277;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39277, 39487);
