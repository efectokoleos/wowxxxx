UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106656);

DELETE FROM `creature_queststarter` WHERE `quest`=42171;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106656, 42171);

DELETE FROM `creature_questender` WHERE `quest`=42171;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106530, 42171);

DELETE FROM `quest_template_addon` WHERE `ID`=42171;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42171, 42222);
