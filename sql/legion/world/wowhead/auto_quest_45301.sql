UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108311);

DELETE FROM `creature_queststarter` WHERE `quest`=45301;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108311, 45301);

DELETE FROM `creature_questender` WHERE `quest`=45301;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116448, 45301);

DELETE FROM `quest_template_addon` WHERE `ID`=45301;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45301, 45330);
