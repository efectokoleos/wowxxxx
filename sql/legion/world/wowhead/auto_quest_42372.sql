UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106915);

DELETE FROM `creature_queststarter` WHERE `quest`=42372;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106915, 42372);

DELETE FROM `creature_questender` WHERE `quest`=42372;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107244, 42372);

DELETE FROM `quest_template_addon` WHERE `ID`=42372;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42372, 42375);
