UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91473);

DELETE FROM `creature_queststarter` WHERE `quest`=38332;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91473, 38332);

DELETE FROM `creature_questender` WHERE `quest`=38332;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91158, 38332);

DELETE FROM `quest_template_addon` WHERE `ID`=38332;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38332, 38360);
