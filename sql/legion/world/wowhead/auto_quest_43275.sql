UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110564);

DELETE FROM `creature_queststarter` WHERE `quest`=43275;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110564, 43275);

DELETE FROM `creature_questender` WHERE `quest`=43275;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110564, 43275);

DELETE FROM `quest_template_addon` WHERE `ID`=43275;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43275, 43273);
