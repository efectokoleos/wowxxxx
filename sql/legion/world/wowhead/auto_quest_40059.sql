UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97736);

DELETE FROM `creature_queststarter` WHERE `quest`=40059;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97736, 40059);

DELETE FROM `creature_questender` WHERE `quest`=40059;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97736, 40059);

DELETE FROM `quest_template_addon` WHERE `ID`=40059;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40059, 39936, 39938);
