UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93542);

DELETE FROM `creature_queststarter` WHERE `quest`=38956;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93542, 38956);

DELETE FROM `creature_questender` WHERE `quest`=38956;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93967, 38956);

DELETE FROM `quest_template_addon` WHERE `ID`=38956;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38956, 38957);
