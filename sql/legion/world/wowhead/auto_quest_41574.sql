UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104381);

DELETE FROM `creature_queststarter` WHERE `quest`=41574;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104381, 41574);

DELETE FROM `creature_questender` WHERE `quest`=41574;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106558, 41574);

DELETE FROM `quest_template_addon` WHERE `ID`=41574;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41574, 41541);
