UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99420);

DELETE FROM `creature_queststarter` WHERE `quest`=39921;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99420, 39921);

DELETE FROM `creature_questender` WHERE `quest`=39921;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99420, 39921);

DELETE FROM `quest_template_addon` WHERE `ID`=39921;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39921, 39920);
