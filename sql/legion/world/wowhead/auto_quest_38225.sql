UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91066);

DELETE FROM `creature_queststarter` WHERE `quest`=38225;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91066, 38225);

DELETE FROM `creature_questender` WHERE `quest`=38225;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91066, 38225);

DELETE FROM `quest_template_addon` WHERE `ID`=38225;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38225, 38219);
