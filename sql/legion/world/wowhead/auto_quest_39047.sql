UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99343);

DELETE FROM `creature_queststarter` WHERE `quest`=39047;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99343, 39047);

DELETE FROM `creature_questender` WHERE `quest`=39047;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99254, 39047);
