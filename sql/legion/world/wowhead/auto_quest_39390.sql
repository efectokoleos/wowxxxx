DELETE FROM `creature_questender` WHERE `quest`=39390;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92458, 39390);

DELETE FROM `gameobject_queststarter` WHERE `quest`=39390;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(243392, 39390);

DELETE FROM `quest_template_addon` WHERE `ID`=39390;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39390, 39566, 39327);
