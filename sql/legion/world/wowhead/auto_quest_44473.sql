UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100973);

DELETE FROM `creature_queststarter` WHERE `quest`=44473;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100973, 44473);

DELETE FROM `creature_questender` WHERE `quest`=44473;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101004, 44473);

DELETE FROM `quest_template_addon` WHERE `ID`=44473;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44473, 40593, 44663);
