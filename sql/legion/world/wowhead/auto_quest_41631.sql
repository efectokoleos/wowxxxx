UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105917);

DELETE FROM `creature_queststarter` WHERE `quest`=41631;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105917, 41631);

DELETE FROM `creature_questender` WHERE `quest`=41631;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105081, 41631);

DELETE FROM `quest_template_addon` WHERE `ID`=41631;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41631, 41630, 41632);
