UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98102);

DELETE FROM `creature_queststarter` WHERE `quest`=42684;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98102, 42684);

DELETE FROM `creature_questender` WHERE `quest`=42684;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98102, 42684);

DELETE FROM `quest_template_addon` WHERE `ID`=42684;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42684, 43253);
