UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=41849;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 41849);

DELETE FROM `creature_questender` WHERE `quest`=41849;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105045, 41849);

DELETE FROM `quest_template_addon` WHERE `ID`=41849;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41849, 41850);
