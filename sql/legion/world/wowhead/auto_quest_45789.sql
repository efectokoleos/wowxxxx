UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102709);

DELETE FROM `creature_queststarter` WHERE `quest`=45789;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102709, 45789);

DELETE FROM `creature_questender` WHERE `quest`=45789;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102709, 45789);

DELETE FROM `quest_template_addon` WHERE `ID`=45789;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45789, 45788);
