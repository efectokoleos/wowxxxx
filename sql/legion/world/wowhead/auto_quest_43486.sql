UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90251);

DELETE FROM `creature_queststarter` WHERE `quest`=43486;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90251, 43486);

DELETE FROM `creature_questender` WHERE `quest`=43486;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110571, 43486);

DELETE FROM `quest_template_addon` WHERE `ID`=43486;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(43486, 43487);
