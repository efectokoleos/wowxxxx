UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93826);

DELETE FROM `creature_queststarter` WHERE `quest`=38912;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93826, 38912);

DELETE FROM `creature_questender` WHERE `quest`=38912;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95256, 38912);

DELETE FROM `quest_template_addon` WHERE `ID`=38912;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38912, 38909, 39372);
