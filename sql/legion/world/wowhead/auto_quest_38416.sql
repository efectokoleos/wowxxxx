UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91750);

DELETE FROM `creature_queststarter` WHERE `quest`=38416;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91750, 38416);

DELETE FROM `creature_questender` WHERE `quest`=38416;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91750, 38416);

DELETE FROM `quest_template_addon` WHERE `ID`=38416;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38416, 38415);
