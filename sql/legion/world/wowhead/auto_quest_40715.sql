UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101441);

DELETE FROM `creature_queststarter` WHERE `quest`=40715;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101441, 40715);

DELETE FROM `creature_questender` WHERE `quest`=40715;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101441, 40715);

DELETE FROM `quest_template_addon` WHERE `ID`=40715;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40715, 40714);
