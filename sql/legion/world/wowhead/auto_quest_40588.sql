UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100323);

DELETE FROM `creature_queststarter` WHERE `quest`=40588;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100323, 40588);

DELETE FROM `creature_questender` WHERE `quest`=40588;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100729, 40588);

DELETE FROM `quest_template_addon` WHERE `ID`=40588;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40588, 40495, 40604);
