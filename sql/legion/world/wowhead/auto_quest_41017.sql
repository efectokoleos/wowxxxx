UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102709);

DELETE FROM `creature_queststarter` WHERE `quest`=41017;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102709, 41017);

DELETE FROM `creature_questender` WHERE `quest`=41017;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102709, 41017);

DELETE FROM `quest_template_addon` WHERE `ID`=41017;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41017, 41015);
