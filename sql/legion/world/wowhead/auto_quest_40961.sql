UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=40961;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 40961);

DELETE FROM `creature_questender` WHERE `quest`=40961;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95844, 40961);

DELETE FROM `quest_template_addon` WHERE `ID`=40961;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40961, 40960);
