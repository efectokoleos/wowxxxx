UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98102);

DELETE FROM `creature_queststarter` WHERE `quest`=43249;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98102, 43249);

DELETE FROM `creature_questender` WHERE `quest`=43249;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98102, 43249);

DELETE FROM `quest_template_addon` WHERE `ID`=43249;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43249, 43253, 43250);
