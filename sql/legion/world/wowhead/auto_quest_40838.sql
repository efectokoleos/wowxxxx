UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100812);

DELETE FROM `creature_queststarter` WHERE `quest`=40838;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100812, 40838);

DELETE FROM `creature_questender` WHERE `quest`=40838;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100812, 40838);

DELETE FROM `quest_template_addon` WHERE `ID`=40838;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40838, 40837, 40900);
