UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103761);

DELETE FROM `creature_queststarter` WHERE `quest`=37447;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103761, 37447);

DELETE FROM `creature_questender` WHERE `quest`=37447;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107723, 37447);

DELETE FROM `quest_template_addon` WHERE `ID`=37447;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37447, 42682, 42510);
