UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108515);

DELETE FROM `creature_queststarter` WHERE `quest`=42696;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108515, 42696);

DELETE FROM `creature_questender` WHERE `quest`=42696;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108515, 42696);

DELETE FROM `quest_template_addon` WHERE `ID`=42696;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42696, 42687);
