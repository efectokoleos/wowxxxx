UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114948);

DELETE FROM `creature_queststarter` WHERE `quest`=38692;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114948, 38692);

DELETE FROM `creature_questender` WHERE `quest`=38692;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115018, 38692);

DELETE FROM `quest_template_addon` WHERE `ID`=38692;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38692, 38694);
