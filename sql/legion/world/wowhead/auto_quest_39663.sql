UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93127);

DELETE FROM `creature_queststarter` WHERE `quest`=39663;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93127, 39663);

DELETE FROM `creature_questender` WHERE `quest`=39663;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97297, 39663);

DELETE FROM `quest_template_addon` WHERE `ID`=39663;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39663, 38727, 38728);
