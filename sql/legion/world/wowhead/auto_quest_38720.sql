UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114948);

DELETE FROM `creature_queststarter` WHERE `quest`=38720;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114948, 38720);

DELETE FROM `creature_questender` WHERE `quest`=38720;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115018, 38720);

DELETE FROM `quest_template_addon` WHERE `ID`=38720;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38720, 38694);
