UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93539);

DELETE FROM `creature_queststarter` WHERE `quest`=40545;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93539, 40545);

DELETE FROM `creature_questender` WHERE `quest`=40545;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102193, 40545);

DELETE FROM `quest_template_addon` WHERE `ID`=40545;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40545, 40854);
