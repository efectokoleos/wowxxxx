UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97748);

DELETE FROM `creature_queststarter` WHERE `quest`=39945;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97748, 39945);

DELETE FROM `creature_questender` WHERE `quest`=39945;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97748, 39945);

DELETE FROM `quest_template_addon` WHERE `ID`=39945;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39945, 39947);
