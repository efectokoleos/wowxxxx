UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116302);

DELETE FROM `creature_queststarter` WHERE `quest`=47076;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116302, 47076);

DELETE FROM `creature_questender` WHERE `quest`=47076;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(111740, 47076);

DELETE FROM `quest_template_addon` WHERE `ID`=47076;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(47076, 47067, 46940);
