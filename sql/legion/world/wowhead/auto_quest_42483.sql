UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107674);

DELETE FROM `creature_queststarter` WHERE `quest`=42483;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107674, 42483);

DELETE FROM `gameobject_questender` WHERE `quest`=42483;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(250612, 42483);
