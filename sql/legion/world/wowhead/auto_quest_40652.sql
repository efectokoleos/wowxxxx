UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104241);

DELETE FROM `creature_queststarter` WHERE `quest`=40652;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104241, 40652);

DELETE FROM `creature_questender` WHERE `quest`=40652;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98002, 40652);

DELETE FROM `quest_template_addon` WHERE `ID`=40652;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40652, 41332, 40653);
