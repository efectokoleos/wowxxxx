UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90866);

DELETE FROM `creature_queststarter` WHERE `quest`=38558;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90866, 38558);

DELETE FROM `creature_questender` WHERE `quest`=38558;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90866, 38558);

DELETE FROM `quest_template_addon` WHERE `ID`=38558;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38558, 38058);
