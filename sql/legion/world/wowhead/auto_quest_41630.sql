UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105917);

DELETE FROM `creature_queststarter` WHERE `quest`=41630;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105917, 41630);

DELETE FROM `quest_template_addon` WHERE `ID`=41630;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41630, 41629, 41631);
