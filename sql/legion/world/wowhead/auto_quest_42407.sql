UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108375);

DELETE FROM `creature_queststarter` WHERE `quest`=42407;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108375, 42407);

DELETE FROM `creature_questender` WHERE `quest`=42407;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107973, 42407);

DELETE FROM `quest_template_addon` WHERE `ID`=42407;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42407, 42402);
