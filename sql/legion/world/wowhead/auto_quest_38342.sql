UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91553);

DELETE FROM `creature_queststarter` WHERE `quest`=38342;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91553, 38342);

DELETE FROM `creature_questender` WHERE `quest`=38342;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91558, 38342);

DELETE FROM `quest_template_addon` WHERE `ID`=38342;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38342, 38410, 38412);
