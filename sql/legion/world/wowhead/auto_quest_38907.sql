UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97666);

DELETE FROM `creature_queststarter` WHERE `quest`=38907;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97666, 38907);

DELETE FROM `creature_questender` WHERE `quest`=38907;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93826, 38907);

DELETE FROM `quest_template_addon` WHERE `ID`=38907;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38907, 39733, 38911);
