UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(110793);

DELETE FROM `creature_queststarter` WHERE `quest`=37666;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(110793, 37666);

DELETE FROM `creature_questender` WHERE `quest`=37666;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(110953, 37666);

DELETE FROM `quest_template_addon` WHERE `ID`=37666;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37666, 43508, 37448);
