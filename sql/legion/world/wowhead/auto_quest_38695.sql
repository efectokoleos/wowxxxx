UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115710);

DELETE FROM `creature_queststarter` WHERE `quest`=38695;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115710, 38695);

DELETE FROM `creature_questender` WHERE `quest`=38695;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(114948, 38695);

DELETE FROM `quest_template_addon` WHERE `ID`=38695;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38695, 38649);
