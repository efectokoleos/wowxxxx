UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105903);

DELETE FROM `creature_queststarter` WHERE `quest`=42034;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105903, 42034);

DELETE FROM `creature_questender` WHERE `quest`=42034;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105948, 42034);

DELETE FROM `quest_template_addon` WHERE `ID`=42034;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42034, 42035);
