UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107139);

DELETE FROM `creature_queststarter` WHERE `quest`=39907;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107139, 39907);

DELETE FROM `creature_questender` WHERE `quest`=39907;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93531, 39907);

DELETE FROM `quest_template_addon` WHERE `ID`=39907;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39907, 39918, 39920);
