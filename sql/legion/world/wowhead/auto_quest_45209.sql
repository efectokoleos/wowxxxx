UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115506);

DELETE FROM `creature_queststarter` WHERE `quest`=45209;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115506, 45209);

DELETE FROM `creature_questender` WHERE `quest`=45209;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 45209);

DELETE FROM `quest_template_addon` WHERE `ID`=45209;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45209, 44832);
