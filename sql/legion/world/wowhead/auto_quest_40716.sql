UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103506);

DELETE FROM `creature_queststarter` WHERE `quest`=40716;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103506, 40716);

DELETE FROM `creature_questender` WHERE `quest`=40716;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101456, 40716);

DELETE FROM `quest_template_addon` WHERE `ID`=40716;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40716, 40685);
