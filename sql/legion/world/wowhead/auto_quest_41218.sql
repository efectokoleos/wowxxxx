UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103614);

DELETE FROM `creature_queststarter` WHERE `quest`=41218;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103614, 41218);

DELETE FROM `creature_questender` WHERE `quest`=41218;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(42317, 41218);

UPDATE `quest_template_addon` SET `PrevQuestID`=26197, `NextQuestID`=26208 WHERE `ID`=41218;
