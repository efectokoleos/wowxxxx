UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105081);

DELETE FROM `creature_queststarter` WHERE `quest`=44914;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105081, 44914);

DELETE FROM `creature_questender` WHERE `quest`=44914;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115791, 44914);

DELETE FROM `quest_template_addon` WHERE `ID`=44914;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44914, 47032);
