UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106519);

DELETE FROM `creature_queststarter` WHERE `quest`=45763;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106519, 45763);

DELETE FROM `creature_questender` WHERE `quest`=45763;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117715, 45763);

DELETE FROM `quest_template_addon` WHERE `ID`=45763;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45763, 45971);
