UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88867);

DELETE FROM `creature_queststarter` WHERE `quest`=37486;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88867, 37486);

DELETE FROM `creature_questender` WHERE `quest`=37486;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88867, 37486);

DELETE FROM `quest_template_addon` WHERE `ID`=37486;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37486, 37497, 37467);
