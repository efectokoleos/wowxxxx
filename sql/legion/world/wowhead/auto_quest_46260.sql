UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117259);

DELETE FROM `creature_queststarter` WHERE `quest`=46260;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117259, 46260);

DELETE FROM `creature_questender` WHERE `quest`=46260;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101513, 46260);

DELETE FROM `quest_template_addon` WHERE `ID`=46260;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46260, 45628);
