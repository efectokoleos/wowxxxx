UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96469);

DELETE FROM `creature_queststarter` WHERE `quest`=46207;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96469, 46207);

DELETE FROM `creature_questender` WHERE `quest`=46207;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96469, 46207);

DELETE FROM `quest_template_addon` WHERE `ID`=46207;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46207, 46208);
