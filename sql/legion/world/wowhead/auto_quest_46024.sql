UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118430);

DELETE FROM `creature_queststarter` WHERE `quest`=46024;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118430, 46024);

DELETE FROM `creature_questender` WHERE `quest`=46024;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99179, 46024);
