UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103023);

DELETE FROM `creature_queststarter` WHERE `quest`=42403;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103023, 42403);

DELETE FROM `creature_questender` WHERE `quest`=42403;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94409, 42403);
