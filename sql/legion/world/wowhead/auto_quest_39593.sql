DELETE FROM `gameobject_queststarter` WHERE `quest`=39593;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(243836, 39593);

DELETE FROM `gameobject_questender` WHERE `quest`=39593;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(243836, 39593);

DELETE FROM `quest_template_addon` WHERE `ID`=39593;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39593, 39594);
