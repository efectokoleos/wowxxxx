UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90417);

DELETE FROM `creature_queststarter` WHERE `quest`=44944;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90417, 44944);

DELETE FROM `creature_questender` WHERE `quest`=44944;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90417, 44944);

DELETE FROM `quest_template_addon` WHERE `ID`=44944;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44944, 44886, 44556);
