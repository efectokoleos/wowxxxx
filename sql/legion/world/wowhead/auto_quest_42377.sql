UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106337);

DELETE FROM `creature_queststarter` WHERE `quest`=42377;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106337, 42377);

DELETE FROM `creature_questender` WHERE `quest`=42377;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106337, 42377);

DELETE FROM `quest_template_addon` WHERE `ID`=42377;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42377, 42120);
