UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92464);

DELETE FROM `creature_queststarter` WHERE `quest`=40037;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92464, 40037);

DELETE FROM `creature_questender` WHERE `quest`=40037;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92464, 40037);

DELETE FROM `quest_template_addon` WHERE `ID`=40037;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40037, 40036);
