UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98156);

DELETE FROM `creature_queststarter` WHERE `quest`=39882;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98156, 39882);

DELETE FROM `creature_questender` WHERE `quest`=39882;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98156, 39882);

DELETE FROM `quest_template_addon` WHERE `ID`=39882;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39882, 39884);
