UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102700);

DELETE FROM `creature_queststarter` WHERE `quest`=41112;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102700, 41112);

DELETE FROM `creature_questender` WHERE `quest`=41112;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103092, 41112);

DELETE FROM `quest_template_addon` WHERE `ID`=41112;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41112, 41125, 41113);
