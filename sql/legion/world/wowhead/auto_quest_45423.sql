UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116781);

DELETE FROM `creature_queststarter` WHERE `quest`=45423;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116781, 45423);

DELETE FROM `creature_questender` WHERE `quest`=45423;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116781, 45423);

DELETE FROM `quest_template_addon` WHERE `ID`=45423;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45423, 45540);
