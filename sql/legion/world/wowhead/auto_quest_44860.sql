UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114841);

DELETE FROM `creature_queststarter` WHERE `quest`=44860;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114841, 44860);

DELETE FROM `creature_questender` WHERE `quest`=44860;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(114841, 44860);

DELETE FROM `quest_template_addon` WHERE `ID`=44860;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44860, 44743, 44861);
