UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115863);

DELETE FROM `creature_queststarter` WHERE `quest`=45024;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115863, 45024);

DELETE FROM `creature_questender` WHERE `quest`=45024;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115863, 45024);

DELETE FROM `quest_template_addon` WHERE `ID`=45024;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45024, 45021);
