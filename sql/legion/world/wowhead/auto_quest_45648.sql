UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116858);

DELETE FROM `creature_queststarter` WHERE `quest`=45648;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116858, 45648);

DELETE FROM `creature_questender` WHERE `quest`=45648;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117475, 45648);

DELETE FROM `quest_template_addon` WHERE `ID`=45648;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45648, 45649);
