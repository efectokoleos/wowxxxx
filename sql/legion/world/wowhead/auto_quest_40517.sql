UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100395);

DELETE FROM `creature_queststarter` WHERE `quest`=40517;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100395, 40517);

DELETE FROM `creature_questender` WHERE `quest`=40517;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100429, 40517);

DELETE FROM `quest_template_addon` WHERE `ID`=40517;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40517, 42740);
