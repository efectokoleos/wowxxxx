UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89975);

DELETE FROM `creature_queststarter` WHERE `quest`=37690;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89975, 37690);

DELETE FROM `creature_questender` WHERE `quest`=37690;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89023, 37690);

DELETE FROM `quest_template_addon` WHERE `ID`=37690;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(37690, 37256);
