UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103571);

DELETE FROM `creature_queststarter` WHERE `quest`=42223;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103571, 42223);

DELETE FROM `creature_questender` WHERE `quest`=42223;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100775, 42223);
