UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93822);

DELETE FROM `creature_queststarter` WHERE `quest`=39056;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93822, 39056);

DELETE FROM `creature_questender` WHERE `quest`=39056;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93822, 39056);

DELETE FROM `quest_template_addon` WHERE `ID`=39056;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39056, 39404);
