UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93974);

DELETE FROM `creature_queststarter` WHERE `quest`=44112;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93974, 44112);

DELETE FROM `creature_questender` WHERE `quest`=44112;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93974, 44112);

DELETE FROM `quest_template_addon` WHERE `ID`=44112;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44112, 39431, 39338);
