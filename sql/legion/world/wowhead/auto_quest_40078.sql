UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92539);

DELETE FROM `creature_queststarter` WHERE `quest`=40078;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92539, 40078);

DELETE FROM `creature_questender` WHERE `quest`=40078;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92539, 40078);

DELETE FROM `quest_template_addon` WHERE `ID`=40078;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40078, 40001);
