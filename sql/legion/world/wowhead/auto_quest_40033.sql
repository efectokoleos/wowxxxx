UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92464);

DELETE FROM `creature_queststarter` WHERE `quest`=40033;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92464, 40033);

DELETE FROM `quest_template_addon` WHERE `ID`=40033;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40033, 40032);
