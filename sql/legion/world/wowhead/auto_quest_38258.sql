UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91242);

DELETE FROM `creature_queststarter` WHERE `quest`=38258;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91242, 38258);

DELETE FROM `creature_questender` WHERE `quest`=38258;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92219, 38258);

DELETE FROM `quest_template_addon` WHERE `ID`=38258;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38258, 38256, 38259);
