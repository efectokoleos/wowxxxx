UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89660);

DELETE FROM `creature_queststarter` WHERE `quest`=37736;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89660, 37736);

DELETE FROM `creature_questender` WHERE `quest`=37736;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89660, 37736);

DELETE FROM `quest_template_addon` WHERE `ID`=37736;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37736, 37468, 37518);
