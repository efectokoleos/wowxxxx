UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(42465);

DELETE FROM `creature_queststarter` WHERE `quest`=42068;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(42465, 42068);

DELETE FROM `creature_questender` WHERE `quest`=42068;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105995, 42068);

DELETE FROM `quest_template_addon` WHERE `ID`=42068;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42068, 41775);
