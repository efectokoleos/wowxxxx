UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(105018);

DELETE FROM `creature_queststarter` WHERE `quest`=41767;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(105018, 41767);

DELETE FROM `creature_questender` WHERE `quest`=41767;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105018, 41767);

DELETE FROM `quest_template_addon` WHERE `ID`=41767;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41767, 40218);
