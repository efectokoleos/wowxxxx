UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98801);

DELETE FROM `creature_queststarter` WHERE `quest`=44489;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98801, 44489);

DELETE FROM `creature_questender` WHERE `quest`=44489;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99514, 44489);
