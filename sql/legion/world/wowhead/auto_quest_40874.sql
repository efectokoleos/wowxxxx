UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102196);

DELETE FROM `creature_queststarter` WHERE `quest`=40874;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102196, 40874);

DELETE FROM `creature_questender` WHERE `quest`=40874;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102196, 40874);

DELETE FROM `quest_template_addon` WHERE `ID`=40874;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40874, 40876);
