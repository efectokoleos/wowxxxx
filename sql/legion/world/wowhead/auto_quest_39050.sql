UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93759);

DELETE FROM `creature_queststarter` WHERE `quest`=39050;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93759, 39050);

DELETE FROM `creature_questender` WHERE `quest`=39050;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93759, 39050);

DELETE FROM `quest_template_addon` WHERE `ID`=39050;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39050, 38759);
