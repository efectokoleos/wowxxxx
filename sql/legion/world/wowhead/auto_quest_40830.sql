UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102142);

DELETE FROM `creature_queststarter` WHERE `quest`=40830;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102142, 40830);

DELETE FROM `creature_questender` WHERE `quest`=40830;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97140, 40830);

DELETE FROM `quest_template_addon` WHERE `ID`=40830;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40830, 40748, 44691);
