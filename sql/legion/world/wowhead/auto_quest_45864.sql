UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90418);

DELETE FROM `creature_queststarter` WHERE `quest`=45864;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90418, 45864);

DELETE FROM `creature_questender` WHERE `quest`=45864;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90418, 45864);

DELETE FROM `quest_template_addon` WHERE `ID`=45864;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45864, 46200);
