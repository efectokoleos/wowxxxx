UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100775);

DELETE FROM `creature_queststarter` WHERE `quest`=40617;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100775, 40617);

DELETE FROM `creature_questender` WHERE `quest`=40617;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100775, 40617);

DELETE FROM `quest_template_addon` WHERE `ID`=40617;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40617, 41230);
