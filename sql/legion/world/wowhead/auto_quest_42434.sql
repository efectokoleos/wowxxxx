UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107341);

DELETE FROM `creature_queststarter` WHERE `quest`=42434;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107341, 42434);

DELETE FROM `creature_questender` WHERE `quest`=42434;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107532, 42434);

DELETE FROM `quest_template_addon` WHERE `ID`=42434;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42434, 42418, 42435);
