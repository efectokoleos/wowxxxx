UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103175);

DELETE FROM `creature_queststarter` WHERE `quest`=41307;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103175, 41307);

DELETE FROM `creature_questender` WHERE `quest`=41307;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103175, 41307);

DELETE FROM `quest_template_addon` WHERE `ID`=41307;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(41307, 41123, 41466);
