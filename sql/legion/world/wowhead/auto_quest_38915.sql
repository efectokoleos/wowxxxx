UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108434);

DELETE FROM `creature_queststarter` WHERE `quest`=38915;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108434, 38915);

DELETE FROM `creature_questender` WHERE `quest`=38915;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93846, 38915);

DELETE FROM `quest_template_addon` WHERE `ID`=38915;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38915, 39580, 39776);
