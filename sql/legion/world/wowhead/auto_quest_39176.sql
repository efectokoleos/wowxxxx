UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94686);

DELETE FROM `creature_queststarter` WHERE `quest`=39176;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94686, 39176);

DELETE FROM `creature_questender` WHERE `quest`=39176;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94605, 39176);

DELETE FROM `quest_template_addon` WHERE `ID`=39176;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39176, 39175, 39177);
