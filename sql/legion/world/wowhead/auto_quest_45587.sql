UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117292);

DELETE FROM `creature_queststarter` WHERE `quest`=45587;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117292, 45587);

DELETE FROM `creature_questender` WHERE `quest`=45587;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117361, 45587);

DELETE FROM `quest_template_addon` WHERE `ID`=45587;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45587, 45575, 45796);
