UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101656);

DELETE FROM `creature_queststarter` WHERE `quest`=40784;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101656, 40784);

DELETE FROM `creature_questender` WHERE `quest`=40784;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101651, 40784);

DELETE FROM `quest_template_addon` WHERE `ID`=40784;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40784, 40783, 40785);
