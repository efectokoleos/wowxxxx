UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119166);

DELETE FROM `creature_queststarter` WHERE `quest`=46241;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119166, 46241);

DELETE FROM `quest_template_addon` WHERE `ID`=46241;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46241, 46242);
