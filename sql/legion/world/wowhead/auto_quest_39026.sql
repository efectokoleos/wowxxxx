UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93805);

DELETE FROM `creature_queststarter` WHERE `quest`=39026;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93805, 39026);

DELETE FROM `creature_questender` WHERE `quest`=39026;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97553, 39026);

DELETE FROM `quest_template_addon` WHERE `ID`=39026;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39026, 42088, 39043);
