UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96541);

DELETE FROM `creature_queststarter` WHERE `quest`=39771;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96541, 39771);

DELETE FROM `creature_questender` WHERE `quest`=39771;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96541, 39771);

DELETE FROM `quest_template_addon` WHERE `ID`=39771;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39771, 43338);
