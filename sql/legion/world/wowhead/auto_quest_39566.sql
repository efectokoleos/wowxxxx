UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92458);

DELETE FROM `creature_queststarter` WHERE `quest`=39566;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92458, 39566);

DELETE FROM `quest_template_addon` WHERE `ID`=39566;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39566, 39390);
