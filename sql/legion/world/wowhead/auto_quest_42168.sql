UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106610);

DELETE FROM `creature_queststarter` WHERE `quest`=42168;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106610, 42168);

DELETE FROM `creature_questender` WHERE `quest`=42168;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106610, 42168);

DELETE FROM `quest_template_addon` WHERE `ID`=42168;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42168, 40611, 40712);
