UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90250);

DELETE FROM `creature_queststarter` WHERE `quest`=39756;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90250, 39756);

DELETE FROM `creature_questender` WHERE `quest`=39756;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90250, 39756);

DELETE FROM `quest_template_addon` WHERE `ID`=39756;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39756, 38933);
