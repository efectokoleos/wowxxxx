UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118242);

DELETE FROM `creature_queststarter` WHERE `quest`=44766;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118242, 44766);

DELETE FROM `creature_questender` WHERE `quest`=44766;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(108247, 44766);

DELETE FROM `quest_template_addon` WHERE `ID`=44766;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44766, 45437, 46335);
