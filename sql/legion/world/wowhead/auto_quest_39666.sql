UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93822);

DELETE FROM `creature_queststarter` WHERE `quest`=39666;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93822, 39666);

DELETE FROM `creature_questender` WHERE `quest`=39666;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93812, 39666);

DELETE FROM `quest_template_addon` WHERE `ID`=39666;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39666, 39665);
