DELETE FROM `creature_questender` WHERE `quest`=40606;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100729, 40606);

DELETE FROM `gameobject_queststarter` WHERE `quest`=40606;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(245793, 40606);

DELETE FROM `quest_template_addon` WHERE `ID`=40606;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(40606, 40604, 40611);
