UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116448);

DELETE FROM `creature_queststarter` WHERE `quest`=45330;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116448, 45330);

DELETE FROM `creature_questender` WHERE `quest`=45330;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(116448, 45330);

DELETE FROM `quest_template_addon` WHERE `ID`=45330;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45330, 45301);
