UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102546);

DELETE FROM `creature_queststarter` WHERE `quest`=40988;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102546, 40988);

DELETE FROM `creature_questender` WHERE `quest`=40988;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101846, 40988);

DELETE FROM `quest_template_addon` WHERE `ID`=40988;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40988, 40990);
