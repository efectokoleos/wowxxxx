UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93538);

DELETE FROM `creature_queststarter` WHERE `quest`=45160;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93538, 45160);

DELETE FROM `creature_questender` WHERE `quest`=45160;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93538, 45160);

DELETE FROM `quest_template_addon` WHERE `ID`=45160;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(45160, 45159, 45238);
