UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88873);

DELETE FROM `creature_queststarter` WHERE `quest`=37507;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88873, 37507);

DELETE FROM `creature_questender` WHERE `quest`=37507;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88873, 37507);

DELETE FROM `quest_template_addon` WHERE `ID`=37507;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37507, 37496);
