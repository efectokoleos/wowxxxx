UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(74163);

DELETE FROM `creature_queststarter` WHERE `quest`=37291;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(74163, 37291);

DELETE FROM `creature_questender` WHERE `quest`=37291;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(88228, 37291);

DELETE FROM `quest_template_addon` WHERE `ID`=37291;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37291, 33493);
