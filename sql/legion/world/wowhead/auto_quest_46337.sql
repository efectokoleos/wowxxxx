UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119943);

DELETE FROM `creature_queststarter` WHERE `quest`=46337;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119943, 46337);

DELETE FROM `creature_questender` WHERE `quest`=46337;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119958, 46337);

DELETE FROM `quest_template_addon` WHERE `ID`=46337;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46337, 46336);
