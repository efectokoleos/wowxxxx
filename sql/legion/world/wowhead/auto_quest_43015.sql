UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98092);

DELETE FROM `creature_queststarter` WHERE `quest`=43015;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98092, 43015);

DELETE FROM `creature_questender` WHERE `quest`=43015;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(101513, 43015);

DELETE FROM `quest_template_addon` WHERE `ID`=43015;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(43015, 43014);
