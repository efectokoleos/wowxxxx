UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117246);

DELETE FROM `creature_queststarter` WHERE `quest`=45556;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117246, 45556);

DELETE FROM `creature_questender` WHERE `quest`=45556;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119097, 45556);

DELETE FROM `quest_template_addon` WHERE `ID`=45556;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45556, 45557);
