UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91190);

DELETE FROM `creature_queststarter` WHERE `quest`=46940;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91190, 46940);

DELETE FROM `creature_questender` WHERE `quest`=46940;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91190, 46940);

DELETE FROM `quest_template_addon` WHERE `ID`=46940;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46940, 46809);
