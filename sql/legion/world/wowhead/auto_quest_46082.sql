UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118752);

DELETE FROM `creature_queststarter` WHERE `quest`=46082;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118752, 46082);

DELETE FROM `creature_questender` WHERE `quest`=46082;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118947, 46082);

DELETE FROM `quest_template_addon` WHERE `ID`=46082;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46082, 46079);
