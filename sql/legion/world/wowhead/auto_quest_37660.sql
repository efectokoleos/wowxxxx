UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89398);

DELETE FROM `creature_queststarter` WHERE `quest`=37660;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89398, 37660);

DELETE FROM `creature_questender` WHERE `quest`=37660;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89398, 37660);

DELETE FROM `quest_template_addon` WHERE `ID`=37660;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(37660, 37653);
