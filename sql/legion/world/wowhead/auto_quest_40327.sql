UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(99689);

DELETE FROM `creature_queststarter` WHERE `quest`=40327;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(99689, 40327);

DELETE FROM `creature_questender` WHERE `quest`=40327;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98964, 40327);

DELETE FROM `quest_template_addon` WHERE `ID`=40327;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40327, 40195);
