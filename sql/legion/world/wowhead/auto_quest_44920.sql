UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115791);

DELETE FROM `creature_queststarter` WHERE `quest`=44920;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115791, 44920);

DELETE FROM `quest_template_addon` WHERE `ID`=44920;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44920, 44924);
