UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92242);

DELETE FROM `creature_queststarter` WHERE `quest`=38531;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92242, 38531);

DELETE FROM `creature_questender` WHERE `quest`=38531;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92242, 38531);

DELETE FROM `quest_template_addon` WHERE `ID`=38531;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38531, 38559);
