UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(104535);

DELETE FROM `creature_queststarter` WHERE `quest`=41783;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(104535, 41783);

DELETE FROM `creature_questender` WHERE `quest`=41783;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(104535, 41783);

DELETE FROM `quest_template_addon` WHERE `ID`=41783;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41783, 41782);
