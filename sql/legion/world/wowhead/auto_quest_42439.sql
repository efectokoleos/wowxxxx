UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107392);

DELETE FROM `creature_queststarter` WHERE `quest`=42439;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107392, 42439);

DELETE FROM `creature_questender` WHERE `quest`=42439;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107392, 42439);

DELETE FROM `quest_template_addon` WHERE `ID`=42439;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42439, 42428);
