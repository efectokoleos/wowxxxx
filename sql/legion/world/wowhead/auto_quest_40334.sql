UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102334);

DELETE FROM `creature_queststarter` WHERE `quest`=40334;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102334, 40334);

DELETE FROM `creature_questender` WHERE `quest`=40334;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102381, 40334);
