UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93011);

DELETE FROM `creature_queststarter` WHERE `quest`=40077;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93011, 40077);

DELETE FROM `creature_questender` WHERE `quest`=40077;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98229, 40077);

DELETE FROM `quest_template_addon` WHERE `ID`=40077;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40077, 40378);
