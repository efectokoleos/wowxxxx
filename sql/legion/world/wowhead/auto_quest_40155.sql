UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98720);

DELETE FROM `creature_queststarter` WHERE `quest`=40155;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98720, 40155);

DELETE FROM `creature_questender` WHERE `quest`=40155;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93541, 40155);

DELETE FROM `quest_template_addon` WHERE `ID`=40155;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40155, 40153);
