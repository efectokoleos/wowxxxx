UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97296);

DELETE FROM `creature_queststarter` WHERE `quest`=39691;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97296, 39691);

DELETE FROM `creature_questender` WHERE `quest`=39691;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102585, 39691);

DELETE FROM `quest_template_addon` WHERE `ID`=39691;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39691, 40518, 40760);
