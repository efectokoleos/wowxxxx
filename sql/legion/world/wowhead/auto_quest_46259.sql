UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119209);

DELETE FROM `creature_queststarter` WHERE `quest`=46259;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119209, 46259);

DELETE FROM `creature_questender` WHERE `quest`=46259;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119209, 46259);

DELETE FROM `quest_template_addon` WHERE `ID`=46259;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46259, 45145);
