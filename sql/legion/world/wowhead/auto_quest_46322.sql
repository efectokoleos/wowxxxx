UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101513);

DELETE FROM `creature_queststarter` WHERE `quest`=46322;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101513, 46322);

DELETE FROM `creature_questender` WHERE `quest`=46322;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119822, 46322);

DELETE FROM `quest_template_addon` WHERE `ID`=46322;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46322, 44758);
