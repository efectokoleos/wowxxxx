UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96479);

DELETE FROM `creature_queststarter` WHERE `quest`=46293;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96479, 46293);

DELETE FROM `creature_questender` WHERE `quest`=46293;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119390, 46293);

DELETE FROM `quest_template_addon` WHERE `ID`=46293;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(46293, 46291);
