UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91462);

DELETE FROM `creature_queststarter` WHERE `quest`=40122;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91462, 40122);

DELETE FROM `creature_questender` WHERE `quest`=40122;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91109, 40122);

DELETE FROM `quest_template_addon` WHERE `ID`=40122;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40122, 39861);
