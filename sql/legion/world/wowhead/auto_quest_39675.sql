UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94789);

DELETE FROM `creature_queststarter` WHERE `quest`=39675;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94789, 39675);

DELETE FROM `creature_questender` WHERE `quest`=39675;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(94801, 39675);

DELETE FROM `quest_template_addon` WHERE `ID`=39675;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39675, 39655, 39676);
