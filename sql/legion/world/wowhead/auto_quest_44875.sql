UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115693);

DELETE FROM `creature_queststarter` WHERE `quest`=44875;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115693, 44875);

DELETE FROM `creature_questender` WHERE `quest`=44875;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115693, 44875);

DELETE FROM `quest_template_addon` WHERE `ID`=44875;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44875, 44874);
