UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95186);

DELETE FROM `creature_queststarter` WHERE `quest`=39316;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95186, 39316);

DELETE FROM `creature_questender` WHERE `quest`=39316;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95186, 39316);

DELETE FROM `quest_template_addon` WHERE `ID`=39316;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39316, 39496);
