UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102799);

DELETE FROM `creature_queststarter` WHERE `quest`=42869;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102799, 42869);

DELETE FROM `creature_questender` WHERE `quest`=42869;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(109196, 42869);
