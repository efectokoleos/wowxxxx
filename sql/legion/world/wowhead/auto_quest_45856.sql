UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(118250);

DELETE FROM `creature_queststarter` WHERE `quest`=45856;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(118250, 45856);

DELETE FROM `creature_questender` WHERE `quest`=45856;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(118440, 45856);

DELETE FROM `quest_template_addon` WHERE `ID`=45856;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45856, 44789);
