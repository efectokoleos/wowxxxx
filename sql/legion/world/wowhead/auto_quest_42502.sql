UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94138);

DELETE FROM `creature_queststarter` WHERE `quest`=42502;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94138, 42502);

DELETE FROM `quest_template_addon` WHERE `ID`=42502;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(42502, 42503);
