UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(95799);

DELETE FROM `creature_queststarter` WHERE `quest`=39439;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(95799, 39439);

DELETE FROM `creature_questender` WHERE `quest`=39439;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(95799, 39439);

DELETE FROM `quest_template_addon` WHERE `ID`=39439;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39439, 39440);
