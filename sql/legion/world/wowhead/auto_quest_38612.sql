UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92561);

DELETE FROM `creature_queststarter` WHERE `quest`=38612;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92561, 38612);

DELETE FROM `creature_questender` WHERE `quest`=38612;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92566, 38612);

DELETE FROM `quest_template_addon` WHERE `ID`=38612;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38612, 38473, 38318);
