UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98931);

DELETE FROM `creature_queststarter` WHERE `quest`=40193;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98931, 40193);

DELETE FROM `creature_questender` WHERE `quest`=40193;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98964, 40193);
