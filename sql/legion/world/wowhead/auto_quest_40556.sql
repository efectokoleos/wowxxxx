DELETE FROM `creature_questender` WHERE `quest`=40556;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100521, 40556);

DELETE FROM `gameobject_queststarter` WHERE `quest`=40556;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(246250, 40556);

DELETE FROM `quest_template_addon` WHERE `ID`=40556;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40556, 40547);
