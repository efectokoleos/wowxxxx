UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113361);

DELETE FROM `creature_queststarter` WHERE `quest`=44415;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113361, 44415);

DELETE FROM `creature_questender` WHERE `quest`=44415;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113355, 44415);

DELETE FROM `quest_template_addon` WHERE `ID`=44415;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44415, 44414, 44416);
