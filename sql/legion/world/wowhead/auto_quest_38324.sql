UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91531);

DELETE FROM `creature_queststarter` WHERE `quest`=38324;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91531, 38324);

DELETE FROM `creature_questender` WHERE `quest`=38324;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91531, 38324);

DELETE FROM `quest_template_addon` WHERE `ID`=38324;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38324, 39837, 38347);
