UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107425);

DELETE FROM `creature_queststarter` WHERE `quest`=42392;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107425, 42392);

DELETE FROM `creature_questender` WHERE `quest`=42392;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107425, 42392);

DELETE FROM `quest_template_addon` WHERE `ID`=42392;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42392, 42390);
