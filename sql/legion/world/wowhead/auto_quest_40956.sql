UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98548);

DELETE FROM `creature_queststarter` WHERE `quest`=40956;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98548, 40956);

DELETE FROM `gameobject_questender` WHERE `quest`=40956;
INSERT INTO `gameobject_questender` (`id`, `quest`) VALUES 
(246713, 40956);

DELETE FROM `quest_template_addon` WHERE `ID`=40956;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40956, 44691);
