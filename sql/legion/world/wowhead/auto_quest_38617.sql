UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92573);

DELETE FROM `creature_queststarter` WHERE `quest`=38617;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92573, 38617);

DELETE FROM `creature_questender` WHERE `quest`=38617;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92569, 38617);

DELETE FROM `quest_template_addon` WHERE `ID`=38617;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38617, 38410, 38412);
