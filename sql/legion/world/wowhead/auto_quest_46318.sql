UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(106299);

DELETE FROM `creature_queststarter` WHERE `quest`=46318;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(106299, 46318);

DELETE FROM `creature_questender` WHERE `quest`=46318;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(106299, 46318);

DELETE FROM `quest_template_addon` WHERE `ID`=46318;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46318, 46317, 46319);
