UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115551);

DELETE FROM `creature_queststarter` WHERE `quest`=44865;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115551, 44865);

DELETE FROM `creature_questender` WHERE `quest`=44865;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115438, 44865);

DELETE FROM `quest_template_addon` WHERE `ID`=44865;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44865, 44803);
