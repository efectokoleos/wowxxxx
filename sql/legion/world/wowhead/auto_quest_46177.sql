UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116048);

DELETE FROM `creature_queststarter` WHERE `quest`=46177;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116048, 46177);

DELETE FROM `creature_questender` WHERE `quest`=46177;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(105081, 46177);
