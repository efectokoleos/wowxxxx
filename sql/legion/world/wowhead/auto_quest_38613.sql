UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92566);

DELETE FROM `creature_queststarter` WHERE `quest`=38613;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92566, 38613);

DELETE FROM `creature_questender` WHERE `quest`=38613;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92567, 38613);

DELETE FROM `quest_template_addon` WHERE `ID`=38613;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38613, 38312, 38410);
