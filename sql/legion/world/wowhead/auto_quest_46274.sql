UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(119338);

DELETE FROM `creature_queststarter` WHERE `quest`=46274;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(119338, 46274);

DELETE FROM `creature_questender` WHERE `quest`=46274;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(119357, 46274);

DELETE FROM `quest_template_addon` WHERE `ID`=46274;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46274, 46272, 46275);
