UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(108492);

DELETE FROM `creature_queststarter` WHERE `quest`=42133;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(108492, 42133);

DELETE FROM `creature_questender` WHERE `quest`=42133;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(107973, 42133);

DELETE FROM `quest_template_addon` WHERE `ID`=42133;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42133, 42658, 42659);
