UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92710);

DELETE FROM `creature_queststarter` WHERE `quest`=38753;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92710, 38753);

DELETE FROM `creature_questender` WHERE `quest`=38753;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102938, 38753);
