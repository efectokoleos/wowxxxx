UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(96258);

DELETE FROM `creature_queststarter` WHERE `quest`=39597;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(96258, 39597);

DELETE FROM `creature_questender` WHERE `quest`=39597;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96254, 39597);

DELETE FROM `quest_template_addon` WHERE `ID`=39597;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39597, 39594, 38473);
