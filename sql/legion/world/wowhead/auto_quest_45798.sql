UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117709);

DELETE FROM `creature_queststarter` WHERE `quest`=45798;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117709, 45798);

DELETE FROM `creature_questender` WHERE `quest`=45798;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117709, 45798);

DELETE FROM `quest_template_addon` WHERE `ID`=45798;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45798, 46266);
