UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113986);

DELETE FROM `creature_queststarter` WHERE `quest`=44421;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113986, 44421);

DELETE FROM `creature_questender` WHERE `quest`=44421;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113986, 44421);

DELETE FROM `quest_template_addon` WHERE `ID`=44421;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(44421, 44182, 44500);
