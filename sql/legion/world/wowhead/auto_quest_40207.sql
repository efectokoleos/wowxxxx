UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98964);

DELETE FROM `creature_queststarter` WHERE `quest`=40207;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98964, 40207);

DELETE FROM `creature_questender` WHERE `quest`=40207;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98964, 40207);

DELETE FROM `quest_template_addon` WHERE `ID`=40207;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40207, 40210);
