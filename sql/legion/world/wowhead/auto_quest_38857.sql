UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(88863);

DELETE FROM `creature_queststarter` WHERE `quest`=38857;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(88863, 38857);

DELETE FROM `creature_questender` WHERE `quest`=38857;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91419, 38857);

DELETE FROM `quest_template_addon` WHERE `ID`=38857;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(38857, 37654);
