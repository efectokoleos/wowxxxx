UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(114310);

DELETE FROM `creature_queststarter` WHERE `quest`=44733;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(114310, 44733);

DELETE FROM `creature_questender` WHERE `quest`=44733;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(114310, 44733);

DELETE FROM `quest_template_addon` WHERE `ID`=44733;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44733, 44764);
