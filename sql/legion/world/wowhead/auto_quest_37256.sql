UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(89023);

DELETE FROM `creature_queststarter` WHERE `quest`=37256;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(89023, 37256);

DELETE FROM `creature_questender` WHERE `quest`=37256;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(89023, 37256);

DELETE FROM `quest_template_addon` WHERE `ID`=37256;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37256, 37690, 37733);
