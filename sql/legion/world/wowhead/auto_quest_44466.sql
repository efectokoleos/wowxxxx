UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(113857);

DELETE FROM `creature_queststarter` WHERE `quest`=44466;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(113857, 44466);

DELETE FROM `creature_questender` WHERE `quest`=44466;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(113857, 44466);

DELETE FROM `quest_template_addon` WHERE `ID`=44466;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44466, 44479);
