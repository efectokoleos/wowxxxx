UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117715);

DELETE FROM `creature_queststarter` WHERE `quest`=45971;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117715, 45971);

DELETE FROM `creature_questender` WHERE `quest`=45971;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117715, 45971);

DELETE FROM `quest_template_addon` WHERE `ID`=45971;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45971, 45763);
