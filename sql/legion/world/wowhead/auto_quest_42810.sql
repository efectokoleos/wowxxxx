UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(98646);

DELETE FROM `creature_queststarter` WHERE `quest`=42810;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(98646, 42810);

DELETE FROM `creature_questender` WHERE `quest`=42810;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98646, 42810);

DELETE FROM `quest_template_addon` WHERE `ID`=42810;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42810, 42754);
