UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115736);

DELETE FROM `creature_queststarter` WHERE `quest`=44919;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115736, 44919);

DELETE FROM `creature_questender` WHERE `quest`=44919;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115736, 44919);

DELETE FROM `quest_template_addon` WHERE `ID`=44919;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44919, 44918);
