DELETE FROM `gameobject_queststarter` WHERE `quest`=37849;
INSERT INTO `gameobject_queststarter` (`id`, `quest`) VALUES 
(239791, 37849);

DELETE FROM `quest_template_addon` WHERE `ID`=37849;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(37849, 37848, 37850);
