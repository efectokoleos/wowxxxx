UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(103155);

DELETE FROM `creature_queststarter` WHERE `quest`=43562;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(103155, 43562);

DELETE FROM `creature_questender` WHERE `quest`=43562;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103155, 43562);

DELETE FROM `quest_template_addon` WHERE `ID`=43562;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(43562, 43502, 43563);
