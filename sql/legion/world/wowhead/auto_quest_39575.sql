UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(94571);

DELETE FROM `creature_queststarter` WHERE `quest`=39575;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(94571, 39575);

DELETE FROM `creature_questender` WHERE `quest`=39575;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(99153, 39575);

DELETE FROM `quest_template_addon` WHERE `ID`=39575;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39575, 38916, 40219);
