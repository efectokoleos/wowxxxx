UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92183);

DELETE FROM `creature_queststarter` WHERE `quest`=39351;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92183, 39351);

DELETE FROM `creature_questender` WHERE `quest`=39351;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92183, 39351);

DELETE FROM `quest_template_addon` WHERE `ID`=39351;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39351, 39348);
