UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93538);

DELETE FROM `creature_queststarter` WHERE `quest`=41171;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93538, 41171);

DELETE FROM `creature_questender` WHERE `quest`=41171;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(103482, 41171);

DELETE FROM `quest_template_addon` WHERE `ID`=41171;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(41171, 41172);
