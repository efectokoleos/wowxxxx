UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(101195);

DELETE FROM `creature_queststarter` WHERE `quest`=42583;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(101195, 42583);

DELETE FROM `creature_questender` WHERE `quest`=42583;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(98002, 42583);

DELETE FROM `quest_template_addon` WHERE `ID`=42583;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(42583, 42516);
