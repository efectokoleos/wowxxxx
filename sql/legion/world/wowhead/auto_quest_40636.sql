UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(100438);

DELETE FROM `creature_queststarter` WHERE `quest`=40636;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(100438, 40636);

DELETE FROM `creature_questender` WHERE `quest`=40636;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(100438, 40636);

DELETE FROM `quest_template_addon` WHERE `ID`=40636;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(40636, 40236);
