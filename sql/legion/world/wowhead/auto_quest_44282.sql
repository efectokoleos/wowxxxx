UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93555);

DELETE FROM `creature_queststarter` WHERE `quest`=44282;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93555, 44282);

DELETE FROM `creature_questender` WHERE `quest`=44282;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93555, 44282);

DELETE FROM `quest_template_addon` WHERE `ID`=44282;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44282, 44247);
