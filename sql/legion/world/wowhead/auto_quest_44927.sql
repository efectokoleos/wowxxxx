UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115805);

DELETE FROM `creature_queststarter` WHERE `quest`=44927;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115805, 44927);

DELETE FROM `creature_questender` WHERE `quest`=44927;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115805, 44927);

DELETE FROM `quest_template_addon` WHERE `ID`=44927;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44927, 44926);
