UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(107460);

DELETE FROM `creature_queststarter` WHERE `quest`=44701;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(107460, 44701);

DELETE FROM `creature_questender` WHERE `quest`=44701;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(96683, 44701);

DELETE FROM `quest_template_addon` WHERE `ID`=44701;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(44701, 38307);
