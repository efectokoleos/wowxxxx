UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97319);

DELETE FROM `creature_queststarter` WHERE `quest`=39837;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97319, 39837);

DELETE FROM `creature_questender` WHERE `quest`=39837;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91531, 39837);

DELETE FROM `quest_template_addon` WHERE `ID`=39837;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39837, 38324);
