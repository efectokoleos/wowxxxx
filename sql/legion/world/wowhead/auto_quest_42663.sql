UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(102700);

DELETE FROM `creature_queststarter` WHERE `quest`=42663;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(102700, 42663);

DELETE FROM `creature_questender` WHERE `quest`=42663;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(102700, 42663);

DELETE FROM `quest_template_addon` WHERE `ID`=42663;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(42663, 42175, 42703);
