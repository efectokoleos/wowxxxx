UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(115465);

DELETE FROM `creature_queststarter` WHERE `quest`=44821;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(115465, 44821);

DELETE FROM `creature_questender` WHERE `quest`=44821;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115465, 44821);

DELETE FROM `quest_template_addon` WHERE `ID`=44821;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(44821, 44782);
