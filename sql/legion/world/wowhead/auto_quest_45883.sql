UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117715);

DELETE FROM `creature_queststarter` WHERE `quest`=45883;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117715, 45883);

DELETE FROM `creature_questender` WHERE `quest`=45883;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(115492, 45883);

DELETE FROM `quest_template_addon` WHERE `ID`=45883;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(45883, 45769);
