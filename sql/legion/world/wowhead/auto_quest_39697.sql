UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92213);

DELETE FROM `creature_queststarter` WHERE `quest`=39697;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92213, 39697);

DELETE FROM `creature_questender` WHERE `quest`=39697;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(92213, 39697);

DELETE FROM `quest_template_addon` WHERE `ID`=39697;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(39697, 39057);
