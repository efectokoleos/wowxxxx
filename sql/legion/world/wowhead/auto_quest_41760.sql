UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(97140);

DELETE FROM `creature_queststarter` WHERE `quest`=41760;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(97140, 41760);

DELETE FROM `creature_questender` WHERE `quest`=41760;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97140, 41760);

DELETE FROM `quest_template_addon` WHERE `ID`=41760;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(41760, 41704);
