UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(6294);

DELETE FROM `creature_queststarter` WHERE `quest`=38345;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(6294, 38345);

DELETE FROM `creature_questender` WHERE `quest`=38345;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(6294, 38345);

DELETE FROM `quest_template_addon` WHERE `ID`=38345;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38345, 38344, 38394);
