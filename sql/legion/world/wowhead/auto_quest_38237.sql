UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(91165);

DELETE FROM `creature_queststarter` WHERE `quest`=38237;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(91165, 38237);

DELETE FROM `creature_questender` WHERE `quest`=38237;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(91166, 38237);

DELETE FROM `quest_template_addon` WHERE `ID`=38237;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38237, 38232);
