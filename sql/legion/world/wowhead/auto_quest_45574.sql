UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(117305);

DELETE FROM `creature_queststarter` WHERE `quest`=45574;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(117305, 45574);

DELETE FROM `creature_questender` WHERE `quest`=45574;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117305, 45574);

DELETE FROM `quest_template_addon` WHERE `ID`=45574;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(45574, 45459);
