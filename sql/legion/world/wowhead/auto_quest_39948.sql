UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92195);

DELETE FROM `creature_queststarter` WHERE `quest`=39948;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92195, 39948);

DELETE FROM `creature_questender` WHERE `quest`=39948;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97359, 39948);

DELETE FROM `quest_template_addon` WHERE `ID`=39948;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(39948, 39949);
