UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92539);

DELETE FROM `creature_queststarter` WHERE `quest`=39796;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92539, 39796);

DELETE FROM `creature_questender` WHERE `quest`=39796;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93231, 39796);

DELETE FROM `quest_template_addon` WHERE `ID`=39796;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(39796, 39804, 38778);
