UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(120726);

DELETE FROM `creature_queststarter` WHERE `quest`=46349;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(120726, 46349);

DELETE FROM `creature_questender` WHERE `quest`=46349;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(120726, 46349);

DELETE FROM `quest_template_addon` WHERE `ID`=46349;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(46349, 46348);
