UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93773);

DELETE FROM `creature_queststarter` WHERE `quest`=38904;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93773, 38904);

DELETE FROM `creature_questender` WHERE `quest`=38904;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93823, 38904);

DELETE FROM `quest_template_addon` WHERE `ID`=38904;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38904, 41052, 39654);
