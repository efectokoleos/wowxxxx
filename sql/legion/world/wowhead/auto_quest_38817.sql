UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93446);

DELETE FROM `creature_queststarter` WHERE `quest`=38817;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93446, 38817);

DELETE FROM `creature_questender` WHERE `quest`=38817;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97270, 38817);

DELETE FROM `quest_template_addon` WHERE `ID`=38817;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(38817, 38811, 38815);
