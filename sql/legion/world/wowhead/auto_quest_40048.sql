UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(90317);

DELETE FROM `creature_queststarter` WHERE `quest`=40048;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(90317, 40048);

DELETE FROM `creature_questender` WHERE `quest`=40048;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(90317, 40048);

DELETE FROM `quest_template_addon` WHERE `ID`=40048;
INSERT INTO `quest_template_addon` (`ID`, `NextQuestID`) VALUES
(40048, 39878);
