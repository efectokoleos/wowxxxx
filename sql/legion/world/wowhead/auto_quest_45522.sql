UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(93628);

DELETE FROM `creature_queststarter` WHERE `quest`=45522;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(93628, 45522);

DELETE FROM `creature_questender` WHERE `quest`=45522;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(117199, 45522);
