UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(92120);

DELETE FROM `creature_queststarter` WHERE `quest`=38463;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(92120, 38463);

DELETE FROM `creature_questender` WHERE `quest`=38463;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(93659, 38463);

DELETE FROM `quest_template_addon` WHERE `ID`=38463;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`) VALUES
(38463, 38462);
