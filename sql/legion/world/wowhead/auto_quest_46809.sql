UPDATE `creature_template` SET `npcflag` = `npcflag` | 0x2 WHERE `entry` IN(116302);

DELETE FROM `creature_queststarter` WHERE `quest`=46809;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES 
(116302, 46809);

DELETE FROM `creature_questender` WHERE `quest`=46809;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES 
(97111, 46809);

DELETE FROM `quest_template_addon` WHERE `ID`=46809;
INSERT INTO `quest_template_addon` (`ID`, `PrevQuestID`, `NextQuestID`) VALUES
(46809, 47067, 46940);
