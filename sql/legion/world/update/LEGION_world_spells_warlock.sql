DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_warl_legion_agony',
'spell_warl_legion_banish',
'spell_warl_legion_burning_rush',
'spell_warl_legion_call_dreadstalkers',
'spell_warl_legion_chaotic_energies',
'spell_warl_legion_cataclysm',
'spell_warl_legion_call_felhunter',
'spell_warl_legion_channel_demonfire',
'spell_warl_legion_channel_demonfire_selector',
'spell_warl_legion_chaos_barrage',
'spell_warl_legion_chaos_bolt',
'spell_warl_legion_command_demon_dummys',
'spell_warl_legion_command_demon_override',
'spell_warl_legion_compounding_horror_damage',
'spell_warl_legion_conflagrate',
'spell_warl_legion_corruption',
'spell_warl_legion_corruption_periodic',
'spell_warl_legion_create_healthstone',
'spell_warl_legion_cremation_damage',
'spell_warl_legion_curse_of_shadows',
'spell_warl_legion_dark_pact',
'spell_warl_legion_demonbolt',
'spell_warl_legion_demonwrath',
'spell_warl_legion_devour_magic',
'spell_warl_legion_dimensional_rift',
'spell_warl_legion_demonic_calling',
'spell_warl_legion_demonic_empowerment',
'spell_warl_legion_demon_skin',
'spell_warl_legion_dimension_ripper',
'spell_warl_legion_doom',
'spell_warl_legion_doom_bolt',
'spell_warl_legion_drain_life',
'spell_warl_legion_essence_drain',
'spell_warl_legion_demonic_circle_summon',
'spell_warl_legion_demonic_circle_teleport',
'spell_warl_legion_demonic_gateway',
'spell_warl_legion_demonic_power',
'spell_warl_legion_drain_soul',
'spell_warl_legion_entrenched_in_flame',
'spell_warl_legion_eradication',
'spell_warl_legion_eye_laser',
'spell_warl_legion_fear',
'spell_warl_legion_fel_cleave',
'spell_warl_legion_fel_fissure',
'spell_warl_legion_fixate',
'spell_warl_legion_grimoire_of_supremacy',
'spell_warl_legion_havoc_trigger',
'spell_warl_legion_hand_of_guldan',
'spell_warl_legion_hand_of_guldan_damage',
'spell_warl_legion_haunt',
'spell_warl_legion_healthstone',
'spell_warl_legion_health_funnel',
'spell_warl_legion_immolate',
'spell_warl_legion_immolate_periodic',
'spell_warl_legion_immolate_proc',
'spell_warl_legion_implosion',
'spell_warl_legion_incinerate',
'spell_warl_legion_life_tap',
'spell_warl_legion_meteor_strike',
'spell_warl_legion_phantom_singularity',
'spell_warl_legion_pleasure_through_pain',
'spell_warl_legion_pleasure_through_pain_pet_aura',
'spell_warl_legion_reap_souls',
'spell_warl_legion_roaring_blaze',
'spell_warl_legion_seed_of_corruption',
'spell_warl_legion_seed_of_corruption_damage',
'spell_warl_legion_shadowburn',
'spell_warl_legion_shadow_barrage',
'spell_warl_legion_shadow_shield',
'spell_warl_legion_singe_magic',
'spell_warl_legion_singe_magic_damage',
'spell_warl_legion_soulshatter',
'spell_warl_legion_shadow_bolt',
'spell_warl_legion_soulsnatcher',
'spell_warl_legion_soul_conduit',
'spell_warl_legion_soul_effigy',
'spell_warl_legion_soul_effigy_periodic',
'spell_warl_legion_soul_effigy_visual',
'spell_warl_legion_soul_flame',
'spell_warl_legion_soul_harvest',
'spell_warl_legion_soul_leech',
'spell_warl_legion_soulstone',
'spell_warl_legion_stolen_power',
'spell_warl_legion_summon_infernal',
'spell_warl_legion_thalkiels_consumption',
'spell_warl_legion_unstable_affliction',
'spell_warl_legion_unstable_affliction_triggered',
'spell_warl_legion_wrath_of_consumption'
);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(980,    'spell_warl_legion_agony'),
(710,    'spell_warl_legion_banish'),
(111400, 'spell_warl_legion_burning_rush'),
(104316, 'spell_warl_legion_call_dreadstalkers'),
(77220,  'spell_warl_legion_chaotic_energies'),
(152108, 'spell_warl_legion_cataclysm'),
(212619, 'spell_warl_legion_call_felhunter'),
(196447, 'spell_warl_legion_channel_demonfire'),
(196449, 'spell_warl_legion_channel_demonfire_selector'),
(187385, 'spell_warl_legion_chaos_barrage'),
(116858, 'spell_warl_legion_chaos_bolt'),
(119905, 'spell_warl_legion_command_demon_dummys'),
(119907, 'spell_warl_legion_command_demon_dummys'),
(119911, 'spell_warl_legion_command_demon_dummys'),
(119910, 'spell_warl_legion_command_demon_dummys'),
(171140, 'spell_warl_legion_command_demon_dummys'),
(171154, 'spell_warl_legion_command_demon_dummys'),
(171152, 'spell_warl_legion_command_demon_dummys'),
(119913, 'spell_warl_legion_command_demon_dummys'),
(119909, 'spell_warl_legion_command_demon_dummys'),
(119914, 'spell_warl_legion_command_demon_dummys'),
(119904, 'spell_warl_legion_command_demon_override'),
(231489, 'spell_warl_legion_compounding_horror_damage'),
(17962,  'spell_warl_legion_conflagrate'),
(172,    'spell_warl_legion_corruption'),
(146739, 'spell_warl_legion_corruption_periodic'),
(6201,   'spell_warl_legion_create_healthstone'),
(34130,  'spell_warl_legion_create_healthstone'),
(212327, 'spell_warl_legion_cremation_damage'),
(234877, 'spell_warl_legion_curse_of_shadows'),
(108416, 'spell_warl_legion_dark_pact'),
(157695, 'spell_warl_legion_demonbolt'),
(193439, 'spell_warl_legion_demonwrath'),
(19505,  'spell_warl_legion_devour_magic'),
(196586, 'spell_warl_legion_dimensional_rift'),
(205145, 'spell_warl_legion_demonic_calling'),
(193396, 'spell_warl_legion_demonic_empowerment'),
(219272, 'spell_warl_legion_demon_skin'),
(219415, 'spell_warl_legion_dimension_ripper'),
(603,    'spell_warl_legion_doom'),
(85692,  'spell_warl_legion_doom_bolt'),
(234153, 'spell_warl_legion_drain_life'),
(48018,  'spell_warl_legion_demonic_circle_summon'),
(48020,  'spell_warl_legion_demonic_circle_teleport'),
(111771, 'spell_warl_legion_demonic_gateway'),
(196099, 'spell_warl_legion_demonic_power'),
(198590, 'spell_warl_legion_drain_soul'),
(233581, 'spell_warl_legion_entrenched_in_flame'),
(196412, 'spell_warl_legion_eradication'),
(221711, 'spell_warl_legion_essence_drain'),
(205231, 'spell_warl_legion_eye_laser'),
(5782,   'spell_warl_legion_fear'),
(213688, 'spell_warl_legion_fel_cleave'),
(200586, 'spell_warl_legion_fel_fissure'),
(226044, 'spell_warl_legion_fixate'),
(152107, 'spell_warl_legion_grimoire_of_supremacy'),
(17962,  'spell_warl_legion_havoc_trigger'),
(29722,  'spell_warl_legion_havoc_trigger'),
(348,    'spell_warl_legion_havoc_trigger'),
(116858, 'spell_warl_legion_havoc_trigger'),
(17877,  'spell_warl_legion_havoc_trigger'),
(6789,   'spell_warl_legion_havoc_trigger'),
(105174, 'spell_warl_legion_hand_of_guldan'),
(86040,  'spell_warl_legion_hand_of_guldan_damage'),
(48181,  'spell_warl_legion_haunt'),
(6262,   'spell_warl_legion_healthstone'),
(755,    'spell_warl_legion_health_funnel'),
(348,    'spell_warl_legion_immolate'),
(157736, 'spell_warl_legion_immolate_periodic'),
(193541, 'spell_warl_legion_immolate_proc'),
(196277, 'spell_warl_legion_implosion'),
(29722,  'spell_warl_legion_incinerate'),
(1454,   'spell_warl_legion_life_tap'),
(171017, 'spell_warl_legion_meteor_strike'),
(205179, 'spell_warl_legion_phantom_singularity'),
(212618, 'spell_warl_legion_pleasure_through_pain'),
(212999, 'spell_warl_legion_pleasure_through_pain_pet_aura'),
(216698, 'spell_warl_legion_reap_souls'),
(205184, 'spell_warl_legion_roaring_blaze'),
(27243,  'spell_warl_legion_seed_of_corruption'),
(27285,  'spell_warl_legion_seed_of_corruption_damage'),
(17877,  'spell_warl_legion_shadowburn'),
(196659, 'spell_warl_legion_shadow_barrage'),
(115232, 'spell_warl_legion_shadow_shield'),
(212623, 'spell_warl_legion_singe_magic'),
(212620, 'spell_warl_legion_singe_magic_damage'),
(212356, 'spell_warl_legion_soulshatter'),
(686,    'spell_warl_legion_shadow_bolt'),
(196236, 'spell_warl_legion_soulsnatcher'),
(215941, 'spell_warl_legion_soul_conduit'),
(205178, 'spell_warl_legion_soul_effigy'),
(205247, 'spell_warl_legion_soul_effigy_periodic'),
(205277, 'spell_warl_legion_soul_effigy_visual'),
(199471, 'spell_warl_legion_soul_flame'),
(196098, 'spell_warl_legion_soul_harvest'),
(108370, 'spell_warl_legion_soul_leech'),
(20707,  'spell_warl_legion_soulstone'),
(211529, 'spell_warl_legion_stolen_power'),
(1122,   'spell_warl_legion_summon_infernal'),
(211714, 'spell_warl_legion_thalkiels_consumption'),
(30108,  'spell_warl_legion_unstable_affliction'),
(233490, 'spell_warl_legion_unstable_affliction_triggered'),
(233496, 'spell_warl_legion_unstable_affliction_triggered'),
(233497, 'spell_warl_legion_unstable_affliction_triggered'),
(233498, 'spell_warl_legion_unstable_affliction_triggered'),
(233499, 'spell_warl_legion_unstable_affliction_triggered'),
(199472, 'spell_warl_legion_wrath_of_consumption');

UPDATE creature_template SET unit_flags = 520, unit_flags2 = 67141664, unit_flags3 = 1, ScriptName = 'npc_warl_legion_demonic_portal' WHERE entry IN (59271, 59262);
UPDATE creature_template SET ScriptName = 'npc_warl_legion_infernal' WHERE entry = 89;
UPDATE creature_template SET ScriptName = 'npc_warl_legion_doomguard' WHERE entry = 11859;
UPDATE creature_template SET ScriptName = 'npc_warl_legion_grimoire_summons' WHERE entry IN (416, 417, 1863, 1860, 17252);
UPDATE creature_template SET ScriptName = 'npc_warl_legion_darkglare', flags_extra = 2 WHERE entry = 103673;
UPDATE creature_template SET ScriptName = 'npc_warl_legion_dreadstalker' WHERE entry = 98035;
UPDATE `creature_template_addon` SET `auras` = '2585' WHERE `entry` = 4277; 
UPDATE creature_template SET ScriptName = 'npc_warl_legion_wild_imp' WHERE entry = 55659;
UPDATE creature_template SET ScriptName = 'npc_warl_legion_eye_of_kilrogg' WHERE entry IN (4277, 107252);
UPDATE creature_template SET ScriptName = 'npc_warl_legion_fel_lord' WHERE entry = 107024;
UPDATE `creature_template` SET `unit_class` = 4 WHERE `entry` = 1863; 
UPDATE creature_template SET ScriptName = 'npc_warl_legion_dimensional_rifts' WHERE entry IN (99887, 94584, 108493);
UPDATE creature_template SET unit_flags3 = 0x1, flags_extra = 0x00000002, ScriptName = 'npc_warl_legion_soul_effigy' WHERE entry = 103679;

UPDATE creature_template_addon SET auras = '' WHERE entry IN (4277, 107252);

DELETE FROM creature_equip_template  WHERE CreatureID = 107024;
INSERT INTO creature_equip_template (CreatureID, ID, ItemID1) VALUES
(107024, 1, 139007);

DELETE FROM npc_spellclick_spells WHERE npc_entry IN (59271, 59262);
INSERT INTO npc_spellclick_spells (npc_entry, spell_id, cast_flags, user_type) VALUES
(59271, 113895, 1, 2),
(59262, 113895, 1, 2);

DELETE FROM `creature_model_info` WHERE `DisplayID` = 74631;
INSERT INTO `creature_model_info` (`DisplayID`, `BoundingRadius`, `CombatReach`) VALUES
(74631, 1.483476, 1.5);

DELETE FROM `creature_template_addon` WHERE entry IN (59271, 59262, 103679, 99887, 94584, 108493);
INSERT INTO `creature_template_addon` (`entry`, `bytes2`, `auras`) VALUES 
(59271, 257, ''),
(59262, 257, ''),
(103679, 1, '205247'),
(99887, 0, '219107'),
(94584, 0, '219290'),
(108493, 0, '219117');

DELETE FROM `creature_template_addon` WHERE entry = 108452;
INSERT INTO `creature_template_addon` (`entry`, `auras`) VALUES
(108452, 19483); 


DELETE FROM `spell_proc` WHERE `SpellId` IN (
235157,205184,196412,233581,219195,211720,205145,205146,219415,196236,199472,199471,201424,221711,212282,200586,117828,205247,193541
);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `Chance`) VALUES
(235157, 0x20, 5,   0x40000,      0x0,  0x0, 0x0, 0x10000, 0x1,   0), -- Empowered Life Tap
(205184,  0x0, 5,       0x0, 0x800000,  0x0, 0x0,     0x0, 0x2,   0), -- Roaring Blaze
(196412,  0x0, 5,       0x0,   0x2000,  0x0, 0x0,     0x0, 0x2,   0), -- Eradication
(233581,  0x0, 5,       0x0, 0x800000,  0x0, 0x0,     0x0, 0x2,   0), -- Entrenched in Flame
(219195,  0x0, 5,      0x80, 0x800000,  0x0, 0x0,     0x0, 0x1,   0), -- Conflagration of Chaos
(211720,  0x0, 5,     0x201,      0x0, 0x40, 0x0,     0x0, 0x2,   0), -- Thal'kiel's Discord
(205145,  0x0, 5,     0x201,      0x0,  0x0, 0x0,     0x0, 0x2, 100), -- Demonic Calling
(205146,  0x0, 5, 0x2000000,      0x0,  0x0, 0x0,     0x0, 0x1,   0), -- Demonic Calling
(219415,  0x0, 5,       0x0,     0x40,  0x0, 0x0,     0x0, 0x1,   0), -- Dimension Ripper
(196236,  0x0, 5,       0x0,   0x2000,  0x0, 0x0,     0x0, 0x1,   0), -- Soulsnatcher
(199472,  0x0, 0,       0x0,      0x0,  0x0, 0x0,     0x2, 0x0, 100), -- Wrath of Consumption
(199471,  0x0, 0,       0x0,      0x0,  0x0, 0x0,     0x2, 0x0,   0), -- Soul Flame
(201424,  0x0, 5,       0x2,      0x0,  0x0, 0x0,     0x0, 0x0,   0), -- Harvester of Souls
(221711,  0x0, 5,  0x800008,      0x0,  0x0, 0x0,     0x0, 0x2,   0), -- Essence Drain
(212282,  0x0, 5,      0x80, 0x800000,  0x0, 0x0,     0x0, 0x2,   0), -- Cremation
(200586,  0x0, 5,       0x0,   0x2000,  0x0, 0x0,     0x0, 0x2,   0), -- Fel Fissure
(117828,  0x0, 5,       0x0,   0x2040,  0x0, 0x0,     0x0, 0x1,   0), -- Backdraft
(205247,  0x0, 5,       0x0,      0x0,  0x0, 0x0, 0xA22A8, 0x0, 100), -- set custom proc event for Soul Effigy periodic dummy (on any damage taken)
(193541,  0x0, 5,      0x10,      0x0,  0x0, 0x0,     0x0, 0x0,   0); -- Immolate

DELETE FROM `areatrigger_template` WHERE `Id` IN (12365, 11457, 10358);
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `VerifiedBuild`) VALUES
(12365, 0, 0, 3, 3, 0, 0, 0, 0, 22566),
(11457, 0, 0, 4, 4, 0, 0, 0, 0, 23360),
(10358, 0, 0, 10, 10, 0, 0, 0, 0, 24461);

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (5420, 6913, 7478, 8138, 6959, 5650);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `MoveCurveId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `VerifiedBuild`) VALUES
(5420, 10133, 0, 0, 0, 0, 0, 0, 7087, 22566), -- SpellId : 5740
(6913, 11417, 0, 0, 0, 0, 0, 0, 6000, 23360), -- SpellId : 211729
(7478, 11908, 0, 0, 0, 0, 0, 0, 90000, 23360), -- SpellId : 216463
(8138, 12365, 0, 0, 0, 0, 0, 0, 10000, 22566), -- SpellId : 221703
(6959, 11457, 0, 0, 0, 0, 0, 0, 15000, 23360), -- SpellId : 212269
(5650, 10358, 0, 0, 0, 0, 0, 0, 10000, 24461); -- SpellId : 200546

UPDATE areatrigger_template SET ScriptName = 'areatrigger_warl_legion_rain_of_fire' WHERE Id = 10133;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_warl_legion_thalkiels_discord' WHERE Id = 11417;

DELETE FROM `spell_areatrigger_scale` WHERE `SpellMiscId` = 6913;
INSERT INTO `spell_areatrigger_scale` (`SpellMiscId`, `ScaleCurveExtra5`, `ScaleCurveExtra6`) VALUES 
(6913, 1, 33753); 

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (12365, 11457, 10358);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES
(12365, 1, 221705, 5),
(11457, 1, 200587, 2),
(10358, 1, 200548, 2);

DELETE FROM spell_linked_spell WHERE spell_trigger IN (-216724, -238144);
INSERT INTO spell_linked_spell (spell_trigger, spell_effect, TYPE, COMMENT) VALUES
(-216724, -216695, 0, 'Warlock - Remove Tormented Souls on Souls of the Damned remove'),
(-238144, -216695, 0, 'Warlock - Remove Tormented Souls on Souls of the Damned remove');

