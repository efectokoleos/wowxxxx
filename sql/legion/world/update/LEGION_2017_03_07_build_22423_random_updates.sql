DELETE FROM `quest_offer_reward` WHERE `ID` IN (43806 /*The Battle for Broken Shore*/, 38729 /*Return to the Black Temple*/, 38728 /*The Keystone*/, 39663 /*On Felbat Wings*/, 39515 /*Vengeance Will Be Mine!*/, 40051 /*Fel Secrets*/, 38819 /*Their Numbers Are Legion*/, 38727 /*Stop the Bombardment*/, 40222 /*The Imp Mother's Tome*/, 38725 /*Into the Foul Creche*/, 39495 /*Hidden No More*/, 39262 /*Give Me Sight Beyond Sight*/, 38813 /*Orders for Your Captains*/, 38766 /*Before We're Overrun*/, 38765 /*Enter the Illidari: Shivarra*/, 39050 /*Meeting With the Queen*/, 40379 /*Enter the Illidari: Coilskar*/, 39049 /*Eye On the Prize*/, 38759 /*Set Them Free*/, 40378 /*Enter the Illidari: Ashtongue*/, 40077 /*The Invasion Begins*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(43806, 1, 1, 0, 0, 0, 0, 0, 0, 'Ah, $n, you''re up! I''m relieved you made it out of that bloodbath alive. We lost so many good women and men down there...$b$b<Genn Greymane clenches his teeth.>$b$bDamn the Horde for serving us up to the Legion!', 22498), -- The Battle for Broken Shore
(38729, 0, 0, 0, 0, 0, 0, 0, 0, '<As you invoke the power of the Sargerite Keystone, the fel portal is suffused with an otherworldly energy.\n\nIt is time to return triumphant to the Black Temple and aid Lord Illidan in repelling the invading forces from Azeroth and Shattrath City.>', 22498), -- Return to the Black Temple
(38728, 1, 0, 0, 0, 0, 0, 0, 0, 'Now that we have the keystone in our possession, there is just one last thing left to do...', 22498), -- The Keystone
(39663, 6, 0, 0, 0, 0, 0, 0, 0, 'Can you sense her power? It will take all of you combined.\n\nEven so... I better go secure our way out of here.', 22498), -- On Felbat Wings
(39515, 1, 274, 0, 0, 0, 0, 0, 0, 'I blame myself, $n. Mannethrel was already struggling. It is a never-ending battle we all must endure... to control the fel energies that we have taken in.\n\nMaybe it would be best if you held off teaching the others for now. We can not afford to lose anyone else.', 22498), -- Vengeance Will Be Mine!
(40051, 273, 0, 0, 0, 0, 0, 0, 0, 'Surprising. I agree. Well chosen, $n.\n\nNow, you must teach the rest of us.', 22498), -- Fel Secrets
(38819, 273, 6, 0, 0, 0, 0, 0, 0, 'I am in complete agreement. From where I''m standing, you utterly decimated their forces.\n\nWhat do you think? Are we ready to fly up to that command center and take the Sargerite Keystone?', 22498), -- Their Numbers Are Legion
(38727, 1, 0, 0, 0, 0, 0, 0, 0, 'True leadership is hard to come by. Leading from the front is an even rarer trait.\n\nWe are just about ready to assail Brood Queen Tyranna''s command center.', 22498), -- Stop the Bombardment
(40222, 6, 0, 0, 0, 0, 0, 0, 0, 'Can you feel the power emanating from the book? I cannot wait to learn its secrets.', 22498), -- The Imp Mother's Tome
(38725, 273, 0, 0, 0, 0, 0, 0, 0, '$n. I''ve found her.', 22498), -- Into the Foul Creche
(39495, 25, 0, 0, 0, 0, 0, 0, 0, 'Excellent. You''ve absorbed that fel lord''s power and become more powerful in-turn.\n\nNow, we can fully concentrate on destroying the Legion and their Brood Queen.', 22498), -- Hidden No More
(39262, 5, 0, 0, 0, 0, 0, 0, 0, 'I KNEW that I''d sensed something foul!', 22498), -- Give Me Sight Beyond Sight
(38813, 273, 0, 0, 0, 0, 0, 0, 0, 'Our forces have engaged the Legion.\n\nSoon we will stand together victorious with Lord Illidan at the Black Temple.', 22498), -- Orders for Your Captains
(38766, 273, 0, 0, 0, 0, 0, 0, 0, 'Already, the flow of demons from below has slowed to a trickle.\n\nI can sense the doom guard''s energy. You have become more powerful.', 22498), -- Before We're Overrun
(38765, 274, 1, 0, 0, 0, 0, 0, 0, 'Grim. But demon hunters are no strangers to sacrifice. We have given everything to become what we are.\n\nThe Legion will fall. We await your orders, $n.', 22498), -- Enter the Illidari: Shivarra
(39050, 1, 0, 0, 0, 0, 0, 0, 0, 'That''s new. She looks like some kind of spider demon. As if the Legion needed spiders...', 22498), -- Meeting With the Queen
(40379, 0, 0, 0, 0, 0, 0, 0, 0, 'Grim business, but we''ve all sacrificed just about everything to get to where we are.\n\nWe will do ANYTHING it takes to defeat the Burning Legion. Anything.', 22498), -- Enter the Illidari: Coilskar
(39049, 273, 0, 0, 0, 0, 0, 0, 0, 'An inquisitor''s essence... perfect. Allari was right.\n\nNow to complete my ritual and spy on the Legion''s leadership.', 22498), -- Eye On the Prize
(38759, 0, 0, 0, 0, 0, 0, 0, 0, 'Their souls were being sucked dry by a jailer demon?\n\nThe Burning Legion in this place is far more powerful and brutal than we have ever seen before.', 22498), -- Set Them Free
(40378, 0, 0, 0, 0, 0, 0, 0, 0, 'I saw them run by. Not the most impressive of our troops, but the Ashtongue have proven effective in the past.', 22498), -- Enter the Illidari: Ashtongue
(40077, 1, 0, 0, 0, 0, 0, 0, 0, 'That ought to get their attention.\n\nNow, onto the business of summoning in the rest of our forces.', 22498); -- The Invasion Begins

DELETE FROM `quest_details` WHERE `ID` IN (38729 /*Return to the Black Temple*/, 38728 /*The Keystone*/, 39663 /*On Felbat Wings*/, 39515 /*Vengeance Will Be Mine!*/, 40051 /*Fel Secrets*/, 40222 /*The Imp Mother's Tome*/, 38725 /*Into the Foul Creche*/, 38819 /*Their Numbers Are Legion*/, 38727 /*Stop the Bombardment*/, 39495 /*Hidden No More*/, 39262 /*Give Me Sight Beyond Sight*/, 38813 /*Orders for Your Captains*/, 38766 /*Before We're Overrun*/, 38765 /*Enter the Illidari: Shivarra*/, 39050 /*Meeting With the Queen*/, 40379 /*Enter the Illidari: Coilskar*/, 39049 /*Eye On the Prize*/, 38759 /*Set Them Free*/, 40378 /*Enter the Illidari: Ashtongue*/, 40077 /*The Invasion Begins*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(38729, 5, 0, 0, 0, 0, 0, 0, 0, 22498), -- Return to the Black Temple
(38728, 396, 0, 0, 0, 0, 0, 0, 0, 22498), -- The Keystone
(39663, 1, 0, 0, 0, 0, 0, 0, 0, 22498), -- On Felbat Wings
(39515, 273, 0, 0, 0, 0, 0, 0, 0, 22498), -- Vengeance Will Be Mine!
(40051, 1, 0, 0, 0, 0, 0, 0, 0, 22498), -- Fel Secrets
(40222, 0, 0, 0, 0, 0, 0, 0, 0, 22498), -- The Imp Mother's Tome
(38725, 1, 0, 0, 0, 0, 0, 0, 0, 22498), -- Into the Foul Creche
(38819, 1, 25, 0, 0, 0, 0, 0, 0, 22498), -- Their Numbers Are Legion
(38727, 274, 0, 0, 0, 0, 0, 0, 0, 22498), -- Stop the Bombardment
(39495, 1, 25, 0, 0, 0, 0, 0, 0, 22498), -- Hidden No More
(39262, 6, 0, 0, 0, 0, 0, 0, 0, 22498), -- Give Me Sight Beyond Sight
(38813, 1, 0, 0, 0, 0, 0, 0, 0, 22498), -- Orders for Your Captains
(38766, 1, 0, 0, 0, 0, 0, 0, 0, 22498), -- Before We're Overrun
(38765, 0, 0, 0, 0, 0, 0, 0, 0, 22498), -- Enter the Illidari: Shivarra
(39050, 1, 0, 0, 0, 0, 0, 0, 0, 22498), -- Meeting With the Queen
(40379, 1, 0, 0, 0, 0, 0, 0, 0, 22498), -- Enter the Illidari: Coilskar
(39049, 0, 0, 0, 0, 0, 0, 0, 0, 22498), -- Eye On the Prize
(38759, 0, 0, 0, 0, 0, 0, 0, 0, 22498), -- Set Them Free
(40378, 0, 0, 0, 0, 0, 0, 0, 0, 22498), -- Enter the Illidari: Ashtongue
(40077, 0, 0, 0, 0, 0, 0, 0, 0, 22498); -- The Invasion Begins

DELETE FROM `quest_request_items` WHERE `ID` IN (38728 /*The Keystone*/, 40222 /*The Imp Mother's Tome*/, 38759 /*Set Them Free*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(38728, 6, 0, 0, 0, '$n, are you ready?', 22498), -- The Keystone
(40222, 6, 0, 0, 0, 'Do you have it?', 22498), -- The Imp Mother's Tome
(38759, 6, 0, 0, 0, 'Some of our demon hunters are missing. Did you see them back on the Molten Shore?', 22498); -- Set Them Free

UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=22498 WHERE `DisplayID`=73059;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22498 WHERE `DisplayID`=73275;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=22498 WHERE `DisplayID`=70146;
UPDATE `creature_model_info` SET `BoundingRadius`=1.2, `CombatReach`=1.8, `VerifiedBuild`=22498 WHERE `DisplayID`=64741;
UPDATE `creature_model_info` SET `BoundingRadius`=3.877664, `CombatReach`=1.5, `VerifiedBuild`=22498 WHERE `DisplayID`=66009;

DELETE FROM `npc_vendor` WHERE (`entry`=96650 AND `item`=123960 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=123959 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=112460 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=112459 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=112461 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=112462 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=129181 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=112458 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133323 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133325 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133318 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133319 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133321 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=133322 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96650 AND `item`=132752 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(96650, 25, 123960, 0, 0, 1, 0, 0, 22498), -- Charm of Demonic Fire
(96650, 24, 123959, 0, 0, 1, 0, 0, 22498), -- Demon Trophy
(96650, 23, 112460, 0, 0, 1, 0, 0, 22498), -- Illidari Band
(96650, 22, 112459, 0, 0, 1, 0, 0, 22498), -- Illidari Ring
(96650, 21, 112461, 0, 0, 1, 0, 0, 22498), -- Illidari Chain
(96650, 20, 112462, 0, 0, 1, 0, 0, 22498), -- Illidari Drape
(96650, 19, 129181, 0, 0, 1, 0, 0, 22498), -- Illidari Warglaive
(96650, 18, 112458, 0, 0, 1, 0, 0, 22498), -- Illidari Warglaive
(96650, 9, 133323, 0, 0, 1, 0, 0, 22498), -- Illidari Bracers
(96650, 8, 133325, 0, 0, 1, 0, 0, 22498), -- Illidari Belt
(96650, 7, 133318, 0, 0, 1, 0, 0, 22498), -- Illidari Shoulders
(96650, 6, 133319, 0, 0, 1, 0, 0, 22498), -- Illidari Leggings
(96650, 5, 133320, 0, 0, 1, 0, 0, 22498), -- Illidari Blindfold
(96650, 4, 133321, 0, 0, 1, 0, 0, 22498), -- Illidari Gloves
(96650, 3, 133324, 0, 0, 1, 0, 0, 22498), -- Illidari Boots
(96650, 2, 133322, 0, 0, 1, 0, 0, 22498), -- Illidari Robe
(96650, 1, 132752, 0, 0, 1, 0, 0, 22498); -- Illidari Rations

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=114756 AND `ID`=1) OR (`CreatureID`=93708 AND `ID`=1) OR (`CreatureID`=113036 AND `ID`=1) OR (`CreatureID`=92776 AND `ID`=1) OR (`CreatureID`=96656 AND `ID`=1) OR (`CreatureID`=96847 AND `ID`=1) OR (`CreatureID`=92718 AND `ID`=1) OR (`CreatureID`=97069 AND `ID`=1) OR (`CreatureID`=97965 AND `ID`=1) OR (`CreatureID`=97964 AND `ID`=1) OR (`CreatureID`=98713 AND `ID`=1) OR (`CreatureID`=98714 AND `ID`=1) OR (`CreatureID`=97303 AND `ID`=1) OR (`CreatureID`=97244 AND `ID`=1) OR (`CreatureID`=97962 AND `ID`=1) OR (`CreatureID`=97959 AND `ID`=1) OR (`CreatureID`=98712 AND `ID`=1) OR (`CreatureID`=97297 AND `ID`=1) OR (`CreatureID`=97971 AND `ID`=1) OR (`CreatureID`=99045 AND `ID`=1) OR (`CreatureID`=101317 AND `ID`=1) OR (`CreatureID`=100543 AND `ID`=1) OR (`CreatureID`=97881 AND `ID`=1) OR (`CreatureID`=101597 AND `ID`=1) OR (`CreatureID`=97676 AND `ID`=1) OR (`CreatureID`=97058 AND `ID`=1) OR (`CreatureID`=97771 AND `ID`=1) OR (`CreatureID`=97770 AND `ID`=1) OR (`CreatureID`=96561 AND `ID`=1) OR (`CreatureID`=99419 AND `ID`=1) OR (`CreatureID`=103432 AND `ID`=1) OR (`CreatureID`=99423 AND `ID`=1) OR (`CreatureID`=98711 AND `ID`=1) OR (`CreatureID`=98158 AND `ID`=1) OR (`CreatureID`=98165 AND `ID`=1) OR (`CreatureID`=98160 AND `ID`=1) OR (`CreatureID`=108408 AND `ID`=1) OR (`CreatureID`=108409 AND `ID`=1) OR (`CreatureID`=96402 AND `ID`=1) OR (`CreatureID`=96277 AND `ID`=1) OR (`CreatureID`=97057 AND `ID`=1) OR (`CreatureID`=100548 AND `ID`=1) OR (`CreatureID`=100545 AND `ID`=1) OR (`CreatureID`=97459 AND `ID`=1) OR (`CreatureID`=99787 AND `ID`=1) OR (`CreatureID`=96562 AND `ID`=1) OR (`CreatureID`=100549 AND `ID`=1) OR (`CreatureID`=93716 AND `ID`=3) OR (`CreatureID`=96494 AND `ID`=2) OR (`CreatureID`=97601 AND `ID`=1) OR (`CreatureID`=96499 AND `ID`=1) OR (`CreatureID`=96473 AND `ID`=1) OR (`CreatureID`=97597 AND `ID`=1) OR (`CreatureID`=102726 AND `ID`=1) OR (`CreatureID`=96494 AND `ID`=1) OR (`CreatureID`=96441 AND `ID`=1) OR (`CreatureID`=97598 AND `ID`=1) OR (`CreatureID`=96493 AND `ID`=1) OR (`CreatureID`=101397 AND `ID`=1) OR (`CreatureID`=101790 AND `ID`=1) OR (`CreatureID`=98157 AND `ID`=1) OR (`CreatureID`=101789 AND `ID`=1) OR (`CreatureID`=101788 AND `ID`=1) OR (`CreatureID`=96436 AND `ID`=1) OR (`CreatureID`=96500 AND `ID`=1) OR (`CreatureID`=96504 AND `ID`=1) OR (`CreatureID`=101787 AND `ID`=1) OR (`CreatureID`=96501 AND `ID`=1) OR (`CreatureID`=96502 AND `ID`=1) OR (`CreatureID`=97014 AND `ID`=1) OR (`CreatureID`=97034 AND `ID`=1) OR (`CreatureID`=96653 AND `ID`=1) OR (`CreatureID`=102906 AND `ID`=1) OR (`CreatureID`=102905 AND `ID`=1) OR (`CreatureID`=102907 AND `ID`=1) OR (`CreatureID`=90247 AND `ID`=1) OR (`CreatureID`=95449 AND `ID`=1) OR (`CreatureID`=95450 AND `ID`=1) OR (`CreatureID`=95447 AND `ID`=1) OR (`CreatureID`=96650 AND `ID`=1) OR (`CreatureID`=96420 AND `ID`=1) OR (`CreatureID`=93127 AND `ID`=1) OR (`CreatureID`=105945 AND `ID`=1) OR (`CreatureID`=96253 AND `ID`=1) OR (`CreatureID`=96655 AND `ID`=1) OR (`CreatureID`=97600 AND `ID`=1) OR (`CreatureID`=102910 AND `ID`=1) OR (`CreatureID`=113927 AND `ID`=1) OR (`CreatureID`=96652 AND `ID`=1) OR (`CreatureID`=97604 AND `ID`=1) OR (`CreatureID`=113924 AND `ID`=1) OR (`CreatureID`=93693 AND `ID`=1) OR (`CreatureID`=102724 AND `ID`=1) OR (`CreatureID`=96231 AND `ID`=1) OR (`CreatureID`=96230 AND `ID`=1) OR (`CreatureID`=96931 AND `ID`=1) OR (`CreatureID`=96654 AND `ID`=1) OR (`CreatureID`=96930 AND `ID`=1) OR (`CreatureID`=93716 AND `ID`=2) OR (`CreatureID`=97603 AND `ID`=1) OR (`CreatureID`=93716 AND `ID`=1) OR (`CreatureID`=98611 AND `ID`=1) OR (`CreatureID`=99915 AND `ID`=1) OR (`CreatureID`=93221 AND `ID`=1) OR (`CreatureID`=95046 AND `ID`=1) OR (`CreatureID`=102714 AND `ID`=1) OR (`CreatureID`=96400 AND `ID`=1) OR (`CreatureID`=94654 AND `ID`=1) OR (`CreatureID`=99917 AND `ID`=1) OR (`CreatureID`=93105 AND `ID`=1) OR (`CreatureID`=93759 AND `ID`=1) OR (`CreatureID`=93112 AND `ID`=2) OR (`CreatureID`=94704 AND `ID`=1) OR (`CreatureID`=94705 AND `ID`=1) OR (`CreatureID`=93112 AND `ID`=1) OR (`CreatureID`=100982 AND `ID`=1) OR (`CreatureID`=97592 AND `ID`=1) OR (`CreatureID`=94410 AND `ID`=1) OR (`CreatureID`=99916 AND `ID`=1) OR (`CreatureID`=98354 AND `ID`=1) OR (`CreatureID`=98229 AND `ID`=1) OR (`CreatureID`=98622 AND `ID`=1) OR (`CreatureID`=98457 AND `ID`=1) OR (`CreatureID`=99919 AND `ID`=1) OR (`CreatureID`=98460 AND `ID`=1) OR (`CreatureID`=98459 AND `ID`=1) OR (`CreatureID`=98458 AND `ID`=1) OR (`CreatureID`=98456 AND `ID`=1) OR (`CreatureID`=98486 AND `ID`=1) OR (`CreatureID`=98484 AND `ID`=1) OR (`CreatureID`=93011 AND `ID`=1) OR (`CreatureID`=97712 AND `ID`=1) OR (`CreatureID`=98228 AND `ID`=1) OR (`CreatureID`=98227 AND `ID`=1) OR (`CreatureID`=99918 AND `ID`=1) OR (`CreatureID`=98292 AND `ID`=1) OR (`CreatureID`=98290 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(114756, 1, 128279, 0, 0), -- Dread Knight Zak'gal
(93708, 1, 12754, 0, 0), -- Thunder Bluff Brave
(113036, 1, 139022, 0, 0), -- Fel Lord Razzar
(92776, 1, 119458, 0, 0), -- Fel Shocktrooper
(96656, 1, 128359, 0, 128371), -- Freed Illidari
(96847, 1, 97129, 0, 95791), -- Drelanim Whisperwind
(92718, 1, 32425, 0, 0), -- Maiev Shadowsong
(97069, 1, 124026, 0, 0), -- Wrath-Lord Lekos
(97965, 1, 122430, 0, 0), -- Allari the Souleater
(97964, 1, 128360, 0, 128370), -- Jace Darkweaver
(98713, 1, 128359, 0, 128371), -- Kor'vas Bloodthorn
(98714, 1, 128359, 0, 128371), -- Cyana Nightglaive
(97303, 1, 128359, 0, 128371), -- Kayn Sunfury
(97244, 1, 128359, 0, 128371), -- Kayn Sunfury
(97962, 1, 122430, 0, 0), -- Allari the Souleater
(97959, 1, 128360, 0, 128370), -- Jace Darkweaver
(98712, 1, 128359, 0, 128371), -- Kor'vas Bloodthorn
(97297, 1, 128359, 0, 128371), -- Cyana Nightglaive
(97971, 1, 128360, 0, 128370), -- Jace Darkweaver Clone
(99045, 1, 128359, 0, 128371), -- Kor'vas Bloodthorn
(101317, 1, 128360, 0, 128370), -- Illysanna Ravencrest
(100543, 1, 128358, 0, 128372), -- Demon Hunter
(97881, 1, 14535, 0, 0), -- Male Naga (scale 2.00)
(101597, 1, 128358, 0, 128372), -- Tirathon Saltheril
(97676, 1, 120477, 0, 0), -- Lady S'theno
(97058, 1, 138757, 0, 0), -- Count Nefarious
(97771, 1, 28965, 0, 0), -- Coilskar Myrmidon
(97770, 1, 14535, 0, 0), -- Coilskar Harpooner
(96561, 1, 14535, 0, 0), -- Coilskar Harpooner
(99419, 1, 128361, 0, 128369), -- Baric Stormrunner
(103432, 1, 126276, 0, 0), -- Queen's Centurion
(99423, 1, 128361, 0, 128369), -- Zaria Shadowheart
(98711, 1, 128359, 0, 128371), -- Kor'vas Bloodthorn
(98158, 1, 128359, 0, 128371), -- Asha Ravensong
(98165, 1, 128359, 0, 128371), -- Cassiel Nightthorn
(98160, 1, 128361, 0, 128369), -- Sirius Ebonwing
(108408, 1, 128360, 0, 128370), -- Ariana Fireheart
(108409, 1, 127651, 0, 127651), -- Fury Champion
(96402, 1, 128519, 0, 0), -- Hulking Forgefiend
(96277, 1, 126276, 0, 0), -- Queen's Centurion
(97057, 1, 138740, 0, 0), -- Overseer Brutarg
(100548, 1, 128358, 0, 128372), -- Demon Hunter
(100545, 1, 128358, 0, 128372), -- Demon Hunter
(97459, 1, 17383, 0, 0), -- Battlelord Gaardoun
(99787, 1, 128358, 0, 128372), -- Marius Felbane
(96562, 1, 31669, 0, 31669), -- Ashtongue Stalker
(100549, 1, 128358, 0, 128372), -- Demon Hunter
(93716, 3, 133005, 0, 138751), -- Doom Slayer
(96494, 2, 133332, 0, 0), -- Felguard Butcher
(97601, 1, 28133, 0, 28133), -- Shivarra Destroyer
(96499, 1, 128360, 0, 128370), -- Jace Darkweaver
(96473, 1, 138749, 0, 0), -- Eredar Sorcerer
(97597, 1, 138749, 0, 0), -- Eredar Sorcerer
(102726, 1, 138749, 0, 0), -- Eredar Sorcerer
(96494, 1, 124360, 0, 0), -- Felguard Butcher
(96441, 1, 124524, 0, 0), -- Fel Lord Caza
(97598, 1, 124360, 0, 0), -- Felguard Butcher
(96493, 1, 124360, 0, 0), -- Felguard Butcher
(101397, 1, 128359, 0, 128371), -- Cailyn Paledoom
(101790, 1, 128358, 0, 128372), -- Demon Hunter
(98157, 1, 128359, 0, 128371), -- Lyana Darksorrow
(101789, 1, 128358, 0, 128372), -- Demon Hunter
(101788, 1, 128358, 0, 128372), -- Demon Hunter
(96436, 1, 128360, 0, 128370), -- Jace Darkweaver
(96500, 1, 17383, 0, 0), -- Ashtongue Warrior
(96504, 1, 28133, 0, 28133), -- Shivarra Destroyer
(101787, 1, 128358, 0, 128372), -- Demon Hunter
(96501, 1, 29685, 0, 0), -- Ashtongue Mystic
(96502, 1, 28965, 0, 0), -- Coilskar Myrmidon
(97014, 1, 138747, 0, 0), -- Vile Soulmaster
(97034, 1, 127651, 0, 127651), -- Fury Champion
(96653, 1, 128358, 0, 128372), -- Izal Whitemoon
(102906, 1, 31669, 0, 31669), -- Ashtongue Stalker
(102905, 1, 29685, 0, 0), -- Ashtongue Mystic
(102907, 1, 17383, 0, 0), -- Ashtongue Warrior
(90247, 1, 17383, 0, 0), -- Battlelord Gaardoun
(95449, 1, 29685, 0, 0), -- Ashtongue Mystic
(95450, 1, 31669, 0, 31669), -- Ashtongue Stalker
(95447, 1, 17383, 0, 0), -- Ashtongue Warrior
(96650, 1, 128361, 0, 128369), -- Falara Nightsong
(96420, 1, 128359, 0, 128371), -- Cyana Nightglaive
(93127, 1, 128359, 0, 128371), -- Kayn Sunfury
(105945, 1, 128361, 0, 128369), -- Sevis Brightflame
(96253, 1, 28133, 0, 28133), -- Shivarra Destroyer
(96655, 1, 122430, 0, 0), -- Allari the Souleater
(97600, 1, 128358, 0, 128372), -- Demon Hunter
(102910, 1, 28965, 0, 0), -- Coilskar Myrmidon
(113927, 1, 128358, 0, 128372), -- Illidari Kilbride
(96652, 1, 128361, 0, 128369), -- Mannethrel Darkstar
(97604, 1, 126276, 0, 0), -- Queen's Centurion
(113924, 1, 128358, 0, 128372), -- Illidari Starr
(93693, 1, 120477, 0, 0), -- Lady S'theno
(102724, 1, 138747, 0, 0), -- Vile Soulmaster
(96231, 1, 14535, 0, 0), -- Coilskar Harpooner
(96230, 1, 28965, 0, 0), -- Coilskar Myrmidon
(96931, 1, 128358, 0, 128372), -- Demon Hunter
(96654, 1, 128361, 0, 128369), -- Belath Dawnblade
(96930, 1, 128358, 0, 128372), -- Demon Hunter
(93716, 2, 138751, 0, 138751), -- Doom Slayer
(97603, 1, 138751, 0, 138751), -- Doom Slayer
(93716, 1, 32743, 0, 138751), -- Doom Slayer
(98611, 1, 50043, 0, 0), -- Doomguard Eradicator
(99915, 1, 128361, 0, 128369), -- Sevis Brightflame
(93221, 1, 138481, 0, 0), -- Doom Commander Beliash
(95046, 1, 138749, 0, 0), -- Eredar Summoner
(102714, 1, 138749, 0, 0), -- Eredar Summoner
(96400, 1, 128521, 0, 0), -- Mo'arg Brute
(94654, 1, 138754, 0, 0), -- Doomguard Eradicator
(99917, 1, 128361, 0, 128369), -- Sevis Brightflame
(93105, 1, 128093, 0, 0), -- Inquisitor Baleful
(93759, 1, 128360, 0, 128370), -- Jace Darkweaver
(93112, 2, 136805, 0, 0), -- Felguard Sentry
(94704, 1, 128358, 0, 128372), -- Demon Hunter
(94705, 1, 128358, 0, 128372), -- Demon Hunter
(93112, 1, 136788, 0, 0), -- Felguard Sentry
(100982, 1, 128369, 0, 128369), -- Sevis Brightflame
(97592, 1, 136788, 0, 0), -- Felguard Sentry
(94410, 1, 122430, 0, 0), -- Allari the Souleater
(99916, 1, 128361, 0, 128369), -- Sevis Brightflame
(98354, 1, 128359, 0, 128371), -- Kor'vas Bloodthorn
(98229, 1, 128359, 0, 128371), -- Kayn Sunfury
(98622, 1, 128521, 0, 0), -- Mo'arg Brute
(98457, 1, 128359, 0, 128371), -- Cyana Nightglaive
(99919, 1, 128361, 0, 128369), -- Sevis Brightflame
(98460, 1, 128359, 0, 128371), -- Kor'vas Bloodthorn
(98459, 1, 128359, 0, 128371), -- Kayn Sunfury
(98458, 1, 128360, 0, 128370), -- Jace Darkweaver
(98456, 1, 122430, 0, 0), -- Allari the Souleater
(98486, 1, 127651, 0, 0), -- Wrath Warrior
(98484, 1, 128521, 0, 0), -- Mo'arg Brute
(93011, 1, 128359, 0, 128371), -- Kayn Sunfury
(97712, 1, 127651, 0, 0), -- Wrath Warrior
(98228, 1, 128360, 0, 128370), -- Jace Darkweaver
(98227, 1, 122430, 0, 0), -- Allari the Souleater
(99918, 1, 128361, 0, 128369), -- Sevis Brightflame
(98292, 1, 128359, 0, 128371), -- Kor'vas Bloodthorn
(98290, 1, 128359, 0, 128371); -- Cyana Nightglaive

UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1961 WHERE (`CreatureID`=911 AND `ID`=1); -- Llane Beshere
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=143 WHERE (`CreatureID`=197 AND `ID`=1); -- Marshal McBride
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=43278 AND `ID`=1); -- Ashley Blank
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10617 WHERE (`CreatureID`=49874 AND `ID`=1); -- Blackrock Spy
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=21572 WHERE (`CreatureID`=49869 AND `ID`=1); -- Stormwind Infantry
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=143 WHERE (`CreatureID`=42216 AND `ID`=1); -- Stormwind Army Registrar
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=21572 WHERE (`CreatureID`=42218 AND `ID`=1); -- Stormwind Royal Guard

DELETE FROM `gameobject_template` WHERE `entry` IN (246353 /*Small Treasure Chest*/, 245996 /*Small Treasure Chest*/, 245316 /*Small Treasure Chest*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(246353, 3, 33263, 'Small Treasure Chest', '', 'Opening', '', 1, 57, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 21400, 0, 0, 0, 110, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64215, 0, 0, 98, 22498), -- Small Treasure Chest
(245996, 3, 33263, 'Small Treasure Chest', '', 'Opening', '', 1, 57, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 21400, 0, 0, 0, 110, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64095, 0, 0, 98, 22498), -- Small Treasure Chest
(245316, 3, 33263, 'Small Treasure Chest', '', 'Opening', '', 1, 57, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 21400, 0, 0, 0, 110, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62804, 0, 0, 98, 22498); -- Small Treasure Chest

DELETE FROM `npc_text` WHERE `ID` IN (29837 /*29837*/, 29277 /*29277*/, 27999 /*27999*/, 27773 /*27773*/, 27772 /*27772*/);
INSERT INTO `npc_text` (`ID`, `Probability0`, `Probability1`, `Probability2`, `Probability3`, `Probability4`, `Probability5`, `Probability6`, `Probability7`, `BroadcastTextId0`, `BroadcastTextId1`, `BroadcastTextId2`, `BroadcastTextId3`, `BroadcastTextId4`, `BroadcastTextId5`, `BroadcastTextId6`, `BroadcastTextId7`, `VerifiedBuild`) VALUES
(29837, 1, 0, 0, 0, 0, 0, 0, 0, 118580, 0, 0, 0, 0, 0, 0, 0, 22498), -- 29837
(29277, 1, 0, 0, 0, 0, 0, 0, 0, 111964, 0, 0, 0, 0, 0, 0, 0, 22498), -- 29277
(27999, 1, 0, 0, 0, 0, 0, 0, 0, 103328, 0, 0, 0, 0, 0, 0, 0, 22498), -- 27999
(27773, 1, 0, 0, 0, 0, 0, 0, 0, 101661, 0, 0, 0, 0, 0, 0, 0, 22498), -- 27773
(27772, 1, 0, 0, 0, 0, 0, 0, 0, 101660, 0, 0, 0, 0, 0, 0, 0, 22498); -- 27772
