DELETE FROM `creature_equip_template` WHERE (`CreatureID`=103482 AND `ID`=1) OR (`CreatureID`=96679 AND `ID`=1) OR (`CreatureID`=112392 AND `ID`=1) OR (`CreatureID`=96572 AND `ID`=1) OR (`CreatureID`=23698 AND `ID`=2) OR (`CreatureID`=24484 AND `ID`=7) OR (`CreatureID`=24484 AND `ID`=6) OR (`CreatureID`=24484 AND `ID`=5) OR (`CreatureID`=40891 AND `ID`=1) OR (`CreatureID`=24484 AND `ID`=4) OR (`CreatureID`=24484 AND `ID`=3) OR (`CreatureID`=24484 AND `ID`=2) OR (`CreatureID`=24484 AND `ID`=1) OR (`CreatureID`=89830 AND `ID`=1) OR (`CreatureID`=19175 AND `ID`=1) OR (`CreatureID`=19177 AND `ID`=1) OR (`CreatureID`=106518 AND `ID`=1) OR (`CreatureID`=94365 AND `ID`=1) OR (`CreatureID`=94358 AND `ID`=1) OR (`CreatureID`=100659 AND `ID`=1) OR (`CreatureID`=109448 AND `ID`=1) OR (`CreatureID`=114288 AND `ID`=1) OR (`CreatureID`=100733 AND `ID`=1) OR (`CreatureID`=114289 AND `ID`=1) OR (`CreatureID`=99307 AND `ID`=1) OR (`CreatureID`=99033 AND `ID`=1) OR (`CreatureID`=96754 AND `ID`=1) OR (`CreatureID`=97185 AND `ID`=1) OR (`CreatureID`=98919 AND `ID`=1) OR (`CreatureID`=97365 AND `ID`=1) OR (`CreatureID`=97182 AND `ID`=1) OR (`CreatureID`=98973 AND `ID`=1) OR (`CreatureID`=97097 AND `ID`=1) OR (`CreatureID`=99188 AND `ID`=1) OR (`CreatureID`=102326 AND `ID`=1) OR (`CreatureID`=98291 AND `ID`=1) OR (`CreatureID`=96756 AND `ID`=1) OR (`CreatureID`=102894 AND `ID`=1) OR (`CreatureID`=102896 AND `ID`=1) OR (`CreatureID`=97043 AND `ID`=1) OR (`CreatureID`=102375 AND `ID`=1) OR (`CreatureID`=97200 AND `ID`=1) OR (`CreatureID`=114712 AND `ID`=1) OR (`CreatureID`=102104 AND `ID`=1) OR (`CreatureID`=105748 AND `ID`=1) OR (`CreatureID`=105746 AND `ID`=1) OR (`CreatureID`=111962 AND `ID`=1) OR (`CreatureID`=112267 AND `ID`=1) OR (`CreatureID`=105473 AND `ID`=3) OR (`CreatureID`=99531 AND `ID`=1) OR (`CreatureID`=105472 AND `ID`=2) OR (`CreatureID`=105472 AND `ID`=1) OR (`CreatureID`=105473 AND `ID`=2) OR (`CreatureID`=105473 AND `ID`=1) OR (`CreatureID`=112262 AND `ID`=1) OR (`CreatureID`=106517 AND `ID`=1) OR (`CreatureID`=106519 AND `ID`=1) OR (`CreatureID`=112208 AND `ID`=1) OR (`CreatureID`=112199 AND `ID`=1) OR (`CreatureID`=103004 AND `ID`=1) OR (`CreatureID`=112597 AND `ID`=1) OR (`CreatureID`=96746 AND `ID`=1) OR (`CreatureID`=96758 AND `ID`=1) OR (`CreatureID`=96745 AND `ID`=1) OR (`CreatureID`=96747 AND `ID`=1) OR (`CreatureID`=113257 AND `ID`=1) OR (`CreatureID`=112318 AND `ID`=1) OR (`CreatureID`=101028 AND `ID`=1) OR (`CreatureID`=102095 AND `ID`=1) OR (`CreatureID`=100485 AND `ID`=1) OR (`CreatureID`=99426 AND `ID`=1) OR (`CreatureID`=99857 AND `ID`=1) OR (`CreatureID`=99858 AND `ID`=1) OR (`CreatureID`=98706 AND `ID`=1) OR (`CreatureID`=102788 AND `ID`=1) OR (`CreatureID`=102094 AND `ID`=1) OR (`CreatureID`=98691 AND `ID`=1) OR (`CreatureID`=98696 AND `ID`=1) OR (`CreatureID`=112725 AND `ID`=1) OR (`CreatureID`=98243 AND `ID`=1) OR (`CreatureID`=98970 AND `ID`=1) OR (`CreatureID`=98965 AND `ID`=1) OR (`CreatureID`=98949 AND `ID`=1) OR (`CreatureID`=111290 AND `ID`=1) OR (`CreatureID`=98538 AND `ID`=1) OR (`CreatureID`=98521 AND `ID`=1) OR (`CreatureID`=98368 AND `ID`=1) OR (`CreatureID`=98370 AND `ID`=1) OR (`CreatureID`=98366 AND `ID`=1) OR (`CreatureID`=95771 AND `ID`=1) OR (`CreatureID`=99366 AND `ID`=1) OR (`CreatureID`=101679 AND `ID`=1) OR (`CreatureID`=100400 AND `ID`=1) OR (`CreatureID`=99358 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(103482, 1, 34337, 0, 0), -- Tae'thelan Bloodwatcher
(96679, 1, 140345, 0, 0), -- Aerylia
(112392, 1, 140747, 0, 0), -- Quartermaster Durnolf
(96572, 1, 137174, 0, 140748), -- Stormforged Valarjar
(23698, 2, 2703, 0, 0), -- Drunken Brewfest Reveler
(24484, 7, 46733, 0, 0), -- Brewfest Reveler
(24484, 6, 37059, 0, 0), -- Brewfest Reveler
(24484, 5, 2704, 0, 0), -- Brewfest Reveler
(40891, 1, 5956, 0, 0), -- -Unknown-
(24484, 4, 2703, 0, 0), -- Brewfest Reveler
(24484, 3, 13861, 0, 0), -- Brewfest Reveler
(24484, 2, 2703, 0, 13859), -- Brewfest Reveler
(24484, 1, 2705, 0, 0), -- Brewfest Reveler
(89830, 1, 33161, 0, 0), -- Brew Vendor
(19175, 1, 2704, 0, 0), -- Orc Commoner
(19177, 1, 2703, 0, 13859), -- Troll Commoner
(106518, 1, 27862, 0, 0), -- -Unknown-
(94365, 1, 120978, 14, 0), -- -Unknown-
(94358, 1, 120978, 14, 0), -- -Unknown-
(100659, 1, 124548, 0, 34590), -- Chosen of Eyir
(109448, 1, 0, 0, 44241), -- -Unknown-
(114288, 1, 106837, 0, 106837), -- -Unknown-
(100733, 1, 1925, 0, 130101), -- -Unknown-
(114289, 1, 129723, 0, 115802), -- -Unknown-
(99307, 1, 1925, 0, 130101), -- -Unknown-
(99033, 1, 35942, 0, 0), -- -Unknown-
(96754, 1, 88793, 0, 0), -- -Unknown-
(97185, 1, 115802, 0, 0), -- -Unknown-
(98919, 1, 77076, 0, 75009), -- -Unknown-
(97365, 1, 35942, 0, 0), -- -Unknown-
(97182, 1, 118083, 0, 6341), -- -Unknown-
(98973, 1, 106837, 0, 106837), -- -Unknown-
(97097, 1, 76287, 0, 107823), -- -Unknown-
(99188, 1, 113562, 0, 56173), -- -Unknown-
(102326, 1, 81357, 0, 0), -- -Unknown-
(98291, 1, 81357, 0, 0), -- -Unknown-
(96756, 1, 129189, 0, 0), -- -Unknown-
(102894, 1, 130196, 0, 0), -- -Unknown-
(102896, 1, 127346, 0, 77408), -- -Unknown-
(97043, 1, 81357, 0, 0), -- -Unknown-
(102375, 1, 61297, 0, 0), -- -Unknown-
(97200, 1, 132557, 0, 0), -- -Unknown-
(114712, 1, 61297, 0, 0), -- -Unknown-
(102104, 1, 127346, 0, 77408), -- -Unknown-
(105748, 1, 128100, 0, 0), -- Helarjar Mistcaller
(105746, 1, 33544, 0, 33544), -- Helarjar Berserker
(111962, 1, 128361, 0, 128369), -- -Unknown-
(112267, 1, 19903, 0, 2040), -- -Unknown-
(105473, 3, 116918, 0, 116918), -- -Unknown-
(99531, 1, 58137, 0, 110159), -- -Unknown-
(105472, 2, 12788, 0, 12788), -- -Unknown-
(105472, 1, 35810, 0, 35810), -- -Unknown-
(105473, 2, 99983, 0, 99983), -- -Unknown-
(105473, 1, 107673, 0, 107673), -- -Unknown-
(112262, 1, 107673, 0, 107673), -- -Unknown-
(106517, 1, 0, 0, 44241), -- -Unknown-
(106519, 1, 1908, 0, 0), -- -Unknown-
(112208, 1, 10591, 0, 10591), -- -Unknown-
(112199, 1, 21398, 0, 57033), -- -Unknown-
(103004, 1, 79722, 0, 0), -- -Unknown-
(112597, 1, 50050, 0, 23907), -- -Unknown-
(96746, 1, 29619, 0, 111464), -- -Unknown-
(96758, 1, 10591, 0, 58938), -- -Unknown-
(96745, 1, 127381, 0, 127381), -- -Unknown-
(96747, 1, 90332, 0, 90332), -- -Unknown-
(113257, 1, 13024, 0, 0), -- -Unknown-
(112318, 1, 109597, 0, 0), -- -Unknown-
(101028, 1, 65483, 0, 0), -- -Unknown-
(102095, 1, 109606, 0, 0), -- -Unknown-
(100485, 1, 83756, 0, 0), -- -Unknown-
(99426, 1, 118562, 0, 0), -- -Unknown-
(99857, 1, 116571, 0, 0), -- -Unknown-
(99858, 1, 29437, 0, 0), -- -Unknown-
(98706, 1, 128191, 0, 63743), -- -Unknown-
(102788, 1, 125797, 0, 0), -- -Unknown-
(102094, 1, 109637, 0, 0), -- -Unknown-
(98691, 1, 15215, 0, 53300), -- -Unknown-
(98696, 1, 128956, 0, 132243), -- -Unknown-
(112725, 1, 116547, 0, 133265), -- -Unknown-
(98243, 1, 87510, 0, 0), -- -Unknown-
(98970, 1, 65483, 0, 0), -- -Unknown-
(98965, 1, 132470, 0, 0), -- -Unknown-
(98949, 1, 128519, 0, 0), -- -Unknown-
(111290, 1, 138422, 0, 138422), -- -Unknown-
(98538, 1, 29437, 0, 29437), -- -Unknown-
(98521, 1, 116571, 0, 0), -- -Unknown-
(98368, 1, 13182, 0, 54932), -- -Unknown-
(98370, 1, 30012, 0, 0), -- -Unknown-
(98366, 1, 40408, 0, 0), -- -Unknown-
(95771, 1, 52529, 0, 0), -- -Unknown-
(99366, 1, 85562, 0, 0), -- Taintheart Summoner
(101679, 1, 39743, 0, 0), -- -Unknown-
(100400, 1, 52529, 0, 0), -- -Unknown-
(99358, 1, 137012, 0, 0); -- -Unknown-

UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=33162 WHERE (`CreatureID`=24527 AND `ID`=1); -- Bok Dropcertain
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=33162 WHERE (`CreatureID`=24492 AND `ID`=1); -- Drohn's Distillery Barker
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=33162 WHERE (`CreatureID`=24493 AND `ID`=1); -- T'chali's Voodoo Brewery Barker
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=33162 WHERE (`CreatureID`=23685 AND `ID`=1); -- Gordok Brew Barker
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13859 WHERE (`CreatureID`=24657 AND `ID`=1); -- Glodrak Huntsniper
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=26822 AND `ID`=1); -- Ursula Direbrew
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=36761 WHERE (`CreatureID`=26764 AND `ID`=1); -- Ilsa Direbrew
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=8983 AND `ID`=1); -- Golem Lord Argelmach
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=9503 AND `ID`=1); -- Private Rocknot
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=48663 WHERE (`CreatureID`=23872 AND `ID`=1); -- Coren Direbrew
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2081 WHERE (`CreatureID`=45817 AND `ID`=1); -- Hierophant Theodora Mulvadania
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2081 WHERE (`CreatureID`=12944 AND `ID`=1); -- Lokhtos Darkbargainer
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=8897 AND `ID`=1); -- Doomforge Craftsman
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2704 WHERE (`CreatureID`=9545 AND `ID`=1); -- Grim Patron
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=0 WHERE (`CreatureID`=45820 AND `ID`=1); -- Razal'blade
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=8904 AND `ID`=1); -- Shadowforge Senator
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=8920 AND `ID`=1); -- Weapon Technician
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=45839 AND `ID`=1); -- Galamav the Marksman
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10617 WHERE (`CreatureID`=9681 AND `ID`=1); -- Jaz
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=8899 AND `ID`=1); -- Doomforge Dragoon
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=11762 WHERE (`CreatureID`=9319 AND `ID`=1); -- Houndmaster Grebmar
UPDATE `creature_equip_template` SET `ItemID1`=1903 WHERE (`CreatureID`=37937 AND `ID`=1); -- Dark Iron Troublemaker
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=8895 AND `ID`=1); -- Anvilrage Officer
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2052 WHERE (`CreatureID`=8901 AND `ID`=1); -- Anvilrage Reservist
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=8891 AND `ID`=1); -- Anvilrage Guardsman
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1961 WHERE (`CreatureID`=8890 AND `ID`=1); -- Anvilrage Warden
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1896 WHERE (`CreatureID`=8892 AND `ID`=1); -- Anvilrage Footman
UPDATE `creature_equip_template` SET `ItemID1`=1902, `ItemID3`=0 WHERE (`CreatureID`=74228 AND `ID`=2); -- -Unknown-
