
DELETE FROM `quest_poi` WHERE (`QuestID`=44887 AND `BlobIndex`=0 AND `Idx1`=7) OR (`QuestID`=44887 AND `BlobIndex`=1 AND `Idx1`=6) OR (`QuestID`=44887 AND `BlobIndex`=0 AND `Idx1`=5) OR (`QuestID`=44887 AND `BlobIndex`=1 AND `Idx1`=4) OR (`QuestID`=44887 AND `BlobIndex`=1 AND `Idx1`=2);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(44887, 0, 7, 32, 0, 0, 1220, 1014, 10, 0, 0, 0, 0, 1004341, 23420), -- -Unknown-
(44887, 1, 6, 2, 287228, 142329, 1220, 1015, 0, 0, 2, 0, 0, 0, 23420), -- -Unknown-
(44887, 0, 5, 2, 287228, 142329, 1493, 1045, 3, 0, 0, 0, 47482, 1041763, 23420), -- -Unknown-
(44887, 1, 4, 1, 287229, 142330, 1220, 1014, 10, 0, 2, 0, 0, 0, 23420), -- -Unknown-
(44887, 1, 2, 0, 287227, 142328, 1501, 1081, 6, 0, 2, 0, 47482, 0, 23420); -- -Unknown-

UPDATE `quest_poi` SET `WorldMapAreaId`=510, `Floor`=0, `PlayerConditionID`=46263, `VerifiedBuild`=23420 WHERE (`QuestID`=40173 AND `BlobIndex`=0 AND `Idx1`=0); -- The Unstable Prism
UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=287229, `QuestObjectID`=142330, `MapID`=1544, `WorldMapAreaId`=1066, `Floor`=1, `PlayerConditionID`=47482, `WoDUnk1`=1120743, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `BlobIndex`=0 AND `Idx1`=3); -- -Unknown-
UPDATE `quest_poi` SET `MapID`=1220, `WorldMapAreaId`=1018, `Floor`=0, `Flags`=2, `WoDUnk1`=0, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-
UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=287229, `QuestObjectID`=142330, `MapID`=1544, `WorldMapAreaId`=1066, `Floor`=1, `PlayerConditionID`=47482, `WoDUnk1`=1120743, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `BlobIndex`=0 AND `Idx1`=3); -- -Unknown-
UPDATE `quest_poi` SET `MapID`=1220, `WorldMapAreaId`=1018, `Floor`=0, `Flags`=2, `WoDUnk1`=0, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-
DELETE FROM `quest_poi_points` WHERE (`QuestID`=44887 AND `Idx1`=7 AND `Idx2`=0) OR (`QuestID`=44887 AND `Idx1`=6 AND `Idx2`=0) OR (`QuestID`=44887 AND `Idx1`=5 AND `Idx2`=0);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(44887, 7, 0, -848, 4639, 23420), -- -Unknown-
(44887, 6, 0, -1804, 6668, 23420), -- -Unknown-
(44887, 5, 0, 4049, -298, 23420); -- -Unknown-

UPDATE `quest_poi_points` SET `X`=5813, `Y`=628, `VerifiedBuild`=23420 WHERE (`QuestID`=40173 AND `Idx1`=0 AND `Idx2`=0); -- The Unstable Prism
UPDATE `quest_poi_points` SET `X`=-956, `Y`=4324, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `Idx1`=4 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=4692, `Y`=4015, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `Idx1`=3 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=3114, `Y`=7553, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=-956, `Y`=4324, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `Idx1`=4 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=4692, `Y`=4015, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `Idx1`=3 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=3114, `Y`=7553, `VerifiedBuild`=23420 WHERE (`QuestID`=44887 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-

DELETE FROM `quest_details` WHERE `ID` IN (41877 /*Lady Lunastre*/, 44636 /*Building an Army*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(41877, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Lady Lunastre
(44636, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- Building an Army



UPDATE `creature_model_info` SET `BoundingRadius`=1.383914, `CombatReach`=5, `VerifiedBuild`=23420 WHERE `DisplayID`=71652;
UPDATE `creature_model_info` SET `BoundingRadius`=0.39, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=69098;
UPDATE `creature_model_info` SET `BoundingRadius`=1.13161, `CombatReach`=1.65 WHERE `DisplayID`=61928;
UPDATE `creature_model_info` SET `CombatReach`=1.685393, `VerifiedBuild`=23420 WHERE `DisplayID`=70095;
UPDATE `creature_model_info` SET `CombatReach`=1.630435, `VerifiedBuild`=23420 WHERE `DisplayID`=70091;
UPDATE `creature_model_info` SET `CombatReach`=1.5625, `VerifiedBuild`=23420 WHERE `DisplayID`=70100;
UPDATE `creature_model_info` SET `CombatReach`=1.648352, `VerifiedBuild`=23420 WHERE `DisplayID`=70096;
UPDATE `creature_model_info` SET `CombatReach`=1.304348, `VerifiedBuild`=23420 WHERE `DisplayID`=70081;
UPDATE `creature_model_info` SET `CombatReach`=1.071429, `VerifiedBuild`=23420 WHERE `DisplayID`=70090;
UPDATE `creature_model_info` SET `CombatReach`=1.648352, `VerifiedBuild`=23420 WHERE `DisplayID`=70038;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=70036;
UPDATE `creature_model_info` SET `CombatReach`=1.304348, `VerifiedBuild`=23420 WHERE `DisplayID`=70078;
UPDATE `creature_model_info` SET `CombatReach`=1.293103, `VerifiedBuild`=23420 WHERE `DisplayID`=70037;
UPDATE `creature_model_info` SET `CombatReach`=1.304348 WHERE `DisplayID`=70082;
UPDATE `creature_model_info` SET `CombatReach`=1.25, `VerifiedBuild`=23420 WHERE `DisplayID`=70077;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=23420 WHERE `DisplayID`=72191;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68407;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69342;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69331;


UPDATE `quest_template` SET `RewardMoney`=16200, `VerifiedBuild`=23420 WHERE `ID`=44310; -- Thrice the Power
UPDATE `quest_template` SET `RewardMoney`=16200, `VerifiedBuild`=23420 WHERE `ID`=43441; -- A Second Weapon
UPDATE `quest_template` SET `RewardMoney`=16200, `VerifiedBuild`=23420 WHERE `ID`=41085; -- A Mage's Weapon
UPDATE `quest_template` SET `RewardMoney`=162000, `VerifiedBuild`=23420 WHERE `ID`=41036; -- The Dreadlord's Prize
UPDATE `quest_template` SET `RewardMoney`=162000, `VerifiedBuild`=23420 WHERE `ID`=31316; -- Julia, The Pet Tamer
UPDATE `quest_template` SET `RewardMoney`=324000, `VerifiedBuild`=23420 WHERE `ID`=42244; -- Fate of the Queen's Reprisal
UPDATE `quest_template` SET `RewardMoney`=162000, `VerifiedBuild`=23420 WHERE `ID`=42213; -- The Tidestone of Golganneth
UPDATE `quest_template` SET `RewardMoney`=162000 WHERE `ID`=38014; -- Feasting on the Dragon
UPDATE `quest_template` SET `RewardMoney`=194000 WHERE `ID`=37856; -- The Withered
UPDATE `quest_template` SET `RewardMoney`=388000 WHERE `ID`=37656; -- Fel Machinations


UPDATE `creature_template` SET `npcflag`=131, `type_flags2`=16384, `VerifiedBuild`=23420 WHERE `entry`=111740; -- Mile Raitheborne
UPDATE `creature_template` SET `minlevel`=10, `maxlevel`=10, `VerifiedBuild`=23420 WHERE `entry`=113183; -- Skidmark
UPDATE `creature_template` SET `unit_flags2`=34816, `VerifiedBuild`=23420 WHERE `entry`=113155; -- Roaster Rat
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=99349; -- Robert "Chance" Llore
UPDATE `creature_template` SET `minlevel`=105 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=109, `speed_walk`=0.666668, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=108, `speed_run`=1.142857 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=109, `speed_run`=0.9523814 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=104, `speed_run`=1.142857 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=104, `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=109 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=190, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=34816, `VerifiedBuild`=23420 WHERE `entry`=106112; -- Wounded Nightborne Civilian
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=2735, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2097152, `VerifiedBuild`=23420 WHERE `entry`=110907; -- Star Augur Etraeus
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=190, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=69240832, `VerifiedBuild`=23420 WHERE `entry`=105340; -- Umbral Bloom
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114889; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114892; -- Withering Civilian
UPDATE `creature_template` SET `unit_flags2`=33556480 WHERE `entry`=114929; -- Duskwatch Defender
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=113126; -- Meredil Lockjaw
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=97140; -- First Arcanist Thalyssra
UPDATE `creature_template` SET `HoverHeight`=2 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=109 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=109, `speed_walk`=0.666668, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=104, `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=108, `speed_run`=1.142857 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=109, `speed_run`=0.9523814 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=104, `speed_run`=1.142857 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=105 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111772; -- Terric the Illuminator
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96724; -- Silver Hand Knight
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96696; -- Silver Hand Knight
UPDATE `creature_template` SET `npcflag`=131, `type_flags2`=16384, `VerifiedBuild`=23420 WHERE `entry`=91190; -- Sister Elda
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=109105; -- Lothraxion
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113254; -- Silver Hand Templar
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=113438; -- Silver Hand Templar
UPDATE `creature_template` SET `type_flags2`=16384, `VerifiedBuild`=23420 WHERE `entry`=102641; -- Filius Sparkstache
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=105853; -- Uncrowned Peacekeeper
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=23420 WHERE `entry`=111897; -- Dagg
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `minlevel`=64, `maxlevel`=64, `VerifiedBuild`=23420 WHERE `entry`=19574; -- Kizzie
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=23420 WHERE `entry`=19576; -- Xyrol
UPDATE `creature_template` SET `minlevel`=67, `VerifiedBuild`=23420 WHERE `entry`=19575; -- Qiff
UPDATE `creature_template` SET `minlevel`=19, `maxlevel`=19, `VerifiedBuild`=23420 WHERE `entry`=24977; -- Warpy
UPDATE `creature_template` SET `maxlevel`=65, `VerifiedBuild`=23420 WHERE `entry`=19612; -- Irradiated Manager
UPDATE `creature_template` SET `minlevel`=63, `maxlevel`=63, `VerifiedBuild`=23420 WHERE `entry`=24974; -- Liza Cutlerflix
UPDATE `creature_template` SET `maxlevel`=55, `VerifiedBuild`=23420 WHERE `entry`=17636; -- Kalynna Lathred
UPDATE `creature_template` SET `maxlevel`=60, `VerifiedBuild`=23420 WHERE `entry`=19573; -- Dash
UPDATE `creature_template` SET `npcflag`=643, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=19617; -- Boots
UPDATE `creature_template` SET `minlevel`=67, `VerifiedBuild`=23420 WHERE `entry`=28344; -- Blazzle
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=35, `npcflag`=128, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=107619; -- Blaze Magmaburn
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=35, `npcflag`=128, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=107599; -- Izzee the 'Clutch'
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=35, `npcflag`=128, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=107610; -- Kitzie Crankshot
UPDATE `creature_template` SET `maxlevel`=5, `VerifiedBuild`=23420 WHERE `entry`=1995; -- Strigid Owl
UPDATE `creature_template` SET `minlevel`=2, `VerifiedBuild`=23420 WHERE `entry`=14560; -- Swift White Steed
UPDATE `creature_template` SET `minlevel`=2, `VerifiedBuild`=23420 WHERE `entry`=308; -- Black Stallion
UPDATE `creature_template` SET `minlevel`=24, `maxlevel`=24, `VerifiedBuild`=23420 WHERE `entry`=42421; -- Stormwind Fisherman
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `VerifiedBuild`=23420 WHERE `entry`=14559; -- Swift Palomino
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `minlevel`=104, `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=108, `speed_run`=1.142857 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=104, `speed_run`=1.142857 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=109, `speed_run`=0.9523814 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=109, `speed_walk`=0.666668, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet

DELETE FROM `gameobject_template` WHERE `entry`=258907;
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(258907, 5, 37914, 'Kingslayers Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- Kingslayers Appearance
