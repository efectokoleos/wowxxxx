DELETE FROM `quest_offer_reward` WHERE `ID` IN (39882 /*The Glamour Has Faded*/, 42233 /*Highmountain Tribes*/, 42234 /*The Valarjar*/, 45353 /*-Unknown-*/, 44040 /*Vote of Confidence*/, 43312 /*Thinly Veiled Threats*/, 43310 /*Either With Us*/, 43309 /*The Perfect Opportunity*/, 43582 /*Shalassic Park*/, 41231 /*Apex Predator*/, 41216 /*Survival of the Fittest*/, 40321 /*Feathersong's Redemption*/, 40319 /*The Final Ingredient*/, 40315 /*End of the Line*/, 40578 /*Paying Respects*/, 40306 /*The Last Chapter*/, 40308 /*Fragments of Memory*/, 40300 /*Tools of the Trade*/, 40227 /*Bad Intentions*/, 40744 /*An Ancient Recipe*/, 40266 /*The Lost Advisor*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(39882, 1, 1, 0, 0, 0, 0, 0, 0, 'You found it? I trust Glaidalis fell in the process?$b$bWhile I feel relieved such a powerful foe has been defeated, I can''t help but feel sad for what could have been...$b$bRegardless, you''ve proven yourself an ally. As a reward, I will teach you one of my enchantments.', 23420), -- The Glamour Has Faded
(42233, 0, 0, 0, 0, 0, 0, 0, 0, 'The Highmountain tribes thank you.', 23420), -- Highmountain Tribes
(42234, 0, 0, 0, 0, 0, 0, 0, 0, 'The valarjar thank you.', 23420), -- The Valarjar
(45353, 0, 0, 0, 0, 0, 0, 0, 0, 'I count myself fortunate to be able to work with the heroes who fight for our world''s survival.', 23420), -- -Unknown-
(44040, 1, 0, 0, 0, 0, 0, 0, 0, 'I know of Coryn. We will not underestimate his venom, rest assured.$b$bThank you for helping Aurore. She is a dear friend of mine.', 23420), -- Vote of Confidence
(43312, 5, 66, 0, 0, 0, 0, 0, 0, 'He is unharmed! I owe you- and Ly''leth- a great debt.', 23420), -- Thinly Veiled Threats
(43310, 1, 0, 0, 0, 0, 0, 0, 0, 'I love Lady Lunastre like a sister, but I am afraid my hands are tied.', 23420), -- Either With Us
(43309, 1, 0, 0, 0, 0, 0, 0, 0, 'Ahh, $n.$b$bSo good to see you once again. I trust you are ready to assist me with my campaign.', 23420), -- The Perfect Opportunity
(43582, 0, 0, 0, 0, 0, 0, 0, 0, 'That was quite the spectacle, $n.$b$bSu''esh will be a valuable ally in the days to come. I will keep an eye on her for the time being.', 23420), -- Shalassic Park
(41231, 1, 0, 0, 0, 0, 0, 0, 0, 'I saw everything from out here. That was... messy.', 23420), -- Apex Predator
(41216, 1, 0, 0, 0, 0, 0, 0, 0, 'I could hear their cries and see the carnage from here! That will teach the Nightborne to enslave creatures. Have they fallen so far during all those years under the shield?', 23420), -- Survival of the Fittest
(40321, 2, 0, 0, 0, 0, 0, 0, 0, 'Thank you, $n. You have given me clarity of purpose and a reason to live again. Together we will save Suramar and atone for the sins of my people.', 23420), -- Feathersong's Redemption
(40319, 18, 0, 0, 0, 0, 0, 0, 0, 'You saw Latara? What happened?$b$b... I see. I am so sorry I failed you, my love.', 23420), -- The Final Ingredient
(40315, 1, 0, 0, 0, 0, 0, 0, 0, 'Your arguments are not without merit, $n. But I remain steadfast.', 23420), -- End of the Line
(40578, 0, 0, 0, 0, 0, 0, 0, 0, 'It pains me to see them suffering. In time they will return, but for now they will be at peace.', 23420), -- Paying Respects
(40306, 1, 0, 0, 0, 0, 0, 0, 0, 'Hmph. I suppose if you''re assisting me with my task, I should learn your name.$b$b<Thaedris reaches into the book and pulls out an unfamiliar, dried plant.>$b$bThank you for fetching me another one of the ingredients. Your assistance is... welcome, $n.', 23420), -- The Last Chapter
(40308, 2, 0, 0, 0, 0, 0, 0, 0, 'These relics are tokens and mementos of the former lives of those interred here. They like to be remembered and these offerings keep them in their eternal peace.$b$bThe spirits of the fallen will be at rest now. You have my gratitude.', 23420), -- Fragments of Memory
(40300, 1, 0, 0, 0, 0, 0, 0, 0, 'Truly a testament to our preservation techniques. I had lost hope that anything would have survived looting and the sands of time.$b$bThe ancestors will be pleased with this.', 23420), -- Tools of the Trade
(40227, 1, 0, 0, 0, 0, 0, 0, 0, 'Honeyed words from a stranger. While comforting, they do not shake my resolve. Come, at this rate I will wither before we are through.', 23420), -- Bad Intentions
(40744, 657, 0, 0, 0, 0, 0, 0, 0, 'That should be plenty. Talondust loses its potency fairly quickly, so we must make haste.', 23420), -- An Ancient Recipe
(40266, 1, 0, 0, 0, 0, 0, 0, 0, '<Thaedris stirs from his thoughts as you approach.>$b$bThese will be sufficient, $r.$b$b Cliffthorn is a deadly poison, and will be perfect for my needs.', 23420); -- The Lost Advisor

DELETE FROM `quest_greeting` WHERE (`ID`=99483 AND `Type`=0) OR (`ID`=99065 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(99483, 0, 0, 0, 'This is where we remember General Startalon, one of the finest generals our people has ever known. He commanded the Hippogryph riders during the War of the Ancients.$b$bI used to visit his tomb often to meditate for guidance. I wonder if I will see him.', 23420), -- 99483
(99065, 0, 0, 0, 'Who are you? Why does a $r come here?$b$bThis place is not meant for strangers. It is... was... a sacred place for my people. Or what my people once were.', 23420); -- 99065

DELETE FROM `quest_details` WHERE `ID` IN (42491 /*Long Buried Knowledge*/, 44040 /*Vote of Confidence*/, 43312 /*Thinly Veiled Threats*/, 43311 /*Or Against Us*/, 43310 /*Either With Us*/, 43309 /*The Perfect Opportunity*/, 43582 /*Shalassic Park*/, 41231 /*Apex Predator*/, 41216 /*Survival of the Fittest*/, 40321 /*Feathersong's Redemption*/, 40319 /*The Final Ingredient*/, 40315 /*End of the Line*/, 40578 /*Paying Respects*/, 40306 /*The Last Chapter*/, 40308 /*Fragments of Memory*/, 40300 /*Tools of the Trade*/, 40227 /*Bad Intentions*/, 40744 /*An Ancient Recipe*/, 40266 /*The Lost Advisor*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(42491, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Long Buried Knowledge
(44040, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Vote of Confidence
(43312, 603, 1, 603, 0, 0, 0, 0, 0, 23420), -- Thinly Veiled Threats
(43311, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Or Against Us
(43310, 1, 603, 0, 0, 0, 1500, 0, 0, 23420), -- Either With Us
(43309, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Perfect Opportunity
(43582, 94, 0, 0, 0, 0, 0, 0, 0, 23420), -- Shalassic Park
(41231, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Apex Predator
(41216, 6, 0, 0, 0, 0, 0, 0, 0, 23420), -- Survival of the Fittest
(40321, 25, 0, 0, 0, 0, 0, 0, 0, 23420), -- Feathersong's Redemption
(40319, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Final Ingredient
(40315, 6, 0, 0, 0, 0, 0, 0, 0, 23420), -- End of the Line
(40578, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Paying Respects
(40306, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Last Chapter
(40308, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Fragments of Memory
(40300, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Tools of the Trade
(40227, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bad Intentions
(40744, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- An Ancient Recipe
(40266, 1, 0, 0, 0, 0, 0, 0, 0, 23420); -- The Lost Advisor

DELETE FROM `quest_request_items` WHERE `ID` IN (39882 /*The Glamour Has Faded*/, 45353 /*-Unknown-*/, 43309 /*The Perfect Opportunity*/, 42832 /*The Fruit of Our Efforts*/, 42829 /*Make an Entrance*/, 41222 /*Into The Pit!*/, 40319 /*The Final Ingredient*/, 40306 /*The Last Chapter*/, 40308 /*Fragments of Memory*/, 40300 /*Tools of the Trade*/, 40744 /*An Ancient Recipe*/, 40266 /*The Lost Advisor*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(39882, 1, 0, 0, 0, 'If you''re truly a friend in this war, then be my sword and strike down Glaidalis.', 23420), -- The Glamour Has Faded
(45353, 0, 0, 0, 0, 'Aman''Thul has likely been slain for countless aeons, yet his power lingers. How is this possible?', 23420), -- -Unknown-
(43309, 0, 0, 0, 0, 'Thalyssra asks much of me.', 23420), -- The Perfect Opportunity
(42832, 0, 0, 0, 0, 'Enjoying yourself?', 23420), -- The Fruit of Our Efforts
(42829, 0, 0, 0, 0, 'Can I help you?', 23420), -- Make an Entrance
(41222, 0, 0, 0, 0, 'We''ve got to get to the felstalker pits an'' save Brambley!', 23420), -- Into The Pit!
(40319, 1, 0, 0, 0, 'What was that? I heard a familiar voice... it couldn''t be.', 23420), -- The Final Ingredient
(40306, 6, 0, 0, 0, 'Do you have the tome, $r?', 23420), -- The Last Chapter
(40308, 6, 0, 0, 0, 'What do you have there?', 23420), -- Fragments of Memory
(40300, 6, 0, 0, 0, 'Did you find anything?', 23420), -- Tools of the Trade
(40744, 6, 0, 0, 0, 'Do you have what I require, $r? We don''t have much time to spare.', 23420), -- An Ancient Recipe
(40266, 0, 0, 0, 0, '<Thaedris seems lost in thought as he gazes towards the city of Suramar.>', 23420); -- The Lost Advisor

UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72432;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72433;
UPDATE `creature_model_info` SET `BoundingRadius`=0.515747 WHERE `DisplayID`=65091;
UPDATE `creature_model_info` SET `CombatReach`=1.530612 WHERE `DisplayID`=70096;
UPDATE `creature_model_info` SET `CombatReach`=1.5625 WHERE `DisplayID`=70095;
UPDATE `creature_model_info` SET `CombatReach`=1.282051 WHERE `DisplayID`=70081;
UPDATE `creature_model_info` SET `CombatReach`=1.744186 WHERE `DisplayID`=70100;
UPDATE `creature_model_info` SET `CombatReach`=1.724138, `VerifiedBuild`=23420 WHERE `DisplayID`=70101;
UPDATE `creature_model_info` SET `CombatReach`=0.75, `VerifiedBuild`=23420 WHERE `DisplayID`=71814;
UPDATE `creature_model_info` SET `CombatReach`=1.648352 WHERE `DisplayID`=70090;
UPDATE `creature_model_info` SET `CombatReach`=1.013513 WHERE `DisplayID`=70091;
UPDATE `creature_model_info` SET `CombatReach`=1.724138, `VerifiedBuild`=23420 WHERE `DisplayID`=70039;
UPDATE `creature_model_info` SET `CombatReach`=1.293103 WHERE `DisplayID`=70036;
UPDATE `creature_model_info` SET `CombatReach`=1.304348 WHERE `DisplayID`=70037;
UPDATE `creature_model_info` SET `BoundingRadius`=9.188382, `CombatReach`=6.75, `VerifiedBuild`=23420 WHERE `DisplayID`=71877;
UPDATE `creature_model_info` SET `BoundingRadius`=0.951831, `VerifiedBuild`=23420 WHERE `DisplayID`=42414;
UPDATE `creature_model_info` SET `CombatReach`=8.25, `VerifiedBuild`=23420 WHERE `DisplayID`=64464;
UPDATE `creature_model_info` SET `CombatReach`=1.5 WHERE `DisplayID`=664;
UPDATE `creature_model_info` SET `CombatReach`=0.8571429, `VerifiedBuild`=23420 WHERE `DisplayID`=1763;
UPDATE `creature_model_info` SET `BoundingRadius`=4.880898, `CombatReach`=8.4, `VerifiedBuild`=23420 WHERE `DisplayID`=55832;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1856, `CombatReach`=1.6, `VerifiedBuild`=23420 WHERE `DisplayID`=15990;
UPDATE `creature_model_info` SET `BoundingRadius`=2.866474, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=72897;
UPDATE `creature_model_info` SET `BoundingRadius`=2.451996, `CombatReach`=2.5, `VerifiedBuild`=23420 WHERE `DisplayID`=16178;
UPDATE `creature_model_info` SET `CombatReach`=1.704545 WHERE `DisplayID`=71539;
UPDATE `creature_model_info` SET `CombatReach`=1.666667 WHERE `DisplayID`=68964;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72675;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72677;
UPDATE `creature_model_info` SET `BoundingRadius`=3.75, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=47516;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=68563;
UPDATE `creature_model_info` SET `BoundingRadius`=2.26048, `CombatReach`=6, `VerifiedBuild`=23420 WHERE `DisplayID`=67031;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6640207, `CombatReach`=0.54 WHERE `DisplayID`=69273;
UPDATE `creature_model_info` SET `BoundingRadius`=1.028736, `CombatReach`=1.5 WHERE `DisplayID`=61927;
UPDATE `creature_model_info` SET `BoundingRadius`=1.65039 WHERE `DisplayID`=64629;

DELETE FROM `npc_vendor` WHERE (`entry`=91190 AND `item`=144435 AND `ExtendedCost`=6153 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(91190, 4, 144435, 0, 6153, 1, 0, 0, 23420); -- -Unknown-

UPDATE `npc_vendor` SET `slot`=9 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=9 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=9 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=9 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=109408 AND `ID`=1) OR (`CreatureID`=97003 AND `ID`=1) OR (`CreatureID`=94248 AND `ID`=1) OR (`CreatureID`=113770 AND `ID`=1) OR (`CreatureID`=113769 AND `ID`=1) OR (`CreatureID`=113632 AND `ID`=1) OR (`CreatureID`=113768 AND `ID`=1) OR (`CreatureID`=113766 AND `ID`=1) OR (`CreatureID`=113764 AND `ID`=1) OR (`CreatureID`=99676 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(109408, 1, 137254, 0, 0), -- Lieutenant Piet
(97003, 1, 80580, 0, 0), -- Sheddle Glossgleam
(94248, 1, 116487, 0, 0), -- Stonebinder Gorgrogg
(113770, 1, 15217, 0, 116647), -- Silver Hand Shieldbearer
(113769, 1, 15217, 0, 116647), -- Silver Hand Shieldbearer
(113632, 1, 28802, 0, 67149), -- Silver Hand Templar
(113768, 1, 25127, 0, 72983), -- Silver Hand Protector
(113766, 1, 34670, 0, 30882), -- Silver Hand Knight
(113764, 1, 34670, 0, 30882), -- Silver Hand Knight
(99676, 1, 77405, 0, 0); -- Mogu'shan Secret-Keeper

UPDATE `creature_equip_template` SET `ItemID1`=1903 WHERE (`CreatureID`=95430 AND `ID`=2); -- Servant of Ravencrest
UPDATE `creature_equip_template` SET `ItemID1`=5956 WHERE (`CreatureID`=95430 AND `ID`=1); -- Servant of Ravencrest
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=89023 AND `ID`=1); -- Nightwatcher Idri
UPDATE `creature_equip_template` SET `ItemID1`=118563 WHERE (`CreatureID`=99386 AND `ID`=5); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=114980 WHERE (`CreatureID`=99386 AND `ID`=4); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=114978 WHERE (`CreatureID`=99386 AND `ID`=3); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=118568 WHERE (`CreatureID`=99386 AND `ID`=2); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=114979 WHERE (`CreatureID`=99386 AND `ID`=1); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=132870 WHERE (`CreatureID`=90350 AND `ID`=2); -- Silver Hand Knight
UPDATE `creature_equip_template` SET `ItemID1`=132869 WHERE (`CreatureID`=90350 AND `ID`=1); -- Silver Hand Knight
UPDATE `creature_equip_template` SET `ItemID1`=59616 WHERE (`CreatureID`=92617 AND `ID`=1); -- Bradensbrook Villager
UPDATE `creature_equip_template` SET `ItemID1`=1902 WHERE (`CreatureID`=44158 AND `ID`=3); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID1`=3367 WHERE (`CreatureID`=44158 AND `ID`=2); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID1`=1903 WHERE (`CreatureID`=44158 AND `ID`=1); -- Orgrimmar Skyway Peon

UPDATE `quest_template` SET `RewardMoney`=194000 WHERE `ID`=37960; -- Leyline Abuse
UPDATE `quest_template` SET `Flags`=33554496 WHERE `ID`=45353; -- -Unknown-
UPDATE `quest_template` SET `Flags`=33554496 WHERE `ID`=45353; -- -Unknown-
UPDATE `quest_template` SET `Flags`=33554496 WHERE `ID`=45353; -- -Unknown-
UPDATE `quest_template` SET `RewardMoney`=388000 WHERE `ID`=42244; -- Fate of the Queen's Reprisal
UPDATE `quest_template` SET `RewardMoney`=194000 WHERE `ID`=37853; -- The Death of the Eldest
UPDATE `quest_template` SET `RewardMoney`=19400 WHERE `ID`=44310; -- Thrice the Power
UPDATE `quest_template` SET `RewardMoney`=194000 WHERE `ID`=37497; -- Trailing the Tidestone
UPDATE `quest_template` SET `RewardMoney`=19400 WHERE `ID`=39864; -- Stormheim
UPDATE `quest_template` SET `RewardMoney`=19400 WHERE `ID`=39733; -- The Lone Mountain
UPDATE `quest_template` SET `RewardMoney`=19400 WHERE `ID`=39731; -- The Tranquil Forest
UPDATE `quest_template` SET `RewardMoney`=19400 WHERE `ID`=39718; -- Paradise Lost
UPDATE `quest_template` SET `Flags`=33554496 WHERE `ID`=45353; -- -Unknown-

UPDATE `creature_template` SET `maxlevel`=108 WHERE `entry`=102587; -- Saa'ra
UPDATE `creature_template` SET `minlevel`=98 WHERE `entry`=107565; -- Lightspawn
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=111757; -- Warden Trainee
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2099200 WHERE `entry`=88094; -- Sea Skrog
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=89051; -- Okuna Longtusk
UPDATE `creature_template` SET `unit_flags`=295680 WHERE `entry`=112874; -- Landon Doi
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=103972; -- Felsworn Betrayer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=67110912 WHERE `entry`=106509; -- Felblaze Portal
UPDATE `creature_template` SET `minlevel`=106, `maxlevel`=106 WHERE `entry`=106772; -- Exotic Book
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048 WHERE `entry`=93064; -- Black Rook Falcon
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=92617; -- Bradensbrook Villager
UPDATE `creature_template` SET `unit_flags`=33555200 WHERE `entry`=110032; -- Festering Eye
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=93030; -- Ironbranch
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=112052; -- Vilepetal Rooter
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=101780; -- Moonfall Hippogryph
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=102034; -- Wild Moonfall Hippogryph
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=103 WHERE `entry`=106271; -- Jarum Skymane
UPDATE `creature_template` SET `unit_flags`=768, `VerifiedBuild`=23420 WHERE `entry`=99862; -- Great Eagle
UPDATE `creature_template` SET `speed_run`=1.828571, `VerifiedBuild`=23420 WHERE `entry`=113598; -- Highmountain Protector
UPDATE `creature_template` SET `speed_run`=1.828571, `VerifiedBuild`=23420 WHERE `entry`=113592; -- Highmountain Protector
UPDATE `creature_template` SET `type_flags`=120, `VerifiedBuild`=23420 WHERE `entry`=100818; -- Bellowing Idol
UPDATE `creature_template` SET `minlevel`=98 WHERE `entry`=105778; -- Angry Crowd
UPDATE `creature_template` SET `minlevel`=108, `maxlevel`=108, `VerifiedBuild`=23420 WHERE `entry`=105688; -- REUSE
UPDATE `creature_template` SET `unit_flags`=536904512, `unit_flags2`=4196353 WHERE `entry`=98406; -- Embershard Scorpion
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=108185; -- Coldscale Gazecrawler
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=108283; -- Mightstone Savage
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=107758; -- Amberfall Greatstag
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=96129; -- Felskorn Raider
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=96135; -- Felskorn Warmonger
UPDATE `creature_template` SET `name`='Ley Line Hunter', `VerifiedBuild`=23420 WHERE `entry`=100237; -- Leyline Hunter
UPDATE `creature_template` SET `name`='Ley Line Researcher', `VerifiedBuild`=23420 WHERE `entry`=111871; -- Leyline Researcher
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=103223; -- Hertha Grimdottir
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=100778; -- Nightborne Trapper
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=116068; -- Arm of the Magistrix
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=111558; -- Felgaze Doomseer
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=102747; -- Tamed Owl
UPDATE `creature_template` SET `npcflag`=4225, `VerifiedBuild`=23420 WHERE `entry`=93979; -- Leyweaver Jorjana
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2821, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=109408; -- Lieutenant Piet
UPDATE `creature_template` SET `subname`='Exotic Imports', `VerifiedBuild`=23420 WHERE `entry`=112063; -- Cornelius Crispin
UPDATE `creature_template` SET `name`='Libation Vendor', `VerifiedBuild`=23420 WHERE `entry`=114530; -- Concerned Merchant
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=111056; -- Tiny Illusory Dancer
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=106919; -- Nightborne Courier
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=2049 WHERE `entry`=108810; -- Chloe
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=34817 WHERE `entry`=108386; -- Ambrena
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114892; -- Withering Civilian
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114888; -- Shal'dorei Civilian
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=4.5 WHERE `entry`=108822; -- Huntress Estrid
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048 WHERE `entry`=106320; -- Volynd Stormbringer
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2099200 WHERE `entry`=108740; -- Velimar
UPDATE `creature_template` SET `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=105921; -- Nightborne Spellsword
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=115595; -- Duskwatch Executor
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=108063; -- Korine
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=108870; -- Sylverin
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=110, `faction`=1786, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=33556480 WHERE `entry`=112742; -- Broodling
UPDATE `creature_template` SET `unit_flags`=570688256, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=102270; -- Eredar Invader
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048 WHERE `entry`=102272; -- Felguard Destroyer
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048 WHERE `entry`=102269; -- Felstalker Ravener
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=103092; -- The Great Akazamzarak
UPDATE `creature_template` SET `HealthScalingExpansion`=-1, `spell1`=0, `VerifiedBuild`=23420 WHERE `entry`=2630; -- Earthbind Totem
UPDATE `creature_template` SET `unit_flags`=768 WHERE `entry`=102263; -- Skorpyron
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=2099200, `HoverHeight`=1.125 WHERE `entry`=111674; -- Cinderwing
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95675; -- God-King Skovald
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=99868; -- Fenryr
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=99891; -- Storm Drake
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95674; -- Fenryr
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=96677; -- Steeljaw Grizzly
UPDATE `creature_template` SET `unit_flags`=570720576, `unit_flags2`=2099201, `VerifiedBuild`=23420 WHERE `entry`=96611; -- Angerhoof Bull
UPDATE `creature_template` SET `unit_flags`=570720256, `unit_flags2`=2099201, `VerifiedBuild`=23420 WHERE `entry`=96609; -- Gildedfur Stag
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95833; -- Hyrja
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95676; -- Odyn
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=95843; -- King Haldor
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=97081; -- King Bjorn
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=97084; -- King Tor
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=97083; -- King Ranulf
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=95834; -- Valarjar Mystic
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=96574; -- Stormforged Sentinel
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=94960; -- Hymdall
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=95842; -- Valarjar Thundercaller
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=97087; -- Valarjar Champion
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=97068; -- Storm Drake
UPDATE `creature_template` SET `speed_run`=1.285714 WHERE `entry`=110973; -- Worthy Vrykul
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=23420 WHERE `entry`=111706; -- Boulder
UPDATE `creature_template` SET `speed_walk`=0.6, `speed_run`=0.2142857, `VerifiedBuild`=23420 WHERE `entry`=99664; -- Restless Soul
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200 WHERE `entry`=111290; -- Braxas the Fleshcarver
UPDATE `creature_template` SET `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=98792; -- Wyrmtongue Scavenger
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=104220; -- Starving Ettin
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=103805; -- Sablehorn Doe
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=108942; -- Withered Feaster
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=113184; -- Cunning Bushtail
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113818; -- Glitterpool Chick
UPDATE `creature_template` SET `HoverHeight`=5 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `npcflag`=17, `VerifiedBuild`=23420 WHERE `entry`=46357; -- Gonto
UPDATE `creature_template` SET `npcflag`=1, `unit_flags`=33587456 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=8 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=44158; -- Orgrimmar Skyway Peon
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=109 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `unit_flags`=768 WHERE `entry`=100362; -- Grasping Tentacle
UPDATE `creature_template` SET `unit_flags2`=67110912 WHERE `entry`=102104; -- Enslaved Shieldmaiden
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=92072; -- Grapple Point
UPDATE `creature_template` SET `modelid1`=68157, `HoverHeight`=1 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `unit_flags`=16 WHERE `entry`=88981; -- Ironclaw Scuttler
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=103514; -- Leystone Basilisk
UPDATE `creature_template` SET `HoverHeight`=0.25 WHERE `entry`=111787; -- Great Sea Ray
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=100179; -- Willbreaker Incubator
UPDATE `creature_template` SET `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_walk`=1, `speed_run`=0.9920629 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `type_flags2`=128, `VerifiedBuild`=23420 WHERE `entry`=97673; -- Mystic Tornado
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=89811; -- Subjugated Murloc
UPDATE `creature_template` SET `faction`=1735 WHERE `entry`=34944; -- Keep Cannon
UPDATE `creature_template` SET `minlevel`=67, `maxlevel`=68 WHERE `entry`=36169; -- [DND] Bor'gorok Peon
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=10 WHERE `entry`=115462; -- Squallhunter Drawing
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=14, `speed_walk`=0.68, `speed_run`=0.6, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=115460; -- Hydra Drawing
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=115461; -- Harpy Drawing
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=101, `VerifiedBuild`=23420 WHERE `entry`=89669; -- Drowned Student
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2099200 WHERE `entry`=88094; -- Sea Skrog
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=103972; -- Felsworn Betrayer
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=89811; -- Subjugated Murloc
UPDATE `creature_template` SET `unit_flags`=295680 WHERE `entry`=112874; -- Landon Doi
UPDATE `creature_template` SET `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=106990; -- Chief Bitterbrine
UPDATE `creature_template` SET `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_walk`=1, `speed_run`=0.9920629 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `HealthScalingExpansion`=-1, `spell1`=0, `VerifiedBuild`=23420 WHERE `entry`=2630; -- Earthbind Totem
UPDATE `creature_template` SET `HealthScalingExpansion`=-1, `VerifiedBuild`=23420 WHERE `entry`=60561; -- Earthgrab Totem
UPDATE `creature_template` SET `difficulty_entry_1`=0, `faction`=1732, `unit_flags2`=1073741824, `spell1`=0, `spell2`=0 WHERE `entry`=28781; -- Battleground Demolisher
UPDATE `creature_template` SET `difficulty_entry_1`=0, `faction`=1735, `unit_flags2`=4194304, `spell1`=0 WHERE `entry`=27894; -- Antipersonnel Cannon
UPDATE `creature_template` SET `minlevel`=1 WHERE `entry`=42548; -- Muddy Crawfish
UPDATE `creature_template` SET `minlevel`=48, `maxlevel`=48 WHERE `entry`=37675; -- Public Relations Agent
UPDATE `creature_template` SET `minlevel`=46, `maxlevel`=46 WHERE `entry`=37674; -- Lovely Merchant
UPDATE `creature_template` SET `npcflag`=1, `unit_flags`=33587456 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=8 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=44158; -- Orgrimmar Skyway Peon
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=99349; -- Robert "Chance" Llore
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=103092; -- The Great Akazamzarak
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=113663; -- Ela'lothen
UPDATE `creature_template` SET `type`=7, `VerifiedBuild`=23420 WHERE `entry`=107127; -- Brawlgoth
UPDATE `creature_template` SET `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=106106; -- Bound Citizen
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2099200 WHERE `entry`=88094; -- Sea Skrog
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `minlevel`=104, `maxlevel`=104 WHERE `entry`=106915; -- Marius Felbane
UPDATE `creature_template` SET `minlevel`=107, `maxlevel`=107 WHERE `entry`=106914; -- Tehd Shoemaker
UPDATE `creature_template` SET `faction`=35, `npcflag`=16777216, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33280, `unit_flags2`=2048 WHERE `entry`=115727; -- Mana Collector NE
UPDATE `creature_template` SET `BaseAttackTime`=2000 WHERE `entry`=90390; -- Tyndrissen
UPDATE `creature_template` SET `faction`=35, `npcflag`=16777216, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33280, `unit_flags2`=2048 WHERE `entry`=115729; -- Mana Collector S
UPDATE `creature_template` SET `speed_run`=1.02, `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=90318; -- Withered Fanatic
UPDATE `creature_template` SET `faction`=16 WHERE `entry`=109826; -- Nightfallen Hungerer
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=90336; -- Azurewing Whelpling
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=91265; -- Llothien Fox
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=4196352 WHERE `entry`=109620; -- The Whisperer
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=106804; -- Suramar Grizzly
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=104220; -- Starving Ettin
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=103805; -- Sablehorn Doe
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=102034; -- Wild Moonfall Hippogryph
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=101780; -- Moonfall Hippogryph
UPDATE `creature_template` SET `HoverHeight`=5 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=94846; -- Bitestone Rockcrusher
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=1, `VerifiedBuild`=23420 WHERE `entry`=97871; -- Highmountain Warbrave
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=23420 WHERE `entry`=94261; -- Bitestone Rockbeater
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2815, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102827; -- Bitestone Chompkeeper
UPDATE `creature_template` SET `unit_flags`=537133312, `unit_flags2`=2049 WHERE `entry`=721; -- Rabbit
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=94248; -- Stonebinder Gorgrogg
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=94250; -- Stonebinder Agrogg
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `VerifiedBuild`=23420 WHERE `entry`=102869; -- Gilnean Warmonger
UPDATE `creature_template` SET `modelid1`=22255, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=103592; -- Bonebeak Hawk
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=108185; -- Coldscale Gazecrawler
UPDATE `creature_template` SET `unit_flags`=768, `VerifiedBuild`=23420 WHERE `entry`=99862; -- Great Eagle
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=96268; -- Mountain Prowler
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=94151; -- Pinerock Elderhorn
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=2049 WHERE `entry`=108810; -- Chloe
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=34817 WHERE `entry`=108386; -- Ambrena
UPDATE `creature_template` SET `subname`='Exotic Imports', `VerifiedBuild`=23420 WHERE `entry`=112063; -- Cornelius Crispin
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=111056; -- Tiny Illusory Dancer
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=106919; -- Nightborne Courier
UPDATE `creature_template` SET `name`='Libation Vendor', `VerifiedBuild`=23420 WHERE `entry`=114530; -- Concerned Merchant
UPDATE `creature_template` SET `npcflag`=4225, `VerifiedBuild`=23420 WHERE `entry`=93979; -- Leyweaver Jorjana
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=102747; -- Tamed Owl
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114892; -- Withering Civilian
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114888; -- Shal'dorei Civilian
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=111558; -- Felgaze Doomseer
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=100778; -- Nightborne Trapper
UPDATE `creature_template` SET `name`='Ley Line Hunter', `VerifiedBuild`=23420 WHERE `entry`=100237; -- Leyline Hunter
UPDATE `creature_template` SET `name`='Ley Line Researcher', `VerifiedBuild`=23420 WHERE `entry`=111871; -- Leyline Researcher
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=107758; -- Amberfall Greatstag
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912 WHERE `entry`=104321; -- Slippery Stormray School
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=108890; -- Runewood Greatstag
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93094; -- Restless Ancestor
UPDATE `creature_template` SET `modelid1`=68157, `HoverHeight`=1 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=92072; -- Grapple Point
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=108885; -- Aegir Wavecrusher
UPDATE `creature_template` SET `speed_walk`=0.35, `speed_run`=0.4, `VerifiedBuild`=23420 WHERE `entry`=105436; -- Voldgar
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `VerifiedBuild`=23420 WHERE `entry`=105443; -- Uldgar
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95676; -- Odyn
UPDATE `creature_template` SET `unit_flags`=16 WHERE `entry`=88981; -- Ironclaw Scuttler
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=103514; -- Leystone Basilisk
UPDATE `creature_template` SET `HoverHeight`=0.25 WHERE `entry`=111787; -- Great Sea Ray
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=100179; -- Willbreaker Incubator
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2824, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=112705; -- Achronos
UPDATE `creature_template` SET `maxlevel`=108 WHERE `entry`=102587; -- Saa'ra
UPDATE `creature_template` SET `minlevel`=98 WHERE `entry`=107565; -- Lightspawn
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=109 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=103092; -- The Great Akazamzarak
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=0.666668, `speed_run`=0.8571429, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2099200 WHERE `entry`=108208; -- Beast of Nightmare
UPDATE `creature_template` SET `unit_flags`=537133312, `unit_flags2`=2049 WHERE `entry`=721; -- Rabbit
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=34816 WHERE `entry`=108659; -- Death Blossom
UPDATE `creature_template` SET `unit_flags`=131328, `VerifiedBuild`=23420 WHERE `entry`=102682; -- Lethon
UPDATE `creature_template` SET `unit_flags`=131328, `VerifiedBuild`=23420 WHERE `entry`=102681; -- Taerar
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1, `unit_flags`=32768 WHERE `entry`=113092; -- Swarming Dread
UPDATE `creature_template` SET `speed_walk`=2.6, `speed_run`=0.9285714, `unit_flags`=32768 WHERE `entry`=113090; -- Corrupted Gatewarden
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=113089; -- Defiled Keeper
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=102672; -- Nythendra
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=112052; -- Vilepetal Rooter
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=104220; -- Starving Ettin
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=104226; -- Gloomfang
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=103805; -- Sablehorn Doe
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=113184; -- Cunning Bushtail
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113818; -- Glitterpool Chick
UPDATE `creature_template` SET `HoverHeight`=5 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1, `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=90173; -- Arcana Stalker
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=23420 WHERE `entry`=90164; -- Warbringer Mox'na
UPDATE `creature_template` SET `unit_flags`=33536 WHERE `entry`=89112; -- Shipwrecked Captive
UPDATE `creature_template` SET `unit_flags`=295680 WHERE `entry`=112874; -- Landon Doi
UPDATE `creature_template` SET `difficulty_entry_1`=0, `scale`=1 WHERE `entry`=26730; -- Mage Slayer
UPDATE `creature_template` SET `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=106990; -- Chief Bitterbrine
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_walk`=1, `speed_run`=0.9920629 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=107328; -- Netherflame Infernal
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=107216; -- Legion Jailer
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=93973; -- Leyweaver Phaxondus
UPDATE `creature_template` SET `maxlevel`=6 WHERE `entry`=107171; -- Azure Whelpling
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2099200 WHERE `entry`=88094; -- Sea Skrog
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=91265; -- Llothien Fox
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=91645; -- Darkfiend Dreamworg
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049 WHERE `entry`=109382; -- Val'sharah Druid
UPDATE `creature_template` SET `npcflag`=82 WHERE `entry`=98135; -- Wildcrafter Osme
UPDATE `creature_template` SET `unit_flags`=537165824, `unit_flags2`=2049 WHERE `entry`=95719; -- Druid of the Antler
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=93318; -- Ashmaw Mauler
UPDATE `creature_template` SET `unit_flags`=33555200 WHERE `entry`=110032; -- Festering Eye
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=640, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=112626; -- Phanthanhes Elune'alor
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049 WHERE `entry`=109364; -- Val'sharah Druid
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=640, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=112626; -- Phanthanhes Elune'alor
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=97504; -- Wraithtalon
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049 WHERE `entry`=109382; -- Val'sharah Druid
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049 WHERE `entry`=109364; -- Val'sharah Druid
UPDATE `creature_template` SET `unit_flags`=33555200 WHERE `entry`=110032; -- Festering Eye
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048 WHERE `entry`=93064; -- Black Rook Falcon
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=92617; -- Bradensbrook Villager
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=93318; -- Ashmaw Mauler
UPDATE `creature_template` SET `speed_walk`=0.34, `speed_run`=0.3, `VerifiedBuild`=23420 WHERE `entry`=111383; -- Lytheron Gloomscale
UPDATE `creature_template` SET `speed_walk`=0.35, `speed_run`=0.4, `VerifiedBuild`=23420 WHERE `entry`=105436; -- Voldgar
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `VerifiedBuild`=23420 WHERE `entry`=105443; -- Uldgar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2824, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=112705; -- Achronos
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=112052; -- Vilepetal Rooter
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=93030; -- Ironbranch
UPDATE `creature_template` SET `maxlevel`=108 WHERE `entry`=102587; -- Saa'ra
UPDATE `creature_template` SET `minlevel`=98 WHERE `entry`=107565; -- Lightspawn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=4, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=557056 WHERE `entry`=107592; -- Ice Shards
UPDATE `creature_template` SET `unit_flags`=768 WHERE `entry`=102263; -- Skorpyron
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=106522; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=33556480 WHERE `entry`=108593; -- Sightless Watcher
UPDATE `creature_template` SET `unit_flags2`=35653632 WHERE `entry`=108934; -- Tainted Blood
UPDATE `creature_template` SET `unit_flags2`=2099200 WHERE `entry`=112973; -- Duskwatch Weaver
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=116256; -- Victoire
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=115499; -- Silgryn
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=115595; -- Duskwatch Executor
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=108063; -- Korine
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=108870; -- Sylverin
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=113184; -- Cunning Bushtail
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113818; -- Glitterpool Chick
UPDATE `creature_template` SET `HoverHeight`=5 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `npcflag`=1, `unit_flags`=33587456 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=8 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=44158; -- Orgrimmar Skyway Peon
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=109 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `minlevel`=101, `speed_walk`=1, `speed_run`=0.9920629 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=99349; -- Robert "Chance" Llore
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=103092; -- The Great Akazamzarak
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=106522; -- Archmage Khadgar
UPDATE `creature_template` SET `unit_flags2`=2099200 WHERE `entry`=112973; -- Duskwatch Weaver
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=33556480 WHERE `entry`=108593; -- Sightless Watcher
UPDATE `creature_template` SET `unit_flags2`=35653632 WHERE `entry`=108934; -- Tainted Blood
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=115499; -- Silgryn
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=116256; -- Victoire
UPDATE `creature_template` SET `unit_flags`=768 WHERE `entry`=102263; -- Skorpyron
UPDATE `creature_template` SET `npcflag`=1, `unit_flags`=33587456 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `minlevel`=1 WHERE `entry`=42548; -- Muddy Crawfish
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=8 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=44158; -- Orgrimmar Skyway Peon
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048 WHERE `entry`=113770; -- Silver Hand Shieldbearer
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048 WHERE `entry`=113769; -- Silver Hand Shieldbearer
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048 WHERE `entry`=113632; -- Silver Hand Templar
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048 WHERE `entry`=113764; -- Silver Hand Knight
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048 WHERE `entry`=113766; -- Silver Hand Knight
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048 WHERE `entry`=113768; -- Silver Hand Protector
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=23420 WHERE `entry`=111706; -- Boulder
UPDATE `creature_template` SET `speed_walk`=0.6, `speed_run`=0.2142857, `VerifiedBuild`=23420 WHERE `entry`=99664; -- Restless Soul
UPDATE `creature_template` SET `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=98792; -- Wyrmtongue Scavenger
UPDATE `creature_template` SET `IconName`='trainer', `VerifiedBuild`=23420 WHERE `entry`=109901; -- Sir Alamande Graythorn
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95676; -- Odyn
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `VerifiedBuild`=23420 WHERE `entry`=90902; -- Dread-Rider Malwick
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `BaseAttackTime`=2000, `VerifiedBuild`=23420 WHERE `entry`=93166; -- Tiptog the Lost
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=108885; -- Aegir Wavecrusher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=99675; -- Enormous Stone Quilen
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=0.888888, `speed_run`=0.9523814, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048 WHERE `entry`=99676; -- Mogu'shan Secret-Keeper
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=111757; -- Warden Trainee
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=107758; -- Amberfall Greatstag
UPDATE `creature_template` SET `unit_flags2`=67110912 WHERE `entry`=102104; -- Enslaved Shieldmaiden
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912 WHERE `entry`=104321; -- Slippery Stormray School
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=92072; -- Grapple Point
UPDATE `creature_template` SET `modelid1`=68157, `HoverHeight`=1 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `unit_flags`=16 WHERE `entry`=88981; -- Ironclaw Scuttler
UPDATE `creature_template` SET `HoverHeight`=0.25 WHERE `entry`=111787; -- Great Sea Ray
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=100179; -- Willbreaker Incubator
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=115608; -- Silgryn
UPDATE `creature_template` SET `speed_walk`=0.35, `speed_run`=0.4, `VerifiedBuild`=23420 WHERE `entry`=105436; -- Voldgar
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `VerifiedBuild`=23420 WHERE `entry`=105443; -- Uldgar
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=109 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=105921; -- Nightborne Spellsword
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2824, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=112705; -- Achronos
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=111558; -- Felgaze Doomseer
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=107632; -- Ly'leth Lunastre
UPDATE `creature_template` SET `HealthScalingExpansion`=-1, `VerifiedBuild`=23420 WHERE `entry`=60561; -- Earthgrab Totem
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=105 WHERE `entry`=110428; -- Hemet Nesingwary
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=109411; -- Shadescale Flyeater
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=108870; -- Sylverin
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=111062; -- Su'esh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=111019; -- Nighteyes
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=104161; -- Brambley Morrison
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=114797; -- Angus Stormbrew
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=103852; -- Brambley Morrison
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200 WHERE `entry`=103670; -- Brambley Morrison
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2110, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=33589248 WHERE `entry`=100878; -- Lyana Darksorrow
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2110, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=33556480 WHERE `entry`=100823; -- Lyana Darksorrow
UPDATE `creature_template` SET `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048 WHERE `entry`=107292; -- Legion Ship Beam Bunny
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=0.888, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=99485; -- Kozak the Afflictor
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=2, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102365; -- Selthaes Starsong
UPDATE `creature_template` SET `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554944, `unit_flags2`=34816, `VehicleId`=4564 WHERE `entry`=102217; -- Black Tome Floating Bunny
UPDATE `creature_template` SET `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554944, `unit_flags2`=34816, `VehicleId`=4556 WHERE `entry`=102216; -- Black Tome Spinner Bunny
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=34816 WHERE `entry`=99462; -- The Black Tome
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=103204; -- Angus Stormbrew
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048 WHERE `entry`=102476; -- Prison Shackle Bunny
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048 WHERE `entry`=102450; -- Prison Shackle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102442; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200 WHERE `entry`=100019; -- Azoran
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=103671; -- Mangelrath
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102759; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102754; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102752; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102758; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102753; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102713; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102757; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102750; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048 WHERE `entry`=102933; -- Fel Meteor Bunny
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=99122; -- Withered Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=0.666668, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048 WHERE `entry`=102837; -- Servant of Azoran
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2110, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=33589248 WHERE `entry`=99890; -- Lyana Darksorrow
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048 WHERE `entry`=99722; -- Image of Azoran
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102755; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102756; -- Felsoul Captive
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048 WHERE `entry`=102595; -- Summoning Crystal Bunny
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=18432 WHERE `entry`=99117; -- Soul Harvester
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=102292; -- Grimwing the Devourer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=100595; -- Baelbug
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2110, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=33589248 WHERE `entry`=99514; -- Lyana Darksorrow
UPDATE `creature_template` SET `npcflag`=16777216 WHERE `entry`=101987; -- Demonic Tome
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048 WHERE `entry`=110858; -- Soul Harvester
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `npcflag`=2, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33024, `unit_flags2`=2048 WHERE `entry`=100779; -- Thaedris Feathersong
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `npcflag`=3, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33024, `unit_flags2`=2048 WHERE `entry`=99575; -- Thaedris Feathersong
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=103514; -- Leystone Basilisk
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33024, `unit_flags2`=2048 WHERE `entry`=99483; -- Thaedris Feathersong
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=99610; -- Garvrulg
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.111111, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=101577; -- Tanzanite Shatterer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=101581; -- Tanzanite Borer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=101580; -- Tanzanite Skitterer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `npcflag`=3, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33024, `unit_flags2`=34816 WHERE `entry`=99093; -- Thaedris Feathersong
UPDATE `creature_template` SET `faction`=35, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048 WHERE `entry`=105677; -- Remy
UPDATE `creature_template` SET `faction`=35, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048 WHERE `entry`=105678; -- Dinner
UPDATE `creature_template` SET `faction`=35, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048 WHERE `entry`=105675; -- Gusteau
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2855, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048 WHERE `entry`=105674; -- Varenne
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=100780; -- Trapper Rodoon
UPDATE `creature_template` SET `name`='Ley Line Researcher', `VerifiedBuild`=23420 WHERE `entry`=111871; -- Leyline Researcher
UPDATE `creature_template` SET `name`='Ley Line Hunter', `VerifiedBuild`=23420 WHERE `entry`=100237; -- Leyline Hunter
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=103223; -- Hertha Grimdottir
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=100778; -- Nightborne Trapper
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=116068; -- Arm of the Magistrix
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=101825; -- Nightborne Enforcer
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=101821; -- Nightborne Warpcaster
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=104837; -- Caged Tiger
UPDATE `creature_template` SET `unit_flags`=33536, `VerifiedBuild`=23420 WHERE `entry`=107134; -- Projection of Aargoss
UPDATE `creature_template` SET `npcflag`=131 WHERE `entry`=93976; -- Leyweaver Mithrogane
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=115595; -- Duskwatch Executor
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113818; -- Glitterpool Chick
UPDATE `creature_template` SET `HoverHeight`=5 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=108063; -- Korine
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114888; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=114892; -- Withering Civilian
UPDATE `creature_template` SET `npcflag`=4225, `VerifiedBuild`=23420 WHERE `entry`=93979; -- Leyweaver Jorjana
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=102747; -- Tamed Owl
UPDATE `creature_template` SET `name`='Libation Vendor', `VerifiedBuild`=23420 WHERE `entry`=114530; -- Concerned Merchant
UPDATE `creature_template` SET `subname`='Exotic Imports', `VerifiedBuild`=23420 WHERE `entry`=112063; -- Cornelius Crispin
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=106919; -- Nightborne Courier
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=111056; -- Tiny Illusory Dancer
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=107598; -- Vanthir
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=113184; -- Cunning Bushtail
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=98548; -- Chief Telemancer Oculeth
UPDATE `creature_template` SET `rank`=0, `VerifiedBuild`=23420 WHERE `entry`=99304; -- Trenchwalker Scavenger
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=104226; -- Gloomfang
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=99349; -- Robert "Chance" Llore
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=103092; -- The Great Akazamzarak
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_walk`=1, `speed_run`=0.9920629 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1.385714 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=98 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=105 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=103, `speed_run`=1 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `minlevel`=109, `maxlevel`=109 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=18 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=23420 WHERE `entry`=56042; -- Mule
UPDATE `creature_template` SET `maxlevel`=98 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `npcflag`=81 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `maxlevel`=100 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `minlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=98 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=105 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=103, `speed_run`=1 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `minlevel`=109, `maxlevel`=109 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=18 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=23420 WHERE `entry`=56042; -- Mule
UPDATE `creature_template` SET `maxlevel`=98 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `npcflag`=81 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `maxlevel`=100 WHERE `entry`=105904; -- Raven

DELETE FROM `gameobject_template` WHERE `entry` IN (266305 /*Arcane Disk*/, 252679 /*Peerless Challenger's Cache*/, 266059 /*Draconic Compendium, Volume IV*/, 266058 /*Handbook of Feathery Friends*/, 266057 /*Incredible Monsters and Where to Locate Them*/, 266165 /*Mana Pylon*/, 266164 /*Mana Pylon*/, 266163 /*Mana Pylon*/, 266162 /*Mana Pylon*/, 266404 /*Bookcase*/, 266400 /*Generic Rubble B*/, 265520 /*Master's Vase*/, 266403 /*Book Pile 1*/, 266402 /*Generic Rubble Glass B*/, 266399 /*Generic Rubble A*/, 266401 /*Generic Rubble Glass A*/, 249422 /*Timeworn Jar*/, 258887 /*Ashbringer Appearance*/, 258891 /*Ashbringer Appearance*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(266305, 10, 39088, 'Arcane Disk', '', '', '', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Arcane Disk
(252679, 3, 33268, 'Peerless Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1835, 0, 1, 0, 0, 68457, 0, 5, 110, 23420), -- Peerless Challenger's Cache
(266059, 3, 29525, 'Draconic Compendium, Volume IV', 'questinteract', 'Collecting', '', 1, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19676, 0, 0, 45879, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70366, 1, 0, 0, 23420), -- Draconic Compendium, Volume IV
(266058, 3, 34063, 'Handbook of Feathery Friends', 'questinteract', 'Collecting', '', 1, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19676, 0, 0, 45878, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70365, 1, 0, 0, 23420), -- Handbook of Feathery Friends
(266057, 3, 34062, 'Incredible Monsters and Where to Locate Them', 'questinteract', 'Collecting', '', 1, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19676, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70364, 1, 0, 0, 23420), -- Incredible Monsters and Where to Locate Them
(266165, 5, 26475, 'Mana Pylon', 'questinteract', '', '', 1.2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mana Pylon
(266164, 5, 26475, 'Mana Pylon', 'questinteract', '', '', 1.2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mana Pylon
(266163, 5, 26475, 'Mana Pylon', 'questinteract', '', '', 1.2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mana Pylon
(266162, 5, 26475, 'Mana Pylon', 'questinteract', '', '', 1.2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mana Pylon
(266404, 5, 39109, 'Bookcase', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bookcase
(266400, 5, 38878, 'Generic Rubble B', '', '', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Generic Rubble B
(265520, 22, 38880, 'Master''s Vase', 'questinteract', '', '', 1, 229000, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Master's Vase
(266403, 5, 39108, 'Book Pile 1', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Book Pile 1
(266402, 5, 36390, 'Generic Rubble Glass B', '', '', '', 0.7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Generic Rubble Glass B
(266399, 5, 38877, 'Generic Rubble A', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Generic Rubble A
(266401, 5, 36392, 'Generic Rubble Glass A', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Generic Rubble Glass A
(249422, 3, 30631, 'Timeworn Jar', '', '', '', 1, 57, 67142, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81040, 0, 0, 0, 0, 0, 0, 0, 23420), -- Timeworn Jar
(258887, 5, 37868, 'Ashbringer Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Ashbringer Appearance
(258891, 5, 37865, 'Ashbringer Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- Ashbringer Appearance

DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=266059 AND `Idx`=0) OR (`GameObjectEntry`=266058 AND `Idx`=0) OR (`GameObjectEntry`=266057 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(266059, 0, 142240, 23420), -- Draconic Compendium, Volume IV
(266058, 0, 142239, 23420), -- Handbook of Feathery Friends
(266057, 0, 142238, 23420); -- Incredible Monsters and Where to Locate Them

DELETE FROM `npc_text` WHERE `ID` IN (28532 /*28532*/, 29229 /*29229*/, 29218 /*29218*/, 29253 /*29253*/, 28972 /*28972*/, 28836 /*28836*/, 31106 /*31106*/, 31058 /*31058*/, 29853 /*29853*/, 30431 /*30431*/, 30405 /*30405*/);
INSERT INTO `npc_text` (`ID`, `Probability0`, `Probability1`, `Probability2`, `Probability3`, `Probability4`, `Probability5`, `Probability6`, `Probability7`, `BroadcastTextId0`, `BroadcastTextId1`, `BroadcastTextId2`, `BroadcastTextId3`, `BroadcastTextId4`, `BroadcastTextId5`, `BroadcastTextId6`, `BroadcastTextId7`, `VerifiedBuild`) VALUES
(28532, 1, 0, 0, 0, 0, 0, 0, 0, 103229, 0, 0, 0, 0, 0, 0, 0, 23420), -- 28532
(29229, 1, 1, 1, 1, 0, 0, 0, 0, 111717, 111718, 111719, 111720, 0, 0, 0, 0, 23420), -- 29229
(29218, 1, 1, 1, 1, 1, 1, 1, 1, 111690, 111691, 111692, 111693, 111694, 111695, 111696, 111697, 23420), -- 29218
(29253, 1, 0, 0, 0, 0, 0, 0, 0, 111837, 0, 0, 0, 0, 0, 0, 0, 23420), -- 29253
(28972, 1, 0, 0, 0, 0, 0, 0, 0, 109455, 0, 0, 0, 0, 0, 0, 0, 23420), -- 28972
(28836, 1, 0, 0, 0, 0, 0, 0, 0, 108580, 0, 0, 0, 0, 0, 0, 0, 23420), -- 28836
(31106, 1, 0, 0, 0, 0, 0, 0, 0, 125569, 0, 0, 0, 0, 0, 0, 0, 23420), -- 31106
(31058, 1, 0, 0, 0, 0, 0, 0, 0, 125359, 0, 0, 0, 0, 0, 0, 0, 23420), -- 31058
(29853, 1, 0, 0, 0, 0, 0, 0, 0, 118749, 0, 0, 0, 0, 0, 0, 0, 23420), -- 29853
(30431, 1, 0, 0, 0, 0, 0, 0, 0, 122231, 0, 0, 0, 0, 0, 0, 0, 23420), -- 30431
(30405, 1, 0, 0, 0, 0, 0, 0, 0, 119001, 0, 0, 0, 0, 0, 0, 0, 23420); -- 30405

DELETE FROM `quest_details` WHERE `ID` IN (13962 /*Stalemate*/, 13983 /*Building Your Own Coffin*/, 13980 /*They're Out There!*/, 13977 /*Mass Production*/, 13888 /*Vortex*/, 13880 /*Hot Lava*/, 13884 /*Put Out The Fire*/, 27698 /*Servants of Theradras*/, 13879 /*Thunder Peak*/, 13974 /*Tweedle's Tiny Package*/, 13958 /*Condition Critical!*/, 26905 /*Agamaggan's Charge*/, 26907 /*Take Them Down!*/, 26906 /*Agamaggan*/, 26901 /*Going, Going, Guano!*/, 13947 /*Blastranaar!*/, 13944 /*Small Hands, Short Fuse*/, 13943 /*Breathing Room*/, 13942 /*Set Us Up the Bomb*/, 13936 /*Tweedle's Dumb*/, 13923 /*To Hellscream's Watch*/, 31034 /*Enemies Below*/, 13842 /*Dread Head Redemption*/, 26417 /*Northern Stranglethorn: The Fallen Empire*/, 34673 /*The Rise of Aku'mai*/, 13901 /*Deep Despair*/, 13920 /*Before You Go...*/, 13883 /*Lousy Pieces of Ship*/, 26890 /*The Essence of Aku'Mai*/, 13890 /*Keep the Fires Burning*/, 26894 /*Blackfathom Deeps*/, 824 /*Je'neu of the Earthen Ring*/, 13841 /*All Apologies*/, 26011 /*Enemy of the Horde: Marshal Paltrow*/, 26026 /*Dream of a Better Tomorrow*/, 26010 /*Ashes to Ashes*/, 26004 /*Krom'gar Fortress*/, 26002 /*Alliance Attack Plans*/, 26003 /*Lessons from the Lost Isles*/, 26001 /*The Missing Blastgineer*/, 25999 /*Barrier to Entry*/, 6621 /*King of the Foulweald*/, 1918 /*The Befouled Element*/, 25945 /*We're Here to Do One Thing, Maybe Two...*/, 25 /*Simmer Down Now*/, 26416 /*Well, Come to the Jungle*/, 13967 /*Thinning the... Herd?*/, 13798 /*Rain of Destruction*/, 13797 /*Dirty Deeds*/, 13751 /*Tell No One!*/, 13875 /*Gurtar's Request*/, 13873 /*Sheelah's Last Wish*/, 13871 /*Security!*/, 13870 /*As Good as it Gets*/, 13865 /*Wet Work*/, 13815 /*Making Stumps*/, 31513 /*Blades of the Anointed*/, 28549 /*Warchief's Command: Southern Barrens!*/, 31490 /*Rank and File*/, 31493 /*Just for Safekeeping, Of Course*/, 2945 /*Grime-Encrusted Ring*/, 6441 /*Satyr Horns*/, 13801 /*Dead Elves Walking*/, 13806 /*Demon Duty*/, 13808 /*Mission Improbable*/, 13848 /*Bad News Bear-er*/, 13730 /*Playing With Felfire*/, 13805 /*Pierce Their Heart!*/, 13803 /*Blood of the Weak*/, 26449 /*Never Again!*/, 26447 /*Diabolical Plans*/, 26448 /*Destroy the Legion*/, 6503 /*Ashenvale Outrunners*/, 13712 /*To the Rescue!*/, 13653 /*Crisis at Splintertree*/, 13651 /*Needs a Little Lubrication*/, 2 /*Sharptalon's Claw*/, 6544 /*Torek's Assault*/, 13640 /*Management Material*/, 13628 /*Got Wood?*/, 13621 /*Gorat's Vengeance*/, 13620 /*To Dinah, at Once!*/, 13619 /*Final Report*/, 29111 /*Mor'shan Caravan Delivery*/, 29112 /*Demon Seed*/, 29110 /*Mor'shan Caravan Rescue*/, 29109 /*Mor'shan Caravan Pick-Up*/, 29095 /*Report to Thork*/, 876 /*Serena Bloodfeather*/, 875 /*Harpy Lieutenants*/, 867 /*Harpy Raiders*/, 29094 /*The Short Way Home*/, 29015 /*Miner's Fortune*/, 14006 /*Read the Manual*/, 14004 /*Return to Samophlanger*/, 13613 /*Rescue the Fallen*/, 13612 /*Mor'shan Defense*/, 13618 /*Find Gorat!*/, 13615 /*Empty Quivers*/, 29027 /*Nugget Slugs*/, 14068 /*Waptor Twapping*/, 14067 /*The Stolen Silver*/, 869 /*To Track a Thief*/, 29026 /*Wenikee Boltbucket*/, 14003 /*Samophlange Repair*/, 29024 /*Samophlange*/, 29023 /*Samophlange*/, 29022 /*Samophlange*/, 863 /*The Escape*/, 29089 /*Sludge Beast!*/, 29087 /*Sludge Investigation*/, 29088 /*Hyena Extermination*/, 26769 /*Raging River Ride*/, 14042 /*Ammo Kerblammo*/, 14050 /*Gazlowe's Fortune*/, 14038 /*Love it or Limpet*/, 14063 /*Mutiny, Mon!*/, 14057 /*Guns. We Need Guns.*/, 14056 /*Glomp is Sitting On It*/, 14049 /*A Most Unusual Map*/, 14046 /*The Baron's Demands*/, 14052 /*Take it up with Tony*/, 4021 /*Counterattack!*/, 852 /*Hezrul Bloodmark*/, 855 /*Centaur Bracers*/, 14073 /*Deathgate's Reinforcements*/, 13999 /*Who's Shroomin' Who?*/, 13991 /*The Purloined Payroll*/, 851 /*Verog the Dervish*/, 14072 /*Flushing Out Verog*/, 13995 /*King of Centaur Mountain*/, 880 /*Altered Beings*/, 877 /*The Stagnant Oasis*/, 32669 /*Learn To Ride*/, 905 /*Into the Raptor's Den*/, 13992 /*A Little Diversion*/, 850 /*Kolkar Leaders*/, 30984 /*No Orc Left Behind*/, 30983 /*The Dark Shaman*/, 30969 /*A New Enemy*/, 848 /*Fungal Spores*/, 845 /*The Zhevra*/, 903 /*Hunting the Huntress*/, 870 /*The Forgotten Pools*/, 26878 /*Disciples of Naralex*/, 13971 /*The Kodo's Return*/, 13970 /*Animal Services*/, 13969 /*Grol'dom's Missing Kodo*/, 13968 /*The Tortusk Takedown*/, 13963 /*By Hook Or By Crook*/, 13975 /*Crossroads Caravan Delivery*/, 13961 /*Drag it Out of Them*/, 13949 /*Crossroads Caravan Pickup*/, 5041 /*Supplies for the Crossroads*/, 13878 /*Through Fire and Flames*/, 6386 /*Return to Razor Hill*/, 29401 /*Blown Away*/, 14129 /*Runaway Shredder!*/, 26543 /*Clammy Hands*/, 31585 /*Learning the Ropes*/, 6385 /*Doras the Wind Rider Master*/, 26642 /*Preserving the Barrens*/, 25648 /*Beyond Durotar*/, 31571 /*Learning the Ropes*/, 6384 /*Ride to Orgrimmar*/, 6365 /*Meats to Orgrimmar*/, 24814 /*An Ancient Enemy*/, 24813 /*Territorial Fetish*/, 24812 /*No More Mercy*/, 25035 /*Breaking the Line*/, 24626 /*Young and Vicious*/, 24625 /*Consort of the Sea Witch*/, 24624 /*Mercy for the Lost*/, 24623 /*Saving the Young*/, 24763 /*More Than Expected*/, 24762 /*Proving Pit*/, 24761 /*A Rough Start*/, 24759 /*The Basics: Hitting Things*/, 25167 /*Breaking the Chain*/, 25170 /*Cleaning Up the Coastline*/, 25133 /*Report to Sen'jin Village*/, 25135 /*Thazz'ril's Pick*/, 25132 /*Burning Blade Medallion*/, 25130 /*Back to the Den*/, 25128 /*Hana'zua*/, 25131 /*Vile Familiars*/, 37446 /*Lazy Peons*/, 25127 /*Sting of the Scorpid*/, 25136 /*Galgar's Cactus Apple Surprise*/, 25172 /*Invaders in Our Home*/, 44284 /*Piercing the Veil*/, 43942 /*Timeworn Artifact*/, 24621 /*Smarts-is-Smarts*/, 25292 /*Next of Kin*/, 24747 /*Sabotage!*/, 24685 /*Dwarf Fortress*/, 24637 /*The Butcher of Taurajo*/, 31440 /*The Four Tomes*/, 31447 /*An End to the Suffering*/, 24618 /*Claim the Battle Scar*/, 24634 /*Intelligence Warfare*/, 24632 /*Tauren Vengeance*/, 24667 /*Firestone Point*/, 24620 /*Hearts-is-Hearts*/, 24619 /*Parts-is-Parts*/, 24654 /*Silithissues*/, 24591 /*Changing of the Gar'dul*/, 24577 /*Desolation Hold Inspection*/, 24573 /*Honoring the Dead*/, 24572 /*Taking Back Taurajo*/, 24569 /*Siegebreaker*/, 24552 /*Lion's Pride*/, 24546 /*A Line in the Dirt*/, 24807 /*Winnoa Pineforest*/, 24601 /*The Nightmare Scar*/, 24534 /*Speaking Their Language*/, 24542 /*A Curious Bloom*/, 24543 /*A Family Divided*/, 24566 /*Sowing a Solution*/, 24574 /*To Harvest Chaos*/, 24513 /*Eye for an Eye*/, 24512 /*Warriors' Redemption*/, 25284 /*Feeding the Fear*/, 24519 /*Stalling the Survey*/, 24518 /*The Low Road*/, 24514 /*Raptor Scraps*/, 24515 /*Signals in the Sky*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(13962, 396, 396, 0, 0, 0, 0, 0, 0, 23420), -- Stalemate
(13983, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Building Your Own Coffin
(13980, 396, 396, 0, 0, 0, 1000, 0, 0, 23420), -- They're Out There!
(13977, 5, 396, 396, 0, 0, 1000, 1000, 0, 23420), -- Mass Production
(13888, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Vortex
(13880, 33, 0, 0, 0, 1000, 0, 0, 0, 23420), -- Hot Lava
(13884, 33, 0, 0, 0, 0, 0, 0, 0, 23420), -- Put Out The Fire
(27698, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Servants of Theradras
(13879, 396, 396, 0, 0, 0, 1000, 0, 0, 23420), -- Thunder Peak
(13974, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Tweedle's Tiny Package
(13958, 396, 396, 0, 0, 0, 1000, 0, 0, 23420), -- Condition Critical!
(26905, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Agamaggan's Charge
(26907, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Take Them Down!
(26906, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Agamaggan
(26901, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- Going, Going, Guano!
(13947, 6, 396, 5, 0, 0, 1000, 1000, 0, 23420), -- Blastranaar!
(13944, 5, 5, 5, 0, 0, 1000, 1000, 0, 23420), -- Small Hands, Short Fuse
(13943, 396, 5, 0, 0, 0, 1000, 0, 0, 23420), -- Breathing Room
(13942, 5, 396, 5, 0, 0, 1000, 1000, 0, 23420), -- Set Us Up the Bomb
(13936, 5, 5, 0, 0, 0, 1000, 0, 0, 23420), -- Tweedle's Dumb
(13923, 396, 396, 0, 0, 0, 500, 0, 0, 23420), -- To Hellscream's Watch
(31034, 15, 0, 0, 0, 0, 0, 0, 0, 23420), -- Enemies Below
(13842, 5, 396, 0, 0, 0, 500, 0, 0, 23420), -- Dread Head Redemption
(26417, 1, 25, 1, 0, 0, 0, 0, 0, 23420), -- Northern Stranglethorn: The Fallen Empire
(34673, 396, 396, 396, 0, 0, 0, 0, 0, 23420), -- The Rise of Aku'mai
(13901, 396, 5, 0, 0, 0, 500, 0, 0, 23420), -- Deep Despair
(13920, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Before You Go...
(13883, 5, 396, 0, 0, 0, 1000, 0, 0, 23420), -- Lousy Pieces of Ship
(26890, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- The Essence of Aku'Mai
(13890, 396, 397, 0, 0, 0, 500, 0, 0, 23420), -- Keep the Fires Burning
(26894, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Blackfathom Deeps
(824, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Je'neu of the Earthen Ring
(13841, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- All Apologies
(26011, 1, 1, 5, 0, 0, 0, 0, 0, 23420), -- Enemy of the Horde: Marshal Paltrow
(26026, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- Dream of a Better Tomorrow
(26010, 1, 1, 5, 0, 0, 0, 0, 0, 23420), -- Ashes to Ashes
(26004, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- Krom'gar Fortress
(26002, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Alliance Attack Plans
(26003, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- Lessons from the Lost Isles
(26001, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Missing Blastgineer
(25999, 5, 1, 25, 0, 0, 0, 0, 0, 23420), -- Barrier to Entry
(6621, 5, 0, 0, 0, 0, 0, 0, 0, 23420), -- King of the Foulweald
(1918, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Befouled Element
(25945, 1, 5, 1, 15, 0, 0, 0, 0, 23420), -- We're Here to Do One Thing, Maybe Two...
(25, 5, 1, 153, 0, 0, 500, 500, 0, 23420), -- Simmer Down Now
(26416, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Well, Come to the Jungle
(13967, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Thinning the... Herd?
(13798, 1, 1, 0, 0, 0, 1000, 0, 0, 23420), -- Rain of Destruction
(13797, 1, 1, 0, 0, 0, 1000, 0, 0, 23420), -- Dirty Deeds
(13751, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Tell No One!
(13875, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Gurtar's Request
(13873, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sheelah's Last Wish
(13871, 5, 5, 5, 0, 0, 500, 500, 0, 23420), -- Security!
(13870, 1, 1, 0, 0, 0, 1000, 0, 0, 23420), -- As Good as it Gets
(13865, 1, 1, 0, 0, 0, 1000, 0, 0, 23420), -- Wet Work
(13815, 1, 1, 0, 0, 0, 1000, 0, 0, 23420), -- Making Stumps
(31513, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Blades of the Anointed
(28549, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- Warchief's Command: Southern Barrens!
(31490, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Rank and File
(31493, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Just for Safekeeping, Of Course
(2945, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Grime-Encrusted Ring
(6441, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Satyr Horns
(13801, 1, 5, 0, 0, 0, 0, 0, 0, 23420), -- Dead Elves Walking
(13806, 1, 5, 1, 0, 0, 1500, 1000, 0, 23420), -- Demon Duty
(13808, 1, 1, 0, 0, 0, 1000, 0, 0, 23420), -- Mission Improbable
(13848, 396, 396, 0, 0, 0, 1000, 0, 0, 23420), -- Bad News Bear-er
(13730, 1, 1, 0, 0, 0, 500, 0, 0, 23420), -- Playing With Felfire
(13805, 1, 153, 5, 0, 0, 1000, 1000, 0, 23420), -- Pierce Their Heart!
(13803, 5, 0, 0, 0, 0, 0, 0, 0, 23420), -- Blood of the Weak
(26449, 5, 1, 1, 0, 0, 1000, 1000, 0, 23420), -- Never Again!
(26447, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Diabolical Plans
(26448, 5, 1, 0, 0, 0, 1000, 0, 0, 23420), -- Destroy the Legion
(6503, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Ashenvale Outrunners
(13712, 396, 0, 0, 0, 0, 0, 0, 0, 23420), -- To the Rescue!
(13653, 1, 5, 1, 0, 0, 500, 500, 0, 23420), -- Crisis at Splintertree
(13651, 1, 1, 0, 0, 0, 500, 0, 0, 23420), -- Needs a Little Lubrication
(2, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sharptalon's Claw
(6544, 5, 1, 0, 0, 0, 0, 0, 0, 23420), -- Torek's Assault
(13640, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Management Material
(13628, 1, 1, 0, 0, 0, 500, 0, 0, 23420), -- Got Wood?
(13621, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Gorat's Vengeance
(13620, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- To Dinah, at Once!
(13619, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Final Report
(29111, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mor'shan Caravan Delivery
(29112, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Demon Seed
(29110, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mor'shan Caravan Rescue
(29109, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mor'shan Caravan Pick-Up
(29095, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Report to Thork
(876, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Serena Bloodfeather
(875, 11, 0, 0, 0, 100, 0, 0, 0, 23420), -- Harpy Lieutenants
(867, 5, 0, 0, 0, 0, 0, 0, 0, 23420), -- Harpy Raiders
(29094, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Short Way Home
(29015, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Miner's Fortune
(14006, 5, 1, 0, 0, 0, 0, 0, 0, 23420), -- Read the Manual
(14004, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Return to Samophlanger
(13613, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Rescue the Fallen
(13612, 5, 1, 1, 0, 0, 500, 500, 0, 23420), -- Mor'shan Defense
(13618, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Find Gorat!
(13615, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Empty Quivers
(29027, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Nugget Slugs
(14068, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Waptor Twapping
(14067, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Stolen Silver
(869, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- To Track a Thief
(29026, 1, 5, 0, 0, 0, 900, 0, 0, 23420), -- Wenikee Boltbucket
(14003, 6, 0, 0, 0, 0, 0, 0, 0, 23420), -- Samophlange Repair
(29024, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Samophlange
(29023, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Samophlange
(29022, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Samophlange
(863, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Escape
(29089, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sludge Beast!
(29087, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sludge Investigation
(29088, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Hyena Extermination
(26769, 1, 5, 0, 0, 0, 0, 0, 0, 23420), -- Raging River Ride
(14042, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Ammo Kerblammo
(14050, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Gazlowe's Fortune
(14038, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Love it or Limpet
(14063, 1, 11, 0, 0, 0, 0, 0, 0, 23420), -- Mutiny, Mon!
(14057, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Guns. We Need Guns.
(14056, 1, 25, 0, 0, 0, 0, 0, 0, 23420), -- Glomp is Sitting On It
(14049, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- A Most Unusual Map
(14046, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Baron's Demands
(14052, 6, 388, 0, 0, 0, 0, 0, 0, 23420), -- Take it up with Tony
(4021, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Counterattack!
(852, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Hezrul Bloodmark
(855, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Centaur Bracers
(14073, 396, 0, 0, 0, 0, 0, 0, 0, 23420), -- Deathgate's Reinforcements
(13999, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Who's Shroomin' Who?
(13991, 273, 1, 0, 0, 0, 900, 0, 0, 23420), -- The Purloined Payroll
(851, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Verog the Dervish
(14072, 1, 6, 0, 0, 0, 0, 0, 0, 23420), -- Flushing Out Verog
(13995, 1, 273, 0, 0, 0, 500, 0, 0, 23420), -- King of Centaur Mountain
(880, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Altered Beings
(877, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Stagnant Oasis
(32669, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Learn To Ride
(905, 5, 0, 0, 0, 0, 0, 0, 0, 23420), -- Into the Raptor's Den
(13992, 66, 1, 0, 0, 0, 800, 0, 0, 23420), -- A Little Diversion
(850, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Kolkar Leaders
(30984, 396, 0, 0, 0, 0, 0, 0, 0, 23420), -- No Orc Left Behind
(30983, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Dark Shaman
(30969, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- A New Enemy
(848, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Fungal Spores
(845, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Zhevra
(903, 5, 0, 0, 0, 100, 0, 0, 0, 23420), -- Hunting the Huntress
(870, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Forgotten Pools
(26878, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Disciples of Naralex
(13971, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Kodo's Return
(13970, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Animal Services
(13969, 6, 273, 0, 0, 0, 1000, 0, 0, 23420), -- Grol'dom's Missing Kodo
(13968, 274, 1, 0, 0, 0, 1000, 0, 0, 23420), -- The Tortusk Takedown
(13963, 1, 274, 0, 0, 300, 1000, 0, 0, 23420), -- By Hook Or By Crook
(13975, 11, 1, 0, 0, 0, 900, 0, 0, 23420), -- Crossroads Caravan Delivery
(13961, 396, 0, 0, 0, 0, 0, 0, 0, 23420), -- Drag it Out of Them
(13949, 1, 25, 0, 0, 100, 900, 0, 0, 23420), -- Crossroads Caravan Pickup
(5041, 1, 1, 0, 0, 0, 500, 0, 0, 23420), -- Supplies for the Crossroads
(13878, 18, 15, 0, 0, 0, 500, 0, 0, 23420), -- Through Fire and Flames
(6386, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Return to Razor Hill
(29401, 3, 396, 25, 0, 0, 1000, 1000, 0, 23420), -- Blown Away
(14129, 6, 0, 0, 0, 0, 0, 0, 0, 23420), -- Runaway Shredder!
(26543, 1, 5, 0, 0, 0, 0, 0, 0, 23420), -- Clammy Hands
(31585, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Learning the Ropes
(6385, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doras the Wind Rider Master
(26642, 2, 1, 0, 0, 0, 0, 0, 0, 23420), -- Preserving the Barrens
(25648, 1, 274, 1, 0, 0, 0, 0, 0, 23420), -- Beyond Durotar
(31571, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Learning the Ropes
(6384, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Ride to Orgrimmar
(6365, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Meats to Orgrimmar
(24814, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- An Ancient Enemy
(24813, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Territorial Fetish
(24812, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- No More Mercy
(25035, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Breaking the Line
(24626, 1, 6, 1, 0, 0, 60, 60, 0, 23420), -- Young and Vicious
(24625, 1, 1, 0, 0, 0, 60, 0, 0, 23420), -- Consort of the Sea Witch
(24624, 1, 1, 0, 0, 0, 60, 0, 0, 23420), -- Mercy for the Lost
(24623, 1, 1, 0, 0, 0, 60, 0, 0, 23420), -- Saving the Young
(24763, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- More Than Expected
(24762, 1, 1, 0, 0, 0, 60, 0, 0, 23420), -- Proving Pit
(24761, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- A Rough Start
(24759, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Basics: Hitting Things
(25167, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Breaking the Chain
(25170, 94, 94, 94, 0, 0, 0, 0, 0, 23420), -- Cleaning Up the Coastline
(25133, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Report to Sen'jin Village
(25135, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Thazz'ril's Pick
(25132, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Burning Blade Medallion
(25130, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Back to the Den
(25128, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Hana'zua
(25131, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Vile Familiars
(37446, 5, 0, 0, 0, 0, 0, 0, 0, 23420), -- Lazy Peons
(25127, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sting of the Scorpid
(25136, 5, 1, 1, 1, 0, 0, 0, 0, 23420), -- Galgar's Cactus Apple Surprise
(25172, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Invaders in Our Home
(44284, 1, 0, 0, 0, 50, 0, 0, 0, 23420), -- Piercing the Veil
(43942, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Timeworn Artifact
(24621, 5, 1, 0, 0, 0, 0, 0, 0, 23420), -- Smarts-is-Smarts
(25292, 1, 274, 0, 0, 0, 0, 0, 0, 23420), -- Next of Kin
(24747, 11, 1, 273, 0, 0, 0, 0, 0, 23420), -- Sabotage!
(24685, 5, 1, 0, 0, 0, 0, 0, 0, 23420), -- Dwarf Fortress
(24637, 5, 15, 0, 0, 0, 0, 0, 0, 23420), -- The Butcher of Taurajo
(31440, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Four Tomes
(31447, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- An End to the Suffering
(24618, 25, 5, 0, 0, 0, 0, 0, 0, 23420), -- Claim the Battle Scar
(24634, 1, 5, 0, 0, 0, 0, 0, 0, 23420), -- Intelligence Warfare
(24632, 1, 273, 0, 0, 0, 0, 0, 0, 23420), -- Tauren Vengeance
(24667, 1, 273, 0, 0, 0, 0, 0, 0, 23420), -- Firestone Point
(24620, 11, 1, 0, 0, 0, 0, 0, 0, 23420), -- Hearts-is-Hearts
(24619, 6, 1, 0, 0, 0, 0, 0, 0, 23420), -- Parts-is-Parts
(24654, 5, 274, 1, 0, 0, 0, 0, 0, 23420), -- Silithissues
(24591, 274, 1, 0, 0, 0, 0, 0, 0, 23420), -- Changing of the Gar'dul
(24577, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Desolation Hold Inspection
(24573, 1, 274, 0, 0, 0, 0, 0, 0, 23420), -- Honoring the Dead
(24572, 1, 15, 25, 0, 0, 0, 0, 0, 23420), -- Taking Back Taurajo
(24569, 273, 1, 66, 0, 0, 0, 0, 0, 23420), -- Siegebreaker
(24552, 1, 6, 0, 0, 0, 0, 0, 0, 23420), -- Lion's Pride
(24546, 5, 1, 25, 0, 0, 0, 0, 0, 23420), -- A Line in the Dirt
(24807, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Winnoa Pineforest
(24601, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Nightmare Scar
(24534, 1, 25, 5, 0, 0, 0, 0, 0, 23420), -- Speaking Their Language
(24542, 1, 5, 273, 0, 0, 0, 0, 0, 23420), -- A Curious Bloom
(24543, 1, 5, 0, 0, 0, 0, 0, 0, 23420), -- A Family Divided
(24566, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sowing a Solution
(24574, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- To Harvest Chaos
(24513, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Eye for an Eye
(24512, 25, 1, 274, 0, 0, 0, 0, 0, 23420), -- Warriors' Redemption
(25284, 1, 273, 0, 0, 0, 0, 0, 0, 23420), -- Feeding the Fear
(24519, 6, 1, 0, 0, 0, 0, 0, 0, 23420), -- Stalling the Survey
(24518, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Low Road
(24514, 1, 273, 0, 0, 0, 0, 0, 0, 23420), -- Raptor Scraps
(24515, 1, 6, 0, 0, 0, 0, 0, 0, 23420); -- Signals in the Sky


UPDATE `creature_model_info` SET `BoundingRadius`=1.992921, `CombatReach`=2.475, `VerifiedBuild`=23420 WHERE `DisplayID`=74323;
UPDATE `creature_model_info` SET `BoundingRadius`=7.285681, `CombatReach`=2.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74354;
UPDATE `creature_model_info` SET `CombatReach`=1.630435 WHERE `DisplayID`=71539;
UPDATE `creature_model_info` SET `CombatReach`=1.704545 WHERE `DisplayID`=68964;
UPDATE `creature_model_info` SET `BoundingRadius`=1.031494 WHERE `DisplayID`=68393;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4279, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=14422;
UPDATE `creature_model_info` SET `BoundingRadius`=1.304315, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=1083;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3366, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=14421;
UPDATE `creature_model_info` SET `BoundingRadius`=1.252845, `VerifiedBuild`=23420 WHERE `DisplayID`=8471;
UPDATE `creature_model_info` SET `BoundingRadius`=0.52515, `CombatReach`=2.025, `VerifiedBuild`=23420 WHERE `DisplayID`=14384;
UPDATE `creature_model_info` SET `BoundingRadius`=0.312, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=42886;
UPDATE `creature_model_info` SET `BoundingRadius`=0.312, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=42887;
UPDATE `creature_model_info` SET `BoundingRadius`=0.312, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=42888;
UPDATE `creature_model_info` SET `BoundingRadius`=0.312, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=42885;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5745, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=40633;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=15137;
UPDATE `creature_model_info` SET `BoundingRadius`=0.21, `CombatReach`=0.9, `VerifiedBuild`=23420 WHERE `DisplayID`=1954;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44462;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44461;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42495;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=0.9, `VerifiedBuild`=23420 WHERE `DisplayID`=13096;
UPDATE `creature_model_info` SET `BoundingRadius`=2.635779, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=14416;
UPDATE `creature_model_info` SET `BoundingRadius`=1.005061, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=65891;
UPDATE `creature_model_info` SET `BoundingRadius`=2.108624, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=11335;
UPDATE `creature_model_info` SET `BoundingRadius`=16, `CombatReach`=16, `VerifiedBuild`=23420 WHERE `DisplayID`=17310;
UPDATE `creature_model_info` SET `BoundingRadius`=0.386562, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=62616;
UPDATE `creature_model_info` SET `BoundingRadius`=1.212458, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=2014;
UPDATE `creature_model_info` SET `BoundingRadius`=1.370605, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=7649;
UPDATE `creature_model_info` SET `BoundingRadius`=1.581468, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=2020;
UPDATE `creature_model_info` SET `BoundingRadius`=1.581468, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=10032;
UPDATE `creature_model_info` SET `BoundingRadius`=1.845046, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=11340;
UPDATE `creature_model_info` SET `BoundingRadius`=0.35, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=19736;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=60030;
UPDATE `creature_model_info` SET `BoundingRadius`=24, `CombatReach`=24, `VerifiedBuild`=23420 WHERE `DisplayID`=28474;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74250;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74266;
UPDATE `creature_model_info` SET `BoundingRadius`=0.389, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73476;
UPDATE `creature_model_info` SET `BoundingRadius`=0.28, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=43485;
UPDATE `creature_model_info` SET `BoundingRadius`=1.0601, `CombatReach`=1.1, `VerifiedBuild`=23420 WHERE `DisplayID`=1267;
UPDATE `creature_model_info` SET `BoundingRadius`=1.263899, `VerifiedBuild`=23420 WHERE `DisplayID`=150;
UPDATE `creature_model_info` SET `BoundingRadius`=1.263899, `VerifiedBuild`=23420 WHERE `DisplayID`=2725;
UPDATE `creature_model_info` SET `BoundingRadius`=3.791697, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=12369;
UPDATE `creature_model_info` SET `BoundingRadius`=1.370605, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=2021;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29294;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29295;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29296;
UPDATE `creature_model_info` SET `BoundingRadius`=1.643068, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=12350;
UPDATE `creature_model_info` SET `BoundingRadius`=1.581468, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=11338;
UPDATE `creature_model_info` SET `BoundingRadius`=1.370605, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=11345;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8892583, `VerifiedBuild`=23420 WHERE `DisplayID`=2452;
UPDATE `creature_model_info` SET `BoundingRadius`=1.176, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=4716;
UPDATE `creature_model_info` SET `BoundingRadius`=5.177787, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=52595;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52778;
UPDATE `creature_model_info` SET `BoundingRadius`=1.764, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=33857;
UPDATE `creature_model_info` SET `BoundingRadius`=5.88, `CombatReach`=5, `VerifiedBuild`=23420 WHERE `DisplayID`=2450;
UPDATE `creature_model_info` SET `BoundingRadius`=2.761487, `CombatReach`=1.6, `VerifiedBuild`=23420 WHERE `DisplayID`=52731;
UPDATE `creature_model_info` SET `BoundingRadius`=1.811348, `CombatReach`=1.049492, `VerifiedBuild`=23420 WHERE `DisplayID`=52732;
UPDATE `creature_model_info` SET `BoundingRadius`=1.6, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52785;
UPDATE `creature_model_info` SET `BoundingRadius`=0.665, `CombatReach`=2.85, `VerifiedBuild`=23420 WHERE `DisplayID`=31042;
UPDATE `creature_model_info` SET `BoundingRadius`=4.035, `CombatReach`=4.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52661;
UPDATE `creature_model_info` SET `BoundingRadius`=0.595, `CombatReach`=2.55, `VerifiedBuild`=23420 WHERE `DisplayID`=52383;
UPDATE `creature_model_info` SET `BoundingRadius`=2.628956, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=52513;
UPDATE `creature_model_info` SET `BoundingRadius`=0.595, `CombatReach`=2.55, `VerifiedBuild`=23420 WHERE `DisplayID`=4756;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5568, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=4645;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=52515;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=23420 WHERE `DisplayID`=52341;
UPDATE `creature_model_info` SET `BoundingRadius`=1.176, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=52423;
UPDATE `creature_model_info` SET `BoundingRadius`=0.56, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=6102;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5916, `CombatReach`=2.55, `VerifiedBuild`=23420 WHERE `DisplayID`=52047;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3132, `CombatReach`=1.35, `VerifiedBuild`=23420 WHERE `DisplayID`=52699;
UPDATE `creature_model_info` SET `BoundingRadius`=1.6095, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=52489;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2625, `CombatReach`=1.125, `VerifiedBuild`=23420 WHERE `DisplayID`=52717;
UPDATE `creature_model_info` SET `BoundingRadius`=0.56, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=52183;
UPDATE `creature_model_info` SET `BoundingRadius`=1.75, `CombatReach`=5.25, `VerifiedBuild`=23420 WHERE `DisplayID`=52861;
UPDATE `creature_model_info` SET `BoundingRadius`=1.176, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=52459;
UPDATE `creature_model_info` SET `BoundingRadius`=0.155, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41279;
UPDATE `creature_model_info` SET `BoundingRadius`=0.56, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=52184;
UPDATE `creature_model_info` SET `BoundingRadius`=1.176, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=52230;
UPDATE `creature_model_info` SET `BoundingRadius`=0.455, `CombatReach`=1.95, `VerifiedBuild`=23420 WHERE `DisplayID`=6092;
UPDATE `creature_model_info` SET `BoundingRadius`=0.35, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52718;
UPDATE `creature_model_info` SET `BoundingRadius`=0.522, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=52232;
UPDATE `creature_model_info` SET `BoundingRadius`=1.073, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45338;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2436, `CombatReach`=1.05, `VerifiedBuild`=23420 WHERE `DisplayID`=52724;
UPDATE `creature_model_info` SET `BoundingRadius`=0.385, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=52719;
UPDATE `creature_model_info` SET `BoundingRadius`=1.275, `CombatReach`=1.0625, `VerifiedBuild`=23420 WHERE `DisplayID`=643;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5056454, `VerifiedBuild`=23420 WHERE `DisplayID`=2723;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3064, `CombatReach`=1.2, `VerifiedBuild`=23420 WHERE `DisplayID`=52764;
UPDATE `creature_model_info` SET `BoundingRadius`=0.152778, `CombatReach`=2.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52768;
UPDATE `creature_model_info` SET `BoundingRadius`=4.185692, `CombatReach`=14.4, `VerifiedBuild`=23420 WHERE `DisplayID`=52493;
UPDATE `creature_model_info` SET `BoundingRadius`=1.16964, `CombatReach`=4.860001, `VerifiedBuild`=23420 WHERE `DisplayID`=51929;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=2.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52303;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=52283;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5362, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51933;
UPDATE `creature_model_info` SET `BoundingRadius`=3, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=52074;
UPDATE `creature_model_info` SET `BoundingRadius`=6.25, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=53029;
UPDATE `creature_model_info` SET `BoundingRadius`=0.124, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=52439;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4284, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51931;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5362, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=52935;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5362, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51922;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4284, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51890;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=52057;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4284, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51892;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3304, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51887;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5362, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51919;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4284, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51920;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=39403;
UPDATE `creature_model_info` SET `BoundingRadius`=0.42228, `CombatReach`=2.07, `VerifiedBuild`=23420 WHERE `DisplayID`=52458;
UPDATE `creature_model_info` SET `BoundingRadius`=1.4, `CombatReach`=1.4, `VerifiedBuild`=23420 WHERE `DisplayID`=52276;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2912, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51930;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=52279;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5362, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51934;
UPDATE `creature_model_info` SET `BoundingRadius`=0.75, `CombatReach`=0.9375, `VerifiedBuild`=23420 WHERE `DisplayID`=52280;
UPDATE `creature_model_info` SET `BoundingRadius`=0.49266, `CombatReach`=2.415, `VerifiedBuild`=23420 WHERE `DisplayID`=2883;
UPDATE `creature_model_info` SET `BoundingRadius`=0.744, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=52084;
UPDATE `creature_model_info` SET `BoundingRadius`=0.0775, `CombatReach`=0.75, `VerifiedBuild`=23420 WHERE `DisplayID`=52751;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52399;
UPDATE `creature_model_info` SET `BoundingRadius`=0.434, `CombatReach`=4.2, `VerifiedBuild`=23420 WHERE `DisplayID`=52173;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52432;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52433;
UPDATE `creature_model_info` SET `BoundingRadius`=0.72975, `CombatReach`=2.625, `VerifiedBuild`=23420 WHERE `DisplayID`=52278;
UPDATE `creature_model_info` SET `BoundingRadius`=2.4, `CombatReach`=3.6, `VerifiedBuild`=23420 WHERE `DisplayID`=52010;
UPDATE `creature_model_info` SET `BoundingRadius`=0.766, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=52034;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2832, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=51886;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52402;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3672, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=51884;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4858, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51885;
UPDATE `creature_model_info` SET `BoundingRadius`=0.417, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52986;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5208, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=51928;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=52401;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5838, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=52277;
UPDATE `creature_model_info` SET `BoundingRadius`=1.3, `CombatReach`=1.3, `VerifiedBuild`=23420 WHERE `DisplayID`=4979;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=0.375 WHERE `DisplayID`=32789;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=0.375, `VerifiedBuild`=23420 WHERE `DisplayID`=32790;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=0.375, `VerifiedBuild`=23420 WHERE `DisplayID`=32791;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=0.375, `VerifiedBuild`=23420 WHERE `DisplayID`=45880;
UPDATE `creature_model_info` SET `BoundingRadius`=0.85, `CombatReach`=0.85, `VerifiedBuild`=23420 WHERE `DisplayID`=4983;
UPDATE `creature_model_info` SET `CombatReach`=10, `VerifiedBuild`=23420 WHERE `DisplayID`=1772;
UPDATE `creature_model_info` SET `BoundingRadius`=1.25, `CombatReach`=1.25, `VerifiedBuild`=23420 WHERE `DisplayID`=28514;
UPDATE `creature_model_info` SET `BoundingRadius`=0.45, `VerifiedBuild`=23420 WHERE `DisplayID`=28510;
UPDATE `creature_model_info` SET `BoundingRadius`=0.375, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=28511;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `VerifiedBuild`=23420 WHERE `DisplayID`=28512;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9999748, `CombatReach`=2.3, `VerifiedBuild`=23420 WHERE `DisplayID`=820;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=37422;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5056454, `VerifiedBuild`=23420 WHERE `DisplayID`=2721;
UPDATE `creature_model_info` SET `BoundingRadius`=1.370605, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=11331;
UPDATE `creature_model_info` SET `BoundingRadius`=1.054312, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=2010;
UPDATE `creature_model_info` SET `BoundingRadius`=1.054312, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=2011;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3328, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=2043;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4896, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=41220;
UPDATE `creature_model_info` SET `BoundingRadius`=0.195, `CombatReach`=0.5, `VerifiedBuild`=23420 WHERE `DisplayID`=43664;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4896, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=41154;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1, `CombatReach`=2.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45860;
UPDATE `creature_model_info` SET `BoundingRadius`=3.75, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40964;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40776;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=43804;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40650;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40672;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40790;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40765;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3825, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=40946;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40948;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40947;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4284, `CombatReach`=2.1, `VerifiedBuild`=23420 WHERE `DisplayID`=40597;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40651;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40673;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40724;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40779;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40791;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40757;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3978, `CombatReach`=1.95, `VerifiedBuild`=23420 WHERE `DisplayID`=40293;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40780;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40725;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40756;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40764;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=43800;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4437, `CombatReach`=2.175, `VerifiedBuild`=23420 WHERE `DisplayID`=43799;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3825, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=43806;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3672, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=43805;
UPDATE `creature_model_info` SET `BoundingRadius`=0.459, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=42264;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=0.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41324;
UPDATE `creature_model_info` SET `BoundingRadius`=0.459, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=40631;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3672, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=40629;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=43808;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3978, `CombatReach`=1.95, `VerifiedBuild`=23420 WHERE `DisplayID`=43809;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=40300;
UPDATE `creature_model_info` SET `BoundingRadius`=0.66, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=41685;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2496, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=43650;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=43801;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=43803;
UPDATE `creature_model_info` SET `BoundingRadius`=0.975, `CombatReach`=0.975, `VerifiedBuild`=23420 WHERE `DisplayID`=43921;
UPDATE `creature_model_info` SET `BoundingRadius`=3.3, `CombatReach`=3.3, `VerifiedBuild`=23420 WHERE `DisplayID`=36560;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=71920;
UPDATE `creature_model_info` SET `BoundingRadius`=1.95, `CombatReach`=1.625, `VerifiedBuild`=23420 WHERE `DisplayID`=776;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=6, `VerifiedBuild`=23420 WHERE `DisplayID`=36555;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1666, `CombatReach`=0.525, `VerifiedBuild`=23420 WHERE `DisplayID`=46936;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48636;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7933017, `CombatReach`=0.6745763, `VerifiedBuild`=23420 WHERE `DisplayID`=32323;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8012746, `CombatReach`=0.681356, `VerifiedBuild`=23420 WHERE `DisplayID`=607;
UPDATE `creature_model_info` SET `BoundingRadius`=1.25, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48672;
UPDATE `creature_model_info` SET `BoundingRadius`=0.295, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48695;
UPDATE `creature_model_info` SET `BoundingRadius`=0.295, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48622;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48621;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48623;
UPDATE `creature_model_info` SET `BoundingRadius`=0.295, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48620;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48545;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48544;
UPDATE `creature_model_info` SET `BoundingRadius`=8, `CombatReach`=6, `VerifiedBuild`=23420 WHERE `DisplayID`=48865;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48591;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48546;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48592;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48547;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48590;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48589;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=7050;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29402;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29431;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29425;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29434;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29432;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=4968;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=4969;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=4970;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=4971;
UPDATE `creature_model_info` SET `BoundingRadius`=1.125, `CombatReach`=0.9375, `VerifiedBuild`=23420 WHERE `DisplayID`=666;
UPDATE `creature_model_info` SET `BoundingRadius`=1.530433, `VerifiedBuild`=23420 WHERE `DisplayID`=2835;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48700;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48693;
UPDATE `creature_model_info` SET `BoundingRadius`=0.295, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48696;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48699;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=0.45, `VerifiedBuild`=23420 WHERE `DisplayID`=48671;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48698;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=0.45, `VerifiedBuild`=23420 WHERE `DisplayID`=48670;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48697;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=38187;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74035;
UPDATE `creature_model_info` SET `BoundingRadius`=0.03, `CombatReach`=0.375, `VerifiedBuild`=23420 WHERE `DisplayID`=74037;
UPDATE `creature_model_info` SET `BoundingRadius`=0.03, `CombatReach`=0.375, `VerifiedBuild`=23420 WHERE `DisplayID`=74036;
UPDATE `creature_model_info` SET `BoundingRadius`=0.03, `CombatReach`=0.375, `VerifiedBuild`=23420 WHERE `DisplayID`=74038;
UPDATE `creature_model_info` SET `BoundingRadius`=0.35, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74039;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=9580;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=8653;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=9575;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5508475, `CombatReach`=0.5508475, `VerifiedBuild`=23420 WHERE `DisplayID`=1742;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1932712, `CombatReach`=0.4417627, `VerifiedBuild`=23420 WHERE `DisplayID`=6368;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42213;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42232;
UPDATE `creature_model_info` SET `BoundingRadius`=2.75, `CombatReach`=5.775, `VerifiedBuild`=23420 WHERE `DisplayID`=42100;
UPDATE `creature_model_info` SET `BoundingRadius`=6, `CombatReach`=15, `VerifiedBuild`=23420 WHERE `DisplayID`=42245;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42198;
UPDATE `creature_model_info` SET `BoundingRadius`=0.295, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42215;
UPDATE `creature_model_info` SET `BoundingRadius`=0.295, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42210;
UPDATE `creature_model_info` SET `BoundingRadius`=0.651, `CombatReach`=2.625, `VerifiedBuild`=23420 WHERE `DisplayID`=42101;
UPDATE `creature_model_info` SET `BoundingRadius`=0.918, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=38759;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42212;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42214;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42208;
UPDATE `creature_model_info` SET `BoundingRadius`=6, `CombatReach`=24, `VerifiedBuild`=23420 WHERE `DisplayID`=42247;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42211;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8100001, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42237;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42196;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2295, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42187;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42209;
UPDATE `creature_model_info` SET `BoundingRadius`=0.459, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42188;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42194;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42219;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=42217;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48632;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48631;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48643;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48641;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48640;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48629;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48607;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=31464;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48630;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48642;
UPDATE `creature_model_info` SET `BoundingRadius`=0.795, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=1043;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46780;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=23420 WHERE `DisplayID`=47148;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47149;
UPDATE `creature_model_info` SET `BoundingRadius`=1.016542, `CombatReach`=0.8644068, `VerifiedBuild`=23420 WHERE `DisplayID`=6807;
UPDATE `creature_model_info` SET `BoundingRadius`=1.073, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=1337;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44805;
UPDATE `creature_model_info` SET `BoundingRadius`=0.062, `CombatReach`=0.2, `VerifiedBuild`=23420 WHERE `DisplayID`=45563;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5885594, `CombatReach`=1.059322, `VerifiedBuild`=23420 WHERE `DisplayID`=1035;
UPDATE `creature_model_info` SET `BoundingRadius`=2.002723, `VerifiedBuild`=23420 WHERE `DisplayID`=31019;
UPDATE `creature_model_info` SET `BoundingRadius`=0.85, `CombatReach`=0.85, `VerifiedBuild`=23420 WHERE `DisplayID`=31017;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `VerifiedBuild`=23420 WHERE `DisplayID`=4036;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1925, `CombatReach`=0.55, `VerifiedBuild`=23420 WHERE `DisplayID`=30847;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2083332, `CombatReach`=0.9, `VerifiedBuild`=23420 WHERE `DisplayID`=48135;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=61875;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42927;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42928;
UPDATE `creature_model_info` SET `BoundingRadius`=0.59015, `CombatReach`=0.825, `VerifiedBuild`=23420 WHERE `DisplayID`=1960;
UPDATE `creature_model_info` SET `BoundingRadius`=0.27, `VerifiedBuild`=23420 WHERE `DisplayID`=6764;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2325, `VerifiedBuild`=23420 WHERE `DisplayID`=30963;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7237288, `CombatReach`=0.9046609, `VerifiedBuild`=23420 WHERE `DisplayID`=31269;
UPDATE `creature_model_info` SET `BoundingRadius`=0.85, `CombatReach`=1.0625, `VerifiedBuild`=23420 WHERE `DisplayID`=1938;
UPDATE `creature_model_info` SET `BoundingRadius`=0.882, `CombatReach`=0.75, `VerifiedBuild`=23420 WHERE `DisplayID`=381;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6572779, `VerifiedBuild`=23420 WHERE `DisplayID`=1185;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=42925;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7056, `CombatReach`=0.6, `VerifiedBuild`=23420 WHERE `DisplayID`=503;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=71061;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6639912, `VerifiedBuild`=23420 WHERE `DisplayID`=54854;

DELETE FROM `npc_vendor` WHERE (`entry`=15131 AND `item`=37460 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43625 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43625 AND `item`=18567 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43625 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43625 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43625 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43625 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43630 AND `item`=37460 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43624 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2030 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2208 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2026 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2028 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2025 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2029 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2024 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=2027 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43615 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43615 AND `item`=18567 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43615 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43615 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43615 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43615 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=34303 AND `item`=4778 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43606 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43617 AND `item`=37460 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42028 AND `item`=57472 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42028 AND `item`=57473 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42028 AND `item`=57474 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42028 AND `item`=131883 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42028 AND `item`=131882 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42028 AND `item`=57475 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42028 AND `item`=57476 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=90146 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=18567 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41890 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41892 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4565 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4499 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4497 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41891 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41674 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=90146 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=18567 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41675 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4565 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4499 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4497 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=41676 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43633 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43646 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43646 AND `item`=4565 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=239 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=1850 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=238 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=237 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=1849 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=236 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=10290 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=6261 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=38426 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43568 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3413 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3413 AND `item`=90146 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3413 AND `item`=68660 AND `ExtendedCost`=6003 AND `type`=1) OR (`entry`=43645 AND `item`=854 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2207 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=1197 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=852 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=1196 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=853 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=1198 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=851 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2493 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2490 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2488 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2495 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2494 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2491 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2492 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2489 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2132 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2139 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2480 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2130 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2479 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2134 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=1194 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43645 AND `item`=2131 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=1202 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=17187 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=850 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=1846 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=849 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=848 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=1845 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=847 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=2376 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=17185 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=2403 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=2402 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=2401 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=2400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=2399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43955 AND `item`=2398 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=14754 AND `item`=30498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=14754 AND `item`=22651 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=14754 AND `item`=22673 AND `ExtendedCost`=5996 AND `type`=1) OR (`entry`=14754 AND `item`=22676 AND `ExtendedCost`=5996 AND `type`=1) OR (`entry`=14754 AND `item`=22740 AND `ExtendedCost`=5996 AND `type`=1) OR (`entry`=14754 AND `item`=22741 AND `ExtendedCost`=5996 AND `type`=1) OR (`entry`=14754 AND `item`=22747 AND `ExtendedCost`=5996 AND `type`=1) OR (`entry`=14754 AND `item`=19582 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19583 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19584 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19587 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19589 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19590 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19595 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19596 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19597 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19578 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19580 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19581 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19505 AND `ExtendedCost`=5992 AND `type`=1) OR (`entry`=14754 AND `item`=19542 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19543 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19544 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19545 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=20441 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19558 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19559 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19560 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19561 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=20437 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19550 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19551 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19552 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19553 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=20430 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19566 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19567 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19568 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=19569 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=20425 AND `ExtendedCost`=5993 AND `type`=1) OR (`entry`=14754 AND `item`=17351 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=14754 AND `item`=17348 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=14754 AND `item`=19534 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19535 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19536 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19537 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=20442 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19510 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19511 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19512 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19513 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=20429 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19526 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19527 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19528 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19529 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=20427 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19518 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19519 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19520 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=19521 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=20426 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=17352 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=14754 AND `item`=17349 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=14754 AND `item`=21565 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=14754 AND `item`=21566 AND `ExtendedCost`=5995 AND `type`=1) OR (`entry`=43953 AND `item`=4499 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4497 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=5048 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43953 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=2376 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=17185 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=2403 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=2402 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=2401 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=2400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=2399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=2398 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=1202 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=17187 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=850 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=1846 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=849 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=848 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=1845 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43957 AND `item`=847 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=8953 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4602 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4539 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4538 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4537 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4536 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=8957 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=21552 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4594 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4593 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=4592 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43945 AND `item`=787 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3495 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3495 AND `item`=90146 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3498 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3498 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3572 AND `item`=136377 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3499 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3499 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3499 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3490 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3490 AND `item`=4565 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3481 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3481 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3482 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3482 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3482 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=844 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=1844 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=843 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=845 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=1843 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=846 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=2375 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=2374 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=2373 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=2372 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=2371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43951 AND `item`=2370 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43956 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43956 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43956 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43956 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43956 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43956 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2375 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2374 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2373 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2372 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2370 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2369 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=3607 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2367 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2366 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=3606 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43949 AND `item`=2364 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3351 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=49737 AND `item`=68689 AND `ExtendedCost`=3934 AND `type`=1) OR (`entry`=3367 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3367 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3367 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3364 AND `item`=6270 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=52809 AND `item`=137663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=49889 AND `item`=61986 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=49889 AND `item`=61985 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=49889 AND `item`=61984 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=49889 AND `item`=61983 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=49889 AND `item`=61982 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=115797 AND `item`=142313 AND `ExtendedCost`=6126 AND `type`=1) OR (`entry`=115797 AND `item`=142311 AND `ExtendedCost`=6126 AND `type`=1) OR (`entry`=115797 AND `item`=142294 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142293 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142292 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142291 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142290 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142289 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142288 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142314 AND `ExtendedCost`=6128 AND `type`=1) OR (`entry`=115797 AND `item`=142317 AND `ExtendedCost`=6129 AND `type`=1) OR (`entry`=115797 AND `item`=142319 AND `ExtendedCost`=6127 AND `type`=1) OR (`entry`=115797 AND `item`=142318 AND `ExtendedCost`=6127 AND `type`=1) OR (`entry`=68794 AND `item`=93158 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68794 AND `item`=93045 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68794 AND `item`=93044 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68794 AND `item`=93043 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68364 AND `item`=142403 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68364 AND `item`=122396 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68364 AND `item`=144392 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68364 AND `item`=144394 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68364 AND `item`=93025 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68364 AND `item`=142326 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68364 AND `item`=142325 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3333 AND `item`=136377 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=69333 AND `item`=92070 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=69333 AND `item`=83080 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42709 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42709 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46642 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46642 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46642 AND `item`=82449 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46642 AND `item`=82448 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46642 AND `item`=81920 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46642 AND `item`=81919 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3335 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3334 AND `item`=4565 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45552 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45552 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45546 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45546 AND `item`=90146 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45546 AND `item`=68660 AND `ExtendedCost`=6003 AND `type`=1) OR (`entry`=45565 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45565 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45566 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45566 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45563 AND `item`=81918 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45563 AND `item`=81917 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45563 AND `item`=82451 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45563 AND `item`=82450 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45563 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45563 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=30723 AND `item`=79740 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=30723 AND `item`=1515 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=30723 AND `item`=113111 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=30723 AND `item`=79255 AND `ExtendedCost`=2583 AND `type`=1) OR (`entry`=30723 AND `item`=79254 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=30723 AND `item`=61978 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=44975 AND `item`=136377 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45008 AND `item`=81920 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45008 AND `item`=81919 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45008 AND `item`=82449 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45008 AND `item`=82448 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45008 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45008 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45093 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45093 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45093 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45086 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45086 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45086 AND `item`=82449 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45086 AND `item`=82448 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45086 AND `item`=81920 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=45086 AND `item`=81919 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3405 AND `item`=4565 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3405 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44780 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44780 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44780 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44785 AND `item`=82451 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44785 AND `item`=82450 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44785 AND `item`=81921 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44785 AND `item`=81922 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44785 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44785 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3322 AND `item`=11303 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=52032 AND `item`=113111 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=52032 AND `item`=79255 AND `ExtendedCost`=2583 AND `type`=1) OR (`entry`=52032 AND `item`=79254 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=52032 AND `item`=61978 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=46718 AND `item`=79740 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46718 AND `item`=1515 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=46718 AND `item`=113111 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=46718 AND `item`=79255 AND `ExtendedCost`=2583 AND `type`=1) OR (`entry`=46718 AND `item`=79254 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=46718 AND `item`=61978 AND `ExtendedCost`=2582 AND `type`=1) OR (`entry`=3368 AND `item`=82451 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3368 AND `item`=82450 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3368 AND `item`=81918 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3368 AND `item`=81917 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=5611 AND `item`=81918 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=5611 AND `item`=81917 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=5611 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=5611 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=6929 AND `item`=81923 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=6929 AND `item`=81924 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=6929 AND `item`=82451 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=6929 AND `item`=82450 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=6929 AND `item`=81918 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=6929 AND `item`=81917 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3342 AND `item`=81920 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3342 AND `item`=81919 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3312 AND `item`=81918 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3312 AND `item`=81917 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3323 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=5188 AND `item`=15199 AND `ExtendedCost`=5962 AND `type`=1) OR (`entry`=5188 AND `item`=15197 AND `ExtendedCost`=5962 AND `type`=1) OR (`entry`=5188 AND `item`=19031 AND `ExtendedCost`=5992 AND `type`=1) OR (`entry`=5188 AND `item`=19505 AND `ExtendedCost`=5992 AND `type`=1) OR (`entry`=5817 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=5817 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3168 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3168 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3168 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3187 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3187 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3187 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=3186 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=39031 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=5942 AND `item`=136377 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44276 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=4497 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44300 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2531 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2523 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2530 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2522 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2521 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2528 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2520 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2534 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44287 AND `item`=2526 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=4232 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=239 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=1850 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=238 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=237 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=1849 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=236 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=4497 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44296 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44349 AND `item`=37460 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=8953 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=4602 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=4539 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=4538 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=4537 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44270 AND `item`=4536 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44285 AND `item`=203 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44285 AND `item`=3598 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44285 AND `item`=202 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44285 AND `item`=201 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44285 AND `item`=3597 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44285 AND `item`=200 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=4497 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44297 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=44286 AND `item`=2319 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=66685 AND `item`=76734 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=79101 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=72120 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=72103 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=72094 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=72093 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=72092 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=74247 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=74250 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=74249 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=72238 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=89610 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=72988 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=79255 AND `ExtendedCost`=3901 AND `type`=1) OR (`entry`=66685 AND `item`=402 AND `ExtendedCost`=3901 AND `type`=2) OR (`entry`=112407 AND `item`=140965 AND `ExtendedCost`=6102 AND `type`=1) OR (`entry`=112407 AND `item`=140936 AND `ExtendedCost`=6101 AND `type`=1) OR (`entry`=112407 AND `item`=140964 AND `ExtendedCost`=5952 AND `type`=1) OR (`entry`=112407 AND `item`=139720 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=139721 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=139716 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=139715 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=139719 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=139717 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=139722 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=139718 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=112407 AND `item`=140560 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=111736 AND `item`=144434 AND `ExtendedCost`=6153 AND `type`=1) OR (`entry`=64126 AND `item`=21219 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=21099 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=74845 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=74832 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=74659 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=85585 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=74851 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=74852 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=85584 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=74660 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=85583 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=64126 AND `item`=74854 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(15131, 1, 37460, 0, 0, 1, 0, 0, 23420), -- Rope Pet Leash
(43625, 6, 3857, 0, 0, 1, 0, 0, 23420), -- Coal
(43625, 5, 18567, 0, 0, 1, 0, 0, 23420), -- Elemental Flux
(43625, 4, 3466, 0, 0, 1, 0, 0, 23420), -- Strong Flux
(43625, 3, 2880, 0, 0, 1, 0, 0, 23420), -- Weak Flux
(43625, 2, 5956, 0, 0, 1, 0, 0, 23420), -- Blacksmith Hammer
(43625, 1, 2901, 0, 0, 1, 0, 0, 23420), -- Mining Pick
(43630, 1, 37460, 0, 0, 1, 0, 0, 23420), -- Rope Pet Leash
(43624, 12, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(43624, 11, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(43624, 10, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(43624, 9, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(43624, 8, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(43624, 7, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(43624, 6, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(43624, 5, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(43624, 4, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(43624, 3, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(43624, 2, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(43624, 1, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(34303, 8, 2030, 0, 0, 1, 0, 1, 23420), -- Gnarled Staff
(34303, 7, 2208, 0, 0, 1, 0, 1, 23420), -- Poniard
(34303, 6, 2026, 0, 0, 1, 0, 1, 23420), -- Rock Hammer
(34303, 5, 2028, 0, 0, 1, 0, 1, 23420), -- Hammer
(34303, 4, 2025, 0, 0, 1, 0, 1, 23420), -- Bearded Axe
(34303, 3, 2029, 0, 0, 1, 0, 1, 23420), -- Cleaver
(34303, 2, 2024, 0, 0, 1, 0, 1, 23420), -- Espadon
(34303, 1, 2027, 0, 0, 1, 0, 1, 23420), -- Scimitar
(43615, 6, 3857, 0, 0, 1, 0, 0, 23420), -- Coal
(43615, 5, 18567, 0, 0, 1, 0, 0, 23420), -- Elemental Flux
(43615, 4, 3466, 0, 0, 1, 0, 0, 23420), -- Strong Flux
(43615, 3, 2880, 0, 0, 1, 0, 0, 23420), -- Weak Flux
(43615, 2, 5956, 0, 0, 1, 0, 0, 23420), -- Blacksmith Hammer
(43615, 1, 2901, 0, 0, 1, 0, 0, 23420), -- Mining Pick
(34303, 158, 4778, 2, 0, 1, 0, 0, 23420), -- Heavy Spiked Mace
(43606, 12, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(43606, 11, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(43606, 10, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(43606, 9, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(43606, 8, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(43606, 7, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(43606, 6, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(43606, 5, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(43606, 4, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(43606, 3, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(43606, 2, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(43606, 1, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(43617, 1, 37460, 0, 0, 1, 0, 0, 23420), -- Rope Pet Leash
(42028, 7, 57472, 0, 0, 1, 0, 0, 23420), -- Krom'gar Sergeant's Linen Footwraps
(42028, 6, 57473, 0, 0, 1, 0, 0, 23420), -- Krom'gar Sergeant's Hide Boots
(42028, 5, 57474, 0, 0, 1, 0, 0, 23420), -- Krom'gar Sergeant's Leather Boots
(42028, 4, 131883, 0, 0, 1, 0, 0, 23420), -- Krom'gar Sergeant's Chain Boots
(42028, 3, 131882, 0, 0, 1, 0, 0, 23420), -- Krom'gar Sergeant's Mesh Boots
(42028, 2, 57475, 0, 0, 1, 0, 0, 23420), -- Krom'gar Sergeant's Plate Greaves
(42028, 1, 57476, 0, 0, 1, 0, 0, 23420), -- Krom'gar Sergeant's Armored Greaves
(41890, 1040, 39354, 0, 0, 1, 0, 0, 23420), -- Light Parchment
(41890, 1039, 4364, 2, 0, 1, 0, 0, 23420), -- Coarse Blasting Powder
(41890, 1038, 4357, 3, 0, 1, 0, 0, 23420), -- Rough Blasting Powder
(41890, 1037, 10647, 0, 0, 1, 0, 0, 23420), -- Engineer's Ink
(41890, 1036, 39684, 0, 0, 1, 0, 0, 23420), -- Hair Trigger
(41890, 1035, 40533, 0, 0, 1, 0, 0, 23420), -- Walnut Stock
(41890, 1034, 4400, 0, 0, 1, 0, 0, 23420), -- Heavy Stock
(41890, 1033, 4399, 0, 0, 1, 0, 0, 23420), -- Wooden Stock
(41890, 1032, 90146, 0, 0, 1, 0, 0, 23420), -- Tinker's Kit
(41890, 6, 3857, 0, 0, 1, 0, 0, 23420), -- Coal
(41890, 5, 18567, 0, 0, 1, 0, 0, 23420), -- Elemental Flux
(41890, 4, 3466, 0, 0, 1, 0, 0, 23420), -- Strong Flux
(41890, 3, 2880, 0, 0, 1, 0, 0, 23420), -- Weak Flux
(41890, 2, 5956, 0, 0, 1, 0, 0, 23420), -- Blacksmith Hammer
(41890, 1, 2901, 0, 0, 1, 0, 0, 23420), -- Mining Pick
(41892, 12, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(41892, 11, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(41892, 10, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(41892, 9, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(41892, 8, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(41892, 7, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(41892, 6, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(41892, 5, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(41892, 4, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(41892, 3, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(41892, 2, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(41892, 1, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(41891, 40, 4565, 0, 0, 1, 0, 0, 23420), -- Simple Dagger
(41891, 39, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(41891, 38, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(41891, 37, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(41891, 36, 6532, 0, 0, 1, 0, 0, 23420), -- Bright Baubles
(41891, 35, 6530, 0, 0, 1, 0, 0, 23420), -- Nightcrawlers
(41891, 34, 4400, 0, 0, 1, 0, 0, 23420), -- Heavy Stock
(41891, 33, 4399, 0, 0, 1, 0, 0, 23420), -- Wooden Stock
(41891, 32, 4289, 0, 0, 1, 0, 0, 23420), -- Salt
(41891, 31, 3371, 0, 0, 1, 0, 0, 23420), -- Crystal Vial
(41891, 30, 4340, 0, 0, 1, 0, 0, 23420), -- Gray Dye
(41891, 29, 4342, 0, 0, 1, 0, 0, 23420), -- Purple Dye
(41891, 28, 4341, 0, 0, 1, 0, 0, 23420), -- Yellow Dye
(41891, 27, 2325, 0, 0, 1, 0, 0, 23420), -- Black Dye
(41891, 26, 2604, 0, 0, 1, 0, 0, 23420), -- Red Dye
(41891, 25, 2324, 0, 0, 1, 0, 0, 23420), -- Bleach
(41891, 24, 3857, 0, 0, 1, 0, 0, 23420), -- Coal
(41891, 23, 3466, 0, 0, 1, 0, 0, 23420), -- Strong Flux
(41891, 22, 2880, 0, 0, 1, 0, 0, 23420), -- Weak Flux
(41891, 21, 2678, 0, 0, 1, 0, 0, 23420), -- Mild Spices
(41891, 20, 14341, 0, 0, 1, 0, 0, 23420), -- Rune Thread
(41891, 19, 8343, 0, 0, 1, 0, 0, 23420), -- Heavy Silken Thread
(41891, 18, 4291, 0, 0, 1, 0, 0, 23420), -- Silken Thread
(41891, 17, 2321, 0, 0, 1, 0, 0, 23420), -- Fine Thread
(41891, 16, 2320, 0, 0, 1, 0, 0, 23420), -- Coarse Thread
(41891, 15, 6217, 0, 0, 1, 0, 0, 23420), -- Copper Rod
(41891, 14, 5956, 0, 0, 1, 0, 0, 23420), -- Blacksmith Hammer
(41891, 13, 6256, 0, 0, 1, 0, 0, 23420), -- Fishing Pole
(41891, 12, 2901, 0, 0, 1, 0, 0, 23420), -- Mining Pick
(41891, 11, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(41891, 10, 7005, 0, 0, 1, 0, 0, 23420), -- Skinning Knife
(41891, 9, 4470, 0, 0, 1, 0, 0, 23420), -- Simple Wood
(41891, 8, 4499, 0, 0, 1, 0, 0, 23420), -- Huge Brown Sack
(41891, 7, 4497, 0, 0, 1, 0, 0, 23420), -- Heavy Brown Bag
(41891, 6, 4601, 0, 0, 1, 0, 0, 23420), -- Soft Banana Bread
(41891, 5, 4542, 0, 0, 1, 0, 0, 23420), -- Moist Cornbread
(41891, 4, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(41891, 3, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(41891, 2, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(41891, 1, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(41674, 12, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(41674, 11, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(41674, 10, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(41674, 9, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(41674, 8, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(41674, 7, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(41674, 6, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(41674, 5, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(41674, 4, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(41674, 3, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(41674, 2, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(41674, 1, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(41675, 1006, 39354, 0, 0, 1, 0, 0, 23420), -- Light Parchment
(41675, 1005, 4364, 2, 0, 1, 0, 0, 23420), -- Coarse Blasting Powder
(41675, 1004, 4357, 4, 0, 1, 0, 0, 23420), -- Rough Blasting Powder
(41675, 1003, 10647, 0, 0, 1, 0, 0, 23420), -- Engineer's Ink
(41675, 1002, 39684, 0, 0, 1, 0, 0, 23420), -- Hair Trigger
(41675, 1001, 40533, 0, 0, 1, 0, 0, 23420), -- Walnut Stock
(41675, 1000, 4400, 0, 0, 1, 0, 0, 23420), -- Heavy Stock
(41675, 999, 4399, 0, 0, 1, 0, 0, 23420), -- Wooden Stock
(41675, 998, 90146, 0, 0, 1, 0, 0, 23420), -- Tinker's Kit
(41675, 6, 3857, 0, 0, 1, 0, 0, 23420), -- Coal
(41675, 5, 18567, 0, 0, 1, 0, 0, 23420), -- Elemental Flux
(41675, 4, 3466, 0, 0, 1, 0, 0, 23420), -- Strong Flux
(41675, 3, 2880, 0, 0, 1, 0, 0, 23420), -- Weak Flux
(41675, 2, 5956, 0, 0, 1, 0, 0, 23420), -- Blacksmith Hammer
(41675, 1, 2901, 0, 0, 1, 0, 0, 23420), -- Mining Pick
(41676, 40, 4565, 0, 0, 1, 0, 0, 23420), -- Simple Dagger
(41676, 39, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(41676, 38, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(41676, 37, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(41676, 36, 6532, 0, 0, 1, 0, 0, 23420), -- Bright Baubles
(41676, 35, 6530, 0, 0, 1, 0, 0, 23420), -- Nightcrawlers
(41676, 34, 4400, 0, 0, 1, 0, 0, 23420), -- Heavy Stock
(41676, 33, 4399, 0, 0, 1, 0, 0, 23420), -- Wooden Stock
(41676, 32, 4289, 0, 0, 1, 0, 0, 23420), -- Salt
(41676, 31, 3371, 0, 0, 1, 0, 0, 23420), -- Crystal Vial
(41676, 30, 4340, 0, 0, 1, 0, 0, 23420), -- Gray Dye
(41676, 29, 4342, 0, 0, 1, 0, 0, 23420), -- Purple Dye
(41676, 28, 4341, 0, 0, 1, 0, 0, 23420), -- Yellow Dye
(41676, 27, 2325, 0, 0, 1, 0, 0, 23420), -- Black Dye
(41676, 26, 2604, 0, 0, 1, 0, 0, 23420), -- Red Dye
(41676, 25, 2324, 0, 0, 1, 0, 0, 23420), -- Bleach
(41676, 24, 3857, 0, 0, 1, 0, 0, 23420), -- Coal
(41676, 23, 3466, 0, 0, 1, 0, 0, 23420), -- Strong Flux
(41676, 22, 2880, 0, 0, 1, 0, 0, 23420), -- Weak Flux
(41676, 21, 2678, 0, 0, 1, 0, 0, 23420), -- Mild Spices
(41676, 20, 14341, 0, 0, 1, 0, 0, 23420), -- Rune Thread
(41676, 19, 8343, 0, 0, 1, 0, 0, 23420), -- Heavy Silken Thread
(41676, 18, 4291, 0, 0, 1, 0, 0, 23420), -- Silken Thread
(41676, 17, 2321, 0, 0, 1, 0, 0, 23420), -- Fine Thread
(41676, 16, 2320, 0, 0, 1, 0, 0, 23420), -- Coarse Thread
(41676, 15, 6217, 0, 0, 1, 0, 0, 23420), -- Copper Rod
(41676, 14, 5956, 0, 0, 1, 0, 0, 23420), -- Blacksmith Hammer
(41676, 13, 6256, 0, 0, 1, 0, 0, 23420), -- Fishing Pole
(41676, 12, 2901, 0, 0, 1, 0, 0, 23420), -- Mining Pick
(41676, 11, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(41676, 10, 7005, 0, 0, 1, 0, 0, 23420), -- Skinning Knife
(41676, 9, 4470, 0, 0, 1, 0, 0, 23420), -- Simple Wood
(41676, 8, 4499, 0, 0, 1, 0, 0, 23420), -- Huge Brown Sack
(41676, 7, 4497, 0, 0, 1, 0, 0, 23420), -- Heavy Brown Bag
(41676, 6, 4601, 0, 0, 1, 0, 0, 23420), -- Soft Banana Bread
(41676, 5, 4542, 0, 0, 1, 0, 0, 23420), -- Moist Cornbread
(41676, 4, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(41676, 3, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(41676, 2, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(41676, 1, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(43633, 12, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(43633, 11, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(43633, 10, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(43633, 9, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(43633, 8, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(43633, 7, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(43633, 6, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(43633, 5, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(43633, 4, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(43633, 3, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(43633, 2, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(43633, 1, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(43646, 2, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(43646, 1, 4565, 0, 0, 1, 0, 0, 23420), -- Simple Dagger
(43568, 23, 239, 0, 0, 1, 0, 0, 23420), -- Cured Leather Gloves
(43568, 22, 1850, 0, 0, 1, 0, 0, 23420), -- Cured Leather Bracers
(43568, 21, 238, 0, 0, 1, 0, 0, 23420), -- Cured Leather Boots
(43568, 20, 237, 0, 0, 1, 0, 0, 23420), -- Cured Leather Pants
(43568, 19, 1849, 0, 0, 1, 0, 0, 23420), -- Cured Leather Belt
(43568, 18, 236, 0, 0, 1, 0, 0, 23420), -- Cured Leather Armor
(43568, 17, 10290, 0, 0, 1, 0, 0, 23420), -- Pink Dye
(43568, 16, 4342, 0, 0, 1, 0, 0, 23420), -- Purple Dye
(43568, 15, 6261, 0, 0, 1, 0, 0, 23420), -- Orange Dye
(43568, 14, 4341, 0, 0, 1, 0, 0, 23420), -- Yellow Dye
(43568, 13, 4340, 0, 0, 1, 0, 0, 23420), -- Gray Dye
(43568, 12, 2605, 0, 0, 1, 0, 0, 23420), -- Green Dye
(43568, 11, 2604, 0, 0, 1, 0, 0, 23420), -- Red Dye
(43568, 10, 6260, 0, 0, 1, 0, 0, 23420), -- Blue Dye
(43568, 9, 2325, 0, 0, 1, 0, 0, 23420), -- Black Dye
(43568, 8, 38426, 0, 0, 1, 0, 0, 23420), -- Eternium Thread
(43568, 7, 14341, 0, 0, 1, 0, 0, 23420), -- Rune Thread
(43568, 6, 8343, 0, 0, 1, 0, 0, 23420), -- Heavy Silken Thread
(43568, 5, 4291, 0, 0, 1, 0, 0, 23420), -- Silken Thread
(43568, 4, 2321, 0, 0, 1, 0, 0, 23420), -- Fine Thread
(43568, 3, 2320, 0, 0, 1, 0, 0, 23420), -- Coarse Thread
(43568, 2, 4289, 0, 0, 1, 0, 0, 23420), -- Salt
(43568, 1, 7005, 0, 0, 1, 0, 0, 23420), -- Skinning Knife
(3413, 2082, 39354, 0, 0, 1, 0, 0, 23420), -- Light Parchment
(3413, 2072, 90146, 0, 0, 1, 0, 0, 23420), -- Tinker's Kit
(3413, 23, 68660, 0, 6003, 1, 0, 0, 23420), -- Mystic Cogwheel
(43645, 24, 854, 0, 0, 1, 0, 1, 23420), -- Quarter Staff
(43645, 23, 2207, 0, 0, 1, 0, 1, 23420), -- Jambiya
(43645, 22, 1197, 0, 0, 1, 0, 1, 23420), -- Giant Mace
(43645, 21, 852, 0, 0, 1, 0, 1, 23420), -- Mace
(43645, 20, 1196, 0, 0, 1, 0, 1, 23420), -- Tabar
(43645, 19, 853, 0, 0, 1, 0, 1, 23420), -- Hatchet
(43645, 18, 1198, 0, 0, 1, 0, 1, 23420), -- Claymore
(43645, 17, 851, 0, 0, 1, 0, 1, 23420), -- Cutlass
(43645, 16, 2493, 0, 0, 1, 0, 1, 23420), -- Wooden Mallet
(43645, 15, 2490, 0, 0, 1, 0, 1, 23420), -- Tomahawk
(43645, 14, 2488, 0, 0, 1, 0, 1, 23420), -- Gladius
(43645, 13, 2495, 0, 0, 1, 0, 1, 23420), -- Walking Stick
(43645, 12, 2494, 0, 0, 1, 0, 1, 23420), -- Stiletto
(43645, 11, 2491, 0, 0, 1, 0, 1, 23420), -- Large Axe
(43645, 10, 2492, 0, 0, 1, 0, 1, 23420), -- Cudgel
(43645, 9, 2489, 0, 0, 1, 0, 1, 23420), -- Two-Handed Sword
(43645, 8, 2132, 0, 0, 1, 0, 0, 23420), -- Short Staff
(43645, 7, 2139, 0, 0, 1, 0, 0, 23420), -- Dirk
(43645, 6, 2480, 0, 0, 1, 0, 0, 23420), -- Large Club
(43645, 5, 2130, 0, 0, 1, 0, 0, 23420), -- Club
(43645, 4, 2479, 0, 0, 1, 0, 0, 23420); -- Broad Axe

INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(43645, 3, 2134, 0, 0, 1, 0, 0, 23420), -- Hand Axe
(43645, 2, 1194, 0, 0, 1, 0, 0, 23420), -- Bastard Sword
(43645, 1, 2131, 0, 0, 1, 0, 0, 23420), -- Shortsword
(43955, 16, 1202, 0, 0, 1, 0, 1, 23420), -- Wall Shield
(43955, 15, 17187, 0, 0, 1, 0, 1, 23420), -- Banded Buckler
(43955, 14, 850, 0, 0, 1, 0, 0, 23420), -- Chainmail Gloves
(43955, 13, 1846, 0, 0, 1, 0, 0, 23420), -- Chainmail Bracers
(43955, 12, 849, 0, 0, 1, 0, 0, 23420), -- Chainmail Boots
(43955, 11, 848, 0, 0, 1, 0, 0, 23420), -- Chainmail Pants
(43955, 10, 1845, 0, 0, 1, 0, 0, 23420), -- Chainmail Belt
(43955, 9, 847, 0, 0, 1, 0, 0, 23420), -- Chainmail Armor
(43955, 8, 2376, 0, 0, 1, 0, 1, 23420), -- Worn Heater Shield
(43955, 7, 17185, 0, 0, 1, 0, 1, 23420), -- Round Buckler
(43955, 6, 2403, 0, 0, 1, 0, 0, 23420), -- Light Chain Gloves
(43955, 5, 2402, 0, 0, 1, 0, 0, 23420), -- Light Chain Bracers
(43955, 4, 2401, 0, 0, 1, 0, 0, 23420), -- Light Chain Boots
(43955, 3, 2400, 0, 0, 1, 0, 0, 23420), -- Light Chain Leggings
(43955, 2, 2399, 0, 0, 1, 0, 0, 23420), -- Light Chain Belt
(43955, 1, 2398, 0, 0, 1, 0, 0, 23420), -- Light Chain Armor
(14754, 74, 30498, 0, 0, 1, 0, 0, 23420), -- Outrider's Lamellar Legguards
(14754, 73, 22651, 0, 0, 1, 0, 0, 23420), -- Outrider's Plate Legguards
(14754, 72, 22673, 0, 5996, 1, 0, 0, 23420), -- Outrider's Chain Leggings
(14754, 71, 22676, 0, 5996, 1, 0, 0, 23420), -- Outrider's Mail Leggings
(14754, 70, 22740, 0, 5996, 1, 0, 0, 23420), -- Outrider's Leather Pants
(14754, 69, 22741, 0, 5996, 1, 0, 0, 23420), -- Outrider's Lizardhide Pants
(14754, 68, 22747, 0, 5996, 1, 0, 0, 23420), -- Outrider's Silk Leggings
(14754, 67, 19582, 0, 5995, 1, 0, 0, 23420), -- Windtalker's Wristguards
(14754, 66, 19583, 0, 5995, 1, 0, 0, 23420), -- Windtalker's Wristguards
(14754, 65, 19584, 0, 5995, 1, 0, 0, 23420), -- Windtalker's Wristguards
(14754, 64, 19587, 0, 5995, 1, 0, 0, 23420), -- Forest Stalker's Bracers
(14754, 63, 19589, 0, 5995, 1, 0, 0, 23420), -- Forest Stalker's Bracers
(14754, 62, 19590, 0, 5995, 1, 0, 0, 23420), -- Forest Stalker's Bracers
(14754, 61, 19595, 0, 5995, 1, 0, 0, 23420), -- Dryad's Wrist Bindings
(14754, 60, 19596, 0, 5995, 1, 0, 0, 23420), -- Dryad's Wrist Bindings
(14754, 59, 19597, 0, 5995, 1, 0, 0, 23420), -- Dryad's Wrist Bindings
(14754, 58, 19578, 0, 5995, 1, 0, 0, 23420), -- Berserker Bracers
(14754, 57, 19580, 0, 5995, 1, 0, 0, 23420), -- Berserker Bracers
(14754, 56, 19581, 0, 5995, 1, 0, 0, 23420), -- Berserker Bracers
(14754, 55, 19505, 0, 5992, 1, 0, 0, 23420), -- Warsong Battle Tabard
(14754, 54, 19542, 0, 5993, 1, 0, 0, 23420), -- Scout's Blade
(14754, 53, 19543, 0, 5993, 1, 0, 0, 23420), -- Scout's Blade
(14754, 52, 19544, 0, 5993, 1, 0, 0, 23420), -- Scout's Blade
(14754, 51, 19545, 0, 5993, 1, 0, 0, 23420), -- Scout's Blade
(14754, 50, 20441, 0, 5993, 1, 0, 0, 23420), -- Scout's Blade
(14754, 49, 19558, 0, 5993, 1, 0, 0, 23420), -- Outrider's Bow
(14754, 48, 19559, 0, 5993, 1, 0, 0, 23420), -- Outrider's Bow
(14754, 47, 19560, 0, 5993, 1, 0, 0, 23420), -- Outrider's Bow
(14754, 46, 19561, 0, 5993, 1, 0, 0, 23420), -- Outrider's Bow
(14754, 45, 20437, 0, 5993, 1, 0, 0, 23420), -- Outrider's Bow
(14754, 44, 19550, 0, 5993, 1, 0, 0, 23420), -- Legionnaire's Sword
(14754, 43, 19551, 0, 5993, 1, 0, 0, 23420), -- Legionnaire's Sword
(14754, 42, 19552, 0, 5993, 1, 0, 0, 23420), -- Legionnaire's Sword
(14754, 41, 19553, 0, 5993, 1, 0, 0, 23420), -- Legionnaire's Sword
(14754, 40, 20430, 0, 5993, 1, 0, 0, 23420), -- Legionnaire's Sword
(14754, 39, 19566, 0, 5993, 1, 0, 0, 23420), -- Advisor's Gnarled Staff
(14754, 38, 19567, 0, 5993, 1, 0, 0, 23420), -- Advisor's Gnarled Staff
(14754, 37, 19568, 0, 5993, 1, 0, 0, 23420), -- Advisor's Gnarled Staff
(14754, 36, 19569, 0, 5993, 1, 0, 0, 23420), -- Advisor's Gnarled Staff
(14754, 35, 20425, 0, 5993, 1, 0, 0, 23420), -- Advisor's Gnarled Staff
(14754, 34, 17351, 0, 0, 1, 0, 0, 23420), -- Major Mana Draught
(14754, 33, 17348, 0, 0, 1, 0, 0, 23420), -- Major Healing Draught
(14754, 32, 19534, 0, 5995, 1, 0, 0, 23420), -- Scout's Medallion
(14754, 31, 19535, 0, 5995, 1, 0, 0, 23420), -- Scout's Medallion
(14754, 30, 19536, 0, 5995, 1, 0, 0, 23420), -- Scout's Medallion
(14754, 29, 19537, 0, 5995, 1, 0, 0, 23420), -- Scout's Medallion
(14754, 28, 20442, 0, 5995, 1, 0, 0, 23420), -- Scout's Medallion
(14754, 27, 19510, 0, 5995, 1, 0, 0, 23420), -- Legionnaire's Band
(14754, 26, 19511, 0, 5995, 1, 0, 0, 23420), -- Legionnaire's Band
(14754, 25, 19512, 0, 5995, 1, 0, 0, 23420), -- Legionnaire's Band
(14754, 24, 19513, 0, 5995, 1, 0, 0, 23420), -- Legionnaire's Band
(14754, 23, 20429, 0, 5995, 1, 0, 0, 23420), -- Legionnaire's Band
(14754, 22, 19526, 0, 5995, 1, 0, 0, 23420), -- Battle Healer's Cloak
(14754, 21, 19527, 0, 5995, 1, 0, 0, 23420), -- Battle Healer's Cloak
(14754, 20, 19528, 0, 5995, 1, 0, 0, 23420), -- Battle Healer's Cloak
(14754, 19, 19529, 0, 5995, 1, 0, 0, 23420), -- Battle Healer's Cloak
(14754, 18, 20427, 0, 5995, 1, 0, 0, 23420), -- Battle Healer's Cloak
(14754, 17, 19518, 0, 5995, 1, 0, 0, 23420), -- Advisor's Ring
(14754, 16, 19519, 0, 5995, 1, 0, 0, 23420), -- Advisor's Ring
(14754, 15, 19520, 0, 5995, 1, 0, 0, 23420), -- Advisor's Ring
(14754, 14, 19521, 0, 5995, 1, 0, 0, 23420), -- Advisor's Ring
(14754, 13, 20426, 0, 5995, 1, 0, 0, 23420), -- Advisor's Ring
(14754, 12, 17352, 0, 0, 1, 0, 0, 23420), -- Superior Mana Draught
(14754, 11, 17349, 0, 0, 1, 0, 0, 23420), -- Superior Healing Draught
(14754, 2, 21565, 0, 5995, 1, 0, 0, 23420), -- Rune of Perfection
(14754, 1, 21566, 0, 5995, 1, 0, 0, 23420), -- Rune of Perfection
(43953, 16, 4499, 0, 0, 1, 0, 0, 23420), -- Huge Brown Sack
(43953, 15, 4601, 0, 0, 1, 0, 0, 23420), -- Soft Banana Bread
(43953, 14, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(43953, 13, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(43953, 12, 4497, 0, 0, 1, 0, 0, 23420), -- Heavy Brown Bag
(43953, 11, 5048, 0, 0, 1, 0, 0, 23420), -- Blue Ribboned Wrapping Paper
(43953, 10, 4542, 0, 0, 1, 0, 0, 23420), -- Moist Cornbread
(43953, 9, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(43953, 8, 4541, 0, 0, 1, 0, 0, 23420), -- Freshly Baked Bread
(43953, 7, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(43953, 6, 5042, 0, 0, 1, 0, 0, 23420), -- Red Ribboned Wrapping Paper
(43953, 5, 4470, 0, 0, 1, 0, 0, 23420), -- Simple Wood
(43953, 4, 4498, 0, 0, 1, 0, 0, 23420), -- Brown Leather Satchel
(43953, 3, 4496, 0, 0, 1, 0, 0, 23420), -- Small Brown Pouch
(43953, 2, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(43953, 1, 4540, 0, 0, 1, 0, 0, 23420), -- Tough Hunk of Bread
(43957, 16, 2376, 0, 0, 1, 0, 1, 23420), -- Worn Heater Shield
(43957, 15, 17185, 0, 0, 1, 0, 1, 23420), -- Round Buckler
(43957, 14, 2403, 0, 0, 1, 0, 0, 23420), -- Light Chain Gloves
(43957, 13, 2402, 0, 0, 1, 0, 0, 23420), -- Light Chain Bracers
(43957, 12, 2401, 0, 0, 1, 0, 0, 23420), -- Light Chain Boots
(43957, 11, 2400, 0, 0, 1, 0, 0, 23420), -- Light Chain Leggings
(43957, 10, 2399, 0, 0, 1, 0, 0, 23420), -- Light Chain Belt
(43957, 9, 2398, 0, 0, 1, 0, 0, 23420), -- Light Chain Armor
(43957, 8, 1202, 0, 0, 1, 0, 1, 23420), -- Wall Shield
(43957, 7, 17187, 0, 0, 1, 0, 1, 23420), -- Banded Buckler
(43957, 6, 850, 0, 0, 1, 0, 0, 23420), -- Chainmail Gloves
(43957, 5, 1846, 0, 0, 1, 0, 0, 23420), -- Chainmail Bracers
(43957, 4, 849, 0, 0, 1, 0, 0, 23420), -- Chainmail Boots
(43957, 3, 848, 0, 0, 1, 0, 0, 23420), -- Chainmail Pants
(43957, 2, 1845, 0, 0, 1, 0, 0, 23420), -- Chainmail Belt
(43957, 1, 847, 0, 0, 1, 0, 0, 23420), -- Chainmail Armor
(43945, 18, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(43945, 17, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(43945, 16, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(43945, 15, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(43945, 14, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(43945, 13, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(43945, 12, 8953, 0, 0, 1, 0, 0, 23420), -- Deep Fried Plantains
(43945, 11, 4602, 0, 0, 1, 0, 0, 23420), -- Moon Harvest Pumpkin
(43945, 10, 4539, 0, 0, 1, 0, 0, 23420), -- Goldenbark Apple
(43945, 9, 4538, 0, 0, 1, 0, 0, 23420), -- Snapvine Watermelon
(43945, 8, 4537, 0, 0, 1, 0, 0, 23420), -- Tel'Abim Banana
(43945, 7, 4536, 0, 0, 1, 0, 0, 23420), -- Shiny Red Apple
(43945, 6, 8957, 0, 0, 1, 0, 0, 23420), -- Spinefin Halibut
(43945, 5, 21552, 0, 0, 1, 0, 0, 23420), -- Striped Yellowtail
(43945, 4, 4594, 0, 0, 1, 0, 0, 23420), -- Rockscale Cod
(43945, 3, 4593, 0, 0, 1, 0, 0, 23420), -- Bristle Whisker Catfish
(43945, 2, 4592, 0, 0, 1, 0, 0, 23420), -- Longjaw Mud Snapper
(43945, 1, 787, 0, 0, 1, 0, 0, 23420), -- Slitherskin Mackerel
(3495, 1673, 39354, 0, 0, 1, 0, 0, 23420), -- Light Parchment
(3495, 1661, 90146, 0, 0, 1, 0, 0, 23420), -- Tinker's Kit
(3498, 5, 4542, 0, 0, 1, 0, 0, 23420), -- Moist Cornbread
(3498, 4, 4541, 0, 0, 1, 0, 0, 23420), -- Freshly Baked Bread
(3572, 523, 136377, 0, 0, 1, 0, 0, 23420), -- Oversized Bobber
(3499, 20, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(3499, 19, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(3499, 5, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(3490, 2, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(3490, 1, 4565, 0, 0, 1, 0, 0, 23420), -- Simple Dagger
(3481, 4, 4541, 0, 0, 1, 0, 0, 23420), -- Freshly Baked Bread
(3481, 3, 4540, 0, 0, 1, 0, 0, 23420), -- Tough Hunk of Bread
(3482, 21, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(3482, 5, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(3482, 2, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(43951, 12, 844, 0, 0, 1, 0, 0, 23420), -- Tanned Leather Gloves
(43951, 11, 1844, 0, 0, 1, 0, 0, 23420), -- Tanned Leather Bracers
(43951, 10, 843, 0, 0, 1, 0, 0, 23420), -- Tanned Leather Boots
(43951, 9, 845, 0, 0, 1, 0, 0, 23420), -- Tanned Leather Pants
(43951, 8, 1843, 0, 0, 1, 0, 0, 23420), -- Tanned Leather Belt
(43951, 7, 846, 0, 0, 1, 0, 0, 23420), -- Tanned Leather Jerkin
(43951, 6, 2375, 0, 0, 1, 0, 1, 23420), -- Battered Leather Gloves
(43951, 5, 2374, 0, 0, 1, 0, 1, 23420), -- Battered Leather Bracers
(43951, 4, 2373, 0, 0, 1, 0, 1, 23420), -- Battered Leather Boots
(43951, 3, 2372, 0, 0, 1, 0, 1, 23420), -- Battered Leather Pants
(43951, 2, 2371, 0, 0, 1, 0, 1, 23420), -- Battered Leather Belt
(43951, 1, 2370, 0, 0, 1, 0, 1, 23420), -- Battered Leather Harness
(43956, 6, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(43956, 5, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(43956, 4, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(43956, 3, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(43956, 2, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(43956, 1, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(43949, 12, 2375, 0, 0, 1, 0, 1, 23420), -- Battered Leather Gloves
(43949, 11, 2374, 0, 0, 1, 0, 1, 23420), -- Battered Leather Bracers
(43949, 10, 2373, 0, 0, 1, 0, 1, 23420), -- Battered Leather Boots
(43949, 9, 2372, 0, 0, 1, 0, 1, 23420), -- Battered Leather Pants
(43949, 8, 2371, 0, 0, 1, 0, 1, 23420), -- Battered Leather Belt
(43949, 7, 2370, 0, 0, 1, 0, 1, 23420), -- Battered Leather Harness
(43949, 6, 2369, 0, 0, 1, 0, 0, 23420), -- Woven Gloves
(43949, 5, 3607, 0, 0, 1, 0, 0, 23420), -- Woven Bracers
(43949, 4, 2367, 0, 0, 1, 0, 0, 23420), -- Woven Boots
(43949, 3, 2366, 0, 0, 1, 0, 0, 23420), -- Woven Pants
(43949, 2, 3606, 0, 0, 1, 0, 0, 23420), -- Woven Belt
(43949, 1, 2364, 0, 0, 1, 0, 0, 23420), -- Woven Vest
(3351, 1, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(49737, 1, 68689, 0, 3934, 1, 0, 0, 23420), -- Imported Supplies
(3367, 29, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(3367, 28, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(3367, 4, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(3364, 280, 6270, 1, 0, 1, 0, 0, 23420), -- Pattern: Blue Linen Vest
(52809, 24, 137663, 0, 0, 1, 0, 0, 23420), -- Soft Foam Sword
(49889, 8, 61986, 0, 0, 1, 0, 0, 23420), -- Tol Barad Coconut Rum
(49889, 7, 61985, 0, 0, 1, 0, 0, 23420), -- Banana Cocktail
(49889, 6, 61984, 0, 0, 1, 0, 0, 23420), -- Potent Pineapple Punch
(49889, 5, 61983, 0, 0, 1, 0, 0, 23420), -- Imported E.K. Ale
(49889, 4, 61982, 0, 0, 1, 0, 0, 23420), -- Fizzy Fruit Wine
(115797, 13, 142313, 0, 6126, 1, 0, 0, 23420), -- -Unknown-
(115797, 12, 142311, 0, 6126, 1, 0, 0, 23420), -- -Unknown-
(115797, 11, 142294, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 10, 142293, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 9, 142292, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 8, 142291, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 7, 142290, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 6, 142289, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 5, 142288, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 4, 142314, 0, 6128, 1, 0, 0, 23420), -- -Unknown-
(115797, 3, 142317, 0, 6129, 1, 0, 0, 23420), -- -Unknown-
(115797, 2, 142319, 0, 6127, 1, 0, 0, 23420), -- -Unknown-
(115797, 1, 142318, 0, 6127, 1, 0, 0, 23420), -- -Unknown-
(68794, 4, 93158, 0, 0, 1, 0, 0, 23420), -- Expired Blackout Brew
(68794, 3, 93045, 0, 0, 1, 0, 0, 23420), -- Rotten Watermelon
(68794, 2, 93044, 0, 0, 1, 0, 0, 23420), -- Rotten Banana
(68794, 1, 93043, 0, 0, 1, 0, 0, 23420), -- Rotten Apple
(68364, 10, 142403, 0, 0, 1, 19944, 1, 23420), -- -Unknown-
(68364, 9, 122396, 0, 0, 1, 19944, 1, 23420), -- Brawler's Razor Claws
(68364, 7, 144392, 0, 0, 1, 19940, 1, 23420), -- -Unknown-
(68364, 6, 144394, 0, 0, 1, 19939, 1, 23420), -- -Unknown-
(68364, 5, 93025, 0, 0, 1, 19939, 1, 23420), -- Clock'em
(68364, 2, 142326, 0, 0, 1, 0, 0, 23420), -- -Unknown-
(68364, 1, 142325, 0, 0, 1, 0, 0, 23420), -- -Unknown-
(3333, 470, 136377, 0, 0, 1, 0, 0, 23420), -- Oversized Bobber
(69333, 2, 92070, 0, 0, 1, 19984, 0, 23420), -- Huojin Satchel
(69333, 1, 83080, 0, 0, 1, 0, 0, 23420), -- Huojin Tabard
(42709, 19, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(42709, 18, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(46642, 40, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(46642, 39, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(46642, 26, 82449, 0, 0, 1, 0, 0, 23420), -- Barnacle Bouillabaisse
(46642, 25, 82448, 0, 0, 1, 0, 0, 23420), -- Smoked Squid Belly
(46642, 13, 81920, 0, 0, 1, 0, 0, 23420), -- Plump Fig
(46642, 12, 81919, 0, 0, 1, 0, 0, 23420), -- Red Raspberry
(3335, 1, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(3334, 1, 4565, 0, 0, 1, 0, 0, 23420), -- Simple Dagger
(45552, 18, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(45552, 3, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(45546, 1570, 39354, 0, 0, 1, 0, 0, 23420), -- Light Parchment
(45546, 1559, 90146, 0, 0, 1, 0, 0, 23420), -- Tinker's Kit
(45546, 23, 68660, 0, 6003, 1, 0, 0, 23420), -- Mystic Cogwheel
(45565, 24, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(45565, 23, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(45566, 24, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(45566, 23, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(45563, 40, 81918, 0, 0, 1, 0, 0, 23420), -- Pickled Pig's Snout
(45563, 39, 81917, 0, 0, 1, 0, 0, 23420), -- Mutton Stew
(45563, 27, 82451, 0, 0, 1, 0, 0, 23420), -- Frybread
(45563, 26, 82450, 0, 0, 1, 0, 0, 23420), -- Cornmeal Biscuit
(45563, 14, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(45563, 13, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(30723, 4971, 79740, 0, 0, 1, 0, 0, 23420), -- Plain Wooden Staff
(30723, 4970, 1515, 0, 0, 1, 0, 0, 23420), -- Rough Wooden Staff
(30723, 33, 113111, 0, 2582, 1, 0, 0, 23420), -- Warbinder's Ink
(30723, 32, 79255, 0, 2583, 1, 0, 0, 23420), -- Starlight Ink
(30723, 29, 79254, 0, 2582, 1, 0, 0, 23420), -- Ink of Dreams
(30723, 28, 61978, 0, 2582, 1, 0, 0, 23420); -- Blackfallow Ink

INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(44975, 438, 136377, 0, 0, 1, 0, 0, 23420), -- Oversized Bobber
(45008, 40, 81920, 0, 0, 1, 0, 0, 23420), -- Plump Fig
(45008, 39, 81919, 0, 0, 1, 0, 0, 23420), -- Red Raspberry
(45008, 27, 82449, 0, 0, 1, 0, 0, 23420), -- Barnacle Bouillabaisse
(45008, 26, 82448, 0, 0, 1, 0, 0, 23420), -- Smoked Squid Belly
(45008, 14, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(45008, 13, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(45093, 28, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(45093, 27, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(45093, 3, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(45086, 45, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(45086, 44, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(45086, 31, 82449, 0, 0, 1, 0, 0, 23420), -- Barnacle Bouillabaisse
(45086, 30, 82448, 0, 0, 1, 0, 0, 23420), -- Smoked Squid Belly
(45086, 18, 81920, 0, 0, 1, 0, 0, 23420), -- Plump Fig
(45086, 17, 81919, 0, 0, 1, 0, 0, 23420), -- Red Raspberry
(3405, 2, 4565, 0, 0, 1, 0, 0, 23420), -- Simple Dagger
(3405, 1, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(44780, 28, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(44780, 27, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(44780, 3, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(44785, 40, 82451, 0, 0, 1, 0, 0, 23420), -- Frybread
(44785, 39, 82450, 0, 0, 1, 0, 0, 23420), -- Cornmeal Biscuit
(44785, 27, 81921, 0, 0, 1, 0, 0, 23420), -- Chilton Stilton
(44785, 26, 81922, 0, 0, 1, 0, 0, 23420), -- Redridge Roquefort
(44785, 14, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(44785, 13, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(3322, 94, 11303, 1, 0, 1, 0, 0, 23420), -- Fine Shortbow
(52032, 27, 113111, 0, 2582, 1, 0, 0, 23420), -- Warbinder's Ink
(52032, 26, 79255, 0, 2583, 1, 0, 0, 23420), -- Starlight Ink
(52032, 23, 79254, 0, 2582, 1, 0, 0, 23420), -- Ink of Dreams
(52032, 22, 61978, 0, 2582, 1, 0, 0, 23420), -- Blackfallow Ink
(46718, 4977, 79740, 0, 0, 1, 0, 0, 23420), -- Plain Wooden Staff
(46718, 4976, 1515, 0, 0, 1, 0, 0, 23420), -- Rough Wooden Staff
(46718, 33, 113111, 0, 2582, 1, 0, 0, 23420), -- Warbinder's Ink
(46718, 32, 79255, 0, 2583, 1, 0, 0, 23420), -- Starlight Ink
(46718, 29, 79254, 0, 2582, 1, 0, 0, 23420), -- Ink of Dreams
(46718, 28, 61978, 0, 2582, 1, 0, 0, 23420), -- Blackfallow Ink
(3368, 26, 82451, 0, 0, 1, 0, 0, 23420), -- Frybread
(3368, 25, 82450, 0, 0, 1, 0, 0, 23420), -- Cornmeal Biscuit
(3368, 13, 81918, 0, 0, 1, 0, 0, 23420), -- Pickled Pig's Snout
(3368, 12, 81917, 0, 0, 1, 0, 0, 23420), -- Mutton Stew
(5611, 32, 81918, 0, 0, 1, 0, 0, 23420), -- Pickled Pig's Snout
(5611, 31, 81917, 0, 0, 1, 0, 0, 23420), -- Mutton Stew
(5611, 19, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(5611, 18, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(6929, 40, 81923, 0, 0, 1, 0, 0, 23420), -- Cobo Cola
(6929, 39, 81924, 0, 0, 1, 0, 0, 23420), -- Carbonated Water
(6929, 26, 82451, 0, 0, 1, 0, 0, 23420), -- Frybread
(6929, 25, 82450, 0, 0, 1, 0, 0, 23420), -- Cornmeal Biscuit
(6929, 13, 81918, 0, 0, 1, 0, 0, 23420), -- Pickled Pig's Snout
(6929, 12, 81917, 0, 0, 1, 0, 0, 23420), -- Mutton Stew
(3342, 13, 81920, 0, 0, 1, 0, 0, 23420), -- Plump Fig
(3342, 12, 81919, 0, 0, 1, 0, 0, 23420), -- Red Raspberry
(3312, 13, 81918, 0, 0, 1, 0, 0, 23420), -- Pickled Pig's Snout
(3312, 12, 81917, 0, 0, 1, 0, 0, 23420), -- Mutton Stew
(3323, 1, 64670, 0, 0, 1, 0, 0, 23420), -- Vanishing Powder
(5188, 18, 15199, 0, 5962, 1, 0, 0, 23420), -- Stone Guard's Herald
(5188, 17, 15197, 0, 5962, 1, 0, 0, 23420), -- Scout's Tabard
(5188, 16, 19031, 0, 5992, 1, 0, 0, 23420), -- Frostwolf Battle Tabard
(5188, 15, 19505, 0, 5992, 1, 0, 0, 23420), -- Warsong Battle Tabard
(5817, 18, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(5817, 3, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(3168, 19, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(3168, 18, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(3168, 3, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(3187, 19, 39505, 0, 0, 1, 0, 0, 23420), -- Virtuoso Inking Set
(3187, 18, 20815, 0, 0, 1, 0, 0, 23420), -- Jeweler's Kit
(3187, 3, 85663, 0, 0, 1, 0, 0, 23420), -- Herbalist's Spade
(3186, 2, 4540, 0, 0, 1, 0, 0, 23420), -- Tough Hunk of Bread
(39031, 12, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(39031, 11, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(39031, 10, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(39031, 9, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(39031, 8, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(39031, 7, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(39031, 6, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(39031, 5, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(39031, 4, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(39031, 3, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(39031, 2, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(39031, 1, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(5942, 447, 136377, 0, 0, 1, 0, 0, 23420), -- Oversized Bobber
(44276, 12, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(44276, 11, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(44276, 10, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(44276, 9, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(44276, 8, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(44276, 7, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(44276, 6, 8952, 0, 0, 1, 0, 0, 23420), -- Roasted Quail
(44276, 5, 4599, 0, 0, 1, 0, 0, 23420), -- Cured Ham Steak
(44276, 4, 3771, 0, 0, 1, 0, 0, 23420), -- Wild Hog Shank
(44276, 3, 3770, 0, 0, 1, 0, 0, 23420), -- Mutton Chop
(44276, 2, 2287, 0, 0, 1, 0, 0, 23420), -- Haunch of Meat
(44276, 1, 117, 0, 0, 1, 0, 0, 23420), -- Tough Jerky
(44300, 8, 4470, 0, 0, 1, 0, 0, 23420), -- Simple Wood
(44300, 7, 4497, 0, 0, 1, 0, 0, 23420), -- Heavy Brown Bag
(44300, 6, 4498, 0, 0, 1, 0, 0, 23420), -- Brown Leather Satchel
(44300, 5, 4542, 0, 0, 1, 0, 0, 23420), -- Moist Cornbread
(44300, 4, 4541, 0, 0, 1, 0, 0, 23420), -- Freshly Baked Bread
(44300, 3, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(44300, 2, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(44300, 1, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(44287, 10, 2531, 0, 0, 1, 0, 1, 23420), -- Great Axe
(44287, 9, 2523, 0, 0, 1, 0, 1, 23420), -- Bullova
(44287, 8, 2530, 0, 0, 1, 0, 1, 23420), -- Francisca
(44287, 7, 2522, 0, 0, 1, 0, 1, 23420), -- Crescent Axe
(44287, 6, 2529, 0, 0, 1, 0, 1, 23420), -- Zweihander
(44287, 5, 2521, 0, 0, 1, 0, 1, 23420), -- Flamberge
(44287, 4, 2528, 0, 0, 1, 0, 1, 23420), -- Falchion
(44287, 3, 2520, 0, 0, 1, 0, 1, 23420), -- Broadsword
(44287, 2, 2534, 0, 0, 1, 0, 1, 23420), -- Rondel
(44287, 1, 2526, 0, 0, 1, 0, 1, 23420), -- Main Gauche
(44286, 18, 4232, 1, 0, 1, 0, 0, 23420), -- Medium Hide
(44286, 6, 239, 0, 0, 1, 0, 0, 23420), -- Cured Leather Gloves
(44286, 5, 1850, 0, 0, 1, 0, 0, 23420), -- Cured Leather Bracers
(44286, 4, 238, 0, 0, 1, 0, 0, 23420), -- Cured Leather Boots
(44286, 3, 237, 0, 0, 1, 0, 0, 23420), -- Cured Leather Pants
(44286, 2, 1849, 0, 0, 1, 0, 0, 23420), -- Cured Leather Belt
(44286, 1, 236, 0, 0, 1, 0, 0, 23420), -- Cured Leather Armor
(44296, 8, 4470, 0, 0, 1, 0, 0, 23420), -- Simple Wood
(44296, 7, 4497, 0, 0, 1, 0, 0, 23420), -- Heavy Brown Bag
(44296, 6, 4498, 0, 0, 1, 0, 0, 23420), -- Brown Leather Satchel
(44296, 5, 4542, 0, 0, 1, 0, 0, 23420), -- Moist Cornbread
(44296, 4, 4541, 0, 0, 1, 0, 0, 23420), -- Freshly Baked Bread
(44296, 3, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(44296, 2, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(44296, 1, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(44349, 1, 37460, 0, 0, 1, 0, 0, 23420), -- Rope Pet Leash
(44270, 12, 8766, 0, 0, 1, 0, 0, 23420), -- Morning Glory Dew
(44270, 11, 1645, 0, 0, 1, 0, 0, 23420), -- Moonberry Juice
(44270, 10, 1708, 0, 0, 1, 0, 0, 23420), -- Sweet Nectar
(44270, 9, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(44270, 8, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(44270, 7, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(44270, 6, 8953, 0, 0, 1, 0, 0, 23420), -- Deep Fried Plantains
(44270, 5, 4602, 0, 0, 1, 0, 0, 23420), -- Moon Harvest Pumpkin
(44270, 4, 4539, 0, 0, 1, 0, 0, 23420), -- Goldenbark Apple
(44270, 3, 4538, 0, 0, 1, 0, 0, 23420), -- Snapvine Watermelon
(44270, 2, 4537, 0, 0, 1, 0, 0, 23420), -- Tel'Abim Banana
(44270, 1, 4536, 0, 0, 1, 0, 0, 23420), -- Shiny Red Apple
(44285, 6, 203, 0, 0, 1, 0, 0, 23420), -- Thick Cloth Gloves
(44285, 5, 3598, 0, 0, 1, 0, 0, 23420), -- Thick Cloth Bracers
(44285, 4, 202, 0, 0, 1, 0, 0, 23420), -- Thick Cloth Shoes
(44285, 3, 201, 0, 0, 1, 0, 0, 23420), -- Thick Cloth Pants
(44285, 2, 3597, 0, 0, 1, 0, 0, 23420), -- Thick Cloth Belt
(44285, 1, 200, 0, 0, 1, 0, 0, 23420), -- Thick Cloth Vest
(44297, 8, 4470, 0, 0, 1, 0, 0, 23420), -- Simple Wood
(44297, 7, 4497, 0, 0, 1, 0, 0, 23420), -- Heavy Brown Bag
(44297, 6, 4498, 0, 0, 1, 0, 0, 23420), -- Brown Leather Satchel
(44297, 5, 4542, 0, 0, 1, 0, 0, 23420), -- Moist Cornbread
(44297, 4, 4541, 0, 0, 1, 0, 0, 23420), -- Freshly Baked Bread
(44297, 3, 1205, 0, 0, 1, 0, 0, 23420), -- Melon Juice
(44297, 2, 1179, 0, 0, 1, 0, 0, 23420), -- Ice Cold Milk
(44297, 1, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(44286, 17, 2319, 5, 0, 1, 0, 0, 23420), -- Medium Leather
(66685, 15, 76734, 0, 3901, 1, 0, 0, 23420), -- Serpent's Eye
(66685, 14, 79101, 0, 3901, 1, 0, 0, 23420), -- Prismatic Scale
(66685, 13, 72120, 0, 3901, 1, 0, 0, 23420), -- Exotic Leather
(66685, 12, 72103, 0, 3901, 1, 0, 0, 23420), -- White Trillium Ore
(66685, 11, 72094, 0, 3901, 1, 0, 0, 23420), -- Black Trillium Ore
(66685, 10, 72093, 0, 3901, 1, 0, 0, 23420), -- Kyparite
(66685, 9, 72092, 0, 3901, 1, 0, 0, 23420), -- Ghost Iron Ore
(66685, 8, 74247, 0, 3901, 1, 0, 0, 23420), -- Ethereal Shard
(66685, 7, 74250, 0, 3901, 1, 0, 0, 23420), -- Mysterious Essence
(66685, 6, 74249, 0, 3901, 1, 0, 0, 23420), -- Spirit Dust
(66685, 5, 72238, 0, 3901, 1, 0, 0, 23420), -- Golden Lotus
(66685, 4, 89610, 0, 3901, 1, 0, 0, 23420), -- Pandaria Herbs
(66685, 3, 72988, 0, 3901, 1, 0, 0, 23420), -- Windwool Cloth
(66685, 2, 79255, 0, 3901, 1, 0, 0, 23420), -- Starlight Ink
(66685, 1, 402, 1, 3901, 2, 0, 0, 23420), -- 402
(112407, 12, 140965, 0, 6102, 1, 0, 0, 23420), -- Slayer's Greater Armor Kit
(112407, 11, 140936, 0, 6101, 1, 0, 0, 23420), -- Slayer's Armor Kit
(112407, 10, 140964, 0, 5952, 1, 0, 0, 23420), -- Slayer's Lesser Armor Kit
(112407, 9, 139720, 0, 6125, 1, 0, 0, 23420), -- Spaulders of the Shattered Abyss
(112407, 8, 139721, 0, 6125, 1, 0, 0, 23420), -- Belt of the Shattered Abyss
(112407, 7, 139716, 0, 6125, 1, 43054, 0, 23420), -- Boots of the Shattered Abyss
(112407, 6, 139715, 0, 6125, 1, 0, 0, 23420), -- Vest of the Shattered Abyss
(112407, 5, 139719, 0, 6125, 1, 0, 0, 23420), -- Breeches of the Shattered Abyss
(112407, 4, 139717, 0, 6125, 1, 0, 0, 23420), -- Gloves of the Shattered Abyss
(112407, 3, 139722, 0, 6125, 1, 0, 0, 23420), -- Wristwraps of the Shattered Abyss
(112407, 2, 139718, 0, 6125, 1, 0, 0, 23420), -- Helm of the Shattered Abyss
(112407, 1, 140560, 0, 0, 1, 0, 0, 23420), -- Warglaive of the Fel Hammer
(111736, 2, 144434, 0, 6153, 1, 0, 0, 23420), -- -Unknown-
(64126, 15, 21219, 0, 0, 1, 0, 0, 23420), -- Recipe: Sagefish Delight
(64126, 14, 21099, 0, 0, 1, 0, 0, 23420), -- Recipe: Smoked Sagefish
(64126, 13, 2678, 0, 0, 1, 0, 0, 23420), -- Mild Spices
(64126, 12, 30817, 0, 0, 1, 0, 0, 23420), -- Simple Flour
(64126, 11, 159, 0, 0, 1, 0, 0, 23420), -- Refreshing Spring Water
(64126, 10, 74845, 0, 0, 1, 0, 0, 23420), -- Ginseng
(64126, 9, 74832, 0, 0, 1, 0, 0, 23420), -- Barley
(64126, 8, 74659, 0, 0, 1, 0, 0, 23420), -- Farm Chicken
(64126, 7, 85585, 0, 0, 1, 0, 0, 23420), -- Red Beans
(64126, 6, 74851, 0, 0, 1, 0, 0, 23420), -- Rice
(64126, 5, 74852, 0, 0, 1, 0, 0, 23420), -- Yak Milk
(64126, 4, 85584, 0, 0, 1, 0, 0, 23420), -- Silkworm Pupa
(64126, 3, 74660, 0, 0, 1, 0, 0, 23420), -- Pandaren Peach
(64126, 2, 85583, 0, 0, 1, 0, 0, 23420), -- Needle Mushrooms
(64126, 1, 74854, 0, 0, 1, 0, 0, 23420); -- Instant Noodles

UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=22 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2446 AND `ExtendedCost`=0 AND `type`=1); -- Kite Shield
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=17192 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Targe
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2151 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Gloves
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2150 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Bracers
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2149 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Boots
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2152 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Leggings
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2148 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Belt
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2153 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2451 AND `ExtendedCost`=0 AND `type`=1); -- Crested Heater Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=17190 AND `ExtendedCost`=0 AND `type`=1); -- Ornate Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62259 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Helm
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62257 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62256 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62255 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62258 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62254 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62253 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Armor
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62266 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Circlet
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62263 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62262 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62261 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62264 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62260 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62265 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=718 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=1852 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=287 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=286 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=1853 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=285 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2446 AND `ExtendedCost`=0 AND `type`=1); -- Kite Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=17192 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Targe
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2151 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2150 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2149 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2152 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2148 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2153 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Vest
UPDATE `npc_vendor` SET `slot`=2086, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=18647 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Red Firework
UPDATE `npc_vendor` SET `slot`=2085, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=2081, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=2080, `maxcount`=3, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=2079, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=2078, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=2077, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=2076, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=2075, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=2074, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=2073, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=2071, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=2070, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel
UPDATE `npc_vendor` SET `slot`=2086, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=18647 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Red Firework
UPDATE `npc_vendor` SET `slot`=2085, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=2081, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=2080, `maxcount`=3, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=2079, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=2078, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=2077, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=2076, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=2075, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=2074, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=2073, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=2071, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=2070, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel
UPDATE `npc_vendor` SET `slot`=2086, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=18647 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Red Firework
UPDATE `npc_vendor` SET `slot`=2085, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=2081, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=2080, `maxcount`=3, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=2079, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=2078, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=2077, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=2076, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=2075, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=2074, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=2073, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=2071, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=2070, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel
UPDATE `npc_vendor` SET `slot`=2085, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=2084, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=18647 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Red Firework
UPDATE `npc_vendor` SET `slot`=2081, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=2080, `maxcount`=3, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=2079, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=2078, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=2077, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=2076, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=2075, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=2074, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=2073, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=2071, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=2070, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=12196 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2446 AND `ExtendedCost`=0 AND `type`=1); -- Kite Shield
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=17192 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Targe
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2151 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Gloves
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2150 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Bracers
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2149 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Boots
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2152 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Leggings
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2148 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Belt
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2153 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Vest
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2446 AND `ExtendedCost`=0 AND `type`=1); -- Kite Shield
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=17192 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Targe
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2151 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Gloves
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2150 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Bracers
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2149 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Boots
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2152 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Leggings
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2148 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Belt
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=6028 AND `item`=2153 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Vest
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=8953 AND `ExtendedCost`=0 AND `type`=1); -- Deep Fried Plantains
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4602 AND `ExtendedCost`=0 AND `type`=1); -- Moon Harvest Pumpkin
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4539 AND `ExtendedCost`=0 AND `type`=1); -- Goldenbark Apple
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4538 AND `ExtendedCost`=0 AND `type`=1); -- Snapvine Watermelon
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4537 AND `ExtendedCost`=0 AND `type`=1); -- Tel'Abim Banana
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4536 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Red Apple
UPDATE `npc_vendor` SET `slot`=557, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=4765 AND `ExtendedCost`=0 AND `type`=1); -- Enamelled Broadsword
UPDATE `npc_vendor` SET `slot`=556, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=4816 AND `ExtendedCost`=0 AND `type`=1); -- Legionnaire's Leggings
UPDATE `npc_vendor` SET `slot`=555, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=4800 AND `ExtendedCost`=0 AND `type`=1); -- Mighty Chain Pants
UPDATE `npc_vendor` SET `slot`=554, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=4798 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Runed Cloak
UPDATE `npc_vendor` SET `slot`=553, `maxcount`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=4778 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Spiked Mace
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=718 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Gloves
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=1852 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Bracers
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=287 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Boots
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=286 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Pants
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=1853 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Belt
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3682 AND `item`=285 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Vest
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=8948 AND `ExtendedCost`=0 AND `type`=1); -- Dried King Bolete
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=4608 AND `ExtendedCost`=0 AND `type`=1); -- Raw Black Truffle
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=4607 AND `ExtendedCost`=0 AND `type`=1); -- Delicious Cave Mold
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=4606 AND `ExtendedCost`=0 AND `type`=1); -- Spongy Morel
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=8953 AND `ExtendedCost`=0 AND `type`=1); -- Deep Fried Plantains
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=4602 AND `ExtendedCost`=0 AND `type`=1); -- Moon Harvest Pumpkin
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=4539 AND `ExtendedCost`=0 AND `type`=1); -- Goldenbark Apple
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=4538 AND `ExtendedCost`=0 AND `type`=1); -- Snapvine Watermelon
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=14964 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=1672, `maxcount`=4, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=1671, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=1670, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=4371 AND `ExtendedCost`=0 AND `type`=1); -- Bronze Tube
UPDATE `npc_vendor` SET `slot`=1669, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=4382 AND `ExtendedCost`=0 AND `type`=1); -- Bronze Framework
UPDATE `npc_vendor` SET `slot`=1668, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=1667, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=1666, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=1665, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=1664, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=1663, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=1662, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=1660, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1659, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=1658, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=18648 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Green Firework
UPDATE `npc_vendor` SET `slot`=1657, `VerifiedBuild`=23420 WHERE (`entry`=3495 AND `item`=14639 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Minor Recombobulator
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3498 AND `item`=5048 AND `ExtendedCost`=0 AND `type`=1); -- Blue Ribboned Wrapping Paper
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3498 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1); -- Simple Wood
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3498 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1); -- Brown Leather Satchel
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3498 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1); -- Small Brown Pouch
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3498 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3498 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3498 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=8957 AND `ExtendedCost`=0 AND `type`=1); -- Spinefin Halibut
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=21552 AND `ExtendedCost`=0 AND `type`=1); -- Striped Yellowtail
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=4594 AND `ExtendedCost`=0 AND `type`=1); -- Rockscale Cod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=4593 AND `ExtendedCost`=0 AND `type`=1); -- Bristle Whisker Catfish
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=4592 AND `ExtendedCost`=0 AND `type`=1); -- Longjaw Mud Snapper
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=6791 AND `item`=787 AND `ExtendedCost`=0 AND `type`=1); -- Slitherskin Mackerel
UPDATE `npc_vendor` SET `slot`=526, `VerifiedBuild`=23420 WHERE (`entry`=3572 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=525, `VerifiedBuild`=23420 WHERE (`entry`=3572 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=524, `VerifiedBuild`=23420 WHERE (`entry`=3572 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=522, `VerifiedBuild`=23420 WHERE (`entry`=3572 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2030 AND `ExtendedCost`=0 AND `type`=1); -- Gnarled Staff
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2208 AND `ExtendedCost`=0 AND `type`=1); -- Poniard
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2026 AND `ExtendedCost`=0 AND `type`=1); -- Rock Hammer
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2028 AND `ExtendedCost`=0 AND `type`=1); -- Hammer
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2025 AND `ExtendedCost`=0 AND `type`=1); -- Bearded Axe
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2029 AND `ExtendedCost`=0 AND `type`=1); -- Cleaver
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2024 AND `ExtendedCost`=0 AND `type`=1); -- Espadon
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3491 AND `item`=2027 AND `ExtendedCost`=0 AND `type`=1); -- Scimitar
UPDATE `npc_vendor` SET `slot`=1759, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=6275 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Greater Adept's Robe
UPDATE `npc_vendor` SET `slot`=1758, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=6272 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Linen Robe
UPDATE `npc_vendor` SET `slot`=1757, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=20855 AND `ExtendedCost`=0 AND `type`=1); -- Design: Wicked Moonstone Ring
UPDATE `npc_vendor` SET `slot`=1753, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=5640 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Rage Potion
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3499 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=194, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=4777 AND `ExtendedCost`=0 AND `type`=1); -- Ironwood Maul
UPDATE `npc_vendor` SET `slot`=193, `maxcount`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=4778 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Spiked Mace
UPDATE `npc_vendor` SET `slot`=192, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=858 AND `ExtendedCost`=0 AND `type`=1); -- Lesser Healing Potion
UPDATE `npc_vendor` SET `slot`=191, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=2455 AND `ExtendedCost`=0 AND `type`=1); -- Minor Mana Potion
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=854 AND `ExtendedCost`=0 AND `type`=1); -- Quarter Staff
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=2207 AND `ExtendedCost`=0 AND `type`=1); -- Jambiya
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=1197 AND `ExtendedCost`=0 AND `type`=1); -- Giant Mace
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=852 AND `ExtendedCost`=0 AND `type`=1); -- Mace
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=1196 AND `ExtendedCost`=0 AND `type`=1); -- Tabar
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=853 AND `ExtendedCost`=0 AND `type`=1); -- Hatchet
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=1198 AND `ExtendedCost`=0 AND `type`=1); -- Claymore
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=851 AND `ExtendedCost`=0 AND `type`=1); -- Cutlass
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=8953 AND `ExtendedCost`=0 AND `type`=1); -- Deep Fried Plantains
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4602 AND `ExtendedCost`=0 AND `type`=1); -- Moon Harvest Pumpkin
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4539 AND `ExtendedCost`=0 AND `type`=1); -- Goldenbark Apple
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4538 AND `ExtendedCost`=0 AND `type`=1); -- Snapvine Watermelon
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4537 AND `ExtendedCost`=0 AND `type`=1); -- Tel'Abim Banana
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4536 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Red Apple
UPDATE `npc_vendor` SET `slot`=730, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=5772 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Red Woolen Bag
UPDATE `npc_vendor` SET `slot`=729, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=6270 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Linen Vest
UPDATE `npc_vendor` SET `slot`=728, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=6272 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Linen Robe
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=10290 AND `ExtendedCost`=0 AND `type`=1); -- Pink Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=6261 AND `ExtendedCost`=0 AND `type`=1); -- Orange Dye
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1); -- Green Dye
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=38426 AND `ExtendedCost`=0 AND `type`=1); -- Eternium Thread
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3485 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=114, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=4786 AND `ExtendedCost`=0 AND `type`=1); -- Wise Man's Belt
UPDATE `npc_vendor` SET `slot`=113, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=4781 AND `ExtendedCost`=0 AND `type`=1); -- Whispering Vest
UPDATE `npc_vendor` SET `slot`=112, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=4790 AND `ExtendedCost`=0 AND `type`=1); -- Inferno Cloak
UPDATE `npc_vendor` SET `slot`=111, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=4793 AND `ExtendedCost`=0 AND `type`=1); -- Sylvan Cloak
UPDATE `npc_vendor` SET `slot`=110, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=4792 AND `ExtendedCost`=0 AND `type`=1); -- Spirit Cloak
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=839 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Weave Gloves
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=3590 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Weave Bracers
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=840 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Weave Shoes
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=838 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Weave Pants
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=3589 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Weave Belt
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=837 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Weave Armor
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=16060 AND `ExtendedCost`=0 AND `type`=1); -- Common White Shirt
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=3428 AND `ExtendedCost`=0 AND `type`=1); -- Common Gray Shirt
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3486 AND `item`=16059 AND `ExtendedCost`=0 AND `type`=1); -- Common Brown Shirt
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3487 AND `item`=4497 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Brown Bag
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3487 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1); -- Brown Leather Satchel
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3487 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1); -- Small Brown Pouch
UPDATE `npc_vendor` SET `slot`=131, `VerifiedBuild`=23420 WHERE (`entry`=3490 AND `item`=765 AND `ExtendedCost`=0 AND `type`=1); -- Silverleaf
UPDATE `npc_vendor` SET `slot`=130, `maxcount`=1, `VerifiedBuild`=23420 WHERE (`entry`=3490 AND `item`=2447 AND `ExtendedCost`=0 AND `type`=1); -- Peacebloom
UPDATE `npc_vendor` SET `slot`=129, `VerifiedBuild`=23420 WHERE (`entry`=3490 AND `item`=6053 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Holy Protection Potion
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=854 AND `ExtendedCost`=0 AND `type`=1); -- Quarter Staff
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=2207 AND `ExtendedCost`=0 AND `type`=1); -- Jambiya
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=1197 AND `ExtendedCost`=0 AND `type`=1); -- Giant Mace
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=852 AND `ExtendedCost`=0 AND `type`=1); -- Mace
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=1196 AND `ExtendedCost`=0 AND `type`=1); -- Tabar
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=853 AND `ExtendedCost`=0 AND `type`=1); -- Hatchet
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=1198 AND `ExtendedCost`=0 AND `type`=1); -- Claymore
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3479 AND `item`=851 AND `ExtendedCost`=0 AND `type`=1); -- Cutlass
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3477 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3477 AND `item`=18567 AND `ExtendedCost`=0 AND `type`=1); -- Elemental Flux
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3477 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3477 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3477 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3477 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3488 AND `item`=2511 AND `ExtendedCost`=0 AND `type`=1); -- Hunter's Boomstick
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3488 AND `item`=2509 AND `ExtendedCost`=0 AND `type`=1); -- Ornate Blunderbuss
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3488 AND `item`=3026 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Bow
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3488 AND `item`=2507 AND `ExtendedCost`=0 AND `type`=1); -- Laminated Recurve Bow
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=850 AND `ExtendedCost`=0 AND `type`=1); -- Chainmail Gloves
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=1846 AND `ExtendedCost`=0 AND `type`=1); -- Chainmail Bracers
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=849 AND `ExtendedCost`=0 AND `type`=1); -- Chainmail Boots
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=848 AND `ExtendedCost`=0 AND `type`=1); -- Chainmail Pants
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=1845 AND `ExtendedCost`=0 AND `type`=1); -- Chainmail Belt
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=847 AND `ExtendedCost`=0 AND `type`=1); -- Chainmail Armor
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=844 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Gloves
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=1844 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Bracers
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=843 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Boots
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=845 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Pants
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=1843 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Belt
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3483 AND `item`=846 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Jerkin
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3481 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1); -- Red Ribboned Wrapping Paper
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3481 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1); -- Simple Wood
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3481 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1); -- Brown Leather Satchel
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3481 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1); -- Small Brown Pouch
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3481 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3481 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=5488 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Crispy Lizard Tail
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=5486 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Strider Stew
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1); -- Green Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3482 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=5871 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=5871 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=5871 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=5871 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=5871 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=5871 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=8953 AND `ExtendedCost`=0 AND `type`=1); -- Deep Fried Plantains
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4602 AND `ExtendedCost`=0 AND `type`=1); -- Moon Harvest Pumpkin
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4539 AND `ExtendedCost`=0 AND `type`=1); -- Goldenbark Apple
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4538 AND `ExtendedCost`=0 AND `type`=1); -- Snapvine Watermelon
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4537 AND `ExtendedCost`=0 AND `type`=1); -- Tel'Abim Banana
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3934 AND `item`=4536 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Red Apple
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3480 AND `item`=8950 AND `ExtendedCost`=0 AND `type`=1); -- Homemade Cherry Pie
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3480 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1); -- Soft Banana Bread
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3480 AND `item`=4544 AND `ExtendedCost`=0 AND `type`=1); -- Mulgore Spice Bread
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3480 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1); -- Moist Cornbread
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3480 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1); -- Freshly Baked Bread
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3480 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1); -- Tough Hunk of Bread
UPDATE `npc_vendor` SET `slot`=93, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=4766 AND `ExtendedCost`=0 AND `type`=1); -- Feral Blade
UPDATE `npc_vendor` SET `slot`=92, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=4765 AND `ExtendedCost`=0 AND `type`=1); -- Enamelled Broadsword
UPDATE `npc_vendor` SET `slot`=91, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=858 AND `ExtendedCost`=0 AND `type`=1); -- Lesser Healing Potion
UPDATE `npc_vendor` SET `slot`=90, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=2455 AND `ExtendedCost`=0 AND `type`=1); -- Minor Mana Potion
UPDATE `npc_vendor` SET `slot`=89, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=4777 AND `ExtendedCost`=0 AND `type`=1); -- Ironwood Maul
UPDATE `npc_vendor` SET `slot`=88, `maxcount`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=4778 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Spiked Mace
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=854 AND `ExtendedCost`=0 AND `type`=1); -- Quarter Staff
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=2207 AND `ExtendedCost`=0 AND `type`=1); -- Jambiya
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=1197 AND `ExtendedCost`=0 AND `type`=1); -- Giant Mace
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=852 AND `ExtendedCost`=0 AND `type`=1); -- Mace
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=1196 AND `ExtendedCost`=0 AND `type`=1); -- Tabar
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=853 AND `ExtendedCost`=0 AND `type`=1); -- Hatchet
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=1198 AND `ExtendedCost`=0 AND `type`=1); -- Claymore
UPDATE `npc_vendor` SET `slot`=1, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3658 AND `item`=851 AND `ExtendedCost`=0 AND `type`=1); -- Cutlass
UPDATE `npc_vendor` SET `PlayerConditionID`=12233, `VerifiedBuild`=23420 WHERE (`entry`=50323 AND `item`=64905 AND `ExtendedCost`=0 AND `type`=1); -- Bilgewater Shroud
UPDATE `npc_vendor` SET `PlayerConditionID`=12233, `VerifiedBuild`=23420 WHERE (`entry`=50323 AND `item`=64906 AND `ExtendedCost`=0 AND `type`=1); -- Bilgewater Cape
UPDATE `npc_vendor` SET `PlayerConditionID`=12233, `VerifiedBuild`=23420 WHERE (`entry`=50323 AND `item`=64907 AND `ExtendedCost`=0 AND `type`=1); -- Bilgewater Mantle
UPDATE `npc_vendor` SET `PlayerConditionID`=12232, `VerifiedBuild`=23420 WHERE (`entry`=50323 AND `item`=67525 AND `ExtendedCost`=0 AND `type`=1); -- Bilgewater Satchel
UPDATE `npc_vendor` SET `slot`=615, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=20856 AND `ExtendedCost`=0 AND `type`=1); -- Design: Heavy Golden Necklace of Battle
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3367 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3315 AND `item`=2618 AND `ExtendedCost`=0 AND `type`=1); -- Silver Dress Robes
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3315 AND `item`=2617 AND `ExtendedCost`=0 AND `type`=1); -- Burning Robes
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3315 AND `item`=2615 AND `ExtendedCost`=0 AND `type`=1); -- Chromatic Robe
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3315 AND `item`=2616 AND `ExtendedCost`=0 AND `type`=1); -- Shimmering Silk Robes
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3315 AND `item`=2614 AND `ExtendedCost`=0 AND `type`=1); -- Robe of Apprenticeship
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3315 AND `item`=2613 AND `ExtendedCost`=0 AND `type`=1); -- Double-Stitched Robes
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3315 AND `item`=2612 AND `ExtendedCost`=0 AND `type`=1); -- Plain Robe
UPDATE `npc_vendor` SET `slot`=281, `VerifiedBuild`=23420 WHERE (`entry`=3364 AND `item`=10314 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Lavender Mageweave Shirt
UPDATE `npc_vendor` SET `slot`=279, `VerifiedBuild`=23420 WHERE (`entry`=3364 AND `item`=5772 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Red Woolen Bag
UPDATE `npc_vendor` SET `slot`=278, `VerifiedBuild`=23420 WHERE (`entry`=3364 AND `item`=6274 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Overalls
UPDATE `npc_vendor` SET `slot`=276, `VerifiedBuild`=23420 WHERE (`entry`=3364 AND `item`=10317 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Pink Mageweave Shirt
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=44606 AND `ExtendedCost`=0 AND `type`=1); -- Toy Train Set
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=45057 AND `ExtendedCost`=0 AND `type`=1); -- Wind-Up Train Wrecker
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=44599 AND `ExtendedCost`=0 AND `type`=1); -- Zippy Copper Racer
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=44601 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Copper Racer
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=44482 AND `ExtendedCost`=0 AND `type`=1); -- Trusty Copper Racer
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=54438 AND `ExtendedCost`=0 AND `type`=1); -- Tiny Blue Ragdoll
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=54437 AND `ExtendedCost`=0 AND `type`=1); -- Tiny Green Ragdoll
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=34498 AND `ExtendedCost`=0 AND `type`=1); -- Paper Zeppelin Kit
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=44481 AND `ExtendedCost`=0 AND `type`=1); -- Grindgear Toy Gorilla
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=54343 AND `ExtendedCost`=0 AND `type`=1); -- Blue Crashin' Thrashin' Racer Controller
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=54436 AND `ExtendedCost`=0 AND `type`=1); -- Blue Clockwork Rocket Bot
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=48601 AND `ExtendedCost`=0 AND `type`=1); -- Red Rider Air Rifle Ammo
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=46725 AND `ExtendedCost`=0 AND `type`=1); -- Red Rider Air Rifle
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=95482 AND `ExtendedCost`=0 AND `type`=1); -- Red War Fuel
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=95481 AND `ExtendedCost`=0 AND `type`=1); -- Blue War Fuel
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=95621 AND `ExtendedCost`=0 AND `type`=1); -- Warbot Ignition Key
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=104324 AND `ExtendedCost`=0 AND `type`=1); -- Foot Ball
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=52809 AND `item`=104323 AND `ExtendedCost`=0 AND `type`=1); -- The Pigskin
UPDATE `npc_vendor` SET `slot`=1658, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=1657, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=18647 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Red Firework
UPDATE `npc_vendor` SET `slot`=1650, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=1649, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=1648, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4361 AND `ExtendedCost`=0 AND `type`=1); -- Bent Copper Tube
UPDATE `npc_vendor` SET `slot`=1647, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=1646, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=1645, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=1644, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=1643, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=1642, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=1641, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=1639, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1638, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=15904 AND `ExtendedCost`=0 AND `type`=1); -- Deadly Fist Blades
UPDATE `npc_vendor` SET `slot`=15, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=15903 AND `ExtendedCost`=0 AND `type`=1); -- Slicing Claw
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=15905 AND `ExtendedCost`=0 AND `type`=1); -- Brass Knuckles
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2534 AND `ExtendedCost`=0 AND `type`=1); -- Rondel
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2526 AND `ExtendedCost`=0 AND `type`=1); -- Main Gauche
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2528 AND `ExtendedCost`=0 AND `type`=1); -- Falchion
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2520 AND `ExtendedCost`=0 AND `type`=1); -- Broadsword
UPDATE `npc_vendor` SET `slot`=9, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=923 AND `ExtendedCost`=0 AND `type`=1); -- Longsword
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=922 AND `ExtendedCost`=0 AND `type`=1); -- Dacian Falx
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2209 AND `ExtendedCost`=0 AND `type`=1); -- Kris
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2024 AND `ExtendedCost`=0 AND `type`=1); -- Espadon
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2208 AND `ExtendedCost`=0 AND `type`=1); -- Poniard
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2027 AND `ExtendedCost`=0 AND `type`=1); -- Scimitar
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=2207 AND `ExtendedCost`=0 AND `type`=1); -- Jambiya
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=1198 AND `ExtendedCost`=0 AND `type`=1); -- Claymore
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3361 AND `item`=851 AND `ExtendedCost`=0 AND `type`=1); -- Cutlass
UPDATE `npc_vendor` SET `slot`=9, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=924 AND `ExtendedCost`=0 AND `type`=1); -- Maul
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=928 AND `ExtendedCost`=0 AND `type`=1); -- Long Staff
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=925 AND `ExtendedCost`=0 AND `type`=1); -- Flail
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=2026 AND `ExtendedCost`=0 AND `type`=1); -- Rock Hammer
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=2028 AND `ExtendedCost`=0 AND `type`=1); -- Hammer
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=2030 AND `ExtendedCost`=0 AND `type`=1); -- Gnarled Staff
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=854 AND `ExtendedCost`=0 AND `type`=1); -- Quarter Staff
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=1197 AND `ExtendedCost`=0 AND `type`=1); -- Giant Mace
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3360 AND `item`=852 AND `ExtendedCost`=0 AND `type`=1); -- Mace
UPDATE `npc_vendor` SET `slot`=12, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=928 AND `ExtendedCost`=0 AND `type`=1); -- Long Staff
UPDATE `npc_vendor` SET `slot`=11, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=2030 AND `ExtendedCost`=0 AND `type`=1); -- Gnarled Staff
UPDATE `npc_vendor` SET `slot`=10, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=854 AND `ExtendedCost`=0 AND `type`=1); -- Quarter Staff
UPDATE `npc_vendor` SET `slot`=9, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=2495 AND `ExtendedCost`=0 AND `type`=1); -- Walking Stick
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=3026 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Bow
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=2507 AND `ExtendedCost`=0 AND `type`=1); -- Laminated Recurve Bow
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=926 AND `ExtendedCost`=0 AND `type`=1); -- Battle Axe
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=2025 AND `ExtendedCost`=0 AND `type`=1); -- Bearded Axe
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=1196 AND `ExtendedCost`=0 AND `type`=1); -- Tabar
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=927 AND `ExtendedCost`=0 AND `type`=1); -- Double Axe
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=2029 AND `ExtendedCost`=0 AND `type`=1); -- Cleaver
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3409 AND `item`=853 AND `ExtendedCost`=0 AND `type`=1); -- Hatchet
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2451 AND `ExtendedCost`=0 AND `type`=1); -- Crested Heater Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=17190 AND `ExtendedCost`=0 AND `type`=1); -- Ornate Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62259 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Helm
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62257 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62256 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62255 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62258 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62254 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62253 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Armor
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62266 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Circlet
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62263 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62262 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62261 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62264 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62260 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=62265 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=718 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=1852 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=287 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=286 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=1853 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=285 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2446 AND `ExtendedCost`=0 AND `type`=1); -- Kite Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=17192 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Targe
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2151 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2150 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2149 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2152 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2148 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5812 AND `item`=2153 AND `ExtendedCost`=0 AND `type`=1); -- Polished Scale Vest
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3358 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1); -- Jeweler's Kit
UPDATE `npc_vendor` SET `slot`=91, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52466 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Eye of Many Deaths
UPDATE `npc_vendor` SET `slot`=90, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52464 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Brazen Elementium Medallion
UPDATE `npc_vendor` SET `slot`=89, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52465 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Entwined Elementium Choker
UPDATE `npc_vendor` SET `slot`=88, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52467 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Elementium Guardian
UPDATE `npc_vendor` SET `slot`=87, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52462 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Ring of Warring Elements
UPDATE `npc_vendor` SET `slot`=86, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52463 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Elementium Moebius Band
UPDATE `npc_vendor` SET `slot`=85, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52461 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Band of Blades
UPDATE `npc_vendor` SET `slot`=84, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52460 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Elementium Destroyer's Ring
UPDATE `npc_vendor` SET `slot`=83, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=69853 AND `ExtendedCost`=2973 AND `type`=1); -- Design: Punisher's Band
UPDATE `npc_vendor` SET `slot`=82, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52440 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Revitalizing Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=81, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52442 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Powerful Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=80, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52444 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Impassive Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=79, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52445 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Forlorn Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=78, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52433 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Fleet Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=77, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52436 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Eternal Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=76, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52443 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Enigmatic Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=75, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52439 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Ember Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=74, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52438 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Effulgent Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=73, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52441 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Destructive Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=72, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52434 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Chaotic Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=71, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52435 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Bracing Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=70, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52437 AND `ExtendedCost`=2971 AND `type`=1); -- Design: Austere Shadowspirit Diamond
UPDATE `npc_vendor` SET `slot`=69, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52392 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Stormy Ocean Sapphire
UPDATE `npc_vendor` SET `slot`=68, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52391 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Sparkling Ocean Sapphire
UPDATE `npc_vendor` SET `slot`=67, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52390 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Solid Ocean Sapphire
UPDATE `npc_vendor` SET `slot`=66, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52393 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Rigid Ocean Sapphire
UPDATE `npc_vendor` SET `slot`=65, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52389 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Precise Inferno Ruby
UPDATE `npc_vendor` SET `slot`=64, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52384 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Flashing Inferno Ruby
UPDATE `npc_vendor` SET `slot`=63, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52380 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Delicate Inferno Ruby
UPDATE `npc_vendor` SET `slot`=62, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52387 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Brilliant Inferno Ruby
UPDATE `npc_vendor` SET `slot`=61, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52362 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Bold Inferno Ruby
UPDATE `npc_vendor` SET `slot`=60, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=68359 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Willful Ember Topaz
UPDATE `npc_vendor` SET `slot`=59, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=68361 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Resplendent Ember Topaz
UPDATE `npc_vendor` SET `slot`=58, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52411 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Resolute Ember Topaz
UPDATE `npc_vendor` SET `slot`=57, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52418 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Skillful Ember Topaz
UPDATE `npc_vendor` SET `slot`=56, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52414 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Potent Ember Topaz
UPDATE `npc_vendor` SET `slot`=55, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52410 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Polished Ember Topaz
UPDATE `npc_vendor` SET `slot`=54, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=68360 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Lucent Ember Topaz
UPDATE `npc_vendor` SET `slot`=53, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52422 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Keen Ember Topaz
UPDATE `npc_vendor` SET `slot`=52, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52412 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Inscribed Ember Topaz
UPDATE `npc_vendor` SET `slot`=51, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52420 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Fine Ember Topaz
UPDATE `npc_vendor` SET `slot`=50, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52415 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Fierce Ember Topaz
UPDATE `npc_vendor` SET `slot`=49, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52416 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Deft Ember Topaz
UPDATE `npc_vendor` SET `slot`=48, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52413 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Deadly Ember Topaz
UPDATE `npc_vendor` SET `slot`=47, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52417 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Reckless Ember Topaz
UPDATE `npc_vendor` SET `slot`=46, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52421 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Artful Ember Topaz
UPDATE `npc_vendor` SET `slot`=45, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52419 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Adept Ember Topaz
UPDATE `npc_vendor` SET `slot`=44, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52431 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Zen Dream Emerald
UPDATE `npc_vendor` SET `slot`=43, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=68742 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Vivid Dream Emerald
UPDATE `npc_vendor` SET `slot`=42, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52427 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Steady Dream Emerald
UPDATE `npc_vendor` SET `slot`=41, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52432 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Sensei's Dream Emerald
UPDATE `npc_vendor` SET `slot`=40, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52423 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Regal Dream Emerald
UPDATE `npc_vendor` SET `slot`=39, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52430 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Puissant Dream Emerald
UPDATE `npc_vendor` SET `slot`=38, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52426 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Piercing Dream Emerald
UPDATE `npc_vendor` SET `slot`=37, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52424 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Nimble Dream Emerald
UPDATE `npc_vendor` SET `slot`=36, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52429 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Lightning Dream Emerald
UPDATE `npc_vendor` SET `slot`=35, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52425 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Jagged Dream Emerald
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52428 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Forceful Dream Emerald
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52402 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Timeless Demonseye
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52399 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Sovereign Demonseye
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52400 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Shifting Demonseye
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52404 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Purified Demonseye
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52407 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Retaliating Demonseye
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52403 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Guardian's Demonseye
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52406 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Glinting Demonseye
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52408 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Veiled Demonseye
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52405 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Etched Demonseye
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52401 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Defender's Demonseye
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52409 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Accurate Demonseye
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52394 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Subtle Amberjewel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52395 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Smooth Amberjewel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52397 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Quick Amberjewel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52396 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Mystic Amberjewel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52398 AND `ExtendedCost`=2970 AND `type`=1); -- Design: Fractured Amberjewel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52455 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Subtle Chimera's Eye
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52453 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Stormy Chimera's Eye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52452 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Sparkling Chimera's Eye
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52451 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Solid Chimera's Eye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52456 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Smooth Chimera's Eye
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52454 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Rigid Chimera's Eye
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52458 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Quick Chimera's Eye
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52450 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Precise Chimera's Eye
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52457 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Mystic Chimera's Eye
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52459 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Fractured Chimera's Eye
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52448 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Flashing Chimera's Eye
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52447 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Delicate Chimera's Eye
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52449 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Brilliant Chimera's Eye
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52381 AND `ExtendedCost`=2972 AND `type`=1); -- Design: Bold Chimera's Eye
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52196 AND `ExtendedCost`=3232 AND `type`=1); -- Chimera's Eye
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=50482 AND `item`=52188 AND `ExtendedCost`=0 AND `type`=1); -- Jeweler's Setting
UPDATE `npc_vendor` SET `slot`=474, `maxcount`=1, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6533 AND `ExtendedCost`=0 AND `type`=1); -- Aquadynamic Fish Attractor
UPDATE `npc_vendor` SET `slot`=473, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=472, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=471, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=469, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6365 AND `ExtendedCost`=0 AND `type`=1); -- Strong Fishing Pole
UPDATE `npc_vendor` SET `slot`=468, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=17062 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Mithril Head Trout
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6369 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Rockscale Cod
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3333 AND `item`=6368 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Rainbow Fin Albacore
UPDATE `npc_vendor` SET `slot`=38, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=58257 AND `ExtendedCost`=0 AND `type`=1); -- Highland Spring Water
UPDATE `npc_vendor` SET `slot`=37, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=58256 AND `ExtendedCost`=0 AND `type`=1); -- Sparkling Oasis Water
UPDATE `npc_vendor` SET `slot`=36, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=33445 AND `ExtendedCost`=0 AND `type`=1); -- Honeymint Tea
UPDATE `npc_vendor` SET `slot`=35, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=33444 AND `ExtendedCost`=0 AND `type`=1); -- Pungent Seal Whey
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=58274 AND `ExtendedCost`=0 AND `type`=1); -- Fresh Water
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=28399 AND `ExtendedCost`=0 AND `type`=1); -- Filtered Draenic Water
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=58263 AND `ExtendedCost`=0 AND `type`=1); -- Grilled Shark
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=58262 AND `ExtendedCost`=0 AND `type`=1); -- Sliced Raw Billfish
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=35951 AND `ExtendedCost`=0 AND `type`=1); -- Poached Emperor Salmon
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=33451 AND `ExtendedCost`=0 AND `type`=1); -- Fillet of Icefin
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=27858 AND `ExtendedCost`=0 AND `type`=1); -- Sunspring Carp
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=8957 AND `ExtendedCost`=0 AND `type`=1); -- Spinefin Halibut
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=21552 AND `ExtendedCost`=0 AND `type`=1); -- Striped Yellowtail
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=4594 AND `ExtendedCost`=0 AND `type`=1); -- Rockscale Cod
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=4593 AND `ExtendedCost`=0 AND `type`=1); -- Bristle Whisker Catfish
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=4592 AND `ExtendedCost`=0 AND `type`=1); -- Longjaw Mud Snapper
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=46642 AND `item`=787 AND `ExtendedCost`=0 AND `type`=1); -- Slitherskin Mackerel
UPDATE `npc_vendor` SET `slot`=41, `VerifiedBuild`=23420 WHERE (`entry`=3348 AND `item`=13478 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Elixir of Superior Defense
UPDATE `npc_vendor` SET `slot`=582, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=3335 AND `item`=5643 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Great Rage Potion
UPDATE `npc_vendor` SET `slot`=581, `maxcount`=1, `VerifiedBuild`=23420 WHERE (`entry`=3335 AND `item`=5640 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Rage Potion
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3330 AND `item`=2535 AND `ExtendedCost`=0 AND `type`=1); -- War Staff
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3330 AND `item`=2527 AND `ExtendedCost`=0 AND `type`=1); -- Battle Staff
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5239 AND `ExtendedCost`=0 AND `type`=1); -- Blackbone Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5238 AND `ExtendedCost`=0 AND `type`=1); -- Pitchwood Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5347 AND `ExtendedCost`=0 AND `type`=1); -- Pestilent Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5236 AND `ExtendedCost`=0 AND `type`=1); -- Combustible Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5211 AND `ExtendedCost`=0 AND `type`=1); -- Dusk Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5210 AND `ExtendedCost`=0 AND `type`=1); -- Burning Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5209 AND `ExtendedCost`=0 AND `type`=1); -- Gloom Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=5816 AND `item`=5208 AND `ExtendedCost`=0 AND `type`=1); -- Smoldering Wand
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2534 AND `ExtendedCost`=0 AND `type`=1); -- Rondel
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2526 AND `ExtendedCost`=0 AND `type`=1); -- Main Gauche
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2528 AND `ExtendedCost`=0 AND `type`=1); -- Falchion
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2520 AND `ExtendedCost`=0 AND `type`=1); -- Broadsword
UPDATE `npc_vendor` SET `slot`=9, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=923 AND `ExtendedCost`=0 AND `type`=1); -- Longsword
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=922 AND `ExtendedCost`=0 AND `type`=1); -- Dacian Falx
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2209 AND `ExtendedCost`=0 AND `type`=1); -- Kris
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2024 AND `ExtendedCost`=0 AND `type`=1); -- Espadon
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2208 AND `ExtendedCost`=0 AND `type`=1); -- Poniard
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2027 AND `ExtendedCost`=0 AND `type`=1); -- Scimitar
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=2207 AND `ExtendedCost`=0 AND `type`=1); -- Jambiya
UPDATE `npc_vendor` SET `slot`=2, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=1198 AND `ExtendedCost`=0 AND `type`=1); -- Claymore
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3331 AND `item`=851 AND `ExtendedCost`=0 AND `type`=1); -- Cutlass
UPDATE `npc_vendor` SET `slot`=1312, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=6270 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Linen Vest
UPDATE `npc_vendor` SET `slot`=1311, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=6274 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Overalls
UPDATE `npc_vendor` SET `slot`=1310, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=10317 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Pink Mageweave Shirt
UPDATE `npc_vendor` SET `slot`=1309, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=10314 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Lavender Mageweave Shirt
UPDATE `npc_vendor` SET `slot`=1308, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=5772 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Red Woolen Bag
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1); -- Green Dye
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1); -- Virtuoso Inking Set
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=1571, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=1569, `maxcount`=4, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=1568, `maxcount`=4, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=1567, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=4361 AND `ExtendedCost`=0 AND `type`=1); -- Bent Copper Tube
UPDATE `npc_vendor` SET `slot`=1566, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=1565, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=1564, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=1563, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=1562, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=1561, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=1560, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=1558, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1557, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=45546 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1); -- Green Dye
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1); -- Virtuoso Inking Set
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=45552 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=38, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=58269 AND `ExtendedCost`=0 AND `type`=1); -- Massive Turkey Leg
UPDATE `npc_vendor` SET `slot`=37, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=58268 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Beef
UPDATE `npc_vendor` SET `slot`=36, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=35953 AND `ExtendedCost`=0 AND `type`=1); -- Mead Basted Caribou
UPDATE `npc_vendor` SET `slot`=35, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=33454 AND `ExtendedCost`=0 AND `type`=1); -- Salted Venison
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=27854 AND `ExtendedCost`=0 AND `type`=1); -- Smoked Talbuk Venison
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=58261 AND `ExtendedCost`=0 AND `type`=1); -- Buttery Wheat Roll
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=58260 AND `ExtendedCost`=0 AND `type`=1); -- Pine Nut Bread
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=35950 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Potato Bread
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=27855 AND `ExtendedCost`=0 AND `type`=1); -- Mag'har Grainbread
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=8950 AND `ExtendedCost`=0 AND `type`=1); -- Homemade Cherry Pie
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1); -- Soft Banana Bread
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=4544 AND `ExtendedCost`=0 AND `type`=1); -- Mulgore Spice Bread
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1); -- Moist Cornbread
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1); -- Freshly Baked Bread
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=45563 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1); -- Tough Hunk of Bread
UPDATE `npc_vendor` SET `slot`=1312, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=6270 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Linen Vest
UPDATE `npc_vendor` SET `slot`=1311, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=6274 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Blue Overalls
UPDATE `npc_vendor` SET `slot`=1310, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=10317 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Pink Mageweave Shirt
UPDATE `npc_vendor` SET `slot`=1309, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=10314 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Lavender Mageweave Shirt
UPDATE `npc_vendor` SET `slot`=1308, `VerifiedBuild`=23420 WHERE (`entry`=45558 AND `item`=5772 AND `ExtendedCost`=0 AND `type`=1); -- Pattern: Red Woolen Bag
UPDATE `npc_vendor` SET `slot`=4969, `maxcount`=2, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=39489 AND `ExtendedCost`=0 AND `type`=1); -- Scribe's Satchel
UPDATE `npc_vendor` SET `slot`=4968, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1); -- Virtuoso Inking Set
UPDATE `npc_vendor` SET `slot`=4967, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=4966, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1); -- Vanishing Powder
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=61981 AND `ExtendedCost`=2583 AND `type`=1); -- Inferno Ink
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=43127 AND `ExtendedCost`=2583 AND `type`=1); -- Snowfall Ink
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=43126 AND `ExtendedCost`=2582 AND `type`=1); -- Ink of the Sea
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=43124 AND `ExtendedCost`=2582 AND `type`=1); -- Ethereal Ink
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=43122 AND `ExtendedCost`=2582 AND `type`=1); -- Shimmering Ink
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=43120 AND `ExtendedCost`=2582 AND `type`=1); -- Celestial Ink
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=43118 AND `ExtendedCost`=2582 AND `type`=1); -- Jadefire Ink
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=43116 AND `ExtendedCost`=2582 AND `type`=1); -- Lion's Ink
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=39774 AND `ExtendedCost`=2582 AND `type`=1); -- Midnight Ink
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=30723 AND `item`=39469 AND `ExtendedCost`=2582 AND `type`=1); -- Moonglow Ink
UPDATE `npc_vendor` SET `slot`=442, `VerifiedBuild`=23420 WHERE (`entry`=44975 AND `item`=6533 AND `ExtendedCost`=0 AND `type`=1); -- Aquadynamic Fish Attractor
UPDATE `npc_vendor` SET `slot`=441, `VerifiedBuild`=23420 WHERE (`entry`=44975 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=440, `VerifiedBuild`=23420 WHERE (`entry`=44975 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=439, `VerifiedBuild`=23420 WHERE (`entry`=44975 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=437, `VerifiedBuild`=23420 WHERE (`entry`=44975 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=38, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=58265 AND `ExtendedCost`=0 AND `type`=1); -- Highland Pomegranate
UPDATE `npc_vendor` SET `slot`=37, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=58264 AND `ExtendedCost`=0 AND `type`=1); -- Sour Green Apple
UPDATE `npc_vendor` SET `slot`=36, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=35948 AND `ExtendedCost`=0 AND `type`=1); -- Savory Snowplum
UPDATE `npc_vendor` SET `slot`=35, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=35949 AND `ExtendedCost`=0 AND `type`=1); -- Tundra Berries
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=27856 AND `ExtendedCost`=0 AND `type`=1); -- Skethyl Berries
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=8953 AND `ExtendedCost`=0 AND `type`=1); -- Deep Fried Plantains
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4602 AND `ExtendedCost`=0 AND `type`=1); -- Moon Harvest Pumpkin
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4539 AND `ExtendedCost`=0 AND `type`=1); -- Goldenbark Apple
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4538 AND `ExtendedCost`=0 AND `type`=1); -- Snapvine Watermelon
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4537 AND `ExtendedCost`=0 AND `type`=1); -- Tel'Abim Banana
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4536 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Red Apple
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=58263 AND `ExtendedCost`=0 AND `type`=1); -- Grilled Shark
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=58262 AND `ExtendedCost`=0 AND `type`=1); -- Sliced Raw Billfish
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=35951 AND `ExtendedCost`=0 AND `type`=1); -- Poached Emperor Salmon
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=33451 AND `ExtendedCost`=0 AND `type`=1); -- Fillet of Icefin
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=27858 AND `ExtendedCost`=0 AND `type`=1); -- Sunspring Carp
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=8957 AND `ExtendedCost`=0 AND `type`=1); -- Spinefin Halibut
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=21552 AND `ExtendedCost`=0 AND `type`=1); -- Striped Yellowtail
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4594 AND `ExtendedCost`=0 AND `type`=1); -- Rockscale Cod
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4593 AND `ExtendedCost`=0 AND `type`=1); -- Bristle Whisker Catfish
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=4592 AND `ExtendedCost`=0 AND `type`=1); -- Longjaw Mud Snapper
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=45008 AND `item`=787 AND `ExtendedCost`=0 AND `type`=1); -- Slitherskin Mackerel
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=45093 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=43, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=58257 AND `ExtendedCost`=0 AND `type`=1); -- Highland Spring Water
UPDATE `npc_vendor` SET `slot`=42, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=58256 AND `ExtendedCost`=0 AND `type`=1); -- Sparkling Oasis Water
UPDATE `npc_vendor` SET `slot`=41, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=33445 AND `ExtendedCost`=0 AND `type`=1); -- Honeymint Tea
UPDATE `npc_vendor` SET `slot`=40, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=33444 AND `ExtendedCost`=0 AND `type`=1); -- Pungent Seal Whey
UPDATE `npc_vendor` SET `slot`=39, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=58274 AND `ExtendedCost`=0 AND `type`=1); -- Fresh Water
UPDATE `npc_vendor` SET `slot`=38, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=28399 AND `ExtendedCost`=0 AND `type`=1); -- Filtered Draenic Water
UPDATE `npc_vendor` SET `slot`=37, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=36, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=35, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=58263 AND `ExtendedCost`=0 AND `type`=1); -- Grilled Shark
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=58262 AND `ExtendedCost`=0 AND `type`=1); -- Sliced Raw Billfish
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=35951 AND `ExtendedCost`=0 AND `type`=1); -- Poached Emperor Salmon
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=33451 AND `ExtendedCost`=0 AND `type`=1); -- Fillet of Icefin
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=27858 AND `ExtendedCost`=0 AND `type`=1); -- Sunspring Carp
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=8957 AND `ExtendedCost`=0 AND `type`=1); -- Spinefin Halibut
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=21552 AND `ExtendedCost`=0 AND `type`=1); -- Striped Yellowtail
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=4594 AND `ExtendedCost`=0 AND `type`=1); -- Rockscale Cod
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=4593 AND `ExtendedCost`=0 AND `type`=1); -- Bristle Whisker Catfish
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=4592 AND `ExtendedCost`=0 AND `type`=1); -- Longjaw Mud Snapper
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=45086 AND `item`=787 AND `ExtendedCost`=0 AND `type`=1); -- Slitherskin Mackerel
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=38, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=58261 AND `ExtendedCost`=0 AND `type`=1); -- Buttery Wheat Roll
UPDATE `npc_vendor` SET `slot`=37, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=58260 AND `ExtendedCost`=0 AND `type`=1); -- Pine Nut Bread
UPDATE `npc_vendor` SET `slot`=36, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=35950 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Potato Bread
UPDATE `npc_vendor` SET `slot`=35, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=27855 AND `ExtendedCost`=0 AND `type`=1); -- Mag'har Grainbread
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=8950 AND `ExtendedCost`=0 AND `type`=1); -- Homemade Cherry Pie
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1); -- Soft Banana Bread
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=4544 AND `ExtendedCost`=0 AND `type`=1); -- Mulgore Spice Bread
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1); -- Moist Cornbread
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1); -- Freshly Baked Bread
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1); -- Tough Hunk of Bread
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=58259 AND `ExtendedCost`=0 AND `type`=1); -- Highland Sheep Cheese
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=58258 AND `ExtendedCost`=0 AND `type`=1); -- Smoked String Cheese
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=35952 AND `ExtendedCost`=0 AND `type`=1); -- Briny Hardcheese
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=33443 AND `ExtendedCost`=0 AND `type`=1); -- Sour Goat Cheese
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=27857 AND `ExtendedCost`=0 AND `type`=1); -- Garadar Sharp
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=8932 AND `ExtendedCost`=0 AND `type`=1); -- Alterac Swiss
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=3927 AND `ExtendedCost`=0 AND `type`=1); -- Fine Aged Cheddar
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=1707 AND `ExtendedCost`=0 AND `type`=1); -- Stormwind Brie
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=422 AND `ExtendedCost`=0 AND `type`=1); -- Dwarven Mild
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=414 AND `ExtendedCost`=0 AND `type`=1); -- Dalaran Sharp
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=44785 AND `item`=2070 AND `ExtendedCost`=0 AND `type`=1); -- Darnassian Bleu
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3322 AND `item`=3027 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Recurve Bow
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3322 AND `item`=3026 AND `ExtendedCost`=0 AND `type`=1); -- Reinforced Bow
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3322 AND `item`=3025 AND `ExtendedCost`=0 AND `type`=1); -- BKP 42 "Ultra"
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3322 AND `item`=3024 AND `ExtendedCost`=0 AND `type`=1); -- BKP 2700 "Enforcer"
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3322 AND `item`=3023 AND `ExtendedCost`=0 AND `type`=1); -- Large Bore Blunderbuss
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3322 AND `item`=2511 AND `ExtendedCost`=0 AND `type`=1); -- Hunter's Boomstick
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3322 AND `item`=2509 AND `ExtendedCost`=0 AND `type`=1); -- Ornate Blunderbuss
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=44780 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=61981 AND `ExtendedCost`=2583 AND `type`=1); -- Inferno Ink
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=43127 AND `ExtendedCost`=2583 AND `type`=1); -- Snowfall Ink
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=43126 AND `ExtendedCost`=2582 AND `type`=1); -- Ink of the Sea
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=43124 AND `ExtendedCost`=2582 AND `type`=1); -- Ethereal Ink
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=43122 AND `ExtendedCost`=2582 AND `type`=1); -- Shimmering Ink
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=43120 AND `ExtendedCost`=2582 AND `type`=1); -- Celestial Ink
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=43118 AND `ExtendedCost`=2582 AND `type`=1); -- Jadefire Ink
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=43116 AND `ExtendedCost`=2582 AND `type`=1); -- Lion's Ink
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=39774 AND `ExtendedCost`=2582 AND `type`=1); -- Midnight Ink
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=52032 AND `item`=39469 AND `ExtendedCost`=2582 AND `type`=1); -- Moonglow Ink
UPDATE `npc_vendor` SET `slot`=4975, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=39489 AND `ExtendedCost`=0 AND `type`=1); -- Scribe's Satchel
UPDATE `npc_vendor` SET `slot`=4974, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1); -- Virtuoso Inking Set
UPDATE `npc_vendor` SET `slot`=4973, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=4972, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=64670 AND `ExtendedCost`=0 AND `type`=1); -- Vanishing Powder
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=61981 AND `ExtendedCost`=2583 AND `type`=1); -- Inferno Ink
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=43127 AND `ExtendedCost`=2583 AND `type`=1); -- Snowfall Ink
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=43126 AND `ExtendedCost`=2582 AND `type`=1); -- Ink of the Sea
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=43124 AND `ExtendedCost`=2582 AND `type`=1); -- Ethereal Ink
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=43122 AND `ExtendedCost`=2582 AND `type`=1); -- Shimmering Ink
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=43120 AND `ExtendedCost`=2582 AND `type`=1); -- Celestial Ink
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=43118 AND `ExtendedCost`=2582 AND `type`=1); -- Jadefire Ink
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=43116 AND `ExtendedCost`=2582 AND `type`=1); -- Lion's Ink
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=39774 AND `ExtendedCost`=2582 AND `type`=1); -- Midnight Ink
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=46718 AND `item`=39469 AND `ExtendedCost`=2582 AND `type`=1); -- Moonglow Ink
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=58261 AND `ExtendedCost`=0 AND `type`=1); -- Buttery Wheat Roll
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=58260 AND `ExtendedCost`=0 AND `type`=1); -- Pine Nut Bread
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=35950 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Potato Bread
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=27855 AND `ExtendedCost`=0 AND `type`=1); -- Mag'har Grainbread
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=8950 AND `ExtendedCost`=0 AND `type`=1); -- Homemade Cherry Pie
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1); -- Soft Banana Bread
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=4544 AND `ExtendedCost`=0 AND `type`=1); -- Mulgore Spice Bread
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1); -- Moist Cornbread
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1); -- Freshly Baked Bread
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3368 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1); -- Tough Hunk of Bread
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=58269 AND `ExtendedCost`=0 AND `type`=1); -- Massive Turkey Leg
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=58268 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Beef
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=35953 AND `ExtendedCost`=0 AND `type`=1); -- Mead Basted Caribou
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=33454 AND `ExtendedCost`=0 AND `type`=1); -- Salted Venison
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=27854 AND `ExtendedCost`=0 AND `type`=1); -- Smoked Talbuk Venison
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=5611 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=38, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=58257 AND `ExtendedCost`=0 AND `type`=1); -- Highland Spring Water
UPDATE `npc_vendor` SET `slot`=37, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=58256 AND `ExtendedCost`=0 AND `type`=1); -- Sparkling Oasis Water
UPDATE `npc_vendor` SET `slot`=36, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=33445 AND `ExtendedCost`=0 AND `type`=1); -- Honeymint Tea
UPDATE `npc_vendor` SET `slot`=35, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=33444 AND `ExtendedCost`=0 AND `type`=1); -- Pungent Seal Whey
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=58274 AND `ExtendedCost`=0 AND `type`=1); -- Fresh Water
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=28399 AND `ExtendedCost`=0 AND `type`=1); -- Filtered Draenic Water
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1); -- Morning Glory Dew
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1); -- Moonberry Juice
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Nectar
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1); -- Melon Juice
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1); -- Ice Cold Milk
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=58261 AND `ExtendedCost`=0 AND `type`=1); -- Buttery Wheat Roll
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=58260 AND `ExtendedCost`=0 AND `type`=1); -- Pine Nut Bread
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=35950 AND `ExtendedCost`=0 AND `type`=1); -- Sweet Potato Bread
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=27855 AND `ExtendedCost`=0 AND `type`=1); -- Mag'har Grainbread
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=8950 AND `ExtendedCost`=0 AND `type`=1); -- Homemade Cherry Pie
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1); -- Soft Banana Bread
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=4544 AND `ExtendedCost`=0 AND `type`=1); -- Mulgore Spice Bread
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1); -- Moist Cornbread
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1); -- Freshly Baked Bread
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=6929 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1); -- Tough Hunk of Bread
UPDATE `npc_vendor` SET `slot`=24, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=854 AND `ExtendedCost`=0 AND `type`=1); -- Quarter Staff
UPDATE `npc_vendor` SET `slot`=23, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2207 AND `ExtendedCost`=0 AND `type`=1); -- Jambiya
UPDATE `npc_vendor` SET `slot`=22, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=1197 AND `ExtendedCost`=0 AND `type`=1); -- Giant Mace
UPDATE `npc_vendor` SET `slot`=21, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=852 AND `ExtendedCost`=0 AND `type`=1); -- Mace
UPDATE `npc_vendor` SET `slot`=20, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=1196 AND `ExtendedCost`=0 AND `type`=1); -- Tabar
UPDATE `npc_vendor` SET `slot`=19, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=853 AND `ExtendedCost`=0 AND `type`=1); -- Hatchet
UPDATE `npc_vendor` SET `slot`=18, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=1198 AND `ExtendedCost`=0 AND `type`=1); -- Claymore
UPDATE `npc_vendor` SET `slot`=17, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=851 AND `ExtendedCost`=0 AND `type`=1); -- Cutlass
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2493 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Mallet
UPDATE `npc_vendor` SET `slot`=15, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2490 AND `ExtendedCost`=0 AND `type`=1); -- Tomahawk
UPDATE `npc_vendor` SET `slot`=14, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2488 AND `ExtendedCost`=0 AND `type`=1); -- Gladius
UPDATE `npc_vendor` SET `slot`=13, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2495 AND `ExtendedCost`=0 AND `type`=1); -- Walking Stick
UPDATE `npc_vendor` SET `slot`=12, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2494 AND `ExtendedCost`=0 AND `type`=1); -- Stiletto
UPDATE `npc_vendor` SET `slot`=11, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2491 AND `ExtendedCost`=0 AND `type`=1); -- Large Axe
UPDATE `npc_vendor` SET `slot`=10, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2492 AND `ExtendedCost`=0 AND `type`=1); -- Cudgel
UPDATE `npc_vendor` SET `slot`=9, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3314 AND `item`=2489 AND `ExtendedCost`=0 AND `type`=1); -- Two-Handed Sword
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=2445 AND `ExtendedCost`=0 AND `type`=1); -- Large Metal Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17188 AND `ExtendedCost`=0 AND `type`=1); -- Ringed Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=718 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1852 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=287 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=286 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1853 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=285 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1202 AND `ExtendedCost`=0 AND `type`=1); -- Wall Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17187 AND `ExtendedCost`=0 AND `type`=1); -- Banded Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=2376 AND `ExtendedCost`=0 AND `type`=1); -- Worn Heater Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17185 AND `ExtendedCost`=0 AND `type`=1); -- Round Buckler
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=46358 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1); -- Jeweler's Kit
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=2445 AND `ExtendedCost`=0 AND `type`=1); -- Large Metal Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17188 AND `ExtendedCost`=0 AND `type`=1); -- Ringed Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=718 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1852 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=287 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=286 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1853 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=285 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1202 AND `ExtendedCost`=0 AND `type`=1); -- Wall Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17187 AND `ExtendedCost`=0 AND `type`=1); -- Banded Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=2376 AND `ExtendedCost`=0 AND `type`=1); -- Worn Heater Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17185 AND `ExtendedCost`=0 AND `type`=1); -- Round Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=2451 AND `ExtendedCost`=0 AND `type`=1); -- Crested Heater Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=17190 AND `ExtendedCost`=0 AND `type`=1); -- Ornate Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62259 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Helm
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62257 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62256 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62255 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62258 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62254 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62253 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Plate Armor
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62266 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Circlet
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62263 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62262 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62261 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62264 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Leggings
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62260 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=46512 AND `item`=62265 AND `ExtendedCost`=0 AND `type`=1); -- Grunt's Chain Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=2445 AND `ExtendedCost`=0 AND `type`=1); -- Large Metal Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17188 AND `ExtendedCost`=0 AND `type`=1); -- Ringed Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=718 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1852 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=287 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=286 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1853 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=285 AND `ExtendedCost`=0 AND `type`=1); -- Scalemail Vest
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=1202 AND `ExtendedCost`=0 AND `type`=1); -- Wall Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17187 AND `ExtendedCost`=0 AND `type`=1); -- Banded Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=2376 AND `ExtendedCost`=0 AND `type`=1); -- Worn Heater Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3319 AND `item`=17185 AND `ExtendedCost`=0 AND `type`=1); -- Round Buckler
UPDATE `npc_vendor` SET `slot`=34, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1); -- Gray Dye
UPDATE `npc_vendor` SET `slot`=33, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1); -- Purple Dye
UPDATE `npc_vendor` SET `slot`=32, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1); -- Yellow Dye
UPDATE `npc_vendor` SET `slot`=31, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1); -- Black Dye
UPDATE `npc_vendor` SET `slot`=30, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1); -- Rune Thread
UPDATE `npc_vendor` SET `slot`=29, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Silken Thread
UPDATE `npc_vendor` SET `slot`=28, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=27, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=26, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=25, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1); -- Coal
UPDATE `npc_vendor` SET `slot`=24, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=23, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1); -- Silken Thread
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1); -- Green Dye
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1); -- Fine Thread
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1); -- Virtuoso Inking Set
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=5817 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3166 AND `item`=2375 AND `ExtendedCost`=0 AND `type`=1); -- Battered Leather Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3166 AND `item`=2374 AND `ExtendedCost`=0 AND `type`=1); -- Battered Leather Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3166 AND `item`=2373 AND `ExtendedCost`=0 AND `type`=1); -- Battered Leather Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3166 AND `item`=2372 AND `ExtendedCost`=0 AND `type`=1); -- Battered Leather Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3166 AND `item`=2371 AND `ExtendedCost`=0 AND `type`=1); -- Battered Leather Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3166 AND `item`=2370 AND `ExtendedCost`=0 AND `type`=1); -- Battered Leather Harness
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3168 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=10, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2493 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Mallet
UPDATE `npc_vendor` SET `slot`=9, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2490 AND `ExtendedCost`=0 AND `type`=1); -- Tomahawk
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2488 AND `ExtendedCost`=0 AND `type`=1); -- Gladius
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2495 AND `ExtendedCost`=0 AND `type`=1); -- Walking Stick
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2494 AND `ExtendedCost`=0 AND `type`=1); -- Stiletto
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2491 AND `ExtendedCost`=0 AND `type`=1); -- Large Axe
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2492 AND `ExtendedCost`=0 AND `type`=1); -- Cudgel
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2489 AND `ExtendedCost`=0 AND `type`=1); -- Two-Handed Sword
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2507 AND `ExtendedCost`=0 AND `type`=1); -- Laminated Recurve Bow
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2506 AND `ExtendedCost`=0 AND `type`=1); -- Hornwood Recurve Bow
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1); -- Red Ribboned Wrapping Paper
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1); -- Simple Wood
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1); -- Brown Leather Satchel
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1); -- Small Brown Pouch
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=39033 AND `item`=714 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=39033 AND `item`=1836 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=39033 AND `item`=210 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=39033 AND `item`=209 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=39033 AND `item`=1835 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=39033 AND `item`=85 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Vest
UPDATE `npc_vendor` SET `slot`=451, `maxcount`=1, `VerifiedBuild`=23420 WHERE (`entry`=5942 AND `item`=6533 AND `ExtendedCost`=0 AND `type`=1); -- Aquadynamic Fish Attractor
UPDATE `npc_vendor` SET `slot`=450, `VerifiedBuild`=23420 WHERE (`entry`=5942 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1); -- Bright Baubles
UPDATE `npc_vendor` SET `slot`=449, `VerifiedBuild`=23420 WHERE (`entry`=5942 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1); -- Nightcrawlers
UPDATE `npc_vendor` SET `slot`=448, `VerifiedBuild`=23420 WHERE (`entry`=5942 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=446, `VerifiedBuild`=23420 WHERE (`entry`=5942 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=5942 AND `item`=6368 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Rainbow Fin Albacore
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=5942 AND `item`=6326 AND `ExtendedCost`=0 AND `type`=1); -- Recipe: Slitherskin Mackerel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=10, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2493 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Mallet
UPDATE `npc_vendor` SET `slot`=9, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2490 AND `ExtendedCost`=0 AND `type`=1); -- Tomahawk
UPDATE `npc_vendor` SET `slot`=8, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2488 AND `ExtendedCost`=0 AND `type`=1); -- Gladius
UPDATE `npc_vendor` SET `slot`=7, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2495 AND `ExtendedCost`=0 AND `type`=1); -- Walking Stick
UPDATE `npc_vendor` SET `slot`=6, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2494 AND `ExtendedCost`=0 AND `type`=1); -- Stiletto
UPDATE `npc_vendor` SET `slot`=5, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2491 AND `ExtendedCost`=0 AND `type`=1); -- Large Axe
UPDATE `npc_vendor` SET `slot`=4, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2492 AND `ExtendedCost`=0 AND `type`=1); -- Cudgel
UPDATE `npc_vendor` SET `slot`=3, `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2489 AND `ExtendedCost`=0 AND `type`=1); -- Two-Handed Sword
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2507 AND `ExtendedCost`=0 AND `type`=1); -- Laminated Recurve Bow
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=10369 AND `item`=2506 AND `ExtendedCost`=0 AND `type`=1); -- Hornwood Recurve Bow
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1); -- Light Parchment
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1); -- Blue Dye
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1); -- Bleach
UPDATE `npc_vendor` SET `slot`=14, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1); -- Red Dye
UPDATE `npc_vendor` SET `slot`=13, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1); -- Shiny Bauble
UPDATE `npc_vendor` SET `slot`=12, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1); -- Salt
UPDATE `npc_vendor` SET `slot`=11, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1); -- Crystal Vial
UPDATE `npc_vendor` SET `slot`=10, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=9, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Thread
UPDATE `npc_vendor` SET `slot`=8, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1); -- Simple Flour
UPDATE `npc_vendor` SET `slot`=7, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1); -- Mild Spices
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1); -- Copper Rod
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1); -- Fishing Pole
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3187 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1); -- Skinning Knife
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1); -- Red Ribboned Wrapping Paper
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1); -- Simple Wood
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1); -- Brown Leather Satchel
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1); -- Small Brown Pouch
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3186 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1); -- Refreshing Spring Water
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3933 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3933 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3933 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3933 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3933 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3933 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3161 AND `item`=1200 AND `ExtendedCost`=0 AND `type`=1); -- Large Wooden Shield
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3161 AND `item`=17183 AND `ExtendedCost`=0 AND `type`=1); -- Dented Buckler
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3160 AND `item`=714 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Gloves
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3160 AND `item`=1836 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Bracers
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3160 AND `item`=210 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Boots
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3160 AND `item`=209 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Pants
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3160 AND `item`=1835 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Belt
UPDATE `npc_vendor` SET `IgnoreFiltering`=1, `VerifiedBuild`=23420 WHERE (`entry`=3160 AND `item`=85 AND `ExtendedCost`=0 AND `type`=1); -- Dirty Leather Vest
UPDATE `npc_vendor` SET `slot`=22 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=22 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=10380 AND `item`=844 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Gloves
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=10380 AND `item`=1844 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Bracers
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=10380 AND `item`=843 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Boots
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=10380 AND `item`=845 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Pants
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=10380 AND `item`=1843 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Belt
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=10380 AND `item`=846 AND `ExtendedCost`=0 AND `type`=1); -- Tanned Leather Jerkin
UPDATE `npc_vendor` SET `slot`=6, `VerifiedBuild`=23420 WHERE (`entry`=3705 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1); -- Roasted Quail
UPDATE `npc_vendor` SET `slot`=5, `VerifiedBuild`=23420 WHERE (`entry`=3705 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1); -- Cured Ham Steak
UPDATE `npc_vendor` SET `slot`=4, `VerifiedBuild`=23420 WHERE (`entry`=3705 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1); -- Wild Hog Shank
UPDATE `npc_vendor` SET `slot`=3, `VerifiedBuild`=23420 WHERE (`entry`=3705 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1); -- Mutton Chop
UPDATE `npc_vendor` SET `slot`=2, `VerifiedBuild`=23420 WHERE (`entry`=3705 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1); -- Haunch of Meat
UPDATE `npc_vendor` SET `slot`=1, `VerifiedBuild`=23420 WHERE (`entry`=3705 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1); -- Tough Jerky
UPDATE `npc_vendor` SET `slot`=118, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=117, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=18647 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Red Firework
UPDATE `npc_vendor` SET `slot`=107, `maxcount`=4, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=106, `maxcount`=3, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=105, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4389 AND `ExtendedCost`=0 AND `type`=1); -- Gyrochronatom
UPDATE `npc_vendor` SET `slot`=104, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=103, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=102, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=101, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=100, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=99, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=98, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=96, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=95, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel
UPDATE `npc_vendor` SET `slot`=118, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=22729 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Steam Tonk Controller
UPDATE `npc_vendor` SET `slot`=117, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=18647 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Red Firework
UPDATE `npc_vendor` SET `slot`=107, `maxcount`=4, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=106, `maxcount`=3, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1); -- Rough Blasting Powder
UPDATE `npc_vendor` SET `slot`=105, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4389 AND `ExtendedCost`=0 AND `type`=1); -- Gyrochronatom
UPDATE `npc_vendor` SET `slot`=104, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1); -- Engineer's Ink
UPDATE `npc_vendor` SET `slot`=103, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1); -- Strong Flux
UPDATE `npc_vendor` SET `slot`=102, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1); -- Weak Flux
UPDATE `npc_vendor` SET `slot`=101, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1); -- Hair Trigger
UPDATE `npc_vendor` SET `slot`=100, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1); -- Walnut Stock
UPDATE `npc_vendor` SET `slot`=99, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1); -- Heavy Stock
UPDATE `npc_vendor` SET `slot`=98, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1); -- Wooden Stock
UPDATE `npc_vendor` SET `slot`=96, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1); -- Mining Pick
UPDATE `npc_vendor` SET `slot`=95, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1); -- Blacksmith Hammer
UPDATE `npc_vendor` SET `slot`=22, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59489 AND `ExtendedCost`=3310 AND `type`=1); -- Precise Cogwheel
UPDATE `npc_vendor` SET `slot`=21, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59477 AND `ExtendedCost`=3392 AND `type`=1); -- Subtle Cogwheel
UPDATE `npc_vendor` SET `slot`=20, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59478 AND `ExtendedCost`=3311 AND `type`=1); -- Smooth Cogwheel
UPDATE `npc_vendor` SET `slot`=19, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59479 AND `ExtendedCost`=3311 AND `type`=1); -- Quick Cogwheel
UPDATE `npc_vendor` SET `slot`=18, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59480 AND `ExtendedCost`=3308 AND `type`=1); -- Fractured Cogwheel
UPDATE `npc_vendor` SET `slot`=17, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59491 AND `ExtendedCost`=3305 AND `type`=1); -- Flashing Cogwheel
UPDATE `npc_vendor` SET `slot`=16, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59493 AND `ExtendedCost`=3307 AND `type`=1); -- Rigid Cogwheel
UPDATE `npc_vendor` SET `slot`=15, `VerifiedBuild`=23420 WHERE (`entry`=3413 AND `item`=59496 AND `ExtendedCost`=3306 AND `type`=1); -- Sparkling Cogwheel

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=116762 AND `ID`=1) OR (`CreatureID`=116761 AND `ID`=1) OR (`CreatureID`=116830 AND `ID`=1) OR (`CreatureID`=39129 AND `ID`=1) OR (`CreatureID`=39124 AND `ID`=1) OR (`CreatureID`=37511 AND `ID`=2) OR (`CreatureID`=75687 AND `ID`=2) OR (`CreatureID`=86884 AND `ID`=1) OR (`CreatureID`=112398 AND `ID`=1) OR (`CreatureID`=45830 AND `ID`=1) OR (`CreatureID`=11681 AND `ID`=2) OR (`CreatureID`=116621 AND `ID`=1) OR (`CreatureID`=103760 AND `ID`=1) OR (`CreatureID`=99552 AND `ID`=1) OR (`CreatureID`=99556 AND `ID`=1) OR (`CreatureID`=58464 AND `ID`=2) OR (`CreatureID`=76089 AND `ID`=1) OR (`CreatureID`=74551 AND `ID`=1) OR (`CreatureID`=75823 AND `ID`=1) OR (`CreatureID`=75821 AND `ID`=1) OR (`CreatureID`=74948 AND `ID`=1) OR (`CreatureID`=75110 AND `ID`=1) OR (`CreatureID`=75152 AND `ID`=1) OR (`CreatureID`=75662 AND `ID`=1) OR (`CreatureID`=76090 AND `ID`=1) OR (`CreatureID`=76495 AND `ID`=1) OR (`CreatureID`=76494 AND `ID`=1) OR (`CreatureID`=74462 AND `ID`=1) OR (`CreatureID`=75439 AND `ID`=3) OR (`CreatureID`=74900 AND `ID`=1) OR (`CreatureID`=75055 AND `ID`=1) OR (`CreatureID`=75439 AND `ID`=2) OR (`CreatureID`=75666 AND `ID`=1) OR (`CreatureID`=75429 AND `ID`=1) OR (`CreatureID`=75822 AND `ID`=1) OR (`CreatureID`=74545 AND `ID`=1) OR (`CreatureID`=74554 AND `ID`=1) OR (`CreatureID`=75439 AND `ID`=1) OR (`CreatureID`=74555 AND `ID`=1) OR (`CreatureID`=74550 AND `ID`=1) OR (`CreatureID`=74549 AND `ID`=1) OR (`CreatureID`=74553 AND `ID`=1) OR (`CreatureID`=75114 AND `ID`=1) OR (`CreatureID`=75437 AND `ID`=1) OR (`CreatureID`=33334 AND `ID`=4) OR (`CreatureID`=33334 AND `ID`=3) OR (`CreatureID`=33334 AND `ID`=2) OR (`CreatureID`=33334 AND `ID`=1) OR (`CreatureID`=76039 AND `ID`=1) OR (`CreatureID`=76038 AND `ID`=1) OR (`CreatureID`=76036 AND `ID`=1) OR (`CreatureID`=76037 AND `ID`=1) OR (`CreatureID`=74518 AND `ID`=1) OR (`CreatureID`=75285 AND `ID`=1) OR (`CreatureID`=75058 AND `ID`=1) OR (`CreatureID`=74505 AND `ID`=1) OR (`CreatureID`=74353 AND `ID`=1) OR (`CreatureID`=74363 AND `ID`=1) OR (`CreatureID`=75012 AND `ID`=1) OR (`CreatureID`=75011 AND `ID`=1) OR (`CreatureID`=74382 AND `ID`=1) OR (`CreatureID`=74351 AND `ID`=1) OR (`CreatureID`=74984 AND `ID`=1) OR (`CreatureID`=74988 AND `ID`=1) OR (`CreatureID`=74983 AND `ID`=1) OR (`CreatureID`=75643 AND `ID`=1) OR (`CreatureID`=75642 AND `ID`=1) OR (`CreatureID`=75641 AND `ID`=1) OR (`CreatureID`=75640 AND `ID`=1) OR (`CreatureID`=74565 AND `ID`=1) OR (`CreatureID`=74409 AND `ID`=1) OR (`CreatureID`=74408 AND `ID`=1) OR (`CreatureID`=74721 AND `ID`=1) OR (`CreatureID`=74720 AND `ID`=1) OR (`CreatureID`=74989 AND `ID`=1) OR (`CreatureID`=74380 AND `ID`=1) OR (`CreatureID`=74476 AND `ID`=1) OR (`CreatureID`=77232 AND `ID`=1) OR (`CreatureID`=75286 AND `ID`=1) OR (`CreatureID`=74980 AND `ID`=1) OR (`CreatureID`=3848 AND `ID`=1) OR (`CreatureID`=24927 AND `ID`=1) OR (`CreatureID`=71000 AND `ID`=2) OR (`CreatureID`=71000 AND `ID`=1) OR (`CreatureID`=34749 AND `ID`=2) OR (`CreatureID`=3382 AND `ID`=2) OR (`CreatureID`=3383 AND `ID`=2) OR (`CreatureID`=115286 AND `ID`=1) OR (`CreatureID`=47232 AND `ID`=2) OR (`CreatureID`=61644 AND `ID`=1) OR (`CreatureID`=71012 AND `ID`=2) OR (`CreatureID`=75687 AND `ID`=1) OR (`CreatureID`=75686 AND `ID`=1) OR (`CreatureID`=75685 AND `ID`=1) OR (`CreatureID`=38217 AND `ID`=2);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(116762, 1, 124548, 0, 34590), -- Auserw�hlte von Eyir
(116761, 1, 109641, 0, 0), -- Hymdall
(116830, 1, 140345, 0, 143547), -- Hyrja
(39129, 1, 1902, 0, 0), -- Peasant Laborer
(39124, 1, 1902, 0, 0), -- Mizzy's Apprentice
(37511, 2, 1901, 0, 0), -- Bristleback Bladewarden
(75687, 2, 85764, 0, 0), -- Thunder Bluff Protector
(86884, 1, 13631, 0, 0), -- Darkspear Loyalist
(112398, 1, 127648, 0, 0), -- Maglothar
(45830, 1, 1117, 0, 0), -- Off-Duty Siegeworker
(11681, 2, 12801, 0, 0), -- Warsong Logger
(116621, 1, 5597, 0, 5597), -- -Unknown-
(103760, 1, 128360, 0, 128370), -- Ariana Fireheart
(99552, 1, 124002, 0, 126057), -- Dark Mistress Repentia
(99556, 1, 126793, 0, 0), -- Lady Elosar
(58464, 2, 12403, 0, 0), -- Golden Lotus Warrior
(76089, 1, 89779, 0, 0), -- Razorfen Beast Stalker
(74551, 1, 89779, 0, 0), -- Razorfen Beast Stalker
(75823, 1, 50141, 0, 95584), -- Razorfen Stonechanter
(75821, 1, 57008, 0, 0), -- Razorfen Scarblade
(74948, 1, 93368, 0, 0), -- Roogug
(75110, 1, 107254, 0, 0), -- Razorfen Torchbearer
(75152, 1, 76467, 0, 0), -- Death Speaker Jargba
(75662, 1, 50141, 0, 95584), -- Razorfen Stonechanter
(76090, 1, 42920, 0, 0), -- Blood-Branded Razorfen
(76495, 1, 50141, 0, 95584), -- Razorfen Stonechanter
(76494, 1, 107652, 0, 95584), -- Razorfen Rockscryer
(74462, 1, 76290, 0, 107351), -- Warlord Ramtusk
(75439, 3, 37924, 0, 0), -- Novice Quilboar
(74900, 1, 53100, 0, 0), -- Kraulshaper Tukaar
(75055, 1, 107254, 0, 0), -- Razorfen Torchbearer
(75439, 2, 17040, 0, 0), -- Novice Quilboar
(75666, 1, 107652, 0, 95584), -- Razorfen Rockscryer
(75429, 1, 17040, 0, 81320), -- Quilboar Mealtender
(75822, 1, 89779, 0, 0), -- Razorfen Beast Stalker
(74545, 1, 11588, 0, 0), -- Razorfen Geomagus
(74554, 1, 57008, 0, 0), -- Razorfen Scarblade
(75439, 1, 3351, 0, 0), -- Novice Quilboar
(74555, 1, 93023, 0, 93024), -- Razorfen Hidecrusher
(74550, 1, 106872, 0, 0), -- Razorfen Kraulshaper
(74549, 1, 50141, 0, 95584), -- Razorfen Stonechanter
(74553, 1, 42920, 0, 0), -- Blood-Branded Razorfen
(75114, 1, 106872, 0, 0), -- Razorfen Kraulshaper
(75437, 1, 77426, 0, 0), -- Quilboar Broodmentor
(33334, 4, 18062, 0, 0), -- Horde Invader
(33334, 3, 14877, 0, 0), -- Horde Invader
(33334, 2, 17383, 0, 0), -- Horde Invader
(33334, 1, 10898, 0, 12456), -- Horde Invader
(76039, 1, 50998, 0, 0), -- Twilight Aquamancer
(76038, 1, 3227, 0, 0), -- Twilight Shadowmage
(76036, 1, 2018, 0, 0), -- Twilight Shadow
(76037, 1, 2176, 0, 0), -- Twilight Storm Mender
(74518, 1, 55411, 0, 0), -- Executioner Gore
(75285, 1, 2176, 0, 0), -- Twilight Storm Mender
(75058, 1, 50998, 0, 0), -- Twilight Aquamancer
(74505, 1, 36684, 0, 0), -- Thruk
(74353, 1, 50998, 0, 0), -- Twilight Aquamancer
(74363, 1, 2018, 0, 0), -- Twilight Shadow
(75012, 1, 55959, 0, 0), -- Twilight Disciple
(75011, 1, 2176, 0, 0), -- Twilight Storm Mender
(74382, 1, 3227, 0, 0), -- Twilight Shadowmage
(74351, 1, 55959, 0, 0), -- Twilight Disciple
(74984, 1, 52962, 0, 52962), -- Blindlight Murloc
(74988, 1, 55411, 0, 0), -- Executioner Gore
(74983, 1, 55959, 0, 0), -- Twilight Disciple
(75643, 1, 52962, 0, 0), -- Blindlight Murloc
(75642, 1, 36606, 0, 0), -- Blindlight Bilefin
(75641, 1, 36606, 0, 0), -- Blindlight Bilefin
(75640, 1, 52962, 0, 0), -- Blindlight Murloc
(74565, 1, 29355, 0, 0), -- Subjugator Kor'ul
(74409, 1, 52962, 0, 0), -- Zeya
(74408, 1, 36606, 0, 0), -- Je'neu Sancrea
(74721, 1, 36606, 0, 0), -- Blindlight Bilefin
(74720, 1, 52962, 0, 52962), -- Blindlight Murloc
(74989, 1, 55959, 0, 0), -- Twilight Disciple
(74380, 1, 2176, 0, 0), -- Twilight Storm Mender
(74476, 1, 78477, 0, 0), -- Domina
(77232, 1, 55959, 0, 0), -- Twilight Disciple
(75286, 1, 55959, 0, 0), -- Twilight Disciple
(74980, 1, 2176, 0, 0), -- Twilight Storm Mender
(3848, 1, 3699, 0, 12870), -- Kayneth Stillwind
(24927, 1, 2716, 0, 0), -- Navigator Sparksizzle
(71000, 2, 1903, 0, 0), -- Mercenary Engineer
(71000, 1, 31824, 0, 0), -- Mercenary Engineer
(34749, 2, 2147, 0, 0), -- Tony Two-Tusk
(3382, 2, 1911, 0, 0), -- Southsea Cannoneer
(3383, 2, 1900, 0, 0), -- Southsea Cutthroat
(115286, 1, 18805, 0, 19346), -- Crysa
(47232, 2, 25646, 0, 0), -- Ghostly Cook
(61644, 1, 80057, 0, 0), -- Dark Shaman Researcher
(71012, 2, 7005, 0, 0), -- Kor'kron Butcher
(75687, 1, 57127, 0, 0), -- Thunder Bluff Protector
(75686, 1, 13631, 0, 0), -- Darkspear Guardian
(75685, 1, 5289, 0, 0), -- Razor Hill Grunt
(38217, 2, 28914, 0, 0); -- Darkspear Watcher

UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1985 WHERE (`CreatureID`=14323 AND `ID`=1); -- Guard Slip'kik
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=18293 WHERE (`CreatureID`=11486 AND `ID`=1); -- Prince Tortheldrin
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1897 WHERE (`CreatureID`=11469 AND `ID`=1); -- Eldreth Seether
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2052 WHERE (`CreatureID`=14321 AND `ID`=1); -- Guard Fengus
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=11488 AND `ID`=1); -- Illyanna Ravenoak
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3375 AND `ID`=1); -- Bael'dun Foreman
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=39153 AND `ID`=1); -- Excavation Raider
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3377 AND `ID`=1); -- Bael'dun Rifleman
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=38290 AND `ID`=1); -- Barrens Brave
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=51852 AND `ID`=1); -- Triumph Sentry
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=39136 AND `ID`=1); -- Triumph Sentry
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=39139 AND `ID`=1); -- Triumph Rifleman
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=33272 WHERE (`CreatureID`=38015 AND `ID`=1); -- Karthog
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=49340 WHERE (`CreatureID`=58722 AND `ID`=1); -- Lilian Voss
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=49340 WHERE (`CreatureID`=59200 AND `ID`=1); -- Lilian Voss
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=75039 WHERE (`CreatureID`=59368 AND `ID`=1); -- Krastinovian Carver
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=51851 AND `ID`=1); -- Desolation Guard
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=39660 WHERE (`CreatureID`=37926 AND `ID`=1); -- Triumph Captain
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=38070 AND `ID`=1); -- Desolation Guard
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=37977 AND `ID`=1); -- Wildhammer Mercenary
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=39660 WHERE (`CreatureID`=37923 AND `ID`=1); -- Triumph Vanguard
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=37922 AND `ID`=1); -- Desolation Raider
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=5285 WHERE (`CreatureID`=37743 AND `ID`=1); -- Taurajo Looter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1985 WHERE (`CreatureID`=37513 AND `ID`=1); -- Sabersnout
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2052 WHERE (`CreatureID`=37706 AND `ID`=1); -- Desolation Grunt
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=37585 AND `ID`=1); -- Northwatch Recon
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=4849 AND `ID`=1); -- Shadowforge Archaeologist
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=7290 AND `ID`=1); -- Shadowforge Sharpshooter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13922 WHERE (`CreatureID`=6908 AND `ID`=1); -- Olaf
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=6906 AND `ID`=1); -- Baelog
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=47287 WHERE (`CreatureID`=64587 AND `ID`=1); -- Horde Peacekeeper
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2052 WHERE (`CreatureID`=14326 AND `ID`=1); -- Guard Mol'dar
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10619 WHERE (`CreatureID`=11452 AND `ID`=1); -- Wildspawn Rogue
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=11454 AND `ID`=1); -- Wildspawn Betrayer
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34848 AND `ID`=1); -- Honor's Stand Sharpshooter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=37180 AND `ID`=1); -- Honor's Stand Guard
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1985 WHERE (`CreatureID`=37153 AND `ID`=1); -- Holgom
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=37204 AND `ID`=1); -- Hunter Hill Brave
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=37161 AND `ID`=1); -- Honor's Stand Footman
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2053 WHERE (`CreatureID`=37170 AND `ID`=1); -- Hunter Hill Scout
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=37220 AND `ID`=1); -- Una'fe Watcher
UPDATE `creature_equip_template` SET `ItemID1`=2716, `ItemID3`=0 WHERE (`CreatureID`=38875 AND `ID`=1); -- Ol' Durty Pete
UPDATE `creature_equip_template` SET `ItemID1`=1905 WHERE (`CreatureID`=19444 AND `ID`=1); -- Peasant Worker
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=3695 WHERE (`CreatureID`=44307 AND `ID`=1); -- Cranston Fizzlespit
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=51861 AND `ID`=1); -- Northwatch Guard
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=44268 AND `ID`=1); -- Keep Watcher Kerry
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=38636 AND `ID`=1); -- Northwatch Guard
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2081 WHERE (`CreatureID`=3454 AND `ID`=1); -- Cannoneer Smythe
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=38624 AND `ID`=1); -- Northwatch Defender
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=33210 WHERE (`CreatureID`=38658 AND `ID`=1); -- Rageroar Grunt
UPDATE `creature_equip_template` SET `ItemID1`=1902, `ItemID3`=0 WHERE (`CreatureID`=74228 AND `ID`=2); -- Darkspear Headhunter
UPDATE `creature_equip_template` SET `ItemID1`=3367 WHERE (`CreatureID`=44158 AND `ID`=3); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID1`=1903 WHERE (`CreatureID`=44158 AND `ID`=2); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID1`=1902 WHERE (`CreatureID`=44158 AND `ID`=1); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12851 WHERE (`CreatureID`=45552 AND `ID`=1); -- Pezik Lockfast
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13611 WHERE (`CreatureID`=45549 AND `ID`=1); -- Zido Helmbreaker
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12855 WHERE (`CreatureID`=45553 AND `ID`=1); -- Denk Hordewell
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13859 WHERE (`CreatureID`=45565 AND `ID`=1); -- Sanzi
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13855 WHERE (`CreatureID`=45563 AND `ID`=1); -- Tinza Silvermug
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2711 WHERE (`CreatureID`=45717 AND `ID`=1); -- Vish the Sneak
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=45713 AND `ID`=1); -- Dankin Farsnipe
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=45827 AND `ID`=1); -- Off-Duty Grunt
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12332 WHERE (`CreatureID`=46078 AND `ID`=1); -- Boss Mida
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13859 WHERE (`CreatureID`=45566 AND `ID`=1); -- Tanzi
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=45822 AND `ID`=1); -- Off-Duty Bruiser
UPDATE `creature_equip_template` SET `ItemID1`=4993 WHERE (`CreatureID`=45138 AND `ID`=1); -- Unjari Feltongue
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13854 WHERE (`CreatureID`=45008 AND `ID`=1); -- Batamsi
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12870 WHERE (`CreatureID`=45093 AND `ID`=1); -- Huju
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12452 WHERE (`CreatureID`=17304 AND `ID`=1); -- Overseer Gorthak
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2809 WHERE (`CreatureID`=12737 AND `ID`=1); -- Mastok Wrilehiss
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33445 AND `ID`=1); -- Sentinel Avana
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=11806 AND `ID`=1); -- Sentinel Onaeya
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1985 WHERE (`CreatureID`=3932 AND `ID`=1); -- Bloodtooth Guard
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1957 WHERE (`CreatureID`=56894 AND `ID`=1); -- Splintertree Guard
UPDATE `creature_equip_template` SET `ItemID1`=6680 WHERE (`CreatureID`=58464 AND `ID`=1); -- Golden Lotus Warrior
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=73425 WHERE (`CreatureID`=64566 AND `ID`=1); -- Sunwalker Dezco
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12454 WHERE (`CreatureID`=34518 AND `ID`=1); -- Thagg
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=21551 WHERE (`CreatureID`=34603 AND `ID`=1); -- Ashenvale Assassin
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=11794 AND `ID`=1); -- Sister of Celebras
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=12243 AND `ID`=1); -- Spirit of Veng
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=12242 AND `ID`=1); -- Spirit of Maraudos
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10616 WHERE (`CreatureID`=12236 AND `ID`=1); -- Lord Vyletongue
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=11586 WHERE (`CreatureID`=13601 AND `ID`=1); -- Tinkerer Gizlock
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=11793 AND `ID`=1); -- Celebrian Dryad
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34494 AND `ID`=1); -- Astranaar Sentinel
UPDATE `creature_equip_template` SET `ItemID1`=107464 WHERE (`CreatureID`=4421 AND `ID`=1); -- Charlga Razorflank
UPDATE `creature_equip_template` SET `ItemID1`=90003, `ItemID3`=38635 WHERE (`CreatureID`=44402 AND `ID`=1); -- Auld Stonespire
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33454 AND `ID`=1); -- Sentinel Luara
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3691 AND `ID`=1); -- Raene Wolfrunner
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34420 AND `ID`=1); -- Astranaar Officer
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=56166 WHERE (`CreatureID`=34419 AND `ID`=1); -- Astranaar Skirmisher
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3926 AND `ID`=1); -- Thistlefur Pathfinder
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2183 WHERE (`CreatureID`=3925 AND `ID`=1); -- Thistlefur Avenger
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33278 AND `ID`=1); -- Maestra's Post Sentinel
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12993 WHERE (`CreatureID`=11137 AND `ID`=1); -- Xai'ander
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=6087 AND `ID`=1); -- Astranaar Sentinel
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10616 WHERE (`CreatureID`=3846 AND `ID`=1); -- Talen
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3845 AND `ID`=1); -- Shindrell Swiftfire
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=12863 AND `ID`=1); -- Warsong Runner
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=43653 AND `ID`=1); -- Hellscream Grunt
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=24738 AND `ID`=1); -- Elenna Edune
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12454 WHERE (`CreatureID`=34366 AND `ID`=1); -- Warsong Vanguard
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=8015 AND `ID`=1); -- Ashenvale Sentinel
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2081 WHERE (`CreatureID`=34591 AND `ID`=1); -- Chief Bombgineer Sploder
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=41870 AND `ID`=1); -- Krom'gar Enforcer
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=41865 AND `ID`=1); -- Darnassian Scout
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=41673 AND `ID`=1); -- Krom'gar Enforcer
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=39254 AND `ID`=1); -- Stardust Sentinel
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=52000 AND `ID`=1); -- Astranaar Sentinel
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=43657 AND `ID`=1); -- Silverwind Vanquisher
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=52161 AND `ID`=1); -- Foulweald Pathfinder
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3745 AND `ID`=1); -- Foulweald Pathfinder
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1957 WHERE (`CreatureID`=51814 AND `ID`=1); -- Splintertree Guard
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33945 AND `ID`=1); -- Ashenvale Assailant
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1957 WHERE (`CreatureID`=33944 AND `ID`=1); -- Splintertree Guard
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33914 AND `ID`=1); -- Ashenvale Bowman
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=51507 AND `ID`=1); -- Forest Song Peacekeeper
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1957 WHERE (`CreatureID`=34242 AND `ID`=1); -- Guardian Gurtar
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1957 WHERE (`CreatureID`=34241 AND `ID`=1); -- Warsong Guardian
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=12864 AND `ID`=1); -- Warsong Outrider
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34208 AND `ID`=1); -- Protector Endolar
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34204 AND `ID`=1); -- Protector Arminon
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=14874 WHERE (`CreatureID`=3752 AND `ID`=1); -- Xavian Rogue
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3763 AND `ID`=1); -- Felmusk Shadowstalker
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10617 WHERE (`CreatureID`=3759 AND `ID`=1); -- Felmusk Rogue
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=61368 WHERE (`CreatureID`=64841 AND `ID`=1); -- Hooded Crusader
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=61368 WHERE (`CreatureID`=64827 AND `ID`=1); -- Hooded Crusader
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=28678 WHERE (`CreatureID`=34294 AND `ID`=1); -- Protector Dorinar
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34177 AND `ID`=1); -- Ashenvale Scout
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12452 WHERE (`CreatureID`=11682 AND `ID`=1); -- Warsong Grunt
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=59240 AND `ID`=1); -- Scarlet Hall Guardian
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=49732 WHERE (`CreatureID`=58676 AND `ID`=1); -- Scarlet Defender
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13718 WHERE (`CreatureID`=58683 AND `ID`=1); -- Scarlet Myrmidon
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=32375 WHERE (`CreatureID`=59302 AND `ID`=1); -- Sergeant Verdone
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=32375 WHERE (`CreatureID`=59299 AND `ID`=1); -- Scarlet Guardian
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=39140 WHERE (`CreatureID`=59303 AND `ID`=1); -- Houndmaster Braun
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=59191 AND `ID`=1); -- Commander Lindon
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=59175 AND `ID`=1); -- Master Archer
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=61368 WHERE (`CreatureID`=64738 AND `ID`=1); -- Hooded Crusader
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=6224 AND `ID`=1); -- Leprous Machinesmith
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=6222 AND `ID`=1); -- Leprous Technician
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=6223 AND `ID`=1); -- Leprous Defender
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=6407 AND `ID`=1); -- Holdout Technician
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=11585 WHERE (`CreatureID`=6391 AND `ID`=1); -- Holdout Warrior
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1957 WHERE (`CreatureID`=12903 AND `ID`=1); -- Splintertree Guard
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33569 AND `ID`=1); -- Ashenvale Assailant
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1906 WHERE (`CreatureID`=3736 AND `ID`=1); -- Darkslayer Mordenthal
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=12856 AND `ID`=1); -- Ashenvale Outrunner
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=12896 AND `ID`=1); -- Silverwing Sentinel
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12454 WHERE (`CreatureID`=12859 AND `ID`=1); -- Splintertree Raider
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1905 WHERE (`CreatureID`=52386 AND `ID`=1); -- Burning Blade Windrider
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1905 WHERE (`CreatureID`=52338 AND `ID`=1); -- Burning Blade Attacker
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1985 WHERE (`CreatureID`=43955 AND `ID`=1); -- Naman
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33193 AND `ID`=1); -- Ashenvale Skirmisher
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33281 AND `ID`=1); -- Mor'shan Watchman
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33195 AND `ID`=1); -- Ashenvale Bowman
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=33201 AND `ID`=1); -- Mor'shan Defender
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12745 WHERE (`CreatureID`=43637 AND `ID`=1); -- Marrok
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=51815 AND `ID`=1); -- Mor'shan Defender
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=104147 WHERE (`CreatureID`=70999 AND `ID`=1); -- Kor'kron Marauder
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=19910 AND `ID`=1); -- Gargok
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13319 WHERE (`CreatureID`=14717 AND `ID`=1); -- Horde Elite
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=5278 WHERE (`CreatureID`=5835 AND `ID`=1); -- Foreman Grills
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=14859 AND `ID`=1); -- Guard Taruc
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=71006 AND `ID`=1); -- Kor'kron Overseer
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=52357 AND `ID`=1); -- Venture Co. Mercenary
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3282 AND `ID`=1); -- Venture Co. Mercenary
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1961 WHERE (`CreatureID`=43957 AND `ID`=1); -- Frazzik
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12745 WHERE (`CreatureID`=43953 AND `ID`=1); -- Niriap
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10616 WHERE (`CreatureID`=34782 AND `ID`=1); -- Alicia Cuthbert
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34790 AND `ID`=1); -- Southsea Mutineer
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=44168 AND `ID`=1); -- Southsea Recruit
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13611 WHERE (`CreatureID`=34804 AND `ID`=1); -- Chef Toofus
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3384 AND `ID`=1); -- Southsea Privateer
UPDATE `creature_equip_template` SET `ItemID1`=2709, `ItemID3`=0 WHERE (`CreatureID`=3382 AND `ID`=1); -- Southsea Cannoneer
UPDATE `creature_equip_template` SET `ItemID1`=3350 WHERE (`CreatureID`=3383 AND `ID`=1); -- Southsea Cutthroat
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34752 AND `ID`=1); -- Lieutenant Pyre
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34706 AND `ID`=1); -- Theramore Sharpshooter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=3385 AND `ID`=1); -- Theramore Marine
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13406 WHERE (`CreatureID`=25089 AND `ID`=1); -- Galley Chief Steelbelly
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13855 WHERE (`CreatureID`=3391 AND `ID`=1); -- Gazlowe
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12857 WHERE (`CreatureID`=8119 AND `ID`=1); -- Zikkel
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12859 WHERE (`CreatureID`=3496 AND `ID`=1); -- Fuzruckle
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2704 WHERE (`CreatureID`=3292 AND `ID`=1); -- Brewmaster Drohn
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12452 WHERE (`CreatureID`=34656 AND `ID`=1); -- Hargash
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3502 AND `ID`=1); -- Ratchet Bruiser
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=47448 WHERE (`CreatureID`=71188 AND `ID`=1); -- Kor'kron Earthshaker
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=9458 AND `ID`=1); -- Horde Axe Thrower
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=19763 WHERE (`CreatureID`=34634 AND `ID`=1); -- Gorgal Angerscar
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1961 WHERE (`CreatureID`=34626 AND `ID`=1); -- Jerrik Highmountain
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=5838 AND `ID`=1); -- Brokespear
UPDATE `creature_equip_template` SET `ItemID1`=45475 WHERE (`CreatureID`=5841 AND `ID`=1); -- Rocklance
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=57195 WHERE (`CreatureID`=47293 AND `ID`=1); -- Deathstalker Commander Belmont
UPDATE `creature_equip_template` SET `ItemID1`=2029 WHERE (`CreatureID`=47232 AND `ID`=1); -- Ghostly Cook
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=56175 WHERE (`CreatureID`=47145 AND `ID`=1); -- Spitebone Guardian
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=25818 WHERE (`CreatureID`=4278 AND `ID`=1); -- Commander Springvale
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2052 WHERE (`CreatureID`=3873 AND `ID`=1); -- Tormented Officer
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=18166 WHERE (`CreatureID`=47030 AND `ID`=1); -- Veteran Forsaken Trooper
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3394 AND `ID`=1); -- Barak Kodobane
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=5286 WHERE (`CreatureID`=3670 AND `ID`=1); -- Lord Pythas
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1906 WHERE (`CreatureID`=3669 AND `ID`=1); -- Lord Cobrahn
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=35719 WHERE (`CreatureID`=3840 AND `ID`=1); -- Druid of the Fang
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=72479 WHERE (`CreatureID`=61412 AND `ID`=1); -- Dark Shaman Koranthal
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=77402 WHERE (`CreatureID`=61672 AND `ID`=1); -- Dark Shaman Acolyte
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3272 AND `ID`=1); -- Kolkar Wrangler
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=73590 AND `ID`=1); -- Kor'kron Outrider
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=71012 AND `ID`=1); -- Kor'kron Butcher
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=71010 AND `ID`=1); -- Kor'kron Outrider
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12745 WHERE (`CreatureID`=3487 AND `ID`=1); -- Kalyimah Stormcloud
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12870 WHERE (`CreatureID`=3390 AND `ID`=1); -- Apothecary Helbrim
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=50027 AND `ID`=1); -- Dargad
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=50032 AND `ID`=1); -- Tarmod
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13406 WHERE (`CreatureID`=3489 AND `ID`=1); -- Zargh
UPDATE `creature_equip_template` SET `ItemID1`=6256 WHERE (`CreatureID`=14857 AND `ID`=1); -- Erk
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=14893 AND `ID`=1); -- Guard Kurall
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1905 WHERE (`CreatureID`=34594 AND `ID`=1); -- Burning Blade Raider
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34560 AND `ID`=1); -- Una Wolfclaw
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2052 WHERE (`CreatureID`=3432 AND `ID`=1); -- Mankrik
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=34504 AND `ID`=1); -- Grol'dom Grunt
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2051 WHERE (`CreatureID`=34545 AND `ID`=1); -- Razormane Frenzy
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13407 WHERE (`CreatureID`=43956 AND `ID`=1); -- Lokarbo
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12452 WHERE (`CreatureID`=5810 AND `ID`=1); -- Uzzek
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3501 AND `ID`=1); -- Horde Guard
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=47321 AND `ID`=1); -- Zugra Flamefist
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=42224 WHERE (`CreatureID`=47336 AND `ID`=1); -- Baradin Grunt
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=42224 WHERE (`CreatureID`=47335 AND `ID`=1); -- Baradin Grunt
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=10611 WHERE (`CreatureID`=8576 AND `ID`=1); -- Ag'tor Bloodfist
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=3774 WHERE (`CreatureID`=45546 AND `ID`=1); -- Vizna Bangwrench
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=45814 AND `ID`=1); -- Orgrimmar Bruiser
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12745 WHERE (`CreatureID`=44770 AND `ID`=1); -- Tatepi
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12870 WHERE (`CreatureID`=44780 AND `ID`=1); -- Isashi
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=25081 AND `ID`=1); -- Grunt Ounda
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=25080 AND `ID`=1); -- Grunt Umgor
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=3203 AND `ID`=1); -- Fizzle Darkclaw
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3265 AND `ID`=1); -- Razormane Hunter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=39660 WHERE (`CreatureID`=39269 AND `ID`=1); -- Lieutenant Palliter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2053 WHERE (`CreatureID`=3129 AND `ID`=1); -- Kul Tiras Marine
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2202 WHERE (`CreatureID`=3881 AND `ID`=1); -- Grimtak
UPDATE `creature_equip_template` SET `ItemID1`=1903 WHERE (`CreatureID`=3163 AND `ID`=1); -- Uhgar
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12745 WHERE (`CreatureID`=3164 AND `ID`=1); -- Jark
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1957 WHERE (`CreatureID`=3169 AND `ID`=1); -- Tarshaw Jaggedscar
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=38225 AND `ID`=1); -- Vol'jin
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13319 WHERE (`CreatureID`=38437 AND `ID`=1); -- Vanira
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=38301 AND `ID`=1); -- Spitescale Siren
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=12454 WHERE (`CreatureID`=38324 AND `ID`=1); -- Darkspear Tribesman
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=39072 AND `ID`=1); -- Naj'tess
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=38953 AND `ID`=1); -- Vision of Vol'jin
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=23428 WHERE (`CreatureID`=38938 AND `ID`=1); -- Vision of Garrosh Hellscream
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=38966 AND `ID`=1); -- Vol'jin
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=13319 WHERE (`CreatureID`=39027 AND `ID`=1); -- Vanira
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=19915 WHERE (`CreatureID`=38442 AND `ID`=1); -- Morakki
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=38282 AND `ID`=1); -- Novice Darkspear Hunter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2184 WHERE (`CreatureID`=38272 AND `ID`=1); -- Novice Darkspear Rogue
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2040 WHERE (`CreatureID`=38242 AND `ID`=1); -- Nekali
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1961 WHERE (`CreatureID`=38281 AND `ID`=1); -- Novice Darkspear Shaman
UPDATE `creature_equip_template` SET `ItemID1`=1814, `ItemID3`=0 WHERE (`CreatureID`=38217 AND `ID`=1); -- Darkspear Watcher
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=51913 AND `ID`=1); -- Sen'jin Guardian
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=6225 WHERE (`CreatureID`=5942 AND `ID`=1); -- Zansoa
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=9467 WHERE (`CreatureID`=50015 AND `ID`=1); -- Munalti
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=2040 WHERE (`CreatureID`=50011 AND `ID`=1); -- Cona
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=3697 WHERE (`CreatureID`=3184 AND `ID`=1); -- Miao'zan
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=39261 AND `ID`=1); -- Northwatch Ranger
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=3112 AND `ID`=1); -- Razormane Scout
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1961 WHERE (`CreatureID`=3161 AND `ID`=1); -- Rarc
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=39317 AND `ID`=1); -- Northwatch Scout
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=5952 AND `ID`=1); -- Den Grunt

DELETE FROM `gameobject_template` WHERE `entry` IN (224816 /*Invisible Door (0.5 scale)*/, 224745 /*Frischer Kadaver*/, 225709 /*Kraul Thorn 01 Small*/, 225576 /*Kraul Thorn 01*/, 225707 /*Crystal 07*/, 225708 /*Crystal 05 Small*/, 225700 /*Crystal 04*/, 225705 /*Crystal 05*/, 225699 /*Crystal 03*/, 225706 /*Crystal 06*/, 225701 /*Crystal 02 Small*/, 224698 /*Frischer Kadaver*/, 225697 /*Crystal 01*/, 225698 /*Crystal 02*/, 225710 /*Kraul Thorn 02*/, 225711 /*Kraul Thorn 01 Tiny*/, 224488 /*Steinkeils ramponierter Speer*/, 223839 /*Behelfsm��ige Barrikade*/, 252870 /*Die Verbannung der Elementarlords*/, 225786 /*Altar des Blutes*/, 224720 /*Feuer von Aku'mai*/, 225586 /*Sch�delhaufen*/, 225779 /*Altar des Blutes*/, 224627 /*Aufgerolltes Seil*/, 224747 /*Seilpfosten*/, 224628 /*Seilpfosten*/, 224826 /*Altar des Blutes*/, 251048 /*Matrizenlochkart-o-mat 3005-E*/, 251015 /*Verschl�sselte T�r*/, 266889 /*Lichtstreif*/, 266890 /*Lichtstreif*/, 248958 /*Kompendium uralter Waffen, Band VII*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(224816, 1, 10403, 'Invisible Door (0.5 scale)', '', '', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Invisible Door (0.5 scale)
(224745, 5, 14, 'Frischer Kadaver', '', '', '', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Frischer Kadaver
(225709, 1, 14651, 'Kraul Thorn 01 Small', '', '', '', 0.1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Kraul Thorn 01 Small
(225576, 1, 14651, 'Kraul Thorn 01', '', '', '', 0.25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Kraul Thorn 01
(225707, 5, 7345, 'Crystal 07', '', '', '', 0.25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 07
(225708, 5, 14698, 'Crystal 05 Small', '', '', '', 0.1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 05 Small
(225700, 5, 14695, 'Crystal 04', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 04
(225705, 5, 14698, 'Crystal 05', '', '', '', 0.2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 05
(225699, 5, 14694, 'Crystal 03', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 03
(225706, 5, 14699, 'Crystal 06', '', '', '', 0.2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 06
(225701, 5, 14693, 'Crystal 02 Small', '', '', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 02 Small
(224698, 5, 2951, 'Frischer Kadaver', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Frischer Kadaver
(225697, 5, 14692, 'Crystal 01', '', '', '', 0.25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 01
(225698, 5, 14693, 'Crystal 02', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Crystal 02
(225710, 1, 14700, 'Kraul Thorn 02', '', '', '', 0.2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Kraul Thorn 02
(225711, 1, 14651, 'Kraul Thorn 01 Tiny', '', '', '', 0.03, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Kraul Thorn 01 Tiny
(224488, 5, 9511, 'Steinkeil''s battered spear', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Steinkeils ramponierter Speer
(223839, 0, 847, 'A makeshift barricade', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Behelfsm��ige Barrikade
(252870, 3, 20144, 'The Banishing of the Elemental Lords', 'openhandglow', 'Aufnehmen', '', 1.5, 1691, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 67959, 1, 0, 0, 23420), -- Die Verbannung der Elementarlords
(225786, 22, 7786, 'Altar of Blood', '', '', '', 1.25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Altar des Blutes
(224720, 1, 524, 'Fire of Aku''mai', 'interact', '', '', 4, 0, 0, 3000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Feuer von Aku'mai
(225586, 5, 293, 'Head of skull', '', '', '', 0.75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sch�delhaufen
(225779, 22, 9403, 'Altar of Blood', 'interact', '', '', 1.25, 153272, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Altar des Blutes
(224627, 1, 7548, 'Coiled rope', 'interact', '', '', 2, 0, 0, 3000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Aufgerolltes Seil
(224747, 22, 14561, 'Rope post', 'interact', '', '', 1.25, 150851, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Seilpfosten
(224628, 5, 14561, 'Rope post', '', '', '', 1.25, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Seilpfosten
(224826, 22, 9403, 'Altar of Blood', 'interact', '', '', 1.25, 153272, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Altar des Blutes
(251048, 2, 2091, 'Matrix Punchograph 3005-E', '', '', '', 1, 93, 0, 0, 19818, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Matrizenlochkart-o-mat 3005-E
(251015, 0, 10398, 'Encrypted door', '', '', '', 1.85, 0, 0, 0, 0, 0, 0, 0, 21439, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(266889, 10, 39529, 'Light Ray', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(266890, 10, 39530, 'Light Ray', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(248958, 2, 27749, 'Compendium of Ancient Weapons Volume VII', '', 'Aufnehmen', '', 1.5, 1690, 20058, 1, 0, 0, 0, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420);

DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=252870 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(252870, 0, 140122, 23420); -- Die Verbannung der Elementarlords

DELETE FROM `npc_text` WHERE `ID` IN (8889 /*8889*/, 14570 /*14570*/, 23466 /*23466*/, 23359 /*23359*/, 14514 /*14514*/, 16580 /*16580*/, 16111 /*16111*/, 14448 /*14448*/, 17942 /*17942*/, 18278 /*18278*/, 17889 /*17889*/, 14651 /*14651*/, 14626 /*14626*/, 14586 /*14586*/, 19936 /*19936*/, 22472 /*22472*/, 30872 /*30872*/, 22037 /*22037*/, 22036 /*22036*/, 30874 /*30874*/, 22039 /*22039*/, 30873 /*30873*/, 22038 /*22038*/, 22035 /*22035*/, 22133 /*22133*/, 20031 /*20031*/, 22257 /*22257*/, 23141 /*23141*/, 25490 /*25490*/, 20033 /*20033*/, 23381 /*23381*/, 23360 /*23360*/, 22592 /*22592*/, 18277 /*18277*/, 15271 /*15271*/, 15265 /*15265*/, 30274 /*30274*/, 30698 /*30698*/, 29821 /*29821*/, 15709 /*15709*/, 20587 /*20587*/, 15199 /*15199*/);
INSERT INTO `npc_text` (`ID`, `Probability0`, `Probability1`, `Probability2`, `Probability3`, `Probability4`, `Probability5`, `Probability6`, `Probability7`, `BroadcastTextId0`, `BroadcastTextId1`, `BroadcastTextId2`, `BroadcastTextId3`, `BroadcastTextId4`, `BroadcastTextId5`, `BroadcastTextId6`, `BroadcastTextId7`, `VerifiedBuild`) VALUES
(8889, 1, 0, 0, 0, 0, 0, 0, 0, 13803, 0, 0, 0, 0, 0, 0, 0, 23420), -- 8889
(14570, 1, 0, 0, 0, 0, 0, 0, 0, 34841, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14570
(23466, 1, 0, 0, 0, 0, 0, 0, 0, 79086, 0, 0, 0, 0, 0, 0, 0, 23420), -- 23466
(23359, 1, 0, 0, 0, 0, 0, 0, 0, 78380, 0, 0, 0, 0, 0, 0, 0, 23420), -- 23359
(14514, 1, 0, 0, 0, 0, 0, 0, 0, 34483, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14514
(16580, 1, 0, 0, 0, 0, 0, 0, 0, 44539, 0, 0, 0, 0, 0, 0, 0, 23420), -- 16580
(16111, 1, 0, 0, 0, 0, 0, 0, 0, 41677, 0, 0, 0, 0, 0, 0, 0, 23420), -- 16111
(14448, 1, 0, 0, 0, 0, 0, 0, 0, 34046, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14448
(17942, 1, 0, 0, 0, 0, 0, 0, 0, 51216, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17942
(18278, 1, 0, 0, 0, 0, 0, 0, 0, 53016, 0, 0, 0, 0, 0, 0, 0, 23420), -- 18278
(17889, 1, 0, 0, 0, 0, 0, 0, 0, 50941, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17889
(14651, 1, 0, 0, 0, 0, 0, 0, 0, 35121, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14651
(14626, 1, 0, 0, 0, 0, 0, 0, 0, 35054, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14626
(14586, 1, 0, 0, 0, 0, 0, 0, 0, 34896, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14586
(19936, 1, 0, 0, 0, 0, 0, 0, 0, 61279, 0, 0, 0, 0, 0, 0, 0, 23420), -- 19936
(22472, 1, 0, 0, 0, 0, 0, 0, 0, 73179, 0, 0, 0, 0, 0, 0, 0, 23420), -- 22472
(30872, 1, 0, 0, 0, 0, 0, 0, 0, 124346, 0, 0, 0, 0, 0, 0, 0, 23420), -- 30872
(22037, 1, 1, 1, 0, 0, 0, 0, 0, 70744, 70745, 70746, 0, 0, 0, 0, 0, 23420), -- 22037
(22036, 1, 1, 1, 0, 0, 0, 0, 0, 70741, 70742, 70743, 0, 0, 0, 0, 0, 23420), -- 22036
(30874, 1, 0, 0, 0, 0, 0, 0, 0, 124359, 0, 0, 0, 0, 0, 0, 0, 23420), -- 30874
(22039, 1, 1, 0, 0, 0, 0, 0, 0, 70750, 70751, 0, 0, 0, 0, 0, 0, 23420), -- 22039
(30873, 1, 0, 0, 0, 0, 0, 0, 0, 124356, 0, 0, 0, 0, 0, 0, 0, 23420), -- 30873
(22038, 1, 1, 1, 0, 0, 0, 0, 0, 70749, 70748, 70747, 0, 0, 0, 0, 0, 23420), -- 22038
(22035, 1, 1, 1, 0, 0, 0, 0, 0, 70738, 70739, 70740, 0, 0, 0, 0, 0, 23420), -- 22035
(22133, 1, 0, 0, 0, 0, 0, 0, 0, 71305, 0, 0, 0, 0, 0, 0, 0, 23420), -- 22133
(20031, 1, 0, 0, 0, 0, 0, 0, 0, 61825, 0, 0, 0, 0, 0, 0, 0, 23420), -- 20031
(22257, 1, 0, 0, 0, 0, 0, 0, 0, 71584, 0, 0, 0, 0, 0, 0, 0, 23420), -- 22257
(23141, 1, 0, 0, 0, 0, 0, 0, 0, 77385, 0, 0, 0, 0, 0, 0, 0, 23420), -- 23141
(25490, 1, 0, 0, 0, 0, 0, 0, 0, 90157, 0, 0, 0, 0, 0, 0, 0, 23420), -- 25490
(20033, 1, 1, 0, 0, 0, 0, 0, 0, 61833, 61834, 0, 0, 0, 0, 0, 0, 23420), -- 20033
(23381, 1, 0, 0, 0, 0, 0, 0, 0, 78627, 0, 0, 0, 0, 0, 0, 0, 23420), -- 23381
(23360, 1, 0, 0, 0, 0, 0, 0, 0, 78604, 0, 0, 0, 0, 0, 0, 0, 23420), -- 23360
(22592, 1, 0, 0, 0, 0, 0, 0, 0, 74284, 0, 0, 0, 0, 0, 0, 0, 23420), -- 22592
(18277, 1, 0, 0, 0, 0, 0, 0, 0, 53015, 0, 0, 0, 0, 0, 0, 0, 23420), -- 18277
(15271, 1, 0, 0, 0, 0, 0, 0, 0, 37932, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15271
(15265, 1, 0, 0, 0, 0, 0, 0, 0, 37940, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15265
(30274, 1, 0, 0, 0, 0, 0, 0, 0, 121509, 0, 0, 0, 0, 0, 0, 0, 23420), -- 30274
(30698, 1, 0, 0, 0, 0, 0, 0, 0, 123260, 0, 0, 0, 0, 0, 0, 0, 23420), -- 30698
(29821, 1, 0, 0, 0, 0, 0, 0, 0, 117142, 0, 0, 0, 0, 0, 0, 0, 23420), -- 29821
(15709, 1, 0, 0, 0, 0, 0, 0, 0, 39705, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15709
(20587, 1, 0, 0, 0, 0, 0, 0, 0, 64984, 0, 0, 0, 0, 0, 0, 0, 23420), -- 20587
(15199, 1, 0, 0, 0, 0, 0, 0, 0, 37467, 0, 0, 0, 0, 0, 0, 0, 23420); -- 15199

UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `VerifiedBuild`=23420 WHERE `ID`=2653; -- 2653
UPDATE `npc_text` SET `BroadcastTextId0`=33513, `VerifiedBuild`=23420 WHERE `ID`=14344; -- 14344
UPDATE `npc_text` SET `BroadcastTextId0`=2465, `VerifiedBuild`=23420 WHERE `ID`=520; -- 520
UPDATE `npc_text` SET `BroadcastTextId0`=24256, `BroadcastTextId1`=24257, `BroadcastTextId2`=24258, `VerifiedBuild`=23420 WHERE `ID`=12315; -- 12315
UPDATE `npc_text` SET `BroadcastTextId0`=31717, `VerifiedBuild`=23420 WHERE `ID`=13911; -- 13911
UPDATE `npc_text` SET `BroadcastTextId0`=7163, `VerifiedBuild`=23420 WHERE `ID`=4439; -- 4439
UPDATE `npc_text` SET `Probability0`=0, `VerifiedBuild`=23420 WHERE `ID`=3173; -- 3173
UPDATE `npc_text` SET `Probability0`=0, `VerifiedBuild`=23420 WHERE `ID`=13311; -- 13311
UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `VerifiedBuild`=23420 WHERE `ID`=2502; -- 2502
UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `VerifiedBuild`=23420 WHERE `ID`=2653; -- 2653
UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `VerifiedBuild`=23420 WHERE `ID`=2500; -- 2500
UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `VerifiedBuild`=23420 WHERE `ID`=2563; -- 2563
UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `VerifiedBuild`=23420 WHERE `ID`=2562; -- 2562
UPDATE `npc_text` SET `BroadcastTextId1`=50625, `VerifiedBuild`=23420 WHERE `ID`=10843; -- 10843
UPDATE `npc_text` SET `BroadcastTextId0`=50545, `BroadcastTextId1`=0, `VerifiedBuild`=23420 WHERE `ID`=2554; -- 2554
UPDATE `npc_text` SET `BroadcastTextId0`=50501, `BroadcastTextId1`=0, `VerifiedBuild`=23420 WHERE `ID`=3075; -- 3075
UPDATE `npc_text` SET `BroadcastTextId0`=47515, `VerifiedBuild`=23420 WHERE `ID`=17204; -- 17204
UPDATE `npc_text` SET `Probability0`=1, `Probability1`=0, `BroadcastTextId1`=0, `VerifiedBuild`=23420 WHERE `ID`=4017; -- 4017
UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `Probability2`=1, `BroadcastTextId2`=50552, `VerifiedBuild`=23420 WHERE `ID`=4014; -- 4014
UPDATE `npc_text` SET `BroadcastTextId0`=6602, `VerifiedBuild`=23420 WHERE `ID`=4035; -- 4035
UPDATE `npc_text` SET `Probability0`=0, `Probability1`=0, `VerifiedBuild`=23420 WHERE `ID`=4033; -- 4033
UPDATE `npc_text` SET `BroadcastTextId0`=6630, `VerifiedBuild`=23420 WHERE `ID`=4037; -- 4037
