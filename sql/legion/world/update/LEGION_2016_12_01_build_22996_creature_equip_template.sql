DELETE FROM `creature_equip_template` WHERE (`CreatureID`=102878 AND `ID`=1) OR (`CreatureID`=115608 AND `ID`=1) OR (`CreatureID`=115079 AND `ID`=1) OR (`CreatureID`=115078 AND `ID`=1) OR (`CreatureID`=115607 AND `ID`=1) OR (`CreatureID`=114985 AND `ID`=1) OR (`CreatureID`=115736 AND `ID`=1) OR (`CreatureID`=108096 AND `ID`=1) OR (`CreatureID`=116658 AND `ID`=1) OR (`CreatureID`=115691 AND `ID`=1) OR (`CreatureID`=115606 AND `ID`=1) OR (`CreatureID`=116360 AND `ID`=1) OR (`CreatureID`=114926 AND `ID`=1) OR (`CreatureID`=114924 AND `ID`=1) OR (`CreatureID`=116206 AND `ID`=1) OR (`CreatureID`=114927 AND `ID`=1) OR (`CreatureID`=115279 AND `ID`=1) OR (`CreatureID`=115595 AND `ID`=1) OR (`CreatureID`=114929 AND `ID`=1) OR (`CreatureID`=115594 AND `ID`=1) OR (`CreatureID`=115497 AND `ID`=1) OR (`CreatureID`=115730 AND `ID`=1) OR (`CreatureID`=116259 AND `ID`=1) OR (`CreatureID`=115388 AND `ID`=1) OR (`CreatureID`=115406 AND `ID`=1) OR (`CreatureID`=115486 AND `ID`=1) OR (`CreatureID`=115501 AND `ID`=1) OR (`CreatureID`=115757 AND `ID`=1) OR (`CreatureID`=114350 AND `ID`=1) OR (`CreatureID`=115489 AND `ID`=1) OR (`CreatureID`=115490 AND `ID`=1) OR (`CreatureID`=114584 AND `ID`=1) OR (`CreatureID`=114260 AND `ID`=1) OR (`CreatureID`=114261 AND `ID`=1) OR (`CreatureID`=114265 AND `ID`=1) OR (`CreatureID`=114266 AND `ID`=1) OR (`CreatureID`=115976 AND `ID`=1) OR (`CreatureID`=115984 AND `ID`=1) OR (`CreatureID`=116002 AND `ID`=1) OR (`CreatureID`=115967 AND `ID`=1) OR (`CreatureID`=114314 AND `ID`=1) OR (`CreatureID`=115038 AND `ID`=1) OR (`CreatureID`=114803 AND `ID`=2) OR (`CreatureID`=114318 AND `ID`=1) OR (`CreatureID`=114320 AND `ID`=1) OR (`CreatureID`=114321 AND `ID`=1) OR (`CreatureID`=114316 AND `ID`=1) OR (`CreatureID`=114312 AND `ID`=1) OR (`CreatureID`=114262 AND `ID`=1) OR (`CreatureID`=114815 AND `ID`=1) OR (`CreatureID`=114799 AND `ID`=1) OR (`CreatureID`=114802 AND `ID`=1) OR (`CreatureID`=114803 AND `ID`=1) OR (`CreatureID`=114801 AND `ID`=1) OR (`CreatureID`=114636 AND `ID`=1) OR (`CreatureID`=114637 AND `ID`=1) OR (`CreatureID`=114629 AND `ID`=1) OR (`CreatureID`=116573 AND `ID`=1) OR (`CreatureID`=115496 AND `ID`=1) OR (`CreatureID`=114716 AND `ID`=1) OR (`CreatureID`=114715 AND `ID`=1) OR (`CreatureID`=114714 AND `ID`=1) OR (`CreatureID`=114256 AND `ID`=1) OR (`CreatureID`=116784 AND `ID`=1) OR (`CreatureID`=114667 AND `ID`=1) OR (`CreatureID`=115029 AND `ID`=1) OR (`CreatureID`=114821 AND `ID`=1) OR (`CreatureID`=115164 AND `ID`=1) OR (`CreatureID`=106552 AND `ID`=1) OR (`CreatureID`=107452 AND `ID`=1) OR (`CreatureID`=108248 AND `ID`=1) OR (`CreatureID`=108380 AND `ID`=1) OR (`CreatureID`=109222 AND `ID`=1) OR (`CreatureID`=108377 AND `ID`=1) OR (`CreatureID`=108342 AND `ID`=1) OR (`CreatureID`=108515 AND `ID`=1) OR (`CreatureID`=109226 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(102878, 1, 134846, 0, 0), -- Gilnearischer Wildfang
(115608, 1, 132170, 0, 132170), -- Silgryn
(115079, 1, 137254, 0, 0), -- Victoire
(115078, 1, 132170, 0, 0), -- Arluelle
(115607, 1, 137256, 0, 0), -- Thoramir
(114985, 1, 134637, 0, 0), -- Scarleth
(115736, 1, 132171, 0, 0), -- Erste Arkanistin Thalyssra
(108096, 1, 132170, 0, 0), -- Abkömmling der Dämmerwache
(116658, 1, 140089, 0, 0), -- Verschwörer des Dämonenpakts
(115691, 1, 140089, 0, 0), -- Magus des Dämonenpakts
(115606, 1, 132170, 0, 0), -- Zauberfechteradept
(116360, 1, 133174, 0, 0), -- Seher der Dämmerwache
(114926, 1, 40497, 0, 0), -- Arkanwache
(114924, 1, 132170, 0, 0), -- Säbelwächter der Dämmerwache
(116206, 1, 138422, 0, 138422), -- Zornwächter der Teufelsseele
(114927, 1, 132170, 0, 132249), -- Faust der Dämmerwache
(115279, 1, 132170, 0, 0), -- Aufrührer der Shal'dorei
(115595, 1, 132171, 0, 0), -- Exekutor der Dämmerwache
(114929, 1, 132170, 0, 133174), -- Verteidiger der Dämmerwache
(115594, 1, 137256, 0, 0), -- Schiedsrichter der Dämmerwache
(115497, 1, 28067, 0, 0), -- Erzmagier Khadgar
(115730, 1, 138755, 0, 0), -- Wachposten der Teufelswache
(116259, 1, 28067, 0, 0), -- Erzmagier Khadgar
(115388, 1, 140999, 0, 0), -- König
(115406, 1, 37579, 0, 12860), -- Springer
(115486, 1, 124447, 0, 0), -- Kultivierte Schlächterin
(115501, 1, 28067, 0, 0), -- Erzmagier Khadgar
(115757, 1, 138422, 0, 138422), -- Flammenbringer der Zornwächter
(114350, 1, 28067, 0, 0), -- Medivhs Schemen
(115489, 1, 21553, 0, 0), -- Anduin Lothar
(115490, 1, 140999, 0, 0), -- Prinz Llane Wrynn
(114584, 1, 5292, 0, 0), -- Phantombühnenarbeiter
(114260, 1, 56697, 0, 0), -- Mrrgria
(114261, 1, 4938, 0, 0), -- Jettony
(114265, 1, 4565, 0, 2092), -- Bandengrobian
(114266, 1, 1155, 0, 0), -- Gezeitensprecher des Ufers
(115976, 1, 10616, 0, 0), -- Julianne
(115984, 1, 5289, 0, 0), -- Blechkopf
(116002, 1, 25587, 0, 0), -- Die böse Hexe
(115967, 1, 3364, 0, 2184), -- Romulo
(114314, 1, 70227, 0, 0), -- Portalhüter der Eredar
(115038, 1, 28067, 0, 0), -- Abbild von Medivh
(114803, 2, 3367, 0, 0), -- Spektraler Stallarbeiter
(114318, 1, 14082, 0, 0), -- Baron Rafe Dreuger
(114320, 1, 22199, 0, 0), -- Lord Robin Daris
(114321, 1, 13165, 0, 12893), -- Lord Crispin Ference
(114316, 1, 28739, 0, 0), -- Baroness Dorothea Mühlenstein
(114312, 1, 23481, 0, 23481), -- Moroes
(114262, 1, 94865, 0, 0), -- Attumen der Jäger
(114815, 1, 1903, 0, 0), -- Koren
(114799, 1, 0, 0, 13609), -- Calliard
(114802, 1, 2525, 0, 0), -- Spektraler Geselle
(114803, 1, 3346, 0, 0), -- Spektraler Stallarbeiter
(114801, 1, 2558, 0, 0), -- Spektraler Lehrling
(114636, 1, 2809, 0, 1985), -- Phantomgardist
(114637, 1, 1896, 0, 5281), -- Spektraler Wachposten
(114629, 1, 1895, 0, 0), -- Spektraler Anhänger
(116573, 1, 13607, 0, 2081), -- R. L. Cooper
(115496, 1, 28067, 0, 0), -- Erzmagier Khadgar
(114716, 1, 3351, 0, 0), -- Geisterhafter Bäcker
(114715, 1, 2827, 0, 0), -- Geisterhafter Koch
(114714, 1, 2716, 0, 0), -- Geisterhafter Bediensteter
(114256, 1, 128962, 0, 0), -- Verderberin der Teufelszungen
(116784, 1, 128962, 0, 0), -- Verderberin der Teufelszungen
(114667, 1, 13069, 0, 0), -- Erzmagier Alturus
(115029, 1, 13709, 0, 0), -- Erzmagierin Leryda
(114821, 1, 43617, 0, 0), -- Magier der Kirin Tor
(115164, 1, 82810, 0, 0), -- Tobender Berserker
(106552, 1, 49687, 0, 118201), -- Nachtwächterin Merayl
(107452, 1, 0, 0, 12868), -- Der alte Fillmaff
(108248, 1, 35781, 0, 0), -- Erzmagierin Modera
(108380, 1, 29405, 0, 0), -- Esara Verrinde
(109222, 1, 111743, 0, 0), -- Meryl Teufelssturm
(108377, 1, 5112, 0, 0), -- Ravandwyr
(108342, 1, 18842, 0, 0), -- Millhaus Manasturm
(108515, 1, 1728, 0, 0), -- Erzmagierin Melis
(109226, 1, 28456, 0, 0); -- Erzmagier Vargoth

UPDATE `creature_equip_template` SET `ItemID1`=59616 WHERE (`CreatureID`=92617 AND `ID`=1); -- Bewohner von Bradenbach
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=5281 WHERE (`CreatureID`=16424 AND `ID`=1); -- Spektraler Wachposten
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1985 WHERE (`CreatureID`=16425 AND `ID`=1); -- Phantomgardist
UPDATE `creature_equip_template` SET `ItemID1`=1902 WHERE (`CreatureID`=44158 AND `ID`=3); -- Peon des Himmelspfads von Orgrimmar
UPDATE `creature_equip_template` SET `ItemID1`=3367 WHERE (`CreatureID`=44158 AND `ID`=1); -- Peon des Himmelspfads von Orgrimmar
UPDATE `creature_equip_template` SET `ItemID3`=118201 WHERE (`CreatureID`=88782 AND `ID`=2); -- Nachtwächter von Nar'thalas
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=88782 AND `ID`=1); -- Nachtwächter von Nar'thalas
UPDATE `creature_equip_template` SET `ItemID1`=94096 WHERE (`CreatureID`=89104 AND `ID`=2); -- Schiffbrüchiger Gefangener
UPDATE `creature_equip_template` SET `ItemID1`=33598 WHERE (`CreatureID`=89112 AND `ID`=2); -- Schiffbrüchiger Gefangener

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=110687 AND `ID`=1) OR (`CreatureID`=91662 AND `ID`=1) OR (`CreatureID`=103653 AND `ID`=1) OR (`CreatureID`=109332 AND `ID`=1) OR (`CreatureID`=106074 AND `ID`=1) OR (`CreatureID`=101794 AND `ID`=1) OR (`CreatureID`=108504 AND `ID`=1) OR (`CreatureID`=108506 AND `ID`=1) OR (`CreatureID`=108502 AND `ID`=1) OR (`CreatureID`=99673 AND `ID`=1) OR (`CreatureID`=99674 AND `ID`=1) OR (`CreatureID`=102865 AND `ID`=1) OR (`CreatureID`=102867 AND `ID`=1) OR (`CreatureID`=104294 AND `ID`=1) OR (`CreatureID`=102876 AND `ID`=1) OR (`CreatureID`=102869 AND `ID`=1) OR (`CreatureID`=102874 AND `ID`=1) OR (`CreatureID`=111823 AND `ID`=1) OR (`CreatureID`=108139 AND `ID`=2) OR (`CreatureID`=109473 AND `ID`=1) OR (`CreatureID`=116117 AND `ID`=1) OR (`CreatureID`=115690 AND `ID`=1) OR (`CreatureID`=116118 AND `ID`=1) OR (`CreatureID`=116116 AND `ID`=1) OR (`CreatureID`=110998 AND `ID`=1) OR (`CreatureID`=111012 AND `ID`=1) OR (`CreatureID`=110957 AND `ID`=1) OR (`CreatureID`=111002 AND `ID`=1) OR (`CreatureID`=110955 AND `ID`=1) OR (`CreatureID`=111006 AND `ID`=1) OR (`CreatureID`=113702 AND `ID`=1) OR (`CreatureID`=113703 AND `ID`=1) OR (`CreatureID`=110999 AND `ID`=1) OR (`CreatureID`=111013 AND `ID`=1) OR (`CreatureID`=111065 AND `ID`=1) OR (`CreatureID`=116050 AND `ID`=1) OR (`CreatureID`=107333 AND `ID`=1) OR (`CreatureID`=113850 AND `ID`=1) OR (`CreatureID`=115012 AND `ID`=1) OR (`CreatureID`=115216 AND `ID`=1) OR (`CreatureID`=114958 AND `ID`=1) OR (`CreatureID`=111619 AND `ID`=1) OR (`CreatureID`=115806 AND `ID`=1) OR (`CreatureID`=114876 AND `ID`=1) OR (`CreatureID`=114956 AND `ID`=1) OR (`CreatureID`=114948 AND `ID`=1) OR (`CreatureID`=114949 AND `ID`=1) OR (`CreatureID`=111455 AND `ID`=1) OR (`CreatureID`=109163 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(110687, 1, 80280, 0, 0), -- Yalia Weisenwisper
(91662, 1, 113170, 0, 0), -- Spektraler Verteidiger
(103653, 1, 134843, 0, 134843), -- Hauptmann Schwarzschlund
(109332, 1, 111703, 0, 0), -- Vorfahre der Bluttotems
(106074, 1, 119206, 0, 118005), -- Taurnash der Ketzer
(101794, 1, 124065, 0, 0), -- -Unknown-
(108504, 1, 128358, 0, 128372), -- Marius Teufelsbann
(108506, 1, 45123, 0, 0), -- Okano der Fels
(108502, 1, 119206, 0, 118005), -- Großer Geistwandler Ko
(99673, 1, 1117, 0, 0), -- Rolin Flusstreter
(99674, 1, 1117, 0, 0), -- Glana Flusstreter
(102865, 1, 134842, 0, 111717), -- Gilnearischer Schildwächter
(102867, 1, 134843, 0, 134843), -- Gilnearischer Raufer
(104294, 1, 134845, 0, 0), -- Erzmagier Brixton
(102876, 1, 134845, 0, 0), -- Gilnearischer Zauberer
(102869, 1, 94592, 0, 0), -- Gilnearischer Kriegstreiber
(102874, 1, 127596, 0, 127204), -- Lichtträger von Gilneas
(111823, 1, 128359, 0, 128371), -- Verräter des Dämonenpakts
(108139, 2, 44703, 0, 0), -- Kielholer der Schwarzsegel
(109473, 1, 132171, 0, 0), -- Verneblerin der Dämmerwache
(116117, 1, 137257, 0, 137257), -- Marodeur des Dämonenpakts
(115690, 1, 143591, 0, 137260), -- Energiewirker des Dämonenpakts
(116118, 1, 140089, 0, 0), -- Magus des Dämonenpakts
(116116, 1, 142316, 0, 137260), -- Teufelsklingenbeschützer
(110998, 1, 138422, 0, 138422), -- Ausbilder Solag
(111012, 1, 75010, 0, 127580), -- Verbrennerin Evixa
(110957, 1, 124524, 0, 0), -- Kommandant Vigrox
(111002, 1, 138422, 0, 138422), -- Verdammnisbringer Sinarus
(110955, 1, 82810, 0, 0), -- Bulliger Stoßtruppensoldat
(111006, 1, 75010, 0, 127580), -- Teufelslohendominator
(113702, 1, 137254, 0, 0), -- Zauberklinge der Loyalisten
(113703, 1, 132171, 0, 0), -- Magus der Loyalisten
(110999, 1, 127651, 0, 127651), -- Teufelsschwertadept
(111013, 1, 137257, 0, 0), -- Champion der Teufelsseele
(111065, 1, 132170, 0, 132170), -- Menageriewärter
(116050, 1, 132171, 0, 0), -- Beschwörer der Dämmerwache
(107333, 1, 137254, 0, 137253), -- Aufseher Durant
(113850, 1, 61113, 0, 0), -- Wyrmzungenverschlinger
(115012, 1, 140089, 0, 0), -- Verschwörer des Dämonenpakts
(115216, 1, 137254, 0, 0), -- Veteranin der Dämmerwache
(114958, 1, 143500, 0, 0), -- Unterwerfer der Dämmerwache
(111619, 1, 132171, 0, 0), -- Vernebler der Dämmerwache
(115806, 1, 133178, 0, 0), -- Bestrafer der Dämmerwache
(114876, 1, 138422, 0, 138422), -- Rachsüchtiger Zornwächter
(114956, 1, 143500, 0, 0), -- Victoire
(114948, 1, 132170, 0, 0), -- Silgryn
(114949, 1, 132170, 0, 0), -- Arluelle
(111455, 1, 111582, 0, 111582), -- Hauptmann Nuthals
(109163, 1, 33213, 0, 0); -- Kapitän Dargun

UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=88782 AND `ID`=2); -- -Unknown-
UPDATE `creature_equip_template` SET `ItemID3`=118201 WHERE (`CreatureID`=88782 AND `ID`=1); -- -Unknown-
UPDATE `creature_equip_template` SET `ItemID1`=140548, `ItemID2`=0 WHERE (`CreatureID`=94358 AND `ID`=3); -- Aschenbringer
UPDATE `creature_equip_template` SET `ItemID2`=29 WHERE (`CreatureID`=94358 AND `ID`=2); -- Aschenbringer
UPDATE `creature_equip_template` SET `ItemID2`=18 WHERE (`CreatureID`=94358 AND `ID`=1); -- Aschenbringer
UPDATE `creature_equip_template` SET `ItemID1`=114978 WHERE (`CreatureID`=99386 AND `ID`=5); -- Tauren der Flussmähnen
UPDATE `creature_equip_template` SET `ItemID1`=118568 WHERE (`CreatureID`=99386 AND `ID`=3); -- Tauren der Flussmähnen
UPDATE `creature_equip_template` SET `ItemID1`=114980 WHERE (`CreatureID`=99386 AND `ID`=2); -- Tauren der Flussmähnen
UPDATE `creature_equip_template` SET `ItemID1`=118563 WHERE (`CreatureID`=99386 AND `ID`=1); -- Tauren der Flussmähnen

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=105996 AND `ID`=1) OR (`CreatureID`=105495 AND `ID`=1) OR (`CreatureID`=112261 AND `ID`=1) OR (`CreatureID`=112260 AND `ID`=1) OR (`CreatureID`=116943 AND `ID`=1) OR (`CreatureID`=114880 AND `ID`=1) OR (`CreatureID`=116760 AND `ID`=1) OR (`CreatureID`=114568 AND `ID`=1) OR (`CreatureID`=114812 AND `ID`=1) OR (`CreatureID`=114709 AND `ID`=1) OR (`CreatureID`=114809 AND `ID`=1) OR (`CreatureID`=114923 AND `ID`=1) OR (`CreatureID`=114811 AND `ID`=1) OR (`CreatureID`=114813 AND `ID`=1) OR (`CreatureID`=114922 AND `ID`=1) OR (`CreatureID`=114639 AND `ID`=1) OR (`CreatureID`=114614 AND `ID`=1) OR (`CreatureID`=114539 AND `ID`=1) OR (`CreatureID`=114532 AND `ID`=1) OR (`CreatureID`=114547 AND `ID`=1) OR (`CreatureID`=114538 AND `ID`=1) OR (`CreatureID`=114996 AND `ID`=1) OR (`CreatureID`=114360 AND `ID`=1) OR (`CreatureID`=114361 AND `ID`=1) OR (`CreatureID`=114263 AND `ID`=1) OR (`CreatureID`=114791 AND `ID`=1) OR (`CreatureID`=114785 AND `ID`=1) OR (`CreatureID`=114788 AND `ID`=1) OR (`CreatureID`=114784 AND `ID`=1) OR (`CreatureID`=115381 AND `ID`=1) OR (`CreatureID`=110870 AND `ID`=1) OR (`CreatureID`=115278 AND `ID`=1) OR (`CreatureID`=114885 AND `ID`=1) OR (`CreatureID`=114883 AND `ID`=1) OR (`CreatureID`=114841 AND `ID`=1) OR (`CreatureID`=114887 AND `ID`=1) OR (`CreatureID`=114931 AND `ID`=1) OR (`CreatureID`=115926 AND `ID`=1) OR (`CreatureID`=114866 AND `ID`=1) OR (`CreatureID`=115684 AND `ID`=1) OR (`CreatureID`=115526 AND `ID`=1) OR (`CreatureID`=114897 AND `ID`=1) OR (`CreatureID`=114995 AND `ID`=1) OR (`CreatureID`=116223 AND `ID`=1) OR (`CreatureID`=116218 AND `ID`=1) OR (`CreatureID`=116225 AND `ID`=1) OR (`CreatureID`=116421 AND `ID`=1) OR (`CreatureID`=109501 AND `ID`=1) OR (`CreatureID`=100303 AND `ID`=1) OR (`CreatureID`=109500 AND `ID`=1) OR (`CreatureID`=109498 AND `ID`=1) OR (`CreatureID`=104292 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(105996, 1, 60765, 0, 0), -- Smaragddryade
(105495, 1, 137012, 0, 0), -- Umnachtete Schwester
(112261, 1, 52529, 0, 0), -- Schreckensseelenverderber
(112260, 1, 39743, 0, 0), -- Schreckensseelenschänder
(116943, 1, 137254, 0, 0), -- Nachtjägerin Syrenne
(114880, 1, 27405, 0, 27406), -- Blutritter der Morgenwache
(116760, 1, 115802, 0, 0), -- Odyn
(114568, 1, 118289, 0, 0), -- Zerfallender Diener
(114812, 1, 1925, 0, 130101), -- Matrosennachtwächter
(114709, 1, 115802, 0, 0), -- Schmutzfürst
(114809, 1, 118083, 0, 118083), -- Matrosennachtwächter
(114923, 1, 3350, 0, 0), -- Splitterknochenskelett
(114811, 1, 85951, 0, 108594), -- Korallenmaid der Kvaldir
(114813, 1, 41180, 0, 0), -- Gezeitenhexe der Kvaldir
(114922, 1, 138736, 0, 49777), -- Dunkler Seraph
(114639, 1, 36543, 0, 0), -- Riffbeschwörer der Kvaldir
(114614, 1, 34058, 0, 0), -- Geistfetzer der Kvaldir
(114539, 1, 36543, 0, 0), -- Riffbeschwörer der Kvaldir
(114532, 1, 129726, 0, 0), -- Seelenbinder der Knochensprecher
(114547, 1, 56193, 0, 56174), -- Uralter Knochenknecht
(114538, 1, 34058, 0, 0), -- Geistfetzer der Kvaldir
(114996, 1, 137263, 0, 137264), -- Runenträger der Valarjar
(114360, 1, 140345, 0, 143547), -- Hyrja
(114361, 1, 109641, 0, 0), -- Hymdall
(114263, 1, 115802, 0, 0), -- Odyn
(114791, 1, 124548, 0, 34590), -- Auserwählte von Eyir
(114785, 1, 34816, 0, 0), -- Schützin der Valarjar
(114788, 1, 36543, 0, 0), -- Donnerrufer der Valarjar
(114784, 1, 137263, 0, 0), -- Champion der Valarjar
(115381, 1, 6976, 0, 0), -- Rebell der Nachtgeborenen
(110870, 1, 132171, 0, 0), -- Apotheker Faldren
(115278, 1, 23481, 0, 23481), -- Untoter Bediensteter
(114885, 1, 35117, 0, 0), -- Wächter der Morgensucher
(114883, 1, 29114, 0, 0), -- Großmagister Rommath
(114841, 1, 49767, 0, 0), -- Lady Liadrin
(114887, 1, 5956, 0, 0), -- Lanesh der Stahlweber
(114931, 1, 34284, 0, 27496), -- Rebell der Nachtsüchtigen
(115926, 1, 91793, 0, 0), -- Schildwache von Darnassus
(114866, 1, 43619, 0, 0), -- Magierwache des Silberbunds
(115684, 1, 43617, 0, 0), -- Friedensbewahrer der Kirin Tor
(115526, 1, 132171, 0, 0), -- Arkusformer Thorendis
(114897, 1, 91793, 0, 0), -- Schildwache von Darnassus
(114995, 1, 43619, 0, 0), -- Kriegsmagier des Silberbunds
(116223, 1, 140089, 0, 0), -- Kapitänin Fiora
(116218, 1, 137257, 0, 137257), -- Bestrafer des Dämonenpakts
(116225, 1, 140089, 0, 0), -- Folterer des Dämonenpakts
(116421, 1, 133175, 0, 0), -- Leylinienkanalisierer
(109501, 1, 18842, 0, 0), -- Düstervoll
(100303, 1, 18608, 0, 0), -- Zenobia
(109500, 1, 19324, 0, 19324), -- Jak
(109498, 1, 18348, 0, 12602), -- Xaander
(104292, 1, 134846, 0, 0); -- Kapitän Klingenklaue

UPDATE `creature_equip_template` SET `ItemID2`=14 WHERE (`CreatureID`=94358 AND `ID`=2); -- Aschenbringer
UPDATE `creature_equip_template` SET `ItemID1`=114980 WHERE (`CreatureID`=99386 AND `ID`=1); -- Tauren der Flussmähnen
UPDATE `creature_equip_template` SET `ItemID2`=32 WHERE (`CreatureID`=94358 AND `ID`=1); -- Aschenbringer
