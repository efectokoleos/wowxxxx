# TrinityCore - WowPacketParser
# File name: FusionV7_1_0_22908deDE.pkt
# Detected build: V7_1_0_22908
# Detected locale: deDE
# Targeted database: Legion
# Parsing date: 02/03/2017 16:41:54

SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=41760 AND `locale`='deDE') OR (`ID`=43994 AND `locale`='deDE') OR (`ID`=41704 AND `locale`='deDE') OR (`ID`=41702 AND `locale`='deDE') OR (`ID`=41149 AND `locale`='deDE') OR (`ID`=40326 AND `locale`='deDE') OR (`ID`=44543 AND `locale`='deDE') OR (`ID`=39838 AND `locale`='deDE') OR (`ID`=38567 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(41760, 'deDE', 'Kel''danaths Vermächtnis', 'Benutzt den Zauberstein von Kel''danath, um 5 Verdorrte zu bezaubern, und schickt sie nach Shal''Aran.', 'Kel''danath hat etwas Unglaubliches entdeckt: die Macht, die Verdorrten zu unterwerfen und sogar zu kontrollieren.\n\nIch mache dort weiter, wo mein alter Freund aufgehört hat. Wir werden dafür sorgen, dass er nicht umsonst gestorben ist.\n\nVielleicht können wir sogar den Elenden, die einem schlimmeren Schicksal als dem Tod überlassen wurden, Frieden verschaffen.\n\nIch benötige weitere Versuchsobjekte. Nehmt Kel''danaths Zauberstein und unterwerft damit weitere Verdorrte.', '', '', '', '', '', '', 22908),
(43994, 'deDE', 'Thalyssra nähren', '', '', '', '', '', '', '', '', 22908),
(41704, 'deDE', 'Subjekt 16', 'Bringt Theryn zu Thalyssra.', '<Theryn scheint noch immer unter dem Zauber des Arkanisten zu stehen. In Kel''danaths Abwesenheit könnte ein lebendes Versuchsobjekt Thalyssra dabei helfen, seine Forschung nachzuvollziehen.>', '', '', '', '', '', '', 22908),
(41702, 'deDE', 'In Stein gemeißelt', 'Beschafft den Zauberstein von Kel''danath.', '<Ihr bündelt die Notizen und steckt sie in Euren Rucksack.\n\nDem letzten Tagebucheintrag zufolge hat Kel''danath für seine Experimente etwas genutzt, das er als "Zauberstein" bezeichnet.\n\nDer Zauberstein, wenn nicht sogar der Arkanist selbst, sollte sich in der Nähe befinden.>', '', '', '', '', '', '', 22908),
(41149, 'deDE', 'Schmucker Schutz', 'Aktiviert $1oa arkane Zauberschutze im Zinnobertal.', '<Das kleine Gerät in eleganter Nachtgeborenenbauart knistert leicht vor arkaner Energie.\n\nIn Kel''danaths Notizen steht etwas von Zauberschutzen, um die Verdorrten in Schach zu halten. Er scheint eine Art des Zauberwirkens entdeckt zu haben, die nicht ihren gewaltigen Appetit auf Magie anspricht.\n\nDiese Geräte könnten hilfreich bei Eurer Suche nach dem vermissten Arkanisten sein.>', '', '', '', '', '', '', 22908),
(40326, 'deDE', 'Vereinzelte Erinnerungen', 'Betrachtet $1oa Seiten von Kel''danaths Notizen.', '<Eure Finger streichen über die eleganten Zeichen auf der Seite, während eine Stimme Euch im Geiste die Worte diktiert.\n\nDie Nachtgeborenen haben offenbar sogar einfache Notizen zu einer Zauberkunst perfektioniert.\n\nEindeutig war hier Arkanist Kel''danath am Werk. Es müssen noch mehr seiner Notizen verstreut sein.>', '', '', '', '', '', '', 22908),
(44543, 'deDE', 'Die Schlacht um die Verheerte Küste', 'Nehmt den Windreiter zur Verheerten Küste.', 'Dieser Windreiter wird Euch zur Verheerten Küste bringen, $n. Möge Euch Eure Waffe heute viele Trophäen einbringen.\n\nDer Kriegshäuptling ist schon an Land gegangen. Ich denke, Ihr müsst einfach nur der Spur Dämonenblut folgen.', '', '', '', '', '', '', 22908),
(39838, 'deDE', 'Feuer!', 'Nutzt Camille Kleisters Wassereimer, um das Feuer zu löschen.', 'Ihr da! $gAbenteurer:Abenteurerin;! Packt hier mit an! Der Wagen hat Feuer gefangen!', '', '', '', '', '', '', 22908),
(38567, 'deDE', 'Garnisonskampagne: Kriegsrat', 'Sprecht mit Kriegshäuptling Vol''jin im Hauptgebäude Eurer Garnison.', 'Eure Garnison befindet sich in Aufruhr. Kriegshäuptling Vol''jin persönlich ist zu Besuch in Eurer Festung, um über die Invasionspläne für den Tanaandschungel zu sprechen.$b$bDer Kriegshäuptling wartet auf Euch im Hauptgebäude. Es wäre klug, ihn nicht allzu lange warten zu lassen...', '', 'Der Kriegshäuptling wartet.', '', '', '', 'Sprecht mit Kriegshäuptling Vol''jin im Hauptgebäude Eurer Garnison.', 22908);

UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=29624 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=29623 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=29627 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=29620 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=29619 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=30151 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=30142 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=30137 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=30136 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=30135 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=29881 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=29882 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=33543 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestDescription`='Habt Ihr die Pilze draußen im Wasser gesehen? Sie wachsen schneller als ein Babyravasaurier aus Un''Goro, und wenn wir sie nicht unter Kontrolle bekommen, wird die Werkstatt genauso ruiniert wie unser letztes Dorf.$b$bOnkel Krixel hat mir immer gesagt, man soll die roten Pilze verbrennen, weil die für das schnelle Wachstum verantwortlich sind.$b$bIhr könntet uns helfen, indem Ihr einen Flammenwerfer nehmt und einige der Sporen im Watt da unten vernichtet.', `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=35077 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=34655 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=33735 AND `locale`='deDE');
UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=22908 WHERE (`ID`=34965 AND `locale`='deDE');

SET NAMES 'latin1';
SET NAMES 'utf8';
DELETE FROM `quest_objectives_locale` WHERE (`ID`=286382 AND `locale`='deDE') OR (`ID`=282759 AND `locale`='deDE') OR (`ID`=281968 AND `locale`='deDE') OR (`ID`=280687 AND `locale`='deDE') OR (`ID`=286696 AND `locale`='deDE') OR (`ID`=280009 AND `locale`='deDE') OR (`ID`=280008 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(286382, 'deDE', 41760, 1, 'Verdorrte bezaubert', 22908),
(282759, 'deDE', 41704, 0, 'Bringt Theryn zu Thalyssra', 22908),
(281968, 'deDE', 41149, 0, 'Arkane Zauberschutze aktiviert', 22908),
(280687, 'deDE', 40326, 0, 'Kel''danaths Notizen gelesen', 22908),
(286696, 'deDE', 44543, 0, 'Die Verheerte Küste angegriffen', 22908),
(280009, 'deDE', 39838, 1, 'Teufelsflammen gelöscht', 22908),
(280008, 'deDE', 39838, 0, 'Feuer gelöscht', 22908);


SET NAMES 'latin1';
