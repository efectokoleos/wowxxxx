DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_hun_legion_a_murder_of_crows',
'spell_hun_legion_aimed_shot',
'spell_hun_legion_aimed_shot_blocker',
'spell_hun_legion_animal_instincts',
'spell_hun_legion_arcane_shot',
'spell_hun_legion_aspect_cheetah',
'spell_hun_legion_aspect_of_the_turtle',
'spell_hun_legion_aspect_of_the_wild',
'spell_hun_legion_barrage',
'spell_hun_legion_basic_pet_attacks',
'spell_hun_legion_beast_cleave',
'spell_hun_legion_beast_cleave_damage', 
'spell_hun_legion_bestial_wrath',
'spell_hun_legion_bird_of_prey',
'spell_hun_legion_black_arrow_proc',
'spell_hun_legion_blink_strikes',
'spell_hun_legion_bullseye',
'spell_hun_legion_butchery',
'spell_hun_legion_bursting_shot',
'spell_hun_legion_chimaera_shot',
'spell_hun_legion_call_of_the_hunter',
'spell_hun_legion_call_pet',
'spell_hun_legion_camouflage',
'spell_hun_legion_careful_aim',
'spell_hun_legion_carve',
'spell_hun_legion_cobra_shot',
'spell_hun_legion_critical_focus',
'spell_hun_legion_dark_whisper',
'spell_hun_legion_dire_beast',
'spell_hun_legion_disengage',
'spell_hun_legion_dragonsfire_conflagration',
'spell_hun_legion_dragonsfire_grenade',
'spell_hun_legion_dragonscale_armor',
'spell_hun_legion_dire_frenzy',
'spell_hun_legion_exhilaration',
'spell_hun_legion_explosive_shot',
'spell_hun_legion_explosive_shot_detonate',
'spell_hun_legion_explosive_trap_damage',
'spell_hun_legion_farstrider',
'spell_hun_legion_flanking_strike',
'spell_hun_legion_fluffy_go',
'spell_hun_legion_freezing_trap',
'spell_hun_legion_freezing_trap_honor',
'spell_hun_legion_fury_of_the_eagle',
'spell_hun_legion_hardiness',
'spell_hun_legion_hati_s_bond',
'spell_hun_legion_heart_of_the_phoenix',
'spell_hun_legion_hunter_s_advantage',
'spell_hun_legion_hunter_s_bounty',
'spell_hun_legion_hunting_companion',
'spell_hun_legion_hunting_companion_proc',
'spell_hun_legion_hunting_party',
'spell_hun_legion_intimidation',
'spell_hun_legion_kill_command',
'spell_hun_legion_kill_command_damage',
'spell_hun_legion_killer_cobra',
'spell_hun_legion_last_stand',
'spell_hun_legion_legacy_of_the_windrunners',
'spell_hun_legion_lone_wolf',
'spell_hun_legion_mark_of_the_windrunner',
'spell_hun_legion_marked_shot',
'spell_hun_legion_marksmanship_hunter',
'spell_hun_legion_masters_call',
'spell_hun_legion_mimiron_s_shell',
'spell_hun_legion_misdirection',
'spell_hun_legion_misdirection_proc',
'spell_hun_legion_mortal_wounds',
'spell_hun_legion_multi_shot',
'spell_hun_legion_mongoose_bite',
'spell_hun_legion_natural_reflexes',
'spell_hun_legion_on_the_trail',
'spell_hun_legion_piercing_shot',
'spell_hun_legion_posthaste',
'spell_hun_legion_roar_of_sacrifice',
'spell_hun_legion_scatter_shot',
'spell_hun_legion_sentinel',
'spell_hun_legion_separation_anxiety',
'spell_hun_legion_serpent_sting',
'spell_hun_legion_sidewinders',
'spell_hun_legion_snake_hunter',
'spell_hun_legion_sparring',
'spell_hun_legion_spider_sting',
'spell_hun_legion_stampede_periodic',
'spell_hun_legion_steady_focus',
'spell_hun_legion_steel_trap_root',
'spell_hun_legion_sticky_bomb',
'spell_hun_legion_stomp',
'spell_hun_legion_surge_of_the_stormgod',
'spell_hun_legion_survivalist',
'spell_hun_legion_talon_strike',
'spell_hun_legion_terms_of_engagement',
'spell_hun_legion_throwing_axes',
'spell_hun_legion_titan_s_thunder',
'spell_hun_legion_titan_s_thunder_periodic',
'spell_hun_legion_trailblazer',
'spell_hun_legion_trick_shot',
'spell_hun_legion_thunderstomp',
'spell_hun_legion_viper_sting',
'spell_hun_legion_volley',
'spell_hun_legion_vulnerable',
'spell_hun_legion_wild_call',
'spell_hun_legion_wild_protector',
'spell_hun_legion_windburst'
);

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(131894, 'spell_hun_legion_a_murder_of_crows'),
(206505, 'spell_hun_legion_a_murder_of_crows'),
(19434,  'spell_hun_legion_aimed_shot'),
(236641, 'spell_hun_legion_aimed_shot_blocker'),
(204315, 'spell_hun_legion_animal_instincts'),
(185358, 'spell_hun_legion_arcane_shot'),
(186257, 'spell_hun_legion_aspect_cheetah'),
(186265, 'spell_hun_legion_aspect_of_the_turtle'),
(193530, 'spell_hun_legion_aspect_of_the_wild'),
(120360, 'spell_hun_legion_barrage'),
(16827,  'spell_hun_legion_basic_pet_attacks'),
(17253,  'spell_hun_legion_basic_pet_attacks'),
(49966,  'spell_hun_legion_basic_pet_attacks'),
(242735, 'spell_hun_legion_basic_pet_attacks'),
(118455, 'spell_hun_legion_beast_cleave'),
(118459, 'spell_hun_legion_beast_cleave_damage'),
(19574,  'spell_hun_legion_bestial_wrath'),
(224764, 'spell_hun_legion_bird_of_prey'),
(214351, 'spell_hun_legion_black_arrow_proc'),
(16827,  'spell_hun_legion_blink_strikes'),
(17253,  'spell_hun_legion_blink_strikes'),
(49966,  'spell_hun_legion_blink_strikes'),
(145625, 'spell_hun_legion_blink_strikes'),
(242735, 'spell_hun_legion_blink_strikes'),
(204089, 'spell_hun_legion_bullseye'),
(212436, 'spell_hun_legion_butchery'),
(186387, 'spell_hun_legion_bursting_shot'),
(191048, 'spell_hun_legion_call_of_the_hunter'),
(883,    'spell_hun_legion_call_pet'),
(83242,  'spell_hun_legion_call_pet'),
(83243,  'spell_hun_legion_call_pet'),
(83244,  'spell_hun_legion_call_pet'),
(83245,  'spell_hun_legion_call_pet'),
(199483, 'spell_hun_legion_camouflage'),
(53238,  'spell_hun_legion_careful_aim'),
(187708, 'spell_hun_legion_carve'),
(53209,  'spell_hun_legion_chimaera_shot'),
(193455, 'spell_hun_legion_cobra_shot'),
(191328, 'spell_hun_legion_critical_focus'),
(204683, 'spell_hun_legion_dark_whisper'),
(120679, 'spell_hun_legion_dire_beast'),
(781,    'spell_hun_legion_disengage'),
(194859, 'spell_hun_legion_dragonsfire_conflagration'),
(194858, 'spell_hun_legion_dragonsfire_grenade'),
(202589, 'spell_hun_legion_dragonscale_armor'),
(217200, 'spell_hun_legion_dire_frenzy'),
(109304, 'spell_hun_legion_exhilaration'),
(212680, 'spell_hun_legion_explosive_shot'),
(212679, 'spell_hun_legion_explosive_shot_detonate'),
(13812,  'spell_hun_legion_explosive_trap_damage'),
(199564, 'spell_hun_legion_farstrider'),
(202800, 'spell_hun_legion_flanking_strike'),
(203669, 'spell_hun_legion_fluffy_go'),
(3355,   'spell_hun_legion_freezing_trap'),
(203337, 'spell_hun_legion_freezing_trap_honor'),
(203413, 'spell_hun_legion_fury_of_the_eagle'),
(232047, 'spell_hun_legion_hardiness'),
(197344, 'spell_hun_legion_hati_s_bond'),
(55709,  'spell_hun_legion_heart_of_the_phoenix'),
(197178, 'spell_hun_legion_hunter_s_advantage'),
(203749, 'spell_hun_legion_hunter_s_bounty'),
(191336, 'spell_hun_legion_hunting_companion'),
(191335, 'spell_hun_legion_hunting_companion_proc'),
(212658, 'spell_hun_legion_hunting_party'),
(19577,  'spell_hun_legion_intimidation'),
(34026,  'spell_hun_legion_kill_command'),
(83381,  'spell_hun_legion_kill_command_damage'),
(199532, 'spell_hun_legion_killer_cobra'),
(53478,  'spell_hun_legion_last_stand'),
(190852, 'spell_hun_legion_legacy_of_the_windrunners'),
(155228, 'spell_hun_legion_lone_wolf'),
(204219, 'spell_hun_legion_mark_of_the_windrunner'),
(185901, 'spell_hun_legion_marked_shot'),
(137016, 'spell_hun_legion_marksmanship_hunter'),
(53271,  'spell_hun_legion_masters_call'),
(197160, 'spell_hun_legion_mimiron_s_shell'),
(34477,  'spell_hun_legion_misdirection'),
(35079,  'spell_hun_legion_misdirection_proc'),
(201091, 'spell_hun_legion_mortal_wounds'),
(2643,   'spell_hun_legion_multi_shot'),
(190928, 'spell_hun_legion_mongoose_bite'),
(197138, 'spell_hun_legion_natural_reflexes'),
(204081, 'spell_hun_legion_on_the_trail'),
(198670, 'spell_hun_legion_piercing_shot'),
(781,    'spell_hun_legion_posthaste'),
(190925, 'spell_hun_legion_posthaste'),
(53480,  'spell_hun_legion_roar_of_sacrifice'),
(213691, 'spell_hun_legion_scatter_shot'),
(206817, 'spell_hun_legion_sentinel'),
(213875, 'spell_hun_legion_separation_anxiety'),
(186270, 'spell_hun_legion_serpent_sting'),
(187708, 'spell_hun_legion_serpent_sting'),
(214579, 'spell_hun_legion_sidewinders'),
(201078, 'spell_hun_legion_snake_hunter'),
(232045, 'spell_hun_legion_sparring'),
(202914, 'spell_hun_legion_spider_sting'),
(201631, 'spell_hun_legion_stampede_periodic'),
(193533, 'spell_hun_legion_steady_focus'),
(162480, 'spell_hun_legion_steel_trap_root'),
(191241, 'spell_hun_legion_sticky_bomb'),
(201754, 'spell_hun_legion_stomp'),
(197354, 'spell_hun_legion_surge_of_the_stormgod'),
(164857, 'spell_hun_legion_survivalist'),
(203563, 'spell_hun_legion_talon_strike'),
(203754, 'spell_hun_legion_terms_of_engagement'),
(200163, 'spell_hun_legion_throwing_axes'),
(207081, 'spell_hun_legion_titan_s_thunder'),
(207094, 'spell_hun_legion_titan_s_thunder_periodic'),
(199921, 'spell_hun_legion_trailblazer'),
(199885, 'spell_hun_legion_trick_shot'),
(63900,  'spell_hun_legion_thunderstomp'),
(202797, 'spell_hun_legion_viper_sting'),
(194386, 'spell_hun_legion_volley'),
(187131, 'spell_hun_legion_vulnerable'),
(185791, 'spell_hun_legion_wild_call'),
(204190, 'spell_hun_legion_wild_protector'),
(204147, 'spell_hun_legion_windburst');

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (4424, 4435, 4436, 510, 1524, 5754, 7019, 9769, 8284, 6026, 1613, 5084, 2392, 9810, 6486, 6237);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `MoveCurveId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `VerifiedBuild`) VALUES
(4424, 9170, 0, 0, 0, 0, 0, 0, 60000, 23420), -- SpellId : 187651
(4435, 9181, 0, 0, 0, 0, 0, 0, 60000, 23420), -- SpellId : 187699
(4436, 3841, 0, 0, 0, 0, 0, 0, 30000, 23420), -- SpellId : 187700
(510, 3678, 0, 0, 0, 0, 0, 0, 20000, 23420), -- SpellId : 132950
(1524, 5972, 0, 0, 0, 0, 0, 0, 10000, 23420), -- SpellId : 109248
(5754, 10466, 1834, 0, 0, 0, 0, 2250, 2250, 23420), -- SpellId : 201591, 201610
(7019, 11492, 1843, 0, 0, 0, 0, 4000, 4000, 23420), -- SpellId : 212431
(9769, 50018, 0, 0, 0, 0, 0, 0, 0, 0), -- SpellId : 206817
(8284, 6197, 0, 0, 0, 0, 0, 0, 5000, 23360), -- SpellId : 223114 -- dest
(6026, 10713, 0, 0, 0, 0, 0, 0, 5000, 23360), -- SpellId : 204475 -- target enemy
(1613, 6094, 0, 0, 0, 0, 0, 0, 60000, 23420), -- SpellId : 13813
(5084, 9805, 0, 0, 0, 0, 0, 0, 15000, 23420), -- SpellId : 194278
(2392, 6966, 0, 0, 0, 0, 0, 0, 60000, 22522), -- SpellId : 162496
(9810, 14725, 0, 0, 0, 0, 0, 0, 60000, 23420), -- SpellId : 236775
(6486, 11112, 0, 0, 0, 0, 0, 0, 10000, 22624), -- SpellId : 208652
(6237, 50019, 0, 0, 0, 0, 0, 0, 0, 0); -- SpellId : 204358 

DELETE FROM `areatrigger_template` WHERE `Id` IN (50018, 14725, 11112, 50019);
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `AIName`, `ScriptName`, `VerifiedBuild`) VALUES
(50018, 0, 0, 10, 10, 0, 0, 0, 0, '', 'areatrigger_hun_legion_sentinel', 0),
(14725, 4, 0, 5, 5, 5, 5, 1, 1, '', 'areatrigger_hun_legion_hi_explosive_trap', 23420),
(11112, 4, 0, 10, 10, 10, 10, 0.3, 0.3, '', '', 22624),
(50019, 0, 0x4, 8, 8, 0, 0, 0, 0, '', 'areatrigger_hun_legion_wild_protector', 0);

UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_freezing_trap' WHERE Id = 9170;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_tar_trap' WHERE Id = 9181;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_flare' WHERE Id = 3678;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_binding_shot' WHERE Id = 5972;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_explosive_shot' WHERE Id = 11492;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_windburst' WHERE Id = 6197;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_explosive_trap' WHERE Id = 6094;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_caltrops' WHERE Id = 9805;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_steel_trap' WHERE Id = 6966;
UPDATE areatrigger_template SET ScriptName = 'areatrigger_hun_legion_tar_trap_slow' WHERE Id = 3841;

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (3841, 10466, 6197, 10713, 11112);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES
(3841, 1, 135299, 2),
(10466, 0, 201594, 2),
(11112, 0, 208684, 2);

DELETE FROM `spell_areatrigger_scale` WHERE `SpellMiscId` IN (510, 8284, 6026, 5084, 6486);
INSERT INTO `spell_areatrigger_scale` (`SpellMiscId`, `ScaleCurveExtra5`, `ScaleCurveExtra6`, `VerifiedBuild`) VALUES 
(510, 1, 1, 24015),
(8284, 1, 1, 24015),
(6026, 1, 1, 24015),
(5084, 1, 1, 24015),
(6486, 1, 1, 22624); 

DELETE FROM `spell_areatrigger_splines` WHERE `SpellMiscId` IN (5754, 7019); -- copy from frozen orbs validate with a working spline parser :D
INSERT INTO `spell_areatrigger_splines` (`SpellMiscId`, `Idx`, `X`, `Y`, `Z`, `VerifiedBuild`) VALUES
(5754, 14, 40.08189, 0.0001946589, 0.00, 22522),
(5754, 13, 40.08189, 0.0001946589, 0.00, 22522),
(5754, 12, 37.08167, 0.0001410497, 0.00, 22522),
(5754, 11, 34.0818, 0.0001125781, 0.00, 22522),
(5754, 10, 31.08175, 0.000243713, 0.00, 22522),
(5754, 9, 28.08188, 0.0002152414, 0.00, 22522),
(5754, 8, 25.08166, 0.0001616322, 0.00, 22522),
(5754, 7, 22.08179, 0.0001331606, 0.00, 22522),
(5754, 6, 19.08174, 0.0002642955, 0.00, 22522),
(5754, 5, 16.08187, 0.0002358239, 0.00, 22522),
(5754, 4, 13.08165, 0.0001822146, 0.00, 22522),
(5754, 3, 10.08178, 0.0001537431, 0.00, 22522),
(5754, 2, 7.081728, 0.000284878, 0.00, 22522),
(5754, 1, 4.081857, 0.0002564064, 0.00, 22522),
(5754, 0, 1.081643, 0.0002027971, 0.00, 22522),
(7019, 14, 40.08189, 0.0001946589, 0.00, 22522),
(7019, 13, 40.08189, 0.0001946589, 0.00, 22522),
(7019, 12, 37.08167, 0.0001410497, 0.00, 22522),
(7019, 11, 34.0818, 0.0001125781, 0.00, 22522),
(7019, 10, 31.08175, 0.000243713, 0.00, 22522),
(7019, 9, 28.08188, 0.0002152414, 0.00, 22522),
(7019, 8, 25.08166, 0.0001616322, 0.00, 22522),
(7019, 7, 22.08179, 0.0001331606, 0.00, 22522),
(7019, 6, 19.08174, 0.0002642955, 0.00, 22522),
(7019, 5, 16.08187, 0.0002358239, 0.00, 22522),
(7019, 4, 13.08165, 0.0001822146, 0.00, 22522),
(7019, 3, 10.08178, 0.0001537431, 0.00, 22522),
(7019, 2, 7.081728, 0.000284878, 0.00, 22522),
(7019, 1, 4.081857, 0.0002564064, 0.00, 22522),
(7019, 0, 1.081643, 0.0002027971, 0.00, 22522);

DELETE FROM `spell_proc` WHERE `SpellId` IN (185789, 199523, 199532, 197160, 197178, 197354, 236641, 35110, 223138, 193533, 53238, 199527, 199483, 191328, 204219, 190503, 191339, 204089, 190852, 191048,
204315, 201082, 201075, 162480, 203754, 224764, 203749, 203755, 203757, 202624, 202746, 202914, 203129, 203133);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`, `SpellTypeMask`) VALUES
(185789,  0x1, 0,     0x0,      0x0,        0x0,  0x0,   0x0, 0x2, 0x2, 0x0), -- Wild Call only procs on critical auto attack
(199523,  0x0, 9,     0x0,      0x0,        0x0,  0x0,   0x0, 0x2, 0x2, 0x1), -- Farstrider
(199532,  0x1, 9,     0x0,      0x0,   0x400000,  0x0,   0x0, 0x1, 0x0, 0x0), -- Killer Cobra
(197160,  0x0, 9,  0x8000,      0x0,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- Mimiron's Shell
(197178,  0x0, 9,     0x0, 0x100000,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- Hunter's Advantage
(197354,  0x1, 9,  0x1000,      0x0,        0x0,  0x0,   0x0, 0x4, 0x0, 0x0), -- Surge of the Stormgod
(236641,  0x0, 9,     0x0,      0x0,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- Aimed Shot blocker
(35110,   0x1, 9,  0x1000,      0x0,        0x0,  0x0,   0x0, 0x2, 0x2, 0x0), -- Bombardment
(223138,  0x0, 9,  0x1800,      0x0,        0x0, 0x80,   0x0, 0x4, 0x0, 0x0), -- Marking Targets
(193533,  0x0, 9,     0x0,      0x0,        0x0,  0x0,   0x0, 0x4, 0x0, 0x0), -- Steady Focus
(53238,   0x0, 9, 0x21820,      0x0,        0x0,  0x0,   0x0, 0x2, 0x2, 0x0), -- Careful Aim
(199527,  0x0, 9, 0x20800,      0x0,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- True Aim
(199483,  0x0, 0,     0x0,      0x0,        0x0,  0x0,   0x0, 0x0, 0x0, 0x1), -- Camouflage
(191328, 0x64, 9,   0x800,      0x0,        0x0,  0x0,   0x0, 0x2, 0x2, 0x1), -- Critical Focus
(204219,  0x1, 9,     0x0,      0x0, 0x20000000,  0x0,   0x0, 0x2, 0x0, 0x1), -- Mark of the Windrunner
(190503,  0x0, 9,  0x8000,      0x0,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- Aspect of the Turtle
(191339,  0x0, 9,     0x0, 0x200000,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- Rapid Killing
(204089,  0x0, 9,     0x0,      0x0,        0x0,  0x0,   0x0, 0x2, 0x0, 0x1), -- Bullseye
(190852,  0x1, 9, 0x20000,      0x0,        0x0,  0x0,   0x0, 0x2, 0x0, 0x1), -- Legacy of the Windrunners
(191048,  0x0, 9,     0x2,      0x0,        0x0,  0x0,   0x0, 0x1, 0x0, 0x0), -- Call of the Hunter
(204315,  0x1, 9,     0x0,      0x0,      0x100,  0x0,   0x0, 0x4, 0x0, 0x0), -- Animal Instincts
(201082,  0x1, 9,     0x0,      0x0,    0x10000,  0x0,   0x0, 0x4, 0x0, 0x0), -- Way of the Mok'Nathal
(201075,  0x1, 9,     0x0,     0x80,        0x0,  0x0,   0x0, 0x0, 0x0, 0x1), -- Mortal Wounds
(162480,  0x0, 0,     0x0,      0x0,        0x0,  0x0,   0x0, 0x0, 0x0, 0x1), -- Steel trap root
(224764,  0x1, 9,     0x0,      0x0,    0x10000,  0x0,   0x0, 0x2, 0x0, 0x1), -- Bird of Prey
(203749,  0x0, 0,     0x0,      0x0,        0x0,  0x0,   0x2, 0x0, 0x0, 0x0), -- Hunter's Bounty - Should only have "01 Kill that yields experience or honor"
(203755,  0x0, 9,     0x0,      0x0,     0x2000,  0x0,   0x0, 0x2, 0x0, 0x0), -- Aspect of the Skylord
(203757,  0x0, 9,     0x0,  0x40000,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- Eagle's Bite
(202624,  0x0, 0,     0x0,      0x0,        0x0,  0x0,   0x0, 0x2, 0x2, 0x1), -- Catlike Reflexes
(202746,  0x0, 9,   0x100,      0x0,        0x0,  0x0, 0x400, 0x1, 0x0, 0x0), -- Survival Tactics
(202914,  0x0, 0,     0x0,      0x0,        0x0,  0x0,   0x0, 0x1, 0x0, 0x0), -- Spider Sting
(203129,  0x0, 9,     0x0, 0x200000,        0x0,  0x0,   0x0, 0x2, 0x0, 0x0), -- Trueshot Mastery 
(203133,  0x1, 9,     0x0,      0x0,       0x40,  0x0,   0x0, 0x2, 0x0, 0x1); -- Spray and Pray

-- Custom Procs
DELETE FROM `spell_proc` WHERE `SpellId` IN (203754, 203563, 202797);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`, `SpellTypeMask`, `Chance`, `Charges`) VALUES
(203754, 0x0, 0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 100, 0), -- Terms of Engagement - procs on killed unit
(203563, 0x0, 0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x2, 0x0, 0x0, 10, 0), -- Talon Strike - 10% chance - should only have "02 Successful melee attack" since there is no melee attack override spell for this spec
(202797, 0x0, 0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x2, 0, 1); -- Viper Sting

UPDATE creature_template SET AIName = '', ScriptName = 'npc_hun_legion_dire_beast' WHERE entry IN (62865,62855,62856,62857,62858,62860,62210,64617,64618,64619,64620,62005,69989,69990,69990,69989,74139,86187,86186,86653,86183,86181,86180,62858,86177,103244,103248,103249,103250,103251,103252,103253,103254,103255,103256,103257,103258,103259,103260,103261,103262,103263,103264,103265,103266,103267,103309,103268,103269,103270,103301,103311,103313,103314,103315,103316,103317,103318,103319,103320,103321,103323,103324,103327,103329,103331,103332,103333,103335,103336,103351,103352,103353,103354,103356,103357,103358,103359,103361,103364,103365,103366,103367,103368,103369,103370,103374,103375,103376,103377,103379,103382,103383,103385,103387,103390,103391,103392,103393,103393,106971,106972,106975,106977,106978,106988,106989,110063,128751,113342,113347,113343,113344,113346);
UPDATE creature_template SET flags_extra = 0x82, ScriptName = 'npc_hun_legion_stampede' WHERE entry = 102199;
UPDATE creature_template SET AIName = 'PetAI' WHERE entry IN (100324, 106548, 106549, 106550, 106551);
UPDATE creature_template SET AIName = '', ScriptName = 'npc_hun_legion_dark_minion' WHERE entry = 94072;
UPDATE creature_template SET AIName = '', ScriptName = 'npc_hun_legion_spitting_cobra' WHERE entry = 104493;
