DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_legion_honor_adaptation',
'spell_legion_honor_defender_of_the_weak',
'spell_legion_honor_hardiness',
'spell_legion_honor_medallion',
'spell_legion_honor_softened_blows',
'spell_legion_honor_sparring',
'spell_legion_honor_train_of_thought',

-- Other Generic Spells
'spell_gen_legion_cc_remove_dots',
'spell_gen_legion_racial_touch_of_elune'
);

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(214027, 'spell_legion_honor_adaptation'),
(195330, 'spell_legion_honor_defender_of_the_weak'),
(195416, 'spell_legion_honor_hardiness'),
(232047, 'spell_legion_honor_hardiness'),
(42292,  'spell_legion_honor_medallion'),
(195710, 'spell_legion_honor_medallion'),
(208683, 'spell_legion_honor_medallion'),
(195389, 'spell_legion_honor_softened_blows'),
(195425, 'spell_legion_honor_sparring'),
(232045, 'spell_legion_honor_sparring'),
(232245, 'spell_legion_honor_sparring'),
(195637, 'spell_legion_honor_train_of_thought'),
(213542, 'spell_legion_honor_train_of_thought'),
(213543, 'spell_legion_honor_train_of_thought'),
(213545, 'spell_legion_honor_train_of_thought'),
(213548, 'spell_legion_honor_train_of_thought'),
(213551, 'spell_legion_honor_train_of_thought'),
(213554, 'spell_legion_honor_train_of_thought'),

-- Other Generic Spells
(118, 'spell_gen_legion_cc_remove_dots'),
(2094, 'spell_gen_legion_cc_remove_dots'),
(31661, 'spell_gen_legion_cc_remove_dots'),
(105421, 'spell_gen_legion_cc_remove_dots'),
(154784, 'spell_gen_legion_racial_touch_of_elune');

-- 214027 Adapation
DELETE FROM `spell_proc` WHERE `SpellId` = 214027;
INSERT INTO `spell_proc` (`SpellId`, `HitMask`) VALUES
(214027, 0x3FFF);
