UPDATE `creature_model_info` SET `BoundingRadius`=0.2635, `VerifiedBuild`=23420 WHERE `DisplayID`=15506;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69390;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72484;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=67216;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69336;

UPDATE `quest_template` SET `RewardMoney`=154000 WHERE `ID`=38014; -- Feasting on the Dragon
UPDATE `quest_template` SET `RewardMoney`=154000 WHERE `ID`=37856; -- The Withered
UPDATE `quest_template` SET `RewardMoney`=308000 WHERE `ID`=37656; -- Fel Machinations
UPDATE `quest_template` SET `RewardMoney`=194000 WHERE `ID`=31316; -- Julia, The Pet Tamer

UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=102104; -- Enslaved Shieldmaiden
UPDATE `creature_template` SET `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=97163; -- Cursed Falke
UPDATE `creature_template` SET `unit_flags`=2147746560, `VerifiedBuild`=23420 WHERE `entry`=48959; -- Rusty Anvil
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=2875, `unit_flags`=33280 WHERE `entry`=101146; -- Orgrimmar Grunt
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=33556480 WHERE `entry`=101019; -- Orgrimmar Grunt
UPDATE `creature_template` SET `maxlevel`=6, `VerifiedBuild`=23420 WHERE `entry`=15651; -- Springpaw Stalker
UPDATE `creature_template` SET `npcflag`=640, `speed_walk`=1, `unit_flags`=512, `VerifiedBuild`=23420 WHERE `entry`=18954; -- Sailor Melinan
UPDATE `creature_template` SET `speed_walk`=1, `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=15921; -- Captain Kelisendra
UPDATE `creature_template` SET `speed_walk`=1, `unit_flags`=512, `VerifiedBuild`=23420 WHERE `entry`=15404; -- Velendris Whitemorn
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=93619; -- Infernal Brutalizer
UPDATE `creature_template` SET `npcflag`=0, `spell1`=0, `spell2`=0, `spell3`=0, `spell4`=0, `VerifiedBuild`=23420 WHERE `entry`=28406; -- Death Knight Initiate
UPDATE `creature_template` SET `npcflag`=0, `spell1`=0, `spell2`=0, `VerifiedBuild`=23420 WHERE `entry`=28606; -- Havenshire Mare
UPDATE `creature_template` SET `npcflag`=0, `spell1`=0, `spell2`=0, `VerifiedBuild`=23420 WHERE `entry`=28607; -- Havenshire Colt
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=28683; -- Stable Master Kitrik
UPDATE `creature_template` SET `speed_run`=1.385714, `VerifiedBuild`=23420 WHERE `entry`=28611; -- Scarlet Captain
UPDATE `creature_template` SET `minlevel`=35, `maxlevel`=49, `VerifiedBuild`=23420 WHERE `entry`=28576; -- Citizen of Havenshire
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=23420 WHERE `entry`=91823; -- Fel Pup
UPDATE `creature_template` SET `minlevel`=23, `maxlevel`=23 WHERE `entry`=42421; -- Stormwind Fisherman
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23420 WHERE `entry`=14561; -- Swift Brown Steed
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1 WHERE `entry`=14560; -- Swift White Steed
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2 WHERE `entry`=14559; -- Swift Palomino
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `VerifiedBuild`=23420 WHERE `entry`=4269; -- Chestnut Mare
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=99349; -- Robert "Chance" Llore
UPDATE `creature_template` SET `minlevel`=99 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `npcflag`=17, `VerifiedBuild`=23420 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=107 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=23420 WHERE `entry`=92464; -- Kuhuine Tenderstride
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=23420 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=23420 WHERE `entry`=92459; -- Dorothy Egan
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110 WHERE `entry`=112079; -- Crimson Pilgrim
