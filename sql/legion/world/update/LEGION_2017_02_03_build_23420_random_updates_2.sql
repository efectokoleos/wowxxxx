DELETE FROM `scene_template` WHERE (`SceneId`=1341 AND `ScriptPackageID`=1667) OR (`SceneId`=1472 AND `ScriptPackageID`=1746) OR (`SceneId`=1327 AND `ScriptPackageID`=1657) OR (`SceneId`=1324 AND `ScriptPackageID`=1653) OR (`SceneId`=1328 AND `ScriptPackageID`=1658) OR (`SceneId`=1301 AND `ScriptPackageID`=1641);
INSERT INTO `scene_template` (`SceneId`, `Flags`, `ScriptPackageID`) VALUES
(1341, 16, 1667),
(1472, 25, 1746),
(1327, 25, 1657),
(1324, 11, 1653),
(1328, 17, 1658),
(1301, 27, 1641);

DELETE FROM `quest_offer_reward` WHERE `ID` IN (44052 /*And They Will Tremble*/, 42792 /*Make Your Mark*/, 42841 /*A Big Score*/, 43352 /*Asset Security*/, 42840 /*If Words Don't Work...*/, 43969 /*Hired Help*/, 42839 /*Seek the Unsavory*/, 44084 /*Vengeance for Margaux*/, 42838 /*Reversal*/, 42836 /*Silkwing Sabotage*/, 42837 /*Balance to Spare*/, 42835 /*The Old Fashioned Way*/, 42834 /*Intense Concentration*/, 42833 /*How It's Made: Arcwine*/, 42832 /*The Fruit of Our Efforts*/, 42829 /*Make an Entrance*/, 42828 /*Moths to a Flame*/, 42489 /*Thalyssra's Drawers*/, 42488 /*Thalyssra's Abode*/, 44171 /*-Unknown-*/, 42487 /*Friends On the Outside*/, 44051 /*Wasted Potential*/, 42486 /*Little One Lost*/, 42722 /*Friends in Cages*/, 40745 /*Shift Change*/, 40947 /*Special Delivery*/, 40727 /*All Along the Waterways*/, 40730 /*Redistribution*/, 41878 /*The Gondolier*/, 41148 /*Dispensing Compassion*/, 40746 /*One of the People*/, 41877 /*Lady Lunastre*/, 43907 /*Into the Nightmare: Xavius*/, 44636 /*Building an Army*/, 44561 /*Seed of Hope*/, 42230 /*The Valewalker's Burden*/, 42228 /*The Hidden City*/, 42227 /*Into the Crevasse*/, 42226 /*Moonshade Holdout*/, 42225 /*Breaking the Seal*/, 42224 /*Cloaked in Moonshade*/, 40325 /*Scenes from a Memory*/, 40324 /*Arcane Communion*/, 45056 /*-Unknown-*/, 40798 /*Cling to Hope*/, 42147 /*First Contact*/, 42079 /*Masquerade*/, 41989 /*Blood of My Blood*/, 41834 /*The Masks We Wear*/, 43811 /*Lunastre Estate Teleporter Online!*/, 41762 /*Sympathizers Among the Shal'dorei*/, 43106 /*Feed Oculeth*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(44052, 1, 0, 0, 0, 0, 0, 0, 0, 'It is a relief, if a small one. You have done well - rest for now.', 23420), -- And They Will Tremble
(42792, 1, 0, 0, 0, 0, 0, 0, 0, 'They will not so easily dismiss the Dusk Lily now. Well done.', 23420), -- Make Your Mark
(42841, 1, 0, 0, 0, 0, 0, 0, 0, 'Pleasure doing business with you.', 23420), -- A Big Score
(43352, 1, 0, 0, 0, 0, 0, 0, 0, 'You''re becoming quite the dependable associate.', 23420), -- Asset Security
(42840, 11, 0, 0, 0, 0, 0, 0, 0, 'Glad to know you understand the value of my services, partner.', 23420), -- If Words Don't Work...
(43969, 1, 0, 0, 0, 0, 0, 0, 0, 'I will help you, but first I need you to do something for me.', 23420), -- Hired Help
(42839, 1, 0, 0, 0, 0, 0, 0, 0, 'Steel yourself, $n.$b$bMargaux''s loss is regrettable, but not likely to be the last in this effort. Far from it, I fear.', 23420), -- Seek the Unsavory
(44084, 274, 0, 0, 0, 0, 0, 0, 0, '<Thalyssra is speechless for a moment.>', 23420), -- Vengeance for Margaux
(42838, 18, 0, 0, 0, 0, 0, 0, 0, 'Margaux... no!', 23420), -- Reversal
(42836, 66, 0, 0, 0, 0, 0, 0, 0, 'Much better. You have my thanks.', 23420), -- Silkwing Sabotage
(42837, 603, 0, 0, 0, 0, 0, 0, 0, 'I will have these... ah... souvenirs... boxed for you.', 23420), -- Balance to Spare
(42835, 1, 0, 0, 0, 0, 0, 0, 0, 'I will be certain to have a bottle of your arcwine prepared for you before you leave.', 23420), -- The Old Fashioned Way
(42834, 1, 0, 0, 0, 0, 0, 0, 0, 'Simple, no?', 23420), -- Intense Concentration
(42833, 1, 0, 0, 0, 0, 0, 0, 0, 'Welcome, enjoying your tour?', 23420), -- How It's Made: Arcwine
(42832, 11, 0, 0, 0, 0, 0, 0, 0, 'You may want to cleanse your palate - we craft some strong flavors here!', 23420), -- The Fruit of Our Efforts
(42829, 1, 0, 0, 0, 0, 0, 0, 0, 'Let me show you around, then.', 23420), -- Make an Entrance
(42828, 1, 0, 0, 0, 0, 0, 0, 0, 'Hmm, yes. It is as I suspected.', 23420), -- Moths to a Flame
(42489, 0, 0, 0, 0, 0, 0, 0, 0, 'This is a comfort beyond words.', 23420), -- Thalyssra's Drawers
(42488, 0, 0, 0, 0, 0, 0, 0, 0, 'How lucky we are that it could be recovered... thank you.', 23420), -- Thalyssra's Abode
(44171, 0, 0, 0, 0, 0, 0, 0, 0, 'Well done, $n. I knew you could do it.', 23420), -- -Unknown-
(42487, 0, 0, 0, 0, 0, 0, 0, 0, 'Vanthir is a very pleasant fellow.', 23420), -- Friends On the Outside
(44051, 0, 0, 0, 0, 0, 0, 0, 0, 'I heard rumors that you intend to leave the City... that there is another place for the Nightborne where we are not beholden to Elisande''s insanity.$b$bI wish to go with you.$b$bI can offer my harp and my hands, as they are not wanted here.$b$bSay the word and I will go with you through the teleporter and share my music with whomever awaits me on the other side.', 23420), -- Wasted Potential
(42486, 273, 0, 0, 0, 0, 0, 0, 0, 'Oh my sweet little Korine! Thank you, thank you!', 23420), -- Little One Lost
(42722, 1, 0, 0, 0, 0, 0, 0, 0, 'You let them go? I''m glad. I didn''t like being in a cage either...', 23420), -- Friends in Cages
(40745, 18, 0, 0, 0, 0, 0, 0, 0, 'I-I thought... they were gonna... *sniff*', 23420), -- Shift Change
(40947, 1, 0, 0, 0, 0, 0, 0, 0, 'Fine work. I will read these over.', 23420), -- Special Delivery
(40727, 1, 66, 0, 0, 0, 700, 0, 0, 'Deline was able to deliver the goods safely, thanks to you.', 23420), -- All Along the Waterways
(40730, 1, 0, 0, 0, 0, 0, 0, 0, 'I will see that this gets back to our friend safely. Good work.', 23420), -- Redistribution
(41878, 0, 0, 0, 0, 0, 0, 0, 0, 'Let''s get started, shall we?', 23420), -- The Gondolier
(41148, 1, 0, 0, 0, 100, 0, 0, 0, 'Your eyes are opening to our reality. I can see it in your expression. Good.', 23420), -- Dispensing Compassion
(40746, 1, 0, 0, 0, 0, 0, 0, 0, 'Glad you could make it. Your help is sorely needed here.', 23420), -- One of the People
(41877, 1, 0, 0, 0, 0, 0, 0, 0, 'I hope you have prepared yourself to enter the City. It is time.', 23420), -- Lady Lunastre
(43907, 0, 0, 0, 0, 0, 0, 0, 0, 'Were you able to overcome the Emerald Nightmare?', 23420), -- Into the Nightmare: Xavius
(44636, 0, 0, 0, 0, 0, 0, 0, 0, 'The withered have been assembled and are ready for their training.', 23420), -- Building an Army
(44561, 0, 0, 0, 0, 0, 0, 0, 0, 'In the changing of seasons, the truth will be revealed.', 23420), -- Seed of Hope
(42230, 0, 0, 0, 0, 0, 0, 0, 0, '<The seed sits perfectly atop the stand and sinks in with a satisfying click.>', 23420), -- The Valewalker's Burden
(42228, 0, 0, 0, 0, 0, 0, 0, 0, '<Energy pulses around the ancient seed.>', 23420), -- The Hidden City
(42227, 0, 0, 0, 0, 0, 0, 0, 0, 'We must move swiftly.', 23420), -- Into the Crevasse
(42226, 0, 0, 0, 0, 0, 0, 0, 0, 'Do you have any idea what you have done?!', 23420), -- Moonshade Holdout
(42225, 0, 0, 0, 0, 0, 0, 0, 0, '<As the magical barrier begins to fade, you hear the rustling of leaves and the clattering of mandibles just outside.>', 23420), -- Breaking the Seal
(42224, 0, 0, 0, 0, 0, 0, 0, 0, '<You recognize the relic from Thalyssra''s vision.>', 23420), -- Cloaked in Moonshade
(40325, 0, 0, 0, 0, 0, 0, 0, 0, 'The withered... Not only do they suffer a burning thirst for the Nightwell, but their own memories torture them as well.$B$BTruly a fate worse than death...', 23420), -- Scenes from a Memory
(40324, 0, 0, 0, 0, 0, 0, 0, 0, 'Good. Let us see what we can learn.', 23420), -- Arcane Communion
(45056, 1, 0, 0, 0, 0, 0, 0, 0, 'I cannot possibly thank you enough. We will be reunited, and I will see we are never separated again.', 23420), -- -Unknown-
(40798, 18, 0, 0, 0, 0, 0, 0, 0, 'Absolon... is alive?', 23420), -- Cling to Hope
(42147, 0, 0, 0, 0, 0, 0, 0, 0, 'The Lunastre family is well-connected. They will be of great help to us when the time comes.', 23420), -- First Contact
(42079, 0, 0, 0, 0, 0, 0, 0, 0, 'My little $Gbrother:sister; is about to turn over a new leaf, whether $Ghe:she; likes it or not.', 23420), -- Masquerade
(41989, 0, 0, 0, 0, 0, 0, 0, 0, 'Thank you. Now our work can begin.', 23420), -- Blood of My Blood
(41834, 0, 0, 0, 0, 0, 0, 0, 0, 'That mask suits you.$B$BIn Suramar, it can be quite advantageous to be someone else for a time...', 23420), -- The Masks We Wear
(43811, 0, 0, 0, 0, 0, 0, 0, 0, '<The beacon glows as you infuse it with Ancient Mana.>', 23420), -- Lunastre Estate Teleporter Online!
(41762, 0, 0, 0, 0, 0, 0, 0, 0, 'We should be safe to speak here for a moment.', 23420), -- Sympathizers Among the Shal'dorei
(43106, 0, 0, 0, 0, 0, 0, 0, 0, '<As you offer the mana crystals, they blink out of your hand one by one.>', 23420); -- Feed Oculeth

DELETE FROM `quest_poi` WHERE (`QuestID`=42835 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=42835 AND `BlobIndex`=0 AND `Idx1`=3) OR (`QuestID`=42835 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=42834 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=42834 AND `BlobIndex`=0 AND `Idx1`=3) OR (`QuestID`=42833 AND `BlobIndex`=0 AND `Idx1`=5) OR (`QuestID`=42832 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=42828 AND `BlobIndex`=0 AND `Idx1`=2);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(42835, 0, 4, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1199869, 23420), -- The Old Fashioned Way
(42835, 0, 3, 31, 0, 0, 1220, 1033, 0, 0, 2, 0, 0, 0, 23420), -- The Old Fashioned Way
(42835, 0, 2, 1, 284855, 109205, 1220, 1033, 0, 0, 0, 0, 0, 1199793, 23420), -- The Old Fashioned Way
(42834, 0, 4, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1199869, 23420), -- Intense Concentration
(42834, 0, 3, 2, 284942, 108870, 1220, 1033, 0, 0, 2, 0, 0, 0, 23420), -- Intense Concentration
(42833, 0, 5, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1199374, 23420), -- How It's Made: Arcwine
(42832, 0, 2, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1195199, 23420), -- The Fruit of Our Efforts
(42828, 0, 2, 32, 0, 0, 1220, 1033, 0, 0, 2, 0, 0, 0, 23420); -- Moths to a Flame

UPDATE `quest_poi` SET `ObjectiveIndex`=0, `QuestObjectiveID`=284854, `QuestObjectID`=109204, `Flags`=0, `WoDUnk1`=1199225, `VerifiedBuild`=23420 WHERE (`QuestID`=42835 AND `BlobIndex`=0 AND `Idx1`=1); -- The Old Fashioned Way
UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=284937, `QuestObjectID`=109345, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `BlobIndex`=0 AND `Idx1`=2); -- Intense Concentration
UPDATE `quest_poi` SET `ObjectiveIndex`=0, `QuestObjectiveID`=286252, `QuestObjectID`=251556, `Flags`=0, `WoDUnk1`=1199981, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `BlobIndex`=0 AND `Idx1`=1); -- Intense Concentration
UPDATE `quest_poi` SET `Flags`=2, `WoDUnk1`=0, `VerifiedBuild`=23420 WHERE (`QuestID`=42829 AND `BlobIndex`=0 AND `Idx1`=4); -- Make an Entrance
DELETE FROM `quest_poi_points` WHERE (`QuestID`=42835 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=11) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=10) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=9) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=8) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=7) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=6) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=5) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=42835 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=42835 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=42834 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=11) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=10) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=9) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=8) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=7) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=6) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=5) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=42834 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=42833 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=42832 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=42828 AND `Idx1`=2 AND `Idx2`=0);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(42835, 4, 0, 1482, 3426, 23420), -- The Old Fashioned Way
(42835, 3, 11, 1350, 3271, 23420), -- The Old Fashioned Way
(42835, 3, 10, 1342, 3279, 23420), -- The Old Fashioned Way
(42835, 3, 9, 1342, 3290, 23420), -- The Old Fashioned Way
(42835, 3, 8, 1342, 3304, 23420), -- The Old Fashioned Way
(42835, 3, 7, 1357, 3304, 23420), -- The Old Fashioned Way
(42835, 3, 6, 1368, 3304, 23420), -- The Old Fashioned Way
(42835, 3, 5, 1379, 3304, 23420), -- The Old Fashioned Way
(42835, 3, 4, 1386, 3293, 23420), -- The Old Fashioned Way
(42835, 3, 3, 1386, 3286, 23420), -- The Old Fashioned Way
(42835, 3, 2, 1383, 3271, 23420), -- The Old Fashioned Way
(42835, 3, 1, 1368, 3268, 23420), -- The Old Fashioned Way
(42835, 3, 0, 1357, 3268, 23420), -- The Old Fashioned Way
(42835, 2, 0, 1359, 3294, 23420), -- The Old Fashioned Way
(42834, 4, 0, 1482, 3426, 23420), -- Intense Concentration
(42834, 3, 11, 1475, 3419, 23420), -- Intense Concentration
(42834, 3, 10, 1475, 3434, 23420), -- Intense Concentration
(42834, 3, 9, 1479, 3441, 23420), -- Intense Concentration
(42834, 3, 8, 1490, 3445, 23420), -- Intense Concentration
(42834, 3, 7, 1505, 3445, 23420), -- Intense Concentration
(42834, 3, 6, 1516, 3441, 23420), -- Intense Concentration
(42834, 3, 5, 1530, 3434, 23420), -- Intense Concentration
(42834, 3, 4, 1530, 3419, 23420), -- Intense Concentration
(42834, 3, 3, 1523, 3415, 23420), -- Intense Concentration
(42834, 3, 2, 1508, 3408, 23420), -- Intense Concentration
(42834, 3, 1, 1501, 3408, 23420), -- Intense Concentration
(42834, 3, 0, 1486, 3408, 23420), -- Intense Concentration
(42833, 5, 0, 1525, 3545, 23420), -- How It's Made: Arcwine
(42832, 2, 0, 1512, 3631, 23420), -- The Fruit of Our Efforts
(42828, 2, 0, 1719, 4602, 23420); -- Moths to a Flame

UPDATE `quest_poi_points` SET `X`=1351, `Y`=3290, `VerifiedBuild`=23420 WHERE (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=0); -- The Old Fashioned Way
UPDATE `quest_poi_points` SET `X`=1352, `Y`=3289, `VerifiedBuild`=23420 WHERE (`QuestID`=42835 AND `Idx1`=0 AND `Idx2`=0); -- The Old Fashioned Way
UPDATE `quest_poi_points` SET `X`=1420, `Y`=3452, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=11); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1420, `Y`=3470, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=10); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1434, `Y`=3489, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=9); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1453, `Y`=3504, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=8); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1464, `Y`=3504, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=7); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1486, `Y`=3504, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=6); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1501, `Y`=3493, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=5); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1512, `Y`=3482, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=4); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1516, `Y`=3463, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=3); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1501, `Y`=3437, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=2); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1445, `Y`=3434, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=1); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1427, `Y`=3434, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=2 AND `Idx2`=0); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1491, `Y`=3427, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=0); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1482, `Y`=3426, `VerifiedBuild`=23420 WHERE (`QuestID`=42834 AND `Idx1`=0 AND `Idx2`=0); -- Intense Concentration
UPDATE `quest_poi_points` SET `X`=1661, `Y`=4671, `VerifiedBuild`=23420 WHERE (`QuestID`=42829 AND `Idx1`=4 AND `Idx2`=0); -- Make an Entrance

DELETE FROM `quest_greeting` WHERE (`ID`=108870 AND `Type`=0) OR (`ID`=107126 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(108870, 0, 1, 200, 'How may I assist you?', 23420), -- 108870
(107126, 0, 0, 0, 'Step carefully.', 23420); -- 107126

DELETE FROM `quest_details` WHERE `ID` IN (44052 /*And They Will Tremble*/, 42792 /*Make Your Mark*/, 43352 /*Asset Security*/, 42841 /*A Big Score*/, 42840 /*If Words Don't Work...*/, 43969 /*Hired Help*/, 42839 /*Seek the Unsavory*/, 44084 /*Vengeance for Margaux*/, 42838 /*Reversal*/, 42836 /*Silkwing Sabotage*/, 42837 /*Balance to Spare*/, 42835 /*The Old Fashioned Way*/, 42834 /*Intense Concentration*/, 42833 /*How It's Made: Arcwine*/, 42832 /*The Fruit of Our Efforts*/, 42829 /*Make an Entrance*/, 42828 /*Moths to a Flame*/, 42489 /*Thalyssra's Drawers*/, 42488 /*Thalyssra's Abode*/, 42487 /*Friends On the Outside*/, 42486 /*Little One Lost*/, 42722 /*Friends in Cages*/, 40745 /*Shift Change*/, 42345 /*Fevered Prayer*/, 40730 /*Redistribution*/, 40727 /*All Along the Waterways*/, 40947 /*Special Delivery*/, 41878 /*The Gondolier*/, 41148 /*Dispensing Compassion*/, 40746 /*One of the People*/, 44562 /*Growing Strong*/, 44561 /*Seed of Hope*/, 42230 /*The Valewalker's Burden*/, 42228 /*The Hidden City*/, 42227 /*Into the Crevasse*/, 42226 /*Moonshade Holdout*/, 42225 /*Breaking the Seal*/, 42224 /*Cloaked in Moonshade*/, 40325 /*Scenes from a Memory*/, 45056 /*-Unknown-*/, 40798 /*Cling to Hope*/, 40324 /*Arcane Communion*/, 42147 /*First Contact*/, 42079 /*Masquerade*/, 41989 /*Blood of My Blood*/, 43811 /*Lunastre Estate Teleporter Online!*/, 41834 /*The Masks We Wear*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(44052, 1, 66, 0, 0, 0, 0, 0, 0, 23420), -- And They Will Tremble
(42792, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Make Your Mark
(43352, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Asset Security
(42841, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- A Big Score
(42840, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- If Words Don't Work...
(43969, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Hired Help
(42839, 5, 0, 0, 0, 0, 0, 0, 0, 23420), -- Seek the Unsavory
(44084, 603, 603, 0, 0, 0, 0, 0, 0, 23420), -- Vengeance for Margaux
(42838, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Reversal
(42836, 603, 1, 0, 0, 0, 1500, 0, 0, 23420), -- Silkwing Sabotage
(42837, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Balance to Spare
(42835, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Old Fashioned Way
(42834, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Intense Concentration
(42833, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- How It's Made: Arcwine
(42832, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Fruit of Our Efforts
(42829, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Make an Entrance
(42828, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Moths to a Flame
(42489, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Thalyssra's Drawers
(42488, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Thalyssra's Abode
(42487, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Friends On the Outside
(42486, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Little One Lost
(42722, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Friends in Cages
(40745, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Shift Change
(42345, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Fevered Prayer
(40730, 1, 1, 0, 0, 0, 900, 0, 0, 23420), -- Redistribution
(40727, 1, 25, 0, 0, 100, 600, 0, 0, 23420), -- All Along the Waterways
(40947, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Special Delivery
(41878, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Gondolier
(41148, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Dispensing Compassion
(40746, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- One of the People
(44562, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Growing Strong
(44561, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Seed of Hope
(42230, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Valewalker's Burden
(42228, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Hidden City
(42227, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Into the Crevasse
(42226, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Moonshade Holdout
(42225, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Breaking the Seal
(42224, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Cloaked in Moonshade
(40325, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Scenes from a Memory
(45056, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(40798, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Cling to Hope
(40324, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Arcane Communion
(42147, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- First Contact
(42079, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Masquerade
(41989, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Blood of My Blood
(43811, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Lunastre Estate Teleporter Online!
(41834, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- The Masks We Wear

DELETE FROM `quest_request_items` WHERE `ID` IN (44562 /*Growing Strong*/, 43969 /*Hired Help*/, 42489 /*Thalyssra's Drawers*/, 42488 /*Thalyssra's Abode*/, 40947 /*Special Delivery*/, 40730 /*Redistribution*/, 41148 /*Dispensing Compassion*/, 43907 /*Into the Nightmare: Xavius*/, 40798 /*Cling to Hope*/, 42147 /*First Contact*/, 41989 /*Blood of My Blood*/, 41834 /*The Masks We Wear*/, 43811 /*Lunastre Estate Teleporter Online!*/, 43106 /*Feed Oculeth*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(44562, 0, 0, 0, 0, 'Have you felt it? Change is on the wind.', 23420), -- Growing Strong
(43969, 0, 0, 0, 0, 'Best not to lurk if you aren''t doing business.', 23420), -- Hired Help
(42489, 0, 0, 0, 0, 'What did you find?', 23420), -- Thalyssra's Drawers
(42488, 0, 0, 0, 0, 'Did you find it?', 23420), -- Thalyssra's Abode
(40947, 0, 0, 0, 0, 'Were you successful?', 23420), -- Special Delivery
(40730, 603, 0, 200, 0, 'Did you find enough?', 23420), -- Redistribution
(41148, 1, 0, 0, 0, 'Have you served those who suffer?', 23420), -- Dispensing Compassion
(43907, 0, 0, 0, 0, 'How are you finding the challenges of the Emerald Nightmare?', 23420), -- Into the Nightmare: Xavius
(40798, 0, 0, 0, 0, '<Noressa eyes you suspiciously.>', 23420), -- Cling to Hope
(42147, 0, 0, 0, 0, 'Have you made contact with our sympathizers?', 23420), -- First Contact
(41989, 0, 0, 0, 0, 'Anarys could halt our efforts before they begin.', 23420), -- Blood of My Blood
(41834, 0, 0, 0, 0, 'Pleased to make your acquaintance.', 23420), -- The Masks We Wear
(43811, 0, 0, 0, 0, '<The beacon is cold and dark.>', 23420), -- Lunastre Estate Teleporter Online!
(43106, 0, 0, 0, 0, '<Oculeth mumbles incoherently under his breath.>', 23420); -- Feed Oculeth


UPDATE `creature_model_info` SET `BoundingRadius`=0.4978999, `CombatReach`=1.95, `VerifiedBuild`=23420 WHERE `DisplayID`=68087;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4978999, `CombatReach`=1.95, `VerifiedBuild`=23420 WHERE `DisplayID`=68184;
UPDATE `creature_model_info` SET `BoundingRadius`=1.581009, `CombatReach`=2.2 WHERE `DisplayID`=66336;
UPDATE `creature_model_info` SET `CombatReach`=0.8571429 WHERE `DisplayID`=664;
UPDATE `creature_model_info` SET `BoundingRadius`=3.5, `CombatReach`=3.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66535;
UPDATE `creature_model_info` SET `BoundingRadius`=3.5, `CombatReach`=3.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66534;
UPDATE `creature_model_info` SET `BoundingRadius`=3.5, `CombatReach`=3.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66536;
UPDATE `creature_model_info` SET `BoundingRadius`=3.5, `CombatReach`=3.5, `VerifiedBuild`=23420 WHERE `DisplayID`=29934;
UPDATE `creature_model_info` SET `BoundingRadius`=1.261248, `CombatReach`=1.65 WHERE `DisplayID`=67552;
UPDATE `creature_model_info` SET `BoundingRadius`=1.847593 WHERE `DisplayID`=32546;
UPDATE `creature_model_info` SET `BoundingRadius`=1.165187, `CombatReach`=1.1, `VerifiedBuild`=23420 WHERE `DisplayID`=66697;
UPDATE `creature_model_info` SET `BoundingRadius`=5.299212, `CombatReach`=3.75 WHERE `DisplayID`=65258;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=71539;
UPDATE `creature_model_info` SET `BoundingRadius`=1.45, `CombatReach`=1.8125 WHERE `DisplayID`=70400;
UPDATE `creature_model_info` SET `BoundingRadius`=1.328041, `CombatReach`=1.08 WHERE `DisplayID`=69273;
UPDATE `creature_model_info` SET `BoundingRadius`=1.031494 WHERE `DisplayID`=68393;
UPDATE `creature_model_info` SET `BoundingRadius`=1.65039 WHERE `DisplayID`=64629;

DELETE FROM `npc_vendor` WHERE (`entry`=98548 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=4357 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=10647 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=39684 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=40533 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=90146 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=98548 AND `item`=140324 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(98548, 14, 39354, 0, 0, 1, 0, 0, 23420), -- Light Parchment
(98548, 13, 4364, 3, 0, 1, 0, 0, 23420), -- Coarse Blasting Powder
(98548, 12, 4357, 4, 0, 1, 0, 0, 23420), -- Rough Blasting Powder
(98548, 11, 10647, 0, 0, 1, 0, 0, 23420), -- Engineer's Ink
(98548, 10, 3466, 0, 0, 1, 0, 0, 23420), -- Strong Flux
(98548, 9, 2880, 0, 0, 1, 0, 0, 23420), -- Weak Flux
(98548, 8, 39684, 0, 0, 1, 0, 0, 23420), -- Hair Trigger
(98548, 7, 40533, 0, 0, 1, 0, 0, 23420), -- Walnut Stock
(98548, 6, 4400, 0, 0, 1, 0, 0, 23420), -- Heavy Stock
(98548, 5, 4399, 0, 0, 1, 0, 0, 23420), -- Wooden Stock
(98548, 4, 90146, 0, 0, 1, 0, 0, 23420), -- Tinker's Kit
(98548, 3, 2901, 0, 0, 1, 0, 0, 23420), -- Mining Pick
(98548, 2, 5956, 0, 0, 1, 0, 0, 23420), -- Blacksmith Hammer
(98548, 1, 140324, 0, 0, 1, 0, 0, 23420); -- Mobile Telemancy Beacon

UPDATE `npc_vendor` SET `slot`=9 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread
UPDATE `npc_vendor` SET `slot`=9 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=100962 AND `ID`=1) OR (`CreatureID`=100963 AND `ID`=1) OR (`CreatureID`=102739 AND `ID`=1) OR (`CreatureID`=107643 AND `ID`=1) OR (`CreatureID`=107822 AND `ID`=1) OR (`CreatureID`=107919 AND `ID`=1) OR (`CreatureID`=116601 AND `ID`=1) OR (`CreatureID`=116600 AND `ID`=1) OR (`CreatureID`=103215 AND `ID`=1) OR (`CreatureID`=103449 AND `ID`=1) OR (`CreatureID`=99637 AND `ID`=1) OR (`CreatureID`=100946 AND `ID`=1) OR (`CreatureID`=100947 AND `ID`=1) OR (`CreatureID`=100945 AND `ID`=1) OR (`CreatureID`=102660 AND `ID`=1) OR (`CreatureID`=100891 AND `ID`=1) OR (`CreatureID`=102551 AND `ID`=1) OR (`CreatureID`=102738 AND `ID`=1) OR (`CreatureID`=100889 AND `ID`=1) OR (`CreatureID`=100888 AND `ID`=1) OR (`CreatureID`=107366 AND `ID`=1) OR (`CreatureID`=109008 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(100962, 1, 120477, 0, 0), -- Sashj'tar Lancer
(100963, 1, 121290, 0, 0), -- Sashj'tar Enchantress
(102739, 1, 134057, 0, 0), -- Eynar
(107643, 1, 3368, 0, 0), -- Vrykul Mariner
(107822, 1, 34818, 0, 0), -- Vrykul Shepherd
(107919, 1, 34819, 0, 0), -- Vrykul Salvager
(116601, 1, 43580, 0, 0), -- Helarjar Marauder
(116600, 1, 34816, 0, 0), -- Cursed Sea Dog
(103215, 1, 134847, 0, 52524), -- Forsaken Deathwarder
(103449, 1, 134849, 0, 0), -- Forsaken Coldwitch
(99637, 1, 121314, 0, 0), -- Sashj'tar Reef Runner
(100946, 1, 129723, 0, 0), -- Jandvik Runecaller
(100947, 1, 121314, 0, 0), -- Sashj'tar Reef Runner
(100945, 1, 108780, 0, 108780), -- Jandvik Splintershield
(102660, 1, 5956, 0, 0), -- Jandvik Metalsmith
(100891, 1, 2632, 0, 0), -- Jandvik Bonepiercer
(102551, 1, 108780, 0, 108780), -- Jandvik Splintershield
(102738, 1, 88683, 0, 0), -- Calder
(100889, 1, 129723, 0, 0), -- Jandvik Runecaller
(100888, 1, 108780, 0, 108780), -- Jandvik Splintershield
(107366, 1, 65335, 0, 0), -- Eredar Enforcer
(109008, 1, 132171, 0, 0); -- First Arcanist Thalyssra

UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=31727 AND `ID`=1); -- Grunt Grikee
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=31726 AND `ID`=1); -- Grunt Gritch
UPDATE `creature_equip_template` SET `ItemID1`=118568 WHERE (`CreatureID`=99386 AND `ID`=2); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=114980 WHERE (`CreatureID`=99386 AND `ID`=1); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=1902 WHERE (`CreatureID`=44158 AND `ID`=3); -- Orgrimmar Skyway Peon

UPDATE `quest_template` SET `Flags`=35651904, `VerifiedBuild`=23420 WHERE `ID`=43532; -- -Unknown-
UPDATE `quest_template` SET `RewardMoney`=194000 WHERE `ID`=37496; -- Infiltrating Shipwreck Arena

UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5, `VerifiedBuild`=23420 WHERE `entry`=111558; -- Felgaze Doomseer
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1, `unit_flags`=32768 WHERE `entry`=112530; -- Garion
UPDATE `creature_template` SET `npcflag`=2, `VerifiedBuild`=23420 WHERE `entry`=108870; -- Sylverin
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103212; -- Injured Vrykul
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103211; -- Injured Vrykul
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103207; -- Injured Vrykul
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2205, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100864; -- Cora'kar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2205, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=536871680, `unit_flags2`=67110913, `VerifiedBuild`=23420 WHERE `entry`=100963; -- Sashj'tar Enchantress
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2205, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=536871680, `unit_flags2`=67110913, `VerifiedBuild`=23420 WHERE `entry`=100962; -- Sashj'tar Lancer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102309; -- Toryl
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2810, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102739; -- Eynar
UPDATE `creature_template` SET `speed_run`=1, `HoverHeight`=3 WHERE `entry`=116601; -- Helarjar Marauder
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=71, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103449; -- Forsaken Coldwitch
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2131968, `VerifiedBuild`=23420 WHERE `entry`=104630; -- Kell
UPDATE `creature_template` SET `BaseAttackTime`=2000 WHERE `entry`=100947; -- Sashj'tar Reef Runner
UPDATE `creature_template` SET `speed_walk`=0.75, `speed_run`=1.125, `VerifiedBuild`=23420 WHERE `entry`=100780; -- Trapper Rodoon
UPDATE `creature_template` SET `unit_flags`=570721024, `unit_flags2`=2049 WHERE `entry`=112972; -- Giant Thicketstomper
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=115931; -- Goal Energy Field
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=116508; -- Launch Up Field
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=116514; -- Energy Wall
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=115930; -- Negative Energy Field
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=115929; -- Energy Field
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=93973; -- Leyweaver Phaxondus
UPDATE `creature_template` SET `speed_run`=1.289683, `VerifiedBuild`=23420 WHERE `entry`=96697; -- Silver Hand Knight
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=89278; -- Demon Hunter
UPDATE `creature_template` SET `unit_flags`=32784, `unit_flags2`=33556480 WHERE `entry`=86969; -- Demon Hunter
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=93619; -- Infernal Brutalizer
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96696; -- Silver Hand Knight
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96724; -- Silver Hand Knight
UPDATE `creature_template` SET `speed_run`=1.142857, `VerifiedBuild`=23420 WHERE `entry`=109028; -- Horkus
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=109349; -- Veil Shadowrunner
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=110032; -- Festering Eye
UPDATE `creature_template` SET `faction`=2849, `unit_flags2`=2052, `VerifiedBuild`=23420 WHERE `entry`=92388; -- Vale Flitter
UPDATE `creature_template` SET `faction`=2849, `unit_flags2`=2052 WHERE `entry`=106286; -- Sylvan Owl
UPDATE `creature_template` SET `unit_flags`=536904512, `unit_flags2`=4196353 WHERE `entry`=98406; -- Embershard Scorpion
UPDATE `creature_template` SET `unit_flags`=33587968, `VerifiedBuild`=23420 WHERE `entry`=105633; -- Understone Drudge
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=109 WHERE `entry`=105778; -- Angry Crowd
UPDATE `creature_template` SET `npcflag`=211 WHERE `entry`=99903; -- Sprig Hashhoof
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=98017; -- Guron Twaintail
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=94691; -- Overgrown Larva
UPDATE `creature_template` SET `npcflag`=2, `unit_flags2`=33587200 WHERE `entry`=101766; -- Thalrenus Rivertree
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=104226; -- Gloomfang
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=103, `VerifiedBuild`=23420 WHERE `entry`=107244; -- Tehd Shoemaker
UPDATE `creature_template` SET `minlevel`=108, `maxlevel`=108, `VerifiedBuild`=23420 WHERE `entry`=107245; -- Marius Felbane
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=91629; -- Illidari Enforcer
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2099201 WHERE `entry`=89940; -- Azurewing Scalewarden
UPDATE `creature_template` SET `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=104837; -- Caged Tiger
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=103223; -- Hertha Grimdottir
UPDATE `creature_template` SET `unit_flags`=570720256, `unit_flags2`=2049 WHERE `entry`=110043; -- Heartwood Doe
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=107755; -- Amberfall Doe
UPDATE `creature_template` SET `unit_flags`=33024, `unit_flags2`=2048 WHERE `entry`=106565; -- Blackfeather Gatherer
UPDATE `creature_template` SET `unit_flags2`=67110912 WHERE `entry`=102104; -- Enslaved Shieldmaiden
UPDATE `creature_template` SET `unit_flags`=33587200 WHERE `entry`=97163; -- Cursed Falke
UPDATE `creature_template` SET `HoverHeight`=1.09 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=93612; -- Dreadwake Deathguard
UPDATE `creature_template` SET `unit_flags`=16, `VerifiedBuild`=23420 WHERE `entry`=88981; -- Ironclaw Scuttler
UPDATE `creature_template` SET `faction`=2166 WHERE `entry`=109635; -- Greywatch Saboteur
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049 WHERE `entry`=107881; -- Tideskorn Beastbreaker
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=103514; -- Leystone Basilisk
UPDATE `creature_template` SET `HoverHeight`=0.25 WHERE `entry`=111787; -- Great Sea Ray
UPDATE `creature_template` SET `HoverHeight`=0.25 WHERE `entry`=111408; -- Great Sea Snake
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=102 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `npcflag`=18 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=93531; -- Enchanter Nalthanis
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `npcflag`=81 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `npcflag`=80 WHERE `entry`=92459; -- Dorothy Egan
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=67143680, `VerifiedBuild`=23420 WHERE `entry`=110124; -- Claws of Ursoc - Alt 5 - Stone
UPDATE `creature_template` SET `npcflag`=3, `VerifiedBuild`=23420 WHERE `entry`=108063; -- Korine
UPDATE `creature_template` SET `unit_flags`=33554432, `VerifiedBuild`=23420 WHERE `entry`=105393; -- Il'gynoth
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=23420 WHERE `entry`=102672; -- Nythendra
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=111056; -- Tiny Illusory Dancer
UPDATE `creature_template` SET `unit_flags`=33024, `unit_flags2`=34816, `VerifiedBuild`=23420 WHERE `entry`=108386; -- Ambrena
UPDATE `creature_template` SET `faction`=2847, `VerifiedBuild`=23420 WHERE `entry`=113515; -- Maribeth
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=114889; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=114892; -- Withering Civilian
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=107598; -- Vanthir
UPDATE `creature_template` SET `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=108810; -- Chloe
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=114888; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=114470; -- Duskwatch Orbitist
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=107632; -- Ly'leth Lunastre
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112320; -- Haunting Nightmare
UPDATE `creature_template` SET `unit_flags`=537133312, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=721; -- Rabbit
UPDATE `creature_template` SET `unit_flags`=537133312, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=883; -- Deer
UPDATE `creature_template` SET `speed_walk`=0.666668, `speed_run`=1.428571, `VerifiedBuild`=23420 WHERE `entry`=112162; -- Grisly Trapper
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=114231; -- Mistaaytch
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `VerifiedBuild`=23420 WHERE `entry`=88703; -- Grunt Arhung
UPDATE `creature_template` SET `faction`=105, `unit_flags`=33587456 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=8, `maxlevel`=8 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `minlevel`=107, `maxlevel`=109 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=92464; -- Kuhuine Tenderstride
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=108 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `maxlevel`=105 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=106, `speed_walk`=0.68, `speed_run`=0.6 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=109 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=109, `speed_run`=0.9523814 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=108 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=109, `speed_walk`=0.666668 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=105885; -- Hungering Husk
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=106804; -- Suramar Grizzly
UPDATE `creature_template` SET `npcflag`=16777216 WHERE `entry`=115951; -- Noressa
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=101848; -- Absolon
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=107803; -- Wild Plains Runehorn
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=106495; -- Vanthir
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=0.9920629 WHERE `entry`=115535; -- Skulking Assassin
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=2049 WHERE `entry`=113752; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=2049 WHERE `entry`=107451; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags2`=33556480 WHERE `entry`=114929; -- Duskwatch Defender
UPDATE `creature_template` SET `HoverHeight`=2 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102960; -- Glitterpool Heron

UPDATE `creature_questitem` SET `ItemId`=129894, `VerifiedBuild`=23420 WHERE (`CreatureEntry`=91792 AND `Idx`=0); -- Stormwake Hydra
UPDATE `creature_questitem` SET `ItemId`=129921, `VerifiedBuild`=23420 WHERE (`CreatureEntry`=100249 AND `Idx`=0); -- Channeler Varisz
UPDATE `creature_questitem` SET `ItemId`=129921, `VerifiedBuild`=23420 WHERE (`CreatureEntry`=100250 AND `Idx`=0); -- Binder Ashioi
UPDATE `creature_questitem` SET `ItemId`=129921, `VerifiedBuild`=23420 WHERE (`CreatureEntry`=100248 AND `Idx`=0); -- Ritualist Lesha
UPDATE `creature_questitem` SET `ItemId`=129921, `VerifiedBuild`=23420 WHERE (`CreatureEntry`=98173 AND `Idx`=0); -- Mystic Ssa'veh

DELETE FROM `gameobject_template` WHERE `entry` IN (266302 /*Arcane Disk*/, 252665 /*Challenger's Cache*/, 252666 /*Superior Challenger's Cache*/, 252667 /*Peerless Challenger's Cache*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(266302, 10, 39088, 'Arcane Disk', '', '', '', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Arcane Disk
(252665, 3, 33268, 'Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1814, 0, 1, 0, 0, 68463, 0, 5, 110, 23420), -- Challenger's Cache
(252666, 3, 33268, 'Superior Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1814, 0, 1, 0, 0, 68464, 0, 5, 110, 23420), -- Superior Challenger's Cache
(252667, 3, 33268, 'Peerless Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1814, 0, 1, 0, 0, 68465, 0, 5, 110, 23420); -- Peerless Challenger's Cache
