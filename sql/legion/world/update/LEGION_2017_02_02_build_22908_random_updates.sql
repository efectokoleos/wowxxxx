DELETE FROM `quest_offer_reward` WHERE `ID` IN (40996 /*Delegation*/, 40995 /*Injection of Power*/, 40994 /*Right Tools for the Job*/, 40950 /*Honoring Success*/, 40849 /*The Dreadblades*/, 40847 /*A Friendly Accord*/, 40840 /*A Worthy Blade*/, 40839 /*The Final Shadow*/, 40832 /*Call of The Uncrowned*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(40996, 1, 0, 0, 0, 0, 0, 0, 0, 'I don''t know nothin'' about nobody.$B$BOh, who I am I kiddin''? I know everything about everybody! How''s it goin'', Shadow $n?', 22908), -- Delegation
(40995, 1, 0, 0, 0, 0, 0, 0, 0, 'Hey, don''t even mention it, pal. I''m here for ya any time you need anything!$B$BSeriously though, don''t mention it. There are some folks that would do anything to get their hands on this stuff.', 22908), -- Injection of Power
(40994, 0, 0, 0, 0, 0, 0, 0, 0, 'So, what can I do for you today, $GMaster:Mistress; $n?', 22908), -- Right Tools for the Job
(40950, 1, 0, 0, 0, 0, 0, 0, 0, 'It seems you struck a nerve.', 22908), -- Honoring Success
(40849, 1, 0, 0, 0, 0, 0, 0, 0, 'You did fine work today, $Glad:lass;.', 22908), -- The Dreadblades
(40847, 11, 0, 0, 0, 0, 0, 0, 0, 'Aha! I knew you''d come!', 22908), -- A Friendly Accord
(40840, 1, 0, 0, 0, 0, 0, 0, 0, 'Well chosen.', 22908), -- A Worthy Blade
(40839, 1, 0, 0, 0, 0, 0, 0, 0, 'Welcome to the Uncrowned, Shadow $n.', 22908), -- The Final Shadow
(40832, 1, 0, 0, 0, 0, 0, 0, 0, 'Your reputation precedes you, $n.$B$BApologies for the secrecy. We simply could not risk the wrong person finding us here.', 22908); -- Call of The Uncrowned

UPDATE `quest_poi` SET `WorldEffectID`=982, `WoDUnk1`=0 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=1); -- Julia, The Pet Tamer
UPDATE `quest_poi` SET `WoDUnk1`=0 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=0); -- Julia, The Pet Tamer

UPDATE `quest_poi_points` SET `X`=-9873, `Y`=87 WHERE (`QuestID`=31316 AND `Idx1`=1 AND `Idx2`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi_points` SET `X`=-9868, `Y`=82 WHERE (`QuestID`=31316 AND `Idx1`=0 AND `Idx2`=0); -- Julia, The Pet Tamer

DELETE FROM `quest_details` WHERE `ID` IN (40997 /*Lethal Efficiency*/, 40996 /*Delegation*/, 40995 /*Injection of Power*/, 40994 /*Right Tools for the Job*/, 40950 /*Honoring Success*/, 40849 /*The Dreadblades*/, 40847 /*A Friendly Accord*/, 40840 /*A Worthy Blade*/, 40839 /*The Final Shadow*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(40997, 1, 0, 0, 0, 0, 0, 0, 0, 22908), -- Lethal Efficiency
(40996, 1, 1, 0, 0, 0, 0, 0, 0, 22908), -- Delegation
(40995, 1, 0, 0, 0, 0, 0, 0, 0, 22908), -- Injection of Power
(40994, 1, 1, 0, 0, 0, 0, 0, 0, 22908), -- Right Tools for the Job
(40950, 1, 1, 0, 0, 0, 0, 0, 0, 22908), -- Honoring Success
(40849, 1, 1, 1, 0, 0, 0, 0, 0, 22908), -- The Dreadblades
(40847, 1, 0, 0, 0, 0, 0, 0, 0, 22908), -- A Friendly Accord
(40840, 1, 1, 1, 0, 0, 0, 0, 0, 22908), -- A Worthy Blade
(40839, 1, 1, 1, 6, 0, 0, 0, 0, 22908); -- The Final Shadow

DELETE FROM `quest_request_items` WHERE `ID` IN (40950 /*Honoring Success*/, 44466 /*An Unclear Path*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(40950, 0, 0, 0, 0, 'It''s kill or be killed.', 22908), -- Honoring Success
(44466, 0, 0, 0, 0, 'You mustn''t lose hope.', 22908); -- An Unclear Path
