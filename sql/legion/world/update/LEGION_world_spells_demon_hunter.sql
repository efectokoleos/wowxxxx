DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_dh_legion_blade_dance',
'spell_dh_legion_blade_dance_damage',
'spell_dh_legion_felblade',
'spell_dh_legion_felblade_aura',
'spell_dh_legion_felblade_charge',
'spell_dh_legion_first_blood',
'spell_dh_legion_glide',
'spell_dh_legion_havoc_artifact_anguish_of_the_deceiver',
'spell_dh_legion_havoc_artifact_deceivers_fury',
'spell_dh_legion_havoc_artifact_feast_on_the_souls',
'spell_dh_legion_havoc_artifact_inner_demons',
'spell_dh_legion_havoc_artifact_rage_of_the_illidari',
'spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage',
'spell_dh_legion_havoc_bloodlet',
'spell_dh_legion_havoc_bloodlet',
'spell_dh_legion_havoc_blur',
'spell_dh_legion_havoc_chaos_blades',
'spell_dh_legion_havoc_chaos_cleave',
'spell_dh_legion_havoc_chaos_nova',
'spell_dh_legion_havoc_chaos_strike',
'spell_dh_legion_havoc_darkness',
'spell_dh_legion_havoc_demonic_appetite_energize',
'spell_dh_legion_havoc_demonic_appetite',
'spell_dh_legion_havoc_desperate_instincts',
'spell_dh_legion_havoc_eye_beam',
'spell_dh_legion_havoc_fel_barrage',
'spell_dh_legion_havoc_fel_mastery',
'spell_dh_legion_havoc_fel_rush',
'spell_dh_legion_havoc_honor_awaken_the_demon_within',
'spell_dh_legion_havoc_honor_fel_lance',
'spell_dh_legion_havoc_honor_glimpse',
'spell_dh_legion_havoc_honor_mana_break',
'spell_dh_legion_havoc_master_of_the_glaives',
'spell_dh_legion_havoc_metamorphosis',
'spell_dh_legion_havoc_momentum',
'spell_dh_legion_havoc_nemesis',
'spell_dh_legion_havoc_prepared',
'spell_dh_legion_havoc_shattered_souls',
'spell_dh_legion_havoc_shattered_souls_missile',
'spell_dh_legion_honor_eye_of_leotheras',
'spell_dh_legion_honor_reverse_magic',
'spell_dh_legion_honor_solitude',
'spell_dh_legion_spectral_sight',
'spell_dh_legion_vengeance_artifact_charred_warblades',
'spell_dh_legion_vengeance_artifact_defensive_spikes',
'spell_dh_legion_vengeance_artifact_fiery_demise',
'spell_dh_legion_vengeance_artifact_fueled_by_pain',
'spell_dh_legion_vengeance_artifact_soul_carver',
'spell_dh_legion_vengeance_burning_alive',
'spell_dh_legion_vengeance_consume_soul',
'spell_dh_legion_vengeance_demon_spikes_aura',
'spell_dh_legion_vengeance_demon_spikes',
'spell_dh_legion_vengeance_demonic_infusion',
'spell_dh_legion_vengeance_empower_wards',
'spell_dh_legion_vengeance_fel_devastation',
'spell_dh_legion_vengeance_fiery_brand',
'spell_dh_legion_vengeance_fracture',
'spell_dh_legion_vengeance_frailty',
'spell_dh_legion_vengeance_honor_jagged_spikes',
'spell_dh_legion_vengeance_immolation_aura',
'spell_dh_legion_vengeance_infernal_strike',
'spell_dh_legion_vengeance_last_resort',
'spell_dh_legion_vengeance_razor_spikes',
'spell_dh_legion_vengeance_shatter_soul',
'spell_dh_legion_vengeance_shattered_souls',
'spell_dh_legion_vengeance_shear',
'spell_dh_legion_vengeance_sigil_of_flame',
'spell_dh_legion_vengeance_soul_barrier',
'spell_dh_legion_vengeance_soul_cleave',
'spell_dh_legion_vengeance_spirit_bomb'
);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(188499, 'spell_dh_legion_blade_dance'),
(210152, 'spell_dh_legion_blade_dance'),
(199552, 'spell_dh_legion_blade_dance_damage'),
(200685, 'spell_dh_legion_blade_dance_damage'),
(210153, 'spell_dh_legion_blade_dance_damage'),
(210155, 'spell_dh_legion_blade_dance_damage'),
(232893, 'spell_dh_legion_felblade'),
(203557, 'spell_dh_legion_felblade_aura'),
(204497, 'spell_dh_legion_felblade_aura'),
(236167, 'spell_dh_legion_felblade_aura'),
(213241, 'spell_dh_legion_felblade_charge'),
(206416, 'spell_dh_legion_first_blood'),
(131347, 'spell_dh_legion_glide'),
(197154, 'spell_dh_legion_glide'),
(202443, 'spell_dh_legion_havoc_artifact_anguish_of_the_deceiver'),
(201473, 'spell_dh_legion_havoc_artifact_anguish_of_the_deceiver'),
(201463, 'spell_dh_legion_havoc_artifact_deceivers_fury'),
(201468, 'spell_dh_legion_havoc_artifact_feast_on_the_souls'),
(201471, 'spell_dh_legion_havoc_artifact_inner_demons'),
(201472, 'spell_dh_legion_havoc_artifact_rage_of_the_illidari'),
(217070, 'spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage'),
(206473, 'spell_dh_legion_havoc_bloodlet'),
(207690, 'spell_dh_legion_havoc_bloodlet'),
(212800, 'spell_dh_legion_havoc_blur'),
(211048, 'spell_dh_legion_havoc_chaos_blades'),
(206475, 'spell_dh_legion_havoc_chaos_cleave'),
(179057, 'spell_dh_legion_havoc_chaos_nova'),
(197125, 'spell_dh_legion_havoc_chaos_strike'),
(162794, 'spell_dh_legion_havoc_chaos_strike'),
(201427, 'spell_dh_legion_havoc_chaos_strike'),
(209426, 'spell_dh_legion_havoc_darkness'),
(210041, 'spell_dh_legion_havoc_demonic_appetite_energize'),
(221461, 'spell_dh_legion_havoc_demonic_appetite'),
(206478, 'spell_dh_legion_havoc_demonic_appetite'),
(205478, 'spell_dh_legion_havoc_desperate_instincts'),
(198013, 'spell_dh_legion_havoc_eye_beam'),
(222703, 'spell_dh_legion_havoc_fel_barrage'),
(211052, 'spell_dh_legion_havoc_fel_barrage'),
(211053, 'spell_dh_legion_havoc_fel_barrage'),
(192939, 'spell_dh_legion_havoc_fel_mastery'),
(195072, 'spell_dh_legion_havoc_fel_rush'),
(197922, 'spell_dh_legion_havoc_fel_rush'),
(197923, 'spell_dh_legion_havoc_fel_rush'),
(205598, 'spell_dh_legion_havoc_honor_awaken_the_demon_within'),
(206966, 'spell_dh_legion_havoc_honor_fel_lance'),
(203468, 'spell_dh_legion_havoc_honor_glimpse'),
(203704, 'spell_dh_legion_havoc_honor_mana_break'),
(203556, 'spell_dh_legion_havoc_master_of_the_glaives'),
(191427, 'spell_dh_legion_havoc_metamorphosis'),
(191428, 'spell_dh_legion_havoc_metamorphosis'),
(162264, 'spell_dh_legion_havoc_metamorphosis'),
(206476, 'spell_dh_legion_havoc_momentum'),
(206491, 'spell_dh_legion_havoc_nemesis'),
(203551, 'spell_dh_legion_havoc_prepared'),
(178940, 'spell_dh_legion_havoc_shattered_souls'),
(228533, 'spell_dh_legion_havoc_shattered_souls_missile'),
(237867, 'spell_dh_legion_havoc_shattered_souls_missile'),
(209651, 'spell_dh_legion_havoc_shattered_souls_missile'),
(206649, 'spell_dh_legion_honor_eye_of_leotheras'),
(205604, 'spell_dh_legion_honor_reverse_magic'),
(211509, 'spell_dh_legion_honor_solitude'),
(188501, 'spell_dh_legion_spectral_sight'),
(213010, 'spell_dh_legion_vengeance_artifact_charred_warblades'),
(212871, 'spell_dh_legion_vengeance_artifact_defensive_spikes'),
(212817, 'spell_dh_legion_vengeance_artifact_fiery_demise'),
(213017, 'spell_dh_legion_vengeance_artifact_fueled_by_pain'),
(207407, 'spell_dh_legion_vengeance_artifact_soul_carver'),
(207760, 'spell_dh_legion_vengeance_burning_alive'),
(208014, 'spell_dh_legion_vengeance_consume_soul'),
(210047, 'spell_dh_legion_vengeance_consume_soul'),
(210050, 'spell_dh_legion_vengeance_consume_soul'),
(203819, 'spell_dh_legion_vengeance_demon_spikes_aura'),
(203720, 'spell_dh_legion_vengeance_demon_spikes'),
(236189, 'spell_dh_legion_vengeance_demonic_infusion'),
(218256, 'spell_dh_legion_vengeance_empower_wards'),
(212084, 'spell_dh_legion_vengeance_fel_devastation'),
(204021, 'spell_dh_legion_vengeance_fiery_brand'),
(204022, 'spell_dh_legion_vengeance_fiery_brand'),
(209795, 'spell_dh_legion_vengeance_fracture'),
(224509, 'spell_dh_legion_vengeance_frailty'),
(208796, 'spell_dh_legion_vengeance_honor_jagged_spikes'),
(187727, 'spell_dh_legion_vengeance_immolation_aura'),
(189110, 'spell_dh_legion_vengeance_infernal_strike'),
(189111, 'spell_dh_legion_vengeance_infernal_strike'),
(209258, 'spell_dh_legion_vengeance_last_resort'),
(209400, 'spell_dh_legion_vengeance_razor_spikes'),
(209980, 'spell_dh_legion_vengeance_shatter_soul'),
(209981, 'spell_dh_legion_vengeance_shatter_soul'),
(210038, 'spell_dh_legion_vengeance_shatter_soul'),
(204254, 'spell_dh_legion_vengeance_shattered_souls'),
(203783, 'spell_dh_legion_vengeance_shear'),
(204598, 'spell_dh_legion_vengeance_sigil_of_flame'),
(227225, 'spell_dh_legion_vengeance_soul_barrier'),
(228477, 'spell_dh_legion_vengeance_soul_cleave'),
(228478, 'spell_dh_legion_vengeance_soul_cleave'),
(218678, 'spell_dh_legion_vengeance_spirit_bomb'),
(218679, 'spell_dh_legion_vengeance_spirit_bomb');

DELETE FROM `spell_proc` WHERE `SpellId` IN (
183782,
212829,
227225,
205625,
203468,
206476,
203551,
224509,
213010,
213480,
205603,
206649,
203753,
201463
);
INSERT INTO `spell_proc` (`SpellId`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`, `Chance`) VALUES
(183782, 107,  0x80000, 0x0, 0x100, 0x0,     0x0, 0x2, 0x1000,   0), -- Consume Magic Interrupt Proc
(205625, 107,    0x800, 0x0,   0x0, 0x0,     0x0, 0x1,    0x0,   0), -- Cleansed by the Flame
(227225, 107, 0x100000, 0x0,   0x0, 0x0,  0x4000, 0x2,    0x0,   0), -- Soul Barrier
(212829, 107,     0x80, 0x0,   0x0, 0x0,     0x0, 0x1,    0x0,   0), -- Defensive Spikes
(203468, 107,      0x8, 0x0,   0x0, 0x0,     0x0, 0x1,    0x0,   0), -- Glimpse
(206476, 107,     0x12, 0x0,   0x0, 0x0, 0x15400, 0x1,    0x0,   0), -- Momentum
(203551, 107,      0x0, 0x0,   0x0, 0x0,    0x10, 0x2,    0x0, 100), -- Prepared
(224509, 107,      0x0, 0x0,   0x0, 0x0,     0x0, 0x0,    0x0,   0), -- Frailty
(213010, 107,      0x0, 0x0,   0x0, 0x0,     0x0, 0x2,    0x0,   0), -- Charred Warblades
(213480, 107,      0x0, 0x0,   0x0, 0x0,     0x0, 0x0,    0x0,   0), -- Unending Hatred
(205603, 107,      0x0, 0x0,   0x0, 0x0,     0x0, 0x0,    0x0,   0), -- Unending Hatred
(201463, 107,      0x0, 0x0,   0x0, 0x0,     0x0, 0x0,   0x10,   0), -- Deceiver's fury
(203753, 107,      0x0, 0x0,   0x0, 0x0,     0x0, 0x0,   0x20,   0); -- Blade Turning

DELETE FROM `areatrigger_template` WHERE `Id` IN (11107, 13662, 12504);
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `VerifiedBuild`) VALUES
(11107, 0, 4, 5, 5, 0, 0, 0, 0, 24461),
(13662, 0, 0, 3, 3, 0, 0, 0, 0, 24461),
(12504, 0, 0, 1, 1, 0, 0, 0, 0, 23171);

UPDATE `areatrigger_template` SET `ScriptName`='at_dh_havoc_artifact_fury_of_the_illidari' WHERE `Id`=10470;
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_havoc_artifact_inner_demons' WHERE `Id`=10529;
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_havoc_darkness' WHERE `Id`=11203;
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_vengeance_soul_fragment' WHERE `Id` IN (10665, 10693, 11266);
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_havoc_shattered_souls' WHERE `Id` IN (12929, 11231, 8352);
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_vengeance_sigil_of_flame' WHERE `Id`=10727;
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_vengeance_sigil_of_misery' WHERE `Id`=11023;
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_vengeance_sigil_of_silence' WHERE `Id`=10714;
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_vengeance_honor_demonic_trample' WHERE `Id`=11107;
UPDATE `areatrigger_template` SET `ScriptName`='at_dh_havoc_honor_mana_rift' WHERE `Id`=13662;

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (5758, 5823, 6615, 5977, 8867, 6039, 6351, 6027, 3680, 6710, 6711, 6659, 6482, 9695, 8318);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `MoveCurveId`, `TimeToTarget`, `TimeToTargetScale`,  `VerifiedBuild`) VALUES
(5758, 10470, 1700, 0,    2500, 23222),
(5823, 10529, 0,    1250, 1250, 23222),
(6615, 11203, 0,    0,    8000, 23222),
(5977, 10665, 0,    0,   20000, 23420),
(8867, 12929, 0,    0,   30000, 23420),
(6710, 11266, 0,    0,   20000, 23420), -- Shattered Soul - Vengeance
(6711, 10693, 0,    0,   20000, 23420), -- Shattered Soul - Vengeance (Demon)
(6659, 11231, 0,    0,   30000, 23420), -- Shattered Soul - Havoc (Demon)
(3680, 8352,  0,    0,   30000, 23420), -- Shattered Soul - Havoc
(6039, 10727, 0,    0,       0, 23420), -- Sigil of flame | TimeToTargetScale is based on duration of the spell
(6351, 11023, 0,    0,       0, 23420), -- Sigil of misery
(6027, 10714, 0,    0,       0, 23420), -- Sigil of silence
(6482, 11107, 0,    0,    8000, 24015), -- Demonic Trample
(9695, 13662, 0,    0,    2000, 24461), -- Mana Rift
(8318, 12504, 0,    0,   15000, 23171); -- Shear Lesser Soul Fragment

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` = 211881 AND `spell_effect` = 225102;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES
(211881, 225102, 'Fel Eruption Stun - Fel Eruption damage');
