DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_legion_sha_aftershock',
'spell_legion_sha_alpha_wolf',
'spell_legion_sha_ancestral_guidance',
'spell_legion_sha_ancestral_guidance_heal',
'spell_legion_sha_ancestral_protection_absorb',
'spell_legion_sha_ascendance_restoration',
'spell_legion_sha_bloodlust',
'spell_legion_sha_caress_of_the_tidemother',
'spell_legion_sha_chain_heal',
'spell_legion_sha_chain_lightning',
'spell_legion_sha_chain_lightning_overload',
'spell_legion_sha_cloudburst_totem',
'spell_legion_sha_cloudburst_totem_heal',
'spell_legion_sha_cloudburst_totem_recall',
'spell_legion_sha_counterstrike_totem',
'spell_legion_sha_crash_lightning',
'spell_legion_sha_crash_lightning_proc',
'spell_legion_sha_doom_vortex',
'spell_legion_sha_earth_shield',
'spell_legion_sha_earthen_shield',
'spell_legion_sha_earth_shock',
'spell_legion_sha_earthen_rage_passive',
'spell_legion_sha_earthen_rage_proc_aura',
'spell_legion_sha_earthgrab',
'spell_legion_sha_earthquake',
'spell_legion_sha_elemental_blast',
'spell_legion_sha_elemental_healing',
'spell_legion_sha_feral_lunge',
'spell_legion_sha_feral_spirit',
'spell_legion_sha_flame_shock',
'spell_legion_sha_flametongue',
'spell_legion_sha_forked_lightning',
'spell_legion_sha_frost_shock',
'spell_legion_sha_frostbrand',
'spell_legion_sha_fury_of_air',
'spell_legion_sha_fury_of_the_storms',
'spell_legion_sha_ghost_in_the_mist',
'spell_legion_sha_ghost_wolf',
'spell_legion_sha_healing_rain',
'spell_legion_sha_healing_stream_totem_heal',
'spell_legion_sha_healing_surge_enhancer',
'spell_legion_sha_healing_tide_totem',
'spell_legion_sha_healing_tide_totem_heal',
'spell_legion_sha_heroism',
'spell_legion_sha_hot_hand',
'spell_legion_sha_lava_burst',
'spell_legion_sha_lava_surge',
'spell_legion_sha_lightning_bolt',
'spell_legion_sha_lightning_bolt_enhancement',
'spell_legion_sha_lightning_bolt_overload',
'spell_legion_sha_lightning_rod_passive',
'spell_legion_sha_liquid_magma_totem_target_selector',
'spell_legion_sha_maelstrom_weapon_energize',
'spell_legion_sha_mastery_elemental_overload',
'spell_legion_sha_mastery_elemental_overload_hack',
'spell_legion_sha_path_of_flames_spread',
'spell_legion_sha_purge',
'spell_legion_sha_queen_ascendant',
'spell_legion_sha_rainfall',
'spell_legion_sha_reincarnation',
'spell_legion_sha_restorative_mists',
'spell_legion_sha_resurgence',
'spell_legion_sha_riptide',
'spell_legion_sha_shamanistic_healing',
'spell_legion_sha_spirit_link',
'spell_legion_sha_spirit_of_the_maelstorm',
'spell_legion_sha_stormbringer',
'spell_legion_sha_stormbringer_proc',
'spell_legion_sha_stormkeeper',
'spell_legion_sha_stormlash',
'spell_legion_sha_stormlash_buff',
'spell_legion_sha_stormstrike',
'spell_legion_sha_stormstrike_damage',
'spell_legion_sha_summon_elemental_totem_generic',
'spell_legion_sha_swelling_waves',
'spell_legion_sha_thunderstorm',
'spell_legion_sha_thunderstorm_ally',
'spell_legion_sha_tidal_pools',
'spell_legion_sha_tidal_waves',
'spell_legion_sha_tidal_waves_proc',
'spell_legion_sha_undulation',
'spell_legion_sha_unleash_doom_proc',
'spell_legion_sha_volcanic_inferno',
'spell_legion_sha_wellspring',
'spell_legion_sha_windfury'
);

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(198486, 'spell_legion_sha_alpha_wolf'),
(108281, 'spell_legion_sha_ancestral_guidance'),
(114911, 'spell_legion_sha_ancestral_guidance_heal'),
(1064,   'spell_legion_sha_chain_heal'),
(188443, 'spell_legion_sha_chain_lightning'),
(45297,  'spell_legion_sha_chain_lightning_overload'),
(187874, 'spell_legion_sha_crash_lightning'),
(187878, 'spell_legion_sha_crash_lightning_proc'),
(199107, 'spell_legion_sha_doom_vortex'),
(8042,   'spell_legion_sha_earth_shock'),
(170374, 'spell_legion_sha_earthen_rage_passive'),
(170377, 'spell_legion_sha_earthen_rage_proc_aura'),
(64695,  'spell_legion_sha_earthgrab'),
(77478,  'spell_legion_sha_earthquake'),
(117014, 'spell_legion_sha_elemental_blast'),
(120588, 'spell_legion_sha_elemental_blast'),
(198248, 'spell_legion_sha_elemental_healing'),
(196884, 'spell_legion_sha_feral_lunge'),
(188389, 'spell_legion_sha_flame_shock'),
(194084, 'spell_legion_sha_flametongue'),
(196840, 'spell_legion_sha_frost_shock'),
(196834, 'spell_legion_sha_frostbrand'),
(197211, 'spell_legion_sha_fury_of_air'),
(191717, 'spell_legion_sha_fury_of_the_storms'),
(207524, 'spell_legion_sha_ghost_in_the_mist'),
(2645,   'spell_legion_sha_ghost_wolf'),
(73920,  'spell_legion_sha_healing_rain'),
(52042,  'spell_legion_sha_healing_stream_totem_heal'),
(32182,  'spell_legion_sha_heroism'),
(51505,  'spell_legion_sha_lava_burst'),
(77756,  'spell_legion_sha_lava_surge'),
(188196, 'spell_legion_sha_lightning_bolt'),
(187837, 'spell_legion_sha_lightning_bolt_enhancement'),
(45284,  'spell_legion_sha_lightning_bolt_overload'),
(210689, 'spell_legion_sha_lightning_rod_passive'),
(168534, 'spell_legion_sha_mastery_elemental_overload'),
(210621, 'spell_legion_sha_path_of_flames_spread'),
(215864, 'spell_legion_sha_rainfall'),
(16196,  'spell_legion_sha_resurgence'),
(61295,  'spell_legion_sha_riptide'),
(98020,  'spell_legion_sha_spirit_link'),
(198240, 'spell_legion_sha_spirit_of_the_maelstorm'),
(201845, 'spell_legion_sha_stormbringer'),
(51490,  'spell_legion_sha_thunderstorm'),
(204406, 'spell_legion_sha_thunderstorm_ally'),
(51564,  'spell_legion_sha_tidal_waves'),
(200071, 'spell_legion_sha_undulation'),
(33757,  'spell_legion_sha_windfury'),
(208997, 'spell_legion_sha_counterstrike_totem'),
(187880, 'spell_legion_sha_maelstrom_weapon_energize'),
(210707, 'spell_legion_sha_aftershock'),
(205495, 'spell_legion_sha_stormkeeper'),
(51533,  'spell_legion_sha_feral_spirit'),
(201900, 'spell_legion_sha_hot_hand'),
(108280, 'spell_legion_sha_healing_tide_totem'),
(114942, 'spell_legion_sha_healing_tide_totem_heal'),
(195255, 'spell_legion_sha_stormlash'),
(195222, 'spell_legion_sha_stormlash_buff'),
(204264, 'spell_legion_sha_swelling_waves'),
(370,    'spell_legion_sha_purge'),
(2825,   'spell_legion_sha_bloodlust'),
(204350, 'spell_legion_sha_forked_lightning'),
(17364,  'spell_legion_sha_stormstrike'),
(32175,  'spell_legion_sha_stormstrike_damage'),
(32176,  'spell_legion_sha_stormstrike_damage'),
(207358, 'spell_legion_sha_tidal_pools'),
(21169,  'spell_legion_sha_reincarnation'),
(157504, 'spell_legion_sha_cloudburst_totem'),
(157503, 'spell_legion_sha_cloudburst_totem_heal'),
(204288, 'spell_legion_sha_earth_shield'),
(201764, 'spell_legion_sha_cloudburst_totem_recall'),
(207285, 'spell_legion_sha_queen_ascendant'),
(114083, 'spell_legion_sha_restorative_mists'),
(192223, 'spell_legion_sha_liquid_magma_totem_target_selector'),
(53390,  'spell_legion_sha_tidal_waves_proc'),
(198103, 'spell_legion_sha_summon_elemental_totem_generic'), -- Earth
(198067, 'spell_legion_sha_summon_elemental_totem_generic'), -- Fire
(192249, 'spell_legion_sha_summon_elemental_totem_generic'), -- Storm
(207498, 'spell_legion_sha_ancestral_protection_absorb'),
(191582, 'spell_legion_sha_shamanistic_healing'),
(207354, 'spell_legion_sha_caress_of_the_tidemother'),
(199055, 'spell_legion_sha_unleash_doom_proc'),
(192630, 'spell_legion_sha_volcanic_inferno'),
(197995, 'spell_legion_sha_wellspring'),
(201846, 'spell_legion_sha_stormbringer_proc'),
(188070, 'spell_legion_sha_healing_surge_enhancer'),
(188443, 'spell_legion_sha_mastery_elemental_overload_hack'),
(117014, 'spell_legion_sha_mastery_elemental_overload_hack'),
(210714, 'spell_legion_sha_mastery_elemental_overload_hack'),
(51505,  'spell_legion_sha_mastery_elemental_overload_hack'),
(188196, 'spell_legion_sha_mastery_elemental_overload_hack'),
(114052, 'spell_legion_sha_ascendance_restoration'),
(201633, 'spell_legion_sha_earthen_shield');

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (8537, 3691, 6826, 5760, 7108, 6518, 6523, 9449, 1941, 6336, 6321, 5503);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `MoveCurveId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `TimeToTarget`, `TimeToTargetScale`, `TimeToTargetExtraScale`, `DecalPropertiesId`, `VerifiedBuild`) VALUES
(8537, 12676, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Wind Rush Totem
(3691, 8382, 0, 0, 0, 0, 0, 0, 10000, 0, 22522),
(6826, 11353, 0, 0, 0, 0, 0, 0, 6000, 0, 22522),
(5760, 10471, 0, 0, 0, 0, 0, 0, 0, 0, 22522),
(7108, 11577, 0, 0, 0, 0, 0, 0, 0, 0, 22522),
(6518, 11137, 0, 0, 0, 0, 0, 0, 0, 0, 22522),
(6523, 11142, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Counterstrike Totem
(9449, 13436, 0, 0, 0, 0, 0, 0, 6000, 0, 22522),
(1941, 6473, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(6336, 11004, 0, 0, 0, 0, 0, 0, 0, 0, 22996),
(6321, 10991, 0, 0, 0, 0, 0, 0, 6000, 0, 22996), -- SpellId : 205532
(5503, 10205, 0, 0, 0, 0, 0, 1514, 3000, 0, 23420); -- SpellId : 199121

DELETE FROM `areatrigger_template` WHERE `Id` IN (11577, 11137, 11142, 12676);
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `AIName`, `ScriptName`, `VerifiedBuild`) VALUES
(11577, 0, 4, 8, 8, 0, 0, 0, 0, '', '', 22522),
(11137, 0, 4, 40, 40, 0, 0, 0, 0, '', '', 22522),
(11142, 0, 4, 20, 20, 0, 0, 0, 0, '', '', 23222),
(12676, 0, 4, 10, 10, 0, 0, 0, 0, '', 'areatrigger_legion_sha_wind_rush_totem', 22522); -- Wind Rush Totem

UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_sha_earthen_shield_totem' WHERE `Id`=10471;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_sha_earthquake' WHERE `Id`=8382;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_sha_voodoo_totem' WHERE `Id`=11577;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_sha_crashing_storm' WHERE `Id`=11353;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_sha_tidal_totem' WHERE `Id`=13436;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_sha_volcanic_inferno' WHERE `Id`=10991;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_sha_doom_vortex' WHERE `Id`=10205;

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (11137, 11142, 6473, 11004);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES
(11137, 0, 208963, 1),
(11142, 0, 208997, 2),
(6473, 1, 157384, 1),
(11004, 1, 207498, 1);

DELETE FROM `creature_template_addon` WHERE `entry` IN (102392, 106321, 106319, 106317, 73094, 53006, 105451, 60561, 97285, 97369, 104818, 2630);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `aiAnimKit`, `movementAnimKit`, `meleeAnimKit`, `auras`) VALUES
(102392, 0, 0, 0, 1, 0, 0, 0, 0, '202192'),
(106321, 0, 0, 0, 1, 0, 0, 0, 0, '210659'),
(106319, 0, 0, 0, 1, 0, 0, 0, 0, '210658'),
(106317, 0, 0, 0, 1, 0, 0, 0, 0, '210652'),
(73094, 0, 0, 0, 1, 0, 0, 0, 0, '146754'),
(53006, 0, 0, 0, 1, 0, 0, 0, 0, '98017'),
(105451, 0, 0, 0, 1, 0, 0, 0, 0, '208990'),
(60561, 0, 0, 0, 1, 0, 0, 0, 0, '116943'),
(97285, 0, 0, 0, 1, 0, 0, 0, 0, '192078'), -- Wind Rush Totem
(97369, 0, 0, 0, 0, 0, 0, 0, 0, '192226'),
(104818, 0, 0, 0, 0, 0, 0, 0, 0, '207495'),
(2630, 0, 0, 0, 0, 0, 0, 0, 0, '6474');

DELETE FROM `spell_proc` WHERE `SpellId` IN (168534, 200071, 77756, 236746, 191861, 191877, 197992, 16164, 207285, 207354, 207357, 204357, 53390, 77762, 210714, 33757, 192630, 215785, 216251, 199107, 207351, 
198248, 205495, 16246, 191717, 210689);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`, `Chance`) VALUES
(168534,  0x0,  0,        0x0,       0x0,     0x0,      0x0, 0x10000, 0x1,     0x0, 100),
(200071,  0x0, 11,       0x80,       0x0, 0x10000,      0x0,  0x4400, 0x1,     0x0, 100),
(77756,   0x0,  0, 0x10000000,       0x0,     0x0,      0x0,     0x0, 0x0,     0x0,   0),
(236746,  0x0, 11,        0x0,    0x1000,     0x0,      0x0,     0x0, 0x1,     0x0,   0),
(191861,  0x0, 11,        0x0,    0x1000,     0x0,      0x0,     0x0, 0x2,     0x0,   0),
(191877,  0x0, 11,        0x0,       0x0,  0x8000,      0x1,     0x0, 0x2,     0x0,   0),
(197992,  0x0, 11,        0x0,     0x100, 0x40000,  0x40000,     0x0, 0x2,     0x0,   0), -- Landslide Proc: Boulderfist(201897), Rockbiter(193786)
(16164,   0x0,  0,        0x0,       0x0,     0x0,      0x0,     0x0, 0x2,     0x2,   0),
(207285,  0x0, 11,        0x0,       0x0,     0x0,      0x0,  0x4400, 0x2,     0x2, 100),
(207354,  0x0, 11,    0x88040,       0x0,     0x0,      0x0,     0x0, 0x1,     0x0,   0),
(207357,  0x0, 11,      0x200,       0x0,     0x0,      0x0,     0x0, 0x1,     0x0,   0),
(204357,  0x0, 11,        0x0, 0x1000010,     0x0,      0x0,     0x0, 0x2,     0x0,   0),
(53390,   0x8, 11,       0x80,       0x0, 0x10000,      0x0,     0x0, 0x1,     0x0,   0),
(77762,   0x4, 11,        0x0,    0x1000,     0x0,      0x0,     0x0, 0x1,     0x0,   0),
(210714, 0x10, 11, 0x80000000,       0x0,     0x0,      0x0,     0x0, 0x2,     0x0,   0),
(33757,   0x0,  0,        0x0,       0x0,     0x0,      0x0,     0x0, 0x2,     0x0,  20),
(192630,  0x0, 11,        0x0,    0x1000,     0x0,      0x0,     0x0, 0x2,     0x0,   0),
(215785,  0x0, 11,        0x0,       0x0,     0x4,      0x0,     0x0, 0x1,     0x0,   0),
(216251,  0x0, 11,       0x80,       0x0, 0x10000,      0x0,     0x0, 0x1,     0x0,   0),
(199107,  0x0, 11,        0x1,       0x0,     0x4,      0x0,     0x0, 0x2,     0x0,   0),
(207351,  0x0, 11,      0x800,       0x0,     0x0,      0x0,     0x0, 0x1,     0x0,   0),
(198248,  0x0, 11,        0x0,     0x800,     0x0,      0x0,     0x0, 0x2,     0x0,   0),
(205495,  0x0, 11,        0x3,       0x0,     0x0,      0x0,     0x0, 0x4,     0x0,   0),
(16246,   0x0, 11, 0x80100083,    0x3004,     0x0, 0x800004,     0x0, 0x4,     0x0,   0),
(191717,  0x0, 11,        0x3,       0x0,     0x0, 0x008000,     0x0, 0x1,     0x0,   0),
(210689,  0x0, 11,        0x0,       0x0,     0x0,      0x0,     0x0, 0x2,     0x0,   0);

UPDATE `creature_template` SET `ScriptName` = 'npc_pet_shaman_earth_elemental' WHERE `entry` = 95072;
UPDATE `creature_template` SET `ScriptName` = 'npc_pet_shaman_spirit_wolf' WHERE `entry` IN (29264, 100820);
UPDATE `creature_template` SET `faction` = 35, `ScriptName` = 'npc_pet_shaman_fire_elemental' WHERE `entry` = 95061;
UPDATE `creature_template` SET `RegenHealth` = 0, `ScriptName` = 'npc_pet_shaman_earthen_shield_totem' WHERE `entry` = 100943;
UPDATE `creature_template` SET `unit_class` = 2, `ScriptName` = 'npc_pet_shaman_storm_elemental' WHERE `entry` = 77936;
UPDATE `creature_template` SET `spell1` = 36213, `spell2` = 118345, `spell3` = 118337, `spell4` = 118347, `AIName` = '', `ScriptName` = '' WHERE `entry` = 61056;
UPDATE `creature_template` SET `unit_class` = 2, `spell1` = 57984, `spell2` = 117588, `spell3` = 118297, `spell4` = 118350, `AIName` = '', `ScriptName` = '' WHERE `entry` = 61029;
UPDATE `creature_template` SET `unit_class` = 2, `spell1` = 157331, `spell2` = 157348, `spell3` = 157375, `spell4` = 157382, `AIName` = '', `ScriptName` = '' WHERE `entry` = 77942;
UPDATE `creature_template` SET `spell1` = 157504 WHERE `entry` = 78001;
UPDATE creature_template SET unit_flags = 0x00000006 WHERE entry = 105427;
UPDATE creature_template SET ScriptName = 'npc_pet_shaman_skyfury_totem' WHERE entry = 105427;
UPDATE creature_template SET ScriptName = 'npc_pet_shaman_voodoo_totem' WHERE entry = 100099;
UPDATE creature_template SET ScriptName = 'npc_pet_shaman_lightning_surge_totem' WHERE entry = 61245;
UPDATE creature_template SET unit_flags = 0x00000004 WHERE entry = 61245;
UPDATE creature_template SET unit_flags = 0x00000006, faction = 2401 WHERE entry = 100943;
UPDATE creature_template SET `spell1` = 98007 WHERE `entry` = 53006;
UPDATE creature_template SET `spell1` = 114941 WHERE `entry` = 59764;
UPDATE creature_template SET ScriptName = 'npc_pet_shaman_lightning_elemental' WHERE entry = 97022;

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (198067, 198103);

DELETE FROM `spell_custom_attr` WHERE `entry`IN (114089);
INSERT INTO `spell_custom_attr` (`entry`, `attributes`) VALUES
(114089, 32768);
