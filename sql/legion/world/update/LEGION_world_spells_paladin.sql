DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_pal_legion_ardent_defender',
'spell_pal_legion_aura_mastery',
'spell_pal_legion_aura_of_mercy',
'spell_pal_legion_aura_of_sacrifice',
'spell_pal_legion_aura_of_sacrifice_absorb',
'spell_pal_legion_avenging_crusader',
'spell_pal_legion_avenging_crusader_group_heal',
'spell_pal_legion_avengers_shield',
'spell_pal_legion_avenging_light',
'spell_pal_legion_arcing_light_heal',
'spell_pal_legion_bastion_of_light',
'spell_pal_legion_beacon_of_light',
'spell_pal_legion_beacon_of_light_power_refund',
'spell_pal_legion_beacon_of_virtue',
'spell_pal_legion_blade_of_justice',
'spell_pal_legion_blade_of_wrath',
'spell_pal_legion_blade_of_wrath_proc',
'spell_pal_legion_blessed_hammer_absorb',
'spell_pal_legion_blessing_of_freedom',
'spell_pal_legion_blessing_of_protection',
'spell_pal_legion_blessing_of_sacrifice',
'spell_pal_legion_blessing_of_sanctuary',
'spell_pal_legion_blinding_light',
'spell_pal_legion_blinding_light_proc',
'spell_pal_legion_bulwark_of_order',
'spell_pal_legion_cleanse',
'spell_pal_legion_cleanse_the_weak_selector',
'spell_pal_legion_consecration',
'spell_pal_legion_consecration_heal',
'spell_pal_legion_crusade_helper',
'spell_pal_legion_crusaders_might',
'spell_pal_legion_darkest_before_the_dawn',
'spell_pal_legion_divine_hammer',
'spell_pal_legion_divine_hammer_aoe',
'spell_pal_legion_divine_intervention',
'spell_pal_legion_divine_punisher',
'spell_pal_legion_divine_purpose_holy',
'spell_pal_legion_divine_purpose_retri',
'spell_pal_legion_divine_shield',
'spell_pal_legion_divine_shield_pet',
'spell_pal_legion_divine_storm',
'spell_pal_legion_divine_steed',
'spell_pal_legion_divine_steed_mount',
'spell_pal_legion_eye_for_an_eye',
'spell_pal_legion_faith_s_armor',
'spell_pal_legion_fervent_martyr',
'spell_pal_legion_fist_of_justice',
'spell_pal_legion_fist_of_justice_retri',
'spell_pal_legion_flash_of_light',
'spell_pal_legion_grand_crusader',
'spell_pal_legion_greater_blessing_of_kings',
'spell_pal_legion_greater_blessing_of_wisdom',
'spell_pal_legion_hammer_of_reckoning',
'spell_pal_legion_hammer_of_the_righteous',
'spell_pal_legion_hand_of_hindrance',
'spell_pal_legion_holy_light',
'spell_pal_legion_holy_prism',
'spell_pal_legion_holy_prism_selector',
'spell_pal_legion_holy_shield',
'spell_pal_legion_holy_shock',
'spell_pal_legion_holy_wrath',
'spell_pal_legion_judgment',
'spell_pal_legion_judgment_of_light_proc',
'spell_pal_legion_judgments_of_the_pure',
'spell_pal_legion_judgment_prot_bonus',
'spell_pal_legion_knight_of_the_silver_hand',
'spell_pal_legion_last_defender',
'spell_pal_legion_lawbringer',
'spell_pal_legion_light_of_dawn',
'spell_pal_legion_light_of_the_martyr',
'spell_pal_legion_light_of_the_protector',
'spell_pal_legion_light_s_beacon',
'spell_pal_legion_light_s_hammer_periodic',
'spell_pal_legion_luminescence',
'spell_pal_legion_pure_of_heart',
'spell_pal_legion_power_of_the_silver_hand',
'spell_pal_legion_repentance',
'spell_pal_legion_retribution',
'spell_pal_legion_retribution_aura',
'spell_pal_legion_seraphim',
'spell_pal_legion_second_sunrise',
'spell_pal_legion_shield_of_the_righteous',
'spell_pal_legion_shield_of_vengeance',
'spell_pal_legion_spreading_the_word',
'spell_pal_legion_spreading_the_world_selector',
'spell_pal_legion_templars_verdict',
'spell_pal_legion_tyrs_deliverance',
'spell_pal_legion_ultimate_sacrifice',
'spell_pal_legion_unbreakable_will',
'spell_pal_legion_unbreakable_will_cc_info',
'spell_pal_legion_vengeance_aura',
'spell_pal_legion_wake_of_ashes',
'spell_pal_legion_word_of_glory',
'spell_pal_legion_zeal'
);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(31850,  'spell_pal_legion_ardent_defender'),
(31821,  'spell_pal_legion_aura_mastery'),
(210291, 'spell_pal_legion_aura_of_mercy'),
(183416, 'spell_pal_legion_aura_of_sacrifice'),
(210372, 'spell_pal_legion_aura_of_sacrifice_absorb'),
(216331, 'spell_pal_legion_avenging_crusader'),
(216371, 'spell_pal_legion_avenging_crusader_group_heal'),
(31935,  'spell_pal_legion_avengers_shield'),
(199441, 'spell_pal_legion_avenging_light'),
(119952, 'spell_pal_legion_arcing_light_heal'),
(204035, 'spell_pal_legion_bastion_of_light'),
(53563,  'spell_pal_legion_beacon_of_light'),
(156910, 'spell_pal_legion_beacon_of_light'),
(177168, 'spell_pal_legion_beacon_of_light'),
(200025, 'spell_pal_legion_beacon_of_light'),
(231642, 'spell_pal_legion_beacon_of_light_power_refund'),
(200025, 'spell_pal_legion_beacon_of_virtue'),
(184575, 'spell_pal_legion_blade_of_justice'),
(231843, 'spell_pal_legion_blade_of_wrath'),
(231832, 'spell_pal_legion_blade_of_wrath_proc'),
(229976, 'spell_pal_legion_blessed_hammer_absorb'),
(1044,   'spell_pal_legion_blessing_of_freedom'),
(1022,   'spell_pal_legion_blessing_of_protection'),
(6940,   'spell_pal_legion_blessing_of_sacrifice'),
(210256, 'spell_pal_legion_blessing_of_sanctuary'),
(115750, 'spell_pal_legion_blinding_light'),
(105421, 'spell_pal_legion_blinding_light_proc'),
(209389, 'spell_pal_legion_bulwark_of_order'),
(4987,   'spell_pal_legion_cleanse'),
(199360, 'spell_pal_legion_cleanse_the_weak_selector'),
(228473, 'spell_pal_legion_cleanse_the_weak_selector'),
(26573,  'spell_pal_legion_consecration'),
(205228, 'spell_pal_legion_consecration'),
(204241, 'spell_pal_legion_consecration_heal'),
(53385,  'spell_pal_legion_crusade_helper'),
(85256,  'spell_pal_legion_crusade_helper'),
(210191, 'spell_pal_legion_crusade_helper'),
(213757, 'spell_pal_legion_crusade_helper'),
(215661, 'spell_pal_legion_crusade_helper'),
(196926, 'spell_pal_legion_crusaders_might'),
(210378, 'spell_pal_legion_darkest_before_the_dawn'),
(198034, 'spell_pal_legion_divine_hammer'),
(198137, 'spell_pal_legion_divine_hammer_aoe'),
(213313, 'spell_pal_legion_divine_intervention'),
(204914, 'spell_pal_legion_divine_punisher'),
(197646, 'spell_pal_legion_divine_purpose_holy'),
(223817, 'spell_pal_legion_divine_purpose_retri'),
(642,    'spell_pal_legion_divine_shield'),
(228050, 'spell_pal_legion_divine_shield_pet'),
(53385,  'spell_pal_legion_divine_storm'),
(190784, 'spell_pal_legion_divine_steed'),
(221886, 'spell_pal_legion_divine_steed_mount'),
(221887, 'spell_pal_legion_divine_steed_mount'),
(221883, 'spell_pal_legion_divine_steed_mount'),
(221885, 'spell_pal_legion_divine_steed_mount'),
(205191, 'spell_pal_legion_eye_for_an_eye'),
(211903, 'spell_pal_legion_faith_s_armor'),
(196923, 'spell_pal_legion_fervent_martyr'),
(198054, 'spell_pal_legion_fist_of_justice'),
(234299, 'spell_pal_legion_fist_of_justice_retri'),
(19750,  'spell_pal_legion_flash_of_light'),
(85043,  'spell_pal_legion_grand_crusader'),
(203538, 'spell_pal_legion_greater_blessing_of_kings'),
(203539, 'spell_pal_legion_greater_blessing_of_wisdom'),
(204939, 'spell_pal_legion_hammer_of_reckoning'),
(53595,  'spell_pal_legion_hammer_of_the_righteous'),
(183218, 'spell_pal_legion_hand_of_hindrance'),
(82326,  'spell_pal_legion_holy_light'),
(114165, 'spell_pal_legion_holy_prism'),
(114852, 'spell_pal_legion_holy_prism_selector'),
(114871, 'spell_pal_legion_holy_prism_selector'),
(152261, 'spell_pal_legion_holy_shield'),
(20473,  'spell_pal_legion_holy_shock'),
(210220, 'spell_pal_legion_holy_wrath'),
(20271,  'spell_pal_legion_judgment'),
(196941, 'spell_pal_legion_judgment_of_light_proc'),
(216869, 'spell_pal_legion_judgments_of_the_pure'),
(231657, 'spell_pal_legion_judgment_prot_bonus'),
(200302, 'spell_pal_legion_knight_of_the_silver_hand'),
(203791, 'spell_pal_legion_last_defender'),
(216527, 'spell_pal_legion_lawbringer'),
(85222,  'spell_pal_legion_light_of_dawn'),
(219562, 'spell_pal_legion_light_of_the_martyr'),
(184092, 'spell_pal_legion_light_of_the_protector'),
(213652, 'spell_pal_legion_light_of_the_protector'),
(53651,  'spell_pal_legion_light_s_beacon'),
(114918, 'spell_pal_legion_light_s_hammer_periodic'),
(199428, 'spell_pal_legion_luminescence'),
(210540, 'spell_pal_legion_pure_of_heart'),
(200656, 'spell_pal_legion_power_of_the_silver_hand'),
(20066,  'spell_pal_legion_repentance'),
(183435, 'spell_pal_legion_retribution'),
(203797, 'spell_pal_legion_retribution_aura'),
(152262, 'spell_pal_legion_seraphim'),
(200482, 'spell_pal_legion_second_sunrise'),
(53600,  'spell_pal_legion_shield_of_the_righteous'),
(184662, 'spell_pal_legion_shield_of_vengeance'),
(199456, 'spell_pal_legion_spreading_the_word'),
(199507, 'spell_pal_legion_spreading_the_world_selector'),
(199508, 'spell_pal_legion_spreading_the_world_selector'),
(85256,  'spell_pal_legion_templars_verdict'),
(200653, 'spell_pal_legion_tyrs_deliverance'),
(199448, 'spell_pal_legion_ultimate_sacrifice'),
(182234, 'spell_pal_legion_unbreakable_will'),
(182497, 'spell_pal_legion_unbreakable_will_cc_info'),
(210323, 'spell_pal_legion_vengeance_aura'),
(205273, 'spell_pal_legion_wake_of_ashes'),
(210191, 'spell_pal_legion_word_of_glory'),
(217020, 'spell_pal_legion_zeal');

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` = 223316 AND `spell_effect` = 225833;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
(223316, 225833, 2, 'Fervent Martyr Button Glow');

DELETE FROM `spell_proc` WHERE `SpellId` IN (
203316,
223817,
197646,
200474,
231642,
223316,
53576,
196923,
198054,
85043,
183778,
200373,
183416,
231657,
200302,
200430,
209389,
199441,
216327,
216868,
216331,
204914,
204898,
209539,
209474,
215652,
199456,
209341,
234299,
200482,
216860,
204940
);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`) VALUES
(203316, 0x1, 10,        0x0,  0x8000,       0x0, 0x0,    0x0, 0x4, 0x0), -- The Fires of Justice
(223817, 0x3, 10, 0x80000400, 0x20000, 0x1006000, 0x0,    0x0, 0x4, 0x0), -- Divine Purpose (Retri)
(197646, 0x2, 10,   0x200000,     0x0,   0x40000, 0x0,0x15400, 0x4, 0x0), -- Divine Purpose (Holy)
(200474, 0x2, 10, 0x40000000,     0x0,     0x400, 0x0,    0x0, 0x2, 0x0), -- Power of the silver hand
(231642, 0x2, 10, 0x40000000,     0x0,     0x400, 0x0, 0x4000, 0x2, 0x0), -- Beacon of Light
(53576,  0x2, 10,   0x200000, 0x10000,       0x0, 0x0,    0x0, 0x2, 0x2), -- Infusion of Light (Holy)
(196923, 0x2, 10, 0x40000000,     0x0,     0x400, 0x0, 0x4000, 0x2, 0x0), -- Fervent Martyr
(198054, 0x2, 10,   0x800000,     0x0,       0x0, 0x0,    0x0, 0x4, 0x0), -- Fist of Justice
(85043,  0x1, 10,        0x0, 0x40000,       0x0, 0x0,   0x10, 0x4, 0x0), -- Grand Crusader
(183778, 0x2, 10,   0x800000,     0x0,       0x0, 0x0,    0x0, 0x2, 0x0), -- Judgment of Light
(200373, 0x2, 10,        0x0,   0x100,       0x0, 0x0,    0x0, 0x2, 0x0), -- Vindicator
(183416, 0x0, 10,        0x0,     0x0,       0x0, 0x0, 0x4000, 0x2, 0x0), -- Aura of Sacrifice
(231657, 0x2, 10,   0x800000,     0x0,       0x0, 0x0,   0x10, 0x4, 0x0), -- Judgment prot level bonus
(200302, 0x2, 10,   0x800000,     0x0,       0x0, 0x0,    0x0, 0x4, 0x0), -- Knight of the Silver Hand
(200430, 0x0, 10,        0x0,     0x0,    0x8000, 0x0,    0x0, 0x4, 0x0), -- Protection of Tyr
(209389, 0x2, 10,     0x4000,     0x0,       0x0, 0x0,    0x0, 0x2, 0x0), -- Bulwark of Order
(199441, 0x0, 10,        0x0,     0x0,     0x400, 0x0,    0x0, 0x2, 0x0), -- Avenging Light
(216327, 0x0, 10,        0x0,     0x0,     0x400, 0x0,    0x0, 0x4, 0x0), -- Light's Grace
(216868, 0x0, 10,        0x0, 0x4000000,     0x0, 0x0,    0x0, 0x2, 0x0), -- Hallowed Ground
(216331, 0x2, 10,   0x800000,  0x8000,       0x0, 0x0,    0x0, 0x2, 0x0), -- Avenging Crusader
(204914, 0x2, 10,   0x800000,     0x0,       0x0, 0x0,    0x0, 0x2, 0x0), -- Divine Punisher
(204898, 0x2, 10,   0x800000,     0x0,       0x0, 0x0,    0x0, 0x2, 0x0), -- Lawbringer
(209539, 0x0, 10,        0x0,     0x0,   0x0, 0x20000000, 0x0, 0x2, 0x0), -- Light of the Titans
(209474, 0x0, 10,     0x4000,     0x0,       0x0, 0x0,    0x0, 0x2, 0x0), -- Tyr's Enforcer
(215652, 0x0, 10,     0x4000,     0x0,       0x0, 0x0,    0x0, 0x2, 0x0), -- Shield of Virtue
(199456, 0x0, 10, 0x00000090,     0x0,       0x0, 0x0,    0x0, 0x1, 0x0), -- Spreading the Word
(209341, 0x1, 10,        0x0,     0x0, 0x0800000, 0x0, 0x4000, 0x2, 0x0), -- Painful Truths
(234299, 0x0, 10,        0x0,     0x0,       0x0, 0x0,    0x0, 0x4, 0x0), -- Fist of Justice (Retri)
(200482, 0x0, 10,        0x0,     0x0, 0x0040000, 0x0,    0x0, 0x4, 0x0), -- Second Sunrise
(216860, 0x0, 10, 0x00800000,     0x0,       0x0, 0x0,    0x0, 0x4, 0x0), -- Judgments of the Pure
(204940, 0x0, 10,        0x0, 0x00080,       0x0, 0x0,    0x0, 0x4, 0x0); -- Hammer of Reckoning 

DELETE FROM spell_linked_spell WHERE spell_trigger = 53563 AND spell_effect = 53651; -- Light's Beacon is applied by default on paladin since legion 

DELETE FROM `areatrigger_template` WHERE `Id` IN (50000, 50001, 50002, 50003, 50004);
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `AIName`, `ScriptName`, `VerifiedBuild`) VALUES
(50000, 0, 4, 10, 10, 0, 0, 0, 0, '', 'areatrigger_pal_legion_devotion_aura', 0),
(50001, 0, 4, 40, 40, 0, 0, 0, 0, '', 'areatrigger_pal_legion_aura_of_sacrifice', 0),
(50002, 0, 4,  8,  8, 0, 0, 0, 0, '', 'areatrigger_pal_legion_last_defender', 0),
(50003, 0, 4, 20, 20, 0, 0, 0, 0, '', '', 0),
(50004, 0, 4, 1, 1, 0, 0, 0, 0, '', '', 0);

UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_pal_legion_consecration' WHERE `Id`=9228;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_pal_legion_blessed_hammer' WHERE `Id`=10698;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_pal_legion_divine_tempest' WHERE `Id`=9110;

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (6770, 6777, 5975, 4488, 6006, 4366, 6013, 6804, 5546);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `MoveCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `TimeToTargetExtraScale`, `VerifiedBuild`) VALUES
(6770, 50000, 0, 0, 0, 0, 0, 0, 0, 0, 22996),
(6777, 50001, 0, 0, 0, 0, 0, 0, 0, 0, 22996),
(5975, 50002, 0, 0, 0, 0, 0, 0, 0, 0, 22996),
(4488, 9228, 0, 0, 0, 0, 0, 0, 0, 0, 22996),
(6006, 10698, 0, 0, 0, 0, 0, 5000, 5000, 0, 22522), -- 204019
(4366, 9110, 0, 0, 0, 0, 0, 1750, 1750, 0, 23420), -- 186775
(6013, 10700, 0, 0, 0, 0, 0, 0, 6000, 0, 22522), -- SpellId : 204150
(6804, 50003, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5546, 50004, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `spell_areatrigger_splines` WHERE `SpellMiscId` IN (6006, 4366, 4366);
INSERT INTO `spell_areatrigger_splines` (`SpellMiscId`, `Idx`, `X`, `Y`, `Z`, `VerifiedBuild`) VALUES
(6006, 28, -12.15229, -0.8607076, 1, 22522),
(6006, 27, -12.15229, -0.8607076, 1, 22522),
(6006, 26, -8.884105, -7.802585, 1, 22522),
(6006, 25, -1.942084, -10.28533, 1, 22522),
(6006, 24, 4.654531, -7.367979, 1, 22522),
(6006, 23, 6.697264, -0.8606191, 1, 22522),
(6006, 22, 3.889161, 4.970629, 1, 22522),
(6006, 21, -1.942115, 6.993277, 1, 22522),
(6006, 20, -7.217988, 4.415323, 1, 22522),
(6006, 19, -9.010657, -0.8605769, 1, 22522),
(6006, 18, -6.662641, -5.581196, 1, 22522),
(6006, 17, -1.942056, -7.143781, 1, 22522),
(6006, 16, 2.223082, -5.025853, 1, 22522),
(6006, 15, 3.555749, -0.8605381, 1, 22522),
(6006, 14, 1.66775, 2.74921, 1, 22522),
(6006, 13, -1.942143, 3.851732, 1, 22522),
(6006, 12, -4.996599, 2.19386, 1, 22522),
(6006, 11, -5.869143, -0.8606579, 1, 22522),
(6006, 10, -4.44123, -3.359777, 1, 22522),
(6006, 9, -1.942081, -4.002206, 1, 22522),
(6006, 8, 0.001639928, -2.804359, 1, 22522),
(6006, 7, 0.4140604, -0.8606385, 1, 22522),
(6006, 6, -0.5536606, 0.5277904, 1, 22522),
(6006, 5, -1.942118, 0.710157, 1, 22522),
(6006, 4, -2.775157, -0.02763466, 1, 22522),
(6006, 3, -2.727454, -0.8605575, 1, 22522),
(6006, 2, -2.219819, -1.138357, 1, 22522),
(6006, 1, -1.942106, -0.8606307, 1, 22522),
(6006, 0, -1.942106, -0.8606307, 1, 22522),
(4366, 9, 20.00000, 0.0002642955, 1.00, 0),
(4366, 8, 20.00000, 0.0002642955, 1.00, 0),
(4366, 7, 16.08187, 0.0002358239, 1.00, 0),
(4366, 6, 13.08165, 0.0001822146, 1.00, 0),
(4366, 5, 10.08178, 0.0001537431, 1.00, 0),
(4366, 4, 7.081728, 0.000284878, 1.00, 0),
(4366, 3, 4.081857, 0.0002564064, 1.00, 0),
(4366, 2, 1.081643, 0.0002027971, 1.00, 0),
(4366, 1, 0.00000, 0.0001743255, 1.00, 0),
(4366, 0, 0.00000, 0.0001743255, 1.00, 0);

DELETE FROM `spell_areatrigger_scale` WHERE `SpellMiscId` IN (4488, 4366);
INSERT INTO `spell_areatrigger_scale` (`SpellMiscId`, `ScaleCurveExtra5`, `ScaleCurveExtra6`) VALUES 
(4488, 1, 1),
(4366, 1, 1);

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (10700, 50003, 50004);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES 
(10700, 0, 204335, 1),
(50003, 0, 210540, 1),
(50004, 0, 199544, 2);

UPDATE creature_template SET modelid1 = 11686, `InhabitType` = 8, ScriptName = 'npc_pal_legion_light_s_hammer', AIName = '' WHERE entry = 59738;
UPDATE creature_template SET HoverHeight = 5, ScriptName = 'npc_pal_legion_guardian_of_the_forgotten_queen' WHERE entry = 114565;

DELETE FROM `spell_custom_attr` WHERE `entry` = 184689;
INSERT INTO `spell_custom_attr` (`entry`, `attributes`) VALUES
(184689, 0x00000008);
