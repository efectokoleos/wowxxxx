DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_dk_legion_army_transform',
'spell_dk_legion_death_and_decay',
'spell_dk_legion_frost_avalanche'
);

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(127517, 'spell_dk_legion_army_transform'),
(43265, 'spell_dk_legion_death_and_decay'),
(207142, 'spell_dk_legion_frost_avalanche');

DELETE FROM `spell_proc` WHERE `SpellId` IN (207142);
INSERT INTO `spell_proc` (`SpellId`, `SpellPhaseMask`, `HitMask`) VALUES
(207142, 0x2, 0x2); -- Avalanche

DELETE FROM `areatrigger_template` WHERE `Id` IN (4485);
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `VerifiedBuild`, `ScriptName`) VALUES
(4485, 0, 0, 6.5, 8, 0, 0, 0, 0, 0, 'areatrigger_dk_legion_death_and_decay');

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (5070);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES
(5070, 0, 145629, 3);

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (1193);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `MoveCurveId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `TimeToTargetExtraScale`, `VerifiedBuild`) VALUES
(1193, 5070, 0, 0, 0, 0, 0, 0, 0, 0, 23420);
