DELETE FROM `quest_offer_reward` WHERE `ID` IN (29393 /*Brew For Brewfest*/, 29396 /*A New Supplier of Souvenirs*/, 40585 /*Thus Begins the War*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(29393, 0, 0, 0, 0, 0, 0, 0, 0, 'That crash between here and Bladefist Bay really messed up the brew flow!  It doesn''t help that Dark Iron dwarves seem to be attacking every hour or so!$b$bSo if you want to help, here''s the deal.  I''ll let you borrow one of my rams.$b$bEvery time you bring me a keg, I''ll give you some tokens.  I''ll even let you use the ram for a bit longer for every keg you bring me!$b$bBUT, once you start this, you won''t be able to do it again until tomorrow.$b$bAre you sure you''re ready to do this?', 22624), -- Brew For Brewfest
(29396, 0, 0, 0, 0, 0, 0, 0, 0, 'What''s this?  A stein voucher?  So you like drinking?  Great!  After I give you this stein, go have a few drinks!  Then a few more.  Maybe have a few more after that...  Then, now this is important, come see me.$b$bLook, I know you''ll be back eventually.  If you want your Brewfest tokens redeemed you have to see me.$b$bAnyways, here''s the stein, and remember to talk to me later.', 22624), -- A New Supplier of Souvenirs
(40585, 1, 0, 0, 0, 0, 0, 0, 0, 'An excellent choice, $n. As more opportunities arise, I will bring them to your attention. Good luck in battle!', 22624); -- Thus Begins the War

UPDATE `quest_offer_reward` SET `Emote1`=0, `RewardText`='Thanks for your help!  As long as Brewfest is going, the Dark Iron dwarves are attacking and our supplier is stuck in that canyon, I''ll need some help shipping kegs.  So if you want to work for more tokens, talk to me every day.$b$bHere are the tokens we promised you.  Have fun!', `VerifiedBuild`=22624 WHERE `ID`=11412; -- There and Back Again
UPDATE `quest_offer_reward` SET `RewardText`='Ah, so Direbrew finally submitted his brew.  What a pity he had to die to do it...$B$BOh, well.  Bottoms up, eh?', `VerifiedBuild`=22624 WHERE `ID`=12492; -- Direbrew's Dire Brew
UPDATE `quest_offer_reward` SET `RewardText`='So you managed to stop the Dark Irons... While drunk too?  That, $c, is truly the spirit of Brewfest!  I guess in keeping with the spirit of Brewfest you want your prize tokens.$b$bThanks for your help, $n.  Enjoy the rest of Brewfest.', `VerifiedBuild`=22624 WHERE `ID`=12192; -- This One Time, When I Was Drunk...

DELETE FROM `quest_details` WHERE `ID` IN (29396 /*A New Supplier of Souvenirs*/, 12191 /*Chug and Chuck!*/, 12192 /*This One Time, When I Was Drunk...*/, 12492 /*Direbrew's Dire Brew*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(29396, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- A New Supplier of Souvenirs
(12191, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Chug and Chuck!
(12192, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- This One Time, When I Was Drunk...
(12492, 0, 0, 0, 0, 0, 0, 0, 0, 22624); -- Direbrew's Dire Brew


DELETE FROM `quest_request_items` WHERE `ID`=29396;
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(29396, 0, 0, 0, 0, 'What''s happening hot stuff?  Got something for me?', 22624); -- A New Supplier of Souvenirs

UPDATE `quest_request_items` SET `EmoteOnComplete`=0, `CompletionText`='Did you see the mess that goblin made?  I hope you didn''t do too much reckless off-roading.', `VerifiedBuild`=22624 WHERE `ID`=11412; -- There and Back Again
UPDATE `quest_request_items` SET `CompletionText`='Hey $n, your face is all smudged!  Have you been to Blackrock Depths?', `VerifiedBuild`=22624 WHERE `ID`=12492; -- Direbrew's Dire Brew

DELETE FROM `creature_model_info` WHERE `DisplayID` IN (62206, 62172);
INSERT INTO `creature_model_info` (`DisplayID`, `BoundingRadius`, `CombatReach`, `DisplayID_Other_Gender`, `VerifiedBuild`) VALUES
(62206, 1.730602, 2, 0, 22624),
(62172, 1.730602, 2, 0, 22624);
