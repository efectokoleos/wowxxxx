DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_item_selfie_camera_view',
'spell_item_selfie_camera'
);

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(181765, 'spell_item_selfie_camera_view'),
(181884, 'spell_item_selfie_camera_view'),
(182562, 'spell_item_selfie_camera'),
(182575, 'spell_item_selfie_camera');
