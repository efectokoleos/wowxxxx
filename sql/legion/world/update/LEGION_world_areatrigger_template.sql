ALTER TABLE `areatrigger_template`
  ADD COLUMN `AIName` CHAR(64) DEFAULT '' NOT NULL AFTER `Data5`;

ALTER TABLE `spell_areatrigger`
  ADD COLUMN `TimeToTargetExtraScale` INT(10) UNSIGNED DEFAULT 0 NOT NULL AFTER `TimeToTargetScale`;

DROP TABLE IF EXISTS `spell_areatrigger_scale`;
CREATE TABLE `spell_areatrigger_scale` (
  `SpellMiscId` INT(10) UNSIGNED NOT NULL,
  `ScaleCurveOverride0` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveOverride1` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveOverride2` FLOAT DEFAULT 0 NOT NULL, 
  `ScaleCurveOverride3` FLOAT DEFAULT 0 NOT NULL, 
  `ScaleCurveOverride4` FLOAT DEFAULT 0 NOT NULL, 
  `ScaleCurveOverride5` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveOverride6` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveExtra0` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveExtra1` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveExtra2` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveExtra3` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveExtra4` INT(10) UNSIGNED DEFAULT 0 NOT NULL, 
  `ScaleCurveExtra5` FLOAT DEFAULT 0 NOT NULL, 
  `ScaleCurveExtra6` INT(10) UNSIGNED DEFAULT 0 NOT NULL,
  `VerifiedBuild` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`SpellMiscId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
