DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_pri_legion_angelic_feather',
'spell_pri_legion_archangel',
'spell_pri_legion_atonement',
'spell_pri_legion_atonement',
'spell_pri_legion_atonement_helper',
'spell_pri_legion_atonement_helper',
'spell_pri_legion_atonement_helper',
'spell_pri_legion_atonement_helper',
'spell_pri_legion_atonement_buff',
'spell_pri_legion_binding_heal',
'spell_pri_legion_call_of_the_void',
'spell_pri_legion_clarity_of_will',
'spell_pri_legion_dark_archangel',
'spell_pri_legion_circle_of_healing',
'spell_pri_legion_desperate_prayer',
'spell_pri_legion_dispersion',
'spell_pri_legion_divine_hymn',
'spell_pri_legion_enduring_renewal',
'spell_pri_legion_fade',
'spell_pri_legion_flash_heal',
'spell_pri_legion_focus_in_the_light',
'spell_pri_legion_guardian_spirit',
'spell_pri_legion_holy_fire_passive',
'spell_pri_legion_holy_word_chastise',
'spell_pri_legion_holy_word_sanctify',
'spell_pri_legion_invoke_the_naaru_copy_proc',
'spell_pri_legion_invoke_the_naaru_summon_proc',
'spell_pri_legion_last_word',
'spell_pri_legion_leap_of_faith',
'spell_pri_legion_leap_of_faith_effect_trigger',
'spell_pri_legion_levitate',
'spell_pri_legion_light_s_wrath',
'spell_pri_legion_lingering_insanity',
'spell_pri_legion_mana_leech_proc',
'spell_pri_legion_mania',
'spell_pri_legion_masochism_heal',
'spell_pri_legion_mastery_echo_of_light',
'spell_pri_legion_mental_fortitude',
'spell_pri_legion_mind_bomb',
'spell_pri_legion_mind_blast',
'spell_pri_legion_mind_control',
'spell_pri_legion_mind_flay',
'spell_pri_legion_mind_flay_aoe_selector',
'spell_pri_legion_penance',
'spell_pri_legion_penance_heal_damage',
'spell_pri_legion_penance_heal_damage',
'spell_pri_legion_penance_triggered',
'spell_pri_legion_penance_triggered',
'spell_pri_legion_power_of_the_naaru',
'spell_pri_legion_power_word_radiance',
'spell_pri_legion_power_word_shield',
'spell_pri_legion_power_word_solace',
'spell_pri_legion_prayer_of_mending_bounce',
'spell_pri_legion_prayer_of_mending_bounce_selector',
'spell_pri_legion_prayer_of_mending_initial',
'spell_pri_legion_prayer_of_mending_proc',
'spell_pri_legion_premonition',
'spell_pri_legion_premonition_damage',
'spell_pri_legion_psychic_link',
'spell_pri_legion_psychic_link_damage',
'spell_pri_legion_psyflay',
'spell_pri_legion_purge_the_wicked_selector',
'spell_pri_legion_purify',
'spell_pri_legion_rapture',
'spell_pri_legion_ray_of_hope',
'spell_pri_legion_renew',
'spell_pri_legion_renew_the_faith',
'spell_pri_legion_searing_light',
'spell_pri_legion_serendipity',
'spell_pri_legion_shadow_covenant',
'spell_pri_legion_shadow_mend_heal',
'spell_pri_legion_shadow_mend_periodic',
'spell_pri_legion_shadow_word_death',
'spell_pri_legion_shadow_word_death',
'spell_pri_legion_shadow_word_death_marker',
'spell_pri_legion_shadow_word_pain',
'spell_pri_legion_shadow_mania',
'spell_pri_legion_shadowform',
'spell_pri_legion_shadowy_apparitions',
'spell_pri_legion_shadowy_insight',
'spell_pri_legion_smite',
'spell_pri_legion_smite_absorb',
'spell_pri_legion_sphere_of_insanity',
'spell_pri_legion_spirit_of_redemption_honor_talent',
'spell_pri_legion_spirit_of_redemption',
'spell_pri_legion_surge_of_light',
'spell_pri_legion_surrender_to_madness',
'spell_pri_legion_sustained_sanity',
'spell_pri_legion_symbol_of_hope',
'spell_pri_legion_trail_of_light',
'spell_pri_legion_twist_of_fate',
'spell_pri_legion_unleash_the_shadows',
'spell_pri_legion_vampiric_embrace',
'spell_pri_legion_vampiric_embrace_heal',
'spell_pri_legion_vampiric_touch',
'spell_pri_legion_vim_and_vigor',
'spell_pri_legion_void_bolt',
'spell_pri_legion_void_shield',
'spell_pri_legion_void_eruption',
'spell_pri_legion_voidform',
'spell_pri_legion_voidform_controller',
'spell_pri_legion_void_shift'
);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(121536, 'spell_pri_legion_angelic_feather'),
(197862, 'spell_pri_legion_archangel'),
(81749,  'spell_pri_legion_atonement'),
(195178, 'spell_pri_legion_atonement'),
(17,     'spell_pri_legion_atonement_helper'),
(186263, 'spell_pri_legion_atonement_helper'),
(194509, 'spell_pri_legion_atonement_helper'),
(200829, 'spell_pri_legion_atonement_helper'),
(194384, 'spell_pri_legion_atonement_buff'),
(32546,  'spell_pri_legion_binding_heal'),
(193371, 'spell_pri_legion_call_of_the_void'),
(152118, 'spell_pri_legion_clarity_of_will'),
(197871, 'spell_pri_legion_dark_archangel'),
(204883, 'spell_pri_legion_circle_of_healing'),
(19236,  'spell_pri_legion_desperate_prayer'),
(47585,  'spell_pri_legion_dispersion'),
(64844,  'spell_pri_legion_divine_hymn'),
(200153, 'spell_pri_legion_enduring_renewal'),
(586,    'spell_pri_legion_fade'),
(2061,   'spell_pri_legion_flash_heal'),
(196419, 'spell_pri_legion_focus_in_the_light'),
(47788,  'spell_pri_legion_guardian_spirit'),
(231687, 'spell_pri_legion_holy_fire_passive'),
(88625,  'spell_pri_legion_holy_word_chastise'),
(34861,  'spell_pri_legion_holy_word_sanctify'),
(196705, 'spell_pri_legion_invoke_the_naaru_copy_proc'),
(196684, 'spell_pri_legion_invoke_the_naaru_summon_proc'),
(215776, 'spell_pri_legion_last_word'),
(73325,  'spell_pri_legion_leap_of_faith'),
(92833,  'spell_pri_legion_leap_of_faith_effect_trigger'),
(1706,   'spell_pri_legion_levitate'),
(207946, 'spell_pri_legion_light_s_wrath'),
(197937, 'spell_pri_legion_lingering_insanity'),
(123050, 'spell_pri_legion_mana_leech_proc'),
(193173, 'spell_pri_legion_mania'),
(193065, 'spell_pri_legion_masochism_heal'),
(77485,  'spell_pri_legion_mastery_echo_of_light'),
(194018, 'spell_pri_legion_mental_fortitude'),
(205369, 'spell_pri_legion_mind_bomb'),
(8092,   'spell_pri_legion_mind_blast'),
(205364, 'spell_pri_legion_mind_control'),
(15407,  'spell_pri_legion_mind_flay'),
(234696, 'spell_pri_legion_mind_flay_aoe_selector'),
(47540,  'spell_pri_legion_penance'),
(47750,  'spell_pri_legion_penance_heal_damage'),
(47666,  'spell_pri_legion_penance_heal_damage'),
(47757,  'spell_pri_legion_penance_triggered'),
(47758,  'spell_pri_legion_penance_triggered'),
(196489, 'spell_pri_legion_power_of_the_naaru'),
(194509, 'spell_pri_legion_power_word_radiance'),
(17,     'spell_pri_legion_power_word_shield'),
(129250, 'spell_pri_legion_power_word_solace'),
(225275, 'spell_pri_legion_prayer_of_mending_bounce'),
(155793, 'spell_pri_legion_prayer_of_mending_bounce_selector'),
(33076,  'spell_pri_legion_prayer_of_mending_initial'),
(41635,  'spell_pri_legion_prayer_of_mending_proc'),
(209780, 'spell_pri_legion_premonition'),
(209885, 'spell_pri_legion_premonition_damage'),
(8092,   'spell_pri_legion_psychic_link'),
(205448, 'spell_pri_legion_psychic_link'),
(199486, 'spell_pri_legion_psychic_link_damage'),
(199845, 'spell_pri_legion_psyflay'),
(204215, 'spell_pri_legion_purge_the_wicked_selector'),
(527,    'spell_pri_legion_purify'),
(47536,  'spell_pri_legion_rapture'),
(197268, 'spell_pri_legion_ray_of_hope'),
(139,    'spell_pri_legion_renew'),
(196529, 'spell_pri_legion_renew_the_faith'),
(215768, 'spell_pri_legion_searing_light'),
(63733,  'spell_pri_legion_serendipity'),
(204065, 'spell_pri_legion_shadow_covenant'),
(186263, 'spell_pri_legion_shadow_mend_heal'),
(187464, 'spell_pri_legion_shadow_mend_periodic'),
(32379,  'spell_pri_legion_shadow_word_death'),
(199911, 'spell_pri_legion_shadow_word_death'),
(226734, 'spell_pri_legion_shadow_word_death_marker'),
(589,    'spell_pri_legion_shadow_word_pain'),
(199572, 'spell_pri_legion_shadow_mania'),
(232698, 'spell_pri_legion_shadowform'),
(78203,  'spell_pri_legion_shadowy_apparitions'),
(162452, 'spell_pri_legion_shadowy_insight'),
(585,    'spell_pri_legion_smite'),
(208771, 'spell_pri_legion_smite_absorb'),
(194230, 'spell_pri_legion_sphere_of_insanity'),
(215769, 'spell_pri_legion_spirit_of_redemption_honor_talent'),
(20711,  'spell_pri_legion_spirit_of_redemption'),
(109186, 'spell_pri_legion_surge_of_light'),
(193223, 'spell_pri_legion_surrender_to_madness'),
(219772, 'spell_pri_legion_sustained_sanity'),
(64901,  'spell_pri_legion_symbol_of_hope'),
(200128, 'spell_pri_legion_trail_of_light'),
(109142, 'spell_pri_legion_twist_of_fate'),
(194093, 'spell_pri_legion_unleash_the_shadows'),
(15286,  'spell_pri_legion_vampiric_embrace'),
(15290,  'spell_pri_legion_vampiric_embrace_heal'),
(34914,  'spell_pri_legion_vampiric_touch'),
(195483, 'spell_pri_legion_vim_and_vigor'),
(234746, 'spell_pri_legion_void_bolt'),
(199145, 'spell_pri_legion_void_shield'),
(228260, 'spell_pri_legion_void_eruption'),
(194249, 'spell_pri_legion_voidform'),
(210199, 'spell_pri_legion_voidform_controller'),
(108968, 'spell_pri_legion_void_shift');

DELETE FROM `spell_proc` WHERE `SpellId` IN (
231687,
231403,
200128,
200153,
197031,
78203,
205371,
162452,
197763,
198068,
194093,
194230,
194018,
193371,
196684,
196419,
196578,
196492,
215776,
215768,
215960,
213610
);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`, `Chance`) VALUES
(231687,  0x2, 6,       0x80,  0x20000,        0x0,   0x0, 0x11000, 0x2, 0x0,  20), -- Holy Fire
(231403,  0x2, 6,   0x100000,      0x0,        0x0,   0x0, 0x11000, 0x2, 0x0,   0), -- Holy Fire
(200128,  0x2, 6,      0x800,      0x0,        0x0,   0x0,     0x0, 0x2, 0x0,   0), -- Trail of Light
(200153,  0x0, 6,        0x0,      0x0,        0x0,   0x0,  0x4000, 0x2, 0x0, 100), -- Enduring Renewal
(197031,  0x2, 6, 0x10000400,      0x0,        0x0,   0x0,     0x0, 0x2, 0x0,   0), -- Divinity
(78203,  0x20, 6,     0x8000,      0x0,        0x0,   0x0,     0x0, 0x0, 0x2,   0), -- Shadowy Apparitions
(205371, 0x20, 6,        0x0,      0x0, 0x00000040,   0x0,     0x0, 0x2, 0x0,   0), -- Void Ray
(162452, 0x20, 6,     0x8000,      0x0,        0x0,   0x0,     0x0, 0x0, 0x0,   0), -- Shadowy Insight
(197763,  0x0, 6,       0x80,    0x200,       0x80,   0x0,     0x0, 0x4, 0x0,   0), -- Borrowed Time
(198068, 0x24, 6,   0x108000,      0x0,        0x0,   0x0,     0x0, 0x2, 0x0,   0), -- Power of the Dark Side
(194093, 0x20, 6,        0x0,    0x400,        0x0,   0x0,     0x0, 0x0, 0x2,   0), -- Unleash the Shadows
(194230, 0x20, 6,     0x2000,      0x0,       0x40, 0x400,     0x0, 0x2, 0x0,   0), -- Sphere of Insanity
(194018, 0x20, 6,        0x0,    0x400,        0x0,   0x0,     0x0, 0x0, 0x0,   0), -- Mental Fortitude
(193371, 0x20, 6,        0x0,      0x0,       0x40,   0x0,     0x0, 0x2, 0x0,   0), -- Call to the Void
(196684,  0x2, 6, 0x10000400,      0x0,       0x20,   0x0,     0x0, 0x2, 0x0,   0), -- Invoke the Naaru
(196419,  0x2, 6,   0x100000,      0x0,       0x20,   0x0,     0x0, 0x2, 0x0,   0), -- Focus in the Light
(196578,  0x2, 6,     0x1800,      0x0,        0x0,   0x0,     0x0, 0x2, 0x2,   0), -- Blessing of T'uure
(196492,  0x2, 6,        0x0, 0x400000,        0x0,   0x0,     0x0, 0x2, 0x0,   0), -- Renew the Faith
(215776, 0x20, 6,        0x0,      0x2,        0x0,   0x0,     0x0, 0x2, 0x0,   0), -- Last Word
(215768,  0x2, 6,       0x80,      0x0,        0x0,   0x0,     0x0, 0x2, 0x0,   0), -- Searing Light
(215960,  0x2, 6,     0x1000,      0x0,        0x0,   0x0,     0x0, 0x1, 0x0,   0), -- Greater Heal
(213610,  0x0, 0,        0x0,      0x0,        0x0,   0x0,     0x0, 0x0, 0x100, 0); -- Holy Ward

UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_pri_legion_angelic_feather' WHERE `Id`=3153;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_pri_legion_divine_star' WHERE `Id`=6700;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_pri_legion_halo' WHERE `Id`=3921;

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (337, 658, 2148, 1489);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `MoveCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `TimeToTargetExtraScale`, `VerifiedBuild`) VALUES
(337,  3153, 0,    0, 0, 0, 0,    0, 600000, 0, 22522), -- 158624
(658,  3921, 0, 1355, 0, 0, 0,    0,   3200, 0, 22522), -- 120517
(1489, 5802, 0,    0, 0, 0, 0,    0,  10000, 0, 23360), -- 62618
(2148, 6700, 0,    0, 0, 0, 0, 1039,  15000, 0, 22522); -- 110744

DELETE FROM `spell_areatrigger_splines` WHERE `SpellMiscId` IN (2148);
INSERT INTO `spell_areatrigger_splines` (`SpellMiscId`, `Idx`, `X`, `Y`, `Z`, `VerifiedBuild`) VALUES
(2148, 18, 23.81006,  -0.0004795893, 1, 22522),
(2148, 17, 23.81006,  -0.0004795893, 1, 22522),
(2148, 16, 22.31002,  -0.0004477229, 1, 22522),
(2148, 15, 20.80998,  -0.0004158565, 1, 22522),
(2148, 14, 19.30995,  -0.0003839901, 1, 22522),
(2148, 13, 17.80991,  -0.0003521237, 1, 22522),
(2148, 12, 16.30987,  -0.0003202573, 1, 22522),
(2148, 11, 14.80983,  -0.0002883909, 1, 22522),
(2148, 10, 13.3098,   -0.0002565245, 1, 22522),
(2148,  9, 11.80976,  -0.0002246581, 1, 22522),
(2148,  8, 10.30972,  -0.0001927917, 1, 22522),
(2148,  7,  8.809687, -0.0001609253, 1, 22522),
(2148,  6,  7.30965,  -0.0001290589, 1, 22522),
(2148,  5,  5.809613, -9.71925E-05,  1, 22522),
(2148,  4,  4.309576, -6.532611E-05, 1, 22522),
(2148,  3,  2.809539, -3.345971E-05, 1, 22522),
(2148,  2,  1.309502, -1.593315E-06, 1, 22522),
(2148,  1, -0.1905355, 3.027308E-05, 1, 22522),
(2148,  0, -0.1905355, 3.027308E-05, 1, 22522);

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (5802);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES
(5802, 0, 81782, 3);

UPDATE `creature_template` SET `ScriptName` = 'npc_pet_pri_mindbender' WHERE `entry` = 62982;
UPDATE `creature_template` SET `ScriptName` = 'npc_pri_summon_call_of_the_void' WHERE `entry` = 98167;
UPDATE `creature_template` SET `InhabitType` = 8 WHERE `entry` IN (99904, 98680);
UPDATE `creature_template` SET `ScriptName` = 'npc_pri_summon_psyfiend', `mechanic_immune_mask` = (0x00000001 | 0x00000002 | 0x00000010 | 0x00000100 | 0x00000200 | 0x00000800 | 0x00002000 | 0x00010000 | 0x00020000 | 0x00800000 | 0x04000000 | 0x20000000) WHERE `entry` = 101398;

DELETE FROM creature_template_addon WHERE entry = 98680;
INSERT INTO creature_template_addon (entry, bytes1, bytes2) VALUES
(98680, 50331648, 1);

DELETE FROM spell_linked_spell WHERE spell_trigger = 196762;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
(196762, 196773, 0, 'Inner Focus linked spell');
