DELETE FROM `command` WHERE `name` = 'pvp';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES 
('pvp', 1042, 'Syntax: .pvp [on/off]\r\n\r\nEnable or Disable in game PVP MODE or show current state of on/off not provided.');

DELETE FROM `trinity_string` WHERE `entry` IN (20001, 20002);
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES 
(20001, 'PVP mode is ON'),
(20002, 'PVP mode is OFF');
