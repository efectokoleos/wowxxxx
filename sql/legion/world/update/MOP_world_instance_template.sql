UPDATE `instance_template` SET `script`='instance_gate_of_the_setting_sun'    WHERE `map`=962;
UPDATE `instance_template` SET `script`='instance_mogu_shan_palace'           WHERE `map`=994;
UPDATE `instance_template` SET `script`='instance_shado_pan_monastery'        WHERE `map`=959;
UPDATE `instance_template` SET `script`='instance_siege_of_niuzao_temple'     WHERE `map`=1011;
UPDATE `instance_template` SET `script`='instance_stormstout_brewery'         WHERE `map`=961;
UPDATE `instance_template` SET `script`='instance_temple_of_the_jade_serpent' WHERE `map`=960;
UPDATE `instance_template` SET `script`='instance_throne_of_thunder'          WHERE `map`=1098;

DELETE FROM `instance_template` WHERE `map` IN (1009, 1008, 1136, 996);
INSERT INTO `instance_template` (`map`, `parent`, `script`, `allowMount`) VALUES
(1009, 870, 'instance_heart_of_fear',             0),
(1008, 870, 'instance_mogu_shan_vault',           0),
(1136, 870, 'instance_siege_of_orgrimmar',        1),
(996,  870, 'instance_terrace_of_endless_spring', 1);
