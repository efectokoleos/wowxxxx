DELETE FROM `command` WHERE `name` LIKE 'server shell%';
DELETE FROM `command` WHERE `name` LIKE 'server state';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('server shell',              1031, 'Syntax: .server shell $subcommand\r\nType .server shell to see the list of possible subcommands or .help server shell $subcommand to see info on subcommands'),
('server shell update',       1032, 'Syntax: .server shell update\r\nUpdates server core.'),
('server shell restore',      1033, 'Syntax: .server shell restore\r\nRestores server core.'),
('server shell status',       1034, 'Syntax: .server shell status\r\nShows current compile status.'),
('server shell switchbranch', 1035, 'Syntax: .server shell switchbranch "branchname"\r\nSwitches current branch to specified branch.'),
('server state',              1036, 'Syntax: .server state\r\nSet server state.\r\n0 = Ready\r\n1 = Compiling\r\n2 = Restoring\r\n3 = Compile Error');
