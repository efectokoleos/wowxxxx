UPDATE `creature_template` SET `type_flags2`=0, `VerifiedBuild`=22594 WHERE `entry`=101074; -- Hatespawn Whelpling
UPDATE `creature_template` SET `minlevel`=110, `speed_run`=1.142857, `BaseAttackTime`=1561, `VerifiedBuild`=22594 WHERE `entry`=111506; -- Dark Zealot
UPDATE `creature_template` SET `minlevel`=110, `faction`=2402, `BaseAttackTime`=1676, `VerifiedBuild`=22594 WHERE `entry`=106551; -- Hati
UPDATE `creature_template` SET `speed_run`=1.142857, `VerifiedBuild`=22594 WHERE `entry`=114281; -- Flight Master's Mount
UPDATE `creature_template` SET `maxlevel`=100, `HoverHeight`=1, `VerifiedBuild`=22594 WHERE `entry`=86963; -- Soul Gem
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5, `VerifiedBuild`=22594 WHERE `entry`=89016; -- Ravyn-Drath
UPDATE `creature_template` SET `minlevel`=106, `maxlevel`=106, `BaseAttackTime`=1114, `VerifiedBuild`=22594 WHERE `entry`=95061; -- Greater Fire Elemental
UPDATE `creature_template` SET `minlevel`=110, `BaseAttackTime`=1821, `VerifiedBuild`=22594 WHERE `entry`=110975; -- Unseen Pathfinder
UPDATE `creature_template` SET `unit_flags2`=33556480, `VerifiedBuild`=22594 WHERE `entry`=90230; -- Wrathguard Invader
UPDATE `creature_template` SET `unit_flags2`=33589248, `VerifiedBuild`=22594 WHERE `entry`=89278; -- Demon Hunter
UPDATE `creature_template` SET `unit_flags`=33280, `unit_flags2`=33589248, `VerifiedBuild`=22594 WHERE `entry`=86969; -- Demon Hunter
UPDATE `creature_template` SET `speed_run`=1.142857, `VerifiedBuild`=22594 WHERE `entry`=91629; -- Illidari Enforcer
UPDATE `creature_template` SET `speed_walk`=0.8888, `speed_run`=1.587143, `VerifiedBuild`=22594 WHERE `entry`=93619; -- Infernal Brutalizer
UPDATE `creature_template` SET `npcflag`=2, `VerifiedBuild`=22594 WHERE `entry`=98159; -- Alynblaze
UPDATE `creature_template` SET `type_flags2`=32768, `VerifiedBuild`=22594 WHERE `entry`=107376; -- Veridis Fallon
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=103482; -- Tae'thelan Bloodwatcher
UPDATE `creature_template` SET `unit_flags`=537166656, `unit_flags2`=2049, `VerifiedBuild`=22594 WHERE `entry`=89891; -- Dragon Turtle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22594 WHERE `entry`=15358; -- Lurky
UPDATE `creature_template` SET `minlevel`=104, `maxlevel`=106, `VerifiedBuild`=22594 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=108, `faction`=35, `VerifiedBuild`=22594 WHERE `entry`=112241; -- Khadgar's Gryphon
UPDATE `creature_template` SET `minlevel`=110, `VerifiedBuild`=22594 WHERE `entry`=108853; -- Great Eagle
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=106, `speed_walk`=0.68, `speed_run`=0.6, `VerifiedBuild`=22594 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=108, `speed_run`=1.142857, `VerifiedBuild`=22594 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=108, `VerifiedBuild`=22594 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=109, `speed_run`=0.9523814, `VerifiedBuild`=22594 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=109, `VerifiedBuild`=22594 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=110, `faction`=2402, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=106152; -- Baby Elderhorn
UPDATE `creature_template` SET `BaseAttackTime`=1803, `VerifiedBuild`=22594 WHERE `entry`=106548; -- Hati
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112356; -- Battlegear of Wrath Set
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=102, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=96401; -- Valarjar Aspirant
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112420; -- Ymirjar Lord's Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112390; -- Destroyer Armor Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112361; -- Warbringer Armor Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112357; -- Conqueror's Battlegear Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112418; -- Hellscream's Conquest Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112416; -- Wrynn's Conquest Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112413; -- Valorous Siegebreaker Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112408; -- Heroes' Dreadnaught Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112400; -- Onslaught Set
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=111741; -- Fjornson Stonecarver
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=35, `npcflag`=3, `speed_run`=1, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=4196352, `VerifiedBuild`=22594 WHERE `entry`=96469; -- Odyn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=22594 WHERE `entry`=96572; -- Stormforged Valarjar
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `npcflag`=4227, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=112392; -- Quartermaster Durnolf
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=18432, `HoverHeight`=2.8, `VerifiedBuild`=22594 WHERE `entry`=96679; -- Aerylia
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=18432, `HoverHeight`=2.8, `VerifiedBuild`=22594 WHERE `entry`=93819; -- Val'kyr of Odyn
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=190, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67143680, `VerifiedBuild`=22594 WHERE `entry`=97389; -- Eye of Odyn
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=100635; -- Skyseer Ghrent
UPDATE `creature_template` SET `faction`=35, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=97128; -- Fledgling Warden Owl
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=0, `unit_flags`=768, `VerifiedBuild`=22594 WHERE `entry`=73542; -- Ashwing Moth
UPDATE `creature_template` SET `unit_flags`=33024, `VerifiedBuild`=22594 WHERE `entry`=23709; -- Dark Iron Guzzler
UPDATE `creature_template` SET `minlevel`=2, `VerifiedBuild`=22594 WHERE `entry`=24462; -- Racing Ram
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22594 WHERE `entry`=27707; -- Great Brewfest Kodo
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22594 WHERE `entry`=24463; -- Swift Racing Ram
UPDATE `creature_template` SET `npcflag`=3, `VerifiedBuild`=22594 WHERE `entry`=24510; -- Driz Tumblequick
UPDATE `creature_template` SET `unit_flags2`=71305216, `VerifiedBuild`=22594 WHERE `entry`=73919; -- Apple Bucket
UPDATE `creature_template` SET `faction`=775, `VerifiedBuild`=22594 WHERE `entry`=24484; -- Brewfest Reveler
UPDATE `creature_template` SET `npcflag`=1, `unit_flags`=32768, `VerifiedBuild`=22594 WHERE `entry`=19175; -- Orc Commoner
UPDATE `creature_template` SET `npcflag`=3, `VerifiedBuild`=22594 WHERE `entry`=27489; -- Ray'ma
UPDATE `creature_template` SET `minlevel`=50, `maxlevel`=50, `faction`=775, `npcflag`=129, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=89830; -- Brew Vendor
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=22594 WHERE `entry`=19177; -- Troll Commoner
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=0.8571429, `unit_flags`=768, `VerifiedBuild`=22594 WHERE `entry`=32841; -- Baby Blizzard Bear
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_walk`=4.8, `speed_run`=1.714286, `BaseAttackTime`=1748, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=103333; -- Beast
UPDATE `creature_template` SET `dmgschool`=0, `VerifiedBuild`=22594 WHERE `entry`=8908; -- Molten War Golem
UPDATE `creature_template` SET `unit_flags`=2181300992, `VerifiedBuild`=22594 WHERE `entry`=28206; -- [DND] L70ETC Drums
UPDATE `creature_template` SET `minlevel`=49, `faction`=735, `VerifiedBuild`=22594 WHERE `entry`=10043; -- Ribbly's Crony
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=22594 WHERE `entry`=9543; -- Ribbly Screwspigot
UPDATE `creature_template` SET `minlevel`=53, `maxlevel`=53, `npcflag`=2, `VerifiedBuild`=22594 WHERE `entry`=9503; -- Private Rocknot
UPDATE `creature_template` SET `dmgschool`=0, `VerifiedBuild`=22594 WHERE `entry`=9017; -- Lord Incendius
UPDATE `creature_template` SET `speed_walk`=0.8, `VerifiedBuild`=22594 WHERE `entry`=26834; -- Mole Machine PoV Bunny
UPDATE `creature_template` SET `minlevel`=55, `VerifiedBuild`=22594 WHERE `entry`=8904; -- Shadowforge Senator
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22594 WHERE `entry`=9024; -- Pyromancer Loregrain
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33555200, `unit_flags2`=2099200, `VerifiedBuild`=22594 WHERE `entry`=83540; -- Ring of Law
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22594 WHERE `entry`=9678; -- Shill Dinger
UPDATE `creature_template` SET `dmgschool`=0, `VerifiedBuild`=22594 WHERE `entry`=8911; -- Fireguard Destroyer
UPDATE `creature_template` SET `faction`=54, `unit_flags`=32768, `VerifiedBuild`=22594 WHERE `entry`=37937; -- Dark Iron Troublemaker
UPDATE `creature_template` SET `dmgschool`=0, `VerifiedBuild`=22594 WHERE `entry`=8909; -- Fireguard
UPDATE `creature_template` SET `dmgschool`=0, `VerifiedBuild`=22594 WHERE `entry`=8910; -- Blazing Fireguard
UPDATE `creature_template` SET `faction`=7, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=99555; -- Moonfeather Statue
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_walk`=4, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=67110912, `VehicleId`=4396, `HoverHeight`=4, `VerifiedBuild`=22594 WHERE `entry`=100659; -- Chosen of Eyir
UPDATE `creature_template` SET `speed_run`=2, `HoverHeight`=1, `VerifiedBuild`=22594 WHERE `entry`=92307; -- God-King Skovald
UPDATE `creature_template` SET `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=97851; -- Felskorn Chosen
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22594 WHERE `entry`=97963; -- Felblood Cup
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049, `VerifiedBuild`=22594 WHERE `entry`=93612; -- Dreadwake Deathguard
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `unit_flags2`=33556480, `VerifiedBuild`=22594 WHERE `entry`=94825; -- Greywatch Infiltrator
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22594 WHERE `entry`=95212; -- Forsaken Catapult
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2099201, `VerifiedBuild`=22594 WHERE `entry`=95436; -- Greywatch Cannoneer
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=22594 WHERE `entry`=109633; -- Greywatch Infiltrator
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=22594 WHERE `entry`=93860; -- Greywatch Infiltrator
UPDATE `creature_template` SET `faction`=2166, `VerifiedBuild`=22594 WHERE `entry`=94614; -- Greywatch Saboteur
UPDATE `creature_template` SET `unit_flags`=536904448, `VerifiedBuild`=22594 WHERE `entry`=107881; -- Tideskorn Beastbreaker
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=22594 WHERE `entry`=107850; -- Highlands Ettin
UPDATE `creature_template` SET `HoverHeight`=0.25, `VerifiedBuild`=22594 WHERE `entry`=111787; -- Great Sea Ray
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=110, `VerifiedBuild`=22594 WHERE `entry`=107206; -- Zoom
UPDATE `creature_template` SET `faction`=2402, `VerifiedBuild`=22594 WHERE `entry`=110984; -- Spirit Flight Form
UPDATE `creature_template` SET `HoverHeight`=1.06, `VerifiedBuild`=22594 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `VerifiedBuild`=22594 WHERE `entry`=23274; -- Stinker
UPDATE `creature_template` SET `faction`=190, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=112330; -- Pocket Fel Spreader
UPDATE `creature_template` SET `speed_walk`=1.2, `speed_run`=0.4285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags2`=2048, `VerifiedBuild`=22594 WHERE `entry`=73559; -- Water Familiar
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=102, `faction`=35, `npcflag`=0, `unit_flags`=768, `VerifiedBuild`=22594 WHERE `entry`=62130; -- Giraffe Calf
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=102, `faction`=35, `npcflag`=0, `unit_flags`=768, `VerifiedBuild`=22594 WHERE `entry`=62119; -- Robo-Chick
UPDATE `creature_template` SET `faction`=7, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=99499; -- Never Ending Treasure
UPDATE `creature_template` SET `faction`=7, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=99496; -- Never Ending Treasure
UPDATE `creature_template` SET `faction`=7, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=99495; -- Never Ending Treasure
UPDATE `creature_template` SET `faction`=7, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=99492; -- Never Ending Treasure
UPDATE `creature_template` SET `faction`=7, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=99490; -- Never Ending Treasure
UPDATE `creature_template` SET `faction`=7, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22594 WHERE `entry`=99489; -- Never Ending Treasure
