UPDATE `quest_offer_reward` SET `RewardText`='Now I can repair my dreamcatcher.  Thank you, $n.', `VerifiedBuild`=22996 WHERE `ID`=2459; -- Ferocitas the Dream Eater
UPDATE `quest_offer_reward` SET `RewardText`='My emerald dreamcatcher is of great importance to me. It is a gift only given to few. Thank you for returning it, $n.', `VerifiedBuild`=22996 WHERE `ID`=2438; -- The Emerald Dreamcatcher
UPDATE `quest_offer_reward` SET `RewardText`='Your service to the creatures of Shadowglen is worthy of reward, $n.$b$bYou confirmed my fears, however. The grellkin are still tainted by fel moss, despite Teldrassil''s blessing. Something sinister remains within the tree. I can only hope that the Gnarlpine tribe of furbolgs are free of the corruption, or we are still in grave danger.$b$bI will look into this further and contact those who might be of aid. Thank you, $c.', `VerifiedBuild`=22996 WHERE `ID`=28714; -- Fel Moss Corruption
UPDATE `quest_offer_reward` SET `RewardText`='You performed your duties well, $n.', `VerifiedBuild`=22996 WHERE `ID`=28713; -- The Balance of Nature
UPDATE `quest_offer_reward` SET `RewardText`='I knew you were more than capable of ridding the lake of that horrible beast.$B$BIt is a distressing situation; Oakenscowl was once a grand leader amongst his kind... but corruption does not discriminate between the lowly and the noble.$B$BThe size of this tumor is quite disturbing, but I must study it to learn more about the disease that upsets the timberling population.$B$BThank you, $n.', `VerifiedBuild`=22996 WHERE `ID`=2499; -- Oakenscowl
UPDATE `quest_offer_reward` SET `RewardText`='Where did you get this?  I haven''t seen a plant like this since a sojourn I made to the Swamp of Sorrows... decades ago!  It''s amazing that a specimen made its way to Teldrassil.  And it''s grown to such a size!$B$BThank you, $n.  Forgive my shortness of words, but there is a test I would like to perform on this frond...', `VerifiedBuild`=22996 WHERE `ID`=931; -- The Shimmering Frond
UPDATE `quest_offer_reward` SET `RewardText`='Well done! These tumors are the symptom of the timberling''s disease. They are filled with a poison that we must cleanse from our new land.$B$BI will dispose of these tumors. Thank you, $n.', `VerifiedBuild`=22996 WHERE `ID`=923; -- Mossy Tumors
UPDATE `quest_offer_reward` SET `RewardText`='The forest mourns for Lady Sathrah, but it was something that had to be done. I hope now that she can rest in peace.$B$BThank you, $n.', `VerifiedBuild`=22996 WHERE `ID`=2518; -- Tears of the Moon
UPDATE `quest_offer_reward` SET `RewardText`='You found this on Teldrassil?  Intriguing... this fruit is exotic.  Perhaps its seeds were brought here from far off.  Perhaps even as far as Azeroth!  And there''s something about this fruit... it seems to have reacted very strangely with the soil of Teldrassil.$B$BThank you, $n.  Now if you''ll excuse me, I must study this further...', `VerifiedBuild`=22996 WHERE `ID`=930; -- The Glowing Fruit
UPDATE `quest_offer_reward` SET `Emote1`=1, `Emote2`=2, `RewardText`='That''s a lot!  I''m afraid they''re spreading at a dangerous rate.  I hope I can solve the riddle of what is tainting them.$B$BThank you for your help, $n.  The land is a cleaner place from your efforts.', `VerifiedBuild`=22996 WHERE `ID`=919; -- Timberling Sprouts
UPDATE `quest_offer_reward` SET `RewardText`='Ah, it''s here!  I have waited for this rare earth for quite some time.  I hope it''s still fresh...$B$BThank you for bringing it to me, $n.  You are a $r who is generous with $ghis:her; time.', `VerifiedBuild`=22996 WHERE `ID`=997; -- Denalan's Earth
UPDATE `quest_offer_reward` SET `RewardText`='$n you have proven yourself a most worthy and able $c.  A $r who follows the path of honor as sure as you do is certain to find great glory in this world.$b$bUrsal''s death brings safety to our once slumbering druids, but the source of his corruption yet remains within our forests. With time, I trust that you will find this evil and cleanse it for the sake of all of our people. You are destined for great things, young $c.', `VerifiedBuild`=22996 WHERE `ID`=486; -- Ursal the Mauler
UPDATE `quest_offer_reward` SET `RewardText`='It sounds like she loved it. Thank you for bringing the book to Sister Aquinne, $n. One of these days, I intend to take her up on her invitation to visit her in Darnassus.', `VerifiedBuild`=22996 WHERE `ID`=6343; -- Return to Nyoma
UPDATE `quest_offer_reward` SET `Emote1`=1, `RewardText`='I am finally free of the control of the Gnarlpine. Thank you, $n.$B$BMy spirit may now rest peacefully forever in the Emerald Dream.$B$BPerhaps one day we may meet again, young $c. But, for now, please accept this reward as a symbol of my gratitude.', `VerifiedBuild`=22996 WHERE `ID`=2561; -- Druid of the Claw
UPDATE `quest_offer_reward` SET `RewardText`='Thank you, $n.$B$BWhat an odd trinket this is... I can sense the vile aura emanating from it; this is a very powerful enchantment.', `VerifiedBuild`=22996 WHERE `ID`=2541; -- The Sleeping Druid

DELETE FROM `quest_details` WHERE `ID` IN (7383 /*Teldrassil: The Burden of the Kaldorei*/, 922 /*Rellian Greenspyre*/, 919 /*Timberling Sprouts*/, 918 /*Timberling Seeds*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(7383, 1, 0, 0, 0, 0, 0, 0, 0, 22996), -- Teldrassil: The Burden of the Kaldorei
(922, 1, 2, 0, 0, 0, 0, 0, 0, 22996), -- Rellian Greenspyre
(919, 1, 0, 0, 0, 0, 0, 0, 0, 22996), -- Timberling Sprouts
(918, 1, 0, 0, 0, 0, 0, 0, 0, 22996); -- Timberling Seeds

UPDATE `quest_request_items` SET `CompletionText`='Did you kill Lord Melenas yet? It is vital that he be taken care of quickly and quietly, $n. His continued existence is an embarrassment to us all.', `VerifiedBuild`=22996 WHERE `ID`=932; -- Twisted Hatred
UPDATE `quest_request_items` SET `CompletionText`='Please move swiftly. I can only hope that my emerald dreamcatcher has been unharmed by the furbolg.$B$BHave you recovered it yet, $n?', `VerifiedBuild`=22996 WHERE `ID`=2438; -- The Emerald Dreamcatcher
UPDATE `quest_request_items` SET `EmoteOnComplete`=6, `VerifiedBuild`=22996 WHERE `ID`=13945; -- Resident Danger
UPDATE `quest_request_items` SET `EmoteOnComplete`=0, `CompletionText`='What do you have for me, $n?  A lovely snack I presume?', `VerifiedBuild`=22996 WHERE `ID`=489; -- Seek Redemption!
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `VerifiedBuild`=22996 WHERE `ID`=28731; -- Teldrassil: Passing Awareness
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `CompletionText`='Have you been a busy little bee, $n?  I''ve been waiting for you to bring me what I need.', `VerifiedBuild`=22996 WHERE `ID`=488; -- Zenn's Bidding
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `VerifiedBuild`=22996 WHERE `ID`=28730; -- Precious Waters
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `VerifiedBuild`=22996 WHERE `ID`=28729; -- Teldrassil: Crown of Azeroth
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `VerifiedBuild`=22996 WHERE `ID`=28729; -- Teldrassil: Crown of Azeroth
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `VerifiedBuild`=22996 WHERE `ID`=28715; -- Demonic Thieves
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `CompletionText`='Satisfy my suspicions, $n. Bring to me fel moss from the grellkin.', `VerifiedBuild`=22996 WHERE `ID`=28714; -- Fel Moss Corruption
UPDATE `quest_request_items` SET `EmoteOnComplete`=6, `VerifiedBuild`=22996 WHERE `ID`=2499; -- Oakenscowl
UPDATE `quest_request_items` SET `EmoteOnComplete`=0, `VerifiedBuild`=22996 WHERE `ID`=931; -- The Shimmering Frond
UPDATE `quest_request_items` SET `EmoteOnComplete`=3, `CompletionText`='Have you been to Wellspring Lake, $n?  Have you been hunting the timberlings there?', `VerifiedBuild`=22996 WHERE `ID`=923; -- Mossy Tumors
UPDATE `quest_request_items` SET `EmoteOnComplete`=3, `CompletionText`='Have you been to Wellspring Lake, $n?  Have you been hunting the timberlings there?', `VerifiedBuild`=22996 WHERE `ID`=923; -- Mossy Tumors
UPDATE `quest_request_items` SET `EmoteOnComplete`=0, `VerifiedBuild`=22996 WHERE `ID`=931; -- The Shimmering Frond
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `VerifiedBuild`=22996 WHERE `ID`=7383; -- Teldrassil: The Burden of the Kaldorei
UPDATE `quest_request_items` SET `EmoteOnComplete`=18, `EmoteOnCompleteDelay`=100, `VerifiedBuild`=22996 WHERE `ID`=2518; -- Tears of the Moon
UPDATE `quest_request_items` SET `EmoteOnComplete`=2, `VerifiedBuild`=22996 WHERE `ID`=922; -- Rellian Greenspyre
UPDATE `quest_request_items` SET `EmoteOnComplete`=1, `VerifiedBuild`=22996 WHERE `ID`=7383; -- Teldrassil: The Burden of the Kaldorei
UPDATE `quest_request_items` SET `EmoteOnComplete`=0, `CompletionText`='$n, you look like you have something to tell me.  Do you have news concerning the timberlings?', `VerifiedBuild`=22996 WHERE `ID`=930; -- The Glowing Fruit
UPDATE `quest_request_items` SET `EmoteOnComplete`=6, `CompletionText`='Hello, $n.  Have you found any sprouts near the waters?', `VerifiedBuild`=22996 WHERE `ID`=919; -- Timberling Sprouts
UPDATE `quest_request_items` SET `EmoteOnComplete`=6, `VerifiedBuild`=22996 WHERE `ID`=918; -- Timberling Seeds
UPDATE `quest_request_items` SET `EmoteOnComplete`=6, `CompletionText`='$n, you''re back from Darnassus?', `VerifiedBuild`=22996 WHERE `ID`=6343; -- Return to Nyoma
UPDATE `quest_request_items` SET `EmoteOnComplete`=6, `VerifiedBuild`=22996 WHERE `ID`=6342; -- An Unexpected Gift
UPDATE `quest_request_items` SET `CompletionText`='$n, the kidnapped Druids of the Talon will be forever trapped in the Emerald Dream if we cannot retrieve the Relics of Wakening from the Ban''ethil Barrow Den to the west.$b$bFor every minute we delay their fate comes one step closer to eternal doom.', `VerifiedBuild`=22996 WHERE `ID`=483; -- The Relics of Wakening

UPDATE `creature_model_info` SET `BoundingRadius`=1.581468, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=8575;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6067745, `CombatReach`=1.2, `VerifiedBuild`=22996 WHERE `DisplayID`=10034;
UPDATE `creature_model_info` SET `BoundingRadius`=0.896165, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=11339;
UPDATE `creature_model_info` SET `BoundingRadius`=0.67575, `CombatReach`=1.275, `VerifiedBuild`=22996 WHERE `DisplayID`=14318;
UPDATE `creature_model_info` SET `BoundingRadius`=0.896165, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=1013;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8892583, `VerifiedBuild`=22996 WHERE `DisplayID`=568;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7732681, `VerifiedBuild`=22996 WHERE `DisplayID`=6809;
UPDATE `creature_model_info` SET `BoundingRadius`=1.054312, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=10035;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=42946;
UPDATE `creature_model_info` SET `BoundingRadius`=0.882, `CombatReach`=0.75, `VerifiedBuild`=22996 WHERE `DisplayID`=6807;
UPDATE `creature_model_info` SET `BoundingRadius`=0.0875, `CombatReach`=0.375, `VerifiedBuild`=22996 WHERE `DisplayID`=6300;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5565, `CombatReach`=1.05, `VerifiedBuild`=22996 WHERE `DisplayID`=11454;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7322034, `VerifiedBuild`=22996 WHERE `DisplayID`=30213;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=72563;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=74257;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=22996 WHERE `DisplayID`=72558;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=72561;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=22996 WHERE `DisplayID`=67425;

UPDATE `creature_template` SET `RegenHealth`=1, `VerifiedBuild`=22996 WHERE `entry`=12429; -- Sentinel Shaya
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=188, `VerifiedBuild`=22996 WHERE `entry`=7553; -- Great Horned Owl
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22996 WHERE `entry`=6781; -- Melarith
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=22996 WHERE `entry`=47420; -- Iranis Shadebloom
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=22996 WHERE `entry`=3603; -- Cyndra Kindwhisper
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=22996 WHERE `entry`=2042; -- Nightsaber
UPDATE `creature_template` SET `minlevel`=5, `maxlevel`=5, `faction`=31, `npcflag`=1073741824, `unit_flags`=512, `VerifiedBuild`=22996 WHERE `entry`=61165; -- Fawn
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=22996 WHERE `entry`=2150; -- Zenn Foulhoof
UPDATE `creature_template` SET `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=63331; -- Laoxi
UPDATE `creature_template` SET `unit_flags`=512, `unit_flags2`=0, `VerifiedBuild`=22996 WHERE `entry`=49598; -- Gnarlpine Corruption Totem
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=89715; -- Franklin Martin
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22996 WHERE `entry`=7690; -- Striped Nightsaber
UPDATE `creature_template` SET `maxlevel`=3, `VerifiedBuild`=22996 WHERE `entry`=1989; -- Grellkin
UPDATE `creature_template` SET `RegenHealth`=1, `VerifiedBuild`=22996 WHERE `entry`=44617; -- Wounded Sentinel
UPDATE `creature_template` SET `npcflag`=2, `VerifiedBuild`=22996 WHERE `entry`=2079; -- Ilthalaine
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22996 WHERE `entry`=52028; -- Talric Forthright
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=22996 WHERE `entry`=7208; -- Noarm
UPDATE `creature_template` SET `npcflag`=1, `unit_class`=4, `VerifiedBuild`=22996 WHERE `entry`=51998; -- Arthur Huwe
UPDATE `creature_template` SET `unit_class`=4, `VerifiedBuild`=22996 WHERE `entry`=13283; -- Lord Tony Romano
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `faction`=7, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=131072, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=114832; -- PvP Training Dummy
UPDATE `creature_template` SET `minlevel`=2, `VerifiedBuild`=22996 WHERE `entry`=14561; -- Swift Brown Steed
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22996 WHERE `entry`=14559; -- Swift Palomino
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22996 WHERE `entry`=14560; -- Swift White Steed
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22996 WHERE `entry`=4269; -- Chestnut Mare
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22996 WHERE `entry`=308; -- Black Stallion
UPDATE `creature_template` SET `minlevel`=2, `VerifiedBuild`=22996 WHERE `entry`=284; -- Brown Horse
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1078, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=112912; -- Martin Ocejo
UPDATE `creature_template` SET `npcflag`=128, `VerifiedBuild`=22996 WHERE `entry`=2795; -- Lenny "Fingers" McCoy
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `VerifiedBuild`=22996 WHERE `entry`=87501; -- Paulie
UPDATE `creature_template` SET `maxlevel`=23, `VerifiedBuild`=22996 WHERE `entry`=42421; -- Stormwind Fisherman
UPDATE `creature_template` SET `minlevel`=1 WHERE `entry`=42339; -- Canal Crab
UPDATE `creature_template` SET `npcflag`=81 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=92464; -- Kuhuine Tenderstride
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `npcflag`=80 WHERE `entry`=92459; -- Dorothy Egan
UPDATE `creature_template` SET `minlevel`=12, `VerifiedBuild`=22996 WHERE `entry`=113183; -- Skidmark
UPDATE `creature_template` SET `unit_flags`=33536, `VerifiedBuild`=22996 WHERE `entry`=113335; -- Trashy Jr.
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=2757, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106217; -- Jared
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2757, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=108018; -- Archivist Melinda
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2757, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33600, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106228; -- Grand Warlock Alythess
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2757, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33600, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106227; -- Lady Sacrolash
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=2757, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=105928; -- Lulubelle Fizzlebang
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=2757, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106068; -- Kira Iresoul
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2757, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=105926; -- Shinfel Blightsworn
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2757, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=105951; -- Ritssyn Flamescowl
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=98, `faction`=2263, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=113393; -- Black Harvest Summoner
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2757, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=105923; -- Zinnin Smythe
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22996 WHERE `entry`=116611; -- Black Harvest Armor
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2757, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=105922; -- Jubeka Shadowbreaker
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=98, `faction`=2263, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=113371; -- Demonia Pickerin
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=113263; -- Black Harvest Invoker
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=22996 WHERE `entry`=34522; -- Corrupted Servant
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=22996 WHERE `entry`=1992; -- Tarindrella
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=22996 WHERE `entry`=34530; -- Ancient Teldrassil Protector
UPDATE `creature_template` SET `minlevel`=8, `spell1`=0, `VerifiedBuild`=22996 WHERE `entry`=2025; -- Timberling Bark Ripper
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=22996 WHERE `entry`=6128; -- Vorlus Vilehoof
UPDATE `creature_template` SET `minlevel`=8, `VerifiedBuild`=22996 WHERE `entry`=15624; -- Forest Wisp
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=22996 WHERE `entry`=6909; -- Sethir the Ancient
UPDATE `creature_template` SET `maxlevel`=10, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=22996 WHERE `entry`=2001; -- Giant Webwood Spider
UPDATE `creature_template` SET `npcflag`=17, `VerifiedBuild`=22996 WHERE `entry`=3606; -- Alanna Raveneye
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22996 WHERE `entry`=5782; -- Crildor
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22996 WHERE `entry`=14380; -- Huntress Leafrunner
UPDATE `creature_template` SET `npcflag`=17, `VerifiedBuild`=22996 WHERE `entry`=52642; -- Foreman Pernic
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=22996 WHERE `entry`=34989; -- Rissa Shadeleaf
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=22996 WHERE `entry`=34988; -- Landuen Moonclaw
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22996 WHERE `entry`=47584; -- Aladrel Whitespire
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=22996 WHERE `entry`=4212; -- Telonis
UPDATE `creature_template` SET `speed_run`=1.385714, `VerifiedBuild`=22996 WHERE `entry`=4262; -- Darnassus Sentinel
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22996 WHERE `entry`=10604; -- Huntress Nhemai
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22996 WHERE `entry`=10606; -- Huntress Yaeliura

DELETE FROM `gameobject_template` WHERE `entry` IN (252800 /*Artifact Research Notes*/, 246286 /*Felbound Tome*/, 257844 /*Man'ari Appearance*/, 257861 /*Man'ari Knife Appearance*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(252800, 45, 15781, 'Artifact Research Notes', '', '', '', 1, 190, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22996), -- Artifact Research Notes
(246286, 5, 26998, 'Felbound Tome', '', '', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22996), -- Felbound Tome
(257844, 5, 37327, 'Man''ari Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22996), -- Man'ari Appearance
(257861, 5, 37347, 'Man''ari Knife Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22996); -- Man'ari Knife Appearance

UPDATE `gameobject_template` SET `type`=48, `Data1`=0, `Data3`=0, `Data8`=0, `VerifiedBuild`=22996 WHERE `entry`=207321; -- Hero's Call Board
