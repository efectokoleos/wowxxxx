DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=252437 AND `Idx`=0);
DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=243690 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(252437, 0, 139401, 23420), -- The Fate of Aegwynn
(243690, 0, 128346, 23420); -- Bejeweled Egg
