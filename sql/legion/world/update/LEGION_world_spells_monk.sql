DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_legion_monk_black_ox_brew',
'spell_legion_monk_blackout_combo',
'spell_legion_monk_breath_of_fire',
'spell_legion_monk_celestial_breath',
'spell_legion_monk_celestial_fortune',
'spell_legion_monk_chi_ji_heal_searcher',
'spell_legion_monk_chi_orbit',
'spell_legion_monk_crackling_jade_lightning_aura',
'spell_legion_monk_dampen_harm',
'spell_legion_monk_dark_side_of_the_moon',
'spell_legion_monk_diffuse_magic',
'spell_legion_monk_disable',
'spell_legion_monk_elusive_brawler',
'spell_legion_monk_enveloping_mist',
'spell_legion_monk_essence_font',
'spell_legion_monk_essence_font_heal',
'spell_legion_monk_fists_of_fury',
'spell_legion_monk_flying_serpent_kick_second_cast',
'spell_legion_monk_fortifying_brew',
'spell_legion_monk_fortification',
'spell_legion_monk_gale_burst',
'spell_legion_monk_gust_of_mists_mastery',
'spell_legion_monk_healing_elixir',
'spell_legion_monk_ironskin_brew',
'spell_legion_monk_keg_smash',
'spell_legion_monk_life_cocoon',
'spell_legion_monk_lifecycles',
'spell_legion_monk_provoke',
'spell_legion_monk_purifying_brew',
'spell_legion_monk_remove_snare',
'spell_legion_monk_remove_snare',
'spell_legion_monk_renewing_mist',
'spell_legion_monk_renewing_mist_hot',
'spell_legion_monk_renewing_mist_jump',
'spell_legion_monk_rising_sun_kick',
'spell_legion_monk_rising_thunder',
'spell_legion_monk_roll',
'spell_legion_monk_rushing_jade_wind_damage',
'spell_legion_monk_soothing_mist',
'spell_legion_monk_soothing_mist_passive',
'spell_legion_monk_special_delivery',
'spell_legion_monk_special_delivery_aura',
'spell_legion_monk_spinning_crane_kick',
'spell_legion_monk_spirit_of_the_crane_passive',
'spell_legion_monk_stagger_base',
'spell_legion_monk_stagger_dot',
'spell_legion_monk_teachings_of_the_monastery_buff',
'spell_legion_monk_teachings_of_the_monastery_passive',
'spell_legion_monk_tiger_palm',
'spell_legion_monk_tornado_kicks',
'spell_legion_monk_touch_of_death',
'spell_legion_monk_touch_of_karma',
'spell_legion_monk_touch_of_karma_buff',
'spell_legion_monk_transcendence',
'spell_legion_monk_transcendence_transfer',
'spell_legion_monk_vivify',
'spell_legion_monk_way_of_the_crane',
'spell_legion_monk_whirling_dragon_punch',
'spell_legion_monk_zen_pilgrimage',
'spell_legion_monk_zen_pilgrimage_return',
'spell_legion_monk_zen_pulse',
'spell_legion_monk_chi_wave',
'spell_legion_monk_chi_wave_heal',
'spell_legion_monk_chi_wave_markers',
'spell_legion_monk_chi_wave_markers',
'spell_legion_monk_chi_wave_target_select',
'spell_legion_monk_afterlife',
'spell_legion_monk_effuse_honor_talent',
'spell_legion_monk_chi_torpedo',
'spell_legion_monk_guard',
'spell_legion_monk_gift_of_the_ox',
'spell_legion_monk_gift_of_the_ox_healing_sphere_expired',
'spell_legion_monk_sheiluns_gift',
'spell_legion_monk_sheiluns_gift_periodic',
'spell_legion_monk_revival',
'spell_legion_monk_eye_of_the_tiger'
);

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES 
(193884, 'spell_legion_monk_soothing_mist_passive'),
(116670, 'spell_legion_monk_vivify'),
(116849, 'spell_legion_monk_life_cocoon'),
(124682, 'spell_legion_monk_enveloping_mist'),
(227345, 'spell_legion_monk_enveloping_mist'),
(115175, 'spell_legion_monk_soothing_mist'),
(209525, 'spell_legion_monk_soothing_mist'),
(191840, 'spell_legion_monk_essence_font_heal'),
(117907, 'spell_legion_monk_gust_of_mists_mastery'),
(119611, 'spell_legion_monk_renewing_mist_hot'),
(115151, 'spell_legion_monk_renewing_mist'),
(119607, 'spell_legion_monk_renewing_mist_jump'),
(124081, 'spell_legion_monk_zen_pulse'),
(122280, 'spell_legion_monk_healing_elixir'),
(109132, 'spell_legion_monk_roll'),
(148187, 'spell_legion_monk_rushing_jade_wind_damage'),
(101643, 'spell_legion_monk_transcendence'),
(119996, 'spell_legion_monk_transcendence_transfer'),
(117959, 'spell_legion_monk_crackling_jade_lightning_aura'),
(115546, 'spell_legion_monk_provoke'),
(159535, 'spell_legion_monk_remove_snare'),
(116841, 'spell_legion_monk_remove_snare'),
(232877, 'spell_legion_monk_remove_snare'),
(122783, 'spell_legion_monk_diffuse_magic'),
(116645, 'spell_legion_monk_teachings_of_the_monastery_passive'),
(202090, 'spell_legion_monk_teachings_of_the_monastery_buff'),
(210804, 'spell_legion_monk_rising_thunder'),
(210802, 'spell_legion_monk_spirit_of_the_crane_passive'),
(197915, 'spell_legion_monk_lifecycles'),
(197916, 'spell_legion_monk_lifecycles'),
(197919, 'spell_legion_monk_lifecycles'),
(216113, 'spell_legion_monk_way_of_the_crane'),
(116095, 'spell_legion_monk_disable'),
(100780, 'spell_legion_monk_tiger_palm'),
(115080, 'spell_legion_monk_touch_of_death'),
(122470, 'spell_legion_monk_touch_of_karma'),
(125174, 'spell_legion_monk_touch_of_karma_buff'),
(196736, 'spell_legion_monk_blackout_combo'),
(152175, 'spell_legion_monk_whirling_dragon_punch'),
(113656, 'spell_legion_monk_fists_of_fury'),
(122278, 'spell_legion_monk_dampen_harm'),
(115057, 'spell_legion_monk_flying_serpent_kick_second_cast'),
(115399, 'spell_legion_monk_black_ox_brew'),
(216519, 'spell_legion_monk_celestial_fortune'),
(115203, 'spell_legion_monk_fortifying_brew'),
(213340, 'spell_legion_monk_fortification'),
(115181, 'spell_legion_monk_breath_of_fire'),
(115069, 'spell_legion_monk_stagger_base'),
(124255, 'spell_legion_monk_stagger_dot'),
(119582, 'spell_legion_monk_purifying_brew'),
(115308, 'spell_legion_monk_ironskin_brew'),
(196734, 'spell_legion_monk_special_delivery'),
(196730, 'spell_legion_monk_special_delivery_aura'),
(214372, 'spell_legion_monk_special_delivery_aura'),
(107270, 'spell_legion_monk_spinning_crane_kick'),
(121253, 'spell_legion_monk_keg_smash'),
(195630, 'spell_legion_monk_elusive_brawler'),
(199640, 'spell_legion_monk_celestial_breath'),
(185099, 'spell_legion_monk_rising_sun_kick'),
(191837, 'spell_legion_monk_essence_font'),
(198766, 'spell_legion_monk_chi_ji_heal_searcher'),
(196743, 'spell_legion_monk_chi_orbit'),
(196082, 'spell_legion_monk_tornado_kicks'),
(195403, 'spell_legion_monk_gale_burst'),
(126892, 'spell_legion_monk_zen_pilgrimage'),
(126895, 'spell_legion_monk_zen_pilgrimage_return'),
(115098, 'spell_legion_monk_chi_wave'),
(132463, 'spell_legion_monk_chi_wave_heal'),
(132464, 'spell_legion_monk_chi_wave_markers'),
(132467, 'spell_legion_monk_chi_wave_markers'),
(132466, 'spell_legion_monk_chi_wave_target_select'),
(116092, 'spell_legion_monk_afterlife'),
(227344, 'spell_legion_monk_effuse_honor_talent'),
(115008, 'spell_legion_monk_chi_torpedo'),
(202162, 'spell_legion_monk_guard'),
(124502, 'spell_legion_monk_gift_of_the_ox'),
(239307, 'spell_legion_monk_dark_side_of_the_moon'),
(213062, 'spell_legion_monk_dark_side_of_the_moon'),
(227688, 'spell_legion_monk_dark_side_of_the_moon'),
(227689, 'spell_legion_monk_dark_side_of_the_moon'),
(227690, 'spell_legion_monk_dark_side_of_the_moon'),
(227691, 'spell_legion_monk_dark_side_of_the_moon'),
(239306, 'spell_legion_monk_dark_side_of_the_moon'),
(178173, 'spell_legion_monk_gift_of_the_ox_healing_sphere_expired'),
(214417, 'spell_legion_monk_gift_of_the_ox_healing_sphere_expired'),
(205406, 'spell_legion_monk_sheiluns_gift'),
(214483, 'spell_legion_monk_sheiluns_gift_periodic'),
(115310, 'spell_legion_monk_revival'),
(196607, 'spell_legion_monk_eye_of_the_tiger');

DELETE FROM `spell_proc` WHERE `SpellId` IN(116095, 116768, 124502, 195300, 195399, 195630, 196082, 196607, 196730, 197206, 199640, 213161, 214372, 216913, 210804);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`, `Chance`) VALUES
(116095,        0x0,      53,     0x0,              0x0,           0x0,     0x0,      0x0,          0x0, 0x0,   0),
(116768,        0x0,      53,     0x10,             0x0,           0x8,     0x0,      0x0,          0x4, 0x0,   0),
(124502,        0x0,      53,     0x0,              0x0,           0x0,     0x0,      0x100000,     0x0, 0x3, 100),
(195300,        0x0,      53,     0x0,              0x0,           0x0,     0x0,      0x200000,     0x0, 0x0,   0),
(195399,        0x0,      53,     0x0,              0x100,         0x0,     0x0,      0x0,          0x2, 0x0,   0),
(195630,        0x0,      53,     0x0,              0x0,           0x0,     0x0,      0x0,          0x0, 0x10,  0),
(196082,        0x0,      53,     0x0,              0x80,          0x0,     0x0,      0x0,          0x2, 0x0,   0),
(196607,        0x1,      53,     0x400,            0x0,           0x0,     0x0,      0x0,          0x2, 0x3,   0),
(196730,        0x0,      53,     0x0,              0x0,           0x0,     0x0,      0x0,          0x4, 0x0,   0),
(197206,        0x0,      53,     0x4000000,        0x0,           0x0,     0x0,      0x0,          0x4, 0x0,   0),
(199640,        0x0,      53,     0x80000000,       0x0,           0x0,     0x0,      0x0,          0x2, 0x0,   0),
(213161,        0x0,      53,     0x0,              0x300000,      0x0,     0x8000,   0x0,          0x2, 0x0,   0),
(214372,        0x0,      53,     0x0,              0x0,           0x0,     0x0,      0x0,          0x0, 0x0,   0),
(216913,        0x0,      53,     0x2000000,        0x0,           0x0,     0x0,      0x0,          0x2, 0x0,   0),
(210804,        0x0,      53,     0x0,              0x00000080,    0x0,     0x0,      0x0,          0x2, 0x0,   0);

-- Add unit flags to transcendence spirit
-- Sniff Data: [2] UNIT_FIELD_FLAGS: 33587208/9.440698E-38
UPDATE `creature_template` SET `unit_flags`= `unit_flags` | 33587208 WHERE  `entry`=54569;

UPDATE `creature_template` SET `BaseAttackTime` = 700, `RangeAttackTime` = 700, `spell1` = 196728, `spell2` = 196727, `spell3` = 227291 WHERE  `entry`=73967;

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (-199888, 199641, -233766, -116680, 202370) AND `type`=0;
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` = 233765 AND `type`=2;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(-199888, 199894, 0, 'Monk - The Mists of Sheilun Removal triggers AOE Heal'),
(199641, 214411, 0, 'Monk - Celestial Breath Triggers Celestial Breath Debuff'),
(233765, 233954, 2, 'Monk - Control the mists Talent Aura also applies instant cast aura'),
(-233766, 233954, 0, 'Monk - Control the mists Debuff Removal triggers instant cast aura'),
(-116680, -197218, 0, 'Monk - Thunder Focus Tea removal also removes essence font buff'),
(202370, 227996, 0, 'Monk - Mighty Ox Kick cast leap back spell');

-- Chi Ji Pet Script
UPDATE `creature_template` SET `ScriptName`='npc_legion_monk_pet_chi_ji' WHERE  `entry`=100868;
-- Niuzao Pet Script
UPDATE `creature_template` SET `ScriptName`='npc_legion_monk_pet_niuzao' WHERE  `entry`=73967;
-- Xuen Pet Script
UPDATE `creature_template` SET `ScriptName`='npc_legion_monk_pet_xuen' WHERE  `entry`=63508;
-- Black Ox Statue
UPDATE `creature_template` SET InhabitType = InhabitType | 8, `ScriptName`='npc_legion_monk_pet_black_ox_statue' WHERE  `entry`=61146;

DELETE FROM `areatrigger_template` WHERE `Id` IN (3983, 5300, 5302, 10191, 10003, 10191, 105000, 10003);
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `AIName`, `ScriptName`, `VerifiedBuild`) VALUES
(3983, 0, 4, 8, 8, 0, 0, 0, 0, '', 'areatrigger_legion_monk_ring_of_peace', 22996),
(105000, 0, 0x4, 10, 10, 0, 0, 0, 0, '', 'areatrigger_legion_monk_windwalking', 0), -- custom - we dont have a sniff and TC dont have this AT on TDB
(10003, 0, 1028, 1.5, 1.5, 0, 0, 0, 0, '', 'areatrigger_legion_monk_chi_orbit', 22522);

UPDATE `areatrigger_template` SET `ScriptName` = 'areatrigger_legion_monk_gift_of_the_ox' WHERE `Id` IN (3282, 11692);
UPDATE `areatrigger_template` SET `ScriptName` = 'areatrigger_legion_monk_song_of_chi_ji' WHERE `Id` = 10191;
UPDATE `areatrigger_template` SET `ScriptName` = 'areatrigger_legion_monk_sheilungs_gift' WHERE `Id` = 11721;
-- UPDATE `areatrigger_template` SET `ScriptName` = 'areatrigger_legion_monk_spirit_tether' WHERE `Id` = 

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (718, 1315, 1316, 5484, 5280, 2763, 383, 2442, 373, 7240, 7267);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `MoveCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `TimeToTargetExtraScale`, `VerifiedBuild`) VALUES
(718, 3983, 0, 0, 0, 0, 0, 0, 0, 0, 22996), -- 116844
(1315, 5300, 0, 0, 0, 392, 0, 1000, 0, 0, 22522), -- 123986
(1316, 5302, 0, 0, 0, 393, 0, 1000, 0, 0, 22522), -- 123986
(5280, 10003, 0, 0, 0, 0, 0, 5000, 0, 0, 22522), -- 196744, 196745, 196746, 196747
(5484, 10191, 0, 0, 0, 1656, 0, 5000, 0, 0, 23420), -- 198898 
(2763, 105000, 0, 0, 0, 0, 0, 0, 0, 0, 0), -- 157411 - custom - we dont have a sniff and TC dont have this AT on TDB
(383, 3319, 0, 0, 0, 0, 0, 0, 30000, 0, 22522),
(2442, 7020, 0, 0, 0, 0, 0, 0, 120000, 0, 22522),
(373, 3282, 0, 0, 0, 0, 0, 0, 30000, 0, 23420),
(7240, 11692, 0, 0, 0, 0, 0, 0, 30000, 0, 23420),
(7267, 11721, 0, 0, 0, 0, 0, 0, 60000, 0, 23420);

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (3319, 5300, 5302, 7020, 10191);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES
(3319, 0, 125355, 5),
(5300, 0, 130654, 1),
(5302, 0, 148135, 2),
(7020, 0, 163272, 5),
(10191, 0, 198909, 2);

DELETE FROM `spell_areatrigger_splines` WHERE `SpellMiscId` IN (1315, 1316, 5484);
INSERT INTO `spell_areatrigger_splines` (`SpellMiscId`, `Idx`, `X`, `Y`, `Z`, `VerifiedBuild`) VALUES
(1315, 22, 40.0, 0.0, 0.0, 22522),
(1315, 21, 40.0, 0.0, 0.0, 22522),
(1315, 20, 38.0, 0.0, 0.0, 22522),
(1315, 19, 36.0, 0.0, 0.0, 22522),
(1315, 18, 34.0, 0.0, 0.0, 22522),
(1315, 17, 32.0, 0.0, 0.0, 22522),
(1315, 16, 30.0, 0.0, 0.0, 22522),
(1315, 15, 28.0, 0.0, 0.0, 22522),
(1315, 14, 26.0, 0.0, 0.0, 22522),
(1315, 13, 24.0, 0.0, 0.0, 22522),
(1315, 12, 22.0, 0.0, 0.0, 22522),
(1315, 11, 20.0, 0.0, 0.0, 22522),
(1315, 10, 18.0, 0.0, 0.0, 22522),
(1315,  9, 16.0, 0.0, 0.0, 22522),
(1315,  8, 14.0, 0.0, 0.0, 22522),
(1315,  7, 12.0, 0.0, 0.0, 22522),
(1315,  6, 10.0, 0.0, 0.0, 22522),
(1315,  5,  8.0, 0.0, 0.0, 22522),
(1315,  4,  6.0, 0.0, 0.0, 22522),
(1315,  3,  4.0, 0.0, 0.0, 22522),
(1315,  2,  2.0, 0.0, 0.0, 22522),
(1315,  1,  0.0, 0.0, 0.0, 22522),
(1315,  0,  0.0, 0.0, 0.0, 22522),
(1316, 22, 40.0, 0.0, 0.0, 22522),
(1316, 21, 40.0, 0.0, 0.0, 22522),
(1316, 20, 38.0, 0.0, 0.0, 22522),
(1316, 19, 36.0, 0.0, 0.0, 22522),
(1316, 18, 34.0, 0.0, 0.0, 22522),
(1316, 17, 32.0, 0.0, 0.0, 22522),
(1316, 16, 30.0, 0.0, 0.0, 22522),
(1316, 15, 28.0, 0.0, 0.0, 22522),
(1316, 14, 26.0, 0.0, 0.0, 22522),
(1316, 13, 24.0, 0.0, 0.0, 22522),
(1316, 12, 22.0, 0.0, 0.0, 22522),
(1316, 11, 20.0, 0.0, 0.0, 22522),
(1316, 10, 18.0, 0.0, 0.0, 22522),
(1316,  9, 16.0, 0.0, 0.0, 22522),
(1316,  8, 14.0, 0.0, 0.0, 22522),
(1316,  7, 12.0, 0.0, 0.0, 22522),
(1316,  6, 10.0, 0.0, 0.0, 22522),
(1316,  5,  8.0, 0.0, 0.0, 22522),
(1316,  4,  6.0, 0.0, 0.0, 22522),
(1316,  3,  4.0, 0.0, 0.0, 22522),
(1316,  2,  2.0, 0.0, 0.0, 22522),
(1316,  1,  0.0, 0.0, 0.0, 22522),
(1316,  0,  0.0, 0.0, 0.0, 22522),

(5484, 16, 40.08189, 0.0001946589, 0.50, 0), -- not sniff verified
(5484, 15, 40.08189, 0.0001946589, 0.50, 0),
(5484, 14, 37.08167, 0.0001410497, 0.50, 0),
(5484, 13, 34.0818, 0.0001125781, 0.50, 0),
(5484, 12, 31.08175, 0.000243713, 0.50, 0),
(5484, 11, 28.08188, 0.0002152414, 0.50, 0),
(5484, 10, 25.08166, 0.0001616322, 0.50, 0),
(5484, 9, 22.08179, 0.0001331606, 0.50, 0),
(5484, 8, 19.08174, 0.0002642955, 0.50, 0),
(5484, 7, 16.08187, 0.0002358239, 0.50, 0),
(5484, 6, 13.08165, 0.0001822146, 0.50, 0),
(5484, 5, 10.08178, 0.0001537431, 0.50, 0),
(5484, 4, 7.081728, 0.000284878, 0.50, 0),
(5484, 3, 4.081857, 0.0002564064, 0.50, 0),
(5484, 2, 1.081643, 0.0002027971, 0.50, 0),
(5484, 1, -1.918228, 0.0001743255, 0.50, 0),
(5484, 0, -1.918228, 0.0001743255, 0.50, 0);

DELETE FROM `spell_areatrigger_scale` WHERE `SpellMiscId` IN (5280, 5484);
INSERT INTO `spell_areatrigger_scale` (`SpellMiscId`, `ScaleCurveOverride0`, `ScaleCurveOverride1`, `ScaleCurveOverride2`, `ScaleCurveOverride3`, `ScaleCurveOverride4`, `ScaleCurveOverride5`, `ScaleCurveOverride6`, `ScaleCurveExtra0`, `ScaleCurveExtra1`, `ScaleCurveExtra2`, `ScaleCurveExtra3`, `ScaleCurveExtra4`, `ScaleCurveExtra5`, `ScaleCurveExtra6`, `VerifiedBuild`) VALUES
(5280, 0, 135, 0.01, 1, 1, 268435456, 1, 0, 0, 0, 0, 0, 1, 1, 23420),
(5484, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 23420);
