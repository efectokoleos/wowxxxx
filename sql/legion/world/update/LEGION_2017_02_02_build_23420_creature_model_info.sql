DELETE FROM `creature_model_info` WHERE `DisplayID` IN (75463, 75432, 74532, 74331, 74633, 73606, 73948, 74285, 74713, 74442, 73718, 73698, 73579, 74419, 74595, 74594, 74268, 73762, 73761, 73760, 73758, 73757, 73735, 73513, 73952, 73988, 73563);
INSERT INTO `creature_model_info` (`DisplayID`, `BoundingRadius`, `CombatReach`, `DisplayID_Other_Gender`, `VerifiedBuild`) VALUES
(75463, 1.25, 2.5, 0, 23420),
(75432, 2.535891, 6.25, 0, 23420),
(74532, 0.525, 1.5, 0, 23420),
(74331, 0.5355, 1.75, 0, 23420),
(74633, 0.465, 3, 0, 23420),
(73606, 0.5, 1, 0, 23420),
(73948, 0.5, 1.5, 0, 23420),
(74285, 1.41225, 3.85, 0, 23420),
(74713, 1, 4, 0, 23420),
(74442, 2.0175, 2.25, 0, 23420),
(73718, 0.611112, 5, 0, 23420),
(73698, 1.75, 2.625, 0, 23420),
(73579, 0.306, 1.5, 0, 23420),
(74419, 0.459, 2.25, 0, 23420),
(74595, 0.347, 1.5, 0, 23420),
(74594, 1.956473, 4.5, 0, 23420),
(74268, 1.25, 7.5, 0, 23420),
(73762, 0.3, 1.5, 0, 23420),
(73761, 0.3, 1.5, 0, 23420),
(73760, 0.3, 1.5, 0, 23420),
(73758, 0.5205, 2.25, 0, 23420),
(73757, 0.5205, 2.25, 0, 23420),
(73735, 0.5205, 2.25, 0, 23420),
(73513, 0.000235, 0.001, 0, 23420),
(73952, 0.383, 1.5, 0, 23420),
(73988, 0.93, 3.3, 0, 23420),
(73563, 0.306, 1.5, 0, 23420);

UPDATE `creature_model_info` SET `BoundingRadius`=2.834045, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73915;
UPDATE `creature_model_info` SET `BoundingRadius`=4.4535, `CombatReach`=1.153846, `VerifiedBuild`=23420 WHERE `DisplayID`=73918;
UPDATE `creature_model_info` SET `BoundingRadius`=1.619454, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=70057;
UPDATE `creature_model_info` SET `BoundingRadius`=1.125, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=73992;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8097272, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73922;
UPDATE `creature_model_info` SET `BoundingRadius`=0.34, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=33658;
UPDATE `creature_model_info` SET `BoundingRadius`=0.525, `VerifiedBuild`=23420 WHERE `DisplayID`=9134;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=4974;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4513886, `CombatReach`=1.95, `VerifiedBuild`=23420 WHERE `DisplayID`=67878;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4674873, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=64588;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=4764;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=72712;
UPDATE `creature_model_info` SET `BoundingRadius`=4.482424, `CombatReach`=8, `VerifiedBuild`=23420 WHERE `DisplayID`=68567;
UPDATE `creature_model_info` SET `BoundingRadius`=3.361818, `CombatReach`=6, `VerifiedBuild`=23420 WHERE `DisplayID`=72144;
UPDATE `creature_model_info` SET `BoundingRadius`=1.930383, `CombatReach`=4, `VerifiedBuild`=23420 WHERE `DisplayID`=67384;
UPDATE `creature_model_info` SET `BoundingRadius`=6.088943, `CombatReach`=12.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68970;
UPDATE `creature_model_info` SET `BoundingRadius`=4.869803, `CombatReach`=6, `VerifiedBuild`=23420 WHERE `DisplayID`=72143;
UPDATE `creature_model_info` SET `BoundingRadius`=2.028713, `CombatReach`=5, `VerifiedBuild`=23420 WHERE `DisplayID`=71794;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=64487;
UPDATE `creature_model_info` SET `BoundingRadius`=10.40062, `CombatReach`=11, `VerifiedBuild`=23420 WHERE `DisplayID`=67087;
UPDATE `creature_model_info` SET `BoundingRadius`=3.120185, `CombatReach`=3.3, `VerifiedBuild`=23420 WHERE `DisplayID`=68101;
UPDATE `creature_model_info` SET `BoundingRadius`=4.160247, `CombatReach`=4.4, `VerifiedBuild`=23420 WHERE `DisplayID`=68891;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2049979, `CombatReach`=1.75, `VerifiedBuild`=23420 WHERE `DisplayID`=68261;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1447804, `CombatReach`=1.75, `VerifiedBuild`=23420 WHERE `DisplayID`=68260;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4807671, `CombatReach`=14, `VerifiedBuild`=23420 WHERE `DisplayID`=67104;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1295585, `CombatReach`=1.75, `VerifiedBuild`=23420 WHERE `DisplayID`=68262;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4807673, `CombatReach`=14, `VerifiedBuild`=23420 WHERE `DisplayID`=67105;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9356054, `CombatReach`=14, `VerifiedBuild`=23420 WHERE `DisplayID`=67103;
UPDATE `creature_model_info` SET `BoundingRadius`=2.080124, `CombatReach`=2.2, `VerifiedBuild`=23420 WHERE `DisplayID`=68089;
UPDATE `creature_model_info` SET `BoundingRadius`=0.375, `CombatReach`=1.25, `VerifiedBuild`=23420 WHERE `DisplayID`=71176;
UPDATE `creature_model_info` SET `BoundingRadius`=2.874561, `VerifiedBuild`=23420 WHERE `DisplayID`=65052;
UPDATE `creature_model_info` SET `BoundingRadius`=1.031494, `VerifiedBuild`=23420 WHERE `DisplayID`=64629;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=70082;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=64536;
UPDATE `creature_model_info` SET `BoundingRadius`=4.4535, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73917;
UPDATE `creature_model_info` SET `BoundingRadius`=2.25, `CombatReach`=7.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73997;
UPDATE `creature_model_info` SET `BoundingRadius`=2.834045, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73916;
UPDATE `creature_model_info` SET `BoundingRadius`=1.619454, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=70058;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8097272, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73924;
UPDATE `creature_model_info` SET `BoundingRadius`=1.125, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=73993;
UPDATE `creature_model_info` SET `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=664;
UPDATE `creature_model_info` SET `BoundingRadius`=1.847593, `VerifiedBuild`=23420 WHERE `DisplayID`=32546;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2495186, `CombatReach`=0.3, `VerifiedBuild`=23420 WHERE `DisplayID`=70663;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=12080;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=65618;
UPDATE `creature_model_info` SET `BoundingRadius`=1.159709, `VerifiedBuild`=23420 WHERE `DisplayID`=10926;
UPDATE `creature_model_info` SET `BoundingRadius`=1.22464, `CombatReach`=2.15625, `VerifiedBuild`=23420 WHERE `DisplayID`=73542;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8114851, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=72850;
UPDATE `creature_model_info` SET `BoundingRadius`=1.028736, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=61928;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9797121, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=73540;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9797121, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=73537;
UPDATE `creature_model_info` SET `BoundingRadius`=1.088063, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=73539;
UPDATE `creature_model_info` SET `BoundingRadius`=1.126669, `CombatReach`=1.98375, `VerifiedBuild`=23420 WHERE `DisplayID`=73541;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5364968, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72827;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7571489, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=73934;
UPDATE `creature_model_info` SET `BoundingRadius`=2.874561, `VerifiedBuild`=23420 WHERE `DisplayID`=67022;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7316077, `CombatReach`=2.15625, `VerifiedBuild`=23420 WHERE `DisplayID`=73543;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5088413, `CombatReach`=1.15, `VerifiedBuild`=23420 WHERE `DisplayID`=74659;
UPDATE `creature_model_info` SET `BoundingRadius`=0.186, `CombatReach`=0.6, `VerifiedBuild`=23420 WHERE `DisplayID`=34171;
UPDATE `creature_model_info` SET `BoundingRadius`=0.186, `CombatReach`=0.6, `VerifiedBuild`=23420 WHERE `DisplayID`=34170;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4564604, `CombatReach`=1.125, `VerifiedBuild`=23420 WHERE `DisplayID`=59044;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47722;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47721;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47723;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5205, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=74601;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=39163;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=39333;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46777;
UPDATE `creature_model_info` SET `BoundingRadius`=0.775, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=48117;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=46262;
UPDATE `creature_model_info` SET `BoundingRadius`=0.62, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=28115;
UPDATE `creature_model_info` SET `BoundingRadius`=0.22875, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=46261;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4596, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=46263;
UPDATE `creature_model_info` SET `BoundingRadius`=0.229167, `CombatReach`=0.75, `VerifiedBuild`=23420 WHERE `DisplayID`=46260;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4513886, `CombatReach`=1.95, `VerifiedBuild`=23420 WHERE `DisplayID`=41568;
UPDATE `creature_model_info` SET `BoundingRadius`=2.222222, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=52049;
UPDATE `creature_model_info` SET `BoundingRadius`=2.222222, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=54490;
UPDATE `creature_model_info` SET `BoundingRadius`=2.628956, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=54491;
UPDATE `creature_model_info` SET `BoundingRadius`=2.222222, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=54497;
UPDATE `creature_model_info` SET `CombatReach`=1.294068, `VerifiedBuild`=23420 WHERE `DisplayID`=2437;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=46257;
UPDATE `creature_model_info` SET `BoundingRadius`=1.0557, `CombatReach`=5.175, `VerifiedBuild`=23420 WHERE `DisplayID`=46332;
UPDATE `creature_model_info` SET `BoundingRadius`=1.25, `CombatReach`=1.875, `VerifiedBuild`=23420 WHERE `DisplayID`=48265;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=47244;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46606;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3875, `CombatReach`=1.375, `VerifiedBuild`=23420 WHERE `DisplayID`=48280;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8725, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=47218;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47151;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48463;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48274;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48275;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48272;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=23420 WHERE `DisplayID`=48273;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46778;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=48312;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4213, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=47221;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46762;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46774;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46796;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3366, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=46436;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=51455;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47146;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46757;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8725, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=47152;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47147;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47150;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46775;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46773;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46776;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=46779;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=47153;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=23420 WHERE `DisplayID`=49338;
UPDATE `creature_model_info` SET `BoundingRadius`=1.612829, `CombatReach`=6, `VerifiedBuild`=23420 WHERE `DisplayID`=70243;
UPDATE `creature_model_info` SET `BoundingRadius`=2.451996, `CombatReach`=2.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73967;
UPDATE `creature_model_info` SET `BoundingRadius`=2.638643, `CombatReach`=4.125, `VerifiedBuild`=23420 WHERE `DisplayID`=68500;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=62820;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=71014;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=71555;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72189;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72177;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74254;
UPDATE `creature_model_info` SET `CombatReach`=1.25, `VerifiedBuild`=23420 WHERE `DisplayID`=64535;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=61396;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69189;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69164;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69169;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69046;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44060;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44049;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69186;
UPDATE `creature_model_info` SET `BoundingRadius`=0.39, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=66997;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6975, `CombatReach`=13.5, `VerifiedBuild`=23420 WHERE `DisplayID`=62607;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66804;
UPDATE `creature_model_info` SET `BoundingRadius`=1.717191, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=67387;
UPDATE `creature_model_info` SET `BoundingRadius`=0.93, `CombatReach`=9, `VerifiedBuild`=23420 WHERE `DisplayID`=66832;
UPDATE `creature_model_info` SET `BoundingRadius`=0.1, `CombatReach`=0.3, `VerifiedBuild`=23420 WHERE `DisplayID`=42771;
UPDATE `creature_model_info` SET `BoundingRadius`=0.35, `CombatReach`=0.525, `VerifiedBuild`=23420 WHERE `DisplayID`=66998;
UPDATE `creature_model_info` SET `BoundingRadius`=0.25, `CombatReach`=17.5, `VerifiedBuild`=23420 WHERE `DisplayID`=39004;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8725, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=69170;
UPDATE `creature_model_info` SET `BoundingRadius`=0.389, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69165;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69005;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=67317;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41710;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3819442, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=41364;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66082;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66083;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41817;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73439;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66096;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66095;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66654;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68999;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66035;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66078;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66026;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66025;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66020;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44046;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66037;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=69014;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=51461;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73004;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66021;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=43667;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2496, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=66038;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4166664, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=42689;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44039;
UPDATE `creature_model_info` SET `BoundingRadius`=1.2, `CombatReach`=1.2, `VerifiedBuild`=23420 WHERE `DisplayID`=66663;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4166664, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=66665;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=23420 WHERE `DisplayID`=69021;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=39642;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66084;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69188;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44057;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69167;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=69328;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69166;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69044;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69163;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72042;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74247;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4166664, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=41461;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66117;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69003;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66114;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3819442, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=39659;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69043;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69045;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=61397;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8725, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=61382;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68820;
UPDATE `creature_model_info` SET `BoundingRadius`=1.117749, `CombatReach`=2.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68386;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66115;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=23420 WHERE `DisplayID`=45005;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45294;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45300;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=66085;
UPDATE `creature_model_info` SET `BoundingRadius`=0.062, `CombatReach`=0.3, `VerifiedBuild`=23420 WHERE `DisplayID`=46811;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3645831, `CombatReach`=1.575, `VerifiedBuild`=23420 WHERE `DisplayID`=39660;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3819442, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=39658;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=67316;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3819442, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=66066;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73455;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4166664, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=51460;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=44023;
UPDATE `creature_model_info` SET `BoundingRadius`=1.1, `CombatReach`=1.1, `VerifiedBuild`=23420 WHERE `DisplayID`=43557;
UPDATE `creature_model_info` SET `BoundingRadius`=0.05568, `CombatReach`=0.08, `VerifiedBuild`=23420 WHERE `DisplayID`=47959;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2, `CombatReach`=0.6, `VerifiedBuild`=23420 WHERE `DisplayID`=14778;
UPDATE `creature_model_info` SET `BoundingRadius`=0.062, `CombatReach`=0.2, `VerifiedBuild`=23420 WHERE `DisplayID`=54752;
UPDATE `creature_model_info` SET `BoundingRadius`=0.375, `CombatReach`=0.75, `VerifiedBuild`=23420 WHERE `DisplayID`=40905;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3645831, `CombatReach`=1.575, `VerifiedBuild`=23420 WHERE `DisplayID`=39661;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=36833;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68650;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=65763;
UPDATE `creature_model_info` SET `BoundingRadius`=1.8, `CombatReach`=2.7, `VerifiedBuild`=23420 WHERE `DisplayID`=65945;
UPDATE `creature_model_info` SET `BoundingRadius`=1.8, `CombatReach`=2.7, `VerifiedBuild`=23420 WHERE `DisplayID`=65942;
UPDATE `creature_model_info` SET `BoundingRadius`=3.226348, `CombatReach`=7.5, `VerifiedBuild`=23420 WHERE `DisplayID`=64297;
UPDATE `creature_model_info` SET `BoundingRadius`=1.8, `CombatReach`=2.7, `VerifiedBuild`=23420 WHERE `DisplayID`=65944;
UPDATE `creature_model_info` SET `BoundingRadius`=2.575513, `CombatReach`=8, `VerifiedBuild`=23420 WHERE `DisplayID`=65948;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3993053, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=39698;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=15, `VerifiedBuild`=23420 WHERE `DisplayID`=65773;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3819442, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=41700;
UPDATE `creature_model_info` SET `BoundingRadius`=1.93281, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=65517;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41824;
UPDATE `creature_model_info` SET `BoundingRadius`=1.324409, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=63749;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41705;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=65746;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68654;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41704;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41703;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41702;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45055;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45048;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45418;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41818;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=41823;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=65745;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45419;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=8, `VerifiedBuild`=23420 WHERE `DisplayID`=73346;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4687497, `CombatReach`=2.025, `VerifiedBuild`=23420 WHERE `DisplayID`=72650;
UPDATE `creature_model_info` SET `CombatReach`=1.5 WHERE `DisplayID`=64535;
