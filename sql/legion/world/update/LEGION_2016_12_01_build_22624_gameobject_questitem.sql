DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=245794 AND `Idx`=0) OR (`GameObjectEntry`=249744 AND `Idx`=0) OR (`GameObjectEntry`=249748 AND `Idx`=0) OR (`GameObjectEntry`=249742 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(245794, 0, 132377, 22624), -- Compass
(249744, 0, 136985, 22624), -- Can of Overheated Oil
(249748, 0, 136987, 22624), -- Aged Snowplum Brandy
(249742, 0, 136983, 22624); -- Freshly Dug Grave
