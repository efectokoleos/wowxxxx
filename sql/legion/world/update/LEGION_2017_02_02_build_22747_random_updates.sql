DELETE FROM `scene_template` WHERE (`SceneId`=1215 AND `ScriptPackageID`=1575) OR (`SceneId`=1059 AND `ScriptPackageID`=1458) OR (`SceneId`=1339 AND `ScriptPackageID`=1665) OR (`SceneId`=937 AND `ScriptPackageID`=1380) OR (`SceneId`=936 AND `ScriptPackageID`=1379) OR (`SceneId`=935 AND `ScriptPackageID`=1378) OR (`SceneId`=1281 AND `ScriptPackageID`=1630) OR (`SceneId`=1280 AND `ScriptPackageID`=1629) OR (`SceneId`=1279 AND `ScriptPackageID`=1628) OR (`SceneId`=1326 AND `ScriptPackageID`=1655) OR (`SceneId`=1191 AND `ScriptPackageID`=1556) OR (`SceneId`=1187 AND `ScriptPackageID`=1553) OR (`SceneId`=1182 AND `ScriptPackageID`=1551) OR (`SceneId`=1474 AND `ScriptPackageID`=1750) OR (`SceneId`=1445 AND `ScriptPackageID`=1722) OR (`SceneId`=1458 AND `ScriptPackageID`=1737) OR (`SceneId`=1287 AND `ScriptPackageID`=1638) OR (`SceneId`=1141 AND `ScriptPackageID`=1508) OR (`SceneId`=1092 AND `ScriptPackageID`=1477) OR (`SceneId`=1246 AND `ScriptPackageID`=1594) OR (`SceneId`=1350 AND `ScriptPackageID`=1676) OR (`SceneId`=1194 AND `ScriptPackageID`=1560) OR (`SceneId`=1368 AND `ScriptPackageID`=1687) OR (`SceneId`=1146 AND `ScriptPackageID`=1518) OR (`SceneId`=1265 AND `ScriptPackageID`=1565) OR (`SceneId`=1258 AND `ScriptPackageID`=1613) OR (`SceneId`=1244 AND `ScriptPackageID`=1600) OR (`SceneId`=1218 AND `ScriptPackageID`=1580) OR (`SceneId`=1060 AND `ScriptPackageID`=1459);
INSERT INTO `scene_template` (`SceneId`, `Flags`, `ScriptPackageID`) VALUES
(1215, 21, 1575),
(1059, 25, 1458),
(1339, 16, 1665),
(937, 16, 1380),
(936, 16, 1379),
(935, 16, 1378),
(1281, 4, 1630),
(1280, 4, 1629),
(1279, 4, 1628),
(1326, 20, 1655),
(1191, 16, 1556),
(1187, 21, 1553),
(1182, 25, 1551),
(1474, 16, 1750),
(1445, 16, 1722),
(1458, 16, 1737),
(1287, 25, 1638),
(1141, 24, 1508),
(1092, 16, 1477),
(1246, 1, 1594),
(1350, 17, 1676),
(1194, 1, 1560),
(1368, 17, 1687),
(1146, 17, 1518),
(1265, 21, 1565),
(1258, 17, 1613),
(1244, 21, 1600),
(1218, 21, 1580),
(1060, 25, 1459);


DELETE FROM `quest_offer_reward` WHERE `ID` IN (44417 /*One More Legend*/, 39191 /*Legacy of the Icebreaker*/, 42609 /*Recruiting the Troops*/, 42607 /*Captain Stahlstrom*/, 42598 /*Champions of Skyhold*/, 42605 /*Champion: Ragnvald Drakeborn*/, 42606 /*Champion: Finna Bjornsdottir*/, 43949 /*More Weapons of Legend*/, 42597 /*Odyn's Summons*/, 38203 /*Challiane Vineyards*/, 44682 /*Unparalleled Power*/, 42103 /*Let it Feed*/, 41019 /*Actions on Azeroth*/, 41017 /*Empowering Your Artifact*/, 41015 /*Artifacts Need Artificers*/, 40938 /*The Light and the Void*/, 40710 /*Blade in Twilight*/, 40706 /*A Legend You Can Hold*/, 40705 /*Priestly Matters*/, 40044 /*Shadows in the Mists*/, 43595 /*To Honor the Fallen*/, 40046 /*Scavenging the Shallows*/, 39984 /*Remnants of the Past*/, 39792 /*A Stack of Racks*/, 39786 /*A Stone Cold Gamble*/, 39793 /*Only the Finest*/, 39787 /*Rigging the Wager*/, 42447 /*Dances With Ravenbears*/, 42445 /*Nithogg's Tribute*/, 42446 /*Singed Feathers*/, 42444 /*Plight of the Blackfeather*/, 39789 /*Eating Into Our Business*/, 38337 /*Built to Scale*/, 43372 /*Whispers in the Void*/, 43371 /*Relieving the Front Lines*/, 39417 /*Rating Razik*/, 39305 /*Empty Nest*/, 42425 /*Going Down, Going Up*/, 40071 /*Tamer Takedown*/, 40070 /*Eagle Egg Recovery*/, 40069 /*Fledgling Worm Guts*/, 39419 /*Hex-a-Gone*/, 42590 /*Moozy's Reunion*/, 41094 /*Hatchlings of the Talon*/, 43277 /*Tech It Up A Notch*/, 43276 /*Troops in the Field*/, 38711 /*The Warden's Signet*/, 39015 /*Grumpy*/, 38323 /*Return to the Grove*/, 39354 /*Wisp in the Willows*/, 42751 /*Moon Reaver*/, 42786 /*Grotesque Remains*/, 42750 /*Dreamcatcher*/, 42747 /*Where the Wildkin Are*/, 42748 /*Emerald Sisters*/, 42857 /*Moist Around the Hedges*/, 42865 /*Grell to Pay*/, 42884 /*Grassroots Effort*/, 42883 /*All Grell Broke Loose*/, 38862 /*Thieving Thistleleaf*/, 40221 /*Spread Your Lunarwings and Fly*/, 40220 /*Thorny Dancing*/, 42074 /*Return of the Light*/, 41993 /*Salvation From On High*/, 41967 /*Out of the Darkness*/, 41966 /*House Call*/, 41957 /*The Vindicator's Plea*/, 44407 /*The Third Legend*/, 41632 /*A Gift of Time*/, 41631 /*The Nexus Vault*/, 41630 /*Unleashing Judgment*/, 41629 /*Harnessing the Holy Fire*/, 41628 /*Eyes of the Dragon*/, 41627 /*A Forgotten Enemy*/, 41626 /*A New Threat*/, 41625 /*The Light's Wrath*/, 43275 /*Recruiting the Troops*/, 43273 /*Spread the Word*/, 43270 /*Rise, Champions*/, 43272 /*Champion: High Priestess Ishanah*/, 43271 /*Champion: Calia Menethil*/, 43935 /*A Second Legend*/, 44100 /*Proper Introductions*/, 40043 /*The Hunter of Heroes*/, 43750 /*The Call of Battle*/, 42611 /*Einar the Runecaster*/, 42610 /*Troops in the Field*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(44417, 0, 0, 0, 0, 0, 0, 0, 0, 'That will definitely increase your power greatly.', 22747), -- One More Legend
(39191, 0, 0, 0, 0, 0, 0, 0, 0, 'A great deed was done today. I have gained a mighty warrior, and you a powerful artifact.', 22747), -- Legacy of the Icebreaker
(42609, 0, 0, 0, 0, 0, 0, 0, 0, 'We are well underway. Before long you will command an army to be reckoned with.', 22747), -- Recruiting the Troops
(42607, 0, 0, 0, 0, 0, 0, 0, 0, 'Excellent. Hjalmar will have the valarjar whipped into shape in no time.', 22747), -- Captain Stahlstrom
(42598, 0, 0, 0, 0, 0, 0, 0, 0, 'You are ready to command your forces. Let us begin!', 22747), -- Champions of Skyhold
(42605, 1, 0, 0, 0, 0, 0, 0, 0, 'Odyn''s putting you in charge, eh? Count me in. I find it generally unwise to question the gods.', 22747), -- Champion: Ragnvald Drakeborn
(42606, 1, 0, 0, 0, 0, 0, 0, 0, 'I will gladly follow you, $n. I''ve seen what you''re capable of and you have no fear of death.', 22747), -- Champion: Finna Bjornsdottir
(43949, 0, 0, 0, 0, 0, 0, 0, 0, 'A powerful weapon for a powerful champion of the Halls. Let us begin.', 22747), -- More Weapons of Legend
(42597, 1, 0, 0, 0, 0, 0, 0, 0, 'It is as I predicted. You continue to grow in power and prove your worth.$B$BThe time has come for you to take command of my armies, $n.', 22747), -- Odyn's Summons
(38203, 1, 0, 0, 0, 0, 0, 0, 0, 'Thank you, outsider! Thank you very much.$b$BYou have done more for me than my own people ever have.$b$bLady Challiane would never take the word of a lowly cellarman over her vintners.$b$bNow I can use this recipe to earn her favor, and then the favor of our beloved queen Azshara...', 22747), -- Challiane Vineyards
(44682, 1, 0, 0, 0, 0, 0, 0, 0, 'The power within you grows, $n. You will serve the council well.', 22747), -- Unparalleled Power
(42103, 1, 0, 0, 0, 0, 0, 0, 0, 'This should be more than enough blood to satiate the Bloodstone.', 22747), -- Let it Feed
(41019, 1, 0, 0, 0, 0, 0, 0, 0, 'An excellent choice! We will do our best to support your efforts on the Broken Isles.', 22747), -- Actions on Azeroth
(41017, 1, 0, 0, 0, 0, 0, 0, 0, 'Well ain''t that nice an'' shiny!$b$bYou''ll need ta get used to usin'' that thing. Then we can mess with it rather than, ye know, puttin'' it in a museum and respectin'' it for the piece o'' history that it is.$b$b You should head back to Alonsus, he''s giving us the eye.', 22747), -- Empowering Your Artifact
(41015, 22, 0, 0, 0, 0, 0, 0, 0, 'Ach, there ye are. Let me see that priceless weapon ye''re swingin'' ''round like a stick ye bought at the gift shop!', 22747), -- Artifacts Need Artificers
(40938, 1, 0, 0, 0, 0, 0, 0, 0, 'Welcome back, Cardinal. I think you''ll find we''ve done quite a job getting this place prepared for you. We should get started immediately.', 22747), -- The Light and the Void
(40710, 1, 0, 0, 0, 0, 0, 0, 0, 'You have demonstrated no small amount of skill in these sorts of tasks.$b$bThat is good, for we will require even more of you if we are to survive this conflict.', 22747), -- Blade in Twilight
(40706, 1, 0, 0, 0, 0, 0, 0, 0, 'A wise choice. My brethren and I stand ready to assist you on your next move, $n.', 22747), -- A Legend You Can Hold
(40705, 1, 0, 0, 0, 0, 0, 0, 0, 'Can I take that as a yes?$b$bIf you are ready, I''d like to get started immediately.$b$bI know you are immensely capable, but let''s not leave anything to chance!', 22747), -- Priestly Matters
(40044, 0, 0, 0, 0, 0, 0, 0, 0, 'Their toil is ended, and fewer kvaldir walk the lands of Stormheim.$B$BBoth are blessings, I would say.', 22747), -- Shadows in the Mists
(43595, 0, 0, 0, 0, 0, 0, 0, 0, 'I will see that these blades are returned where they belong.$B$BThe dead smile upon you, outsider.', 22747), -- To Honor the Fallen
(40046, 0, 0, 0, 0, 0, 0, 0, 0, 'The Naglfar...$B$BSo Helya''s kvaldir can navigate the mists?', 22747), -- Scavenging the Shallows
(39984, 0, 0, 0, 0, 0, 0, 0, 0, 'The mists of this bay rot not only the wood of the ships, but the very souls of its inhabitants.$B$BBut it seems you have already discovered this.', 22747), -- Remnants of the Past
(39792, 0, 0, 0, 0, 0, 0, 0, 0, 'Oh hey, it''s... you again! Of course you didn''t have any trouble with those musken. You''re a big, tough hero after all!$B$BThe tauren still aren''t back, though. I''m a bit worried that they may have gone off and hunted a bit more that they can chew.$B$BOh, I''ll take those ribs, though. What do you want for em?', 22747), -- A Stack of Racks
(39786, 0, 0, 0, 0, 0, 0, 0, 0, 'Well, well. I half expected to find your statue on the riverbank. You''re gonna get a nice payout once those tauren get back...$B$BYou haven''t seen them around, have you?', 22747), -- A Stone Cold Gamble
(39793, 0, 0, 0, 0, 0, 0, 0, 0, 'You''re back? $B$BI mean... hey, you''re back!$B$BWell, the tauren haven''t come back yet, but no worries, I''ll take that stuff off your hands! Let me show you some of my prime trade goods.', 22747), -- Only the Finest
(39787, 0, 0, 0, 0, 0, 0, 0, 0, 'So you took him out, eh? Those tauren are gonna be so jealous when they get back!  \n\nNaturally, you''ll have to wait till the tauren get back and pay their wager before I can give you your cut. But in the meantime, I''ve got another target for you.', 22747), -- Rigging the Wager
(42447, 0, 0, 0, 0, 0, 0, 0, 0, 'It seems that Nithogg wasn''t interested in the ravenbears'' tribute.$B$BIt''s probably a good idea to get out of here before he comes back...', 22747), -- Dances With Ravenbears
(42445, 0, 0, 0, 0, 0, 0, 0, 0, 'Caw! Caw! $B$B<The ravenbear chieftain flaps his wings excitedly when he sees the reagents you''ve brought.>', 22747), -- Nithogg's Tribute
(42446, 0, 0, 0, 0, 0, 0, 0, 0, 'The Blackfeather chieftain bows respectfully to you. $B$BYou have helped ensure the survival of his tribe.', 22747), -- Singed Feathers
(42444, 0, 0, 0, 0, 0, 0, 0, 0, 'Caw?$B$B<The ravenbear chieftain seems preoccupied with the gathering of reagents for some sort of ritual. Perhaps you could help him?>', 22747), -- Plight of the Blackfeather
(39789, 0, 0, 0, 0, 0, 0, 0, 0, 'Oh, hey! You must be lookin for them tauren was camped here. They just stepped out for a bit, but Snaggle here and I - we can take care of ya!$B$BYou say they were gonna pay you for killin'' some worgs, huh? No prob, bub!$B$BI ain''t got a lot of gold, but these good are pretty valuable. Take your pick!', 22747), -- Eating Into Our Business
(38337, 1, 0, 0, 0, 0, 0, 0, 0, 'Good thinking gathering up these scales. $B$BIt should be simple enough to make a solid piece of armor from them.', 22747), -- Built to Scale
(43372, 1, 0, 0, 0, 0, 0, 0, 0, 'Gilner''s hearing voices, is he? All that dabbling in the shadows has finally caught up to him.', 22747), -- Whispers in the Void
(43371, 1, 0, 0, 0, 0, 0, 0, 0, 'Hopefully our priests have a chance to rest before heading back into battle.', 22747), -- Relieving the Front Lines
(39417, 1, 0, 0, 0, 0, 0, 0, 0, 'It''s good to hear that Razik is makin'' some progress on his gadgets.$b$bMaybe if he could design a gun that aims itself, these amateurs infesting the basin would hit their targets for once.', 22747), -- Rating Razik
(39305, 1, 0, 0, 0, 0, 0, 0, 0, 'Thanks to you, the great eagles of Highmountain will live on.$b$bOne of the eggs I thought was too damaged to salvage appears to be hatching...', 22747), -- Empty Nest
(42425, 1, 0, 0, 0, 0, 0, 0, 0, 'I can''t believe it! I hoped it would work, but I couldn''t be sure.\n\nYou recovered heirlooms passed down by my family for many generations.\n\nI thought them lost forever.', 22747), -- Going Down, Going Up
(40071, 1, 0, 0, 0, 0, 0, 0, 0, 'Korgrul deserved his fate. Thank you, $n.', 22747), -- Tamer Takedown
(40070, 1, 0, 0, 0, 0, 0, 0, 0, 'These eggs are in poor condition, $n. Some are cracked and spoiled, while others are cold to the touch.$b$bYet there is still hope. I believe a few of the eggs you recovered can be saved.', 22747), -- Eagle Egg Recovery
(40069, 1, 0, 0, 0, 0, 0, 0, 0, 'Those worm guts will be perfect for a special sort of rope.', 22747), -- Fledgling Worm Guts
(39419, 1, 0, 0, 0, 0, 0, 0, 0, 'You have done the Skyhorn a great service, $n.', 22747), -- Hex-a-Gone
(42590, 0, 0, 0, 0, 0, 0, 0, 0, 'Your kindness will not be forgotten. You are a friend to my family, $n.', 22747), -- Moozy's Reunion
(41094, 0, 0, 0, 0, 0, 0, 0, 0, '<You feel the warmth of Aviana''s blessing once more. You have earned the ancient''s respect.>', 22747), -- Hatchlings of the Talon
(43277, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent choice, $n. Archon Torias will continue to conduct his research in Netherlight Temple. Speak to him when you want to make additional upgrades.', 22747), -- Tech It Up A Notch
(43276, 1, 0, 0, 0, 0, 0, 0, 0, 'The scouting mission was more successful than we could have imagined, $n.$B$BYour troops were able to rescue Archon Torias, a skilled researcher whose knowledge extends far beyond light and shadow.$B$BIn thanks, he has agreed to lend his expertise to our cause.', 22747), -- Troops in the Field
(38711, 0, 0, 0, 0, 0, 0, 0, 0, '<Jarod gazes at the ring for a long minute before closing his hands around it. He puts the ring in a pocket.>$B$BThis is Maiev''s ring. She would never part with it.$B$BShe must be at Black Rook Hold!', 22747), -- The Warden's Signet
(39015, 0, 0, 0, 0, 0, 0, 0, 0, '<The puppy wags its tail furiously as soon as he spots you. He seems extremely grateful.>', 22747), -- Grumpy
(38323, 0, 0, 0, 0, 0, 0, 0, 0, 'Thaon will be dearly missed. We may not have always seen eye to eye, but he was a great druid and a true protector of the vale.$b$bXavius will pay for his death...', 22747), -- Return to the Grove
(39354, 0, 0, 0, 0, 0, 0, 0, 0, 'Oh thank you traveler! You have truly helped the forest today.', 22747), -- Wisp in the Willows
(42751, 1, 0, 0, 0, 0, 0, 0, 0, 'My wounds are great, and my magic has been severely drained. I must recover from this enervation and return to the Dreamgrove.\n\nThe owlkin here are beyond saving by mortal hands, but perhaps Elune will one day shine her light upon them again.\n\nThank you, $n. May your path be blessed.', 22747), -- Moon Reaver
(42786, 0, 0, 0, 0, 0, 0, 0, 0, 'These remains... this is horrible! They are turning dryads into mindless piles of ooze!', 22747), -- Grotesque Remains
(42750, 18, 0, 0, 0, 0, 0, 0, 0, 'I knew Leirana... she was one of Remulos'' favored, and kindhearted soul. If he has been captured as she believes, we have no time to mourn her passing.', 22747), -- Dreamcatcher
(42747, 1, 0, 0, 0, 0, 0, 0, 0, 'I wish it did not have to come to this, truly.', 22747), -- Where the Wildkin Are
(42748, 603, 0, 0, 0, 0, 0, 0, 0, 'The owlbeasts are... melting the dryads down and using their bones to create minions?! How... how could this even be?', 22747), -- Emerald Sisters
(42857, 396, 0, 0, 0, 0, 0, 0, 0, 'Ah yes, this is the grell food supply. Perhaps I can use this to lure them away. Thank you.', 22747), -- Moist Around the Hedges
(42865, 1, 0, 0, 0, 0, 0, 0, 0, 'I don''t know if grell have mothers, but I hope that one never kissed his mother with his mouth.', 22747), -- Grell to Pay
(42884, 1, 0, 0, 0, 0, 0, 0, 0, 'The grell trapped our young within the huts. They are no longer at risk, thanks to your generous efforts.', 22747), -- Grassroots Effort
(42883, 603, 0, 0, 0, 0, 0, 0, 0, 'Thank you, $n. You are welcome in our village anytime.', 22747), -- All Grell Broke Loose
(38862, 0, 0, 0, 0, 0, 0, 0, 0, 'Until I can discover what is vexing the Thistleleaf, I''ll have to look after the Lunarwing eggs. At least I know they will be safe.', 22747), -- Thieving Thistleleaf
(40221, 0, 0, 0, 0, 0, 0, 0, 0, 'On behalf of the Lunarwings, thank you.', 22747), -- Spread Your Lunarwings and Fly
(40220, 0, 0, 0, 0, 0, 0, 0, 0, 'That should set those vexing Thorndancers on their way.', 22747), -- Thorny Dancing
(42074, 1, 603, 0, 0, 0, 0, 0, 0, 'It is T''uure! I had thought it was lost to us forever!$b$bWhen I last saw this staff we were escaping from a small world called Niskara. The Legion came upon us unawares, we were only able to escape thanks to a few brave Vindicators who remained behind with T''uure to stall their attack.$b$bI did not think it could be done, but you were able to recover T''uure. You truly walk in the light, $n. You have my respect, and my thanks.', 22747), -- Return of the Light
(41993, 1, 0, 0, 0, 0, 0, 0, 0, 'You seek a demon with a staff of crystal? The Eredar that calls itself Lady Calindris has one that matches your description perfectly.$b$bShe''s MY prey though! I''ve been chasing her minion all over the isles and I''m not about to let this opportunity pass.$b$bI could use someone to watch my back though. If you help me out I''d be willing to let you take this staff you seek. I don''t care about her equipment, it''s her soul that I''m after.$b$bWell, what do you say?', 22747), -- Salvation From On High
(41967, 0, 0, 0, 0, 0, 0, 0, 0, 'I felt my soul being ripped from my body. If you hadn''t come when you did... I don''t even want to think about it!$b$bIs there anything I can do to repay you? I owe you my life after all.$b$b<You mention to her Barrem''s story.>$b$bBarrem still lives too? Oh thank the light! Yes, I''ll gladly tell you everything I know.', 22747), -- Out of the Darkness
(41966, 396, 0, 0, 0, 0, 0, 0, 0, 'I... I owe you my life, $gbrother:sister;, but that look on your face tells me you''re here for something else.$b$b<You tell Barrem about his mentioning of T''uure in his sleep.>$b$bI must have been dreaming of Alora, she was my cell mate in that accursed world the demons took me to. She told me she saw the crystal but never said much else.$b$bI just hope she is still alive.', 22747), -- House Call
(41957, 0, 0, 0, 0, 0, 0, 0, 0, 'Thank you for arriving in such haste, $gBrother:Sister;.', 22747), -- The Vindicator's Plea
(44407, 1, 0, 0, 0, 0, 0, 0, 0, 'That was probably the easiest decision you''ve had to make! Let''s get started.', 22747), -- The Third Legend
(41632, 0, 0, 0, 0, 0, 0, 0, 0, 'So the dragons proved helpful? This is good, we may need their assistance in the war to come.', 22747), -- A Gift of Time
(41631, 0, 0, 0, 0, 0, 0, 0, 0, 'You have my gratitude for saving Azuregos, $n. I can rest easy knowing that the Nexus is under his care.$B$BAs for you, I can see you''re full of surprises. Never in my time have I seen a priest who can control the chaotic energies of Light''s Wrath. $B$BYou are truly the hero we need to face the Legion.', 22747), -- The Nexus Vault
(41630, 0, 0, 0, 0, 0, 0, 0, 0, 'Azuregos!$B$BHe must have returned to the Nexus to secure the artifacts in the vault. Malygos always trusted him to watch over the magical items in the care of the flight.$B$BI''m afraid I must ask another favor of you, $n.', 22747), -- Unleashing Judgment
(41629, 0, 0, 0, 0, 0, 0, 0, 0, 'I see you are able to harness the energies of Light''s Wrath. It has been a long time since there was a $c that could control it.\n\nNow, are you prepared to unleash it?', 22747), -- Harnessing the Holy Fire
(41628, 0, 0, 0, 0, 0, 0, 0, 0, 'It seems that the Ethereum has gleaned how to use Malygos''s surge needles to bore deeper into the Twisting Nether.$B$BI believe we might be able to use these devices against them.', 22747), -- Eyes of the Dragon
(41627, 0, 0, 0, 0, 0, 0, 0, 0, 'So the Ethereum has taken the Nexus... and yet, that seems the least of our worries.$B$BTheir plan to widen the breach into the Twisting Nether poses a threat to all of Azeroth. It seems fate has set you on this path for a reason, $n. Not only must you recover the weapon you seek, but you must put a stop to the Ethereum as well!$B$BI regret that I cannot assist you more directly, but I will guide you as I can.', 22747), -- A Forgotten Enemy
(41626, 0, 0, 0, 0, 0, 0, 0, 0, 'This appears to be a communication device commonly utilized by the ethereals.$B$BPerhaps it could be the key to finding out who is behind the happenings at the Dragonshrine.', 22747), -- A New Threat
(41625, 0, 0, 0, 0, 0, 0, 0, 0, 'So you seek Light''s Wrath? A dangerous weapon, to be sure.$B$BLong ago the Kirin Tor entrusted its safekeeping to the blue dragonflight. It was locked away in the only place that could contain its rampant power  - the Nexus Vault.$B$BI have not been there in some time, however. After the blue dragonflight was dissolved, the Nexus was abandoned. There is no knowing whether the Nexus Vault is still intact - or what may be waiting in the Nexus itself.', 22747), -- The Light's Wrath
(43275, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. These troops will be invaluable in our defense of the Broken Isles. Deploy them on missions, just as you would with our champions.', 22747), -- Recruiting the Troops
(43273, 1, 0, 0, 0, 0, 0, 0, 0, 'Calia''s mission was a success! We may be seeing some new faces around here soon.', 22747), -- Spread the Word
(43270, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. Calia Menethil and High Priestess Ishanah are worthy champions. We can trust them to carry out any task we ask of them.', 22747), -- Rise, Champions
(43272, 1, 0, 0, 0, 0, 0, 0, 0, 'Your efforts to restore Saa''ra have not gone unnoticed. As a humble servant of the Sha''tar, I thank you.$B$BAs leader of the Aldor, it is my duty to safeguard all that is holy. This temple is a beacon of light that must be protected.$B$BYou can count me among your allies, willing to do whatever is necessary to destroy the Burning Legion.', 22747), -- Champion: High Priestess Ishanah
(43271, 1, 0, 0, 0, 0, 0, 0, 0, 'Well met, $n. It''s an honor to serve at your side.$B$BI''m happy to help in any way I can.', 22747), -- Champion: Calia Menethil
(43935, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. You can begin as soon as you are ready.', 22747), -- A Second Legend
(44100, 1, 0, 0, 0, 0, 0, 0, 0, 'I see you received my message.', 22747), -- Proper Introductions
(40043, 0, 0, 0, 0, 0, 0, 0, 0, 'Curse Helya, she''s corrupted the blades! Still, her champion defeated, her helarjar skulking back to Helheim, and my weapons in the hands of my chosen champion! She even made them more powerful than before. Wield them well, $n, smite the enemies of this world!', 22747), -- The Hunter of Heroes
(43750, 1, 0, 0, 0, 0, 0, 0, 0, 'Ah yes, the Gjallarhorn.', 22747), -- The Call of Battle
(42611, 1, 0, 0, 0, 0, 0, 0, 0, 'A wise choice, $n.', 22747), -- Einar the Runecaster
(42610, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent! With the runecaster''s powers your armies will be even stronger!', 22747); -- Troops in the Field


DELETE FROM `quest_poi` WHERE (`QuestID`=42371 AND `BlobIndex`=0 AND `Idx1`=10) OR (`QuestID`=39387 AND `BlobIndex`=0 AND `Idx1`=3) OR (`QuestID`=39387 AND `BlobIndex`=0 AND `Idx1`=1) OR (`QuestID`=38717 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=38715 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=38714 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=38377 AND `BlobIndex`=1 AND `Idx1`=3) OR (`QuestID`=41626 AND `BlobIndex`=0 AND `Idx1`=2);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(42371, 0, 10, 32, 0, 0, 1220, 1015, 0, 0, 0, 0, 0, 1166222, 22747), -- Study Hall: Combat Research
(39387, 0, 3, 2, 280509, 99225, 1220, 1024, 0, 0, 0, 0, 0, 1082778, 22747), -- The Skies of Highmountain
(39387, 0, 1, 0, 280358, 98773, 1220, 1024, 0, 0, 0, 0, 0, 1082777, 22747), -- The Skies of Highmountain
(38717, 0, 2, 32, 0, 0, 1220, 1018, 0, 0, 0, 0, 0, 1013448, 22747), -- Black Rook Prison
(38715, 0, 4, 32, 0, 0, 1220, 1018, 0, 0, 0, 0, 0, 1012704, 22747), -- The Rook's Guard
(38714, 0, 4, 32, 0, 0, 1220, 1018, 0, 0, 0, 0, 0, 1012704, 22747), -- Maiev's Trail
(38377, 1, 3, 32, 0, 0, 1220, 1018, 0, 0, 0, 0, 0, 1000294, 22747), -- The Emerald Queen
(41626, 0, 2, 1, 282954, 105092, 571, 488, 0, 0, 0, 0, 0, 1140706, 22747); -- A New Threat

DELETE FROM `quest_poi_points` WHERE (`QuestID`=42371 AND `Idx1`=10 AND `Idx2`=0) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=40593 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=38717 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=38715 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=38714 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=11) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=10) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=41724 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=41708 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=11) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=10) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=9) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=8) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=7) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=6) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=5) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=41626 AND `Idx1`=3 AND `Idx2`=1);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(42371, 10, 0, 191, 6463, 22747), -- Study Hall: Combat Research
(40593, 1, 11, -8403, 179, 22747), -- Demons Among Us
(40593, 1, 10, -8441, 223, 22747), -- Demons Among Us
(40593, 1, 9, -8401, 247, 22747), -- Demons Among Us
(40593, 1, 8, -8446, 305, 22747), -- Demons Among Us
(40593, 1, 7, -8390, 360, 22747), -- Demons Among Us
(40593, 1, 6, -8352, 327, 22747), -- Demons Among Us
(40593, 1, 5, -8314, 359, 22747), -- Demons Among Us
(40593, 1, 4, -8264, 297, 22747), -- Demons Among Us
(40593, 1, 3, -8284, 279, 22747), -- Demons Among Us
(40593, 1, 2, -8246, 241, 22747), -- Demons Among Us
(40593, 1, 1, -8289, 207, 22747), -- Demons Among Us
(38717, 2, 0, 3111, 7185, 22747), -- Black Rook Prison
(38715, 4, 0, 3016, 7362, 22747), -- The Rook's Guard
(38714, 4, 0, 3016, 7362, 22747), -- Maiev's Trail
(41724, 0, 11, 3304, 5950, 22747), -- Heart of the Nightmare
(41724, 0, 10, 3304, 5956, 22747), -- Heart of the Nightmare
(41724, 0, 9, 3304, 5969, 22747), -- Heart of the Nightmare
(41724, 0, 8, 3310, 5979, 22747), -- Heart of the Nightmare
(41724, 0, 7, 3323, 5979, 22747), -- Heart of the Nightmare
(41724, 0, 6, 3333, 5979, 22747), -- Heart of the Nightmare
(41724, 0, 5, 3340, 5976, 22747), -- Heart of the Nightmare
(41724, 0, 4, 3343, 5969, 22747), -- Heart of the Nightmare
(41724, 0, 3, 3343, 5960, 22747), -- Heart of the Nightmare
(41724, 0, 2, 3343, 5947, 22747), -- Heart of the Nightmare
(41724, 0, 1, 3327, 5940, 22747), -- Heart of the Nightmare
(41708, 1, 11, 3113, 5900, 22747), -- Dark Side of the Moon
(41708, 1, 10, 3109, 5907, 22747), -- Dark Side of the Moon
(41708, 1, 9, 3106, 5918, 22747), -- Dark Side of the Moon
(41708, 1, 8, 3106, 5925, 22747), -- Dark Side of the Moon
(41708, 1, 7, 3109, 5935, 22747), -- Dark Side of the Moon
(41708, 1, 6, 3119, 5939, 22747), -- Dark Side of the Moon
(41708, 1, 5, 3126, 5939, 22747), -- Dark Side of the Moon
(41708, 1, 4, 3140, 5935, 22747), -- Dark Side of the Moon
(41708, 1, 3, 3140, 5928, 22747), -- Dark Side of the Moon
(41708, 1, 2, 3140, 5918, 22747), -- Dark Side of the Moon
(41708, 1, 1, 3130, 5893, 22747), -- Dark Side of the Moon
(41626, 3, 11, 3078, 387, 22747), -- A New Threat
(41626, 3, 10, 3024, 414, 22747), -- A New Threat
(41626, 3, 9, 2951, 496, 22747), -- A New Threat
(41626, 3, 8, 2951, 532, 22747), -- A New Threat
(41626, 3, 7, 2979, 596, 22747), -- A New Threat
(41626, 3, 6, 3006, 641, 22747), -- A New Threat
(41626, 3, 5, 3069, 641, 22747), -- A New Threat
(41626, 3, 4, 3169, 623, 22747), -- A New Threat
(41626, 3, 3, 3233, 586, 22747), -- A New Threat
(41626, 3, 2, 3233, 505, 22747), -- A New Threat
(41626, 3, 1, 3233, 459, 22747); -- A New Threat


DELETE FROM `quest_greeting` WHERE (`ID`=94318 AND `Type`=0) OR (`ID`=97480 AND `Type`=0) OR (`ID`=91531 AND `Type`=0) OR (`ID`=97270 AND `Type`=0) OR (`ID`=93446 AND `Type`=0) OR (`ID`=107498 AND `Type`=0) OR (`ID`=91249 AND `Type`=0) OR (`ID`=91519 AND `Type`=0) OR (`ID`=91481 AND `Type`=0) OR (`ID`=243836 AND `Type`=1) OR (`ID`=90866 AND `Type`=0) OR (`ID`=94561 AND `Type`=0) OR (`ID`=93826 AND `Type`=0) OR (`ID`=93805 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(94318, 0, 0, 0, 'We must act swiftly if we wish to apprehend the Banshee Queen. $B$BAre you prepared to answer the call of duty, $c?', 22747), -- 94318
(97480, 0, 0, 0, 'We face a formidable foe, but we must not let Helya win.', 22747), -- 97480
(91531, 0, 0, 0, 'I''m sure ye don''t belong here in Helheim, just like me.  $B$BI just hope Helya agrees with us.', 22747), -- 91531
(97270, 0, 0, 0, 'What have you uncovered in Haustvald, outsider?', 22747), -- 97270
(93446, 0, 0, 0, 'The Mystics have committed an unforgivable heresy here. The Valkyra will not forget this trespass.', 22747), -- 93446
(107498, 0, 0, 0, 'Caw?', 22747), -- 107498
(91249, 0, 0, 0, 'You... $B$BYou are not like the others.', 22747), -- 91249
(91519, 0, 0, 0, 'We''re almost there, just one small problem...', 22747), -- 91519
(91481, 0, 0, 0, 'Once we''re able to breach this wall, we should be able to cut right through to the mountain path.', 22747), -- 91481
(243836, 1, 0, 0, 'This cannot be! What kind of gall does this Skovald have to disregard these ancient rites! $B$BThis must not stand!', 22747), -- 243836
(90866, 0, 0, 0, 'Those freaks managed to blow what was left of our ammunition stores; it''s a miracle the ship held together as long as she did!', 22747), -- 90866
(94561, 0, 0, 0, 'My duty is to the Chieftain of Highmountain and no other.', 22747), -- 94561
(93826, 0, 0, 0, 'Dargrul the Underking is using my people as fodder for his new toy and army. He thinks by defeating us it will prepare him for the coming of the Burning Legion.$b$bWhen I''m done with him, he will wish he had faced the Burning Legion instead.', 22747), -- 93826
(93805, 0, 0, 0, '', 22747); -- 93805


DELETE FROM `quest_details` WHERE `ID` IN (40043 /*The Hunter of Heroes*/, 44417 /*One More Legend*/, 42610 /*Troops in the Field*/, 42609 /*Recruiting the Troops*/, 42607 /*Captain Stahlstrom*/, 42598 /*Champions of Skyhold*/, 39191 /*Legacy of the Icebreaker*/, 43949 /*More Weapons of Legend*/, 42597 /*Odyn's Summons*/, 38203 /*Challiane Vineyards*/, 44283 /*Piercing the Veil*/, 42517 /*Ritual of Doom*/, 41019 /*Actions on Azeroth*/, 41017 /*Empowering Your Artifact*/, 41015 /*Artifacts Need Artificers*/, 40938 /*The Light and the Void*/, 40710 /*Blade in Twilight*/, 40706 /*A Legend You Can Hold*/, 40705 /*Priestly Matters*/, 39985 /*Khadgar's Discovery*/, 43341 /*Uniting the Isles*/, 40044 /*Shadows in the Mists*/, 43595 /*To Honor the Fallen*/, 40046 /*Scavenging the Shallows*/, 39984 /*Remnants of the Past*/, 42483 /*Put It All on Red*/, 39786 /*A Stone Cold Gamble*/, 39792 /*A Stack of Racks*/, 42447 /*Dances With Ravenbears*/, 42445 /*Nithogg's Tribute*/, 42446 /*Singed Feathers*/, 42444 /*Plight of the Blackfeather*/, 39787 /*Rigging the Wager*/, 39793 /*Only the Finest*/, 39789 /*Eating Into Our Business*/, 43373 /*The Best and Brightest*/, 43372 /*Whispers in the Void*/, 39305 /*Empty Nest*/, 42425 /*Going Down, Going Up*/, 40071 /*Tamer Takedown*/, 40070 /*Eagle Egg Recovery*/, 40069 /*Fledgling Worm Guts*/, 39419 /*Hex-a-Gone*/, 39417 /*Rating Razik*/, 42590 /*Moozy's Reunion*/, 41094 /*Hatchlings of the Talon*/, 43371 /*Relieving the Front Lines*/, 43277 /*Tech It Up A Notch*/, 38711 /*The Warden's Signet*/, 39354 /*Wisp in the Willows*/, 42751 /*Moon Reaver*/, 42786 /*Grotesque Remains*/, 42750 /*Dreamcatcher*/, 42747 /*Where the Wildkin Are*/, 42748 /*Emerald Sisters*/, 42857 /*Moist Around the Hedges*/, 42865 /*Grell to Pay*/, 42884 /*Grassroots Effort*/, 42883 /*All Grell Broke Loose*/, 38323 /*Return to the Grove*/, 38862 /*Thieving Thistleleaf*/, 40221 /*Spread Your Lunarwings and Fly*/, 40220 /*Thorny Dancing*/, 42074 /*Return of the Light*/, 41993 /*Salvation From On High*/, 41967 /*Out of the Darkness*/, 41966 /*House Call*/, 41957 /*The Vindicator's Plea*/, 44407 /*The Third Legend*/, 41632 /*A Gift of Time*/, 41631 /*The Nexus Vault*/, 41630 /*Unleashing Judgment*/, 41629 /*Harnessing the Holy Fire*/, 41628 /*Eyes of the Dragon*/, 41627 /*A Forgotten Enemy*/, 41626 /*A New Threat*/, 44009 /*A Falling Star*/, 43276 /*Troops in the Field*/, 43275 /*Recruiting the Troops*/, 43273 /*Spread the Word*/, 41625 /*The Light's Wrath*/, 43270 /*Rise, Champions*/, 43935 /*A Second Legend*/, 44100 /*Proper Introductions*/, 42193 /*The Gjallarhorn*/, 43750 /*The Call of Battle*/, 42611 /*Einar the Runecaster*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(40043, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Hunter of Heroes
(44417, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- One More Legend
(42610, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Troops in the Field
(42609, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Recruiting the Troops
(42607, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Captain Stahlstrom
(42598, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Champions of Skyhold
(39191, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Legacy of the Icebreaker
(43949, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- More Weapons of Legend
(42597, 1, 1, 0, 0, 0, 0, 0, 0, 22747), -- Odyn's Summons
(38203, 20, 1, 0, 0, 0, 0, 0, 0, 22747), -- Challiane Vineyards
(44283, 1, 0, 0, 0, 50, 0, 0, 0, 22747), -- Piercing the Veil
(42517, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Ritual of Doom
(41019, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Actions on Azeroth
(41017, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Empowering Your Artifact
(41015, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Artifacts Need Artificers
(40938, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Light and the Void
(40710, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Blade in Twilight
(40706, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- A Legend You Can Hold
(40705, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Priestly Matters
(39985, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Khadgar's Discovery
(43341, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Uniting the Isles
(40044, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Shadows in the Mists
(43595, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- To Honor the Fallen
(40046, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Scavenging the Shallows
(39984, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Remnants of the Past
(42483, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Put It All on Red
(39786, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- A Stone Cold Gamble
(39792, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- A Stack of Racks
(42447, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Dances With Ravenbears
(42445, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Nithogg's Tribute
(42446, 403, 0, 0, 0, 0, 0, 0, 0, 22747), -- Singed Feathers
(42444, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Plight of the Blackfeather
(39787, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Rigging the Wager
(39793, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Only the Finest
(39789, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Eating Into Our Business
(43373, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Best and Brightest
(43372, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Whispers in the Void
(39305, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Empty Nest
(42425, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Going Down, Going Up
(40071, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Tamer Takedown
(40070, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Eagle Egg Recovery
(40069, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Fledgling Worm Guts
(39419, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Hex-a-Gone
(39417, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Rating Razik
(42590, 2, 1, 0, 0, 0, 3000, 0, 0, 22747), -- Moozy's Reunion
(41094, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Hatchlings of the Talon
(43371, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Relieving the Front Lines
(43277, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Tech It Up A Notch
(38711, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Warden's Signet
(39354, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Wisp in the Willows
(42751, 603, 0, 0, 0, 0, 0, 0, 0, 22747), -- Moon Reaver
(42786, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Grotesque Remains
(42750, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Dreamcatcher
(42747, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Where the Wildkin Are
(42748, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Emerald Sisters
(42857, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Moist Around the Hedges
(42865, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Grell to Pay
(42884, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Grassroots Effort
(42883, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- All Grell Broke Loose
(38323, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Return to the Grove
(38862, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Thieving Thistleleaf
(40221, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Spread Your Lunarwings and Fly
(40220, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Thorny Dancing
(42074, 1, 273, 1, 603, 0, 0, 0, 0, 22747), -- Return of the Light
(41993, 1, 273, 1, 603, 0, 0, 0, 0, 22747), -- Salvation From On High
(41967, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Out of the Darkness
(41966, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- House Call
(41957, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Vindicator's Plea
(44407, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Third Legend
(41632, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- A Gift of Time
(41631, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Nexus Vault
(41630, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Unleashing Judgment
(41629, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Harnessing the Holy Fire
(41628, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- Eyes of the Dragon
(41627, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- A Forgotten Enemy
(41626, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- A New Threat
(44009, 1, 1, 1, 0, 0, 0, 0, 0, 22747), -- A Falling Star
(43276, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Troops in the Field
(43275, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Recruiting the Troops
(43273, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Spread the Word
(41625, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Light's Wrath
(43270, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Rise, Champions
(43935, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- A Second Legend
(44100, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- Proper Introductions
(42193, 0, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Gjallarhorn
(43750, 1, 0, 0, 0, 0, 0, 0, 0, 22747), -- The Call of Battle
(42611, 1, 0, 0, 0, 0, 0, 0, 0, 22747); -- Einar the Runecaster


DELETE FROM `quest_request_items` WHERE `ID` IN (38203 /*Challiane Vineyards*/, 37857 /*Runas Knows the Way*/, 38232 /*Minion! Kill Them!*/, 37528 /*Let Sleeping Giants Lie*/, 37469 /*The Tidestone: Shattered*/, 37730 /*The Headmistress' Keys*/, 42371 /*Study Hall: Combat Research*/, 42370 /*Wanding 101*/, 37736 /*Dressing With Class*/, 42693 /*You Never Know Until You Scry*/, 44283 /*Piercing the Veil*/, 42102 /*One Who's Worthy*/, 42103 /*Let it Feed*/, 40517 /*The Fallen Lion*/, 39061 /*Whispers from the Dark*/, 43595 /*To Honor the Fallen*/, 40046 /*Scavenging the Shallows*/, 39849 /*To Light the Way*/, 39848 /*A Desperate Bargain*/, 38324 /*Accessories of the Cursed*/, 38339 /*A Little Kelp From My Foes*/, 38817 /*Regal Remains*/, 38808 /*Bjornharta*/, 38810 /*The Dreaming Fungus*/, 39792 /*A Stack of Racks*/, 39786 /*A Stone Cold Gamble*/, 39793 /*Only the Finest*/, 39787 /*Rigging the Wager*/, 42445 /*Nithogg's Tribute*/, 38405 /*To Weather the Storm*/, 38337 /*Built to Scale*/, 39593 /*The Shattered Watcher*/, 39595 /*Blood and Gold*/, 39590 /*Ahead of the Game*/, 38558 /*See Ya Later, Oscillator*/, 38053 /*Assault and Battery*/, 38036 /*Supplies From the Skies*/, 38206 /*Making the Rounds*/, 39862 /*The Siegebrul*/, 39777 /*Buy Us Time*/, 40047 /*I'll Huff, I'll Puff...*/, 39439 /*Stonedark Relics*/, 39426 /*Blood Debt*/, 39425 /*Stonedark Crystal*/, 39373 /*Hags of a Feather*/, 39867 /*I'm Not Lion!*/, 39178 /*Moose on the Loose*/, 39392 /*Bear Huntin'*/, 39764 /*Shiny, But Deadly*/, 39859 /*Note-Eating Goats*/, 39322 /*The Witchqueen*/, 42425 /*Going Down, Going Up*/, 40070 /*Eagle Egg Recovery*/, 40069 /*Fledgling Worm Guts*/, 39772 /*Can't Hold a Candle To You*/, 40345 /*Burn the Candle at Both Ends*/, 39765 /*Wax On, Wax Off*/, 39670 /*Critter Scatter Shot*/, 42622 /*Ceremonial Drums*/, 39990 /*Huln's War - Reinforcements*/, 40520 /*To See the Past*/, 38909 /*Get to High Ground*/, 39043 /*Bitestone Enclave*/, 39488 /*Balance of Elements*/, 39491 /*Ormgul the Pestilent*/, 43371 /*Relieving the Front Lines*/, 38717 /*Black Rook Prison*/, 38647 /*For the Corn!*/, 39117 /*Shriek No More*/, 38684 /*Reading the Leaves*/, 38671 /*Lost in Retreat*/, 38655 /*Root Cause*/, 38662 /*Tears for Fears*/, 42857 /*Moist Around the Hedges*/, 38862 /*Thieving Thistleleaf*/, 38014 /*Feasting on the Dragon*/, 37960 /*Leyline Abuse*/, 37856 /*The Withered*/, 37657 /*Making the World Safe for Profit*/, 42694 /*Back from the Dead*/, 37492 /*A Rather Long Walk*/, 37853 /*The Death of the Eldest*/, 38645 /*Children of the Night*/, 39354 /*Wisp in the Willows*/, 38147 /*Entangled Dreams*/, 38455 /*Frenzied Furbolgs*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(38203, 0, 0, 0, 0, 'Have you found the pieces of the recipe?', 22747), -- Challiane Vineyards
(37857, 0, 0, 0, 0, 'I still don''t trust him.', 22747), -- Runas Knows the Way
(38232, 0, 0, 0, 0, 'This land is ripe for the taking. Especially the olives. They are really ripe. And I love ripe olives.', 22747), -- Minion! Kill Them!
(37528, 0, 0, 0, 0, 'As soon as I can, I''ll arm the gladiators in Oceanus Cove.', 22747), -- Let Sleeping Giants Lie
(37469, 0, 0, 0, 0, '<The last remaining piece of the Tidestone lies before you.>', 22747), -- The Tidestone: Shattered
(37730, 0, 0, 0, 0, 'Were you able to meet with the headmistress?', 22747), -- The Headmistress' Keys
(42371, 0, 0, 0, 0, 'I agree. Research can be grueling work.', 22747), -- Study Hall: Combat Research
(42370, 0, 0, 0, 0, 'You''ll never learn if you never practice.', 22747), -- Wanding 101
(37736, 0, 0, 0, 0, 'Still trying to get into the academy, I see.', 22747), -- Dressing With Class
(42693, 0, 0, 0, 0, 'I would have brought my own spell foci if SOMEONE hadn''t been rushing me...', 22747), -- You Never Know Until You Scry
(44283, 0, 0, 0, 0, 'If only Cenarius were with us, we could confront Xavius at once.', 22747), -- Piercing the Veil
(42102, 1, 0, 0, 0, 'We need a warlock who is skilled in summoning greater demons if we wish to proceed.', 22747), -- One Who's Worthy
(42103, 0, 0, 0, 0, 'We will need to feed the Bloodstone before it will be strong enough to control the eredar sisters.', 22747), -- Let it Feed
(40517, 0, 0, 0, 0, 'Welcome, $n. I am pleased you survived the battle. You have something for me?', 22747), -- The Fallen Lion
(39061, 0, 0, 0, 0, 'What have you found?', 22747), -- Whispers from the Dark
(43595, 0, 0, 0, 0, 'Do you have the blades?', 22747), -- To Honor the Fallen
(40046, 0, 0, 0, 0, 'Have you found answers, or more questions?', 22747), -- Scavenging the Shallows
(39849, 0, 0, 0, 0, 'Were you able to secure the lantern?', 22747), -- To Light the Way
(39848, 0, 0, 0, 0, '<The air hangs silent as you approach.>', 22747), -- A Desperate Bargain
(38324, 0, 0, 0, 0, 'Make sure ye''ve got enough bones before we move on.', 22747), -- Accessories of the Cursed
(38339, 0, 0, 0, 0, 'Do ye have enough for a proper disguise?', 22747), -- A Little Kelp From My Foes
(38817, 0, 0, 0, 0, 'Were you able to recover her remains?', 22747), -- Regal Remains
(38808, 0, 0, 0, 0, 'How went the hunt, outsider?', 22747), -- Bjornharta
(38810, 0, 0, 0, 0, 'What do you bring?', 22747), -- The Dreaming Fungus
(39792, 0, 0, 0, 0, 'Aint nothin like some good old meaty musken ribs, eh pal?', 22747), -- A Stack of Racks
(39786, 0, 0, 0, 0, 'Did you take him out?', 22747), -- A Stone Cold Gamble
(39793, 0, 0, 0, 0, 'Make sure to get the good stuff!', 22747), -- Only the Finest
(39787, 0, 0, 0, 0, 'You got my trophy yet?', 22747), -- Rigging the Wager
(42445, 0, 0, 0, 0, 'Ca-caw?', 22747), -- Nithogg's Tribute
(38405, 0, 0, 0, 0, 'Did you find everything?', 22747), -- To Weather the Storm
(38337, 0, 0, 0, 0, 'Oh, what do you have there?', 22747), -- Built to Scale
(39593, 0, 0, 0, 0, 'Have you managed to find them all?', 22747), -- The Shattered Watcher
(39595, 0, 0, 0, 0, 'What is this you bring?$B$BYou honor me, outsider.', 22747), -- Blood and Gold
(39590, 0, 0, 0, 0, 'An ironic gift you bring me, challenger.', 22747), -- Ahead of the Game
(38558, 0, 0, 0, 0, 'Did you find the oscillator?', 22747), -- See Ya Later, Oscillator
(38053, 0, 0, 0, 0, 'I cannot build a new transponder without that battery!', 22747), -- Assault and Battery
(38036, 0, 0, 0, 0, 'Did you find anything?', 22747), -- Supplies From the Skies
(38206, 0, 0, 0, 0, 'As soon as preparations are complete, we set out for Stormheim.', 22747), -- Making the Rounds
(39862, 0, 0, 0, 0, 'Siegbrul Olgrul torments all who serve him. There are evil drogbar, and then there is this foul monster.', 22747), -- The Siegebrul
(39777, 0, 0, 0, 0, 'We need all the time you can buy us.', 22747), -- Buy Us Time
(40047, 0, 0, 0, 0, 'Pufferfish have this impressive ability of... exploding. They could be a powerful tool for Murky.', 22747), -- I'll Huff, I'll Puff...
(39439, 0, 0, 0, 0, 'We have possessed these relics for generations until the murlocs put their fishy flippers on them.', 22747), -- Stonedark Relics
(39426, 0, 0, 0, 0, 'Drogbar are not violent by nature, despite our size. But the Bloodtotem crossed us, and they pay the debt in blood.', 22747), -- Blood Debt
(39425, 0, 0, 0, 0, 'I have power over the earth without my crystal, but my power is much greater with it than without.', 22747), -- Stonedark Crystal
(39373, 0, 0, 0, 0, 'Your size belies your strength, $n.', 22747), -- Hags of a Feather
(39867, 0, 0, 0, 0, 'I''ll have the last laugh when Hemet sees proof that this magical lion exists!', 22747), -- I'm Not Lion!
(39178, 0, 0, 0, 0, 'I''m looking forward to hanging the antlers of an ancient spirit within the walls of the hunters'' lodge!', 22747), -- Moose on the Loose
(39392, 1, 0, 0, 0, 'Procuring a few Bristlefur Pelts should help sell me on your huntin'' skills.', 22747), -- Bear Huntin'
(39764, 0, 0, 0, 0, 'Have you recovered the soul chambers yet?', 22747), -- Shiny, But Deadly
(39859, 1, 0, 0, 0, 'I''ve been waitin'' all day for Addie to finish her first draft.$b$bHow hard can it be to write a few words about the greatest hunter who''s ever lived?', 22747), -- Note-Eating Goats
(39322, 0, 0, 0, 0, 'Without a queen, it will take the harpies many years before they can organize in such numbers again.', 22747), -- The Witchqueen
(42425, 1, 0, 0, 0, 'I have never been particularly fond of heights.', 22747), -- Going Down, Going Up
(40070, 1, 0, 0, 0, 'If the pilfered eggs are not recovered, the eagle may go extinct.', 22747), -- Eagle Egg Recovery
(40069, 1, 0, 0, 0, 'What are their guts for? Well, you''ll see. You might not like it, but you will soon find out.', 22747), -- Fledgling Worm Guts
(39772, 0, 0, 0, 0, 'Have you found my skull?', 22747), -- Can't Hold a Candle To You
(40345, 0, 0, 0, 0, 'You come in when jobs are done.', 22747), -- Burn the Candle at Both Ends
(39765, 0, 0, 0, 0, 'The desecration...', 22747), -- Wax On, Wax Off
(39670, 0, 0, 0, 0, 'Have you snatched the rabbits yet?', 22747), -- Critter Scatter Shot
(42622, 0, 0, 0, 0, 'Can you feel the beat?', 22747), -- Ceremonial Drums
(39990, 0, 0, 0, 0, 'This shouldn''t take long. Please do not allow them to interrupt my spell.', 22747), -- Huln's War - Reinforcements
(40520, 0, 0, 0, 0, 'Earth, air, and water are all magics we have come to know well in our mountain.', 22747), -- To See the Past
(38909, 0, 0, 0, 0, 'Have you had any luck with the Rivermane?', 22747), -- Get to High Ground
(39043, 0, 0, 0, 0, 'Highmountain stands!', 22747), -- Bitestone Enclave
(39488, 0, 0, 0, 0, 'I believe the drogbar are learning stronger magic now that they have possession of the Hammer of Khaz''goroth.', 22747), -- Balance of Elements
(39491, 0, 0, 0, 0, 'The Rivermane are a peaceful tribe. We provide the others with food and healing in exchange for protection and equipment.\n\nWhen pressed, however, I am willing to put my magic to more violent use.', 22747), -- Ormgul the Pestilent
(43371, 1, 0, 0, 0, 'How''s the mission proceeding?', 22747), -- Relieving the Front Lines
(38717, 0, 0, 0, 0, 'Do you have the keys?', 22747), -- Black Rook Prison
(38647, 0, 0, 0, 0, 'The corn isn''t gonna gather itself.', 22747), -- For the Corn!
(39117, 1, 0, 0, 0, 'Papa always said the only way he''d leave here would be feet-first.$B$BI wish he''d been wrong.', 22747), -- Shriek No More
(38684, 0, 0, 0, 0, 'Ahh... the fear! The panic!$b$bI haven''t the strength to fight much longer...', 22747), -- Reading the Leaves
(38671, 0, 0, 0, 0, 'Have you retrieved them?', 22747), -- Lost in Retreat
(38655, 0, 0, 0, 0, 'Have you collected the root samples?', 22747), -- Root Cause
(38662, 0, 0, 0, 0, 'Do you have the Tears of Elune?', 22747), -- Tears for Fears
(42857, 0, 0, 0, 0, 'You have found a fruit that might be of interest to me?', 22747), -- Moist Around the Hedges
(38862, 0, 0, 0, 0, 'Gather as many eggs as you can and bring them back to me. I will keep them safe.', 22747), -- Thieving Thistleleaf
(38014, 0, 0, 0, 0, 'Yes, $n?', 22747), -- Feasting on the Dragon
(37960, 0, 0, 0, 0, 'With each one of these cretins you slay, I grow stronger.', 22747), -- Leyline Abuse
(37856, 0, 0, 0, 0, 'Yes?', 22747), -- The Withered
(37657, 6, 0, 0, 0, 'How goes the cleanup?', 22747), -- Making the World Safe for Profit
(42694, 0, 0, 0, 0, 'Thank you for your aid, $c.', 22747), -- Back from the Dead
(37492, 0, 0, 0, 0, 'Have you seen him?', 22747), -- A Rather Long Walk
(37853, 0, 0, 0, 0, 'Call me a fool. I''d always just thought that grandfather would be with us forever.', 22747), -- The Death of the Eldest
(38645, 0, 0, 0, 0, 'The attackers are night elves, yes, but they are not the same night elves I once knew.', 22747), -- Children of the Night
(39354, 0, 0, 0, 0, 'Any luck?', 22747), -- Wisp in the Willows
(38147, 0, 0, 0, 0, 'Someone entangled us within a sinister spell.', 22747), -- Entangled Dreams
(38455, 0, 0, 0, 0, 'Did it work?', 22747); -- Frenzied Furbolgs


DELETE FROM `creature_model_info` WHERE `DisplayID` IN (65819, 70810, 62208);
INSERT INTO `creature_model_info` (`DisplayID`, `BoundingRadius`, `CombatReach`, `DisplayID_Other_Gender`, `VerifiedBuild`) VALUES
(65819, 2.213402, 1, 0, 22747),
(70810, 0.5782362, 0.6, 0, 22747),
(62208, 1.730602, 2, 0, 22747);

UPDATE `creature_model_info` SET `CombatReach`=1.5 WHERE `DisplayID`=70398;


UPDATE `gameobject_template` SET `type`=5, `Data2`=0, `VerifiedBuild`=22747 WHERE `entry`=258226; -- Black Rook Gate

