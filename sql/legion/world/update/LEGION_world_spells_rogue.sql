DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_legion_rogue_alacrity',
'spell_legion_rogue_backstab',
'spell_legion_rogue_bag_of_tricks',
'spell_legion_rogue_between_the_eyes',
'spell_legion_rogue_blade_flurry',
'spell_legion_rogue_blade_flurry_chain',
'spell_legion_rogue_blind',
'spell_legion_rogue_blunderbuss',
'spell_legion_rogue_boarding_party',
'spell_legion_rogue_cannonball_barrage',
'spell_legion_rogue_cannonball_barrage_damage',
'spell_legion_rogue_cheat_death',
'spell_legion_rogue_cloak_of_shadows',
'spell_legion_rogue_combat_potency',
'spell_legion_rogue_control_is_king',
'spell_legion_rogue_creeping_venom',
'spell_legion_rogue_curse_of_the_dreadblades',
'spell_legion_rogue_dagger_in_the_dark',
'spell_legion_rogue_deadly_poison',
'spell_legion_rogue_death_from_above',
'spell_legion_rogue_death_from_above_damage',
'spell_legion_rogue_death_from_above_jump_target',
'spell_legion_rogue_death_from_above_pre_jump',
'spell_legion_rogue_envenom',
'spell_legion_rogue_eviscerate',
'spell_legion_rogue_exsanguinate',
'spell_legion_rogue_fan_of_knives',
'spell_legion_rogue_faster_than_the_light',
'spell_legion_rogue_finality',
'spell_legion_rogue_garrote',
'spell_legion_rogue_ghostly_shell_dummy_aura',
'spell_legion_rogue_grappling_hook',
'spell_legion_rogue_greed_damage',
'spell_legion_rogue_greed_proc',
'spell_legion_rogue_honor_among_thieves',
'spell_legion_rogue_internal_bleeding',
'spell_legion_rogue_kidney_shot',
'spell_legion_rogue_killing_spree',
'spell_legion_rogue_kingsbane_pct_mod',
'spell_legion_rogue_marked_for_death_trig',
'spell_legion_rogue_mastery_main_gauche',
'spell_legion_rogue_mind_numbing_poison',
'spell_legion_rogue_nightblade',
'spell_legion_rogue_parley',
'spell_legion_rogue_poisoned_knife',
'spell_legion_rogue_relentless_strikes',
'spell_legion_rogue_riposte',
'spell_legion_rogue_roll_the_bones',
'spell_legion_rogue_run_through',
'spell_legion_rogue_rupture',
'spell_legion_rogue_saber_slash',
'spell_legion_rogue_seal_fate',
'spell_legion_rogue_shadow_dance',
'spell_legion_rogue_shadow_nova',
'spell_legion_rogue_shadow_techniques',
'spell_legion_rogue_shadowstep',
'spell_legion_rogue_shadowstrike',
'spell_legion_rogue_shuriken_storm',
'spell_legion_rogue_stealth',
'spell_legion_rogue_stealth',
'spell_legion_rogue_subterfuge',
'spell_legion_rogue_vanish',
'spell_legion_rogue_vanish_aura',
'spell_legion_rogue_venomous_wounds',
'spell_legion_rogue_weaponmaster',
'spell_legion_rogue_from_the_shadows_buff',
'spell_legion_rogue_from_the_shadows',
'spell_legion_rogue_surge_of_toxin',
'spell_legion_rogue_take_your_cut',
'spell_legion_rogue_take_your_cut_bonus',
'spell_legion_rogue_true_bearing',
'spell_legion_rogue_tricks_of_the_trade',
'spell_legion_rogue_turn_the_tables',
'spell_legion_rogue_turn_the_tables_periodic',
'spell_legion_rogue_sprint',
'spell_legion_rogue_deepening_shadows',
'spell_legion_rogue_unfair_advantage',
'spell_legion_rogue_lethal_poisons',
'spell_legion_rogue_cold_blood',
'spell_legion_rogue_shadowy_duel',
'spell_legion_rogue_pistol_shot',
'spell_legion_rogue_bribe',
'spell_legion_rogue_ruthlessness'
);

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(193539, 'spell_legion_rogue_alacrity'), 
(53,     'spell_legion_rogue_backstab'),
(192657, 'spell_legion_rogue_bag_of_tricks'),
(199804, 'spell_legion_rogue_between_the_eyes'),
(13877,  'spell_legion_rogue_blade_flurry'),
(22482,  'spell_legion_rogue_blade_flurry_chain'),
(2094,   'spell_legion_rogue_blind'),
(202895, 'spell_legion_rogue_blunderbuss'),
(209754, 'spell_legion_rogue_boarding_party'),
(185767, 'spell_legion_rogue_cannonball_barrage'),
(185779, 'spell_legion_rogue_cannonball_barrage_damage'),
(31230,  'spell_legion_rogue_cheat_death'),
(31224,  'spell_legion_rogue_cloak_of_shadows'),
(35551,  'spell_legion_rogue_combat_potency'),
(212219, 'spell_legion_rogue_control_is_king'),
(213981, 'spell_legion_rogue_cold_blood'),
(198097, 'spell_legion_rogue_creeping_venom'),
(2098,   'spell_legion_rogue_curse_of_the_dreadblades'),
(193316, 'spell_legion_rogue_curse_of_the_dreadblades'),
(199804, 'spell_legion_rogue_curse_of_the_dreadblades'),
(198675, 'spell_legion_rogue_dagger_in_the_dark'),
(2818,   'spell_legion_rogue_deadly_poison'),
(152150, 'spell_legion_rogue_death_from_above'),
(178070, 'spell_legion_rogue_death_from_above_damage'),
(156327, 'spell_legion_rogue_death_from_above_jump_target'),
(178236, 'spell_legion_rogue_death_from_above_pre_jump'),
(32645,  'spell_legion_rogue_envenom'),
(196819, 'spell_legion_rogue_eviscerate'),
(200806, 'spell_legion_rogue_exsanguinate'),
(51723,  'spell_legion_rogue_fan_of_knives'),
(197270, 'spell_legion_rogue_faster_than_the_light'),
(197406, 'spell_legion_rogue_finality'),
(703,    'spell_legion_rogue_garrote'),
(202534, 'spell_legion_rogue_ghostly_shell_dummy_aura'),
(195457, 'spell_legion_rogue_grappling_hook'),
(202822, 'spell_legion_rogue_greed_damage'),
(202820, 'spell_legion_rogue_greed_proc'),
(198031, 'spell_legion_rogue_honor_among_thieves'),
(154953, 'spell_legion_rogue_internal_bleeding'),
(408,    'spell_legion_rogue_kidney_shot'),
(51690,  'spell_legion_rogue_killing_spree'),
(192853, 'spell_legion_rogue_kingsbane_pct_mod'),
(2818,   'spell_legion_rogue_lethal_poisons'),
(200803, 'spell_legion_rogue_lethal_poisons'),
(140149, 'spell_legion_rogue_marked_for_death_trig'),
(76806,  'spell_legion_rogue_mastery_main_gauche'),
(197051, 'spell_legion_rogue_mind_numbing_poison'),
(195452, 'spell_legion_rogue_nightblade'),
(199743, 'spell_legion_rogue_parley'),
(185565, 'spell_legion_rogue_poisoned_knife'),
(58423,  'spell_legion_rogue_relentless_strikes'),
(199754, 'spell_legion_rogue_riposte'),
(193316, 'spell_legion_rogue_roll_the_bones'),
(2098,   'spell_legion_rogue_run_through'),
(1943,   'spell_legion_rogue_rupture'),
(193315, 'spell_legion_rogue_saber_slash'),
(14190,  'spell_legion_rogue_seal_fate'),
(185422, 'spell_legion_rogue_shadow_dance'),
(209781, 'spell_legion_rogue_shadow_nova'),
(196912, 'spell_legion_rogue_shadow_techniques'),
(36554,  'spell_legion_rogue_shadowstep'),
(185438, 'spell_legion_rogue_shadowstrike'),
(197835, 'spell_legion_rogue_shuriken_storm'),
(1784,   'spell_legion_rogue_stealth'),
(115191, 'spell_legion_rogue_stealth'),
(115192, 'spell_legion_rogue_subterfuge'),
(1856,   'spell_legion_rogue_vanish'),
(11327,  'spell_legion_rogue_vanish_aura'),
(79134,  'spell_legion_rogue_venomous_wounds'),
(193537, 'spell_legion_rogue_weaponmaster'),
(192428, 'spell_legion_rogue_from_the_shadows'),
(192432, 'spell_legion_rogue_from_the_shadows_buff'),
(192424, 'spell_legion_rogue_surge_of_toxin'),
(5171,   'spell_legion_rogue_take_your_cut'),
(193316, 'spell_legion_rogue_take_your_cut'),
(198368, 'spell_legion_rogue_take_your_cut_bonus'),
(193359, 'spell_legion_rogue_true_bearing'),
(57934,  'spell_legion_rogue_tricks_of_the_trade'),
(198020, 'spell_legion_rogue_turn_the_tables'),
(198023, 'spell_legion_rogue_turn_the_tables_periodic'),
(2983,   'spell_legion_rogue_sprint'),
(185314, 'spell_legion_rogue_deepening_shadows'),
(206317, 'spell_legion_rogue_unfair_advantage'),
(207736, 'spell_legion_rogue_shadowy_duel'),
(185763, 'spell_legion_rogue_pistol_shot'),
(199740, 'spell_legion_rogue_bribe'),
(14161, 'spell_legion_rogue_ruthlessness');

DELETE FROM `spell_proc` WHERE `SpellId` IN(14190, 35551, 76806, 79134, 192384, 192424, 192428, 192657, 192923, 193539, 193640, 195627, 197239, 197610, 198031, 199754, 202533, 202534, 202631, 
202665, 202753, 202820, 202848, 206317, 209752, 212035, 213981, 209781, 197406, 185314);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellPhaseMask`, `HitMask`) VALUES
(14190,         0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x0,           0x2,  0x2),
(35551,         0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x800014,      0x2,  0x0),
(76806,         0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x400154,      0x2,  0x0),
(79134,         0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x250000,      0x2,  0x0),
(192384,        0x0,        8,      0x0,            0x1000000,  0x0,           0x0,         0x11000,       0x2,  0x0),
(192424,        0x0,        8,      0x320000,       0x8,        0x12000000,    0x138010,    0x0,           0x2,  0x0),
(192428,        0x0,        8,      0x0,            0x1000000,  0x0,           0x0,         0x0,           0x2,  0x0),
(192657,        0x0,        8,      0x100000,       0x8,        0x0,           0x0,         0x0,           0x2,  0x0),
(192923,        0x0,        8,      0x100000,       0x0,        0x0,           0x0,         0x0,           0x2,  0x0),
(193539,        0x0,        8,      0x320000,       0x8,        0x12000000,    0x138010,    0x0,           0x2,  0x0),
(193640,        0x0,        8,      0x320000,       0x8,        0x12000000,    0x138010,    0x0,           0x2,  0x0),
(195627,        0x0,        8,      0x0,            0x0,        0x0,           0x20,        0x0,           0x2,  0x0),
(197239,        0x0,        8,      0x400,          0x0,        0x0,           0x100,       0x0,           0x2,  0x0),
(197610,        0x0,        8,      0x0,            0x0,        0x0,           0x80000000,  0x0,           0x2,  0x0),
(198031,        0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x0,           0x2,  0x2),
(199754,        0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x0,           0x0, 0x20),
(202533,        0x1,        8,      0x0,            0x10000,    0x0,           0x0,         0x0,           0x2,  0x0),
(202534,        0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x0,           0x0,  0x4),
(202631,        0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x0,           0x0, 0x20),
(202665,        0x0,        8,      0x4000200,      0x0,        0x0,           0x24,        0x0,           0x4,  0x0),
(202753,        0x0,        8,      0x200,          0x0,        0x0,           0x0,         0x0,           0x2,  0x0),
(202820,        0x0,        8,      0x0,            0x0,        0x0,           0x10000,     0x0,           0x2,  0x0),
(202848,        0x0,        8,      0x0,            0x0,        0x0,           0x20,        0x0,           0x2,  0x0),
(206317,        0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x0,           0x2,  0x2),
(209752,        0x1,        8,      0x0,            0x0,        0x0,           0x8000,      0x0,           0x1,  0x0),
(212035,        0x0,        8,      0x0,            0x0,        0x0,           0x8000,      0x0,           0x4,  0x0),
(213981,        0x0,        8,      0x800400,       0x0,        0x0,           0x100,       0x0,           0x2,  0x0),
(209781,        0x0,        8,      0x000400,       0x0,        0x0,           0x80000100,  0x0,           0x1,  0x0),
(197406,        0x0,        8,      0x020000,       0x0,        0x0,           0x00000010,  0x0,           0x1,  0x0),
(185314,        0x0,        8,      0x0,            0x0,        0x0,           0x0,         0x0,           0x4,  0x0);

DELETE FROM `disables` WHERE `sourceType`=0 AND `entry` = 156327;
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) VALUES 
(0, 156327, 64, '', '', 'Ignore LOS on Death from Above Jump (Rogue)');

DELETE FROM `spell_group` WHERE `id`=1100;
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES 
(1100, 2823   ),
(1100, 8679   ),
(1100, 200802 );

DELETE FROM `spell_group_stack_rules` WHERE `group_id`=1100;
INSERT INTO `spell_group_stack_rules` (`group_id`, `stack_rule`) VALUES 
(1100, 1);

DELETE FROM `spell_linked_spell` WHERE `spell_trigger`= 185313 AND `spell_effect`= 185422;
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`= 51723 AND `spell_effect`= 234278;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(185313, 185422, 0, 'Rogue - Legion Shadowdance triggers Shadowdance effect'),
(51723, 234278, 0, 'Rogue - Fan of Knives energize effect');

DELETE FROM spell_custom_attr WHERE entry IN (53, 703);

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (4928, 5410, 6952, 6951);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `MoveCurveId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `VerifiedBuild`) VALUES
(4928, 9645, 0, 0, 0, 0, 0, 0, 3000, 23420), -- SpellId : 192661
(5410, 50009, 0, 0, 0, 0, 0, 0, 0, 0), -- SpellId : 198032
(6952, 50010, 0, 0, 0, 0, 0, 0, 0, 0), -- SpellId : 198032
(6951, 11451, 0, 0, 0, 0, 0, 0, 5000, 23420); -- SpellId : 212182

UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_legion_rogue_poison_bomb' WHERE `Id`= 9645;

DELETE FROM `areatrigger_template` WHERE `Id` IN (50009, 50010);
INSERT INTO `areatrigger_template` (`Id`, `Flags`, `Data0`, `Data1`) VALUES 
(50009, 4, 15, 15),
(50010, 4, 40, 40);

DELETE FROM `areatrigger_template` WHERE `Id` = 11451;
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `VerifiedBuild`) VALUES
(11451, 4, 0, 8, 8, 4, 4, 1, 1, 23420);

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (50009, 50010, 11451);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES 
(50009, 1, 198031, 3),
(50010, 1, 212219, 3),
(11451, 1, 212183, 0);


