DELETE FROM `quest_offer_reward` WHERE `ID` IN (44495 /*Leyline Master*/, 44493 /*Leyline Proficiency*/, 43588 /*Leyline Feed: Kel'balor*/, 41499 /*Squid Out of Water*/, 41001 /*Shatter the Sashj'tar*/, 40336 /*Turning the Tidemistress*/, 41709 /*Breaking Down the Big Guns*/, 41426 /*Against Their Will*/, 41409 /*Timing Is Everything*/, 41425 /*Sunken Foes*/, 41618 /*The Seawarden*/, 40364 /*Bubble Trouble*/, 41606 /*Finding Brytag*/, 41410 /*Dry Powder*/, 40927 /*Jandvik's Last Hope*/, 41034 /*Testing the Waters*/, 40334 /*Fisherman's Tonic*/, 40331 /*Bite of the Sashj'tar*/, 40320 /*Band of Blood Brothers*/, 40332 /*Beach Bonfire*/, 40908 /*Jarl Come Back Now*/, 40907 /*Removing Obstacles*/, 43587 /*Leyline Feed: Elor'shan*/, 43593 /*Leyline Feed: Falanaar Depths*/, 43592 /*Leyline Feed: Falanaar Arcway*/, 44492 /*Leyline Apprentice*/, 43590 /*Leyline Feed: Ley Station Moonfall*/, 43591 /*Leyline Feed: Ley Station Aethenar*/, 41108 /*Rain Death Upon Them*/, 40412 /*Azoran Must Die*/, 41098 /*Shard of Kozak*/, 41097 /*Shard of Vorgos*/, 40929 /*Symbols of Power*/, 40328 /*A Fate Worse Than Dying*/, 40901 /*Grimwing the Devourer*/, 40898 /*Fresh Meat*/, 40307 /*Glaive Circumstances*/, 40297 /*Lyana Darksorrow*/, 43995 /*Feed Valtrois*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(44495, 0, 0, 0, 0, 0, 0, 0, 0, 'Perhaps there is hope for outlanders after all.', 22566), -- Leyline Master
(44493, 0, 0, 0, 0, 0, 0, 0, 0, 'Do your best to pick this up the first time - I do hate repeating myself.', 22566), -- Leyline Proficiency
(43588, 0, 0, 0, 0, 0, 0, 0, 0, '<As you imbue the feed with the mana the structure stirs to life. Your skin tingles as the surrounding air becomes charged with arcane power.>', 22566), -- Leyline Feed: Kel'balor
(41499, 0, 0, 0, 0, 0, 0, 0, 0, 'The naga will not be taking any more of my kin!', 22566), -- Squid Out of Water
(41001, 0, 0, 0, 0, 0, 0, 0, 0, 'We may have finally overcome this naga threat.', 22566), -- Shatter the Sashj'tar
(40336, 0, 0, 0, 0, 0, 0, 0, 0, 'You have done it, $n. You have slain the Tidemistress and saved Jandvik from the Sashj''tar.', 22566), -- Turning the Tidemistress
(41709, 0, 0, 0, 0, 0, 0, 0, 0, 'You have all the parts that you will need... two in some cases.', 22566), -- Breaking Down the Big Guns
(41426, 0, 0, 0, 0, 0, 0, 0, 0, 'I must express gratitude for the vrykul you have saved this day.', 22566), -- Against Their Will
(41409, 0, 0, 0, 0, 0, 0, 0, 0, 'Fjolrik died in honor of Jandvik. We will remember his bravery.', 22566), -- Timing Is Everything
(41425, 0, 0, 0, 0, 0, 0, 0, 0, 'The Sashj''tar forces are far greater than I imagined.', 22566), -- Sunken Foes
(41618, 0, 0, 0, 0, 0, 0, 0, 0, 'I only wish that I had the chance to get to him first.$B$BBah! His body can rot in these waters for all I care.', 22566), -- The Seawarden
(40364, 0, 0, 0, 0, 0, 0, 0, 0, 'There is no glory or honor in being digested.', 22566), -- Bubble Trouble
(41606, 0, 0, 0, 0, 0, 0, 0, 0, 'I do not know who you are, but you are not a sea giant, and that is enough for me.', 22566), -- Finding Brytag
(41410, 0, 0, 0, 0, 0, 0, 0, 0, '<Fjolrik looks at you weakly.>$B$BYou made it back in time...', 22566), -- Dry Powder
(40927, 0, 0, 0, 0, 0, 0, 0, 0, 'I do not know who you are, but I must thank you.', 22566), -- Jandvik's Last Hope
(41034, 0, 0, 0, 0, 0, 0, 0, 0, 'There are sea giants in the bay!?$B$BYou must tell Toryl about this grave news right away.', 22566), -- Testing the Waters
(40334, 0, 0, 0, 0, 0, 0, 0, 0, '<Brandolf mixes the concoction together.>$B$BWell, I never said that it would taste good. Bottom''s up.', 22566), -- Fisherman's Tonic
(40331, 0, 0, 0, 0, 0, 0, 0, 0, 'I will wear this in honor of Throndyr!', 22566), -- Bite of the Sashj'tar
(40320, 0, 0, 0, 0, 0, 0, 0, 0, 'My kin are safe. Let us see to these serpents!', 22566), -- Band of Blood Brothers
(40332, 0, 0, 0, 0, 0, 0, 0, 0, 'Hail Jarl Throndyr. May your soul find its place in Valhalas.', 22566), -- Beach Bonfire
(40908, 0, 0, 0, 0, 0, 0, 0, 0, '<Throndyr mutters through shallow breaths.>$B$BPoison... runs through... my body.', 22566), -- Jarl Come Back Now
(40907, 0, 0, 0, 0, 0, 0, 0, 0, 'With my adversaries gone, I can claim leadership of Jandvik unopposed.', 22566), -- Removing Obstacles
(43587, 0, 0, 0, 0, 0, 0, 0, 0, '<As you imbue the feed with the mana the structure stirs to life. Your skin tingles as the surrounding air becomes charged with arcane power.>', 22566), -- Leyline Feed: Elor'shan
(43593, 0, 0, 0, 0, 0, 0, 0, 0, '<As you imbue the feed with the mana the structure stirs to life. Your skin tingles as the surrounding air becomes charged with arcane power.>', 22566), -- Leyline Feed: Falanaar Depths
(43592, 0, 0, 0, 0, 0, 0, 0, 0, '<As you imbue the feed with the mana the structure stirs to life. Your skin tingles as the surrounding air becomes charged with arcane power.>', 22566), -- Leyline Feed: Falanaar Arcway
(44492, 0, 0, 0, 0, 0, 0, 0, 0, 'Now listen closely.', 22566), -- Leyline Apprentice
(43590, 0, 0, 0, 0, 0, 0, 0, 0, '<As you place the broken piece onto the pillar and begin infusing it with mana, arcane energy begins to repair the break. It slowly begins to turn as the leyline is activated.>', 22566), -- Leyline Feed: Ley Station Moonfall
(43591, 0, 0, 0, 0, 0, 0, 0, 0, '<As you insert the fragments back into the pillar the arcane energy begins repairing the cracks. You infuse the feed with ancient mana and the structure stirs to life.>', 22566), -- Leyline Feed: Ley Station Aethenar
(41108, 0, 0, 0, 0, 0, 0, 0, 0, 'Though more Nightborne still pour in through their portals, they will not soon forget the burn of the Moon Guard''s wrath.', 22566), -- Rain Death Upon Them
(40412, 0, 0, 0, 0, 0, 0, 0, 0, 'With Azoran slain, the Legion has been dealt a crippling blow that they will not soon recover from.$b$bYou have my respect, $n.', 22566), -- Azoran Must Die
(41098, 0, 0, 0, 0, 0, 0, 0, 0, '<The Shard of Kozak locks into position next to the portal. You can feel the dark writhing of fel energy surround you as it collects near the portal.>', 22566), -- Shard of Kozak
(41097, 0, 0, 0, 0, 0, 0, 0, 0, '<The Shard of Vorgos locks into position next to the portal. Swirling fel energy collects around the shard, filling the room with an ominous, dark presence.>', 22566), -- Shard of Vorgos
(40929, 0, 0, 0, 0, 0, 0, 0, 0, 'I see you have returned with the emblems. Excellent.$b$bLet''s see how this wyrmtongue reacts when we show him what we''ve made of his comrades.', 22566), -- Symbols of Power
(40328, 0, 0, 0, 0, 0, 0, 0, 0, 'The destruction of the soul is a fate far worse than death. You have done a great service to those you saved.', 22566), -- A Fate Worse Than Dying
(40901, 0, 0, 0, 0, 0, 0, 0, 0, 'Grimwing was a formidable demon... I must say I am impressed with your skill, $n.$b$bThere are many more demons here to kill. It is good to have someone capable at my side.', 22566), -- Grimwing the Devourer
(40898, 0, 0, 0, 0, 0, 0, 0, 0, 'I see that you have returned with the freshly carved remains of the fel basilisks.$b$bListen closely, and I will explain the purpose behind retrieving them...', 22566), -- Fresh Meat
(40307, 0, 0, 0, 0, 0, 0, 0, 0, 'Thank you. Now that my warglaives are returned, I will bring ruin upon Felsoul Hold.$b$bMy wrath is unending and my vengeance swift.', 22566), -- Glaive Circumstances
(40297, 0, 0, 0, 0, 0, 0, 0, 0, 'My name is Lyana. What is yours, $c?\n\nYou have my gratitude for releasing me.\n\nI do not know how long I have been held captive... but I sense an overwhelming number of demons nearby.\n\nWe have much to do here, you and I.', 22566), -- Lyana Darksorrow
(43995, 0, 0, 0, 0, 0, 0, 0, 0, '<She hesitates for a moment before accepting the crystals.>', 22566); -- Feed Valtrois

DELETE FROM `quest_poi_points` WHERE (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=42835 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=42834 AND `Idx1`=1 AND `Idx2`=1);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(42835, 1, 11, 1350, 3271, 22566), -- The Old Fashioned Way
(42835, 1, 10, 1342, 3279, 22566), -- The Old Fashioned Way
(42835, 1, 9, 1342, 3290, 22566), -- The Old Fashioned Way
(42835, 1, 8, 1342, 3304, 22566), -- The Old Fashioned Way
(42835, 1, 7, 1357, 3304, 22566), -- The Old Fashioned Way
(42835, 1, 6, 1368, 3304, 22566), -- The Old Fashioned Way
(42835, 1, 5, 1379, 3304, 22566), -- The Old Fashioned Way
(42835, 1, 4, 1386, 3293, 22566), -- The Old Fashioned Way
(42835, 1, 3, 1386, 3286, 22566), -- The Old Fashioned Way
(42835, 1, 2, 1383, 3271, 22566), -- The Old Fashioned Way
(42835, 1, 1, 1368, 3268, 22566), -- The Old Fashioned Way
(42834, 1, 11, 1420, 3452, 22566), -- Intense Concentration
(42834, 1, 10, 1420, 3470, 22566), -- Intense Concentration
(42834, 1, 9, 1434, 3489, 22566), -- Intense Concentration
(42834, 1, 8, 1453, 3504, 22566), -- Intense Concentration
(42834, 1, 7, 1464, 3504, 22566), -- Intense Concentration
(42834, 1, 6, 1486, 3504, 22566), -- Intense Concentration
(42834, 1, 5, 1501, 3493, 22566), -- Intense Concentration
(42834, 1, 4, 1512, 3482, 22566), -- Intense Concentration
(42834, 1, 3, 1516, 3463, 22566), -- Intense Concentration
(42834, 1, 2, 1501, 3437, 22566), -- Intense Concentration
(42834, 1, 1, 1445, 3434, 22566); -- Intense Concentration

DELETE FROM `quest_greeting` WHERE (`ID`=102845 AND `Type`=0) OR (`ID`=102410 AND `Type`=0) OR (`ID`=104406 AND `Type`=0) OR (`ID`=104630 AND `Type`=0) OR (`ID`=102334 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(102845, 0, 0, 0, 'We have the Sashj''tar on the run.', 22566), -- 102845
(102410, 0, 0, 0, 'Now is the time to destroy the Sashj''tar and rid Jandvik of their presence.', 22566), -- 102410
(104406, 0, 0, 0, 'I implore you sense of honor, stranger. There is no glory in being eaten by a sea giant.', 22566), -- 104406
(104630, 0, 0, 0, 'There... are... others...', 22566), -- 104630
(102334, 0, 0, 0, 'Throndyr was not just the leader of Jandvik, he was also an honorable vrykul. He taught me much and I always aspired to be as great as he was.', 22566); -- 102334

DELETE FROM `quest_details` WHERE `ID` IN (44495 /*Leyline Master*/, 44493 /*Leyline Proficiency*/, 43588 /*Leyline Feed: Kel'balor*/, 41499 /*Squid Out of Water*/, 41001 /*Shatter the Sashj'tar*/, 40336 /*Turning the Tidemistress*/, 41618 /*The Seawarden*/, 40364 /*Bubble Trouble*/, 41409 /*Timing Is Everything*/, 41410 /*Dry Powder*/, 41709 /*Breaking Down the Big Guns*/, 41425 /*Sunken Foes*/, 41606 /*Finding Brytag*/, 41426 /*Against Their Will*/, 40927 /*Jandvik's Last Hope*/, 41034 /*Testing the Waters*/, 40334 /*Fisherman's Tonic*/, 40331 /*Bite of the Sashj'tar*/, 40320 /*Band of Blood Brothers*/, 40332 /*Beach Bonfire*/, 40908 /*Jarl Come Back Now*/, 40907 /*Removing Obstacles*/, 43587 /*Leyline Feed: Elor'shan*/, 43593 /*Leyline Feed: Falanaar Depths*/, 43592 /*Leyline Feed: Falanaar Arcway*/, 44492 /*Leyline Apprentice*/, 43590 /*Leyline Feed: Ley Station Moonfall*/, 43591 /*Leyline Feed: Ley Station Aethenar*/, 41108 /*Rain Death Upon Them*/, 41109 /*Waiting for Revenge*/, 40883 /*Fate of the Guard*/, 40412 /*Azoran Must Die*/, 41098 /*Shard of Kozak*/, 41097 /*Shard of Vorgos*/, 40929 /*Symbols of Power*/, 40328 /*A Fate Worse Than Dying*/, 40901 /*Grimwing the Devourer*/, 40898 /*Fresh Meat*/, 40307 /*Glaive Circumstances*/, 40297 /*Lyana Darksorrow*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(44495, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Master
(44493, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Proficiency
(43588, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Feed: Kel'balor
(41499, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Squid Out of Water
(41001, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Shatter the Sashj'tar
(40336, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Turning the Tidemistress
(41618, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- The Seawarden
(40364, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Bubble Trouble
(41409, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Timing Is Everything
(41410, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Dry Powder
(41709, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Breaking Down the Big Guns
(41425, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Sunken Foes
(41606, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Finding Brytag
(41426, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Against Their Will
(40927, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Jandvik's Last Hope
(41034, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Testing the Waters
(40334, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Fisherman's Tonic
(40331, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Bite of the Sashj'tar
(40320, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Band of Blood Brothers
(40332, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Beach Bonfire
(40908, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Jarl Come Back Now
(40907, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Removing Obstacles
(43587, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Feed: Elor'shan
(43593, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Feed: Falanaar Depths
(43592, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Feed: Falanaar Arcway
(44492, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Apprentice
(43590, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Feed: Ley Station Moonfall
(43591, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Leyline Feed: Ley Station Aethenar
(41108, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Rain Death Upon Them
(41109, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Waiting for Revenge
(40883, 603, 1, 0, 0, 0, 0, 0, 0, 22566), -- Fate of the Guard
(40412, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Azoran Must Die
(41098, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Shard of Kozak
(41097, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Shard of Vorgos
(40929, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Symbols of Power
(40328, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- A Fate Worse Than Dying
(40901, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Grimwing the Devourer
(40898, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Fresh Meat
(40307, 0, 0, 0, 0, 0, 0, 0, 0, 22566), -- Glaive Circumstances
(40297, 0, 0, 0, 0, 0, 0, 0, 0, 22566); -- Lyana Darksorrow

DELETE FROM `quest_request_items` WHERE `ID` IN (42840 /*If Words Don't Work...*/, 42834 /*Intense Concentration*/, 43588 /*Leyline Feed: Kel'balor*/, 41709 /*Breaking Down the Big Guns*/, 40364 /*Bubble Trouble*/, 41410 /*Dry Powder*/, 40334 /*Fisherman's Tonic*/, 40331 /*Bite of the Sashj'tar*/, 40332 /*Beach Bonfire*/, 43587 /*Leyline Feed: Elor'shan*/, 38784 /*Leystone Seam Sample*/, 38797 /*Living Felslate Sample*/, 38777 /*Leystone Deposit Sample*/, 43593 /*Leyline Feed: Falanaar Depths*/, 43592 /*Leyline Feed: Falanaar Arcway*/, 43590 /*Leyline Feed: Ley Station Moonfall*/, 43591 /*Leyline Feed: Ley Station Aethenar*/, 41098 /*Shard of Kozak*/, 41097 /*Shard of Vorgos*/, 40929 /*Symbols of Power*/, 40901 /*Grimwing the Devourer*/, 40898 /*Fresh Meat*/, 40307 /*Glaive Circumstances*/, 43995 /*Feed Valtrois*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(42840, 0, 0, 0, 0, 'My price will not change. Do we have a deal?', 22566), -- If Words Don't Work...
(42834, 0, 0, 0, 0, 'Having trouble?', 22566), -- Intense Concentration
(43588, 0, 0, 0, 0, '<The leyline feed pulses softly.>', 22566), -- Leyline Feed: Kel'balor
(41709, 0, 0, 0, 0, 'The Tidemistress is a dangerous foe. We must be just as cunning to defeat her.', 22566), -- Breaking Down the Big Guns
(40364, 0, 0, 0, 0, 'Move quickly, $c! These sea giants have an insatiable appetite!', 22566), -- Bubble Trouble
(41410, 0, 0, 0, 0, 'Move quickly, $n.', 22566), -- Dry Powder
(40334, 0, 0, 0, 0, 'I will help you collect what we need.', 22566), -- Fisherman's Tonic
(40331, 0, 0, 0, 0, 'We will slay them all!', 22566), -- Bite of the Sashj'tar
(40332, 0, 0, 0, 0, 'Throndyr deserved a more honorable death.', 22566), -- Beach Bonfire
(43587, 0, 0, 0, 0, '<The leyline feed pulses softly.>', 22566), -- Leyline Feed: Elor'shan
(38784, 0, 0, 0, 0, 'What''ve ye brought me now?', 22566), -- Leystone Seam Sample
(38797, 0, 0, 0, 0, 'What''s that?', 22566), -- Living Felslate Sample
(38777, 0, 0, 0, 0, 'What''s that you''ve got there, $gboy:lass;?', 22566), -- Leystone Deposit Sample
(43593, 0, 0, 0, 0, '<The leyline feed pulses softly.>', 22566), -- Leyline Feed: Falanaar Depths
(43592, 0, 0, 0, 0, '<The leyline feed pulses softly.>', 22566), -- Leyline Feed: Falanaar Arcway
(43590, 0, 0, 0, 0, '<The feed pulses softly.>', 22566), -- Leyline Feed: Ley Station Moonfall
(43591, 0, 0, 0, 0, '<The leyline feed crackles as wild energy escapes the gaps in the column.>', 22566), -- Leyline Feed: Ley Station Aethenar
(41098, 0, 0, 0, 0, '<The Shard of Kozak tugs as if it wants to fly into place.>', 22566), -- Shard of Kozak
(41097, 0, 0, 0, 0, '<The Shard of Vargos pulses in your hand.>', 22566), -- Shard of Vorgos
(40929, 0, 0, 0, 0, 'Do you have the Legion Emblems?', 22566), -- Symbols of Power
(40901, 0, 0, 0, 0, 'What news do you bring of Grimwing the Devourer?', 22566), -- Grimwing the Devourer
(40898, 0, 0, 0, 0, 'Do you have the Fresh Fel-Flesh I require?', 22566), -- Fresh Meat
(40307, 0, 0, 0, 0, 'Do you have my glaives, $r?', 22566), -- Glaive Circumstances
(43995, 0, 0, 0, 0, '<Valtrois does not respond. Her expression betrays a deep pain welling within.>', 22566); -- Feed Valtrois

UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=2.25, `VerifiedBuild`=22566 WHERE `DisplayID`=66312;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=22566 WHERE `DisplayID`=62832;

DELETE FROM `npc_vendor` WHERE (`entry`=113516 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=113516 AND `item`=18567 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=113516 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=113516 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=113516 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=113516 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(113516, 6, 3857, 0, 0, 1, 0, 0, 22566), -- Coal
(113516, 5, 18567, 0, 0, 1, 0, 0, 22566), -- Elemental Flux
(113516, 4, 3466, 0, 0, 1, 0, 0, 22566), -- Strong Flux
(113516, 3, 2880, 0, 0, 1, 0, 0, 22566), -- Weak Flux
(113516, 2, 5956, 0, 0, 1, 0, 0, 22566), -- Blacksmith Hammer
(113516, 1, 2901, 0, 0, 1, 0, 0, 22566); -- Mining Pick

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=103610 AND `ID`=1) OR (`CreatureID`=100016 AND `ID`=1) OR (`CreatureID`=102840 AND `ID`=1) OR (`CreatureID`=102841 AND `ID`=1) OR (`CreatureID`=100949 AND `ID`=1) OR (`CreatureID`=100950 AND `ID`=1) OR (`CreatureID`=102334 AND `ID`=1) OR (`CreatureID`=103474 AND `ID`=1) OR (`CreatureID`=99544 AND `ID`=1) OR (`CreatureID`=100878 AND `ID`=1) OR (`CreatureID`=100823 AND `ID`=1) OR (`CreatureID`=99890 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(103610, 1, 1910, 0, 0), -- Sashj'tar Collier
(100016, 1, 5322, 0, 0), -- Fjolrik
(102840, 1, 121788, 0, 0), -- Commander Raz'jira
(102841, 1, 54820, 0, 0), -- Commander Malt'his
(100949, 1, 120477, 0, 0), -- Sashj'tar Lancer
(100950, 1, 121290, 0, 0), -- Sashj'tar Enchantress
(102334, 1, 37608, 0, 40540), -- Brandolf
(103474, 1, 121289, 0, 0), -- Sashj'tar Diviner
(99544, 1, 2695, 0, 34816), -- Brandolf
(100878, 1, 131718, 0, 0), -- Lyana Darksorrow
(100823, 1, 131718, 0, 0), -- Lyana Darksorrow
(99890, 1, 131718, 0, 0); -- Lyana Darksorrow

UPDATE `quest_template` SET `RewardXPDifficulty`=0, `RewardXPMultiplier`=0, `Flags`=3735808, `RewardFactionValue1`=3, `RewardFactionValue2`=3, `RewardFactionOverride1`=0, `RewardFactionOverride2`=0 WHERE `ID`=41828; -- WANTED: Bristlemaul
UPDATE `quest_template` SET `RewardXPDifficulty`=0, `RewardXPMultiplier`=0, `RewardFactionValue1`=3, `RewardFactionOverride1`=0 WHERE `ID`=41701; -- Gettin' Tuffer
UPDATE `quest_template` SET `RewardXPDifficulty`=0, `RewardXPMultiplier`=0, `Flags`=3735808, `RewardFactionValue1`=3, `RewardFactionValue2`=3, `RewardFactionOverride1`=0, `RewardFactionOverride2`=0 WHERE `ID`=41828; -- WANTED: Bristlemaul
UPDATE `quest_template` SET `RewardXPDifficulty`=0, `RewardXPMultiplier`=0, `RewardFactionValue1`=3, `RewardFactionOverride1`=0 WHERE `ID`=41701; -- Gettin' Tuffer

UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=22566 WHERE `entry`=100179; -- Willbreaker Incubator

DELETE FROM `npc_text` WHERE `ID` IN (28329 /*28329*/, 28323 /*28323*/, 29793 /*29793*/);
INSERT INTO `npc_text` (`ID`, `Probability0`, `Probability1`, `Probability2`, `Probability3`, `Probability4`, `Probability5`, `Probability6`, `Probability7`, `BroadcastTextId0`, `BroadcastTextId1`, `BroadcastTextId2`, `BroadcastTextId3`, `BroadcastTextId4`, `BroadcastTextId5`, `BroadcastTextId6`, `BroadcastTextId7`, `VerifiedBuild`) VALUES
(28329, 1, 0, 0, 0, 0, 0, 0, 0, 105180, 0, 0, 0, 0, 0, 0, 0, 22566), -- 28329
(28323, 1, 0, 0, 0, 0, 0, 0, 0, 105152, 0, 0, 0, 0, 0, 0, 0, 22566), -- 28323
(29793, 1, 0, 0, 0, 0, 0, 0, 0, 116407, 0, 0, 0, 0, 0, 0, 0, 22566); -- 29793
