DELETE FROM `command` WHERE `name` LIKE 'instance setdata';
DELETE FROM `command` WHERE `name` LIKE 'instance getdata';
DELETE FROM `command` WHERE `name` LIKE 'instance getdata64';

INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('instance setdata',     1065, 'Syntax: .instance setdata $type $data [$name]\r\nSets the data for the given type to a new value.\r\nIf no character name is provided, the current map will be used as target.'),
('instance getdata',     1063, 'Syntax: .instance getdata $type [$name]\r\nGets the data for the given type.\r\nIf no character name is provided, the current map will be used as target.'),
('instance getdata64',   1064, 'Syntax: .instance getdata64 $type [$name]\r\nGets the data for the given type.\r\nIf no character name is provided, the current map will be used as target.');
