DELETE FROM `scene_template` WHERE (`SceneId`=1365 AND `ScriptPackageID`=1681) OR (`SceneId`=1213 AND `ScriptPackageID`=1602) OR (`SceneId`=1266 AND `ScriptPackageID`=1617);
INSERT INTO `scene_template` (`SceneId`, `Flags`, `ScriptPackageID`) VALUES
(1365, 21, 1681),
(1213, 21, 1602),
(1266, 21, 1617);

DELETE FROM `quest_offer_reward` WHERE `ID` IN (40824 /*The Path of the Dreadscar*/, 40823 /*Rebuilding the Council*/, 40821 /*Power Overwhelming*/, 40731 /*The Heart of the Dreadscar*/, 43254 /*Ritual Ruination*/, 40623 /*The Dark Riders*/, 40611 /*The Fate of Deadwind*/, 40606 /*To Point the Way*/, 40604 /*Disturbing the Past*/, 40588 /*Following the Curse*/, 40495 /*Ulthalesh, the Deadwind Harvester*/, 44089 /*A Greater Arsenal*/, 42125 /*Dark Whispers*/, 42168 /*Looking into the Darkness*/, 42128 /*Ritual Reagents*/, 41797 /*Recruiting The Troops*/, 42603 /*Information at Any Cost*/, 42608 /*Rise, Champions*/, 41748 /*Champion: Ritssyn Flamescowl*/, 43984 /*The Tome Opens Again*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(40824, 0, 0, 0, 0, 0, 0, 0, 0, 'The plan is set, and the wheels are in motion.', 22624), -- The Path of the Dreadscar
(40823, 0, 0, 0, 0, 0, 0, 0, 0, 'I can hardly believe my eyes. For one warlock to control an entire army of demons is beyond what I thought the council was capable of. $B$BI only hope such power is enough to turn the tide of this war.', 22624), -- Rebuilding the Council
(40821, 0, 0, 0, 0, 0, 0, 0, 0, 'Yes, your weapon grows stronger! $B$BFriend cannot rest, though, for a new danger lies around the corner!', 22624), -- Power Overwhelming
(40731, 0, 0, 0, 0, 0, 0, 0, 0, 'The cruel Jagganoth is defeated!$B$BFriend is the new Overlord of the Dreadscar! $B$BThis place will be friend''s new home. With the Dreadscar under your control, you can come and go as you please!', 22624), -- The Heart of the Dreadscar
(43254, 1, 0, 0, 0, 0, 0, 0, 0, 'Calydus not believe that worked! Ritual messed up, Shadow Council wiped out, and we not dead!$b$bOh, and you have Scepter! Big win for friend!', 22624), -- Ritual Ruination
(40623, 0, 0, 0, 0, 0, 0, 0, 0, 'Today was a great blow against the Dark Riders, and a victory for the people of Duskwood. $B$BThey are in your debt, as am I.', 22624), -- The Dark Riders
(40611, 0, 0, 0, 0, 0, 0, 0, 0, 'It looks like it worked, and the compass is pointing toward the river. $B$BCould it be..?', 22624), -- The Fate of Deadwind
(40606, 0, 0, 0, 0, 0, 0, 0, 0, 'There is definitely something wrong with this compass. $B$BIt seems to be trying to point in all directions at once.', 22624), -- To Point the Way
(40604, 0, 0, 0, 0, 0, 0, 0, 0, 'Otherwise an ordinary looking journal, this book hums with dark energy as you approach it.$B$BA quick scan of the writing reveals this as the journal of Ariden, and chronicles his time in Deadwind Pass. Toward the end of the journal, the writing begins to change, and the tone becomes noticeably darker. It constantly refers to "the curse" and of the endless desire to possess things of great power.$B$BOne such entry catches your eye as you glance through.', 22624), -- Disturbing the Past
(40588, 0, 0, 0, 0, 0, 0, 0, 0, 'It''s strange that the Dark Riders haven''t followed us here, but I will take whatever boon I can get.$B$BI believe the key to finding the Dark Riders is here somewhere. Let us begin our search.', 22624), -- Following the Curse
(40495, 0, 0, 0, 0, 0, 0, 0, 0, 'I am left with little choice. I cannot face another Dark Rider ambush alone, and you are obviously formidable.$B$BI will help you find the weapon you seek, but only if you will allow me to return the others in their possession to their rightful place.$B$BDo we have a deal, warlock?', 22624), -- Ulthalesh, the Deadwind Harvester
(44089, 0, 0, 0, 0, 0, 0, 0, 0, 'Friend is very wise! You can never have too many weapons!', 22624), -- A Greater Arsenal
(42125, 1, 0, 0, 0, 0, 0, 0, 0, 'Friend was able to get the skull?$b$bYes, Calydus senses it! Keep its gaze turned away from Calydus, friend. I gladly work for you without compulsion.', 22624), -- Dark Whispers
(42168, 1, 0, 0, 0, 0, 0, 0, 0, 'Yes, Felsoul Hold! Calydus knows where that is!$b$bCalydus can make a path that connects to one of the portals there!', 22624), -- Looking into the Darkness
(42128, 1, 0, 0, 0, 0, 0, 0, 0, 'Very good! Now, we''re ready to begin.', 22624), -- Ritual Reagents
(41797, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. These troops will be invaluable in our defense of the Broken Isles. Deploy them on missions, just as you would with our champions.', 22624), -- Recruiting The Troops
(42603, 1, 0, 0, 0, 0, 0, 0, 0, 'Ritssyn and Calydus were successful in getting information, though we don''t know how reliable it is, considering how it was obtained.', 22624), -- Information at Any Cost
(42608, 1, 0, 0, 0, 0, 0, 0, 0, 'It says a lot that Ritssyn would give up his position and look to you for leadership. He must believe that you are the key to rebuilding the council.', 22624), -- Rise, Champions
(41748, 1, 0, 0, 0, 0, 0, 0, 0, 'Rebuilding the council is our key to defeating the Legion, but it''s a task that cannot be completed alone.$B$BYou could use someone at your side, someone who''s familiar with the burden of leadership.$B$BI would be honored to serve as Second. Through your power and guidance, we can rebuild.', 22624), -- Champion: Ritssyn Flamescowl
(43984, 1, 0, 0, 0, 0, 0, 0, 0, 'Yes, very good choice!', 22624); -- The Tome Opens Again

DELETE FROM `quest_details` WHERE `ID` IN (40824 /*The Path of the Dreadscar*/, 40823 /*Rebuilding the Council*/, 40821 /*Power Overwhelming*/, 40731 /*The Heart of the Dreadscar*/, 40623 /*The Dark Riders*/, 40611 /*The Fate of Deadwind*/, 40606 /*To Point the Way*/, 40604 /*Disturbing the Past*/, 40588 /*Following the Curse*/, 40495 /*Ulthalesh, the Deadwind Harvester*/, 44089 /*A Greater Arsenal*/, 42125 /*Dark Whispers*/, 42168 /*Looking into the Darkness*/, 42602 /*Troops in the Field*/, 41797 /*Recruiting The Troops*/, 42603 /*Information at Any Cost*/, 42128 /*Ritual Reagents*/, 40013 /*Aethril Sample*/, 39325 /*Get Your Mix On*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(40824, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- The Path of the Dreadscar
(40823, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Rebuilding the Council
(40821, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Power Overwhelming
(40731, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- The Heart of the Dreadscar
(40623, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- The Dark Riders
(40611, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- The Fate of Deadwind
(40606, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- To Point the Way
(40604, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Disturbing the Past
(40588, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Following the Curse
(40495, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Ulthalesh, the Deadwind Harvester
(44089, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- A Greater Arsenal
(42125, 1, 0, 0, 0, 0, 0, 0, 0, 22624), -- Dark Whispers
(42168, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Looking into the Darkness
(42602, 1, 0, 0, 0, 0, 0, 0, 0, 22624), -- Troops in the Field
(41797, 1, 0, 0, 0, 0, 0, 0, 0, 22624), -- Recruiting The Troops
(42603, 1, 0, 0, 0, 0, 0, 0, 0, 22624), -- Information at Any Cost
(42128, 0, 0, 0, 0, 0, 0, 0, 0, 22624), -- Ritual Reagents
(40013, 1, 1, 0, 0, 0, 0, 0, 0, 22624), -- Aethril Sample
(39325, 1, 0, 0, 0, 0, 0, 0, 0, 22624); -- Get Your Mix On

DELETE FROM `quest_request_items` WHERE `ID` IN (40823 /*Rebuilding the Council*/, 40606 /*To Point the Way*/, 42168 /*Looking into the Darkness*/, 42128 /*Ritual Reagents*/, 37957 /*Runas the Shamed*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(40823, 0, 0, 0, 0, 'I will not rest until the council has been rebuilt.', 22624), -- Rebuilding the Council
(40606, 0, 0, 0, 0, 'Is that the one?', 22624), -- To Point the Way
(42168, 0, 0, 0, 0, 'The ritual circle is ready, friend.', 22624), -- Looking into the Darkness
(42128, 0, 0, 0, 0, 'Did you obtain everything, friend?', 22624), -- Ritual Reagents
(37957, 0, 0, 0, 0, 'Ah, Stella. Always so quick to act. She is still young.', 22624); -- Runas the Shamed

DELETE FROM `creature_model_info` WHERE `DisplayID`=62215;
INSERT INTO `creature_model_info` (`DisplayID`, `BoundingRadius`, `CombatReach`, `DisplayID_Other_Gender`, `VerifiedBuild`) VALUES
(62215, 1.990192, 2.3, 0, 22624);
