DELETE FROM `quest_offer_reward` WHERE `ID` IN (41351 /*Supplies Needed: Stonehide Leather*/, 40029 /*Fjarnskaggl Sample*/, 43517 /*Fallen Power*/, 45160 /*-Unknown-*/, 42170 /*The Dreamweavers*/, 45296 /*-Unknown-*/, 40753 /*Blingtron 6000*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(41351, 2, 0, 0, 0, 0, 0, 0, 0, 'You couldn''t have arrived at a better time, $c. Our people were running short on supplies.', 23420), -- Supplies Needed: Stonehide Leather
(40029, 1, 1, 0, 0, 0, 0, 0, 0, 'What a fantastic find, $n. This must be the celebrated vrykul herb, fjarnskaggl. I''ve only read about it in books!$b$bHere, I''ll share what I remember with you...', 23420), -- Fjarnskaggl Sample
(43517, 1, 0, 0, 0, 0, 0, 0, 0, 'The ancient is at peace once more.$B$BMay his sacrifice not be in vain.', 23420), -- Fallen Power
(45160, 6, 0, 0, 0, 0, 0, 0, 0, 'I trust you obtained all the ancient mana we need to infuse the cube?', 23420), -- -Unknown-
(42170, 0, 0, 0, 0, 0, 0, 0, 0, 'The Dreamweavers thank you.', 23420), -- The Dreamweavers
(45296, 1, 1, 0, 0, 0, 0, 0, 0, 'Nightbane has been defeated! I''m in awe!$b$bMany years have passed since the blue dragon Arcanagos was slain by the corrupted Medivh. Archmage Alturus sought out the remains of the fallen dragon in an effort to study the Guardian''s power. $b$bIn retrospect, maybe not the best idea. $b$bYour help has been invaluable.$b$bI still have a lot of work ahead to clean up Karazhan, but for now I dare ask no more of you. Thank you again for your assistance, $n.', 23420), -- -Unknown-
(40753, 0, 0, 0, 0, 0, 0, 0, 0, 'While I am programmed for gift distribution, my observations indicate that you are programmed for gift reception.$b$bShall we engage in a mutually-favorable transaction?', 23420); -- Blingtron 6000

DELETE FROM `quest_details` WHERE `ID` IN (45088 /*-Unknown-*/, 40029 /*Fjarnskaggl Sample*/, 45238 /*-Unknown-*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(45088, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(40029, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Fjarnskaggl Sample
(45238, 1, 6, 0, 0, 0, 0, 0, 0, 23420); -- -Unknown-

DELETE FROM `quest_request_items` WHERE `ID` IN (41351 /*Supplies Needed: Stonehide Leather*/, 40029 /*Fjarnskaggl Sample*/, 40035 /*The Gentlest Touch*/, 43517 /*Fallen Power*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(41351, 0, 0, 0, 0, 'Yes, outlander?', 23420), -- Supplies Needed: Stonehide Leather
(40029, 1, 0, 0, 0, 'What do you have there, herbalist?', 23420), -- Fjarnskaggl Sample
(40035, 1, 0, 0, 0, 'Keep at it, herbalist.', 23420), -- The Gentlest Touch
(43517, 1, 0, 0, 0, 'You have the essence?', 23420); -- Fallen Power

DELETE FROM `creature_model_info` WHERE `DisplayID` IN (75610, 75606);
INSERT INTO `creature_model_info` (`DisplayID`, `BoundingRadius`, `CombatReach`, `DisplayID_Other_Gender`, `VerifiedBuild`) VALUES
(75610, 1.05, 8.75, 0, 23420),
(75606, 0.9, 7.5, 0, 23420);

UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72434;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=72435;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=45436;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=73284;
UPDATE `creature_model_info` SET `BoundingRadius`=0.416, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=73156;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=1.1, `VerifiedBuild`=23420 WHERE `DisplayID`=73262;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73607;
UPDATE `creature_model_info` SET `BoundingRadius`=0.775, `CombatReach`=2.75, `VerifiedBuild`=23420 WHERE `DisplayID`=73613;
UPDATE `creature_model_info` SET `BoundingRadius`=0.312, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=73195;
UPDATE `creature_model_info` SET `BoundingRadius`=0.312, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=73197;
UPDATE `creature_model_info` SET `BoundingRadius`=0.15, `CombatReach`=4.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73372;
UPDATE `creature_model_info` SET `BoundingRadius`=3.906734, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=27571;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8227149, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69625;
UPDATE `creature_model_info` SET `BoundingRadius`=3, `CombatReach`=14.25, `VerifiedBuild`=23420 WHERE `DisplayID`=69635;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=8, `VerifiedBuild`=23420 WHERE `DisplayID`=69232;
UPDATE `creature_model_info` SET `BoundingRadius`=1.360725, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74382;
UPDATE `creature_model_info` SET `BoundingRadius`=1.360725, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69315;
UPDATE `creature_model_info` SET `BoundingRadius`=1.625, `CombatReach`=3.25, `VerifiedBuild`=23420 WHERE `DisplayID`=70158;
UPDATE `creature_model_info` SET `BoundingRadius`=5, `CombatReach`=6, `VerifiedBuild`=23420 WHERE `DisplayID`=71214;
UPDATE `creature_model_info` SET `BoundingRadius`=2.232307, `CombatReach`=1.3, `VerifiedBuild`=23420 WHERE `DisplayID`=71423;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=44466;
UPDATE `creature_model_info` SET `BoundingRadius`=1.346166, `CombatReach`=4.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68963;
UPDATE `creature_model_info` SET `BoundingRadius`=1.800289, `CombatReach`=2.625, `VerifiedBuild`=23420 WHERE `DisplayID`=72124;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9797121, `CombatReach`=1.725, `VerifiedBuild`=23420 WHERE `DisplayID`=66586;
UPDATE `creature_model_info` SET `BoundingRadius`=15.58945, `CombatReach`=24.5, `VerifiedBuild`=23420 WHERE `DisplayID`=68323;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=71769;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=71770;
UPDATE `creature_model_info` SET `BoundingRadius`=3.451858, `CombatReach`=4, `VerifiedBuild`=23420 WHERE `DisplayID`=72822;
UPDATE `creature_model_info` SET `BoundingRadius`=1.612829, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=70367;
UPDATE `creature_model_info` SET `BoundingRadius`=1.469568, `CombatReach`=2.5875, `VerifiedBuild`=23420 WHERE `DisplayID`=71739;
UPDATE `creature_model_info` SET `BoundingRadius`=1.468884, `CombatReach`=2.32875, `VerifiedBuild`=23420 WHERE `DisplayID`=71737;
UPDATE `creature_model_info` SET `BoundingRadius`=1.196869, `CombatReach`=1.8975, `VerifiedBuild`=23420 WHERE `DisplayID`=71740;
UPDATE `creature_model_info` SET `BoundingRadius`=1.028698, `CombatReach`=1.81125, `VerifiedBuild`=23420 WHERE `DisplayID`=71734;
UPDATE `creature_model_info` SET `BoundingRadius`=1.937479, `CombatReach`=7, `VerifiedBuild`=23420 WHERE `DisplayID`=68378;
UPDATE `creature_model_info` SET `BoundingRadius`=1.273626, `CombatReach`=2.2425, `VerifiedBuild`=23420 WHERE `DisplayID`=72131;
UPDATE `creature_model_info` SET `BoundingRadius`=2.7502, `CombatReach`=1.1, `VerifiedBuild`=23420 WHERE `DisplayID`=71753;
UPDATE `creature_model_info` SET `BoundingRadius`=1.24, `CombatReach`=8, `VerifiedBuild`=23420 WHERE `DisplayID`=74021;
UPDATE `creature_model_info` SET `CombatReach`=1.5 WHERE `DisplayID`=70038;
UPDATE `creature_model_info` SET `BoundingRadius`=1.795303, `CombatReach`=2.84625, `VerifiedBuild`=23420 WHERE `DisplayID`=72145;
UPDATE `creature_model_info` SET `BoundingRadius`=1.567539, `CombatReach`=2.76, `VerifiedBuild`=23420 WHERE `DisplayID`=72342;
UPDATE `creature_model_info` SET `BoundingRadius`=0.765, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=74111;
UPDATE `creature_model_info` SET `BoundingRadius`=0.52, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=74112;
UPDATE `creature_model_info` SET `BoundingRadius`=0.765, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=74113;
UPDATE `creature_model_info` SET `BoundingRadius`=0.52, `CombatReach`=3.75, `VerifiedBuild`=23420 WHERE `DisplayID`=74108;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=3, `VerifiedBuild`=23420 WHERE `DisplayID`=71771;
UPDATE `creature_model_info` SET `CombatReach`=1.578947 WHERE `DisplayID`=71539;
UPDATE `creature_model_info` SET `CombatReach`=1.612903, `VerifiedBuild`=23420 WHERE `DisplayID`=68964;
UPDATE `creature_model_info` SET `BoundingRadius`=1.085, `CombatReach`=3.5, `VerifiedBuild`=23420 WHERE `DisplayID`=74166;
UPDATE `creature_model_info` SET `BoundingRadius`=1.031494 WHERE `DisplayID`=64629;
UPDATE `creature_model_info` SET `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=62708;
UPDATE `creature_model_info` SET `BoundingRadius`=1.13161, `CombatReach`=1.65, `VerifiedBuild`=23420 WHERE `DisplayID`=61927;
UPDATE `creature_model_info` SET `BoundingRadius`=1.65039 WHERE `DisplayID`=68393;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4668, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=67973;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3672, `CombatReach`=1.8, `VerifiedBuild`=23420 WHERE `DisplayID`=67976;
UPDATE `creature_model_info` SET `BoundingRadius`=1.65039, `VerifiedBuild`=23420 WHERE `DisplayID`=65091;
UPDATE `creature_model_info` SET `BoundingRadius`=0.665383, `CombatReach`=0.8 WHERE `DisplayID`=70663;

DELETE FROM `npc_vendor` WHERE (`entry`=114815 AND `item`=31394 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=31395 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=31393 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2535 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2534 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2533 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2532 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2531 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2530 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114815 AND `item`=2528 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(114815, 11, 31394, 0, 0, 1, 0, 0, 23420), -- Plans: Iceguard Leggings
(114815, 10, 31395, 0, 0, 1, 0, 0, 23420), -- Plans: Iceguard Helm
(114815, 9, 31393, 0, 0, 1, 0, 0, 23420), -- Plans: Iceguard Breastplate
(114815, 8, 2535, 0, 0, 1, 0, 1, 23420), -- War Staff
(114815, 7, 2534, 0, 0, 1, 0, 1, 23420), -- Rondel
(114815, 6, 2533, 0, 0, 1, 0, 1, 23420), -- War Maul
(114815, 5, 2532, 0, 0, 1, 0, 1, 23420), -- Morning Star
(114815, 4, 2531, 0, 0, 1, 0, 1, 23420), -- Great Axe
(114815, 3, 2530, 0, 0, 1, 0, 1, 23420), -- Francisca
(114815, 2, 2529, 0, 0, 1, 0, 1, 23420), -- Zweihander
(114815, 1, 2528, 0, 0, 1, 0, 1, 23420); -- Falchion

UPDATE `npc_vendor` SET `maxcount`=4, `VerifiedBuild`=23420 WHERE (`entry`=35642 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `maxcount`=4, `VerifiedBuild`=23420 WHERE (`entry`=35642 AND `item`=4364 AND `ExtendedCost`=0 AND `type`=1); -- Coarse Blasting Powder
UPDATE `npc_vendor` SET `slot`=22 WHERE (`entry`=32642 AND `item`=33449 AND `ExtendedCost`=0 AND `type`=1); -- Crusty Flatbread

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=111215 AND `ID`=1) OR (`CreatureID`=97829 AND `ID`=1) OR (`CreatureID`=114251 AND `ID`=1) OR (`CreatureID`=114284 AND `ID`=1) OR (`CreatureID`=115022 AND `ID`=1) OR (`CreatureID`=114317 AND `ID`=1) OR (`CreatureID`=114319 AND `ID`=1) OR (`CreatureID`=116146 AND `ID`=1) OR (`CreatureID`=116710 AND `ID`=1) OR (`CreatureID`=106522 AND `ID`=1) OR (`CreatureID`=116233 AND `ID`=1) OR (`CreatureID`=112678 AND `ID`=1) OR (`CreatureID`=112973 AND `ID`=1) OR (`CreatureID`=111303 AND `ID`=1) OR (`CreatureID`=111225 AND `ID`=1) OR (`CreatureID`=111226 AND `ID`=1) OR (`CreatureID`=108591 AND `ID`=1) OR (`CreatureID`=115499 AND `ID`=1) OR (`CreatureID`=116256 AND `ID`=1) OR (`CreatureID`=116372 AND `ID`=1) OR (`CreatureID`=107980 AND `ID`=1) OR (`CreatureID`=112595 AND `ID`=1) OR (`CreatureID`=112596 AND `ID`=1) OR (`CreatureID`=111166 AND `ID`=1) OR (`CreatureID`=111164 AND `ID`=1) OR (`CreatureID`=111170 AND `ID`=1) OR (`CreatureID`=112638 AND `ID`=1) OR (`CreatureID`=113013 AND `ID`=1) OR (`CreatureID`=113012 AND `ID`=1) OR (`CreatureID`=112675 AND `ID`=1) OR (`CreatureID`=112676 AND `ID`=1) OR (`CreatureID`=113052 AND `ID`=1) OR (`CreatureID`=112671 AND `ID`=1) OR (`CreatureID`=112603 AND `ID`=1) OR (`CreatureID`=112803 AND `ID`=1) OR (`CreatureID`=112709 AND `ID`=1) OR (`CreatureID`=111227 AND `ID`=1) OR (`CreatureID`=111772 AND `ID`=1) OR (`CreatureID`=109102 AND `ID`=1) OR (`CreatureID`=104660 AND `ID`=1) OR (`CreatureID`=110472 AND `ID`=1) OR (`CreatureID`=110347 AND `ID`=1) OR (`CreatureID`=112090 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(111215, 1, 28188, 0, 0), -- Mariella Ward
(97829, 1, 44091, 0, 0), -- Onslaught Apostate
(114251, 1, 31038, 0, 0), -- Galindre
(114284, 1, 25587, 0, 0), -- Elfyra
(115022, 1, 92961, 0, 0), -- The Monkey King
(114317, 1, 28738, 0, 0), -- Lady Catriona Von'Indi
(114319, 1, 28736, 0, 28737), -- Lady Keira Berrybuck
(116146, 1, 32632, 0, 32633), -- Lord Illidan Stormrage
(116710, 1, 88793, 0, 0), -- Kvaldir Minion
(106522, 1, 22589, 0, 0), -- Archmage Khadgar
(116233, 1, 88793, 0, 0), -- Fog Cloud
(112678, 1, 45613, 0, 0), -- Shal'dorei Naturalist
(112973, 1, 133175, 0, 0), -- Duskwatch Weaver
(111303, 1, 133175, 0, 0), -- Nightborne Sage
(111225, 1, 115069, 0, 0), -- Chaos Mage Beleron
(111226, 1, 126793, 0, 0), -- Summoner Xiv
(108591, 1, 138751, 0, 138743), -- Felsworn Spellguard
(115499, 1, 132170, 0, 0), -- Silgryn
(116256, 1, 137254, 0, 0), -- Victoire
(116372, 1, 132170, 0, 0), -- Arluelle
(107980, 1, 137256, 0, 137259), -- Spellblade Aluriel
(112595, 1, 133175, 0, 0), -- Shal'dorei Archmage
(112596, 1, 132170, 0, 137253), -- Duskwatch Warden
(111166, 1, 132170, 0, 132249), -- Resolute Courtesan
(111164, 1, 40497, 0, 0), -- Twilight Stardancer
(111170, 1, 132171, 0, 0), -- Astral Farseer
(112638, 1, 132170, 0, 132249), -- Astral Defender
(113013, 1, 138755, 0, 0), -- Felsworn Shadowblade
(113012, 1, 138747, 0, 0), -- Felsworn Chaos-Mage
(112675, 1, 133177, 0, 137253), -- Duskwatch Sentinel
(112676, 1, 133175, 0, 0), -- Nobleborn Warpcaster
(113052, 1, 138753, 0, 138753), -- Dreadguard
(112671, 1, 132170, 0, 132171), -- Duskwatch Battle-Magus
(112603, 1, 133176, 0, 0), -- Terrace Grove-Tender
(112803, 1, 124381, 0, 0), -- Astrologer Jarin
(112709, 1, 133177, 0, 0), -- Promenade Guard
(111227, 1, 116496, 0, 0), -- Felweaver Pharamere
(111772, 1, 13312, 0, 49160), -- Terric the Illuminator
(109102, 1, 128366, 0, 0), -- Delas Moonfang
(104660, 1, 107696, 0, 0), -- Blargrul the Siegebrul
(110472, 1, 19808, 0, 0), -- King Mrgl-Mrgl
(110347, 1, 116454, 0, 116454), -- Timeworn Taskmaster
(112090, 1, 57005, 0, 0); -- Shoalfin Warrior

UPDATE `creature_equip_template` SET `ItemID1`=3346 WHERE (`CreatureID`=114803 AND `ID`=2); -- Spectral Stable Hand
UPDATE `creature_equip_template` SET `ItemID1`=3367 WHERE (`CreatureID`=114803 AND `ID`=1); -- Spectral Stable Hand
UPDATE `creature_equip_template` SET `ItemID1`=132870 WHERE (`CreatureID`=90349 AND `ID`=1); -- Silver Hand Knight
UPDATE `creature_equip_template` SET `ItemID1`=2705 WHERE (`CreatureID`=96793 AND `ID`=2); -- Stefen Cotter
UPDATE `creature_equip_template` SET `ItemID1`=3367 WHERE (`CreatureID`=44158 AND `ID`=3); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID1`=1903 WHERE (`CreatureID`=44158 AND `ID`=2); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID1`=1902 WHERE (`CreatureID`=44158 AND `ID`=1); -- Orgrimmar Skyway Peon
UPDATE `creature_equip_template` SET `ItemID1`=118563 WHERE (`CreatureID`=99386 AND `ID`=1); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=88782 AND `ID`=2); -- Nar'thalas Nightwatcher
UPDATE `creature_equip_template` SET `ItemID1`=1910 WHERE (`CreatureID`=89112 AND `ID`=2); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=1899 WHERE (`CreatureID`=89104 AND `ID`=4); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=12993 WHERE (`CreatureID`=89113 AND `ID`=3); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=88668 WHERE (`CreatureID`=89104 AND `ID`=2); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=33598 WHERE (`CreatureID`=89112 AND `ID`=1); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=88668 WHERE (`CreatureID`=89113 AND `ID`=2); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=2703 WHERE (`CreatureID`=96793 AND `ID`=1); -- Stefen Cotter
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=89023 AND `ID`=1); -- Nightwatcher Idri
UPDATE `creature_equip_template` SET `ItemID3`=118201 WHERE (`CreatureID`=88782 AND `ID`=1); -- Nar'thalas Nightwatcher

UPDATE `quest_template` SET `Flags`=34603008, `VerifiedBuild`=23420 WHERE `ID`=45163; -- -Unknown-
UPDATE `quest_template` SET `RewardMoney`=19400 WHERE `ID`=37855; -- The Last of the Last
UPDATE `quest_template` SET `LogTitle`='Darkheart Thicket: Through the Fog', `VerifiedBuild`=23420 WHERE `ID`=41169; -- -Unknown-
UPDATE `quest_template` SET `Flags`=34603008, `VerifiedBuild`=23420 WHERE `ID`=45163; -- -Unknown-

UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115881; -- Vis'ileth
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103065; -- Lynel'a
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103055; -- Kir'altius
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103053; -- Hargo'then
UPDATE `creature_template` SET `faction`=190, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=116374; -- Swirling Zephyr
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2831, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=106351; -- Artificer Lothaire
UPDATE `creature_template` SET `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=102034; -- Wild Moonfall Hippogryph
UPDATE `creature_template` SET `unit_flags`=33280 WHERE `entry`=101780; -- Moonfall Hippogryph
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=100409; -- Dusky Howler
UPDATE `creature_template` SET `unit_flags`=33040 WHERE `entry`=92617; -- Bradensbrook Villager
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=0.8, `speed_run`=0.2857143, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=4196352, `VehicleId`=5076, `VerifiedBuild`=23420 WHERE `entry`=114535; -- Orb of Corrosion
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=37750784, `VerifiedBuild`=23420 WHERE `entry`=114566; -- Orb of Corrosion
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.285714, `rank`=1, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554432, `HealthModifier`=4, `VerifiedBuild`=23420 WHERE `entry`=114568; -- Decaying Minion
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=14, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114812; -- Night Watch Mariner
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=3.2, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=262208, `type_flags`=270532680, `HealthModifier`=23.8, `VerifiedBuild`=23420 WHERE `entry`=114709; -- Grimelord
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=3.2, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=262208, `HealthModifier`=13.5, `VerifiedBuild`=23420 WHERE `entry`=114809; -- Night Watch Mariner
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=2.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554432, `unit_flags2`=4196352, `VehicleId`=5067, `VerifiedBuild`=23420 WHERE `entry`=115941; -- Kvaldir Longboat
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114905; -- Gripping Tentacle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114901; -- Gripping Tentacle
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=37781504, `VehicleId`=5010, `VerifiedBuild`=23420 WHERE `entry`=114900; -- Gripping Tentacle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116214; -- Orb of Corruption
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=0.8, `speed_run`=0.2857143, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VehicleId`=5076, `VerifiedBuild`=23420 WHERE `entry`=115166; -- Orb of Corruption
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=37750784, `VerifiedBuild`=23420 WHERE `entry`=114523; -- Orb of Corruption
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114923; -- Shatterbone Skeleton
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=4.5, `VerifiedBuild`=23420 WHERE `entry`=114811; -- Kvaldir Coral Maiden
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=4.2, `VerifiedBuild`=23420 WHERE `entry`=114813; -- Kvaldir Tide Witch
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_walk`=1.555556, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114932; -- Deepbrine Monstrosity
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33600, `unit_flags2`=1073741824, `VehicleId`=5002, `HealthModifier`=747, `VerifiedBuild`=23420 WHERE `entry`=114537; -- Helya
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33816576, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=117783; -- Gripping Tentacle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33816576, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=117784; -- Gripping Tentacle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=0.5714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=114344; -- Guarm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=0.5714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=114343; -- Guarm
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=14, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2097152, `VehicleId`=4995, `HealthModifier`=495, `VerifiedBuild`=23420 WHERE `entry`=114323; -- Guarm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=0.5714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=114341; -- Guarm
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=6, `VerifiedBuild`=23420 WHERE `entry`=114922; -- Dark Seraph
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114639; -- Kvaldir Reefcaller
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114614; -- Kvaldir Spiritrender
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114539; -- Kvaldir Reefcaller
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=268437504, `VerifiedBuild`=23420 WHERE `entry`=114672; -- Brazier Stalker
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114548; -- Rotsoul Giant
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114546; -- Risen Bonethrall
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=0.8, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114534; -- Helhound
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=109795; -- Neglected Bones
UPDATE `creature_template` SET `unit_flags`=33587968, `VerifiedBuild`=23420 WHERE `entry`=93094; -- Restless Ancestor
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114547; -- Ancient Bonethrall
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114538; -- Kvaldir Spiritrender
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114532; -- Bonespeaker Soulbinder
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116268; -- Aspiring Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116269; -- Heroic Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116270; -- Mythical Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116271; -- Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=4.8, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114996; -- Valarjar Runebearer
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=4.8, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=4196352, `HoverHeight`=9, `HealthModifier`=140, `VerifiedBuild`=23420 WHERE `entry`=114360; -- Hyrja
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=4.8, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=4196352, `HealthModifier`=140, `VerifiedBuild`=23420 WHERE `entry`=114361; -- Hymdall
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `npcflag`=0, `speed_walk`=6, `speed_run`=2.142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags2`=1073741824, `HealthModifier`=450, `VerifiedBuild`=23420 WHERE `entry`=114263; -- Odyn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=114785; -- Valarjar Marksman
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_walk`=4, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=4196352, `HoverHeight`=6, `VerifiedBuild`=23420 WHERE `entry`=114791; -- Chosen of Eyir
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=35, `speed_walk`=1.6, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114789; -- Stormforged Sentinel
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114786; -- Valarjar Mystic
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114787; -- Valarjar Runecarver
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114784; -- Valarjar Champion
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114788; -- Valarjar Thundercaller
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=94691; -- Overgrown Larva
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048 WHERE `entry`=721; -- Rabbit
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=88981; -- Ironclaw Scuttler
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=33556481 WHERE `entry`=93095; -- Voracious Bear
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=93344; -- Runebound Stonewarden
UPDATE `creature_template` SET `npcflag`=0, `unit_flags`=32768 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `minlevel`=9, `maxlevel`=9 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=15891; -- Lunar Festival Herald
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=44158; -- Orgrimmar Skyway Peon
UPDATE `creature_template` SET `minlevel`=99, `maxlevel`=110 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=108 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=82 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=93531; -- Enchanter Nalthanis
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `npcflag`=17 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `npcflag`=16 WHERE `entry`=92459; -- Dorothy Egan
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=92464; -- Kuhuine Tenderstride
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102273; -- Doomguard Infiltrator
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `unit_flags`=570688256, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=102269; -- Felstalker Ravener
UPDATE `creature_template` SET `unit_flags`=570688256, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=102272; -- Felguard Destroyer
UPDATE `creature_template` SET `unit_flags`=256 WHERE `entry`=102672; -- Nythendra
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049 WHERE `entry`=108890; -- Runewood Greatstag
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049 WHERE `entry`=107758; -- Amberfall Greatstag
UPDATE `creature_template` SET `unit_flags2`=34816 WHERE `entry`=110534; -- Provisioner Sheldon
UPDATE `creature_template` SET `HoverHeight`=1.06 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=100778; -- Nightborne Trapper
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2883, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116225; -- Felborne Torturer
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5 WHERE `entry`=116068; -- Arm of the Magistrix
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2883, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116218; -- Felborne Punisher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2884, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116199; -- Nightborne Exile
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=110043; -- Heartwood Doe
UPDATE `creature_template` SET `HoverHeight`=1 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=111056; -- Tiny Illusory Dancer
UPDATE `creature_template` SET `unit_flags`=33024, `unit_flags2`=2048 WHERE `entry`=113752; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags`=33024, `unit_flags2`=2048 WHERE `entry`=107451; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags2`=34816 WHERE `entry`=107598; -- Vanthir
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=108063; -- Korine
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=115951; -- Noressa
UPDATE `creature_template` SET `npcflag`=4227, `VerifiedBuild`=23420 WHERE `entry`=98548; -- Chief Telemancer Oculeth
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=108870; -- Sylverin
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=97140; -- First Arcanist Thalyssra
UPDATE `creature_template` SET `unit_flags`=33040, `VerifiedBuild`=23420 WHERE `entry`=109411; -- Shadescale Flyeater
UPDATE `creature_template` SET `unit_flags`=33587968, `VerifiedBuild`=23420 WHERE `entry`=113184; -- Cunning Bushtail
UPDATE `creature_template` SET `unit_flags`=294976 WHERE `entry`=90901; -- Pridelord Meowl
UPDATE `creature_template` SET `minlevel`=108, `maxlevel`=108 WHERE `entry`=106772; -- Exotic Book
UPDATE `creature_template` SET `minlevel`=98 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `maxlevel`=109 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=109 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=109 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `speed_walk`=1, `speed_run`=1 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=111757; -- Warden Trainee
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `unit_flags`=33536, `VerifiedBuild`=23420 WHERE `entry`=89051; -- Okuna Longtusk
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=109028; -- Horkus
UPDATE `creature_template` SET `unit_flags`=33536 WHERE `entry`=112874; -- Landon Doi
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=111929; -- Felsworn Defiler
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23420 WHERE `entry`=103972; -- Felsworn Betrayer
UPDATE `creature_template` SET `minlevel`=106, `maxlevel`=106 WHERE `entry`=107245; -- Marius Felbane
UPDATE `creature_template` SET `minlevel`=108, `maxlevel`=108 WHERE `entry`=107244; -- Tehd Shoemaker
UPDATE `creature_template` SET `unit_flags`=570721024, `unit_flags2`=2049 WHERE `entry`=101825; -- Nightborne Enforcer
UPDATE `creature_template` SET `unit_flags`=570721024, `unit_flags2`=2049 WHERE `entry`=101821; -- Nightborne Warpcaster
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=109188; -- Withered Test Subject
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=99791; -- Koralune Ettin
UPDATE `creature_template` SET `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=108942; -- Withered Feaster
UPDATE `creature_template` SET `faction`=2575, `unit_flags2`=2048 WHERE `entry`=106286; -- Sylvan Owl
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=97548; -- Shala'nir Druid
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=97565; -- Nightmare Totem
UPDATE `creature_template` SET `speed_walk`=1.111112, `speed_run`=1.142857, `unit_flags`=32768 WHERE `entry`=116765; -- Ancient Protector
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=93946; -- Temple Archer
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=93318; -- Ashmaw Mauler
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=93319; -- Ashmaw Cub
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=109819; -- Wild Dreamrunner
UPDATE `creature_template` SET `faction`=188, `unit_flags2`=2048 WHERE `entry`=92388; -- Vale Flitter
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93314; -- Gleamhoof Doe
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93313; -- Gleamhoof Stag
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=106630; -- Burrowing Leyworm
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=105 WHERE `entry`=106915; -- Marius Felbane
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=106 WHERE `entry`=106914; -- Tehd Shoemaker
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=99420; -- Kharmeera
UPDATE `creature_template` SET `unit_flags`=537165824, `unit_flags2`=2049 WHERE `entry`=90336; -- Azurewing Whelpling
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2099200 WHERE `entry`=89940; -- Azurewing Scalewarden
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857 WHERE `entry`=88090; -- Fathom-Commander Zarrin
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=98159; -- Alynblaze
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=89278; -- Demon Hunter
UPDATE `creature_template` SET `unit_flags`=33280, `unit_flags2`=33589248 WHERE `entry`=86969; -- Demon Hunter
UPDATE `creature_template` SET `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=101943; -- Felguard Shocktrooper
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5, `unit_flags`=536887360 WHERE `entry`=89884; -- Flog the Captain-Eater
UPDATE `creature_template` SET `unit_flags`=33024, `VerifiedBuild`=23420 WHERE `entry`=89112; -- Shipwrecked Captive
UPDATE `creature_template` SET `unit_flags`=768, `unit_flags2`=69208064, `VerifiedBuild`=23420 WHERE `entry`=88094; -- Sea Skrog
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `VerifiedBuild`=23420 WHERE `entry`=89713; -- Koak Hoburn
UPDATE `creature_template` SET `npcflag`=0, `unit_flags`=32768 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `npcflag`=81, `VerifiedBuild`=23420 WHERE `entry`=3363; -- Magar
UPDATE `creature_template` SET `minlevel`=2, `VerifiedBuild`=23420 WHERE `entry`=14499; -- Horde Orphan
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=23420 WHERE `entry`=3345; -- Godan
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=3 WHERE `entry`=42548; -- Muddy Crawfish
UPDATE `creature_template` SET `minlevel`=9, `maxlevel`=9 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=15891; -- Lunar Festival Herald
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=44158; -- Orgrimmar Skyway Peon
UPDATE `creature_template` SET `minlevel`=99, `maxlevel`=110 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=108 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=82 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=93531; -- Enchanter Nalthanis
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `npcflag`=17 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `npcflag`=16 WHERE `entry`=92459; -- Dorothy Egan
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=92464; -- Kuhuine Tenderstride
UPDATE `creature_template` SET `minlevel`=98 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `maxlevel`=109 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=109 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=109 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `speed_walk`=1, `speed_run`=1 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=111757; -- Warden Trainee
UPDATE `creature_template` SET `npcflag`=0, `unit_flags`=32784 WHERE `entry`=91097; -- Tehd Shoemaker
UPDATE `creature_template` SET `unit_flags`=32832 WHERE `entry`=91100; -- Brogozog
UPDATE `creature_template` SET `unit_flags`=33536 WHERE `entry`=112874; -- Landon Doi
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=109028; -- Horkus
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1, `BaseAttackTime`=1600, `VerifiedBuild`=23420 WHERE `entry`=95318; -- Perrexx
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=1700, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=16779264, `VerifiedBuild`=23420 WHERE `entry`=110472; -- King Mrgl-Mrgl
UPDATE `creature_template` SET `HoverHeight`=1 WHERE `entry`=111787; -- Great Sea Ray
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=107216; -- Legion Jailer
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=107328; -- Netherflame Infernal
UPDATE `creature_template` SET `npcflag`=129 WHERE `entry`=93973; -- Leyweaver Phaxondus
UPDATE `creature_template` SET `unit_flags`=570720256, `unit_flags2`=2049 WHERE `entry`=91288; -- Smolderhide Warrior
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93680; -- Druid of the Claw
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=93946; -- Temple Archer
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=108876; -- Risen Vanguard
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112247; -- Budding Tree
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=106630; -- Burrowing Leyworm
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=90040; -- Azurewing Keeper
UPDATE `creature_template` SET `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=101943; -- Felguard Shocktrooper
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=89278; -- Demon Hunter
UPDATE `creature_template` SET `unit_flags`=33280, `unit_flags2`=33589248 WHERE `entry`=86969; -- Demon Hunter
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857 WHERE `entry`=88090; -- Fathom-Commander Zarrin
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2099200 WHERE `entry`=89940; -- Azurewing Scalewarden
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=108389; -- Mrgrlilgrl
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=190, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114113; -- Felslate Basilisk
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=109188; -- Withered Test Subject
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=99791; -- Koralune Ettin
UPDATE `creature_template` SET `unit_flags`=33024, `VerifiedBuild`=23420 WHERE `entry`=103805; -- Sablehorn Doe
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=103808; -- Managazer
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=103527; -- Manascale Basilisk
UPDATE `creature_template` SET `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=102034; -- Wild Moonfall Hippogryph
UPDATE `creature_template` SET `faction`=190, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116373; -- Lunar Crucible Portal
UPDATE `creature_template` SET `HoverHeight`=1 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `faction`=190, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=116374; -- Swirling Zephyr
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103065; -- Lynel'a
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103055; -- Kir'altius
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103053; -- Hargo'then
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110 WHERE `entry`=105778; -- Angry Crowd
UPDATE `creature_template` SET `unit_flags`=33536 WHERE `entry`=105633; -- Understone Drudge
UPDATE `creature_template` SET `unit_flags`=32832, `unit_flags2`=4196352 WHERE `entry`=98406; -- Embershard Scorpion
UPDATE `creature_template` SET `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=89652; -- Shallows Heron
UPDATE `creature_template` SET `unit_flags`=768, `unit_flags2`=69208064, `VerifiedBuild`=23420 WHERE `entry`=88094; -- Sea Skrog
UPDATE `creature_template` SET `unit_flags`=537165824, `unit_flags2`=2049 WHERE `entry`=90336; -- Azurewing Whelpling
UPDATE `creature_template` SET `BaseAttackTime`=1818, `VerifiedBuild`=23420 WHERE `entry`=90390; -- Tyndrissen
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=105959; -- Ley Siphon
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=105944; -- Prepared Mana Dust
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93314; -- Gleamhoof Doe
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93313; -- Gleamhoof Stag
UPDATE `creature_template` SET `faction`=188, `unit_flags2`=2048 WHERE `entry`=92388; -- Vale Flitter
UPDATE `creature_template` SET `minlevel`=102, `VerifiedBuild`=23420 WHERE `entry`=92852; -- Elder Treant
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=100409; -- Dusky Howler
UPDATE `creature_template` SET `unit_flags`=33040 WHERE `entry`=92617; -- Bradensbrook Villager
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=93319; -- Ashmaw Cub
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=93318; -- Ashmaw Mauler
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=109819; -- Wild Dreamrunner
UPDATE `creature_template` SET `speed_run`=1.142857, `VerifiedBuild`=23420 WHERE `entry`=95247; -- Black Rook Spectral Officer
UPDATE `creature_template` SET `unit_flags`=33587968, `VerifiedBuild`=23420 WHERE `entry`=113184; -- Cunning Bushtail
UPDATE `creature_template` SET `HoverHeight`=1 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=97565; -- Nightmare Totem
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=97548; -- Shala'nir Druid
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=109819; -- Wild Dreamrunner
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=7, `speed_walk`=0.8, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=570720256, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=112907; -- Gleamhoof Stag
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2156, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112812; -- Feral Worgen
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=93946; -- Temple Archer
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=108876; -- Risen Vanguard
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=100409; -- Dusky Howler
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=93319; -- Ashmaw Cub
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=93318; -- Ashmaw Mauler
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93314; -- Gleamhoof Doe
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=93313; -- Gleamhoof Stag
UPDATE `creature_template` SET `minlevel`=25, `maxlevel`=25, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=99016; -- Beaky
UPDATE `creature_template` SET `minlevel`=25, `maxlevel`=25, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=99015; -- Sunny
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=99035; -- Durian Strongfruit
UPDATE `creature_template` SET `minlevel`=25, `maxlevel`=25, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=99017; -- Roots
UPDATE `creature_template` SET `faction`=188, `unit_flags2`=2048 WHERE `entry`=92388; -- Vale Flitter
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23420 WHERE `entry`=113966; -- Dungeoneer's Training Dummy
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3, `VerifiedBuild`=23420 WHERE `entry`=113964; -- Raider's Training Dummy
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116612; -- High Priest Armor
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=110819; -- Truth
UPDATE `creature_template` SET `npcflag`=131, `type_flags2`=16384, `VerifiedBuild`=23420 WHERE `entry`=111738; -- Juvess the Duskwhisperer
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113220; -- Dark Zealot
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111215; -- Mariella Ward
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=98, `VerifiedBuild`=23420 WHERE `entry`=102587; -- Saa'ra
UPDATE `creature_template` SET `minlevel`=99, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=107565; -- Lightspawn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=106451; -- Vicar Eliza
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=97829; -- Onslaught Apostate
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `VerifiedBuild`=23420 WHERE `entry`=113147; -- Weak Lightspawn
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3, `VerifiedBuild`=23420 WHERE `entry`=107556; -- Bound Void Walker
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=110, `VerifiedBuild`=23420 WHERE `entry`=102342; -- Hooded Priest
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=108 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `npcflag`=82 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `npcflag`=83 WHERE `entry`=93531; -- Enchanter Nalthanis
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `npcflag`=17 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115486; -- Erudite Slayer
UPDATE `creature_template` SET `unit_flags`=768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=115487; -- Medivh
UPDATE `creature_template` SET `HealthModifier`=2.25, `VerifiedBuild`=23420 WHERE `entry`=114249; -- Volatile Energy
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=0.888888, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114324; -- Winged Assistant
UPDATE `creature_template` SET `faction`=35, `speed_run`=1.385714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=67584, `VehicleId`=5000, `VerifiedBuild`=23420 WHERE `entry`=114486; -- Crone's Broom
UPDATE `creature_template` SET `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114520; -- Figero
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114251; -- Galindre
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114284; -- Elfyra
UPDATE `creature_template` SET `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115007; -- Hozen Cage
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=35, `speed_walk`=0.888888, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115005; -- Caged Assistant
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_walk`=0.888888, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115022; -- The Monkey King
UPDATE `creature_template` SET `unit_flags`=33587456, `VerifiedBuild`=23420 WHERE `entry`=114544; -- Skeletal Usher
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114317; -- Lady Catriona Von'Indi
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114319; -- Lady Keira Berrybuck
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=114714; -- Ghostly Steward
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=114794; -- Skeletal Hound
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114256; -- Feltongue Corruptor
UPDATE `creature_template` SET `faction`=7, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=18432, `VerifiedBuild`=23420 WHERE `entry`=114623; -- Disturbed Energy
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114314; -- Eredar Portal-Keeper
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=114645; -- Water Sample
UPDATE `creature_template` SET `faction`=7, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=18432, `VerifiedBuild`=23420 WHERE `entry`=114678; -- Arcane Ward
UPDATE `creature_template` SET `faction`=7, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=18432, `VerifiedBuild`=23420 WHERE `entry`=114781; -- Arcane Ward
UPDATE `creature_template` SET `faction`=7, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=18432, `VerifiedBuild`=23420 WHERE `entry`=114782; -- Arcane Ward
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=100665344, `VerifiedBuild`=23420 WHERE `entry`=115219; -- Southern Ward
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1696, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114667; -- Archmage Alturus
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1696, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115029; -- Archmage Leryda
UPDATE `creature_template` SET `npcflag`=16 WHERE `entry`=92459; -- Dorothy Egan
UPDATE `creature_template` SET `npcflag`=0, `unit_flags`=32768 WHERE `entry`=72559; -- Thunder Bluff Protector
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=9, `maxlevel`=9 WHERE `entry`=44160; -- Suspicious Peon
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=15891; -- Lunar Festival Herald
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=44158; -- Orgrimmar Skyway Peon
UPDATE `creature_template` SET `minlevel`=99, `maxlevel`=110 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `npcflag`=19 WHERE `entry`=92464; -- Kuhuine Tenderstride
UPDATE `creature_template` SET `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=17213; -- Broom
UPDATE `creature_template` SET `minlevel`=98 WHERE `entry`=105904; -- Raven
UPDATE `creature_template` SET `maxlevel`=110 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `maxlevel`=109 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=109 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `speed_run`=1.142857 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=109 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `speed_walk`=1, `speed_run`=1 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116146; -- Lord Illidan Stormrage
UPDATE `creature_template` SET `modelid1`=75610, `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2099200, `type`=10, `type_flags`=16778240, `type_flags2`=16, `VerifiedBuild`=23420 WHERE `entry`=116710; -- Kvaldir Minion
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116156; -- Light's Heart
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=4196352, `VehicleId`=4329, `VerifiedBuild`=23420 WHERE `entry`=105371; -- Well of Souls
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=4196352, `VehicleId`=4329, `VerifiedBuild`=23420 WHERE `entry`=104594; -- Well of Souls
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=2110, `npcflag`=0, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=768, `unit_flags2`=1073743872, `VerifiedBuild`=23420 WHERE `entry`=106522; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=6.8, `speed_run`=2.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33554432, `unit_flags2`=4194304, `HealthModifier`=20, `movementId`=864, `VerifiedBuild`=23420 WHERE `entry`=106545; -- Empowered Eye of Gul'dan
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=0.4, `speed_run`=0.1428571, `rank`=0, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33554432, `unit_flags2`=4194304, `HealthModifier`=6, `VerifiedBuild`=23420 WHERE `entry`=105630; -- Eye of Gul'dan
UPDATE `creature_template` SET `modelid1`=75606, `modelid2`=0, `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2099200, `movementId`=0, `VerifiedBuild`=23420 WHERE `entry`=116233; -- Fog Cloud
UPDATE `creature_template` SET `type_flags`=16778240, `type_flags2`=16, `VerifiedBuild`=23420 WHERE `entry`=104308; -- Bonds of Fel
UPDATE `creature_template` SET `type_flags`=16778240, `type_flags2`=16, `VerifiedBuild`=23420 WHERE `entry`=104252; -- Bonds of Fel
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33555200, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=104214; -- Nightwell Entity
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114437; -- Lord Illidan Stormrage
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=14, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33555202, `unit_flags2`=67108864, `VehicleId`=4081, `HealthModifier`=379.016, `VerifiedBuild`=23420 WHERE `entry`=106330; -- Elisande
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=0.4, `speed_run`=0.1428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554432, `unit_flags2`=67108864, `VerifiedBuild`=23420 WHERE `entry`=107754; -- Elisande
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554432, `unit_flags2`=67108864, `VerifiedBuild`=23420 WHERE `entry`=105958; -- Echo of Elisande
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554432, `unit_flags2`=67108864, `VerifiedBuild`=23420 WHERE `entry`=106680; -- Echo of Elisande
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554432, `unit_flags2`=67108864, `VerifiedBuild`=23420 WHERE `entry`=105364; -- Echo of Elisande
UPDATE `creature_template` SET `HealthModifier`=37.5989, `VerifiedBuild`=23420 WHERE `entry`=105301; -- Expedient Elemental
UPDATE `creature_template` SET `HealthModifier`=24.7043, `VerifiedBuild`=23420 WHERE `entry`=105299; -- Recursive Elemental
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=3.8, `speed_run`=1.357143, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=100663296, `movementId`=987, `VerifiedBuild`=23420 WHERE `entry`=105370; -- Arcanetic Ring
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=3.8, `speed_run`=1.357143, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=100663296, `movementId`=987, `VerifiedBuild`=23420 WHERE `entry`=105367; -- Arcanetic Ring
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=3.8, `speed_run`=1.357143, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=100663296, `movementId`=987, `VerifiedBuild`=23420 WHERE `entry`=105315; -- Arcanetic Ring
UPDATE `creature_template` SET `HealthModifier`=379.016, `movementId`=184, `VerifiedBuild`=23420 WHERE `entry`=105297; -- Elisande
UPDATE `creature_template` SET `HealthModifier`=379.016, `movementId`=184, `VerifiedBuild`=23420 WHERE `entry`=105474; -- Elisande
UPDATE `creature_template` SET `HealthModifier`=116.592, `VerifiedBuild`=23420 WHERE `entry`=104880; -- Thing That Should Not Be
UPDATE `creature_template` SET `HealthModifier`=1287, `VerifiedBuild`=23420 WHERE `entry`=109041; -- Naturalist Tel'arn
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2654208, `VerifiedBuild`=23420 WHERE `entry`=109804; -- Plasma Sphere
UPDATE `creature_template` SET `HealthModifier`=1287, `VerifiedBuild`=23420 WHERE `entry`=109038; -- Solarist Tel'arn
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=14, `speed_walk`=1.6, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2097152, `HealthModifier`=3.4125, `VerifiedBuild`=23420 WHERE `entry`=109075; -- Parasitic Lasher
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=109583; -- Solar Collapse Stalker
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554496, `unit_flags2`=67108864, `VerifiedBuild`=23420 WHERE `entry`=110341; -- Arcanist Tel'arn
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554496, `unit_flags2`=69206016, `HealthModifier`=1287, `VerifiedBuild`=23420 WHERE `entry`=109164; -- High Botanist Tel'arn
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2097152, `HealthModifier`=1287, `VerifiedBuild`=23420 WHERE `entry`=104528; -- High Botanist Tel'arn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2136, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112956; -- Shimmering Manaspine
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=112973; -- Duskwatch Weaver
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111295; -- Domesticated Manasaber
UPDATE `creature_template` SET `modelid1`=66275, `modelid2`=66589, `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=112678; -- Shal'dorei Naturalist
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=112955; -- Shadescale Wyrm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=111303; -- Nightborne Sage
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `VerifiedBuild`=23420 WHERE `entry`=89713; -- Koak Hoburn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=110677; -- Image of Khadgar
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags2`=4227072, `VehicleId`=5350, `HealthModifier`=749.32, `VerifiedBuild`=23420 WHERE `entry`=101002; -- Krosus
UPDATE `creature_template` SET `HealthModifier`=185, `VerifiedBuild`=23420 WHERE `entry`=111210; -- Searing Infernal
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=634, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587456, `unit_flags2`=4194304, `VerifiedBuild`=23420 WHERE `entry`=111581; -- Fiery Core
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `HealthModifier`=112.5, `VerifiedBuild`=23420 WHERE `entry`=111226; -- Summoner Xiv
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `HealthModifier`=100, `VerifiedBuild`=23420 WHERE `entry`=111225; -- Chaos Mage Beleron
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67143680, `VerifiedBuild`=23420 WHERE `entry`=116670; -- Suramar Portal
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67143680, `VerifiedBuild`=23420 WHERE `entry`=116820; -- Suramar Portal
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=33556480, `HealthModifier`=9.6, `VerifiedBuild`=23420 WHERE `entry`=108591; -- Felsworn Spellguard
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554432, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=108739; -- Carrion Nightmare
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `HealthModifier`=3.88178, `VerifiedBuild`=23420 WHERE `entry`=104326; -- Phantasmal Bloodfang
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `HealthModifier`=7.47602, `VerifiedBuild`=23420 WHERE `entry`=108934; -- Tainted Blood
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=2110, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=104271; -- Combat Stalker
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=1.6, `speed_run`=2, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=2147517184, `VehicleId`=4081, `HealthModifier`=379.016, `VerifiedBuild`=23420 WHERE `entry`=106643; -- Elisande
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=115683; -- The Nightwell
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67143680, `VerifiedBuild`=23420 WHERE `entry`=116662; -- Suramar Portal
UPDATE `creature_template` SET `npcflag`=0, `unit_flags`=33280, `unit_flags2`=2048, `VehicleId`=0 WHERE `entry`=115499; -- Silgryn
UPDATE `creature_template` SET `npcflag`=0, `unit_flags`=33280 WHERE `entry`=116372; -- Arluelle
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=116256; -- Victoire
UPDATE `creature_template` SET `npcflag`=0, `unit_flags`=33280 WHERE `entry`=115840; -- Arcanist Valtrois
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33280, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115772; -- Kal
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=0.4, `speed_run`=1.857143, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=107570; -- Arcane Orb
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=2, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33587264, `VerifiedBuild`=23420 WHERE `entry`=107980; -- Spellblade Aluriel
UPDATE `creature_template` SET `HealthModifier`=6.9984, `VerifiedBuild`=23420 WHERE `entry`=107237; -- Icy Enchantment
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67143680, `VerifiedBuild`=23420 WHERE `entry`=116667; -- Suramar Portal
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=112595; -- Shal'dorei Archmage
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=112596; -- Duskwatch Warden
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=34816, `VerifiedBuild`=23420 WHERE `entry`=112767; -- Star Shooting Stalker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2847, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33024, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=113406; -- Manaflush Noble
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=4000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=111166; -- Resolute Courtesan
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=4000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=111170; -- Astral Farseer
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=4000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=111164; -- Twilight Stardancer
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=4000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=111151; -- Midnight Siphoner
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2799, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=117085; -- Ly'leth Lunastre
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114440; -- The Eye of Aman'thul
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=111587; -- Nether Elemental
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=113052; -- Dreadguard
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=35653632, `HoverHeight`=1.5, `VerifiedBuild`=23420 WHERE `entry`=113043; -- Abyss Watcher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=113013; -- Felsworn Shadowblade
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=113012; -- Felsworn Chaos-Mage
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2097152, `HealthModifier`=1009.8, `VerifiedBuild`=23420 WHERE `entry`=103685; -- Tichondrius
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2099200, `HealthModifier`=1094.4, `VerifiedBuild`=23420 WHERE `entry`=103758; -- Star Augur Etraeus
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=112803; -- Astrologer Jarin
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=112603; -- Terrace Grove-Tender
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112671; -- Duskwatch Battle-Magus
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=14, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=112655; -- Celestial Acolyte
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112638; -- Astral Defender
UPDATE `creature_template` SET `modelid1`=55233, `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=2.4, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113307; -- Chronowraith
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112804; -- Trained Shadescale
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112718; -- Sidereal Familiar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=33556480, `movementId`=186, `VerifiedBuild`=23420 WHERE `entry`=112709; -- Promenade Guard
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=112665; -- Nighthold Protector
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=6000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `VerifiedBuild`=23420 WHERE `entry`=116046; -- Webbed Victim
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116008; -- Kar'zun
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=35, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554688, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113383; -- Fallen Defender
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=112676; -- Nobleborn Warpcaster
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=112675; -- Duskwatch Sentinel
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2847, `speed_walk`=1.6, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33024, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=112660; -- Nighthold Citizen
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `HealthModifier`=100, `VerifiedBuild`=23420 WHERE `entry`=111227; -- Felweaver Pharamere
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=102263; -- Skorpyron
UPDATE `creature_template` SET `unit_flags`=262912, `VerifiedBuild`=23420 WHERE `entry`=100362; -- Grasping Tentacle
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049 WHERE `entry`=107758; -- Amberfall Greatstag
UPDATE `creature_template` SET `unit_flags2`=34816 WHERE `entry`=110534; -- Provisioner Sheldon
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=100778; -- Nightborne Trapper
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2883, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116218; -- Felborne Punisher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2883, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116225; -- Felborne Torturer
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=110043; -- Heartwood Doe
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5 WHERE `entry`=116068; -- Arm of the Magistrix
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2884, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116199; -- Nightborne Exile
UPDATE `creature_template` SET `unit_flags`=570721024, `unit_flags2`=2049 WHERE `entry`=101825; -- Nightborne Enforcer
UPDATE `creature_template` SET `unit_flags`=570721024, `unit_flags2`=2049 WHERE `entry`=101821; -- Nightborne Warpcaster
UPDATE `creature_template` SET `unit_flags`=33344, `unit_flags2`=67635200 WHERE `entry`=97678; -- Aranasi Broodmother
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114881; -- Striking Tentacle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116214; -- Orb of Corruption
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=0.8, `speed_run`=0.2857143, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VehicleId`=5076, `VerifiedBuild`=23420 WHERE `entry`=115166; -- Orb of Corruption
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=37750784, `VerifiedBuild`=23420 WHERE `entry`=114523; -- Orb of Corruption
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114923; -- Shatterbone Skeleton
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=4.5, `VerifiedBuild`=23420 WHERE `entry`=114811; -- Kvaldir Coral Maiden
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=4.2, `VerifiedBuild`=23420 WHERE `entry`=114813; -- Kvaldir Tide Witch
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_walk`=1.555556, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114932; -- Deepbrine Monstrosity
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33600, `unit_flags2`=1073741824, `VehicleId`=5002, `HealthModifier`=747, `VerifiedBuild`=23420 WHERE `entry`=114537; -- Helya
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33816576, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=117783; -- Gripping Tentacle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33816576, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=117784; -- Gripping Tentacle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=0.5714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=114344; -- Guarm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=0.5714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=114343; -- Guarm
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=14, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2097152, `VehicleId`=4995, `HealthModifier`=495, `VerifiedBuild`=23420 WHERE `entry`=114323; -- Guarm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=0.5714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=114341; -- Guarm
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=526336, `VerifiedBuild`=23420 WHERE `entry`=116404; -- Guarm's Chew Toy
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=6, `VerifiedBuild`=23420 WHERE `entry`=114922; -- Dark Seraph
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=0.8, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114640; -- Helhound
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114639; -- Kvaldir Reefcaller
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114614; -- Kvaldir Spiritrender
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114539; -- Kvaldir Reefcaller
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114546; -- Risen Bonethrall
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=268437504, `VerifiedBuild`=23420 WHERE `entry`=114672; -- Brazier Stalker
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114548; -- Rotsoul Giant
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=0.8, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114534; -- Helhound
UPDATE `creature_template` SET `unit_flags`=33587456 WHERE `entry`=109795; -- Neglected Bones
UPDATE `creature_template` SET `unit_flags`=33587968, `VerifiedBuild`=23420 WHERE `entry`=93094; -- Restless Ancestor
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114547; -- Ancient Bonethrall
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114538; -- Kvaldir Spiritrender
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114532; -- Bonespeaker Soulbinder
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116229; -- Mythical Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116245; -- Heroic Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116241; -- Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2844, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116242; -- Aspiring Champion of the Chosen Dead
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=4.8, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114996; -- Valarjar Runebearer
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=4.8, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=4196352, `HoverHeight`=9, `HealthModifier`=140, `VerifiedBuild`=23420 WHERE `entry`=114360; -- Hyrja
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=4.8, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=4196352, `HealthModifier`=140, `VerifiedBuild`=23420 WHERE `entry`=114361; -- Hymdall
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `npcflag`=0, `speed_walk`=6, `speed_run`=2.142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags2`=1073741824, `HealthModifier`=450, `VerifiedBuild`=23420 WHERE `entry`=114263; -- Odyn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_walk`=4, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=4196352, `HoverHeight`=6, `VerifiedBuild`=23420 WHERE `entry`=114791; -- Chosen of Eyir
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=35, `speed_walk`=1.6, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=114789; -- Stormforged Sentinel
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=35653632, `VerifiedBuild`=23420 WHERE `entry`=114785; -- Valarjar Marksman
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114786; -- Valarjar Mystic
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114787; -- Valarjar Runecarver
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114784; -- Valarjar Champion
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=114788; -- Valarjar Thundercaller
UPDATE `creature_template` SET `speed_walk`=0.4, `speed_run`=0.4, `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=113092; -- Swarming Dread
UPDATE `creature_template` SET `speed_walk`=2.08, `speed_run`=0.7428572, `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=113090; -- Corrupted Gatewarden
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5, `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=113089; -- Defiled Keeper
UPDATE `creature_template` SET `unit_flags`=256 WHERE `entry`=102672; -- Nythendra
UPDATE `creature_template` SET `npcflag`=4227, `VerifiedBuild`=23420 WHERE `entry`=98548; -- Chief Telemancer Oculeth
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=97140; -- First Arcanist Thalyssra
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=96697; -- Silver Hand Knight
UPDATE `creature_template` SET `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=102375; -- Runecarver Slave
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=102104; -- Enslaved Shieldmaiden
UPDATE `creature_template` SET `HoverHeight`=1.06 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=88981; -- Ironclaw Scuttler
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=104660; -- Blargrul the Siegebrul
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=94691; -- Overgrown Larva
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048 WHERE `entry`=721; -- Rabbit
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=104226; -- Gloomfang
UPDATE `creature_template` SET `unit_flags`=33587968, `VerifiedBuild`=23420 WHERE `entry`=113184; -- Cunning Bushtail

DELETE FROM `gameobject_template` WHERE `entry` IN (266917 /*Doodad_7vr_Vrykul_fence004*/, 266915 /*Doodad_7vr_Vrykul_fence002*/, 266914 /*Doodad_7vr_Vrykul_fence001*/, 266916 /*Doodad_7vr_Vrykul_fence003*/, 242454 /*Hellfire Gate*/, 266533 /*Guarm_Boss_Door02*/, 266532 /*Guarm_Boss_Door01*/, 265596 /*Ritual Stone*/, 266530 /*Bonfire*/, 266531 /*Bonfire*/, 260526 /*Spoils*/, 258903 /*Xal'atath Appearance*/, 258906 /*Xal'atath Appearance*/, 258898 /*T'uure Appearance*/, 258900 /*T'uure Appearance*/, 260311 /*Naughty Background*/, 260313 /*Naughty Gears*/, 260312 /*Naughty Furnace*/, 266743 /*Box of Spare Motivators*/, 266848 /*POI Marker*/, 267263 /*Doodad_7TI_PillarCreation_EyeOfAmanthul01_Offset001*/, 266483 /*Gift of the Nightborne*/, 250082 /*Eye of Amun'thul*/, 256883 /*Chair*/, 256882 /*Chair*/, 256881 /*Chair*/, 256880 /*Chair*/, 256879 /*Chair*/, 256878 /*Chair*/, 256877 /*Chair*/, 256876 /*Chair*/, 256903 /*Chair*/, 256902 /*Chair*/, 256901 /*Chair*/, 256875 /*Chair*/, 256874 /*Chair*/, 256873 /*Chair*/, 256885 /*Chair*/, 256884 /*Chair*/, 252108 /*Mysterious Fruit*/, 256887 /*Chair*/, 256886 /*Chair*/, 248513 /*Felbound Chest*/, 258848 /*Doodad_7NB_NIGHTBORN_CITYTELEPORTER003*/, 255927 /*Doodad_7NB_NIGHTBORN_TELEPORTER_BASE002*/, 255926 /*Doodad_7NB_NIGHTBORN_CITYTELEPORTER001*/, 251259 /*Cylinder Collision*/, 255928 /*Doodad_7NB_NIGHTBORN_CITYTELEPORTER002*/, 255925 /*Doodad_7NB_NIGHTBORN_TELEPORTER_BASE001*/, 256894 /*Bench*/, 251330 /*Doodad_7sr_suramar_nightwellfx004*/, 252317 /*Doodad_7sr_suramar_nightwellfx003*/, 266835 /*Nightborne Teleporter*/, 258843 /*Doodad_7NB_NIGHTBORN_TELEPORTER_BASE003*/, 256893 /*Bench*/, 256892 /*Bench*/, 252320 /*Nighthold Focusing Statue*/, 256872 /*Bench*/, 266395 /*The Eye of Aman'thul*/, 266183 /*Statue Energy Conduit*/, 266182 /*Statue Energy Conduit*/, 266181 /*Statue Energy Conduit*/, 266180 /*Statue Energy Conduit*/, 266174 /*The Eye of Aman'thul*/, 260985 /*The Eye of Aman'thul*/, 252318 /*The Eye of Aman'thul*/, 251988 /*Nighthold Focusing Statue*/, 266959 /*Containment Field*/, 252321 /*Nighthold Focusing Statue*/, 252319 /*Nighthold Focusing Statue*/, 256900 /*Bench*/, 256899 /*Bench*/, 256898 /*Bench*/, 256895 /*Bench*/, 256870 /*Bench*/, 256857 /*Bench*/, 256891 /*Bench*/, 256890 /*Bench*/, 256889 /*Bench*/, 256888 /*Bench*/, 256897 /*Bench*/, 256896 /*Bench*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(266917, 5, 39573, 'Doodad_7vr_Vrykul_fence004', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7vr_Vrykul_fence004
(266915, 5, 39573, 'Doodad_7vr_Vrykul_fence002', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7vr_Vrykul_fence002
(266914, 5, 39573, 'Doodad_7vr_Vrykul_fence001', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7vr_Vrykul_fence001
(266916, 5, 39573, 'Doodad_7vr_Vrykul_fence003', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7vr_Vrykul_fence003
(242454, 0, 10403, 'Hellfire Gate', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Hellfire Gate
(266533, 0, 31091, 'Guarm_Boss_Door02', '', '', '', 1.85, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Guarm_Boss_Door02
(266532, 0, 31091, 'Guarm_Boss_Door01', '', '', '', 1.01, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Guarm_Boss_Door01
(265596, 10, 38979, 'Ritual Stone', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Ritual Stone
(266530, 8, 7748, 'Bonfire', '', '', '', 1, 4, 10, 2066, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bonfire
(266531, 8, 7748, 'Bonfire', '', '', '', 1, 4, 10, 2066, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bonfire
(260526, 3, 8685, 'Spoils', '', '', '', 2, 1634, 70585, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 100, 0, 0, 0, 0, 1958, 0, 1, 0, 0, 70673, 0, 2, 0, 23420), -- Spoils
(258903, 5, 37897, 'Xal''atath Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Xal'atath Appearance
(258906, 5, 37896, 'Xal''atath Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Xal'atath Appearance
(258898, 5, 37892, 'T''uure Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- T'uure Appearance
(258900, 5, 37891, 'T''uure Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- T'uure Appearance
(260311, 5, 38495, 'Naughty Background', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Naughty Background
(260313, 5, 38497, 'Naughty Gears', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Naughty Gears
(260312, 5, 38496, 'Naughty Furnace', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Naughty Furnace
(266743, 3, 15585, 'Box of Spare Motivators', 'questinteract', 'Retrieving', '', 1, 1691, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70750, 1, 0, 0, 23420), -- Box of Spare Motivators
(266848, 5, 2150, 'POI Marker', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- POI Marker
(267263, 5, 39961, 'Doodad_7TI_PillarCreation_EyeOfAmanthul01_Offset001', '', '', '', 0.7997621, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7TI_PillarCreation_EyeOfAmanthul01_Offset001
(266483, 3, 36450, 'Gift of the Nightborne', 'questinteract', '', '', 2, 1634, 70581, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1866, 0, 1, 0, 0, 68261, 0, 2, 0, 23420), -- Gift of the Nightborne
(250082, 10, 13594, 'Eye of Amun''thul', '', '', '', 1.15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Eye of Amun'thul
(256883, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256882, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256881, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256880, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256879, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256878, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256877, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256876, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256903, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256902, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256901, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256875, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256874, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256873, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256885, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256884, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(252108, 10, 35800, 'Mysterious Fruit', 'questinteract', '', '', 1.5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 220114, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 41995, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Mysterious Fruit
(256887, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(256886, 7, 34334, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Chair
(248513, 3, 27394, 'Felbound Chest', '', '', '', 2, 1634, 70565, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 100, 0, 0, 0, 0, 1842, 0, 1, 0, 0, 70672, 0, 2, 0, 23420), -- Felbound Chest
(258848, 5, 36343, 'Doodad_7NB_NIGHTBORN_CITYTELEPORTER003', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7NB_NIGHTBORN_CITYTELEPORTER003
(255927, 5, 36224, 'Doodad_7NB_NIGHTBORN_TELEPORTER_BASE002', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7NB_NIGHTBORN_TELEPORTER_BASE002
(255926, 5, 36343, 'Doodad_7NB_NIGHTBORN_CITYTELEPORTER001', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7NB_NIGHTBORN_CITYTELEPORTER001
(251259, 5, 19131, 'Cylinder Collision', '', '', '', 4, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Cylinder Collision
(255928, 5, 36343, 'Doodad_7NB_NIGHTBORN_CITYTELEPORTER002', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7NB_NIGHTBORN_CITYTELEPORTER002
(255925, 5, 36224, 'Doodad_7NB_NIGHTBORN_TELEPORTER_BASE001', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7NB_NIGHTBORN_TELEPORTER_BASE001
(256894, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(251330, 10, 34930, 'Doodad_7sr_suramar_nightwellfx004', '', '', '', 1.2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7sr_suramar_nightwellfx004
(252317, 5, 35935, 'Doodad_7sr_suramar_nightwellfx003', '', '', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7sr_suramar_nightwellfx003
(266835, 5, 36343, 'Nightborne Teleporter', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Nightborne Teleporter
(258843, 5, 36224, 'Doodad_7NB_NIGHTBORN_TELEPORTER_BASE003', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7NB_NIGHTBORN_TELEPORTER_BASE003
(256893, 7, 36542, 'Bench', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256892, 7, 36542, 'Bench', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(252320, 10, 35702, 'Nighthold Focusing Statue', '', '', '', 11.14733, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Nighthold Focusing Statue
(256872, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(266395, 10, 39101, 'The Eye of Aman''thul', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Eye of Aman'thul
(266183, 10, 39033, 'Statue Energy Conduit', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Statue Energy Conduit
(266182, 10, 39033, 'Statue Energy Conduit', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Statue Energy Conduit
(266181, 10, 39033, 'Statue Energy Conduit', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Statue Energy Conduit
(266180, 10, 39033, 'Statue Energy Conduit', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Statue Energy Conduit
(266174, 10, 39030, 'The Eye of Aman''thul', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 23420), -- The Eye of Aman'thul
(260985, 10, 38790, 'The Eye of Aman''thul', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 23420), -- The Eye of Aman'thul
(252318, 10, 35131, 'The Eye of Aman''thul', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 23420), -- The Eye of Aman'thul
(251988, 10, 35702, 'Nighthold Focusing Statue', '', '', '', 11.14733, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Nighthold Focusing Statue
(266959, 10, 36092, 'Containment Field', 'questinteract', '', '', 0.8, 2063, 0, 55894, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110, 23420), -- Containment Field
(252321, 10, 35702, 'Nighthold Focusing Statue', '', '', '', 11.14733, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Nighthold Focusing Statue
(252319, 10, 35702, 'Nighthold Focusing Statue', '', '', '', 11.14733, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Nighthold Focusing Statue
(256900, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256899, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256898, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256895, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256870, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256857, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256891, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256890, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256889, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256888, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256897, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Bench
(256896, 7, 34367, 'Bench', '', '', '', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- Bench

DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=266743 AND `Idx`=0) OR (`GameObjectEntry`=252239 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(266743, 0, 143549, 23420), -- Box of Spare Motivators
(252239, 0, 129174, 23420); -- Shipwreck Debris

