DELETE FROM `quest_offer_reward` WHERE `ID` IN (41138 /*Feeding Shal'Aran*/, 41028 /*Power Grid*/, 41298 /*Supplies Needed: Fjarnskaggl*/, 43519 /*Lucid Strength*/, 43514 /*A Vainglorious Past*/, 45159 /*-Unknown-*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(41138, 0, 0, 0, 0, 0, 0, 0, 0, 'I will call upon you again, I am sure.', 23420), -- Feeding Shal'Aran
(41028, 0, 0, 0, 0, 0, 0, 0, 0, 'I must admit I am impressed. You may actually be of some use to us.', 23420), -- Power Grid
(41298, 2, 0, 0, 0, 0, 0, 0, 0, 'Well met, $r $c. The Valarjar thank you for bringing this fjarnskaggl.', 23420), -- Supplies Needed: Fjarnskaggl
(43519, 1, 0, 0, 0, 0, 0, 0, 0, 'Well done! Everything went as planned.', 23420), -- Lucid Strength
(43514, 1, 0, 0, 0, 0, 0, 0, 0, 'Well done. We can use this to disable the magic protecting the Heart.', 23420), -- A Vainglorious Past
(45159, 273, 0, 0, 0, 0, 0, 0, 0, '<Dariness peers at the artifact you hand her.>$b$bA most interesting design! It reminds me of an ancient curio I read about long ago while still in training. I doubt it could be that though. Such a device has never been found.$b$bHmm, I wonder if it still works. Only one way to find out...', 23420); -- -Unknown-

DELETE FROM `quest_poi` WHERE (`QuestID`=43325 AND `BlobIndex`=0 AND `Idx1`=2);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(43325, 0, 2, 32, 0, 0, 1220, 1015, 0, 0, 2, 0, 0, 0, 23420); -- Ley Race

UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=285169, `QuestObjectID`=109958, `VerifiedBuild`=23420 WHERE (`QuestID`=43325 AND `BlobIndex`=0 AND `Idx1`=1); -- Ley Race
UPDATE `quest_poi` SET `ObjectiveIndex`=0, `QuestObjectiveID`=288519, `QuestObjectID`=110903, `VerifiedBuild`=23420 WHERE (`QuestID`=43325 AND `BlobIndex`=0 AND `Idx1`=0); -- Ley Race
DELETE FROM `quest_poi_points` WHERE (`QuestID`=43325 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=1);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(43325, 2, 0, 488, 5670, 23420), -- Ley Race
(43325, 1, 11, -447, 5409, 23420), -- Ley Race
(43325, 1, 10, -834, 5999, 23420), -- Ley Race
(43325, 1, 9, -705, 6557, 23420), -- Ley Race
(43325, 1, 8, -458, 6955, 23420), -- Ley Race
(43325, 1, 7, 99, 7663, 23420), -- Ley Race
(43325, 1, 6, 765, 7652, 23420), -- Ley Race
(43325, 1, 5, 1258, 7287, 23420), -- Ley Race
(43325, 1, 4, 1333, 6869, 23420), -- Ley Race
(43325, 1, 3, 1194, 6375, 23420), -- Ley Race
(43325, 1, 2, 1054, 6010, 23420), -- Ley Race
(43325, 1, 1, 604, 5516, 23420); -- Ley Race

UPDATE `quest_poi_points` SET `X`=-7, `Y`=5377, `VerifiedBuild`=23420 WHERE (`QuestID`=43325 AND `Idx1`=1 AND `Idx2`=0); -- Ley Race
UPDATE `quest_poi_points` SET `X`=486, `Y`=5667, `VerifiedBuild`=23420 WHERE (`QuestID`=43325 AND `Idx1`=0 AND `Idx2`=0); -- Ley Race

DELETE FROM `quest_greeting` WHERE (`ID`=108072 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(108072, 0, 0, 0, 'What-ho traveller! What brings you to these dusty ruins?', 23420); -- 108072

DELETE FROM `quest_details` WHERE `ID` IN (41138 /*Feeding Shal'Aran*/, 43521 /*Essence of Power*/, 43520 /*In Nightmares*/, 43519 /*Lucid Strength*/, 44176 /*The Conveniences of Home*/, 41167 /*Fel Fragments*/, 45160 /*-Unknown-*/, 45159 /*-Unknown-*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(41138, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Feeding Shal'Aran
(43521, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- Essence of Power
(43520, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- In Nightmares
(43519, 1, 1, 1, 0, 0, 0, 0, 0, 23420), -- Lucid Strength
(44176, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Conveniences of Home
(41167, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Fel Fragments
(45160, 1, 6, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45159, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- -Unknown-

DELETE FROM `quest_request_items` WHERE `ID` IN (41298 /*Supplies Needed: Fjarnskaggl*/, 43514 /*A Vainglorious Past*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(41298, 0, 0, 0, 0, 'Yes?', 23420), -- Supplies Needed: Fjarnskaggl
(43514, 1, 0, 0, 0, 'It may take some effort to convince Farondis'' people to part with the draught; some among them still cling desperately to their past lives.', 23420); -- A Vainglorious Past

UPDATE `creature_model_info` SET `BoundingRadius`=0.5745, `CombatReach`=2.25, `VerifiedBuild`=23420 WHERE `DisplayID`=68559;
UPDATE `creature_model_info` SET `BoundingRadius`=1.740532, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=70876;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8929112, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=69422;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7, `VerifiedBuild`=23420 WHERE `DisplayID`=30213;
UPDATE `creature_model_info` SET `BoundingRadius`=1.35, `VerifiedBuild`=23420 WHERE `DisplayID`=73491;
UPDATE `creature_model_info` SET `BoundingRadius`=3.461204, `CombatReach`=4, `VerifiedBuild`=23420 WHERE `DisplayID`=68066;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8929112, `CombatReach`=2, `VerifiedBuild`=23420 WHERE `DisplayID`=68959;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=60541;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=60536;
UPDATE `creature_model_info` SET `BoundingRadius`=0.891, `CombatReach`=1.35, `VerifiedBuild`=23420 WHERE `DisplayID`=72979;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `CombatReach`=1, `VerifiedBuild`=23420 WHERE `DisplayID`=68129;
UPDATE `creature_model_info` SET `BoundingRadius`=0.637418, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=54194;
UPDATE `creature_model_info` SET `BoundingRadius`=1.384481, `CombatReach`=1.6, `VerifiedBuild`=23420 WHERE `DisplayID`=67694;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=69696;
UPDATE `creature_model_info` SET `BoundingRadius`=1.14659, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=67552;
UPDATE `creature_model_info` SET `CombatReach`=1.5 WHERE `DisplayID`=64535;
UPDATE `creature_model_info` SET `BoundingRadius`=5.652493, `CombatReach`=4, `VerifiedBuild`=23420 WHERE `DisplayID`=65258;
UPDATE `creature_model_info` SET `BoundingRadius`=1.724737, `CombatReach`=2.4, `VerifiedBuild`=23420 WHERE `DisplayID`=66336;
UPDATE `creature_model_info` SET `BoundingRadius`=5.652493, `CombatReach`=4, `VerifiedBuild`=23420 WHERE `DisplayID`=64987;
UPDATE `creature_model_info` SET `BoundingRadius`=0.75, `CombatReach`=0.75, `VerifiedBuild`=23420 WHERE `DisplayID`=71964;
UPDATE `creature_model_info` SET `BoundingRadius`=2.25, `CombatReach`=7.5, `VerifiedBuild`=23420 WHERE `DisplayID`=73996;
UPDATE `creature_model_info` SET `BoundingRadius`=1.078632 WHERE `DisplayID`=32546;
UPDATE `creature_model_info` SET `BoundingRadius`=2.225214, `CombatReach`=1.5, `VerifiedBuild`=23420 WHERE `DisplayID`=71940;
UPDATE `creature_model_info` SET `BoundingRadius`=0.7, `CombatReach`=0.875, `VerifiedBuild`=23420 WHERE `DisplayID`=32789;
UPDATE `creature_model_info` SET `BoundingRadius`=5.749122, `VerifiedBuild`=23420 WHERE `DisplayID`=66850;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6640207, `CombatReach`=0.54, `VerifiedBuild`=23420 WHERE `DisplayID`=69273;
UPDATE `creature_model_info` SET `BoundingRadius`=0.75, `CombatReach`=0.9375, `VerifiedBuild`=23420 WHERE `DisplayID`=70400;
UPDATE `creature_model_info` SET `BoundingRadius`=1.65039, `VerifiedBuild`=23420 WHERE `DisplayID`=68393;

UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112939; -- Arkethrax
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112895; -- Baelinar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112894; -- Anostronoth
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112893; -- Varudis
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112929; -- Kathra'natir
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_walk`=0.888, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112932; -- Oublion
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112931; -- Azoran
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112941; -- Soulscreech Imp
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112936; -- Lord Hel'nurath
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1768, `speed_walk`=0.777776, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23420 WHERE `entry`=112938; -- Gorgonnash
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2838, `npcflag`=8192, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111323; -- Izal Whitemoon
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2846, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=105096; -- Demonsaber
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2838, `npcflag`=4225, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111324; -- Falara Nightsong
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2846, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111319; -- Illidari Enforcer
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2843, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=111320; -- Demon Hunter
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2843, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=111317; -- Demon Hunter
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=536903680, `unit_flags2`=34817, `VerifiedBuild`=23420 WHERE `entry`=104685; -- Kell
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=536871680, `unit_flags2`=67110913, `VerifiedBuild`=23420 WHERE `entry`=100953; -- Jandvik Splintershield
UPDATE `creature_template` SET `unit_flags`=537133824, `VerifiedBuild`=23420 WHERE `entry`=104600; -- Sashj'tar Scaleflayer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2205, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=16779264, `VerifiedBuild`=23420 WHERE `entry`=102840; -- Commander Raz'jira
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=16779264, `VerifiedBuild`=23420 WHERE `entry`=104459; -- Seawarden Largush
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=74, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102828; -- Sashj'tar Sandcrusher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `npcflag`=4227, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100017; -- Stokalfr
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102410; -- Toryl
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `npcflag`=66177, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112226; -- Markus Hjolbruk
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=34816, `VerifiedBuild`=23420 WHERE `entry`=104550; -- Brytag
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102845; -- Brandolf
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `npcflag`=2, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100015; -- Katarine
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=100946; -- Jandvik Runecaller
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2205, `speed_run`=1, `BaseAttackTime`=667, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=100947; -- Sashj'tar Reef Runner
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=100945; -- Jandvik Splintershield
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=74, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=536871680, `unit_flags2`=67110913, `VerifiedBuild`=23420 WHERE `entry`=103474; -- Sashj'tar Diviner
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `npcflag`=4224, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102660; -- Jandvik Metalsmith
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100948; -- Jandvik Bonepiercer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2205, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=536871680, `unit_flags2`=67110913, `VerifiedBuild`=23420 WHERE `entry`=99637; -- Sashj'tar Reef Runner
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102551; -- Jandvik Splintershield
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2811, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100891; -- Jandvik Bonepiercer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2810, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102738; -- Calder
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100890; -- Jandvik Ripfang
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100889; -- Jandvik Runecaller
UPDATE `creature_template` SET `faction`=31, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=101073; -- Jandvik Rabbit
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2809, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100888; -- Jandvik Splintershield
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=112972; -- Giant Thicketstomper
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=116421; -- Ley Line Channeler
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2804, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33280, `unit_flags2`=33622016, `VerifiedBuild`=23420 WHERE `entry`=112539; -- Marius Felbane
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=67584, `VehicleId`=4936, `VerifiedBuild`=23420 WHERE `entry`=90139; -- Inquisitor Ernstenbok
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2804, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33280, `unit_flags2`=33622016, `VerifiedBuild`=23420 WHERE `entry`=112536; -- Tehd Shoemaker
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=90140; -- Eye of Ernstenbok
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049 WHERE `entry`=95073; -- Forsaken Dreadwing
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=107881; -- Tideskorn Beastbreaker
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=71, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103307; -- Forsaken Plaguebringer
UPDATE `creature_template` SET `HoverHeight`=1, `VerifiedBuild`=23420 WHERE `entry`=111787; -- Great Sea Ray
UPDATE `creature_template` SET `HoverHeight`=1, `VerifiedBuild`=23420 WHERE `entry`=111408; -- Great Sea Snake
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=71, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=104290; -- Captain Grimshanks
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=71, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103215; -- Forsaken Deathwarder
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111463; -- Bulvinkel
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=536904448, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=107822; -- Vrykul Shepherd
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=2000, `speed_run`=1.385714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=536904448, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=107663; -- Domesticated Goat
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `HoverHeight`=5, `VerifiedBuild`=23420 WHERE `entry`=115885; -- Young Storm Drake
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=536904448, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=107643; -- Vrykul Mariner
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2817, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116180; -- Mist Seer
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2817, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116600; -- Cursed Sea Dog
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=536871680, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=107919; -- Vrykul Salvager
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2817, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115846; -- Vaettfang
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2817, `speed_run`=1.071429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=116601; -- Helarjar Marauder
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=108187; -- Tomb Bat
UPDATE `creature_template` SET `faction`=35 WHERE `entry`=109110; -- Tracking Hound
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=109083; -- Houndmaster Payne
UPDATE `creature_template` SET `npcflag`=66177 WHERE `entry`=98112; -- Steward Dayton
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=110973; -- Worthy Vrykul
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=108890; -- Runewood Greatstag
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=33556480 WHERE `entry`=93095; -- Voracious Bear
UPDATE `creature_template` SET `npcflag`=16777216, `unit_flags`=33587968, `VerifiedBuild`=23420 WHERE `entry`=91818; -- Unworthy Soul
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=109795; -- Neglected Bones
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=1.6, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115263; -- Fallen Stormforged
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=1, `speed_run`=1.289683, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=100067; -- Hydrannon
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=107758; -- Amberfall Greatstag
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049 WHERE `entry`=107803; -- Wild Plains Runehorn
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=92471; -- Skyfire Engineer
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=1, `VerifiedBuild`=23420 WHERE `entry`=91085; -- Skyfire Marine
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=96135; -- Felskorn Warmonger
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3, `VerifiedBuild`=23420 WHERE `entry`=96282; -- Vault Guardian
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=106565; -- Blackfeather Gatherer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=33556480, `VerifiedBuild`=23420 WHERE `entry`=106611; -- Vadrak
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1917, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=18432, `VehicleId`=4880, `HoverHeight`=4, `VerifiedBuild`=23420 WHERE `entry`=109994; -- Stormtalon
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=95748; -- Katterin the Blistered
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2000, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=18432, `VehicleId`=4878, `VerifiedBuild`=23420 WHERE `entry`=109942; -- Storm's Reach Greatstag
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=103, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=95224; -- Dyna
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=3, `VerifiedBuild`=23420 WHERE `entry`=102843; -- Aegira
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=2, `VerifiedBuild`=23420 WHERE `entry`=102848; -- Slifna
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2000, `speed_run`=1.385714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=18432, `VehicleId`=4771, `VerifiedBuild`=23420 WHERE `entry`=107020; -- Storm's Reach Cliffwalker
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=1711, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VehicleId`=4780, `VerifiedBuild`=23420 WHERE `entry`=107463; -- Storm's Reach Worg
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=1711, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=18432, `VehicleId`=4879, `VerifiedBuild`=23420 WHERE `entry`=109967; -- Storm's Reach Warbear
UPDATE `creature_template` SET `HoverHeight`=1, `VerifiedBuild`=23420 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=91956; -- Guthrie
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=91954; -- Beagan
UPDATE `creature_template` SET `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=110534; -- Provisioner Sheldon
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VehicleId`=4583, `VerifiedBuild`=23420 WHERE `entry`=102633; -- Waxeye Glazer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103235; -- Taskmaster Filthwhisker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103234; -- Taskmaster Skraggletail
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103178; -- Gunksnout Scavenger
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=14881; -- Spider
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2788, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102205; -- Pathfinder Linny
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=1.111111, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=101649; -- Frostshard
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102123; -- Ronir Wrangler
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=93021; -- Kubrul
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=14, `speed_walk`=1.555556, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=104243; -- Bound Elemental
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=94691; -- Overgrown Larva
UPDATE `creature_template` SET `BaseAttackTime`=1600, `unit_flags`=32768 WHERE `entry`=94386; -- Cursed Elderhorn
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=94149; -- Pinerock Prowler
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=99481; -- Pinerock Stalker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67635200, `VerifiedBuild`=23420 WHERE `entry`=103308; -- Scythe of the Unseen Path
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103721; -- Lee Olesky
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103719; -- Kew Wyldheart
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103716; -- Nathaniel Beastbreaker
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103715; -- Loric Fielding
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100530; -- Tyrathan Khort
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100447; -- Tamed Black Bear
UPDATE `creature_template` SET `minlevel`=-109, `maxlevel`=0, `faction`=7, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=131072, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113862; -- Training Dummy
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=110799; -- Gedrah
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100699; -- Nel
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100698; -- Sif
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100697; -- Nimi Brightcastle
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=18432, `VerifiedBuild`=23420 WHERE `entry`=102646; -- Snowfeather
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102578; -- Emmarel Shadewarden
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=8192, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=98968; -- Odan Battlebow
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100488; -- Sentry Pierce
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=3, `VerifiedBuild`=23420 WHERE `entry`=109572; -- Great Eagle
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67635200, `VerifiedBuild`=23420 WHERE `entry`=102694; -- Visage of Ohn'ahra
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=98731; -- Leokk
UPDATE `creature_template` SET `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67635200, `VerifiedBuild`=23420 WHERE `entry`=103371; -- Remembrance of the Fallen
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `speed_run`=1.289683, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100702; -- Scout Brightspear
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=110789; -- Kettric
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=100709; -- Sentry Seamster
UPDATE `creature_template` SET `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=113391; -- Protected Owl
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=16384, `VerifiedBuild`=23420 WHERE `entry`=111582; -- Fevered Explorer
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=94151; -- Pinerock Elderhorn
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=96146; -- Bristlefur Bear
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=98890; -- Slumber
UPDATE `creature_template` SET `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=95253; -- Stonedark Drogbar
UPDATE `creature_template` SET `speed_run`=0.2264286, `VerifiedBuild`=23420 WHERE `entry`=107130; -- Various and Sundry
UPDATE `creature_template` SET `speed_run`=0.2835714, `VerifiedBuild`=23420 WHERE `entry`=107129; -- Glip Gloop Haberdasher
UPDATE `creature_template` SET `speed_run`=0.07928572, `VerifiedBuild`=23420 WHERE `entry`=107128; -- Helix for Brains
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=96410; -- Majestic Elderhorn
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=104531; -- Hexxing Fetish
UPDATE `creature_template` SET `faction`=14, `unit_flags`=0, `HoverHeight`=1, `VerifiedBuild`=23420 WHERE `entry`=98356; -- Corrupted Great Eagle
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100 WHERE `entry`=107415; -- Oren's Rappel Point
UPDATE `creature_template` SET `unit_flags`=67108864, `VerifiedBuild`=23420 WHERE `entry`=94986; -- Fleshrender Roc
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=7, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `HoverHeight`=2, `VerifiedBuild`=23420 WHERE `entry`=102745; -- Silverskin Remora
UPDATE `creature_template` SET `faction`=188, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102749; -- Golden Kelpdweller
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1700, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=16779264, `VerifiedBuild`=23420 WHERE `entry`=103633; -- King Mrgl-Mrgl
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1700, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=16779264, `VerifiedBuild`=23420 WHERE `entry`=103632; -- Rockaway Murloc
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=7, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102741; -- Highcliff Gull
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2102, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=104590; -- Clackbrine Matron
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2102, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=104589; -- Clackbrine Pincer
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=98046; -- Swamprock Tadpole
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=115927; -- Snowfeather Matriarch's Nest
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115673; -- Snowfeather Matriarch
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=95290; -- Feltotem Warmonger
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=115677; -- Young Snowfeather
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=115665; -- Snowfeather Nest-Keeper
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=115679; -- Snowfeather Nest
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23420 WHERE `entry`=91629; -- Illidari Enforcer
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=115937; -- Goal Energy Field
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=116510; -- Launch Up Field
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=71321600, `VerifiedBuild`=23420 WHERE `entry`=115935; -- Energy Field
UPDATE `creature_template` SET `maxlevel`=100, `VerifiedBuild`=23420 WHERE `entry`=103085; -- Fledgling Druid
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=106340; -- Maelisandra Venitox
UPDATE `creature_template` SET `unit_flags`=33587200 WHERE `entry`=100358; -- Oro Junior
UPDATE `creature_template` SET `speed_run`=1.15, `VerifiedBuild`=23420 WHERE `entry`=94846; -- Bitestone Rockcrusher
UPDATE `creature_template` SET `unit_flags`=32832, `VerifiedBuild`=23420 WHERE `entry`=108014; -- Azureback Broodmother
UPDATE `creature_template` SET `speed_run`=1.142857, `VerifiedBuild`=23420 WHERE `entry`=97236; -- Burrow Spiderling
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=96548; -- Rabbit
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103227; -- Wax Miner
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103179; -- Gunksnout Geomancer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103177; -- Gunksnout Runt
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VehicleId`=4684, `VerifiedBuild`=23420 WHERE `entry`=104690; -- Beamer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33849344, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=102634; -- Scalewhisker Cavalry
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33570816, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=103197; -- Kill Credit: Kobold Candle
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=103196; -- Kobold Candle
UPDATE `creature_template` SET `minlevel`=107, `maxlevel`=107 WHERE `entry`=106271; -- Jarum Skymane
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=103514; -- Leystone Basilisk
UPDATE `creature_template` SET `HoverHeight`=3 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=23420 WHERE `entry`=97380; -- Splint
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `VerifiedBuild`=23420 WHERE `entry`=114662; -- Kayvon Quinnstar
UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0, `VerifiedBuild`=23420 WHERE `entry`=114665; -- Sergeant Wilson
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=188, `npcflag`=1073741824, `VerifiedBuild`=23420 WHERE `entry`=110666; -- Young Mutant Warturtle
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=113155; -- Roaster Rat
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=108, `speed_walk`=1 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=106, `speed_run`=1.190476 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `speed_run`=1.142857 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=107 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=108, `speed_run`=1.142857 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=104 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `npcflag`=1073741824, `VerifiedBuild`=23420 WHERE `entry`=105241; -- Splint Jr.
UPDATE `creature_template` SET `maxlevel`=109 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=23420 WHERE `entry`=93531; -- Enchanter Nalthanis
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=23420 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `npcflag`=82, `VerifiedBuild`=23420 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=92617; -- Bradensbrook Villager
UPDATE `creature_template` SET `unit_flags`=537133824, `unit_flags2`=2049 WHERE `entry`=93064; -- Black Rook Falcon
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=108876; -- Risen Vanguard
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=103, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=95222; -- Sparkly
UPDATE `creature_template` SET `speed_run`=1.142857 WHERE `entry`=114676; -- Risen Vanguard
UPDATE `creature_template` SET `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=100409; -- Dusky Howler
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=97548; -- Shala'nir Druid
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=97565; -- Nightmare Totem
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1, `VerifiedBuild`=23420 WHERE `entry`=113646; -- Defiled Grovewalker
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `speed_walk`=1.6, `speed_run`=0.7142857, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=67127296, `VerifiedBuild`=23420 WHERE `entry`=116456; -- Barrels o' Fun
UPDATE `creature_template` SET `speed_walk`=1.111112, `speed_run`=1.142857, `unit_flags`=32768 WHERE `entry`=94014; -- Ancient Protector
UPDATE `creature_template` SET `npcflag`=18, `VerifiedBuild`=23420 WHERE `entry`=98135; -- Wildcrafter Osme
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=109382; -- Val'sharah Druid
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=109364; -- Val'sharah Druid
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=109819; -- Wild Dreamrunner
UPDATE `creature_template` SET `unit_flags`=33024 WHERE `entry`=93314; -- Gleamhoof Doe
UPDATE `creature_template` SET `unit_flags`=33024, `VerifiedBuild`=23420 WHERE `entry`=93313; -- Gleamhoof Stag
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=111509; -- Arcane Servitor
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115075; -- Doomed Shal'dorei Civilian
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115003; -- Doomed Shal'dorei Civilian
UPDATE `creature_template` SET `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VehicleId`=5086, `VerifiedBuild`=23420 WHERE `entry`=116659; -- Felsoul Ferry
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115002; -- Maribeth
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115000; -- Doomed Shal'dorei Civilian
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115074; -- Doomed Shal'dorei Civilian
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115073; -- Doomed Shal'dorei Civilian
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2851, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115014; -- Felsoul Imp
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2855, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=115071; -- Doomed Shal'dorei Civilian
UPDATE `creature_template` SET `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=116411; -- Soul Engine Gateway
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2851, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=114998; -- Aargoss
UPDATE `creature_template` SET `unit_flags`=570458368 WHERE `entry`=114892; -- Withering Civilian
UPDATE `creature_template` SET `unit_flags`=570458368 WHERE `entry`=114888; -- Shal'dorei Civilian
UPDATE `creature_template` SET `unit_flags`=570458368 WHERE `entry`=114889; -- Shal'dorei Civilian
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=23420 WHERE `entry`=93531; -- Enchanter Nalthanis
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95888; -- Cordana Felsong
UPDATE `creature_template` SET `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=97678; -- Aranasi Broodmother
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=96015; -- Inquisitor Tormentorum
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=100525; -- Glowing Sentry
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95886; -- Ash'Golm
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95885; -- Tirathon Saltheril
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `VerifiedBuild`=23420 WHERE `entry`=95887; -- Glazer
UPDATE `creature_template` SET `HealthModifier`=11.05, `VerifiedBuild`=23420 WHERE `entry`=98177; -- Glayvianna Soulrender
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049, `VerifiedBuild`=23420 WHERE `entry`=93319; -- Ashmaw Cub
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049 WHERE `entry`=93318; -- Ashmaw Mauler
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=91288; -- Smolderhide Warrior
UPDATE `creature_template` SET `unit_flags`=33587200, `VerifiedBuild`=23420 WHERE `entry`=93680; -- Druid of the Claw
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=101 WHERE `entry`=106772; -- Exotic Book
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=106920; -- Feathered Prowler
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=89801; -- Withered Scavenger
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=105959; -- Ley Siphon
UPDATE `creature_template` SET `faction`=2136 WHERE `entry`=109826; -- Nightfallen Hungerer
UPDATE `creature_template` SET `npcflag`=16777216, `VerifiedBuild`=23420 WHERE `entry`=105944; -- Prepared Mana Dust
UPDATE `creature_template` SET `speed_walk`=2.8, `speed_run`=1.285714, `BaseAttackTime`=2000 WHERE `entry`=90057; -- Daggerbeak
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `BaseAttackTime`=4000 WHERE `entry`=88090; -- Fathom-Commander Zarrin
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23420 WHERE `entry`=98159; -- Alynblaze
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=88797; -- Elder Aldryth
UPDATE `creature_template` SET `speed_run`=1.73611, `BaseAttackTime`=1333, `unit_flags`=32768, `VerifiedBuild`=23420 WHERE `entry`=89082; -- Ooker Dooker
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=91188; -- Son of Beacher
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=16, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=91187; -- Beacher
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23420 WHERE `entry`=106630; -- Burrowing Leyworm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_walk`=1.2, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23420 WHERE `entry`=112120; -- Nightmare Essence
UPDATE `creature_template` SET `npcflag`=131, `VerifiedBuild`=23420 WHERE `entry`=99420; -- Kharmeera
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=90336; -- Azurewing Whelpling
UPDATE `creature_template` SET `npcflag`=3 WHERE `entry`=100482; -- Senegos
UPDATE `creature_template` SET `speed_walk`=0.4, `speed_run`=0.4, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=91579; -- Doomlord Kazrok
UPDATE `creature_template` SET `npcflag`=129 WHERE `entry`=93973; -- Leyweaver Phaxondus
UPDATE `creature_template` SET `minlevel`=107, `maxlevel`=107 WHERE `entry`=90622; -- Cordana Felsong
UPDATE `creature_template` SET `unit_flags`=67108864 WHERE `entry`=90803; -- Infernal Lord
UPDATE `creature_template` SET `speed_walk`=0.444, `speed_run`=0.5, `VerifiedBuild`=23420 WHERE `entry`=86535; -- Overseer Lykill
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=33556480 WHERE `entry`=89278; -- Demon Hunter
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23420 WHERE `entry`=91629; -- Illidari Enforcer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=108212; -- Second Mate Cilieth
UPDATE `creature_template` SET `unit_flags`=33280 WHERE `entry`=89290; -- Queen's Reprisal Sailor
UPDATE `creature_template` SET `unit_flags`=32784, `VerifiedBuild`=23420 WHERE `entry`=109349; -- Veil Shadowrunner
UPDATE `creature_template` SET `unit_flags`=295680 WHERE `entry`=112874; -- Landon Doi
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=111767; -- Shadowflame Fiend
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=2.182543, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=111939; -- Lysanis Shadesoul
UPDATE `creature_template` SET `maxlevel`=111, `VerifiedBuild`=23420 WHERE `entry`=111929; -- Felsworn Defiler
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1, `unit_flags`=32768 WHERE `entry`=111768; -- Felfire Fiend
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=108163; -- Scumshell Crab
UPDATE `creature_template` SET `maxlevel`=109 WHERE `entry`=112079; -- Crimson Pilgrim
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=108, `speed_run`=1.142857 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=106, `speed_run`=1.190476 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=107 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `speed_run`=1.142857 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=108, `speed_walk`=1 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=97163; -- Cursed Falke
UPDATE `creature_template` SET `HoverHeight`=1, `VerifiedBuild`=23420 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `faction`=35, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=34816, `VerifiedBuild`=23420 WHERE `entry`=113295; -- Traveler's Banking Chest
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2848, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=110386; -- Spellwyrm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2848, `speed_run`=1.190476, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112411; -- Psillych
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.190476, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=112412; -- Psillych Spiderling
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2822, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23420 WHERE `entry`=110383; -- Hungering Husk
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_run`=1, `BaseAttackTime`=6000, `RangeAttackTime`=2000, `unit_class`=1, `VerifiedBuild`=23420 WHERE `entry`=114267; -- Ley Spider Egg
UPDATE `creature_template` SET `unit_flags2`=2099200, `VerifiedBuild`=23420 WHERE `entry`=110041; -- Chief Telemancer Oculeth
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=113126; -- Meredil Lockjaw
UPDATE `creature_template` SET `speed_run`=1.385714 WHERE `entry`=114838; -- Tyrande Whisperwind
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=112972; -- Giant Thicketstomper
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5, `unit_flags`=294912, `VerifiedBuild`=23420 WHERE `entry`=112530; -- Garion
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=109023; -- Shadescale Flyeater
UPDATE `creature_template` SET `speed_run`=1, `unit_flags`=33024 WHERE `entry`=114897; -- Darnassus Sentinel
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry`=101848; -- Absolon
UPDATE `creature_template` SET `HoverHeight`=3 WHERE `entry`=88989; -- Violet Firefly
UPDATE `creature_template` SET `npcflag`=1073741824, `VerifiedBuild`=23420 WHERE `entry`=105241; -- Splint Jr.
UPDATE `creature_template` SET `unit_flags`=768 WHERE `entry`=102263; -- Skorpyron
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=23420 WHERE `entry`=93542; -- Tanithria
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110 WHERE `entry`=110738; -- Kaela Grimlocket
UPDATE `creature_template` SET `npcflag`=82, `VerifiedBuild`=23420 WHERE `entry`=93525; -- Ainderu Summerleaf
UPDATE `creature_template` SET `minlevel`=104 WHERE `entry`=107622; -- Glutonia
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=23420 WHERE `entry`=93531; -- Enchanter Nalthanis
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=107 WHERE `entry`=96636; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=107 WHERE `entry`=96635; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=108, `speed_run`=1.142857 WHERE `entry`=96641; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=106, `speed_run`=1.190476 WHERE `entry`=96639; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `speed_run`=1.142857 WHERE `entry`=96592; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=108, `speed_walk`=1 WHERE `entry`=96643; -- Stabled Hunter Pet
UPDATE `creature_template` SET `maxlevel`=109 WHERE `entry`=112079; -- Crimson Pilgrim

UPDATE `creature_questitem` SET `ItemId`=134129, `VerifiedBuild`=23420 WHERE (`CreatureEntry`=95043 AND `Idx`=2); -- Young Mountainstrider
UPDATE `creature_questitem` SET `ItemId`=129901, `VerifiedBuild`=23420 WHERE (`CreatureEntry`=95043 AND `Idx`=1); -- Young Mountainstrider

DELETE FROM `gameobject_template` WHERE `entry` IN (266488 /*Hot Sour Goat Milk*/, 252166 /*The Hunter's Guide to Being Useful*/, 250894 /*Training Troops*/, 250976 /*Cookpot*/, 250965 /*Campfire*/, 250896 /*Training Troops*/, 252675 /*Superior Challenger's Cache*/, 252676 /*Peerless Challenger's Cache*/, 268231 /*Doodad_7DU_Warden_DoorMedium008*/, 251655 /*Moonblade*/, 251183 /*Ephemeral Crystal*/, 266996 /*Marching Orders*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(266488, 10, 16171, 'Hot Sour Goat Milk', '', '', '', 1, 0, 0, 0, 3000, 0, 1, 0, 0, 0, 0, 231260, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Hot Sour Goat Milk
(252166, 9, 18715, 'The Hunter''s Guide to Being Useful', '', '', '', 0.5, 5331, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Hunter's Guide to Being Useful
(250894, 45, 9510, 'Training Troops', '', '', '', 1, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Training Troops
(250976, 8, 32263, 'Cookpot', '', '', '', 0.53, 4, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Cookpot
(250965, 8, 16847, 'Campfire', '', '', '', 1.02, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Campfire
(250896, 45, 9510, 'Training Troops', '', '', '', 1, 145, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Training Troops
(252675, 3, 33268, 'Superior Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1818, 0, 1, 0, 0, 68472, 0, 5, 110, 23420), -- Superior Challenger's Cache
(252676, 3, 33268, 'Peerless Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1818, 0, 1, 0, 0, 68473, 0, 5, 110, 23420), -- Peerless Challenger's Cache
(268231, 0, 28523, 'Doodad_7DU_Warden_DoorMedium008', '', '', '', 1.75, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Doodad_7DU_Warden_DoorMedium008
(251655, 10, 28285, 'Moonblade', '', '', '', 1, 0, 42924, 0, 0, 0, 1, 0, 0, 0, 0, 217430, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Moonblade
(251183, 10, 15703, 'Ephemeral Crystal', '', '', '', 0.3, 0, 0, 0, 3000, 0, 0, 0, 0, 0, 0, 215325, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 40802, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Ephemeral Crystal
(266996, 10, 30915, 'Marching Orders', 'questinteract', 'Burning', '', 0.75, 1691, 0, 0, 3000, 0, 1, 0, 0, 0, 0, 145803, 0, 0, 0, 30602, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- Marching Orders
