
DELETE FROM `scene_template` WHERE (`SceneId`=1541 AND `ScriptPackageID`=1781) OR (`SceneId`=1539 AND `ScriptPackageID`=1779) OR (`SceneId`=130 AND `ScriptPackageID`=291) OR (`SceneId`=129 AND `ScriptPackageID`=287) OR (`SceneId`=128 AND `ScriptPackageID`=286) OR (`SceneId`=127 AND `ScriptPackageID`=285);
INSERT INTO `scene_template` (`SceneId`, `Flags`, `ScriptPackageID`) VALUES
(1541, 27, 1781),
(1539, 27, 1779),
(130, 0, 291),
(129, 0, 287),
(128, 0, 286),
(127, 0, 285);


DELETE FROM `quest_offer_reward` WHERE `ID` IN (41189 /*-Unknown-*/, 32325 /*Infiltrating the Black Temple*/, 32340 /*Plunder the Black Temple*/, 32324 /*Seek the Signal*/, 32317 /*Seeking the Soulstones*/, 32309 /*A Tale of Six Masters*/, 32307 /*Reader for the Dead Tongue*/, 32295 /*An Unusual Tome*/, 44164 /*-Unknown-*/, 29681 /*Maximum Security Breakout*/, 29675 /*Hey There Dalliah*/, 29674 /*Unbound Darkness*/, 40168 /*The Swirling Vial*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(41189, 0, 0, 0, 0, 0, 0, 0, 0, 'There''s more to this land than I ever realized, $n. Now listen...', 23222), -- -Unknown-
(32325, 0, 0, 0, 0, 0, 0, 0, 0, 'You have done more for this world than you can know, Warlock. It is... something of a shame Kanrethad met this end. A shame, but inevitable.', 23222), -- Infiltrating the Black Temple
(32340, 0, 0, 0, 0, 0, 0, 0, 0, 'You''ve looted everything you could grab on your way up. Time to tally up your plunder and hide it before Akama catches up...', 23222), -- Plunder the Black Temple
(32324, 0, 0, 0, 0, 0, 0, 0, 0, 'Jubeka''s soulstone has lead you to the Black Temple. The only thing left to do is to find a way inside...', 23222), -- Seek the Signal
(32317, 0, 0, 0, 0, 0, 0, 0, 0, 'You''ve collected all four fragments of Jubeka''s soulstone.$B$BYou just need need to fuse it together now...', 23222), -- Seeking the Soulstones
(32309, 0, 0, 0, 0, 0, 0, 0, 0, 'You are ready to learn more...', 23222), -- A Tale of Six Masters
(32307, 0, 0, 0, 0, 0, 0, 0, 0, 'What... where in the great dark did you find this?', 23222), -- Reader for the Dead Tongue
(32295, 0, 0, 0, 0, 0, 0, 0, 0, 'The codex appears to be a powerful tome written in an ancient, pre-demonic tongue. You strongly suspect this should be brought to the attention of the Warlock trainers in your capital city.', 23222), -- An Unusual Tome
(44164, 0, 0, 0, 0, 0, 0, 0, 0, 'Ah, $n, you have returned. An otherworldly air hangs heavy about you.', 23222), -- -Unknown-
(29681, 0, 0, 0, 0, 0, 0, 0, 0, 'Thank you, $n. Your actions this day have prevented countless tragedies.', 23222), -- Maximum Security Breakout
(29675, 0, 0, 0, 0, 0, 0, 0, 0, 'The Legion seeks only to consume. They hold no regard for the balance.', 23222), -- Hey There Dalliah
(29674, 0, 0, 0, 0, 0, 0, 0, 0, 'Creatures of the void are naturally chaotic. They are a necessary part of the universe, but they must be kept in check by the Light.', 23222), -- Unbound Darkness
(40168, 1, 1, 5, 0, 0, 0, 0, 0, 'Oh, interesting! $b$b<Cupri quickly grabs the vial and studies it intensely.>  $b$bPerhaps Zeph was right to keep investigating here. This vial certainly didn''t originate from any currently known timeline. Most curious! Thank you for bringing it to me. Or did you already bring it once before?', 23222); -- The Swirling Vial



UPDATE `quest_poi` SET `ObjectiveIndex`=32, `QuestObjectiveID`=0, `QuestObjectID`=0, `WoDUnk1`=1128841, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=3); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=282031, `QuestObjectID`=103756, `WoDUnk1`=1130018, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=2); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=282029, `QuestObjectID`=103754, `WoDUnk1`=1130020, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-
UPDATE `quest_poi` SET `ObjectiveIndex`=32, `QuestObjectiveID`=0, `QuestObjectID`=0, `WoDUnk1`=1128841, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=3); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=282031, `QuestObjectID`=103756, `WoDUnk1`=1130018, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=2); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=282029, `QuestObjectID`=103754, `WoDUnk1`=1130020, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-
UPDATE `quest_poi` SET `ObjectiveIndex`=32, `QuestObjectiveID`=0, `QuestObjectID`=0, `WoDUnk1`=1128841, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=3); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=282031, `QuestObjectID`=103756, `WoDUnk1`=1130018, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=2); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=282029, `QuestObjectID`=103754, `WoDUnk1`=1130020, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-

UPDATE `quest_poi_points` SET `X`=4118, `Y`=4373, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=3 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=4787, `Y`=4798, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=2 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=5330, `Y`=4268, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=4118, `Y`=4373, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=3 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=4787, `Y`=4798, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=2 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=5330, `Y`=4268, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=4118, `Y`=4373, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=3 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=4787, `Y`=4798, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=2 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=5330, `Y`=4268, `VerifiedBuild`=23222 WHERE (`QuestID`=41190 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-

DELETE FROM `quest_details` WHERE `ID` IN (41190 /*-Unknown-*/, 41189 /*-Unknown-*/, 32340 /*Plunder the Black Temple*/, 32325 /*Infiltrating the Black Temple*/, 32324 /*Seek the Signal*/, 32317 /*Seeking the Soulstones*/, 28459 /*Stones of Binding*/, 32309 /*A Tale of Six Masters*/, 27402 /*Token of Power*/, 32307 /*Reader for the Dead Tongue*/, 29681 /*Maximum Security Breakout*/, 29675 /*Hey There Dalliah*/, 29674 /*Unbound Darkness*/, 40168 /*The Swirling Vial*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(41190, 1, 1, 0, 0, 0, 0, 0, 0, 23222), -- -Unknown-
(41189, 1, 1, 0, 0, 0, 0, 0, 0, 23222), -- -Unknown-
(32340, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Plunder the Black Temple
(32325, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Infiltrating the Black Temple
(32324, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Seek the Signal
(32317, 1, 0, 0, 0, 0, 0, 0, 0, 23222), -- Seeking the Soulstones
(28459, 1, 0, 0, 0, 0, 0, 0, 0, 23222), -- Stones of Binding
(32309, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- A Tale of Six Masters
(27402, 1, 1, 0, 0, 0, 0, 0, 0, 23222), -- Token of Power
(32307, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Reader for the Dead Tongue
(29681, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Maximum Security Breakout
(29675, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Hey There Dalliah
(29674, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Unbound Darkness
(40168, 1, 5, 0, 0, 0, 0, 0, 0, 23222); -- The Swirling Vial


DELETE FROM `quest_request_items` WHERE `ID`=32317;
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(32317, 0, 0, 0, 0, 'You located all four fragments of the soulstone... now you just need to fuse them all together...', 23222); -- Seeking the Soulstones



UPDATE `creature_model_info` SET `BoundingRadius`=0.7, `CombatReach`=1, `VerifiedBuild`=23222 WHERE `DisplayID`=49044;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1, `VerifiedBuild`=23222 WHERE `DisplayID`=38214;
UPDATE `creature_model_info` SET `BoundingRadius`=11.01058, `CombatReach`=6, `VerifiedBuild`=23222 WHERE `DisplayID`=73528;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=15, `VerifiedBuild`=23222 WHERE `DisplayID`=37786;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2392, `CombatReach`=1.725, `VerifiedBuild`=23222 WHERE `DisplayID`=73339;
UPDATE `creature_model_info` SET `BoundingRadius`=12.5, `CombatReach`=5, `VerifiedBuild`=23222 WHERE `DisplayID`=47651;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5489203, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=69927;
UPDATE `creature_model_info` SET `BoundingRadius`=0.39, `CombatReach`=1, `VerifiedBuild`=23222 WHERE `DisplayID`=69492;
UPDATE `creature_model_info` SET `CombatReach`=1.5625, `VerifiedBuild`=23222 WHERE `DisplayID`=70096;
UPDATE `creature_model_info` SET `CombatReach`=1.515152, `VerifiedBuild`=23222 WHERE `DisplayID`=70101;
UPDATE `creature_model_info` SET `CombatReach`=1.704545, `VerifiedBuild`=23222 WHERE `DisplayID`=70091;
UPDATE `creature_model_info` SET `CombatReach`=1.704545, `VerifiedBuild`=23222 WHERE `DisplayID`=70095;
UPDATE `creature_model_info` SET `CombatReach`=1.260504, `VerifiedBuild`=23222 WHERE `DisplayID`=70039;
UPDATE `creature_model_info` SET `CombatReach`=1.027397, `VerifiedBuild`=23222 WHERE `DisplayID`=70077;
UPDATE `creature_model_info` SET `CombatReach`=1.020408, `VerifiedBuild`=23222 WHERE `DisplayID`=70100;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=3, `VerifiedBuild`=23222 WHERE `DisplayID`=71674;
UPDATE `creature_model_info` SET `CombatReach`=1.704545, `VerifiedBuild`=23222 WHERE `DisplayID`=70036;
UPDATE `creature_model_info` SET `CombatReach`=1.744186, `VerifiedBuild`=23222 WHERE `DisplayID`=70038;
UPDATE `creature_model_info` SET `CombatReach`=1.304348, `VerifiedBuild`=23222 WHERE `DisplayID`=70037;
UPDATE `creature_model_info` SET `CombatReach`=1.260504, `VerifiedBuild`=23222 WHERE `DisplayID`=70078;
UPDATE `creature_model_info` SET `CombatReach`=1.293103, `VerifiedBuild`=23222 WHERE `DisplayID`=70081;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=71015;
UPDATE `creature_model_info` SET `BoundingRadius`=2.973505, `CombatReach`=2.625, `VerifiedBuild`=23222 WHERE `DisplayID`=71813;
UPDATE `creature_model_info` SET `BoundingRadius`=3.461204, `VerifiedBuild`=23222 WHERE `DisplayID`=22321;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6409937, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=16217;
UPDATE `creature_model_info` SET `BoundingRadius`=4.311841, `VerifiedBuild`=23222 WHERE `DisplayID`=65065;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=71445;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=74248;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=69413;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=71472;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=72476;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=71480;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=72477;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=69337;
UPDATE `creature_model_info` SET `BoundingRadius`=0.4687497, `CombatReach`=2.025, `VerifiedBuild`=23222 WHERE `DisplayID`=68544;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=68404;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=71537;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=74333;
UPDATE `creature_model_info` SET `BoundingRadius`=1.504815, `CombatReach`=1.4, `VerifiedBuild`=23222 WHERE `DisplayID`=71894;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=72552;
UPDATE `creature_model_info` SET `BoundingRadius`=30, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=74365;
UPDATE `creature_model_info` SET `BoundingRadius`=1.581009, `VerifiedBuild`=23222 WHERE `DisplayID`=66336;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3519, `CombatReach`=1.725, `VerifiedBuild`=23222 WHERE `DisplayID`=71930;
UPDATE `creature_model_info` SET `BoundingRadius`=1.031494, `VerifiedBuild`=23222 WHERE `DisplayID`=68393;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6, `CombatReach`=1.6, `VerifiedBuild`=23222 WHERE `DisplayID`=47903;
UPDATE `creature_model_info` SET `BoundingRadius`=7, `CombatReach`=21, `VerifiedBuild`=23222 WHERE `DisplayID`=47829;
UPDATE `creature_model_info` SET `BoundingRadius`=1, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=47843;
UPDATE `creature_model_info` SET `BoundingRadius`=1.784168, `VerifiedBuild`=23222 WHERE `DisplayID`=21456;
UPDATE `creature_model_info` SET `BoundingRadius`=0.675, `CombatReach`=1.35, `VerifiedBuild`=23222 WHERE `DisplayID`=19718;
UPDATE `creature_model_info` SET `BoundingRadius`=1.581468, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=2687;
UPDATE `creature_model_info` SET `BoundingRadius`=2.517913, `CombatReach`=2.6, `VerifiedBuild`=23222 WHERE `DisplayID`=18251;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=8, `VerifiedBuild`=23222 WHERE `DisplayID`=21484;
UPDATE `creature_model_info` SET `BoundingRadius`=2, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=46637;
UPDATE `creature_model_info` SET `BoundingRadius`=1.4, `CombatReach`=1.75, `VerifiedBuild`=23222 WHERE `DisplayID`=18063;
UPDATE `creature_model_info` SET `BoundingRadius`=8.503078, `CombatReach`=15, `VerifiedBuild`=23222 WHERE `DisplayID`=19501;
UPDATE `creature_model_info` SET `BoundingRadius`=0.45, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=19416;
UPDATE `creature_model_info` SET `BoundingRadius`=2.003105, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=19435;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2205, `CombatReach`=0.945, `VerifiedBuild`=23222 WHERE `DisplayID`=9535;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2205, `CombatReach`=0.945, `VerifiedBuild`=23222 WHERE `DisplayID`=20843;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3, `CombatReach`=1, `VerifiedBuild`=23222 WHERE `DisplayID`=19404;
UPDATE `creature_model_info` SET `BoundingRadius`=0.39, `CombatReach`=1.3, `VerifiedBuild`=23222 WHERE `DisplayID`=18311;
UPDATE `creature_model_info` SET `BoundingRadius`=1.3, `CombatReach`=1.625, `VerifiedBuild`=23222 WHERE `DisplayID`=11417;
UPDATE `creature_model_info` SET `CombatReach`=3, `VerifiedBuild`=23222 WHERE `DisplayID`=20044;
UPDATE `creature_model_info` SET `BoundingRadius`=2.108624, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=2879;
UPDATE `creature_model_info` SET `BoundingRadius`=1.3, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=20441;
UPDATE `creature_model_info` SET `BoundingRadius`=0.6093387, `CombatReach`=0.75, `VerifiedBuild`=23222 WHERE `DisplayID`=850;
UPDATE `creature_model_info` SET `BoundingRadius`=0.21, `CombatReach`=0.9, `VerifiedBuild`=23222 WHERE `DisplayID`=15227;
UPDATE `creature_model_info` SET `BoundingRadius`=0.75, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=11410;
UPDATE `creature_model_info` SET `BoundingRadius`=0.925, `CombatReach`=1.85, `VerifiedBuild`=23222 WHERE `DisplayID`=16572;
UPDATE `creature_model_info` SET `BoundingRadius`=13.60493, `CombatReach`=24, `VerifiedBuild`=23222 WHERE `DisplayID`=18671;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `VerifiedBuild`=23222 WHERE `DisplayID`=21155;
UPDATE `creature_model_info` SET `BoundingRadius`=0.372, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=53256;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=53270;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=53272;
UPDATE `creature_model_info` SET `BoundingRadius`=0.236, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=53271;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=23222 WHERE `DisplayID`=53269;


UPDATE `quest_template` SET `RewardSpell`=237602, `Flags`=2184773888, `RewardFactionValue1`=3, `RewardFactionValue2`=3, `RewardFactionOverride1`=0, `RewardFactionOverride2`=0, `VerifiedBuild`=23222 WHERE `ID`=45069; -- -Unknown-


UPDATE `creature_template` SET `npcflag`=134217729, `VerifiedBuild`=23222 WHERE `entry`=109686; -- Unjari Feltongue
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=190, `npcflag`=3, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=101097; -- Calydus
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `speed_walk`=0.666668, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=110408; -- Murr
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23222 WHERE `entry`=97163; -- Cursed Falke
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=114463; -- Medivh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115497; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=1.2, `speed_run`=0.4285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115789; -- Demonic Rift
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=1.2, `speed_run`=0.4285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115743; -- Legion Console
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1720, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115730; -- Felguard Sentry
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2780, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `unit_flags2`=2097152, `VerifiedBuild`=23222 WHERE `entry`=115734; -- Shadow Spitter
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=116124; -- Medivh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116259; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115407; -- Rook
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115395; -- Queen
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115388; -- King
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115406; -- Knight
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115402; -- Bishop
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115401; -- Bishop
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=1.2, `speed_run`=0.4285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=1073743872, `VerifiedBuild`=23222 WHERE `entry`=114913; -- Command Ship
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `HoverHeight`=5, `VerifiedBuild`=23222 WHERE `entry`=114790; -- Viz'aduum the Watcher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=115489; -- Anduin Lothar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=537133824, `unit_flags2`=2099201, `VerifiedBuild`=23222 WHERE `entry`=115487; -- Medivh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115501; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_run`=1.428571, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `VehicleId`=5059, `VerifiedBuild`=23222 WHERE `entry`=115831; -- Mana Devourer
UPDATE `creature_template` SET `faction`=35, `npcflag`=16777216, `speed_walk`=1.6, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `HoverHeight`=6, `VerifiedBuild`=23222 WHERE `entry`=116551; -- Flying Tome
UPDATE `creature_template` SET `faction`=190, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116491; -- Spider
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=67143680, `VerifiedBuild`=23222 WHERE `entry`=115694; -- Soul Harvester
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=537166656, `unit_flags2`=2049, `VerifiedBuild`=23222 WHERE `entry`=115486; -- Erudite Slayer
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33588032, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115386; -- Chest Board
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=16, `speed_walk`=1.2, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116418; -- Ethereal Thief
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `speed_walk`=0.6, `BaseAttackTime`=2500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115004; -- Abhorrent Drudge
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115418; -- Spider
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `HoverHeight`=9, `VerifiedBuild`=23222 WHERE `entry`=115419; -- Ancient Tome
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115417; -- Rat
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=4194304, `VerifiedBuild`=23222 WHERE `entry`=116494; -- Mana Devourer
UPDATE `creature_template` SET `faction`=35, `speed_walk`=8, `speed_run`=2.857143, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=1073743872, `HoverHeight`=60, `VerifiedBuild`=23222 WHERE `entry`=116865; -- Flying Tome
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=1073741824, `VerifiedBuild`=23222 WHERE `entry`=114350; -- Shade of Medivh
UPDATE `creature_template` SET `faction`=35, `speed_walk`=1.6, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2099200, `HoverHeight`=6, `VerifiedBuild`=23222 WHERE `entry`=116864; -- Flying Tome
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114834; -- Arcane Watchman
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115484; -- Fel Bat
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=16, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33600, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114843; -- Shadow Pillager
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114842; -- Homunculus
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=1.6, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115757; -- Wrathguard Flamebringer
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555264, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115746; -- Burning Tiles
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=71, `faction`=16, `BaseAttackTime`=1200, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114839; -- Chaotic Sentience
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114835; -- Mana Feeder
UPDATE `creature_template` SET `minlevel`=71, `maxlevel`=71, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114844; -- Spell Shade
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115488; -- Infused Pyromancer
UPDATE `creature_template` SET `IconName`='pickup', `faction`=31, `npcflag`=16777216, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116495; -- Rat
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33555200, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=114462; -- The Curator
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=115491; -- Medivh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115113; -- Soul Fragment
UPDATE `creature_template` SET `minlevel`=25, `maxlevel`=25, `faction`=35, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=99086; -- Quillino
UPDATE `creature_template` SET `minlevel`=25, `maxlevel`=25, `faction`=35, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=99084; -- Fethyr
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23222 WHERE `entry`=97163; -- Cursed Falke
UPDATE `creature_template` SET `HoverHeight`=1.03, `VerifiedBuild`=23222 WHERE `entry`=97755; -- Galecrested Eagle
UPDATE `creature_template` SET `unit_flags`=33536, `VerifiedBuild`=23222 WHERE `entry`=100192; -- Astoril
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23222 WHERE `entry`=103005; -- Bowl of Fruit
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=2049, `VerifiedBuild`=23222 WHERE `entry`=113818; -- Glitterpool Chick
UPDATE `creature_template` SET `unit_flags`=537166080, `unit_flags2`=2049, `VerifiedBuild`=23222 WHERE `entry`=102960; -- Glitterpool Heron
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `speed_walk`=1.266668, `speed_run`=2.182543, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114946; -- Muninn
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=23222 WHERE `entry`=94691; -- Overgrown Larva
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=110441; -- Snowglobe Stalker
UPDATE `creature_template` SET `minlevel`=1, `VerifiedBuild`=23222 WHERE `entry`=56042; -- Mule
UPDATE `creature_template` SET `faction`=35, `VerifiedBuild`=23222 WHERE `entry`=108628; -- Armond Thaco
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=23222 WHERE `entry`=92183; -- Alard Schmied
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=23222 WHERE `entry`=106655; -- Arcanomancer Vridiel
UPDATE `creature_template` SET `faction`=775, `unit_flags`=33024, `VerifiedBuild`=23222 WHERE `entry`=15760; -- Winter Reveler
UPDATE `creature_template` SET `minlevel`=58, `maxlevel`=58, `VerifiedBuild`=23222 WHERE `entry`=16284; -- Argent Medic
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=92165; -- Dungeoneer's Training Dummy
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3, `VerifiedBuild`=23222 WHERE `entry`=92166; -- Raider's Training Dummy
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=92168; -- Dungeoneer's Training Dummy
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=110434; -- Kristoff
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=109102; -- Delas Moonfang
UPDATE `creature_template` SET `speed_run`=1.142857, `VerifiedBuild`=23222 WHERE `entry`=92149; -- Silver Hand Protector
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=116608; -- Silver Hand Armor
UPDATE `creature_template` SET `npcflag`=16777216, `speed_walk`=0.944444, `speed_run`=1.111111, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=256, `unit_flags2`=4194304, `VehicleId`=2745, `VerifiedBuild`=23222 WHERE `entry`=69964; -- Kanrethad Ebonlocke
UPDATE `creature_template` SET `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33554688, `unit_flags2`=4229152, `VerifiedBuild`=23222 WHERE `entry`=70028; -- Demonic Gateway
UPDATE `creature_template` SET `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=512, `unit_flags2`=34848, `VerifiedBuild`=23222 WHERE `entry`=70052; -- Demonic Soulwell
UPDATE `creature_template` SET `speed_walk`=2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68206; -- Unbound Shivarra
UPDATE `creature_template` SET `speed_walk`=2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68204; -- Unbound Rogue
UPDATE `creature_template` SET `speed_walk`=2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68205; -- Unbound Succubus
UPDATE `creature_template` SET `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68173; -- Freed Imp
UPDATE `creature_template` SET `speed_walk`=2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=68175; -- Unbound Bonemender
UPDATE `creature_template` SET `speed_walk`=2, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=68176; -- Unbound Centurion
UPDATE `creature_template` SET `speed_walk`=2, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=68174; -- Unbound Nightlord
UPDATE `creature_template` SET `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68151; -- Essence of Order
UPDATE `creature_template` SET `speed_walk`=1.111112, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68139; -- Suffering Soul Fragment
UPDATE `creature_template` SET `speed_walk`=1.2, `speed_run`=0.9920629, `BaseAttackTime`=1300, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68137; -- Akama
UPDATE `creature_template` SET `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=68129; -- Ashtongue Shaman
UPDATE `creature_template` SET `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=68096; -- Ashtongue Primalist
UPDATE `creature_template` SET `unit_flags`=768, `VerifiedBuild`=23222 WHERE `entry`=48959; -- Rusty Anvil
UPDATE `creature_template` SET `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=68098; -- Ashtongue Worker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=113941; -- Invisible Stalker
UPDATE `creature_template` SET `speed_run`=1, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=22859; -- Shadowhoof Summoner
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=22857; -- Illidari Ravager
UPDATE `creature_template` SET `minlevel`=69, `unit_flags`=570425600, `VerifiedBuild`=23222 WHERE `entry`=19760; -- Cooling Infernal
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=23222 WHERE `entry`=21080; -- Dormant Infernal
UPDATE `creature_template` SET `maxlevel`=68, `VerifiedBuild`=23222 WHERE `entry`=19759; -- Newly Crafted Infernal
UPDATE `creature_template` SET `minlevel`=64, `VerifiedBuild`=23222 WHERE `entry`=18670; -- Ironjaw
UPDATE `creature_template` SET `maxlevel`=64, `VerifiedBuild`=23222 WHERE `entry`=17142; -- Wrekt Warrior
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=23222 WHERE `entry`=19188; -- Raging Colossus
UPDATE `creature_template` SET `maxlevel`=61, `VerifiedBuild`=23222 WHERE `entry`=20283; -- Marshrock Stomper
UPDATE `creature_template` SET `unit_flags`=0, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=19706; -- Marshrock Threshalisk
UPDATE `creature_template` SET `maxlevel`=65, `VerifiedBuild`=23222 WHERE `entry`=21840; -- Cosmetic Silkwing
UPDATE `creature_template` SET `dmgschool`=0, `VerifiedBuild`=23222 WHERE `entry`=18881; -- Sundered Rumbler
UPDATE `creature_template` SET `dmgschool`=0, `VerifiedBuild`=23222 WHERE `entry`=18864; -- Mana Wraith
UPDATE `creature_template` SET `maxlevel`=67, `VerifiedBuild`=23222 WHERE `entry`=18872; -- Disembodied Vindicator
UPDATE `creature_template` SET `minlevel`=69, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=18859; -- Wrath Priestess
UPDATE `creature_template` SET `maxlevel`=68, `spell1`=0, `spell2`=0, `spell3`=0, `spell4`=0, `VerifiedBuild`=23222 WHERE `entry`=16943; -- Cyber-Rage Forgelord
UPDATE `creature_template` SET `maxlevel`=68, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=20453; -- Ethereum Shocktrooper
UPDATE `creature_template` SET `minlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=22821; -- Ethereum Avenger
UPDATE `creature_template` SET `dmgschool`=0, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=20774; -- Farahlon Lasher
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=22293; -- Inactive Fel Reaver
UPDATE `creature_template` SET `unit_flags`=32768, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=18879; -- Phase Hunter
UPDATE `creature_template` SET `minlevel`=67, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=21123; -- Felsworn Scalewing
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=188, `npcflag`=1073741824, `unit_flags`=33280, `VerifiedBuild`=23222 WHERE `entry`=62184; -- Rock Viper
UPDATE `creature_template` SET `minlevel`=61, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=18132; -- Umbraglow Stinger
UPDATE `creature_template` SET `minlevel`=62, `speed_run`=0.9571428, `VerifiedBuild`=23222 WHERE `entry`=18124; -- Withered Giant
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=17058; -- Illidari Taskmaster
UPDATE `creature_template` SET `minlevel`=62, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=19189; -- Quillfang Skitterer
UPDATE `creature_template` SET `maxlevel`=62, `unit_flags`=0, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=16934; -- Quillfang Ravager
UPDATE `creature_template` SET `minlevel`=64, `VerifiedBuild`=23222 WHERE `entry`=16993; -- Wounded Blood Elf Pilgrim
UPDATE `creature_template` SET `minlevel`=64, `VerifiedBuild`=23222 WHERE `entry`=18565; -- Theloria Shadecloak
UPDATE `creature_template` SET `minlevel`=68, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=19740; -- Wrathwalker
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=23222 WHERE `entry`=19341; -- Grutah
UPDATE `creature_template` SET `npcflag`=17, `VerifiedBuild`=23222 WHERE `entry`=25099; -- Jonathan Garrett
UPDATE `creature_template` SET `maxlevel`=68, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=19757; -- Infernal Soul
UPDATE `creature_template` SET `minlevel`=69, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=22016; -- Eclipsion Soldier
UPDATE `creature_template` SET `npcflag`=640, `VerifiedBuild`=23222 WHERE `entry`=19625; -- Alorya
UPDATE `creature_template` SET `npcflag`=640, `VerifiedBuild`=23222 WHERE `entry`=19518; -- Feranin
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23222 WHERE `entry`=22113; -- Mordenai
UPDATE `creature_template` SET `npcflag`=66177, `VerifiedBuild`=23222 WHERE `entry`=21744; -- Roldemar
UPDATE `creature_template` SET `maxlevel`=68, `VerifiedBuild`=23222 WHERE `entry`=21478; -- Rocknail Ripper
UPDATE `creature_template` SET `speed_run`=1, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=21827; -- Zandras
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=21520; -- Illidari Jailor
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=21808; -- Illidari Overseer
UPDATE `creature_template` SET `speed_walk`=1, `VerifiedBuild`=23222 WHERE `entry`=21940; -- Invis Illidari Bane Caster
UPDATE `creature_template` SET `speed_walk`=1, `VerifiedBuild`=23222 WHERE `entry`=21939; -- Invis Illidari Blade Target
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=23222 WHERE `entry`=21878; -- Felboar
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=0, `VerifiedBuild`=23222 WHERE `entry`=21419; -- Infernal Attacker
UPDATE `creature_template` SET `speed_walk`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32768, `VerifiedBuild`=23222 WHERE `entry`=21789; -- Nakansi
UPDATE `creature_template` SET `unit_flags`=33280, `VerifiedBuild`=23222 WHERE `entry`=19362; -- Kor'kron Defender
UPDATE `creature_template` SET `minlevel`=64, `spell1`=0, `spell2`=0, `spell3`=0, `spell4`=0, `VerifiedBuild`=23222 WHERE `entry`=16769; -- Firewing Warlock
UPDATE `creature_template` SET `unit_flags`=33280, `VerifiedBuild`=23222 WHERE `entry`=18760; -- Isla Starmane
UPDATE `creature_template` SET `scale`=1, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=16974; -- Rogue Voidwalker
UPDATE `creature_template` SET `scale`=1, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=16975; -- Uncontrolled Voidwalker
UPDATE `creature_template` SET `maxlevel`=59, `VerifiedBuild`=23222 WHERE `entry`=18827; -- Gan'arg Sapper
UPDATE `creature_template` SET `maxlevel`=59, `VerifiedBuild`=23222 WHERE `entry`=16896; -- Honor Hold Archer
UPDATE `creature_template` SET `npcflag`=145, `VerifiedBuild`=23222 WHERE `entry`=18754; -- Barim Spilthoof
UPDATE `creature_template` SET `minlevel`=58, `VerifiedBuild`=23222 WHERE `entry`=16590; -- Injured Thrallmar Grunt
UPDATE `creature_template` SET `npcflag`=81, `VerifiedBuild`=23222 WHERE `entry`=18748; -- Ruak Stronghorn
UPDATE `creature_template` SET `npcflag`=3283, `VerifiedBuild`=23222 WHERE `entry`=16588; -- Apothecary Antonivich
UPDATE `creature_template` SET `maxlevel`=54, `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=17004; -- Jir'see
UPDATE `creature_template` SET `minlevel`=58, `VerifiedBuild`=23222 WHERE `entry`=17002; -- Angela "The Claw" Kestrel
UPDATE `creature_template` SET `npcflag`=4227, `VerifiedBuild`=23222 WHERE `entry`=16583; -- Rohok
UPDATE `creature_template` SET `minlevel`=60, `VerifiedBuild`=23222 WHERE `entry`=16579; -- Falcon Watch Sentinel
UPDATE `creature_template` SET `minlevel`=58, `VerifiedBuild`=23222 WHERE `entry`=16578; -- Blood Elf Pilgrim
UPDATE `creature_template` SET `npcflag`=66177, `VerifiedBuild`=23222 WHERE `entry`=16602; -- Floyd Pinkus
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=23222 WHERE `entry`=16580; -- Thrallmar Grunt
UPDATE `creature_template` SET `minlevel`=62, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=16946; -- Mo'arg Forgefiend
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=16954; -- Forge Camp Legionnaire
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=23222 WHERE `entry`=16582; -- Thrallmar Marksman
UPDATE `creature_template` SET `minlevel`=62, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=19415; -- Shattered Hand Acolyte
UPDATE `creature_template` SET `minlevel`=49, `VerifiedBuild`=23222 WHERE `entry`=16887; -- Eye of Honor Hold
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=188, `npcflag`=1073741824, `unit_flags`=512, `VerifiedBuild`=23222 WHERE `entry`=61326; -- Scorpid
UPDATE `creature_template` SET `minlevel`=63, `VerifiedBuild`=23222 WHERE `entry`=18539; -- Ashkaz
UPDATE `creature_template` SET `minlevel`=63, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=18476; -- Timber Worg
UPDATE `creature_template` SET `minlevel`=64, `maxlevel`=64, `VerifiedBuild`=23222 WHERE `entry`=24437; -- Consortium Assistant
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=23222 WHERE `entry`=20125; -- Zula Slagfury
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=23222 WHERE `entry`=20124; -- Kradu Grimblade
UPDATE `creature_template` SET `maxlevel`=69, `VerifiedBuild`=23222 WHERE `entry`=22987; -- Skyguard Nether Ray
UPDATE `creature_template` SET `unit_flags`=33536, `VerifiedBuild`=23222 WHERE `entry`=25115; -- Shattered Sun Warrior
UPDATE `creature_template` SET `speed_run`=0.9920629, `VerifiedBuild`=23222 WHERE `entry`=18166; -- Archmage Khadgar
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23222 WHERE `entry`=17076; -- Lady Liadrin
UPDATE `creature_template` SET `faction`=35, `VerifiedBuild`=23222 WHERE `entry`=23131; -- Blood Knight Honor Guard
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=40, `npcflag`=16777216, `speed_walk`=0.444444, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=71321600, `VerifiedBuild`=23222 WHERE `entry`=82273; -- Iron Demolisher
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1735, `npcflag`=3, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=82851; -- Thrall
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1935, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=76500; -- Orgrimmar Sea Dog
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1735, `npcflag`=4224, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=76521; -- Alkra Hammerslam
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1735, `npcflag`=80, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=76519; -- Rott Bonefinger
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1735, `npcflag`=2688, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=76522; -- Zerpy Clampshock
UPDATE `creature_template` SET `minlevel`=93, `maxlevel`=93, `faction`=1735, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=85247; -- Rokhan
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1735, `npcflag`=640, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=76520; -- Ulzann
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1734, `npcflag`=8193, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=85734; -- Ameri Windblade
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=1801, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=76524; -- Grizzled Grenadier
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=76886; -- Ironmarch Scout
UPDATE `creature_template` SET `npcflag`=16777216, `unit_flags2`=1140852736, `VerifiedBuild`=23222 WHERE `entry`=53194; -- Capture Point
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3, `VerifiedBuild`=23222 WHERE `entry`=101956; -- Rebellious Fel Lord
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=102045; -- Rebellious Wrathguard
UPDATE `creature_template` SET `npcflag`=134217729, `VerifiedBuild`=23222 WHERE `entry`=109686; -- Unjari Feltongue
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=190, `npcflag`=3, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=101097; -- Calydus
UPDATE `creature_template` SET `minlevel`=1, `VerifiedBuild`=23222 WHERE `entry`=56042; -- Mule
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `speed_walk`=1.266668, `speed_run`=2.182543, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114946; -- Muninn
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=23222 WHERE `entry`=92183; -- Alard Schmied
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=23222 WHERE `entry`=106655; -- Arcanomancer Vridiel
UPDATE `creature_template` SET `faction`=35, `VerifiedBuild`=23222 WHERE `entry`=108628; -- Armond Thaco
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=114463; -- Medivh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115497; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=1.2, `speed_run`=0.4285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115789; -- Demonic Rift
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=1.2, `speed_run`=0.4285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115743; -- Legion Console
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1720, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115730; -- Felguard Sentry
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2780, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `unit_flags2`=2097152, `VerifiedBuild`=23222 WHERE `entry`=115734; -- Shadow Spitter
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=116124; -- Medivh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116259; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115407; -- Rook
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115395; -- Queen
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115406; -- Knight
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115402; -- Bishop
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115401; -- Bishop
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115388; -- King
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `speed_walk`=1.2, `speed_run`=0.4285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=1073743872, `VerifiedBuild`=23222 WHERE `entry`=114913; -- Command Ship
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=4196352, `HoverHeight`=5, `VerifiedBuild`=23222 WHERE `entry`=114790; -- Viz'aduum the Watcher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115501; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_run`=1.428571, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `VehicleId`=5059, `VerifiedBuild`=23222 WHERE `entry`=115831; -- Mana Devourer
UPDATE `creature_template` SET `faction`=35, `npcflag`=16777216, `speed_walk`=1.6, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `HoverHeight`=6, `VerifiedBuild`=23222 WHERE `entry`=116551; -- Flying Tome
UPDATE `creature_template` SET `faction`=190, `speed_run`=1.285714, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116491; -- Spider
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=67143680, `VerifiedBuild`=23222 WHERE `entry`=115694; -- Soul Harvester
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=537166656, `unit_flags2`=2049, `VerifiedBuild`=23222 WHERE `entry`=115486; -- Erudite Slayer
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33588032, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115386; -- Chest Board
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=16, `speed_walk`=1.2, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116418; -- Ethereal Thief
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115418; -- Spider
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `HoverHeight`=9, `VerifiedBuild`=23222 WHERE `entry`=115419; -- Ancient Tome
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115417; -- Rat
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=14, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=4194304, `VerifiedBuild`=23222 WHERE `entry`=116494; -- Mana Devourer
UPDATE `creature_template` SET `faction`=35, `speed_walk`=8, `speed_run`=2.857143, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=1073743872, `HoverHeight`=60, `VerifiedBuild`=23222 WHERE `entry`=116865; -- Flying Tome
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=1073741824, `VerifiedBuild`=23222 WHERE `entry`=114350; -- Shade of Medivh
UPDATE `creature_template` SET `faction`=35, `speed_walk`=1.6, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=2099200, `HoverHeight`=6, `VerifiedBuild`=23222 WHERE `entry`=116864; -- Flying Tome
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114834; -- Arcane Watchman
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115484; -- Fel Bat
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=16, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33600, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114843; -- Shadow Pillager
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114842; -- Homunculus
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=1.6, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115757; -- Wrathguard Flamebringer
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555264, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115746; -- Burning Tiles
UPDATE `creature_template` SET `minlevel`=71, `maxlevel`=71, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114844; -- Spell Shade
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=71, `faction`=16, `BaseAttackTime`=1200, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114839; -- Chaotic Sentience
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=320, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114835; -- Mana Feeder
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115488; -- Infused Pyromancer
UPDATE `creature_template` SET `IconName`='pickup', `faction`=31, `npcflag`=16777216, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116495; -- Rat
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=115489; -- Anduin Lothar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=115490; -- Prince Llane Wrynn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=537133824, `unit_flags2`=2099201, `VerifiedBuild`=23222 WHERE `entry`=115487; -- Medivh
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=103, `speed_walk`=5.6, `speed_run`=2, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=6293504, `VerifiedBuild`=23222 WHERE `entry`=114895; -- Nightbane
UPDATE `creature_template` SET `speed_run`=1, `unit_flags`=33554432, `VerifiedBuild`=23222 WHERE `entry`=17677; -- Arcanagos Spell Dummy
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=1683, `speed_walk`=3.2, `speed_run`=1.857143, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=256, `HoverHeight`=15, `VerifiedBuild`=23222 WHERE `entry`=115213; -- Image of Arcanagos
UPDATE `creature_template` SET `unit_flags`=32768, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=17672; -- Deadwind Villager
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33555200, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=114462; -- The Curator
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=115491; -- Medivh
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115113; -- Soul Fragment
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=768, `unit_flags2`=2097152, `VerifiedBuild`=23222 WHERE `entry`=115426; -- Medivh
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33570816, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116804; -- Scene Actor
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_walk`=0.7, `speed_run`=0.8, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `VerifiedBuild`=23222 WHERE `entry`=115427; -- Nielas Aran
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=114408; -- Will Breaker Stalker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114526; -- Ghostly Understudy
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114584; -- Phantom Crew
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=1828, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=114266; -- Shoreline Tidespeaker
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=114261; -- Toe Knee
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=114265; -- Gang Ruffian
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=1828, `speed_run`=1.428571, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=114260; -- Mrrgria
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=0.8, `speed_run`=0.2857143, `BaseAttackTime`=3000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587200, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=114471; -- Wash Away
UPDATE `creature_template` SET `minlevel`=73, `maxlevel`=73, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116002; -- The Crone
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=35, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115985; -- Tito
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115984; -- Tinhead
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=1200, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115983; -- Roar
UPDATE `creature_template` SET `minlevel`=72, `maxlevel`=72, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115982; -- Strawman
UPDATE `creature_template` SET `minlevel`=73, `maxlevel`=73, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115981; -- Dorothee
UPDATE `creature_template` SET `minlevel`=73, `maxlevel`=73, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115976; -- Julianne
UPDATE `creature_template` SET `minlevel`=73, `maxlevel`=73, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115967; -- Romulo
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1683, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33554688, `VerifiedBuild`=23222 WHERE `entry`=115038; -- Image of Medivh
UPDATE `creature_template` SET `minlevel`=73, `maxlevel`=73, `faction`=35, `npcflag`=1, `speed_walk`=2, `speed_run`=1.214286, `BaseAttackTime`=1200, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116003; -- The Big Bad Wolf
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `faction`=35, `npcflag`=1, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115986; -- Grandmother
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114339; -- Barnes
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33587456, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116549; -- Backup Singer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114616; -- Sebastian
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33587456, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116550; -- Spectral Patron
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115105; -- Soul Fragment
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115013; -- Soul Fragment
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115101; -- Soul Fragment
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114542; -- Ghostly Philanthropist
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114541; -- Spectral Patron
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=2.8, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2097152, `VerifiedBuild`=23222 WHERE `entry`=114247; -- The Curator
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_walk`=0.888888, `speed_run`=0.9523814, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `HoverHeight`=2.5, `VerifiedBuild`=23222 WHERE `entry`=115765; -- Abstract Nullifier
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1375, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=570426176, `unit_flags2`=67110913, `VerifiedBuild`=23222 WHERE `entry`=115059; -- Shadowbeast
UPDATE `creature_template` SET `minlevel`=71, `maxlevel`=71, `faction`=1375, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=570426176, `unit_flags2`=67110913, `VerifiedBuild`=23222 WHERE `entry`=115061; -- Dreadbeast
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=512, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=115103; -- Soul Fragment
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114338; -- Mana Confluence
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114544; -- Skeletal Usher
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `VerifiedBuild`=23222 WHERE `entry`=114364; -- Mana-Gorged Wyrm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114796; -- Wholesome Hostess
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114783; -- Reformed Maiden
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114792; -- Virtuous Lady
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_walk`=1.6, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=113971; -- Maiden of Virtue
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114633; -- Spectral Valet
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114320; -- Lord Robin Daris
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114318; -- Baron Rafe Dreuger
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114321; -- Lord Crispin Ference
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=16, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=64, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=114312; -- Moroes
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114316; -- Baroness Dorothea Millstipe
UPDATE `creature_template` SET `minlevel`=71, `maxlevel`=71, `faction`=1375, `speed_walk`=2.4, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=570426176, `unit_flags2`=67110913, `VerifiedBuild`=23222 WHERE `entry`=115063; -- Greater Shadowbat
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=1375, `speed_walk`=2.4, `speed_run`=1.714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=570426176, `unit_flags2`=67110913, `VerifiedBuild`=23222 WHERE `entry`=115062; -- Shadowbat
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5021, `VerifiedBuild`=23222 WHERE `entry`=115034; -- Web
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=35, `npcflag`=4225, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114815; -- Koren
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=16, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554496, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=114262; -- Attumen the Huntsman
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5029, `VerifiedBuild`=23222 WHERE `entry`=115124; -- Web
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5028, `VerifiedBuild`=23222 WHERE `entry`=115123; -- Web
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114334; -- Damaged Golem
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114802; -- Spectral Journeyman
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5020, `VerifiedBuild`=23222 WHERE `entry`=115033; -- Web
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5025, `VerifiedBuild`=23222 WHERE `entry`=115057; -- Web
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114799; -- Calliard
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114801; -- Spectral Apprentice
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114552; -- Egona Spangly
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114554; -- Peta Venkner
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5019, `VerifiedBuild`=23222 WHERE `entry`=115032; -- Web
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114803; -- Spectral Stable Hand
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114804; -- Spectral Charger
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114636; -- Phantom Guardsman
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=1.111112, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `HoverHeight`=5, `VerifiedBuild`=23222 WHERE `entry`=115020; -- Arcanid
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114637; -- Spectral Sentry
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114649; -- Potter Golem
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114648; -- Aron Gillwort
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116573; -- R.L Cooper
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114647; -- Willy Zulemore
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114629; -- Spectral Retainer
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114632; -- Spectral Attendant
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114646; -- Babby Yikes
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114556; -- Mae Stance
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115019; -- Coldmist Widow
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114555; -- Gillian Voltzman
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114624; -- Arcane Warden
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5018, `VerifiedBuild`=23222 WHERE `entry`=115030; -- Web
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=0.9920629, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115496; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=114940; -- Phantom Guest
UPDATE `creature_template` SET `minlevel`=113, `maxlevel`=113, `faction`=14, `speed_walk`=0.6, `speed_run`=0.2142857, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5012, `VerifiedBuild`=23222 WHERE `entry`=114938; -- Dancing Vehicle Stalker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114634; -- Undying Servant
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=1667, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114625; -- Phantom Guest
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=0.8, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=294976, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114794; -- Skeletal Hound
UPDATE `creature_template` SET `minlevel`=92, `maxlevel`=92, `faction`=14, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554688, `unit_flags2`=67110912, `VehicleId`=5027, `VerifiedBuild`=23222 WHERE `entry`=115118; -- Web
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33088, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115115; -- Coldmist Stalker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114627; -- Shrieking Terror
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114626; -- Forlorn Spirit
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114715; -- Ghostly Chef
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114716; -- Ghostly Baker
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=294976, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114714; -- Ghostly Steward
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=114628; -- Skeletal Waiter
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `speed_walk`=0.6, `BaseAttackTime`=2500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115004; -- Abhorrent Drudge
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=90, `npcflag`=16777216, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=69224448, `HoverHeight`=1.7, `VerifiedBuild`=23222 WHERE `entry`=115037; -- Fel Spreader
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=954, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=71321600, `VerifiedBuild`=23222 WHERE `entry`=115169; -- Portal Stabilizer
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115009; -- Felfire Imp
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115006; -- Deranged Collector
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=188, `npcflag`=1073741824, `unit_flags`=512, `VerifiedBuild`=23222 WHERE `entry`=68819; -- Arcane Eye
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=537166592, `unit_flags2`=2049, `VerifiedBuild`=23222 WHERE `entry`=116680; -- Felfire Imp
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=115164; -- Raging Berserker
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=100665344, `VerifiedBuild`=23222 WHERE `entry`=115228; -- Eastern Ward
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2009, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=33556480, `VerifiedBuild`=23222 WHERE `entry`=114821; -- Kirin Tor Mage
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554432, `unit_flags2`=100665344, `VerifiedBuild`=23222 WHERE `entry`=115227; -- Northern Ward
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=634, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=18432, `VerifiedBuild`=23222 WHERE `entry`=114822; -- Captured Wyrmtongue
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116784; -- Feltongue Corruptor
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=110, `faction`=90, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=115024; -- Dreadwing
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=23222 WHERE `entry`=95888; -- Cordana Felsong
UPDATE `creature_template` SET `unit_flags`=33344, `unit_flags2`=67635200, `VerifiedBuild`=23222 WHERE `entry`=97678; -- Aranasi Broodmother
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=23222 WHERE `entry`=96015; -- Inquisitor Tormentorum
UPDATE `creature_template` SET `minlevel`=112, `maxlevel`=112, `faction`=35, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33554752, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=109773; -- Focus
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_walk`=0.577776, `speed_run`=1.714286, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=96579; -- Frenzied Animus
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=23222 WHERE `entry`=95886; -- Ash'Golm
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=23222 WHERE `entry`=95885; -- Tirathon Saltheril
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=23222 WHERE `entry`=95887; -- Glazer
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=190, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=256, `unit_flags2`=67143680, `VerifiedBuild`=23222 WHERE `entry`=106110; -- Waterlogged Scroll
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `resistance2`=0, `resistance3`=0, `resistance4`=0, `resistance5`=0, `resistance6`=0, `VerifiedBuild`=23222 WHERE `entry`=20910; -- Twilight Drakonaar
UPDATE `creature_template` SET `difficulty_entry_1`=0, `speed_run`=1.428571, `unit_flags2`=2099200, `resistance6`=0, `VerifiedBuild`=23222 WHERE `entry`=20908; -- Akkiris Lightning-Waker
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=20977; -- Millhouse Manastorm
UPDATE `creature_template` SET `difficulty_entry_1`=0, `speed_walk`=1, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=64, `unit_flags2`=2099200, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=20905; -- Blazing Trickster
UPDATE `creature_template` SET `difficulty_entry_1`=0, `npcflag`=0, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=20912; -- Harbinger Skyriss
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=20904; -- Warden Mellichar
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=20900; -- Unchained Doombringer
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=20898; -- Gargantuan Abyssal
UPDATE `creature_template` SET `difficulty_entry_1`=0, `npcflag`=0, `dmgschool`=0, `unit_flags2`=2099200, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=20870; -- Zereketh the Unbound
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=1, `VerifiedBuild`=23222 WHERE `entry`=20869; -- Arcatraz Sentinel
UPDATE `creature_template` SET `difficulty_entry_1`=0, `npcflag`=0, `unit_flags2`=2099200, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=20886; -- Wrath-Scryer Soccothrates
UPDATE `creature_template` SET `difficulty_entry_1`=0, `npcflag`=0, `unit_flags2`=2099200, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=20885; -- Dalliah the Doomsayer
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags`=0, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=20865; -- Protean Horror
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=69, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=20859; -- Arcatraz Warder
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=131, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=98685; -- Cupri
UPDATE `creature_template` SET `speed_run`=0.9920629, `VerifiedBuild`=23222 WHERE `entry`=18166; -- Archmage Khadgar
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23222 WHERE `entry`=17076; -- Lady Liadrin
UPDATE `creature_template` SET `faction`=35, `VerifiedBuild`=23222 WHERE `entry`=23131; -- Blood Knight Honor Guard
UPDATE `creature_template` SET `npcflag`=1, `unit_flags`=32768, `VerifiedBuild`=23222 WHERE `entry`=20102; -- Goblin Commoner
UPDATE `creature_template` SET `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=17611; -- Warchief's Portal
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=17301; -- Shattered Hand Executioner
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=17465; -- Shattered Hand Centurion
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags`=32848, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=17464; -- Shattered Hand Gladiator
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=20923; -- Blood Guard Porung
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `unit_flags2`=2099200, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=17427; -- Shattered Hand Archer
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=16704; -- Shattered Hand Sharpshooter
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=17693; -- Shattered Hand Scout
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `unit_flags2`=2099200, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=17462; -- Shattered Hand Zealot
UPDATE `creature_template` SET `difficulty_entry_1`=0, `maxlevel`=70, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=17695; -- Shattered Hand Assassin
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=16809; -- Warbringer O'mrogg
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=17671; -- Shattered Hand Champion
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `faction`=16, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=17669; -- Rabid Warhound
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=16523; -- Shattered Hand Savage
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=16699; -- Shattered Hand Reaver
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=17083; -- Fel Orc Convert
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=23222 WHERE `entry`=17296; -- Captain Boneshatter
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=72, `maxlevel`=72, `unit_flags2`=2099200, `resistance5`=0, `VerifiedBuild`=23222 WHERE `entry`=16807; -- Grand Warlock Nethekurse
UPDATE `creature_template` SET `difficulty_entry_1`=0, `unit_flags2`=2099200, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=16808; -- Warchief Kargath Bladefist
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `VerifiedBuild`=23222 WHERE `entry`=17356; -- Creeping Ooze
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `VerifiedBuild`=23222 WHERE `entry`=17357; -- Creeping Oozeling
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=16507; -- Shattered Hand Sentry
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=17420; -- Shattered Hand Heathen
UPDATE `creature_template` SET `minlevel`=64, `maxlevel`=64, `VerifiedBuild`=23222 WHERE `entry`=57908; -- Thrallmar Scryer
UPDATE `creature_template` SET `minlevel`=65, `VerifiedBuild`=23222 WHERE `entry`=19677; -- Consortium Spell Marker
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=71, `maxlevel`=72, `VerifiedBuild`=23222 WHERE `entry`=18314; -- Nexus Stalker
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=72, `maxlevel`=72, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=18343; -- Tavarok
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=71, `maxlevel`=71, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=18331; -- Ethereal Darkcaster
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=72, `maxlevel`=72, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=18315; -- Ethereal Theurgist
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=71, `maxlevel`=72, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=18312; -- Ethereal Spellbinder
UPDATE `creature_template` SET `minlevel`=90, `maxlevel`=90, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33555200, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=83996; -- Shaffar's Stasis Chamber
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=19306; -- Mana Leech
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `dmgschool`=0, `VerifiedBuild`=23222 WHERE `entry`=19307; -- Nexus Terror
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=71, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=18317; -- Ethereal Priest
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=72, `maxlevel`=72, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=18344; -- Nexus-Prince Shaffar
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=18431; -- Ethereal Beacon
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=72, `maxlevel`=72, `dmgschool`=0, `unit_flags2`=2099200, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=18341; -- Pandemonius
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=71, `spell1`=0, `VerifiedBuild`=23222 WHERE `entry`=18313; -- Ethereal Sorcerer
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=18311; -- Ethereal Crypt Raider
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `spell3`=0, `VerifiedBuild`=23222 WHERE `entry`=18309; -- Ethereal Scavenger
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=71, `maxlevel`=71, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=24697; -- Sister of Torment
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=72, `maxlevel`=72, `VerifiedBuild`=23222 WHERE `entry`=24664; -- Kael'thas Sunstrider
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=24683; -- Sunblade Mage Guard
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=71, `maxlevel`=71, `VerifiedBuild`=23222 WHERE `entry`=24808; -- Broken Sentinel
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `VerifiedBuild`=23222 WHERE `entry`=24685; -- Sunblade Magister
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=71, `maxlevel`=71, `VerifiedBuild`=23222 WHERE `entry`=24698; -- Ethereum Smuggler
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `VerifiedBuild`=23222 WHERE `entry`=24684; -- Sunblade Blood Knight
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=24686; -- Sunblade Warlock
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `spell1`=0, `spell2`=0, `VerifiedBuild`=23222 WHERE `entry`=24687; -- Sunblade Physician
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=71, `maxlevel`=71, `VerifiedBuild`=23222 WHERE `entry`=24696; -- Coilskar Witch
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `VerifiedBuild`=23222 WHERE `entry`=17893; -- Naturalist Bite
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=72, `maxlevel`=72, `speed_run`=0.9571428, `spell1`=0, `spell2`=0, `spell3`=0, `spell4`=0, `VerifiedBuild`=23222 WHERE `entry`=17942; -- Quagmirran
UPDATE `creature_template` SET `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=22421; -- Skar'this the Heretic
UPDATE `creature_template` SET `difficulty_entry_1`=0, `minlevel`=70, `maxlevel`=70, `speed_run`=1, `VerifiedBuild`=23222 WHERE `entry`=21127; -- Coilfang Tempest
UPDATE `creature_template` SET `speed_walk`=4, `speed_run`=1.428571, `VerifiedBuild`=23222 WHERE `entry`=111706; -- Boulder
UPDATE `creature_template` SET `speed_walk`=0.8, `speed_run`=0.2857143, `VerifiedBuild`=23222 WHERE `entry`=99664; -- Restless Soul
UPDATE `creature_template` SET `unit_flags`=768, `VerifiedBuild`=23222 WHERE `entry`=112725; -- Kalyndras
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=98813; -- Bloodscent Felhound
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=4196352, `VerifiedBuild`=23222 WHERE `entry`=100759; -- Fel Bat
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=537166656, `unit_flags2`=2099201, `VerifiedBuild`=23222 WHERE `entry`=98792; -- Wyrmtongue Scavenger
UPDATE `creature_template` SET `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33555200, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=103662; -- Secret Door Stalker
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=98521; -- Lord Etheldrin Ravencrest
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=98538; -- Lady Velandras Ravencrest
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=98370; -- Ghostly Councilor
UPDATE `creature_template` SET `minlevel`=111, `maxlevel`=111, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=98368; -- Ghostly Protector
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=16777216, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=98806; -- Soul Essence
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32832, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=98366; -- Ghostly Retainer
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=190, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=98362; -- Troubled Soul
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `VerifiedBuild`=23222 WHERE `entry`=14561; -- Swift Brown Steed
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=14559; -- Swift Palomino
UPDATE `creature_template` SET `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=14560; -- Swift White Steed
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=4269; -- Chestnut Mare
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=23222 WHERE `entry`=43694; -- Katie Stokx
UPDATE `creature_template` SET `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=308; -- Black Stallion
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `VerifiedBuild`=23222 WHERE `entry`=284; -- Brown Horse
UPDATE `creature_template` SET `maxlevel`=4, `VerifiedBuild`=23222 WHERE `entry`=42339; -- Canal Crab
UPDATE `creature_template` SET `npcflag`=1, `unit_flags`=32768, `VerifiedBuild`=23222 WHERE `entry`=18927; -- Human Commoner
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=14, `speed_walk`=1.6, `speed_run`=0.5714286, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=16384, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=105147; -- Slag Elemental
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=110, `faction`=16, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=2099200, `VerifiedBuild`=23222 WHERE `entry`=105137; -- Wild Inferno
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3, `VerifiedBuild`=23222 WHERE `entry`=92166; -- Raider's Training Dummy
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=23222 WHERE `entry`=92165; -- Dungeoneer's Training Dummy
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=23222 WHERE `entry`=116597; -- Uncrowned Armor
UPDATE `creature_template` SET `npcflag`=134217729, `VerifiedBuild`=23222 WHERE `entry`=109609; -- Lorena Belle
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=105991; -- Night-Stalker Ku'nanji
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=105979; -- Lonika Stillblade
UPDATE `creature_template` SET `minlevel`=104, `maxlevel`=104, `VerifiedBuild`=23222 WHERE `entry`=111897; -- Dagg
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=23222 WHERE `entry`=98093; -- Scouting Map
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=2110, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=102550; -- Vanessa VanCleef
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33280, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=94141; -- Garona Halforcen
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=113186; -- Defias Thief
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=113152; -- Defias Thief
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=113139; -- Pirate
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33536, `unit_flags2`=34816, `VerifiedBuild`=23222 WHERE `entry`=106083; -- Yancey Grillsen
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=105998; -- Winstone Wolfe
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=104236; -- Kilejo
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2789, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=4, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=116801; -- Max Crits
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=23222 WHERE `entry`=110348; -- Mal
UPDATE `creature_template` SET `faction`=775, `unit_flags`=33024, `VerifiedBuild`=23222 WHERE `entry`=15760; -- Winter Reveler

DELETE FROM `gameobject_template` WHERE `entry` IN (257855 /*Scepter Appearance*/, 265486 /*Command Ship*/, 259961 /*Chair*/, 259960 /*Chair*/, 259962 /*Chair*/, 259963 /*Chair*/, 259965 /*Chair*/, 259964 /*Chair*/, 266656 /*Nether Barrier*/, 266593 /*Tower Door*/, 265488 /*Bookshelf*/, 266592 /*Medivh's Footlocker*/, 266577 /*Falling Books*/, 266295 /*Shattered Board*/, 265602 /*Sealed Tome*/, 266576 /*Falling Books*/, 265489 /*Scroll*/, 259959 /*Chair*/, 259958 /*Chair*/, 266826 /*Medivh's Footlocker*/, 266846 /*Door*/, 266806 /*Mysterious Wall*/, 266510 /*Suspicious Bookcase*/, 259985 /*Door*/, 266896 /*Doodad_7DU_KarazhanB_BooksFall001*/, 266894 /*Doodad_7DU_KarazhanB_BooksFall002*/, 259980 /*Doodad_GeneralMedChair99*/, 259979 /*Doodad_GeneralMedChair98*/, 259978 /*Doodad_GeneralMedChair97*/, 259977 /*Doodad_GeneralMedChair96*/, 259976 /*Doodad_GeneralMedChair95*/, 259975 /*Doodad_GeneralMedChair94*/, 259974 /*Doodad_GeneralMedChair93*/, 259973 /*Doodad_GeneralMedChair92*/, 259972 /*Doodad_GeneralMedChair91*/, 259971 /*Doodad_GeneralMedChair90*/, 259970 /*Doodad_GeneralMedChair89*/, 259969 /*Doodad_GeneralMedChair86*/, 259968 /*Doodad_GeneralMedChair85*/, 259967 /*Doodad_GeneralMedChair84*/, 259966 /*Doodad_GeneralMedChair83*/, 266895 /*Doodad_7DU_KarazhanB_BooksFall003*/, 259984 /*Doodad_GeneralChairHighEnd180*/, 259983 /*Doodad_GeneralChairHighEnd179*/, 259982 /*Doodad_GeneralChairHighEnd178*/, 259986 /*Doodad_UldamanScroll70*/, 259981 /*Doodad_GeneralChairHighEnd08*/, 266878 /*Terrifying Stories to Tell at Night*/, 265579 /*Naraxas Collision*/, 259052 /*Truthguard Appearance*/, 258882 /*Truthguard Appearance*/, 252670 /*Peerless Challenger's Cache*/, 252669 /*Superior Challenger's Cache*/, 265597 /*Sealed Tome*/, 260304 /*Westfall Story Background*/, 260307 /*Westfall Story Murloc Hut*/, 260306 /*Westfall Story House*/, 260305 /*Westfall Story Hay Stack*/, 260308 /*Westfall Story Rock*/, 259693 /*Chair*/, 259696 /*Chair*/, 259692 /*Chair*/, 259691 /*Chair*/, 259695 /*Chair*/, 259666 /*Chair*/, 266905 /*Chair*/, 259681 /*Chair*/, 259682 /*Chair*/, 259694 /*Stage Door Left*/, 259668 /*Chair*/, 259683 /*Chair*/, 259684 /*Stage Curtain*/, 259637 /*Chair*/, 259638 /*Chair*/, 259639 /*Chair*/, 259640 /*Chair*/, 259702 /*Stage Door Right*/, 259649 /*Chair*/, 259651 /*Chair*/, 259647 /*Chair*/, 259648 /*Chair*/, 259650 /*Chair*/, 259635 /*Chair*/, 259634 /*Chair*/, 259632 /*Chair*/, 259652 /*Chair*/, 259653 /*Chair*/, 259633 /*Chair*/, 259654 /*Chair*/, 259670 /*Chair*/, 259636 /*Chair*/, 259655 /*Chair*/, 259676 /*Chair*/, 259656 /*Chair*/, 266903 /*Chair*/, 266904 /*Chair*/, 259671 /*Chair*/, 259678 /*Chair*/, 259644 /*Chair*/, 259642 /*Chair*/, 265605 /*Sealed Tome*/, 259657 /*Chair*/, 259641 /*Chair*/, 259658 /*Chair*/, 259660 /*Chair*/, 259643 /*Chair*/, 259659 /*Chair*/, 259665 /*Chair*/, 259662 /*Chair*/, 259661 /*Chair*/, 259698 /*Chair*/, 259664 /*Chair*/, 259663 /*Chair*/, 259646 /*Chair*/, 259675 /*Chair*/, 266858 /*Portal to Karazhan Entrance*/, 259645 /*Chair*/, 259674 /*Chair*/, 259672 /*Chair*/, 259680 /*Chair*/, 259673 /*Chair*/, 259679 /*Chair*/, 259586 /*Chair*/, 266508 /*Strange Wall*/, 259701 /*Chair*/, 259700 /*Chair*/, 259699 /*Chair*/, 259578 /*Chair*/, 259582 /*Chair*/, 259713 /*Chair*/, 259577 /*Chair*/, 259712 /*Chair*/, 259704 /*Chair*/, 259583 /*Chair*/, 259714 /*Chair*/, 259579 /*Chair*/, 259705 /*Chair*/, 259602 /*Chair*/, 259594 /*Chair*/, 259593 /*Chair*/, 259603 /*Chair*/, 259581 /*Chair*/, 259576 /*Chair*/, 259600 /*Chair*/, 265600 /*Sealed Tome*/, 259580 /*Chair*/, 259590 /*Chair*/, 259601 /*Chair*/, 259575 /*Chair*/, 259591 /*Chair*/, 265599 /*Sealed Tome*/, 259592 /*Chair*/, 265604 /*Sealed Tome*/, 259613 /*Chair*/, 259614 /*Chair*/, 259622 /*Chair*/, 259703 /*Chair*/, 259574 /*Chair*/, 259615 /*Chair*/, 259621 /*Chair*/, 259572 /*Chair*/, 259573 /*Chair*/, 259616 /*Chair*/, 259595 /*Chair*/, 259620 /*Chair*/, 259617 /*Chair*/, 259596 /*Chair*/, 259619 /*Chair*/, 259686 /*Chair*/, 259524 /*Chair*/, 259612 /*Chair*/, 259623 /*Chair*/, 259525 /*Chair*/, 259618 /*Chair*/, 259555 /*Chair*/, 259556 /*Chair*/, 259571 /*Chair*/, 259598 /*Chair*/, 259685 /*Chair*/, 259550 /*Chair*/, 259549 /*Chair*/, 259547 /*Chair*/, 259710 /*Chair*/, 259554 /*Chair*/, 259526 /*Chair*/, 259544 /*Chair*/, 259711 /*Chair*/, 259597 /*Chair*/, 259527 /*Chair*/, 259715 /*Chair*/, 259528 /*Chair*/, 259548 /*Chair*/, 259716 /*Chair*/, 259529 /*Chair*/, 259624 /*Chair*/, 259611 /*Chair*/, 259553 /*Chair*/, 259545 /*Chair*/, 259599 /*Chair*/, 259552 /*Chair*/, 259560 /*Chair*/, 259559 /*Chair*/, 259558 /*Chair*/, 259557 /*Chair*/, 259626 /*Chair*/, 259561 /*Chair*/, 259562 /*Chair*/, 259546 /*Chair*/, 259610 /*Chair*/, 259604 /*Chair*/, 259625 /*Chair*/, 259687 /*Chair*/, 259628 /*Chair*/, 259542 /*Chair*/, 259688 /*Chair*/, 259563 /*Chair*/, 259609 /*Chair*/, 259719 /*Chair*/, 259541 /*Chair*/, 265598 /*Sealed Tome*/, 259605 /*Chair*/, 259718 /*Chair*/, 259627 /*Chair*/, 259567 /*Chair*/, 259721 /*Chair*/, 259717 /*Chair*/, 259722 /*Chair*/, 259723 /*Doodad_potbellystove03*/, 259540 /*Chair*/, 259608 /*Chair*/, 259631 /*Chair*/, 259720 /*Chair*/, 259629 /*Chair*/, 259607 /*Chair*/, 259630 /*Chair*/, 259606 /*Chair*/, 259539 /*Chair*/, 259707 /*Chair*/, 259588 /*Chair*/, 259708 /*Chair*/, 259538 /*Chair*/, 259589 /*Chair*/, 259706 /*Chair*/, 259709 /*Chair*/, 259564 /*Chair*/, 259537 /*Chair*/, 259565 /*Chair*/, 259585 /*Chair*/, 259536 /*Chair*/, 259566 /*Chair*/, 259584 /*Chair*/, 259535 /*Chair*/, 259587 /*Chair*/, 259534 /*Chair*/, 259543 /*Chair*/, 259726 /*Chair*/, 259725 /*Chair*/, 259551 /*Chair*/, 259530 /*Chair*/, 265603 /*Sealed Tome*/, 259724 /*Chair*/, 259570 /*Chair*/, 259928 /*Ghost Trap*/, 259533 /*Chair*/, 259531 /*Chair*/, 259532 /*Chair*/, 259569 /*Chair*/, 259727 /*Chair*/, 259568 /*Chair*/, 266803 /*Door*/, 259689 /*Door*/, 259728 /*Doodad_Karazahn_silverrmDoor03*/, 259690 /*Door*/, 259697 /*Door*/, 266804 /*Door*/, 259730 /*Grand Ballroom*/, 265594 /*Cage*/, 265575 /*Legion Portal*/, 259235 /*Ley Energy*/, 259236 /*Ley Energy*/, 259234 /*Ley Energy*/, 265574 /*Fel Rune*/, 265595 /*Access Device*/, 265589 /*Cage*/, 265588 /*Torch*/, 265590 /*Brazier*/, 266746 /*Box of 'New' Horseshoes*/, 260287 /*Fel Corruption*/, 265578 /*Fel Corruption*/, 265581 /*Fel Corruption*/, 265580 /*Fel Corruption*/, 252678 /*Superior Challenger's Cache*/, 252862 /*The Age of Galakrond*/, 258912 /*Dreadblades Appearance*/, 252798 /*Artifact Research Notes*/, 252017 /*The Raven's Eye*/, 249427 /*Poster*/, 249426 /*Oar*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(257855, 5, 37339, 'Scepter Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Scepter Appearance
(265486, 43, 34994, 'Command Ship', '', '', '', 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Command Ship
(259961, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259960, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259962, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259963, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259965, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259964, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266656, 0, 39264, 'Nether Barrier', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Nether Barrier
(266593, 0, 39206, 'Tower Door', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Tower Door
(265488, 0, 6626, 'Bookshelf', '', '', '', 1, 0, 0, 3000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Bookshelf
(266592, 3, 10315, 'Medivh''s Footlocker', 'questinteract', '', '', 1, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1959, 0, 1, 0, 0, 70641, 0, 0, 0, 23222), -- Medivh's Footlocker
(266577, 10, 39195, 'Falling Books', '', '', '', 1, 0, 0, 0, 10000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Falling Books
(266295, 10, 39070, 'Shattered Board', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Shattered Board
(265602, 3, 338, 'Sealed Tome', '', '', '', 1, 43, 70342, 100, 0, 0, 0, 0, 265601, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(266576, 10, 39195, 'Falling Books', '', '', '', 1, 0, 0, 0, 10000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Falling Books
(265489, 5, 525, 'Scroll', '', '', '', 0.3663004, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Scroll
(259959, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259958, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266826, 10, 10315, 'Medivh''s Footlocker', '', '', '', 24.73121, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 232397, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 23222), -- Medivh's Footlocker
(266846, 0, 6631, 'Door', '', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Door
(266806, 0, 39163, 'Mysterious Wall', '', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Mysterious Wall
(266510, 0, 39165, 'Suspicious Bookcase', '', '', '', 1.465428, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Suspicious Bookcase
(259985, 0, 6626, 'Door', '', '', '', 1, 0, 0, 3000, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Door
(266896, 5, 39195, 'Doodad_7DU_KarazhanB_BooksFall001', '', '', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_7DU_KarazhanB_BooksFall001
(266894, 5, 39195, 'Doodad_7DU_KarazhanB_BooksFall002', '', '', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_7DU_KarazhanB_BooksFall002
(259980, 7, 91, 'Doodad_GeneralMedChair99', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair99
(259979, 7, 91, 'Doodad_GeneralMedChair98', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair98
(259978, 7, 91, 'Doodad_GeneralMedChair97', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair97
(259977, 7, 91, 'Doodad_GeneralMedChair96', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair96
(259976, 7, 91, 'Doodad_GeneralMedChair95', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair95
(259975, 7, 91, 'Doodad_GeneralMedChair94', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair94
(259974, 7, 91, 'Doodad_GeneralMedChair93', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair93
(259973, 7, 91, 'Doodad_GeneralMedChair92', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair92
(259972, 7, 91, 'Doodad_GeneralMedChair91', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair91
(259971, 7, 91, 'Doodad_GeneralMedChair90', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair90
(259970, 7, 91, 'Doodad_GeneralMedChair89', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair89
(259969, 7, 91, 'Doodad_GeneralMedChair86', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair86
(259968, 7, 91, 'Doodad_GeneralMedChair85', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair85
(259967, 7, 91, 'Doodad_GeneralMedChair84', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair84
(259966, 7, 91, 'Doodad_GeneralMedChair83', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralMedChair83
(266895, 5, 39195, 'Doodad_7DU_KarazhanB_BooksFall003', '', '', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_7DU_KarazhanB_BooksFall003
(259984, 7, 92, 'Doodad_GeneralChairHighEnd180', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralChairHighEnd180
(259983, 7, 92, 'Doodad_GeneralChairHighEnd179', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralChairHighEnd179
(259982, 7, 92, 'Doodad_GeneralChairHighEnd178', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralChairHighEnd178
(259986, 5, 525, 'Doodad_UldamanScroll70', '', '', '', 0.3663004, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_UldamanScroll70
(259981, 7, 92, 'Doodad_GeneralChairHighEnd08', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_GeneralChairHighEnd08
(266878, 10, 14799, 'Terrifying Stories to Tell at Night', 'questinteract', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 232658, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46662, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Terrifying Stories to Tell at Night
(265579, 0, 13594, 'Naraxas Collision', '', '', '', 1.1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Naraxas Collision
(259052, 5, 37883, 'Truthguard Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Truthguard Appearance
(258882, 5, 37878, 'Truthguard Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Truthguard Appearance
(252670, 3, 33268, 'Peerless Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1839, 0, 1, 0, 0, 68461, 0, 5, 110, 23222), -- Peerless Challenger's Cache
(252669, 3, 33268, 'Superior Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1839, 0, 1, 0, 0, 68460, 0, 5, 110, 23222), -- Superior Challenger's Cache
(265597, 3, 6893, 'Sealed Tome', '', '', '', 1, 43, 70342, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(260304, 5, 38490, 'Westfall Story Background', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Westfall Story Background
(260307, 5, 38493, 'Westfall Story Murloc Hut', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Westfall Story Murloc Hut
(260306, 5, 38492, 'Westfall Story House', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Westfall Story House
(260305, 5, 38491, 'Westfall Story Hay Stack', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Westfall Story Hay Stack
(260308, 5, 38494, 'Westfall Story Rock', '', '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Westfall Story Rock
(259693, 7, 39, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259696, 7, 39, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259692, 7, 39, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259691, 7, 39, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259695, 7, 39, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259666, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266905, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259681, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259682, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259694, 0, 6633, 'Stage Door Left', '', '', '', 0.9999999, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Stage Door Left
(259668, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259683, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259684, 0, 7113, 'Stage Curtain', '', '', '', 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Stage Curtain
(259637, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259638, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259639, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259640, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259702, 0, 6633, 'Stage Door Right', '', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Stage Door Right
(259649, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259651, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259647, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259648, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259650, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259635, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259634, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259632, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259652, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259653, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259633, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259654, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259670, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259636, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259655, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259676, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259656, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266903, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266904, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259671, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259678, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259644, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259642, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(265605, 3, 417, 'Sealed Tome', '', '', '', 1, 43, 70342, 0, 1, 0, 0, 0, 265601, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(259657, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259641, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259658, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259660, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259643, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259659, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259665, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259662, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259661, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259698, 7, 648, 'Chair', '', '', '', 1.271522, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259664, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259663, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259646, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259675, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266858, 22, 6831, 'Portal to Karazhan Entrance', '', '', '', 1, 232406, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Portal to Karazhan Entrance
(259645, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259674, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259672, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259680, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259673, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259679, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259586, 7, 648, 'Chair', '', '', '', 0.9999996, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266508, 0, 39163, 'Strange Wall', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Strange Wall
(259701, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259700, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259699, 7, 648, 'Chair', '', '', '', 1.21285, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259578, 7, 648, 'Chair', '', '', '', 0.9999996, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259582, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259713, 7, 39, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259577, 7, 648, 'Chair', '', '', '', 0.9999996, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259712, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259704, 7, 39, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259583, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259714, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259579, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259705, 7, 39, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259602, 7, 92, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259594, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259593, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259603, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259581, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259576, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259600, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(265600, 3, 6893, 'Sealed Tome', '', '', '', 1, 43, 70342, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(259580, 7, 648, 'Chair', '', '', '', 0.9999996, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259590, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259601, 7, 92, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259575, 7, 648, 'Chair', '', '', '', 0.9999996, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259591, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(265599, 3, 6892, 'Sealed Tome', '', '', '', 1, 43, 70342, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(259592, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(265604, 3, 3731, 'Sealed Tome', '', '', '', 1, 43, 70342, 0, 1, 0, 0, 0, 265601, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(259613, 7, 92, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259614, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259622, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259703, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259574, 7, 648, 'Chair', '', '', '', 0.9999997, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259615, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259621, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259572, 7, 648, 'Chair', '', '', '', 0.9999997, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259573, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259616, 7, 92, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259595, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259620, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259617, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259596, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259619, 7, 92, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259686, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259524, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259612, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259623, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259525, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259618, 7, 92, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259555, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259556, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259571, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259598, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259685, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259550, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259549, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259547, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259710, 7, 39, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259554, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259526, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259544, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259711, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259597, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259527, 7, 648, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259715, 7, 39, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259528, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259548, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259716, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259529, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259624, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259611, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259553, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259545, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259599, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259552, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259560, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259559, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259558, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259557, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259626, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259561, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259562, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259546, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259610, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259604, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259625, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259687, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259628, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259542, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259688, 7, 92, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259563, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259609, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259719, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259541, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(265598, 3, 558, 'Sealed Tome', '', '', '', 1, 43, 70342, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(259605, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259718, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259627, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259567, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259721, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259717, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259722, 7, 39, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259723, 8, 348, 'Doodad_potbellystove03', '', '', '', 1.17, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_potbellystove03
(259540, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259608, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259631, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259720, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259629, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259607, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259630, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259606, 7, 92, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259539, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259707, 7, 39, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259588, 7, 648, 'Chair', '', '', '', 0.9999996, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259708, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259538, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259589, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259706, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259709, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259564, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259537, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259565, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259585, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259536, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259566, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222); -- Chair

INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(259584, 7, 648, 'Chair', '', '', '', 0.9999996, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259535, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259587, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259534, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259543, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259726, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259725, 7, 39, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259551, 7, 648, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259530, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(265603, 3, 4135, 'Sealed Tome', '', '', '', 1, 43, 70342, 0, 1, 0, 0, 0, 265601, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Sealed Tome
(259724, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259570, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259928, 22, 1247, 'Ghost Trap', '', '', '', 1.5, 227900, 1, 0, 0, 0, 45187, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Ghost Trap
(259533, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259531, 7, 648, 'Chair', '', '', '', 0.9999998, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259532, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259569, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259727, 7, 39, 'Chair', '', '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(259568, 7, 648, 'Chair', '', '', '', 0.9999999, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Chair
(266803, 0, 6629, 'Door', '', '', '', 1, 0, 2648, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Door
(259689, 0, 6629, 'Door', '', '', '', 1, 0, 2648, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Door
(259728, 0, 6625, 'Doodad_Karazahn_silverrmDoor03', '', '', '', 0.9451349, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Doodad_Karazahn_silverrmDoor03
(259690, 0, 6627, 'Door', '', '', '', 1.14085, 0, 0, 3000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Door
(259697, 0, 413, 'Door', '', '', '', 1.952174, 0, 0, 3000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Door
(266804, 0, 6625, 'Door', '', '', '', 0.8006391, 0, 2648, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Door
(259730, 0, 6630, 'Grand Ballroom', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Grand Ballroom
(265594, 5, 26854, 'Cage', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Cage
(265575, 10, 26244, 'Legion Portal', '', 'Destroying', '', 0.4, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 24585, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Legion Portal
(259235, 5, 29533, 'Ley Energy', '', '', '', 0.6, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Ley Energy
(259236, 5, 25185, 'Ley Energy', '', '', '', 0.6, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Ley Energy
(259234, 5, 29534, 'Ley Energy', '', '', '', 0.75, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Ley Energy
(265574, 5, 30992, 'Fel Rune', '', '', '', 0.75, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Fel Rune
(265595, 5, 29815, 'Access Device', '', '', '', 0.65, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Access Device
(265589, 5, 31443, 'Cage', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Cage
(265588, 5, 27615, 'Torch', '', '', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Torch
(265590, 5, 27286, 'Brazier', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Brazier
(266746, 3, 17937, 'Box of ''New'' Horseshoes', 'questinteract', 'Retrieving', '', 1, 1691, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70751, 1, 0, 0, 23222), -- Box of 'New' Horseshoes
(260287, 5, 31034, 'Fel Corruption', '', '', '', 0.65, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Fel Corruption
(265578, 5, 38963, 'Fel Corruption', '', '', '', 0.5, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Fel Corruption
(265581, 5, 31034, 'Fel Corruption', '', '', '', 0.4, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Fel Corruption
(265580, 5, 38963, 'Fel Corruption', '', '', '', 0.35, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Fel Corruption
(252678, 3, 33268, 'Superior Challenger''s Cache', '', '', '', 2.5, 1634, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 1835, 0, 1, 0, 0, 68456, 0, 5, 110, 23222), -- Superior Challenger's Cache
(252862, 3, 36036, 'The Age of Galakrond', 'openhandglow', 'Retrieving', '', 1, 1691, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 67949, 1, 0, 0, 23222), -- The Age of Galakrond
(258912, 5, 37904, 'Dreadblades Appearance', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Dreadblades Appearance
(252798, 45, 15781, 'Artifact Research Notes', '', '', '', 1, 188, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Artifact Research Notes
(252017, 10, 31225, 'The Raven''s Eye', 'questinteract', 'Using', '', 2, 93, 0, 0, 1, 0, 0, 0, 0, 0, 0, 219701, 0, 0, 0, 19700, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- The Raven's Eye
(249427, 5, 33362, 'Poster', '', '', '', 0.65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222), -- Poster
(249426, 5, 33361, 'Oar', '', '', '', 0.65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23222); -- Oar

UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216451; -- Rusted Sword
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216445; -- Shiny Yarn
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216446; -- Gorgeous Gem
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216442; -- Golden High Elf Statuette
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216437; -- Ruby Ring
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216449; -- Fluffy Pillow
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216450; -- Ancient Orcish Shield
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216431; -- Expensive Ruby
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216441; -- Golden Goblet
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216435; -- Spellstone Necklace
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216444; -- Gold Platter
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216448; -- Dusty Painting
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216433; -- Jade Kitten
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216440; -- Large Pile of Coins
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216439; -- Small Pile of Coins
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216436; -- Diamond Ring
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216434; -- Ruby Necklace
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216430; -- Cologne
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216447; -- Gold Fruit Bowl
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216438; -- Gold Ring
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216428; -- Fragrant Perfume
UPDATE `gameobject_template` SET `castBarCaption`='Looting', `Data14`=69544, `Data17`=19771, `VerifiedBuild`=23222 WHERE `entry`=216432; -- Sparkling Sapphire
UPDATE `gameobject_template` SET `type`=50, `Data3`=0, `Data4`=340, `Data5`=390, `Data6`=30, `Data7`=0, `Data12`=60, `Data13`=1, `Data14`=28694, `Data18`=10, `Data19`=0, `VerifiedBuild`=23222 WHERE `entry`=183045; -- Dreaming Glory
UPDATE `gameobject_template` SET `type`=50, `Data3`=0, `Data4`=350, `Data5`=400, `Data6`=30, `Data12`=65, `Data13`=1, `Data18`=10, `Data19`=0, `VerifiedBuild`=23222 WHERE `entry`=183043; -- Ragveil

DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=266592 AND `Idx`=1) OR (`GameObjectEntry`=266592 AND `Idx`=0) OR (`GameObjectEntry`=216328 AND `Idx`=0) OR (`GameObjectEntry`=216326 AND `Idx`=0) OR (`GameObjectEntry`=216325 AND `Idx`=0) OR (`GameObjectEntry`=266746 AND `Idx`=0) OR (`GameObjectEntry`=252862 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(266592, 1, 138482, 23222), -- Medivh's Footlocker
(266592, 0, 143537, 23222), -- Medivh's Footlocker
(216328, 0, 92495, 23222), -- Soulstone Fragment
(216326, 0, 92496, 23222), -- Soulstone Fragment
(216325, 0, 92497, 23222), -- Soulstone Fragment
(266746, 0, 143550, 23222), -- Box of 'New' Horseshoes
(252862, 0, 140112, 23222); -- The Age of Galakrond
