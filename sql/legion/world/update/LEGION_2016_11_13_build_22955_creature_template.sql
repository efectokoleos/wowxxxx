UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `speed_run`=1.571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=88750; -- Raptor
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `faction`=126, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=86884; -- Loyalistin der Dunkelspeere
UPDATE `creature_template` SET `minlevel`=10, `maxlevel`=10, `faction`=16, `speed_walk`=1, `speed_run`=0.8571429, `VerifiedBuild`=22995 WHERE `entry`=17648; -- Teufelsj�gerdiener
UPDATE `creature_template` SET `minlevel`=9, `VerifiedBuild`=22995 WHERE `entry`=3123; -- Blutkrallensensenschlund
UPDATE `creature_template` SET `npcflag`=3, `VerifiedBuild`=22995 WHERE `entry`=3362; -- Ogunaro Wolfsl�ufer
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `faction`=29, `VerifiedBuild`=22995 WHERE `entry`=31769; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=78, `maxlevel`=78, `faction`=29, `VerifiedBuild`=22995 WHERE `entry`=31757; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=79, `maxlevel`=79, `faction`=29, `VerifiedBuild`=22995 WHERE `entry`=31756; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=77, `maxlevel`=77, `faction`=29, `VerifiedBuild`=22995 WHERE `entry`=31758; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=78, `maxlevel`=78, `faction`=29, `VerifiedBuild`=22995 WHERE `entry`=31768; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=76, `maxlevel`=79, `faction`=29, `speed_walk`=1, `VerifiedBuild`=22995 WHERE `entry`=31755; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `npcflag`=18, `VerifiedBuild`=22995 WHERE `entry`=11178; -- Borgosh Glutformer
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=22995 WHERE `entry`=7231; -- Kelgruk Blutaxt
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=22995 WHERE `entry`=7230; -- Shayis Stahlzorn
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=49573; -- Karba Flammenschlund
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=34955; -- Karg Grindsch�del
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=22995 WHERE `entry`=65058; -- Schwarze Drachenschildkr�te
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=22995 WHERE `entry`=65065; -- Rote Drachenschildkr�te
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=22995 WHERE `entry`=65060; -- Blaue Drachenschildkr�te
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=46667; -- Klingenmeister Ronakada
UPDATE `creature_template` SET `minlevel`=2, `maxlevel`=2, `VerifiedBuild`=22995 WHERE `entry`=65074; -- Gro�e blaue Drachenschildkr�te
UPDATE `creature_template` SET `npcflag`=129, `VerifiedBuild`=22995 WHERE `entry`=66022; -- Schildkr�tenmeisterin Odai
UPDATE `creature_template` SET `faction`=188, `VerifiedBuild`=22995 WHERE `entry`=16069; -- Gurky
UPDATE `creature_template` SET `ManaModifierExtra`=1, `VerifiedBuild`=22995 WHERE `entry`=45244; -- Scharfseher Krogar
UPDATE `creature_template` SET `minlevel`=51, `maxlevel`=51, `VerifiedBuild`=22995 WHERE `entry`=7391; -- Hyazinthara
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=44787; -- Auktionatorin Sowata
UPDATE `creature_template` SET `npcflag`=4227, `VerifiedBuild`=22995 WHERE `entry`=3322; -- Kaja
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22995 WHERE `entry`=99403; -- Auferstandenes S�blerk�tzchen
UPDATE `creature_template` SET `unit_class`=4, `VerifiedBuild`=22995 WHERE `entry`=45095; -- Nachtpirscher Ku'nanji
UPDATE `creature_template` SET `npcflag`=17, `VerifiedBuild`=22995 WHERE `entry`=3404; -- Jandi
UPDATE `creature_template` SET `npcflag`=3200, `VerifiedBuild`=22995 WHERE `entry`=3405; -- Zeal'aya
UPDATE `creature_template` SET `npcflag`=131072, `VerifiedBuild`=22995 WHERE `entry`=45081; -- Makavu
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `npcflag`=2097152, `VerifiedBuild`=22995 WHERE `entry`=45082; -- Auktionatorin Ziji
UPDATE `creature_template` SET `npcflag`=640, `VerifiedBuild`=22995 WHERE `entry`=45094; -- Jin'diza
UPDATE `creature_template` SET `npcflag`=128, `VerifiedBuild`=22995 WHERE `entry`=45093; -- Huju
UPDATE `creature_template` SET `npcflag`=128, `VerifiedBuild`=22995 WHERE `entry`=8404; -- Xan'tish
UPDATE `creature_template` SET `npcflag`=640, `VerifiedBuild`=22995 WHERE `entry`=45008; -- Batamsi
UPDATE `creature_template` SET `npcflag`=209, `VerifiedBuild`=22995 WHERE `entry`=44975; -- Der alte Umbehto
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=22995 WHERE `entry`=30706; -- Jo'mah
UPDATE `creature_template` SET `npcflag`=128, `VerifiedBuild`=22995 WHERE `entry`=3400; -- Xen'to
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=22995 WHERE `entry`=3399; -- Zamja
UPDATE `creature_template` SET `npcflag`=128, `VerifiedBuild`=22995 WHERE `entry`=30723; -- Xantili
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=31, `npcflag`=1073741824, `unit_flags`=512, `VerifiedBuild`=22995 WHERE `entry`=62119; -- Roboglucke
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `npcflag`=0, `VerifiedBuild`=22995 WHERE `entry`=45015; -- Kopfj�ger der Dunkelspeere
UPDATE `creature_template` SET `unit_class`=4, `VerifiedBuild`=22995 WHERE `entry`=45565; -- Sanzi
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=45659; -- Auktionator Fenk
UPDATE `creature_template` SET `unit_class`=4, `VerifiedBuild`=22995 WHERE `entry`=45567; -- Miragohn Mixmeister
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=45827; -- Grunzer au�er Dienst
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=45822; -- Haudrauf au�er Dienst
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=22995 WHERE `entry`=45830; -- Belagerungsarbeiter au�er Dienst
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=30611; -- Greela "die Grunzerin" Rostkette
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=45814; -- Haudrauf von Orgrimmar
UPDATE `creature_template` SET `npcflag`=81, `VerifiedBuild`=22995 WHERE `entry`=46357; -- Gonto
UPDATE `creature_template` SET `minlevel`=36, `maxlevel`=93, `VerifiedBuild`=22995 WHERE `entry`=16445; -- Terky
UPDATE `creature_template` SET `faction`=35, `speed_run`=0.8571429, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=768, `VerifiedBuild`=22995 WHERE `entry`=15429; -- Ekelhafter Schlammling
UPDATE `creature_template` SET `minlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=88543; -- Magierin von Orgrimmar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=0, `unit_flags`=768, `VerifiedBuild`=22995 WHERE `entry`=68819; -- Arkanes Auge
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=68979; -- Leerenbinder Zorlan
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=57801; -- Thaumaturg Altha
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=54473; -- Warpweber Dushar
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=54472; -- Tresorh�ter Jazra
UPDATE `creature_template` SET `minlevel`=11, `maxlevel`=11, `VerifiedBuild`=22995 WHERE `entry`=3225; -- Verderbter scheckiger Eber
UPDATE `creature_template` SET `rank`=1, `VerifiedBuild`=22995 WHERE `entry`=31146; -- Trainingsattrappe des Schlachtzuges
UPDATE `creature_template` SET `maxlevel`=3, `VerifiedBuild`=22995 WHERE `entry`=14499; -- Waisenkind der Horde
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=188, `npcflag`=1073741824, `unit_flags`=512, `VerifiedBuild`=22995 WHERE `entry`=62115; -- Mistk�fer
UPDATE `creature_template` SET `maxlevel`=1, `VerifiedBuild`=22995 WHERE `entry`=61369; -- Kr�te
UPDATE `creature_template` SET `unit_flags`=33024, `VerifiedBuild`=22995 WHERE `entry`=14881; -- Spinne
UPDATE `creature_template` SET `speed_run`=1.142857, `VerifiedBuild`=22995 WHERE `entry`=3296; -- Grunzer von Orgrimmar
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=22995 WHERE `entry`=106655; -- Arkanomant Vridiel
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=0.9920629, `BaseAttackTime`=1856, `movementId`=0, `VerifiedBuild`=22995 WHERE `entry`=106548; -- Hati
UPDATE `creature_template` SET `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=90202; -- Abyssius
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=0, `VerifiedBuild`=22995 WHERE `entry`=95720; -- Druide des Geweihs
UPDATE `creature_template` SET `unit_flags`=537165824, `unit_flags2`=2049, `VerifiedBuild`=22995 WHERE `entry`=95719; -- Druidin des Geweihs
UPDATE `creature_template` SET `unit_flags`=294912, `VerifiedBuild`=22995 WHERE `entry`=93318; -- Aschenmaulraufer
UPDATE `creature_template` SET `npcflag`=3, `VerifiedBuild`=22995 WHERE `entry`=93974; -- Leyweberin Erenyi
UPDATE `creature_template` SET `BaseAttackTime`=8126, `unit_flags`=33587456, `VerifiedBuild`=22995 WHERE `entry`=94358; -- Aschenbringer
UPDATE `creature_template` SET `BaseAttackTime`=1706, `VerifiedBuild`=22995 WHERE `entry`=111536; -- Hohepriesterin Ishanah
UPDATE `creature_template` SET `faction`=2849, `unit_flags2`=2052, `VerifiedBuild`=22995 WHERE `entry`=106286; -- Sylvaneule
UPDATE `creature_template` SET `BaseAttackTime`=8126, `VerifiedBuild`=22995 WHERE `entry`=94365; -- Aschenbringer
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `npcflag`=640, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=112632; -- Magdalena D�mmersee
UPDATE `creature_template` SET `speed_run`=1.1, `VerifiedBuild`=22995 WHERE `entry`=108655; -- Brut von Serpentrix
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=2634, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=112856; -- Wahnsinnige Eulenbestie
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=8, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=106086; -- Verst�rter Welpling
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=2849, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `VerifiedBuild`=22995 WHERE `entry`=106061; -- Kriechender Alptraum
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=22995 WHERE `entry`=95138; -- Wurmkrallenhexe
UPDATE `creature_template` SET `HealthModifier`=7, `VerifiedBuild`=22995 WHERE `entry`=93205; -- Thondrax
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=22995 WHERE `entry`=110032; -- Schw�rendes Auge
UPDATE `creature_template` SET `unit_flags`=67141632, `VerifiedBuild`=22995 WHERE `entry`=92383; -- Rottholzranker
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1.142857, `VerifiedBuild`=22995 WHERE `entry`=95951; -- Schreckensstichlauerer
UPDATE `creature_template` SET `HealthModifier`=7, `VerifiedBuild`=22995 WHERE `entry`=95123; -- Grelda die Hexe
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=22995 WHERE `entry`=97531; -- Terrorlarve
UPDATE `creature_template` SET `BaseAttackTime`=1749, `VerifiedBuild`=22995 WHERE `entry`=111962; -- Adept der Illidari
UPDATE `creature_template` SET `KillCredit1`=115527, `HealthModifier`=3, `VerifiedBuild`=22995 WHERE `entry`=108941; -- Phantomoffizier
UPDATE `creature_template` SET `KillCredit1`=115527, `VerifiedBuild`=22995 WHERE `entry`=93061; -- Auferstandener S�bler
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=22995 WHERE `entry`=100409; -- D�mmerheuler
UPDATE `creature_template` SET `KillCredit1`=115527, `VerifiedBuild`=22995 WHERE `entry`=92954; -- Auferstandener Soldat
UPDATE `creature_template` SET `KillCredit1`=115527, `VerifiedBuild`=22995 WHERE `entry`=91860; -- Wiederbelebte Bogensch�tzin
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=768, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=85387; -- Obstj�ger
UPDATE `creature_template` SET `minlevel`=110, `BaseAttackTime`=1267, `VerifiedBuild`=22995 WHERE `entry`=19668; -- Schattengeist
UPDATE `creature_template` SET `BaseAttackTime`=1730, `VerifiedBuild`=22995 WHERE `entry`=102199; -- Stampede
UPDATE `creature_template` SET `BaseAttackTime`=1331, `VerifiedBuild`=22995 WHERE `entry`=62982; -- Geistbeuger
UPDATE `creature_template` SET `minlevel`=110, `BaseAttackTime`=1730, `VerifiedBuild`=22995 WHERE `entry`=113347; -- Wildtier
UPDATE `creature_template` SET `BaseAttackTime`=1689, `VerifiedBuild`=22995 WHERE `entry`=98167; -- Leerententakel
UPDATE `creature_template` SET `minlevel`=110, `faction`=35, `speed_walk`=1, `speed_run`=1, `BaseAttackTime`=1730, `movementId`=0, `VerifiedBuild`=22995 WHERE `entry`=100324; -- Hati
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=92746; -- Knorriges Urtum
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22995 WHERE `entry`=92874; -- Akolyth von Elothir
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22995 WHERE `entry`=91149; -- Akolythin von Elothir
UPDATE `creature_template` SET `unit_flags`=32768, `VerifiedBuild`=22995 WHERE `entry`=91121; -- Steinblutverheererin
UPDATE `creature_template` SET `npcflag`=0, `VerifiedBuild`=22995 WHERE `entry`=91150; -- Akolyth von Elothir
UPDATE `creature_template` SET `BaseAttackTime`=1802, `VerifiedBuild`=22995 WHERE `entry`=111906; -- Krieger der Aschenzungen
UPDATE `creature_template` SET `minlevel`=104, `maxlevel`=104, `VerifiedBuild`=22995 WHERE `entry`=35396; -- Pfeilschnelles Jungtier
UPDATE `creature_template` SET `speed_run`=1.385714, `BaseAttackTime`=1625, `VerifiedBuild`=22995 WHERE `entry`=111292; -- Ritter der Silbernen Hand
UPDATE `creature_template` SET `BaseAttackTime`=1576, `VerifiedBuild`=22995 WHERE `entry`=111510; -- Lichtbrut
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=93483; -- Alptraumglocke
UPDATE `creature_template` SET `faction`=35, `speed_run`=0.8571429, `unit_flags`=33536, `VerifiedBuild`=22995 WHERE `entry`=32595; -- Pengu
UPDATE `creature_template` SET `BaseAttackTime`=1512, `VerifiedBuild`=22995 WHERE `entry`=111694; -- Todesritter von Acherus
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=113984; -- Zornbringer Murky
UPDATE `creature_template` SET `speed_run`=1.385714, `BaseAttackTime`=1715, `VerifiedBuild`=22995 WHERE `entry`=111914; -- Eisenzwerg
UPDATE `creature_template` SET `faction`=35, `speed_run`=1.142857, `VerifiedBuild`=22995 WHERE `entry`=114281; -- Reittier des Flugmeisters
UPDATE `creature_template` SET `speed_run`=1.142857, `BaseAttackTime`=1448, `VerifiedBuild`=22995 WHERE `entry`=109448; -- Erdenrufer
UPDATE `creature_template` SET `minlevel`=107, `BaseAttackTime`=1755, `VerifiedBuild`=22995 WHERE `entry`=109839; -- Druide der Klaue
UPDATE `creature_template` SET `speed_run`=1.142857, `BaseAttackTime`=1860, `VerifiedBuild`=22995 WHERE `entry`=110975; -- Unsichtbarer Pfadfinder
UPDATE `creature_template` SET `modelid1`=15405, `VerifiedBuild`=22995 WHERE `entry`=93750; -- Feenflitzer
UPDATE `creature_template` SET `modelid1`=74537, `VerifiedBuild`=22995 WHERE `entry`=106901; -- Sylvia Hirschhorn
UPDATE `creature_template` SET `speed_run`=1.142857, `BaseAttackTime`=1734, `VerifiedBuild`=22995 WHERE `entry`=110456; -- Unsichtbare Sch�tzin
UPDATE `creature_template` SET `BaseAttackTime`=1532, `VerifiedBuild`=22995 WHERE `entry`=111506; -- Dunkler Zelot
UPDATE `creature_template` SET `faction`=2892, `unit_flags2`=33556480, `HealthModifier`=5, `VerifiedBuild`=22995 WHERE `entry`=93462; -- Schildwache von Lorlathil
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=22995 WHERE `entry`=97555; -- Umbraringelschwanz
UPDATE `creature_template` SET `unit_flags`=294912, `VerifiedBuild`=22995 WHERE `entry`=109807; -- Wilder Nachts�bler
UPDATE `creature_template` SET `unit_flags`=0, `VerifiedBuild`=22995 WHERE `entry`=88465; -- Tangkrabbler
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=101554; -- Nadelr�ckenkrabbler
UPDATE `creature_template` SET `minlevel`=105, `faction`=35, `speed_run`=1.142857, `BaseAttackTime`=1879, `VerifiedBuild`=22995 WHERE `entry`=110715; -- Herbeiruferin der Kirin Tor
UPDATE `creature_template` SET `modelid1`=22255, `VerifiedBuild`=22995 WHERE `entry`=91717; -- Azsunaolivr�cken
UPDATE `creature_template` SET `modelid1`=43225, `VerifiedBuild`=22995 WHERE `entry`=91715; -- Azsunak�nigsfeder
UPDATE `creature_template` SET `unit_flags`=536904448, `unit_flags2`=2099201, `VerifiedBuild`=22995 WHERE `entry`=89940; -- Schuppenw�chter der Azurschwingen
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=106772; -- Exotisches Buch
UPDATE `creature_template` SET `VignetteID`=1824, `VerifiedBuild`=22995 WHERE `entry`=104523; -- Shalas'aman
UPDATE `creature_template` SET `speed_walk`=0.8, `speed_run`=0.8, `VerifiedBuild`=22995 WHERE `entry`=92180; -- Sehsei
UPDATE `creature_template` SET `minlevel`=23, `maxlevel`=105, `VerifiedBuild`=22995 WHERE `entry`=95841; -- Knurps
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=107, `VerifiedBuild`=22995 WHERE `entry`=112079; -- Purpurner Pilger
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=22995 WHERE `entry`=93189; -- Mama Buddel
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1, `BaseAttackTime`=1743, `movementId`=0, `VerifiedBuild`=22995 WHERE `entry`=106551; -- Hati
UPDATE `creature_template` SET `minlevel`=104, `VerifiedBuild`=22995 WHERE `entry`=105904; -- Rabe
UPDATE `creature_template` SET `minlevel`=104, `maxlevel`=106, `speed_walk`=0.666668, `speed_run`=1.142857, `VerifiedBuild`=22995 WHERE `entry`=96643; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=102, `VerifiedBuild`=22995 WHERE `entry`=96639; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=106, `speed_run`=1.142857, `VerifiedBuild`=22995 WHERE `entry`=96641; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=109, `VerifiedBuild`=22995 WHERE `entry`=96636; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `maxlevel`=105, `speed_run`=1.142857, `VerifiedBuild`=22995 WHERE `entry`=96635; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `minlevel`=109, `maxlevel`=109, `speed_run`=1.142857, `VerifiedBuild`=22995 WHERE `entry`=96592; -- Im Stall abgestellter J�gerbegleiter
UPDATE `creature_template` SET `unit_flags`=33587456, `VerifiedBuild`=22995 WHERE `entry`=17213; -- Besen
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=99, `VerifiedBuild`=22995 WHERE `entry`=107622; -- Gierina
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=110687; -- Yalia Weisenwisper
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=22995 WHERE `entry`=110595; -- Lilith
UPDATE `creature_template` SET `minlevel`=103, `maxlevel`=103, `VerifiedBuild`=22995 WHERE `entry`=102587; -- Saa'ra
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=22995 WHERE `entry`=102589; -- Schlachtkarte
UPDATE `creature_template` SET `type_flags2`=32768, `VerifiedBuild`=22995 WHERE `entry`=110564; -- Alonsus Faol
UPDATE `creature_template` SET `minlevel`=104, `maxlevel`=104, `VerifiedBuild`=22995 WHERE `entry`=110571; -- Delas Mondfang
UPDATE `creature_template` SET `minlevel`=110, `faction`=2402, `VerifiedBuild`=22995 WHERE `entry`=103159; -- Baby Winston
UPDATE `creature_template` SET `minlevel`=24, `maxlevel`=24, `VerifiedBuild`=22995 WHERE `entry`=86719; -- Brillante Spore
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=47944; -- Dunkles Ph�nixk�ken
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `VerifiedBuild`=22995 WHERE `entry`=51090; -- Singende Sonnenblume
UPDATE `creature_template` SET `minlevel`=73, `maxlevel`=73, `VerifiedBuild`=22995 WHERE `entry`=32939; -- Kleines Rehkitz
UPDATE `creature_template` SET `modelid1`=62217, `VerifiedBuild`=22995 WHERE `entry`=32589; -- Madenhackerjunges
UPDATE `creature_template` SET `minlevel`=1, `maxlevel`=1, `faction`=188, `VerifiedBuild`=22995 WHERE `entry`=7565; -- Schwarze K�nigsnatter
UPDATE `creature_template` SET `minlevel`=19, `maxlevel`=19, `faction`=35, `speed_run`=0.8571429, `unit_flags`=33536, `VerifiedBuild`=22995 WHERE `entry`=28883; -- Frosti
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `speed_run`=0.8571429, `RangeAttackTime`=2000, `unit_flags`=768, `VerifiedBuild`=22995 WHERE `entry`=16085; -- Q. Pido
UPDATE `creature_template` SET `faction`=35, `VerifiedBuild`=22995 WHERE `entry`=21055; -- Goldenes Drachenfalkenjunges