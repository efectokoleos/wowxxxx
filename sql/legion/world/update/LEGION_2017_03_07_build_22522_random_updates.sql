DELETE FROM `scene_template` WHERE (`SceneId`=1459 AND `ScriptPackageID`=1714) OR (`SceneId`=1235 AND `ScriptPackageID`=1595) OR (`SceneId`=1234 AND `ScriptPackageID`=1593);
INSERT INTO `scene_template` (`SceneId`, `Flags`, `ScriptPackageID`) VALUES
(1459, 11, 1714),
(1235, 21, 1595),
(1234, 29, 1593);

DELETE FROM `quest_offer_reward` WHERE `ID` IN (41258 /*Fruit of the Doom*/, 41256 /*Blast of Spice Fish*/, 41107 /*Bad Apples*/, 41309 /*Mangelrath*/, 41214 /*Parts Unknown*/, 41222 /*Into The Pit!*/, 41140 /*Search and Rescue!*/, 43594 /*Leyline Feed: Halls of the Eclipse*/, 41139 /*The Key Is Around Here Somewhere...*/, 40470 /*Quality of Life*/, 41575 /*Felsoul Teleporter Online!*/, 40424 /*A Desperate Journey*/, 40469 /*Final Preparations*/, 40401 /*A Way Back In*/, 43341 /*Uniting the Isles*/, 41452 /*Feline Frantic*/, 43813 /*Sanctum of Order Teleporter Online!*/, 40010 /*Tapping the Leylines*/, 41760 /*Kel'danath's Legacy*/, 41704 /*Subject 16*/, 40796 /*Lingering on the Edge*/, 41702 /*Written in Stone*/, 40326 /*Scattered Memories*/, 41149 /*A Re-Warding Effort*/, 40012 /*An Old Ally*/, 40956 /*Survey Says...*/, 44691 /*Hungry Work*/, 44672 /*Ancient Mana*/, 40830 /*Close Enough*/, 40748 /*Network Security*/, 40747 /*The Delicate Art of Telemancy*/, 40011 /*Oculeth's Workshop*/, 43476 /*Experimental Potion: Test Subjects Needed*/, 42120 /*The Silver Hand*/, 42377 /*The Brother's Trail*/, 43349 /*The Aegis of Aggramar*/, 40072 /*Securing the Aegis*/, 42483 /*Put It All on Red*/, 42454 /*The Hammer of Khaz'goroth*/, 39781 /*Neltharion's Lair*/, 44055 /*They Have A Pitlord*/, 44547 /*Isle Hopping*/, 44464 /*Awakenings*/, 43486 /*Cracking the Codex*/, 43494 /*Silver Hand Knights*/, 42852 /*Champion: Justicar Julia Celeste*/, 42851 /*Champion: Vindicator Boros*/, 42890 /*The Codex of Command*/, 42888 /*Communication Orbs*/, 43462 /*Mother Ozram*/, 40653 /*Making Trails*/, 40652 /*Word on the Winds*/, 41332 /*Ascending The Circle*/, 40651 /*The Seed of Ages*/, 41255 /*Sowing The Seed*/, 41689 /*Cleansing the Mother Tree*/, 41690 /*Reconvene*/, 41436 /*In Deep Slumber*/, 41449 /*Join the Dreamer*/, 41422 /*Necessary Preparations*/, 40649 /*Meet with Mylune*/, 40646 /*Weapons of Legend*/, 40645 /*To The Dreamgrove*/, 40644 /*The Dreamway*/, 41106 /*Call of the Wilds*/, 40643 /*A Summons From Moonglade*/, 42213 /*The Tidestone of Golganneth*/, 38286 /*Wrath of Azshara*/, 44448 /*In the House of Light and Shadow*/, 44337 /*Goddess Watch Over You*/, 42967 /*The Highlord's Command*/, 44250 /*Champion of the Light*/, 43883 /*Hitting the Books*/, 39839 /*Mysterious Dust*/, 39838 /*Fire!*/, 42229 /*Shal'Aran*/, 40009 /*Arcane Thirst*/, 40123 /*The Nightborne Pact*/, 40008 /*The Only Way Out is Through*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(41258, 11, 0, 0, 0, 0, 0, 0, 0, 'Victory tastes sweeter than any dish, even one of mine.', 22522), -- Fruit of the Doom
(41256, 1, 0, 0, 0, 0, 0, 0, 0, 'Manatrout was completely fished out of our harbors. Since the shield was raised all we have had of the liquor is memories on our palates.', 22522), -- Blast of Spice Fish
(41107, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. This will be enough for my purposes.', 22522), -- Bad Apples
(41309, 0, 0, 0, 0, 0, 0, 0, 0, 'It''s good to see a friendly face, I don''t know how much longer I could have held out on my own!', 22522), -- Mangelrath
(41214, 0, 0, 0, 0, 0, 0, 0, 0, '<Angus wipes away a tear as he speaks.>$b$bBrambley, lad... I promise ye''ll have tha'' best burial money can buy! An'' all the ale ye can drink...', 22522), -- Parts Unknown
(41222, 0, 0, 0, 0, 0, 0, 0, 0, 'It canna'' be true!$b$bBrambley... my dear ol'' friend! Why did ye have to go an'' die?', 22522), -- Into The Pit!
(41140, 0, 0, 0, 0, 0, 0, 0, 0, 'It''s good to finally be free from this dreadful cage. As thanks, I will keep my word to you and your grotesquely hairy friend.', 22522), -- Search and Rescue!
(43594, 0, 0, 0, 0, 0, 0, 0, 0, '<As you imbue the feed with the mana the structure stirs to life. Your skin tingles as the surrounding air becomes charged with arcane power.>', 22522), -- Leyline Feed: Halls of the Eclipse
(41139, 0, 0, 0, 0, 0, 0, 0, 0, 'Ye found the key! Quickly, see if it fits...$b$bAye, that''s the one! Ye did it!', 22522), -- The Key Is Around Here Somewhere...
(40470, 1, 0, 0, 0, 0, 0, 0, 0, 'This is a most troubling tale. $b$bYou have done a good thing, $n. Now he will no longer torment our people.', 22522), -- Quality of Life
(41575, 0, 0, 0, 0, 0, 0, 0, 0, '<The beacon glows as you infuse it with Ancient Mana.>', 22522), -- Felsoul Teleporter Online!
(40424, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. Your part here is done.$b$bRest assured knowing that you have done me a great service.$b$bHere, take this payment. It is a small token for your service.', 22522), -- A Desperate Journey
(40469, 7, 0, 0, 0, 0, 0, 0, 0, 'Is that mana wine? For me?$b$b<Iadreth hungrily gulps down the bottle.>$b$bYou and this Astoril have my thanks. I would surely have perished out here.', 22522), -- Final Preparations
(40401, 1, 0, 0, 0, 0, 0, 0, 0, 'I hope for both of our sakes that you were not followed. We must work quickly, considering how close I am.', 22522), -- A Way Back In
(43341, 0, 0, 0, 0, 0, 0, 0, 0, 'Excellent work, $N. We still have a long road ahead of us to rid this world of the legion, but this brings us one step closer.', 22522), -- Uniting the Isles
(41452, 11, 0, 0, 0, 200, 0, 0, 0, 'All is in its proper place. We are that much closer to being settled.', 22522), -- Feline Frantic
(43813, 0, 0, 0, 0, 0, 0, 0, 0, '<The beacon glows as you infuse it with Ancient Mana.>', 22522), -- Sanctum of Order Teleporter Online!
(40010, 0, 0, 0, 0, 0, 0, 0, 0, 'And just who might you be?', 22522), -- Tapping the Leylines
(41760, 0, 0, 0, 0, 0, 0, 0, 0, 'Fascinating.$B$BLet us hope the spell holds.', 22522), -- Kel'danath's Legacy
(41704, 0, 0, 0, 0, 0, 0, 0, 0, 'So, Kel''danath turned in the end...$B$BHe deserved better. Thank you for giving him peace.', 22522), -- Subject 16
(40796, 7, 0, 0, 0, 0, 0, 0, 0, '<Absolon immediately consumes the Ancient Mana and seems incapable of speech for a short while.>', 22522), -- Lingering on the Edge
(41702, 0, 0, 0, 0, 0, 0, 0, 0, '<A small tag hanging from his sleeve reads "16 - Theryn.">', 22522), -- Written in Stone
(40326, 0, 0, 0, 0, 0, 0, 0, 0, '<A bag containing countless pages of technical notes. Flipping through them, you find Kel''danath''s last journal entry.>', 22522), -- Scattered Memories
(41149, 0, 0, 0, 0, 0, 0, 0, 0, '<The ward snaps to life, giving you a slight pinch. Kel''danath must have put great care into developing a spell that would scare the withered away without harming them.>', 22522), -- A Re-Warding Effort
(40012, 0, 0, 0, 0, 0, 0, 0, 0, '<Somebody has been living here recently. This scroll bears strange lettering that glows warmly, beckoning your hand.>', 22522), -- An Old Ally
(40956, 0, 0, 0, 0, 0, 0, 0, 0, '<This seems like a good location for the teleporter.>', 22522), -- Survey Says...
(44691, 0, 0, 0, 0, 0, 0, 0, 0, 'Ah, I am feeling much better. Thank you!$B$BNow let us get to work.', 22522), -- Hungry Work
(44672, 0, 0, 0, 0, 0, 0, 0, 0, 'Keep a lookout for those crystal formations all over Suramar. We will need them.', 22522), -- Ancient Mana
(40830, 0, 0, 0, 0, 0, 0, 0, 0, 'Thank you for recovering Oculeth. He will be an invaluable asset to our cause.', 22522), -- Close Enough
(40748, 0, 0, 0, 0, 0, 0, 0, 0, 'Did we... make it?', 22522), -- Network Security
(40747, 0, 0, 0, 0, 0, 0, 0, 0, 'These are in rough shape, but they will suffice.', 22522), -- The Delicate Art of Telemancy
(40011, 0, 0, 0, 0, 0, 0, 0, 0, 'You may deliver me to Thalyssra once my work here is complete.', 22522), -- Oculeth's Workshop
(43476, 1, 1, 5, 0, 0, 0, 0, 0, 'Want to earn a few quick coins? I need a new subject to try one of my favorite potion recipes.$b$bI have to admit, the last few times I tried to make a potion like this, it didn''t work out so well for the test subjects. In fact, this is why I have to pay people to drink my potions now.$b$bPlease enjoy.', 22522), -- Experimental Potion: Test Subjects Needed
(42120, 603, 1, 0, 0, 0, 2000, 0, 0, 'I am heartened at your successful quest. We have been waiting for you to return.', 22522), -- The Silver Hand
(42377, 1, 0, 0, 0, 0, 0, 0, 0, 'A terrible loss. You have the Spark he held, though, and I have another.$b$bWe must press on. I will mourn Galford another day.', 22522), -- The Brother's Trail
(43349, 0, 0, 0, 0, 0, 0, 0, 0, 'The Aegis of Aggramar is secure within the well-defended city of Dalaran.$B$BYour quest to drive back the Legion is one step closer to fruition.', 22522), -- The Aegis of Aggramar
(40072, 0, 0, 0, 0, 0, 0, 0, 0, 'With the God-King''s defeat, the Aegis of Aggramar is yours to claim.', 22522), -- Securing the Aegis
(42483, 0, 0, 0, 0, 0, 0, 0, 0, '<A note from the goblins is left behind, along with an old magnifying glass>$B$BI wish I coulda been there to see the look on your face, but Raz and I got bigger fish to fry!$B$BThanks for the trophies and meat. I''m sure they''ll fetch a nice sum on the black market, along with all the goodies we got from those stupid tauren! I''m sure they won''t be needing them any more!$B$BSo long sucker!', 22522), -- Put It All on Red
(42454, 0, 0, 0, 0, 0, 0, 0, 0, '<Archmage Khadgar has set aside a space here for the Hammer of Khaz''goroth, as well as the rest of the Pillars of Creation.>', 22522), -- The Hammer of Khaz'goroth
(39781, 0, 0, 0, 0, 0, 0, 0, 0, 'It was a hard fight, but necessary. $p, on behalf of the Rivermane, the Skyhorn, the Stonedark, I thank you for your efforts.$b$bBut where one battle ends, another begins. When you stand against the coming of the Burning Legion, know that the tribes of Highmountain stand with you.', 22522), -- Neltharion's Lair
(44055, 0, 0, 0, 0, 0, 0, 0, 0, 'The other demons we will contend with in time. But that pit lord, it was a creature of great evil and beyond our devices to defeat on our own. Your skills were appreciated.', 22522), -- They Have A Pitlord
(44547, 0, 0, 0, 0, 0, 0, 0, 0, '<You are well-prepared to hunt down the next Pillar of Creation.>', 22522), -- Isle Hopping
(44464, 0, 0, 0, 0, 0, 0, 0, 0, 'An auspicious start to a life of tragedy. Perhaps one of the few joyous memories Illidan would have in his life. While the years that followed tested the prophesied child, they would never break him. You must remember that, despite what you may see as our journey continues.$B$BThere is much more to be done, but I am not yet ready. I will call for you when it is time.', 22522), -- Awakenings
(43486, 1, 0, 0, 0, 0, 0, 0, 0, 'Aponi sent you? How is she? It is hard to keep in touch with friends in these times.', 22522), -- Cracking the Codex
(43494, 1, 0, 0, 0, 0, 0, 0, 0, 'Wonderful! These knights will serve the order well.', 22522), -- Silver Hand Knights
(42852, 66, 0, 0, 0, 0, 0, 0, 0, 'For years I have heard of your deeds, Highlord. Now that I have fought at your side, I can say the stories were true.$B$BAllow me to represent the Order of the Silver Hand as a champion of the light, $n. I will fight justly and bravely, and carry out your orders to the end.', 22522), -- Champion: Justicar Julia Celeste
(42851, 66, 0, 0, 0, 0, 0, 0, 0, 'Highlord, I cannot thank you enough for both saving my life, and opening my eyes.$B$BIt is true I harbor great hatred against the Burning Legion, but you have reminded me that my choices define the path I take.$B$BI choose to live a path of righteousness, and I will follow your leadership and guidance, Highlord.', 22522), -- Champion: Vindicator Boros
(42890, 1, 0, 0, 0, 0, 0, 0, 0, 'Our mission to Faronaar was successful, no thanks to me.$B$BI let my anger get out of control, and I would have paid for it dearly were it not for you.$B$BThank you, Highlord.', 22522), -- The Codex of Command
(42888, 1, 0, 0, 0, 0, 0, 0, 0, 'No... we underestimated the enemy.', 22522), -- Communication Orbs
(43462, 1, 0, 0, 0, 0, 0, 0, 0, 'Good riddance.', 22522), -- Mother Ozram
(40653, 1, 0, 0, 0, 0, 0, 0, 0, 'Yes, I agree. That is a good place to focus our initial efforts.$b$bAs new threats arise, I will bring them to your attention. Elune guide your path, $ct.', 22522), -- Making Trails
(40652, 1, 0, 0, 0, 0, 0, 0, 0, 'Greetings, $ct.$b$bThe threats we face are numerous. We must act quickly.', 22522), -- Word on the Winds
(41332, 1, 0, 0, 0, 0, 0, 0, 0, 'Congratulations, $ct $n.$b$bWe will be counting on you in the days ahead.', 22522), -- Ascending The Circle
(40651, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent!$b$bAs your weapon grows in power you can return here and unleash its true potential.', 22522), -- The Seed of Ages
(41255, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. There''s no time to waste.', 22522), -- Sowing The Seed
(41689, 2, 0, 0, 0, 0, 0, 0, 0, 'We all owe you a great deal for your heroism, $n. I owe you a great deal.$B$BI suspect this war is far from over.', 22522), -- Cleansing the Mother Tree
(41690, 1, 0, 0, 0, 0, 0, 0, 0, 'I don''t know how much longer I would have lasted had you not arrived - thank you.', 22522), -- Reconvene
(41436, 0, 0, 0, 0, 0, 0, 0, 0, 'Delving into the Dream with such darkness encroaching is no simple task, one few druids dare attempt, but I''m afraid your journey is far from over.', 22522), -- In Deep Slumber
(41449, 1, 0, 0, 0, 0, 0, 0, 0, 'My preparations are complete. Are you prepared to enter the Dream?', 22522), -- Join the Dreamer
(41422, 0, 0, 0, 0, 0, 0, 0, 0, '<Mylune''s face brightens.>$B$BThen there is still hope!', 22522), -- Necessary Preparations
(40649, 0, 0, 0, 0, 0, 0, 0, 0, '<Mylune''s eyes are watery with dew, her usual smile an unsightly frown.>$B$BY-yes?', 22522), -- Meet with Mylune
(40646, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent choice, $n!$b$bWith the power of that weapon in your hands we will be able to bring balance back to Azeroth.', 22522), -- Weapons of Legend
(40645, 1, 0, 0, 0, 0, 0, 0, 0, 'You did well to navigate through the Emerald Dream to get to this place. Still, there are times when you might wish to return here a bit more quickly. This spell will guide you.$b$bTraveling the Dreamway is no small feat in itself, but it pales to the tasks ahead.', 22522), -- To The Dreamgrove
(40644, 1, 0, 0, 0, 0, 0, 0, 0, 'Well done, $n!$b$bNow that the path is open we can begin our journey to Val''sharah.', 22522), -- The Dreamway
(41106, 1, 0, 0, 0, 0, 0, 0, 0, 'Good, soon the ritual will be ready to begin.', 22522), -- Call of the Wilds
(40643, 1, 0, 0, 0, 0, 0, 0, 0, '$n, I''m pleased you came on such short notice.$b$bWe have a great undertaking here in the Moonglade that is about to begin, one in which you will play a key role.', 22522), -- A Summons From Moonglade
(42213, 0, 0, 0, 0, 0, 0, 0, 0, '<Archmage Khadgar has set aside a space here for the Tidestone of Golganneth, as well as the rest of the Pillars of Creation.>', 22522), -- The Tidestone of Golganneth
(38286, 0, 0, 0, 0, 0, 0, 0, 0, '<The Tidestone of Golganneth stands whole and unprotected.>', 22522), -- Wrath of Azshara
(44448, 6, 661, 1, 1, 0, 0, 0, 0, 'Dead? The titans are dead?$B$B<Khadgar ponders the news for a minute.>$B$BIt explains so much, yet I cannot help but feel an overwhelming sense of sadness. We have been on our own all along. Our gods killed before we were born.$B$BAs for the Army of the Light and Illidan, I am at a loss. I have never felt so powerless.$B$BIllidan is dead and the Golden Army fights an endless war in our name from across the cosmos.$B$BI need a moment, $n. This is too much bad news for an old man to bear.', 22522), -- In the House of Light and Shadow
(44337, 1, 0, 0, 0, 0, 0, 0, 0, 'If this works, then perhaps there is still hope for Turalyon and the Army of the Light.', 22522), -- Goddess Watch Over You
(42967, 1, 0, 0, 0, 0, 0, 0, 0, 'Well done. That should halt the Legion attacks for a while.', 22522), -- The Highlord's Command
(44250, 1, 0, 0, 0, 0, 0, 0, 0, 'We stand with you, $n.', 22522), -- Champion of the Light
(43883, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. I''ll get back to you with the results as soon as I can.', 22522), -- Hitting the Books
(39839, 603, 0, 0, 0, 0, 0, 0, 0, 'This powder... I think I know what this is. I had theorized its existence, but...$b$bHey, Vanessa! You''d better come take a look at this.', 22522), -- Mysterious Dust
(39838, 1, 1, 0, 0, 0, 0, 0, 0, 'That was... unexpected. Fel flames? I''d say this warrants further investigation.', 22522), -- Fire!
(42229, 1, 0, 0, 0, 0, 0, 0, 0, 'Such a temporary home will serve us well.', 22522), -- Shal'Aran
(40009, 0, 0, 0, 0, 0, 0, 0, 0, '<Thalyssra takes the crystals with a trembling hand.>', 22522), -- Arcane Thirst
(40123, 0, 0, 0, 0, 0, 0, 0, 0, 'We must find a way to reclaim Suramar City.', 22522), -- The Nightborne Pact
(40008, 0, 0, 0, 0, 0, 0, 0, 0, 'You fight well.$B$BPerhaps it is time for a proper introduction.', 22522); -- The Only Way Out is Through

DELETE FROM `quest_poi` WHERE (`QuestID`=42111 AND `BlobIndex`=0 AND `Idx1`=11) OR (`QuestID`=42111 AND `BlobIndex`=4 AND `Idx1`=10) OR (`QuestID`=42111 AND `BlobIndex`=3 AND `Idx1`=9) OR (`QuestID`=42111 AND `BlobIndex`=2 AND `Idx1`=8) OR (`QuestID`=42111 AND `BlobIndex`=1 AND `Idx1`=7) OR (`QuestID`=42111 AND `BlobIndex`=0 AND `Idx1`=6) OR (`QuestID`=42111 AND `BlobIndex`=0 AND `Idx1`=5) OR (`QuestID`=42111 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=42111 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=42111 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=42111 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=40470 AND `BlobIndex`=2 AND `Idx1`=8) OR (`QuestID`=40470 AND `BlobIndex`=1 AND `Idx1`=7) OR (`QuestID`=40470 AND `BlobIndex`=0 AND `Idx1`=6) OR (`QuestID`=42120 AND `BlobIndex`=0 AND `Idx1`=8) OR (`QuestID`=42120 AND `BlobIndex`=1 AND `Idx1`=5) OR (`QuestID`=42120 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=42120 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=39990 AND `BlobIndex`=1 AND `Idx1`=6) OR (`QuestID`=39990 AND `BlobIndex`=0 AND `Idx1`=5) OR (`QuestID`=39990 AND `BlobIndex`=2 AND `Idx1`=4) OR (`QuestID`=39990 AND `BlobIndex`=1 AND `Idx1`=3) OR (`QuestID`=39990 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=42888 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=41436 AND `BlobIndex`=1 AND `Idx1`=5);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(42111, 0, 11, 32, 0, 0, 1220, 1033, 0, 0, 2, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 4, 10, 4, 283697, 106374, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 3, 9, 4, 283697, 106374, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 2, 8, 4, 283697, 106374, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 1, 7, 4, 283697, 106374, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 0, 6, 4, 283697, 106374, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 0, 5, 3, 283676, 100274, 1220, 1033, 0, 0, 0, 0, 0, 1091992, 22522), -- Aggressive Reconnaisance
(42111, 4, 4, 2, 283675, 100273, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 3, 3, 2, 283675, 100273, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 2, 2, 2, 283675, 100273, 1220, 1033, 0, 0, 0, 0, 0, 0, 22522), -- Aggressive Reconnaisance
(42111, 1, 1, 2, 283675, 100273, 1220, 1015, 0, 0, 0, 0, 0, 1126435, 22522), -- Aggressive Reconnaisance
(40470, 2, 8, 32, 0, 0, 1220, 1033, 23, 0, 0, 0, 0, 1097371, 22522), -- Quality of Life
(40470, 1, 7, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1097187, 22522), -- Quality of Life
(40470, 0, 6, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1097371, 22522), -- Quality of Life
(42120, 0, 8, 3, 283930, 105892, 1220, 1014, 10, 0, 2, 0, 0, 0, 22522), -- The Silver Hand
(42120, 1, 5, 0, 283687, 102394, 1220, 1014, 12, 0, 2, 0, 38486, 0, 22522), -- The Silver Hand
(42120, 0, 4, 0, 283687, 102394, 1220, 1014, 10, 0, 2, 0, 38488, 0, 22522), -- The Silver Hand
(42120, 2, 2, -1, 0, 0, 1220, 1014, 10, 0, 0, 0, 44622, 1194329, 22522), -- The Silver Hand
(39990, 1, 6, 32, 0, 0, 1220, 1024, 0, 0, 0, 0, 0, 1079692, 22522), -- Huln's War - Reinforcements
(39990, 0, 5, 32, 0, 0, 1515, 1038, 0, 0, 0, 0, 0, 1066297, 22522), -- Huln's War - Reinforcements
(39990, 2, 4, 31, 0, 0, 1220, 1080, 0, 0, 0, 0, 0, 1079692, 22522), -- Huln's War - Reinforcements
(39990, 1, 3, 31, 0, 0, 1220, 1024, 6, 0, 0, 0, 0, 1079692, 22522), -- Huln's War - Reinforcements
(39990, 0, 2, 31, 0, 0, 1220, 1024, 0, 0, 0, 0, 0, 1079692, 22522), -- Huln's War - Reinforcements
(42888, 0, 4, 32, 0, 0, 1220, 1015, 0, 0, 0, 0, 0, 1200514, 22522), -- Communication Orbs
(41436, 1, 5, 32, 0, 0, 1220, 1018, 0, 0, 0, 0, 0, 1134796, 22522); -- In Deep Slumber

DELETE FROM `quest_poi_points` WHERE (`QuestID`=42111 AND `Idx1`=11 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=10 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=10 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=10 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=9 AND `Idx2`=5) OR (`QuestID`=42111 AND `Idx1`=9 AND `Idx2`=4) OR (`QuestID`=42111 AND `Idx1`=9 AND `Idx2`=3) OR (`QuestID`=42111 AND `Idx1`=9 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=9 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=9 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=8 AND `Idx2`=5) OR (`QuestID`=42111 AND `Idx1`=8 AND `Idx2`=4) OR (`QuestID`=42111 AND `Idx1`=8 AND `Idx2`=3) OR (`QuestID`=42111 AND `Idx1`=8 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=8 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=8 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=7 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=7 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=7 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=6 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=6 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=6 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=4 AND `Idx2`=4) OR (`QuestID`=42111 AND `Idx1`=4 AND `Idx2`=3) OR (`QuestID`=42111 AND `Idx1`=4 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=4 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=42111 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=42111 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=42111 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=42111 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=42111 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=42111 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=42111 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=42111 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=40470 AND `Idx1`=8 AND `Idx2`=0) OR (`QuestID`=40470 AND `Idx1`=5 AND `Idx2`=3) OR (`QuestID`=40470 AND `Idx1`=5 AND `Idx2`=2) OR (`QuestID`=40470 AND `Idx1`=5 AND `Idx2`=1) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=11) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=10) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=9) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=8) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=7) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=6) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=5) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=4) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=3) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=2) OR (`QuestID`=42120 AND `Idx1`=7 AND `Idx2`=1) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=39791 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=41436 AND `Idx1`=5 AND `Idx2`=0);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(42111, 11, 0, 814, 4956, 22522), -- Aggressive Reconnaisance
(42111, 10, 2, 1080, 4864, 22522), -- Aggressive Reconnaisance
(42111, 10, 1, 1084, 4863, 22522), -- Aggressive Reconnaisance
(42111, 10, 0, 1082, 4861, 22522), -- Aggressive Reconnaisance
(42111, 9, 5, 652, 4966, 22522), -- Aggressive Reconnaisance
(42111, 9, 4, 636, 5005, 22522), -- Aggressive Reconnaisance
(42111, 9, 3, 640, 5004, 22522), -- Aggressive Reconnaisance
(42111, 9, 2, 761, 4973, 22522), -- Aggressive Reconnaisance
(42111, 9, 1, 758, 4971, 22522), -- Aggressive Reconnaisance
(42111, 9, 0, 654, 4964, 22522), -- Aggressive Reconnaisance
(42111, 8, 5, 1165, 5207, 22522), -- Aggressive Reconnaisance
(42111, 8, 4, 1164, 5261, 22522), -- Aggressive Reconnaisance
(42111, 8, 3, 1252, 5256, 22522), -- Aggressive Reconnaisance
(42111, 8, 2, 1256, 5255, 22522), -- Aggressive Reconnaisance
(42111, 8, 1, 1253, 5253, 22522), -- Aggressive Reconnaisance
(42111, 8, 0, 1167, 5204, 22522), -- Aggressive Reconnaisance
(42111, 7, 2, 1216, 5048, 22522), -- Aggressive Reconnaisance
(42111, 7, 1, 1220, 5047, 22522), -- Aggressive Reconnaisance
(42111, 7, 0, 1218, 5045, 22522), -- Aggressive Reconnaisance
(42111, 6, 2, 851, 4671, 22522), -- Aggressive Reconnaisance
(42111, 6, 1, 856, 4670, 22522), -- Aggressive Reconnaisance
(42111, 6, 0, 853, 4668, 22522), -- Aggressive Reconnaisance
(42111, 5, 0, 664, 4821, 22522), -- Aggressive Reconnaisance
(42111, 4, 4, 369, 5007, 22522), -- Aggressive Reconnaisance
(42111, 4, 3, 288, 5077, 22522), -- Aggressive Reconnaisance
(42111, 4, 2, 243, 5141, 22522), -- Aggressive Reconnaisance
(42111, 4, 1, 443, 5090, 22522), -- Aggressive Reconnaisance
(42111, 4, 0, 384, 5006, 22522), -- Aggressive Reconnaisance
(42111, 3, 4, 403, 4851, 22522), -- Aggressive Reconnaisance
(42111, 3, 3, 470, 4820, 22522), -- Aggressive Reconnaisance
(42111, 3, 2, 517, 4729, 22522), -- Aggressive Reconnaisance
(42111, 3, 1, 515, 4709, 22522), -- Aggressive Reconnaisance
(42111, 3, 0, 494, 4682, 22522), -- Aggressive Reconnaisance
(42111, 2, 5, 830, 4699, 22522), -- Aggressive Reconnaisance
(42111, 2, 4, 997, 4969, 22522), -- Aggressive Reconnaisance
(42111, 2, 3, 1012, 4891, 22522), -- Aggressive Reconnaisance
(42111, 2, 2, 984, 4726, 22522), -- Aggressive Reconnaisance
(42111, 2, 1, 965, 4694, 22522), -- Aggressive Reconnaisance
(42111, 2, 0, 919, 4652, 22522), -- Aggressive Reconnaisance
(40470, 8, 0, 623, 4542, 22522), -- Quality of Life
(40470, 5, 3, 1709, 4628, 22522), -- Quality of Life
(40470, 5, 2, 1715, 4631, 22522), -- Quality of Life
(40470, 5, 1, 1734, 4599, 22522), -- Quality of Life
(42120, 7, 11, 2109, 2363, 22522), -- The Silver Hand
(42120, 7, 10, 2088, 2385, 22522), -- The Silver Hand
(42120, 7, 9, 2088, 2450, 22522), -- The Silver Hand
(42120, 7, 8, 2093, 2488, 22522), -- The Silver Hand
(42120, 7, 7, 2115, 2509, 22522), -- The Silver Hand
(42120, 7, 6, 2153, 2525, 22522), -- The Silver Hand
(42120, 7, 5, 2201, 2525, 22522), -- The Silver Hand
(42120, 7, 4, 2228, 2504, 22522), -- The Silver Hand
(42120, 7, 3, 2250, 2466, 22522), -- The Silver Hand
(42120, 7, 2, 2250, 2412, 22522), -- The Silver Hand
(42120, 7, 1, 2217, 2358, 22522), -- The Silver Hand
(39791, 1, 9, 3396, 1107, 22522), -- Lay Them to Rest
(39791, 1, 8, 3334, 1164, 22522), -- Lay Them to Rest
(39791, 1, 7, 3319, 1230, 22522), -- Lay Them to Rest
(39791, 1, 6, 3352, 1312, 22522), -- Lay Them to Rest
(39791, 1, 5, 3424, 1282, 22522), -- Lay Them to Rest
(39791, 1, 4, 3506, 1239, 22522), -- Lay Them to Rest
(39791, 1, 3, 3516, 1227, 22522), -- Lay Them to Rest
(39791, 1, 2, 3566, 1161, 22522), -- Lay Them to Rest
(39791, 1, 1, 3557, 1101, 22522), -- Lay Them to Rest
(41436, 5, 0, 4295, 7423, 22522); -- In Deep Slumber

DELETE FROM `quest_greeting` WHERE (`ID`=108434 AND `Type`=0) OR (`ID`=109434 AND `Type`=0) OR (`ID`=109384 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(108434, 0, 0, 0, 'Dargrul the Underking is using my people as fodder for his new toy and army. He thinks by defeating us it will prepare him for the coming of the Burning Legion.$b$bWhen I''m done with him, he will wish he had faced the Burning Legion instead.', 22522), -- 108434
(109434, 0, 0, 0, 'Every man and woman in our order would sacrifice themselves for the greater good, but we won''t get anywhere without a plan.', 22522), -- 109434
(109384, 0, 0, 0, 'Every man and woman in our order would sacrifice themselves for the greater good, but we won''t get anywhere without a plan.', 22522); -- 109384

DELETE FROM `quest_details` WHERE `ID` IN (41258 /*Fruit of the Doom*/, 41256 /*Blast of Spice Fish*/, 41107 /*Bad Apples*/, 41309 /*Mangelrath*/, 41214 /*Parts Unknown*/, 41222 /*Into The Pit!*/, 43594 /*Leyline Feed: Halls of the Eclipse*/, 41140 /*Search and Rescue!*/, 41139 /*The Key Is Around Here Somewhere...*/, 41575 /*Felsoul Teleporter Online!*/, 40470 /*Quality of Life*/, 40424 /*A Desperate Journey*/, 40469 /*Final Preparations*/, 40401 /*A Way Back In*/, 41453 /*Homeward Bounding*/, 41463 /*Missing Along the Way*/, 41452 /*Feline Frantic*/, 43813 /*Sanctum of Order Teleporter Online!*/, 41028 /*Power Grid*/, 41762 /*Sympathizers Among the Shal'dorei*/, 41760 /*Kel'danath's Legacy*/, 40796 /*Lingering on the Edge*/, 41704 /*Subject 16*/, 41702 /*Written in Stone*/, 41149 /*A Re-Warding Effort*/, 40326 /*Scattered Memories*/, 40010 /*Tapping the Leylines*/, 40956 /*Survey Says...*/, 44691 /*Hungry Work*/, 40830 /*Close Enough*/, 40748 /*Network Security*/, 40747 /*The Delicate Art of Telemancy*/, 44547 /*Isle Hopping*/, 42967 /*The Highlord's Command*/, 42120 /*The Silver Hand*/, 43349 /*The Aegis of Aggramar*/, 42454 /*The Hammer of Khaz'goroth*/, 44055 /*They Have A Pitlord*/, 43488 /*Blood of Our Enemy*/, 43487 /*The Fel Lexicon*/, 43494 /*Silver Hand Knights*/, 43486 /*Cracking the Codex*/, 42890 /*The Codex of Command*/, 43462 /*Mother Ozram*/, 42888 /*Communication Orbs*/, 40653 /*Making Trails*/, 40652 /*Word on the Winds*/, 41332 /*Ascending The Circle*/, 40651 /*The Seed of Ages*/, 41255 /*Sowing The Seed*/, 41689 /*Cleansing the Mother Tree*/, 41690 /*Reconvene*/, 41436 /*In Deep Slumber*/, 41449 /*Join the Dreamer*/, 41422 /*Necessary Preparations*/, 40649 /*Meet with Mylune*/, 40646 /*Weapons of Legend*/, 40645 /*To The Dreamgrove*/, 40644 /*The Dreamway*/, 41106 /*Call of the Wilds*/, 40643 /*A Summons From Moonglade*/, 42213 /*The Tidestone of Golganneth*/, 44464 /*Awakenings*/, 44448 /*In the House of Light and Shadow*/, 44250 /*Champion of the Light*/, 44337 /*Goddess Watch Over You*/, 43883 /*Hitting the Books*/, 43331 /*Time to Collect*/, 39863 /*Trial By Fel Fire*/, 39839 /*Mysterious Dust*/, 39838 /*Fire!*/, 40012 /*An Old Ally*/, 40011 /*Oculeth's Workshop*/, 44672 /*Ancient Mana*/, 42229 /*Shal'Aran*/, 40009 /*Arcane Thirst*/, 40123 /*The Nightborne Pact*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(41258, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Fruit of the Doom
(41256, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Blast of Spice Fish
(41107, 5, 0, 0, 0, 0, 0, 0, 0, 22522), -- Bad Apples
(41309, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Mangelrath
(41214, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Parts Unknown
(41222, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Into The Pit!
(43594, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Leyline Feed: Halls of the Eclipse
(41140, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Search and Rescue!
(41139, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- The Key Is Around Here Somewhere...
(41575, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Felsoul Teleporter Online!
(40470, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Quality of Life
(40424, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- A Desperate Journey
(40469, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Final Preparations
(40401, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- A Way Back In
(41453, 6, 25, 0, 0, 200, 1200, 0, 0, 22522), -- Homeward Bounding
(41463, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Missing Along the Way
(41452, 2, 0, 0, 0, 200, 0, 0, 0, 22522), -- Feline Frantic
(43813, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Sanctum of Order Teleporter Online!
(41028, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Power Grid
(41762, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Sympathizers Among the Shal'dorei
(41760, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Kel'danath's Legacy
(40796, 1, 18, 0, 0, 0, 1500, 0, 0, 22522), -- Lingering on the Edge
(41704, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Subject 16
(41702, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Written in Stone
(41149, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- A Re-Warding Effort
(40326, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Scattered Memories
(40010, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Tapping the Leylines
(40956, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Survey Says...
(44691, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Hungry Work
(40830, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Close Enough
(40748, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Network Security
(40747, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- The Delicate Art of Telemancy
(44547, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Isle Hopping
(42967, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- The Highlord's Command
(42120, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- The Silver Hand
(43349, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- The Aegis of Aggramar
(42454, 5, 1, 0, 0, 0, 0, 0, 0, 22522), -- The Hammer of Khaz'goroth
(44055, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- They Have A Pitlord
(43488, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Blood of Our Enemy
(43487, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- The Fel Lexicon
(43494, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Silver Hand Knights
(43486, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Cracking the Codex
(42890, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- The Codex of Command
(43462, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Mother Ozram
(42888, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Communication Orbs
(40653, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Making Trails
(40652, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Word on the Winds
(41332, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Ascending The Circle
(40651, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- The Seed of Ages
(41255, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Sowing The Seed
(41689, 1, 1, 1, 0, 0, 0, 0, 0, 22522), -- Cleansing the Mother Tree
(41690, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Reconvene
(41436, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- In Deep Slumber
(41449, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Join the Dreamer
(41422, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Necessary Preparations
(40649, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Meet with Mylune
(40646, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Weapons of Legend
(40645, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- To The Dreamgrove
(40644, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- The Dreamway
(41106, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Call of the Wilds
(40643, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- A Summons From Moonglade
(42213, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- The Tidestone of Golganneth
(44464, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Awakenings
(44448, 1, 273, 0, 0, 0, 0, 0, 0, 22522), -- In the House of Light and Shadow
(44250, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Champion of the Light
(44337, 1, 1, 661, 0, 0, 0, 0, 0, 22522), -- Goddess Watch Over You
(43883, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Hitting the Books
(43331, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Time to Collect
(39863, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Trial By Fel Fire
(39839, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Mysterious Dust
(39838, 1, 1, 0, 0, 0, 0, 0, 0, 22522), -- Fire!
(40012, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- An Old Ally
(40011, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Oculeth's Workshop
(44672, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Ancient Mana
(42229, 1, 0, 0, 0, 0, 0, 0, 0, 22522), -- Shal'Aran
(40009, 0, 0, 0, 0, 0, 0, 0, 0, 22522), -- Arcane Thirst
(40123, 0, 0, 0, 0, 0, 0, 0, 0, 22522); -- The Nightborne Pact

DELETE FROM `quest_request_items` WHERE `ID` IN (41256 /*Blast of Spice Fish*/, 41107 /*Bad Apples*/, 42722 /*Friends in Cages*/, 40745 /*Shift Change*/, 41877 /*Lady Lunastre*/, 41214 /*Parts Unknown*/, 41140 /*Search and Rescue!*/, 43594 /*Leyline Feed: Halls of the Eclipse*/, 41139 /*The Key Is Around Here Somewhere...*/, 41575 /*Felsoul Teleporter Online!*/, 40424 /*A Desperate Journey*/, 40469 /*Final Preparations*/, 44561 /*Seed of Hope*/, 43813 /*Sanctum of Order Teleporter Online!*/, 40796 /*Lingering on the Edge*/, 41702 /*Written in Stone*/, 40747 /*The Delicate Art of Telemancy*/, 42967 /*The Highlord's Command*/, 43702 /*Softening the Target*/, 44153 /*Light's Charge*/, 42773 /*The Light Reveals*/, 42483 /*Put It All on Red*/, 42639 /*A Stone of Blood*/, 42635 /*The Mystery of Dreyrgrot*/, 42640 /*The Value of Knowledge*/, 39405 /*Stories of Battle*/, 40002 /*A Familiar Fate*/, 38347 /*Stealth by Seaweed*/, 42847 /*Dark Storms*/, 42447 /*Dances With Ravenbears*/, 44500 /*Author! Author!*/, 40755 /*Hiding in the Stacks*/, 39781 /*Neltharion's Lair*/, 42590 /*Moozy's Reunion*/, 42890 /*The Codex of Command*/, 41422 /*Necessary Preparations*/, 43341 /*Uniting the Isles*/, 40009 /*Arcane Thirst*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(41256, 603, 0, 0, 0, 'Daglir is young and foolish!', 22522), -- Blast of Spice Fish
(41107, 1, 0, 0, 0, 'I would avoid eating any food until you''ve washed your hands.', 22522), -- Bad Apples
(42722, 0, 0, 0, 0, 'Did you let them out?', 22522), -- Friends in Cages
(40745, 0, 0, 0, 0, 'W-who are you?', 22522), -- Shift Change
(41877, 0, 0, 0, 0, 'Hm?', 22522), -- Lady Lunastre
(41214, 0, 0, 0, 0, 'Aye, is that all of him? We aren''t missing any parts are we?', 22522), -- Parts Unknown
(41140, 0, 0, 0, 0, 'Have you found the key yet?', 22522), -- Search and Rescue!
(43594, 0, 0, 0, 0, '<The leyline feed pulses softly.>', 22522), -- Leyline Feed: Halls of the Eclipse
(41139, 0, 0, 0, 0, 'Did ye find the key?', 22522), -- The Key Is Around Here Somewhere...
(41575, 0, 0, 0, 0, '<The beacon is cold and dark.>', 22522), -- Felsoul Teleporter Online!
(40424, 1, 0, 0, 0, 'I was worried something had happened to you in the forest. I am glad that I was wrong.', 22522), -- A Desperate Journey
(40469, 6, 0, 0, 0, 'Did you find the smuggler? What did they say?', 22522), -- Final Preparations
(44561, 0, 0, 0, 0, 'The arcan''dor needs time.', 22522), -- Seed of Hope
(43813, 0, 0, 0, 0, '<The beacon is cold and dark.>', 22522), -- Sanctum of Order Teleporter Online!
(40796, 0, 0, 0, 0, 'Can you help me? Is there nothing you can do?', 22522), -- Lingering on the Edge
(41702, 0, 0, 0, 0, '<This withered seems entranced by the spellstone. He is completely pacified.>', 22522), -- Written in Stone
(40747, 0, 0, 0, 0, 'Salvaging the equipment here is necessary if I am to develop a teleporter network for Thalyssra.', 22522), -- The Delicate Art of Telemancy
(42967, 1, 0, 0, 0, 'How did it go?', 22522), -- The Highlord's Command
(43702, 0, 0, 0, 0, 'Have you managed to thin their numbers?', 22522), -- Softening the Target
(44153, 0, 0, 0, 0, 'This is where you must place Light''s Heart.', 22522), -- Light's Charge
(42773, 0, 0, 0, 0, 'We must do whatever it takes.', 22522), -- The Light Reveals
(42483, 0, 0, 0, 0, 'Hmm...', 22522), -- Put It All on Red
(42639, 0, 0, 0, 0, 'We will need the amulet''s protection to proceed.', 22522), -- A Stone of Blood
(42635, 0, 0, 0, 0, 'How goes the hunt for knowledge?', 22522), -- The Mystery of Dreyrgrot
(42640, 0, 0, 0, 0, 'What''s this?', 22522), -- The Value of Knowledge
(39405, 0, 0, 0, 0, 'This appears to be the last verse.', 22522), -- Stories of Battle
(40002, 0, 0, 0, 0, 'The jailer has... the key...', 22522), -- A Familiar Fate
(38347, 0, 0, 0, 0, 'Gods, what is that stench?! $B$BThis disguise had better work!', 22522), -- Stealth by Seaweed
(42847, 1, 0, 0, 0, 'How is the mission proceeding?', 22522), -- Dark Storms
(42447, 0, 0, 0, 0, '', 22522), -- Dances With Ravenbears
(44500, 1, 0, 0, 0, 'Alodi''s knowledge of the Pillars of Creation is crucial to our campaign against the Legion.', 22522), -- Author! Author!
(40755, 6, 0, 0, 0, 'Do you have something for me?', 22522), -- Hiding in the Stacks
(39781, 0, 0, 0, 0, 'The war continues.', 22522), -- Neltharion's Lair
(42590, 0, 0, 0, 0, 'Thank you for your kindness. Truly.', 22522), -- Moozy's Reunion
(42890, 1, 0, 0, 0, 'Highlord, you have returned.', 22522), -- The Codex of Command
(41422, 0, 0, 0, 0, 'Are you ready?', 22522), -- Necessary Preparations
(43341, 0, 0, 0, 0, 'Any luck uniting the factions?', 22522), -- Uniting the Isles
(40009, 0, 0, 0, 0, '<Thalyssra''s eyes have dulled and her body is shaking uncontrollably.>', 22522); -- Arcane Thirst

UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=70392;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5489203, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=69518;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5745, `CombatReach`=2.25, `VerifiedBuild`=22522 WHERE `DisplayID`=68364;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=65118;
UPDATE `creature_model_info` SET `BoundingRadius`=1.222224, `CombatReach`=9, `VerifiedBuild`=22522 WHERE `DisplayID`=27992;
UPDATE `creature_model_info` SET `BoundingRadius`=0.766, `CombatReach`=3, `VerifiedBuild`=22522 WHERE `DisplayID`=55814;
UPDATE `creature_model_info` SET `BoundingRadius`=0.612, `CombatReach`=3, `VerifiedBuild`=22522 WHERE `DisplayID`=58655;
UPDATE `creature_model_info` SET `BoundingRadius`=0.389, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=69476;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5, `CombatReach`=1, `VerifiedBuild`=22522 WHERE `DisplayID`=68677;
UPDATE `creature_model_info` SET `BoundingRadius`=1.065357, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=69259;
UPDATE `creature_model_info` SET `BoundingRadius`=0.8829392, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=69157;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=68674;
UPDATE `creature_model_info` SET `BoundingRadius`=1.2, `CombatReach`=1.2, `VerifiedBuild`=22522 WHERE `DisplayID`=68648;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=2.25, `VerifiedBuild`=22522 WHERE `DisplayID`=68815;
UPDATE `creature_model_info` SET `BoundingRadius`=0.5205, `CombatReach`=3, `VerifiedBuild`=22522 WHERE `DisplayID`=65700;
UPDATE `creature_model_info` SET `BoundingRadius`=0.34, `CombatReach`=1, `VerifiedBuild`=22522 WHERE `DisplayID`=33659;
UPDATE `creature_model_info` SET `BoundingRadius`=1.345, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=69105;
UPDATE `creature_model_info` SET `BoundingRadius`=1.324209, `CombatReach`=0.625, `VerifiedBuild`=22522 WHERE `DisplayID`=64509;
UPDATE `creature_model_info` SET `BoundingRadius`=2.648418, `CombatReach`=1.25, `VerifiedBuild`=22522 WHERE `DisplayID`=63406;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3875, `CombatReach`=1.25, `VerifiedBuild`=22522 WHERE `DisplayID`=68624;
UPDATE `creature_model_info` SET `BoundingRadius`=0.34, `CombatReach`=1, `VerifiedBuild`=22522 WHERE `DisplayID`=33657;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=68114;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60502;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=59186;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=57284;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=58283;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=57180;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=59266;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=57432;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60489;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60014;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=59263;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60537;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60494;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60505;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=58363;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60540;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=60530;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=57160;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=57418;
UPDATE `creature_model_info` SET `BoundingRadius`=0.383, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=63305;
UPDATE `creature_model_info` SET `BoundingRadius`=0.465, `CombatReach`=3, `VerifiedBuild`=22522 WHERE `DisplayID`=63567;
UPDATE `creature_model_info` SET `BoundingRadius`=1.5, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=46569;
UPDATE `creature_model_info` SET `CombatReach`=4, `VerifiedBuild`=22522 WHERE `DisplayID`=28823;
UPDATE `creature_model_info` SET `BoundingRadius`=0.39, `CombatReach`=1, `VerifiedBuild`=22522 WHERE `DisplayID`=48529;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=22522 WHERE `DisplayID`=62770;
UPDATE `creature_model_info` SET `BoundingRadius`=0.9747, `CombatReach`=4.05, `VerifiedBuild`=22522 WHERE `DisplayID`=62768;
UPDATE `creature_model_info` SET `BoundingRadius`=10.07533, `CombatReach`=9, `VerifiedBuild`=22522 WHERE `DisplayID`=68981;
UPDATE `creature_model_info` SET `BoundingRadius`=0.306, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=73273;
UPDATE `creature_model_info` SET `BoundingRadius`=0.208, `CombatReach`=1.5, `VerifiedBuild`=22522 WHERE `DisplayID`=73177;

DELETE FROM `npc_vendor` WHERE (`entry`=103131 AND `item`=140631 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=103131 AND `item`=140300 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=103131 AND `item`=140298 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=97140 AND `item`=139695 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=97140 AND `item`=139692 AND `ExtendedCost`=6125 AND `type`=1) OR (`entry`=107467 AND `item`=137178 AND `ExtendedCost`=5958 AND `type`=1) OR (`entry`=107467 AND `item`=137467 AND `ExtendedCost`=5959 AND `type`=1) OR (`entry`=107467 AND `item`=137401 AND `ExtendedCost`=5958 AND `type`=1) OR (`entry`=109562 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=38426 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=109562 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114530 AND `item`=141215 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=114530 AND `item`=140298 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128851 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128849 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128848 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128839 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128840 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=138977 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128835 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128836 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=138292 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=96790 AND `item`=128853 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=107675 AND `item`=140207 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=107675 AND `item`=140206 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=107675 AND `item`=140205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=107675 AND `item`=140204 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=107675 AND `item`=140202 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=107675 AND `item`=140201 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=107675 AND `item`=140203 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(103131, 3, 140631, 0, 0, 1, 0, 0, 22522), -- Nightpear
(103131, 2, 140300, 0, 0, 1, 0, 0, 22522), -- Fresh Arcfruit
(103131, 1, 140298, 0, 0, 1, 0, 0, 22522), -- Mananelle's Sparkling Cider
(97140, 32, 139695, 0, 6125, 1, 0, 0, 22522), -- Spaulders of the Silver Hand
(97140, 16, 139692, 0, 6125, 1, 0, 0, 22522), -- Gauntlets of the Silver Hand
(107467, 3, 137178, 0, 5958, 1, 0, 0, 22522), -- -Unknown-
(107467, 2, 137467, 0, 5959, 1, 0, 0, 22522), -- -Unknown-
(107467, 1, 137401, 0, 5958, 1, 0, 0, 22522), -- -Unknown-
(109562, 29, 39505, 0, 0, 1, 0, 0, 22522), -- Virtuoso Inking Set
(109562, 28, 20815, 0, 0, 1, 0, 0, 22522), -- Jeweler's Kit
(109562, 27, 6532, 0, 0, 1, 0, 0, 22522), -- Bright Baubles
(109562, 26, 6530, 0, 0, 1, 0, 0, 22522), -- Nightcrawlers
(109562, 25, 4400, 0, 0, 1, 0, 0, 22522), -- Heavy Stock
(109562, 24, 4399, 0, 0, 1, 0, 0, 22522), -- Wooden Stock
(109562, 23, 4289, 0, 0, 1, 0, 0, 22522), -- Salt
(109562, 22, 3371, 0, 0, 1, 0, 0, 22522), -- Crystal Vial
(109562, 21, 4340, 0, 0, 1, 0, 0, 22522), -- Gray Dye
(109562, 20, 4342, 0, 0, 1, 0, 0, 22522), -- Purple Dye
(109562, 19, 4341, 0, 0, 1, 0, 0, 22522), -- Yellow Dye
(109562, 18, 2325, 0, 0, 1, 0, 0, 22522), -- Black Dye
(109562, 17, 2604, 0, 0, 1, 0, 0, 22522), -- Red Dye
(109562, 16, 3857, 0, 0, 1, 0, 0, 22522), -- Coal
(109562, 15, 3466, 0, 0, 1, 0, 0, 22522), -- Strong Flux
(109562, 14, 2880, 0, 0, 1, 0, 0, 22522), -- Weak Flux
(109562, 13, 2678, 0, 0, 1, 0, 0, 22522), -- Mild Spices
(109562, 12, 38426, 0, 0, 1, 0, 0, 22522), -- Eternium Thread
(109562, 11, 14341, 0, 0, 1, 0, 0, 22522), -- Rune Thread
(109562, 10, 8343, 0, 0, 1, 0, 0, 22522), -- Heavy Silken Thread
(109562, 9, 4291, 0, 0, 1, 0, 0, 22522), -- Silken Thread
(109562, 8, 2321, 0, 0, 1, 0, 0, 22522), -- Fine Thread
(109562, 7, 2320, 0, 0, 1, 0, 0, 22522), -- Coarse Thread
(109562, 6, 5956, 0, 0, 1, 0, 0, 22522), -- Blacksmith Hammer
(109562, 5, 6217, 0, 0, 1, 0, 0, 22522), -- Copper Rod
(109562, 4, 6256, 0, 0, 1, 0, 0, 22522), -- Fishing Pole
(109562, 3, 85663, 0, 0, 1, 0, 0, 22522), -- Herbalist's Spade
(109562, 2, 7005, 0, 0, 1, 0, 0, 22522), -- Skinning Knife
(109562, 1, 2901, 0, 0, 1, 0, 0, 22522), -- Mining Pick
(114530, 2, 141215, 0, 0, 1, 0, 0, 22522), -- Arcberry Juice
(114530, 1, 140298, 0, 0, 1, 0, 0, 22522), -- Mananelle's Sparkling Cider
(96790, 10, 128851, 0, 0, 1, 0, 0, 22522), -- Roasted Juicycrunch Carrots
(96790, 9, 128849, 0, 0, 1, 0, 0, 22522), -- Scallion Kimchi
(96790, 8, 128848, 0, 0, 1, 0, 0, 22522), -- Roasted Maize
(96790, 7, 128839, 0, 0, 1, 0, 0, 22522), -- Smoked Elderhorn
(96790, 6, 128840, 0, 0, 1, 0, 0, 22522), -- Honey-Glazed Ham
(96790, 5, 138977, 0, 0, 1, 0, 0, 22522), -- Thundertotem Rice Cake
(96790, 4, 128835, 0, 0, 1, 0, 0, 22522), -- Highmountain Fry Bread
(96790, 3, 128836, 0, 0, 1, 0, 0, 22522), -- Barley Bread
(96790, 2, 138292, 0, 0, 1, 0, 0, 22522), -- Ley-Enriched Water
(96790, 1, 128853, 0, 0, 1, 0, 0, 22522), -- Highmountain Spring Water
(107675, 7, 140207, 0, 0, 1, 0, 0, 22522), -- 'Free-Range' Honey-Glazed Ham
(107675, 6, 140206, 0, 0, 1, 0, 0, 22522), -- Grilled 'Wild' Mini Rays
(107675, 5, 140205, 0, 0, 1, 0, 0, 22522), -- 'Fresh' Moist Azsunian Feta
(107675, 4, 140204, 0, 0, 1, 0, 0, 22522), -- 'Bottled' Ley-Enriched Water
(107675, 3, 140202, 0, 0, 1, 0, 0, 22522), -- Smoked 'Grass Fed' Elderhorn
(107675, 2, 140201, 0, 0, 1, 0, 0, 22522), -- 'Organic' Azsunian Grapes
(107675, 1, 140203, 0, 0, 1, 0, 0, 22522); -- 'Natural' Highmountain Spring Water

UPDATE `npc_vendor` SET `slot`=40 WHERE (`entry`=90866 AND `item`=23805 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Ultra-Spectropic Detection Goggles
UPDATE `npc_vendor` SET `slot`=39 WHERE (`entry`=90866 AND `item`=23816 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Fel Iron Toolbox
UPDATE `npc_vendor` SET `slot`=38 WHERE (`entry`=90866 AND `item`=23803 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Cogspinner Goggles
UPDATE `npc_vendor` SET `slot`=37 WHERE (`entry`=90866 AND `item`=23807 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Adamantite Scope
UPDATE `npc_vendor` SET `slot`=36 WHERE (`entry`=90866 AND `item`=23799 AND `ExtendedCost`=0 AND `type`=1); -- Schematic: Adamantite Rifle
UPDATE `npc_vendor` SET `PlayerConditionID`=0 WHERE (`entry`=100196 AND `item`=139693 AND `ExtendedCost`=6125 AND `type`=1); -- Crown of the Silver Hand
UPDATE `npc_vendor` SET `PlayerConditionID`=0 WHERE (`entry`=100196 AND `item`=139693 AND `ExtendedCost`=6125 AND `type`=1); -- Crown of the Silver Hand
UPDATE `npc_vendor` SET `PlayerConditionID`=0 WHERE (`entry`=100196 AND `item`=139693 AND `ExtendedCost`=6125 AND `type`=1); -- Crown of the Silver Hand

DELETE FROM `creature_equip_template` WHERE (`CreatureID`=105044 AND `ID`=1) OR (`CreatureID`=112756 AND `ID`=1) OR (`CreatureID`=106418 AND `ID`=1) OR (`CreatureID`=104521 AND `ID`=1) OR (`CreatureID`=104522 AND `ID`=1) OR (`CreatureID`=104519 AND `ID`=1) OR (`CreatureID`=101116 AND `ID`=1) OR (`CreatureID`=112016 AND `ID`=1) OR (`CreatureID`=97390 AND `ID`=1) OR (`CreatureID`=108628 AND `ID`=2) OR (`CreatureID`=99482 AND `ID`=1) OR (`CreatureID`=98312 AND `ID`=1) OR (`CreatureID`=111199 AND `ID`=1) OR (`CreatureID`=111256 AND `ID`=1) OR (`CreatureID`=93686 AND `ID`=1) OR (`CreatureID`=93684 AND `ID`=1) OR (`CreatureID`=94167 AND `ID`=1) OR (`CreatureID`=94166 AND `ID`=1) OR (`CreatureID`=91194 AND `ID`=1) OR (`CreatureID`=104619 AND `ID`=1) OR (`CreatureID`=104657 AND `ID`=1) OR (`CreatureID`=104658 AND `ID`=1) OR (`CreatureID`=104659 AND `ID`=1) OR (`CreatureID`=105238 AND `ID`=1) OR (`CreatureID`=104620 AND `ID`=1) OR (`CreatureID`=105237 AND `ID`=1) OR (`CreatureID`=105239 AND `ID`=1) OR (`CreatureID`=104639 AND `ID`=1) OR (`CreatureID`=104398 AND `ID`=1) OR (`CreatureID`=103420 AND `ID`=1) OR (`CreatureID`=114246 AND `ID`=1) OR (`CreatureID`=112958 AND `ID`=1) OR (`CreatureID`=90959 AND `ID`=1) OR (`CreatureID`=90351 AND `ID`=2) OR (`CreatureID`=45254 AND `ID`=7) OR (`CreatureID`=45254 AND `ID`=6) OR (`CreatureID`=45254 AND `ID`=5) OR (`CreatureID`=45255 AND `ID`=5) OR (`CreatureID`=45255 AND `ID`=4) OR (`CreatureID`=45254 AND `ID`=4) OR (`CreatureID`=45255 AND `ID`=3) OR (`CreatureID`=45255 AND `ID`=2) OR (`CreatureID`=45254 AND `ID`=3) OR (`CreatureID`=45255 AND `ID`=1) OR (`CreatureID`=45254 AND `ID`=2) OR (`CreatureID`=45254 AND `ID`=1) OR (`CreatureID`=92052 AND `ID`=1) OR (`CreatureID`=114444 AND `ID`=1) OR (`CreatureID`=114753 AND `ID`=1) OR (`CreatureID`=114754 AND `ID`=1) OR (`CreatureID`=97525 AND `ID`=1);
INSERT INTO `creature_equip_template` (`CreatureID`, `ID`, `ItemID1`, `ItemID2`, `ItemID3`) VALUES
(105044, 1, 28736, 0, 0), -- Lidia Sunglow
(112756, 1, 139007, 0, 0), -- Sorallus
(106418, 1, 132170, 0, 0), -- Anarys Lunastre
(104521, 1, 133175, 0, 0), -- Alteria
(104522, 1, 133176, 0, 133176), -- Selenyi
(104519, 1, 132170, 0, 132170), -- Colerian
(101116, 1, 132171, 0, 0), -- Warpcaster Thwen
(112016, 1, 133177, 0, 137253), -- Corvelyn
(97390, 1, 104400, 0, 104400), -- Thieving Scoundrel
(108628, 2, 127651, 0, 0), -- Armond Thaco
(99482, 1, 132171, 0, 0), -- First Arcanist Thalyssra
(98312, 1, 132171, 0, 0), -- First Arcanist Thalyssra
(111199, 1, 104045, 0, 0), -- Elliott Van Rook
(111256, 1, 89776, 0, 0), -- Chani Malflame
(93686, 1, 132258, 0, 0), -- Jinikki the Puncturer
(93684, 1, 55146, 0, 0), -- Malicious Sprite
(94167, 1, 88553, 0, 0), -- Bitterbrine Raider
(94166, 1, 88553, 0, 0), -- Bitterbrine Raider
(91194, 1, 88553, 0, 0), -- Bitterbrine Raider
(104619, 1, 127648, 0, 0), -- Destromath
(104657, 1, 50695, 0, 0), -- Celestine of the Harvest
(104658, 1, 57020, 0, 0), -- Zen'tabra
(104659, 1, 63052, 0, 0), -- Archdruid Hamuul Runetotem
(105238, 1, 63052, 0, 0), -- Archdruid Hamuul Runetotem
(104620, 1, 13339, 0, 0), -- Skylord Omnuron
(105237, 1, 50695, 0, 0), -- Celestine of the Harvest
(105239, 1, 57020, 0, 0), -- Zen'tabra
(104639, 1, 119458, 0, 0), -- Felguard Invader
(104398, 1, 12322, 0, 0), -- Bashana Runetotem
(103420, 1, 63052, 0, 0), -- Archdruid Hamuul Runetotem
(114246, 1, 768, 0, 0), -- Karl Wogksch
(112958, 1, 2520, 0, 49933), -- Soulare of Andorhal
(90959, 1, 1899, 0, 117413), -- Stormwind Footman
(90351, 2, 132870, 0, 0), -- Silver Hand Knight
(45254, 7, 3346, 0, 0), -- Hillsbrad Worgen
(45254, 6, 3367, 0, 0), -- Hillsbrad Worgen
(45254, 5, 2715, 0, 0), -- Hillsbrad Worgen
(45255, 5, 2714, 0, 0), -- Hillsbrad Worgen
(45255, 4, 2715, 0, 0), -- Hillsbrad Worgen
(45254, 4, 1906, 0, 0), -- Hillsbrad Worgen
(45255, 3, 25587, 0, 13604), -- Hillsbrad Worgen
(45255, 2, 3367, 0, 0), -- Hillsbrad Worgen
(45254, 3, 3351, 0, 2197), -- Hillsbrad Worgen
(45255, 1, 2704, 0, 0), -- Hillsbrad Worgen
(45254, 2, 2714, 0, 0), -- Hillsbrad Worgen
(45254, 1, 25587, 0, 13604), -- Hillsbrad Worgen
(92052, 1, 108731, 0, 38635), -- Sunwalker Keeper
(114444, 1, 118993, 0, 113666), -- Dread Bulwark
(114753, 1, 28365, 0, 0), -- Gorgoloth
(114754, 1, 28365, 0, 0), -- Mazgoroth
(97525, 1, 12754, 0, 0); -- Thunder Bluff Brave

UPDATE `creature_equip_template` SET `ItemID1`=2703 WHERE (`CreatureID`=96793 AND `ID`=2); -- Stefen Cotter
UPDATE `creature_equip_template` SET `ItemID1`=114979 WHERE (`CreatureID`=99386 AND `ID`=5); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=114978 WHERE (`CreatureID`=99386 AND `ID`=4); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=118568 WHERE (`CreatureID`=99386 AND `ID`=3); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=118563 WHERE (`CreatureID`=99386 AND `ID`=2); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=114980 WHERE (`CreatureID`=99386 AND `ID`=1); -- Rivermane Tauren
UPDATE `creature_equip_template` SET `ItemID1`=18610 WHERE (`CreatureID`=92617 AND `ID`=3); -- Bradensbrook Villager
UPDATE `creature_equip_template` SET `ItemID1`=85663 WHERE (`CreatureID`=92617 AND `ID`=2); -- Bradensbrook Villager
UPDATE `creature_equip_template` SET `ItemID1`=3367 WHERE (`CreatureID`=92617 AND `ID`=1); -- Bradensbrook Villager
UPDATE `creature_equip_template` SET `ItemID1`=24015, `ItemID3`=0 WHERE (`CreatureID`=109774 AND `ID`=5); -- Exodar Citizen
UPDATE `creature_equip_template` SET `ItemID1`=24017 WHERE (`CreatureID`=109774 AND `ID`=3); -- Exodar Citizen
UPDATE `creature_equip_template` SET `ItemID1`=138756, `ItemID3`=0 WHERE (`CreatureID`=109112 AND `ID`=3); -- Rakeeshi Honor Guard
UPDATE `creature_equip_template` SET `ItemID1`=23906, `ItemID3`=23907 WHERE (`CreatureID`=109774 AND `ID`=2); -- Exodar Citizen
UPDATE `creature_equip_template` SET `ItemID1`=138753, `ItemID3`=138753 WHERE (`CreatureID`=109112 AND `ID`=2); -- Rakeeshi Honor Guard
UPDATE `creature_equip_template` SET `ItemID1`=138758, `ItemID3`=0 WHERE (`CreatureID`=109112 AND `ID`=1); -- Rakeeshi Honor Guard
UPDATE `creature_equip_template` SET `ItemID1`=132870 WHERE (`CreatureID`=90348 AND `ID`=1); -- Silver Hand Knight
UPDATE `creature_equip_template` SET `ItemID3`=40596 WHERE (`CreatureID`=89023 AND `ID`=1); -- Nightwatcher Idri
UPDATE `creature_equip_template` SET `ItemID1`=33598 WHERE (`CreatureID`=89110 AND `ID`=2); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=33598 WHERE (`CreatureID`=89112 AND `ID`=3); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=94096 WHERE (`CreatureID`=89112 AND `ID`=2); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=12993 WHERE (`CreatureID`=89111 AND `ID`=2); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID1`=2703 WHERE (`CreatureID`=93466 AND `ID`=1); -- Seska Seafang
UPDATE `creature_equip_template` SET `ItemID1`=1910 WHERE (`CreatureID`=89112 AND `ID`=1); -- Shipwrecked Captive
UPDATE `creature_equip_template` SET `ItemID2`=0 WHERE (`CreatureID`=1472 AND `ID`=1); -- Morgg Stormshot
UPDATE `creature_equip_template` SET `ItemID1`=1911 WHERE (`CreatureID`=29016 AND `ID`=3); -- Steam Tank Engineer
UPDATE `creature_equip_template` SET `ItemID1`=1903 WHERE (`CreatureID`=29016 AND `ID`=2); -- Steam Tank Engineer
UPDATE `creature_equip_template` SET `ItemID1`=31824 WHERE (`CreatureID`=29016 AND `ID`=1); -- Steam Tank Engineer
UPDATE `creature_equip_template` SET `ItemID1`=118560 WHERE (`CreatureID`=108847 AND `ID`=4); -- Disturbed Resident
UPDATE `creature_equip_template` SET `ItemID1`=118559 WHERE (`CreatureID`=108847 AND `ID`=3); -- Disturbed Resident
UPDATE `creature_equip_template` SET `ItemID1`=118560 WHERE (`CreatureID`=108824 AND `ID`=3); -- Disturbed Resident
UPDATE `creature_equip_template` SET `ItemID1`=1902 WHERE (`CreatureID`=108847 AND `ID`=2); -- Disturbed Resident
UPDATE `creature_equip_template` SET `ItemID1`=118562 WHERE (`CreatureID`=108847 AND `ID`=1); -- Disturbed Resident
UPDATE `creature_equip_template` SET `ItemID1`=118562 WHERE (`CreatureID`=108824 AND `ID`=2); -- Disturbed Resident
UPDATE `creature_equip_template` SET `ItemID1`=1902 WHERE (`CreatureID`=108824 AND `ID`=1); -- Disturbed Resident
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=11762 WHERE (`CreatureID`=44916 AND `ID`=1); -- Admiral Hatchet
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=18166 WHERE (`CreatureID`=45496 AND `ID`=1); -- Commander Hickley
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=3756 WHERE (`CreatureID`=44912 AND `ID`=1); -- Apothecary Wormcrud
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=18166 WHERE (`CreatureID`=44911 AND `ID`=1); -- Dreadguard
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=54823 WHERE (`CreatureID`=45196 AND `ID`=1); -- Orc Sea Dog
UPDATE `creature_equip_template` SET `ItemID3`=118201 WHERE (`CreatureID`=88782 AND `ID`=2); -- Nar'thalas Nightwatcher
UPDATE `creature_equip_template` SET `ItemID3`=0 WHERE (`CreatureID`=88782 AND `ID`=1); -- Nar'thalas Nightwatcher
UPDATE `creature_equip_template` SET `ItemID1`=132870 WHERE (`CreatureID`=90350 AND `ID`=1); -- Silver Hand Knight
UPDATE `creature_equip_template` SET `ItemID1`=132870 WHERE (`CreatureID`=90349 AND `ID`=2); -- Silver Hand Knight
UPDATE `creature_equip_template` SET `ItemID1`=132869 WHERE (`CreatureID`=90349 AND `ID`=1); -- Silver Hand Knight
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=47032 WHERE (`CreatureID`=19907 AND `ID`=1); -- Grumbol Grimhammer
UPDATE `creature_equip_template` SET `ItemID1`=38490 WHERE (`CreatureID`=27953 AND `ID`=7); -- Wyrmrest Protector
UPDATE `creature_equip_template` SET `ItemID1`=32729 WHERE (`CreatureID`=27953 AND `ID`=6); -- Wyrmrest Protector
UPDATE `creature_equip_template` SET `ItemID1`=38488 WHERE (`CreatureID`=27953 AND `ID`=5); -- Wyrmrest Protector
UPDATE `creature_equip_template` SET `ItemID1`=31273 WHERE (`CreatureID`=27953 AND `ID`=4); -- Wyrmrest Protector
UPDATE `creature_equip_template` SET `ItemID1`=38209 WHERE (`CreatureID`=27953 AND `ID`=3); -- Wyrmrest Protector
UPDATE `creature_equip_template` SET `ItemID1`=38487 WHERE (`CreatureID`=27953 AND `ID`=2); -- Wyrmrest Protector
UPDATE `creature_equip_template` SET `ItemID1`=38491 WHERE (`CreatureID`=27953 AND `ID`=1); -- Wyrmrest Protector
UPDATE `creature_equip_template` SET `ItemID1`=2705 WHERE (`CreatureID`=96793 AND `ID`=1); -- Stefen Cotter
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=29295 AND `ID`=1); -- Meghan Dawson
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=29294 AND `ID`=1); -- Candace Thomas
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=29293 AND `ID`=1); -- Daniel Kramer
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=19487 WHERE (`CreatureID`=29291 AND `ID`=1); -- Galley Chief Paul Kubit
UPDATE `creature_equip_template` SET `ItemID1`=2714 WHERE (`CreatureID`=29300 AND `ID`=1); -- Robert Richardson
UPDATE `creature_equip_template` SET `ItemID1`=1117 WHERE (`CreatureID`=29296 AND `ID`=1); -- Justin Boehm
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=1984 WHERE (`CreatureID`=29292 AND `ID`=1); -- Art Peshkov
UPDATE `creature_equip_template` SET `ItemID2`=0, `ItemID3`=143 WHERE (`CreatureID`=1642 AND `ID`=1); -- Northshire Guard

UPDATE `creature_template` SET `minlevel`=0, `maxlevel`=0 WHERE `entry`=111588; -- Crugin the Gatekeeper
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=98141; -- Razzok
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=91645; -- Darkfiend Dreamworg
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=91144; -- Lord Maxwell Tyrosus
UPDATE `creature_template` SET `maxlevel`=42 WHERE `entry`=45664; -- Landlocked Grouper
UPDATE `creature_template` SET `maxlevel`=58, `spell1`=0 WHERE `entry`=11620; -- Spectral Marauder
UPDATE `creature_template` SET `maxlevel`=102 WHERE `entry`=106916; -- Fallen Paladin
UPDATE `creature_template` SET `minlevel`=101, `maxlevel`=101 WHERE `entry`=107395; -- Lanigosa
UPDATE `creature_template` SET `maxlevel`=109 WHERE `entry`=107336; -- Wastes Scavenger
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=26925; -- Wyrmrest Temple Drake
UPDATE `creature_template` SET `IconName`='workorders' WHERE `entry`=109901; -- Sir Alamande Graythorn
UPDATE `creature_template` SET `unit_flags`=32832, `unit_flags2`=2099200 WHERE `entry`=96611; -- Angerhoof Bull
UPDATE `creature_template` SET `unit_flags`=32848 WHERE `entry`=93005; -- Rotting Jailer
UPDATE `creature_template` SET `unit_flags`=294912 WHERE `entry`=105680; -- Bonespeaker Drudge
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=96129; -- Felskorn Raider
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=92072; -- Grapple Point
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `BaseAttackTime`=1333 WHERE `entry`=93166; -- Tiptog the Lost
UPDATE `creature_template` SET `name`='Leyline Hunter' WHERE `entry`=100237; -- Leyline Hunter
UPDATE `creature_template` SET `name`='Leyline Researcher' WHERE `entry`=111871; -- Leyline Researcher
UPDATE `creature_template` SET `subname`='<Exotic Imports>' WHERE `entry`=112063; -- Cornelius Crispin
UPDATE `creature_template` SET `name`='Concerned Merchant' WHERE `entry`=114530; -- Concerned Merchant
UPDATE `creature_template` SET `subname`='PH MODEL' WHERE `entry`=109255; -- High Exarch Turalyon
UPDATE `creature_template` SET `unit_flags`=67141696 WHERE `entry`=91803; -- Fathnyr
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=89817; -- Vault Guardian
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286, `BaseAttackTime`=1333 WHERE `entry`=93166; -- Tiptog the Lost
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=96129; -- Felskorn Raider
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=92072; -- Grapple Point
UPDATE `creature_template` SET `subname`='PH MODEL' WHERE `entry`=109255; -- High Exarch Turalyon
UPDATE `creature_template` SET `maxlevel`=42 WHERE `entry`=45664; -- Landlocked Grouper
UPDATE `creature_template` SET `maxlevel`=58, `spell1`=0 WHERE `entry`=11620; -- Spectral Marauder
UPDATE `creature_template` SET `unit_flags`=0 WHERE `entry`=26925; -- Wyrmrest Temple Drake
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=114399; -- Doomguard Firecaller
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=113928; -- Didi the Wrench
UPDATE `creature_template` SET `unit_flags`=33536 WHERE `entry`=114401; -- Doomguard Firebrand
UPDATE `creature_template` SET `speed_run`=1.142857, `unit_flags`=360448 WHERE `entry`=114406; -- Spellfiend Devourer
UPDATE `creature_template` SET `minlevel`=109, `maxlevel`=109 WHERE `entry`=101628; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=105 WHERE `entry`=101328; -- Arcane Protector
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110 WHERE `entry`=101159; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100 WHERE `entry`=101547; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=109, `maxlevel`=109 WHERE `entry`=113637; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=-7, `maxlevel`=3 WHERE `entry`=101449; -- Lady Jaina Proudmoore
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=108 WHERE `entry`=101425; -- Archmage Khadgar
UPDATE `creature_template` SET `minlevel`=97, `maxlevel`=97, `speed_run`=1 WHERE `entry`=113348; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=95, `maxlevel`=95 WHERE `entry`=113350; -- Stabled Hunter Pet
UPDATE `creature_template` SET `minlevel`=96, `maxlevel`=96, `speed_walk`=1, `speed_run`=1.142857 WHERE `entry`=113351; -- Stabled Hunter Pet
UPDATE `creature_template` SET `speed_walk`=0.25, `speed_run`=0.2857143 WHERE `entry`=113329; -- Generic Bunny
UPDATE `creature_template` SET `unit_flags`=32784 WHERE `entry`=97352; -- Mayla Highmountain
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=103592; -- Bonebeak Hawk
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.4285714 WHERE `entry`=98809; -- Clawdayshus
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=98754; -- Sloppy "Sloppy Joe" Joe
UPDATE `creature_template` SET `npcflag`=1073741824 WHERE `entry`=105841; -- Lil'idan
UPDATE `creature_template` SET `unit_flags`=294976 WHERE `entry`=100231; -- Dargok Thunderuin
UPDATE `creature_template` SET `unit_flags2`=2048 WHERE `entry`=100230; -- "Sure-Shot" Arnie
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=97095; -- Soulkeeper Uriah
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286 WHERE `entry`=97933; -- Crab Rider Grmlrml
UPDATE `creature_template` SET `unit_flags`=8388608 WHERE `entry`=98445; -- Mudshell Conch
UPDATE `creature_template` SET `BaseAttackTime`=1143 WHERE `entry`=96621; -- Mellok, Son of Torok
UPDATE `creature_template` SET `speed_run`=1 WHERE `entry`=95799; -- Damrul the Stronk
UPDATE `creature_template` SET `faction`=2785, `speed_walk`=1, `speed_run`=1, `unit_class`=1, `unit_flags`=768, `HoverHeight`=2.5 WHERE `entry`=98777; -- Aviash
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=108185; -- Coldscale Gazecrawler
UPDATE `creature_template` SET `speed_run`=1.177143 WHERE `entry`=97955; -- Night Elf Defender
UPDATE `creature_template` SET `speed_run`=1.24 WHERE `entry`=98022; -- Plague Imp
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=96268; -- Mountain Prowler
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=94261; -- Bitestone Rockbeater
UPDATE `creature_template` SET `unit_flags`=32832 WHERE `entry`=97793; -- Flamescale
UPDATE `creature_template` SET `unit_flags2`=34816 WHERE `entry`=104221; -- Auburn Ringtail
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=103476; -- Archdruid Hamuul Runetotem
UPDATE `creature_template` SET `npcflag`=16 WHERE `entry`=12025; -- Malvor
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=101064; -- Archdruid Hamuul Runetotem
UPDATE `creature_template` SET `unit_flags`=32768 WHERE `entry`=114399; -- Doomguard Firecaller
UPDATE `creature_template` SET `npcflag`=1 WHERE `entry`=113928; -- Didi the Wrench
UPDATE `creature_template` SET `minlevel`=-7, `maxlevel`=3 WHERE `entry`=101449; -- Lady Jaina Proudmoore
UPDATE `creature_template` SET `minlevel`=102, `maxlevel`=108 WHERE `entry`=101425; -- Archmage Khadgar
UPDATE `creature_template` SET `speed_run`=1.142857, `unit_flags`=360448 WHERE `entry`=114406; -- Spellfiend Devourer
UPDATE `creature_template` SET `minlevel`=-109, `maxlevel`=-109 WHERE `entry`=17555; -- Stephanos
UPDATE `creature_template` SET `subname`='Exotic Goods' WHERE `entry`=91079; -- Vashti the Wandering Merchant
UPDATE `creature_template` SET `npcflag`=0 WHERE `entry`=106780; -- Tidestone of Golganneth
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=100591; -- Tidestone Fragment
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=100590; -- Tidestone Fragment
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=100594; -- Tidestone Fragment
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=100593; -- Tidestone Fragment
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=100592; -- Tidestone Fragment
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=90735; -- Tidestone Fragment
UPDATE `creature_template` SET `minlevel`=3, `maxlevel`=3 WHERE `entry`=91402; -- Vision of Queen Azshara
UPDATE `creature_template` SET `maxlevel`=100 WHERE `entry`=89669; -- Drowned Student
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=91265; -- Llothien Fox
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=89731; -- Fel Seeker
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5714286 WHERE `entry`=90164; -- Warbringer Mox'na
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5, `unit_flags`=294912 WHERE `entry`=90173; -- Arcana Stalker
UPDATE `creature_template` SET `unit_flags`=67141632 WHERE `entry`=89731; -- Fel Seeker

DELETE FROM `gameobject_template` WHERE `entry`=244446;
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(244446, 3, 10316, 'Actually Safe Treasure Chest', '', 'Opening', '', 1.5, 57, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 21400, 0, 0, 0, 110, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 62008, 0, 0, 98, 22522); -- Actually Safe Treasure Chest

DELETE FROM `npc_text` WHERE `ID` IN (28946 /*28946*/, 28851 /*28851*/, 28948 /*28948*/, 28757 /*28757*/, 30413 /*30413*/, 28527 /*28527*/, 26705 /*26705*/, 30510 /*30510*/, 27881 /*27881*/, 26233 /*26233*/, 30386 /*30386*/, 29565 /*29565*/, 29570 /*29570*/, 29573 /*29573*/, 27288 /*27288*/, 29031 /*29031*/, 29017 /*29017*/, 29013 /*29013*/, 28974 /*28974*/, 29213 /*29213*/, 29202 /*29202*/, 29201 /*29201*/, 29507 /*29507*/, 29237 /*29237*/, 30590 /*30590*/, 29996 /*29996*/, 28134 /*28134*/, 27092 /*27092*/, 26451 /*26451*/, 29824 /*29824*/, 30462 /*30462*/, 29650 /*29650*/, 26778 /*26778*/, 27062 /*27062*/, 28075 /*28075*/, 28074 /*28074*/, 28926 /*28926*/, 28838 /*28838*/, 28837 /*28837*/, 29486 /*29486*/, 27824 /*27824*/, 29494 /*29494*/, 29489 /*29489*/, 29882 /*29882*/, 30532 /*30532*/, 28578 /*28578*/, 28750 /*28750*/, 28617 /*28617*/, 28569 /*28569*/, 28059 /*28059*/, 28594 /*28594*/, 27432 /*27432*/, 29827 /*29827*/, 29243 /*29243*/, 28414 /*28414*/, 28419 /*28419*/, 28003 /*28003*/, 27884 /*27884*/, 29102 /*29102*/, 29569 /*29569*/, 29576 /*29576*/, 26034 /*26034*/, 29526 /*29526*/);
INSERT INTO `npc_text` (`ID`, `Probability0`, `Probability1`, `Probability2`, `Probability3`, `Probability4`, `Probability5`, `Probability6`, `Probability7`, `BroadcastTextId0`, `BroadcastTextId1`, `BroadcastTextId2`, `BroadcastTextId3`, `BroadcastTextId4`, `BroadcastTextId5`, `BroadcastTextId6`, `BroadcastTextId7`, `VerifiedBuild`) VALUES
(28946, 1, 0, 0, 0, 0, 0, 0, 0, 108709, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28946
(28851, 1, 0, 0, 0, 0, 0, 0, 0, 108646, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28851
(28948, 1, 0, 0, 0, 0, 0, 0, 0, 108111, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28948
(28757, 3, 1, 1, 0, 0, 0, 0, 0, 108110, 108112, 109287, 0, 0, 0, 0, 0, 22522), -- 28757
(30413, 1, 0, 0, 0, 0, 0, 0, 0, 122176, 0, 0, 0, 0, 0, 0, 0, 22522), -- 30413
(28527, 1, 0, 0, 0, 0, 0, 0, 0, 106429, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28527
(26705, 1, 0, 0, 0, 0, 0, 0, 0, 95519, 0, 0, 0, 0, 0, 0, 0, 22522), -- 26705
(30510, 1, 0, 0, 0, 0, 0, 0, 0, 122446, 0, 0, 0, 0, 0, 0, 0, 22522), -- 30510
(27881, 1, 0, 0, 0, 0, 0, 0, 0, 102274, 0, 0, 0, 0, 0, 0, 0, 22522), -- 27881
(26233, 1, 0, 0, 0, 0, 0, 0, 0, 93352, 0, 0, 0, 0, 0, 0, 0, 22522), -- 26233
(30386, 1, 0, 0, 0, 0, 0, 0, 0, 122075, 0, 0, 0, 0, 0, 0, 0, 22522), -- 30386
(29565, 1, 0, 0, 0, 0, 0, 0, 0, 114961, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29565
(29570, 1, 0, 0, 0, 0, 0, 0, 0, 114981, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29570
(29573, 1, 0, 0, 0, 0, 0, 0, 0, 114984, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29573
(27288, 1, 0, 0, 0, 0, 0, 0, 0, 98973, 0, 0, 0, 0, 0, 0, 0, 22522), -- 27288
(29031, 1, 0, 0, 0, 0, 0, 0, 0, 110262, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29031
(29017, 1, 0, 0, 0, 0, 0, 0, 0, 109906, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29017
(29013, 1, 0, 0, 0, 0, 0, 0, 0, 109848, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29013
(28974, 1, 0, 0, 0, 0, 0, 0, 0, 109482, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28974
(29213, 1, 0, 0, 0, 0, 0, 0, 0, 111674, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29213
(29202, 1, 0, 0, 0, 0, 0, 0, 0, 111588, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29202
(29201, 1, 0, 0, 0, 0, 0, 0, 0, 111585, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29201
(29507, 1, 0, 0, 0, 0, 0, 0, 0, 114512, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29507
(29237, 1, 0, 0, 0, 0, 0, 0, 0, 111761, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29237
(30590, 1, 0, 0, 0, 0, 0, 0, 0, 122716, 0, 0, 0, 0, 0, 0, 0, 22522), -- 30590
(29996, 1, 0, 0, 0, 0, 0, 0, 0, 119956, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29996
(28134, 1, 0, 0, 0, 0, 0, 0, 0, 104100, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28134
(27092, 1, 0, 0, 0, 0, 0, 0, 0, 97468, 0, 0, 0, 0, 0, 0, 0, 22522), -- 27092
(26451, 1, 1, 1, 1, 0, 0, 0, 0, 94341, 94342, 94343, 94344, 0, 0, 0, 0, 22522), -- 26451
(29824, 1, 0, 0, 0, 0, 0, 0, 0, 117227, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29824
(30462, 1, 0, 0, 0, 0, 0, 0, 0, 122295, 0, 0, 0, 0, 0, 0, 0, 22522), -- 30462
(29650, 1, 0, 0, 0, 0, 0, 0, 0, 115602, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29650
(26778, 1, 0, 0, 0, 0, 0, 0, 0, 95928, 0, 0, 0, 0, 0, 0, 0, 22522), -- 26778
(27062, 1, 0, 0, 0, 0, 0, 0, 0, 97338, 0, 0, 0, 0, 0, 0, 0, 22522), -- 27062
(28075, 1, 0, 0, 0, 0, 0, 0, 0, 103690, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28075
(28074, 1, 0, 0, 0, 0, 0, 0, 0, 103688, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28074
(28926, 1, 0, 0, 0, 0, 0, 0, 0, 109094, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28926
(28838, 1, 0, 0, 0, 0, 0, 0, 0, 108600, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28838
(28837, 1, 0, 0, 0, 0, 0, 0, 0, 108598, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28837
(29486, 1, 0, 0, 0, 0, 0, 0, 0, 114338, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29486
(27824, 1, 0, 0, 0, 0, 0, 0, 0, 101847, 0, 0, 0, 0, 0, 0, 0, 22522), -- 27824
(29494, 1, 0, 0, 0, 0, 0, 0, 0, 114413, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29494
(29489, 1, 0, 0, 0, 0, 0, 0, 0, 114389, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29489
(29882, 1, 0, 0, 0, 0, 0, 0, 0, 122180, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29882
(30532, 1, 0, 0, 0, 0, 0, 0, 0, 122519, 0, 0, 0, 0, 0, 0, 0, 22522), -- 30532
(28578, 1, 0, 0, 0, 0, 0, 0, 0, 106713, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28578
(28750, 1, 0, 0, 0, 0, 0, 0, 0, 108068, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28750
(28617, 1, 0, 0, 0, 0, 0, 0, 0, 107099, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28617
(28569, 1, 0, 0, 0, 0, 0, 0, 0, 106679, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28569
(28059, 1, 1, 1, 1, 0, 0, 0, 0, 103644, 111822, 111821, 111820, 0, 0, 0, 0, 22522), -- 28059
(28594, 1, 0, 0, 0, 0, 0, 0, 0, 106815, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28594
(27432, 1, 0, 0, 0, 0, 0, 0, 0, 99860, 0, 0, 0, 0, 0, 0, 0, 22522), -- 27432
(29827, 1, 0, 0, 0, 0, 0, 0, 0, 117231, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29827
(29243, 1, 0, 0, 0, 0, 0, 0, 0, 111799, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29243
(28414, 1, 0, 0, 0, 0, 0, 0, 0, 105861, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28414
(28419, 1, 0, 0, 0, 0, 0, 0, 0, 105867, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28419
(28003, 1, 0, 0, 0, 0, 0, 0, 0, 103350, 0, 0, 0, 0, 0, 0, 0, 22522), -- 28003
(27884, 1, 0, 0, 0, 0, 0, 0, 0, 102370, 0, 0, 0, 0, 0, 0, 0, 22522), -- 27884
(29102, 1, 0, 0, 0, 0, 0, 0, 0, 111058, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29102
(29569, 1, 0, 0, 0, 0, 0, 0, 0, 114975, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29569
(29576, 1, 0, 0, 0, 0, 0, 0, 0, 114987, 0, 0, 0, 0, 0, 0, 0, 22522), -- 29576
(26034, 1, 0, 0, 0, 0, 0, 0, 0, 92550, 0, 0, 0, 0, 0, 0, 0, 22522), -- 26034
(29526, 1, 0, 0, 0, 0, 0, 0, 0, 114640, 0, 0, 0, 0, 0, 0, 0, 22522); -- 29526
