DELETE FROM `quest_offer_reward` WHERE `ID` IN (29419 /*The Missing Driver*/, 29424 /*Items of Utmost Importance*/, 29410 /*Aysa of the Tushui*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(29419, 1, 0, 0, 0, 0, 0, 0, 0, 'You are too kind, $c.', 22996), -- The Missing Driver
(29424, 1, 0, 0, 0, 0, 0, 0, 0, 'Thank you! You''re an honorable $c. They''ve taught you well.', 22996), -- Items of Utmost Importance
(29410, 1, 0, 0, 0, 0, 0, 0, 0, 'You came for Aysa? You... you really shouldn''t interrupt her until she finishes her exercises. She doesn''t speak to anyone until her routine is done.$B$BIn the meantime, could you maybe help me? I had some bad luck with forest sprites.', 22996); -- Aysa of the Tushui

UPDATE `quest_poi` SET `WorldEffectID`=0, `WoDUnk1`=709234, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=1); -- Julia, The Pet Tamer
UPDATE `quest_poi` SET `WoDUnk1`=709234, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi` SET `WorldEffectID`=0, `WoDUnk1`=709234, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=1); -- Julia, The Pet Tamer
UPDATE `quest_poi` SET `WoDUnk1`=709234, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi` SET `WorldEffectID`=0, `WoDUnk1`=709234, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=1); -- Julia, The Pet Tamer
UPDATE `quest_poi` SET `WoDUnk1`=709234, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `BlobIndex`=0 AND `Idx1`=0); -- Julia, The Pet Tamer

UPDATE `quest_poi_points` SET `X`=-9876, `Y`=90, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `Idx1`=1 AND `Idx2`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi_points` SET `X`=-9876, `Y`=90, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `Idx1`=0 AND `Idx2`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi_points` SET `X`=-9876, `Y`=90, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `Idx1`=1 AND `Idx2`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi_points` SET `X`=-9876, `Y`=90, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `Idx1`=0 AND `Idx2`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi_points` SET `X`=-9876, `Y`=90, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `Idx1`=1 AND `Idx2`=0); -- Julia, The Pet Tamer
UPDATE `quest_poi_points` SET `X`=-9876, `Y`=90, `VerifiedBuild`=22996 WHERE (`QuestID`=31316 AND `Idx1`=0 AND `Idx2`=0); -- Julia, The Pet Tamer

DELETE FROM `quest_details` WHERE `ID` IN (29414 /*The Way of the Tushui*/, 29419 /*The Missing Driver*/, 29424 /*Items of Utmost Importance*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(29414, 1, 0, 0, 0, 0, 0, 0, 0, 22996), -- The Way of the Tushui
(29419, 1, 0, 0, 0, 0, 0, 0, 0, 22996), -- The Missing Driver
(29424, 1, 0, 0, 0, 0, 0, 0, 0, 22996); -- Items of Utmost Importance

DELETE FROM `quest_request_items` WHERE `ID`=29424;
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(29424, 1, 0, 0, 0, 'You needn''t worry about hurting the sprites. They''re really just living plants when it comes down to it - they''ll resprout later.', 22996); -- Items of Utmost Importance

UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=45311;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=45306;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=45308;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=44067;
UPDATE `creature_model_info` SET `BoundingRadius`=0.279, `CombatReach`=0.99, `VerifiedBuild`=22996 WHERE `DisplayID`=39481;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3819442, `CombatReach`=1.65, `VerifiedBuild`=22996 WHERE `DisplayID`=39759;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=1.1, `VerifiedBuild`=22996 WHERE `DisplayID`=39480;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=1.1, `VerifiedBuild`=22996 WHERE `DisplayID`=39478;
UPDATE `creature_model_info` SET `BoundingRadius`=0.31, `CombatReach`=1.1, `VerifiedBuild`=22996 WHERE `DisplayID`=39479;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=44146;
UPDATE `creature_model_info` SET `BoundingRadius`=0.347222, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=43682;
UPDATE `creature_model_info` SET `BoundingRadius`=0.3819442, `CombatReach`=1.65, `VerifiedBuild`=22996 WHERE `DisplayID`=39644;
UPDATE `creature_model_info` SET `BoundingRadius`=0.217, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=39807;
UPDATE `creature_model_info` SET `BoundingRadius`=0.2635, `CombatReach`=1.5, `VerifiedBuild`=22996 WHERE `DisplayID`=39806;
UPDATE `creature_model_info` SET `BoundingRadius`=0.29495, `CombatReach`=1.7, `VerifiedBuild`=22996 WHERE `DisplayID`=39804;

UPDATE `creature_template` SET `minlevel`=90, `VerifiedBuild`=22996 WHERE `entry`=66706; -- Wandering Pilgrim
UPDATE `creature_template` SET `minlevel`=90, `VerifiedBuild`=22996 WHERE `entry`=64948; -- Wandering Celebrant
UPDATE `creature_template` SET `maxlevel`=89, `VerifiedBuild`=22996 WHERE `entry`=66705; -- Wandering Celebrant
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=22996 WHERE `entry`=92458; -- Deucus Valdera
UPDATE `creature_template` SET `npcflag`=17, `VerifiedBuild`=22996 WHERE `entry`=92456; -- Linzy Blackbolt
UPDATE `creature_template` SET `npcflag`=16, `VerifiedBuild`=22996 WHERE `entry`=92459; -- Dorothy Egan
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=22996 WHERE `entry`=92183; -- Alard Schmied
UPDATE `creature_template` SET `npcflag`=19, `VerifiedBuild`=22996 WHERE `entry`=92464; -- Kuhuine Tenderstride
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=22996 WHERE `entry`=106655; -- Arcanomancer Vridiel
UPDATE `creature_template` SET `faction`=14, `VerifiedBuild`=22996 WHERE `entry`=108628; -- Armond Thaco
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=768, `unit_flags2`=67110912, `VerifiedBuild`=22996 WHERE `entry`=116604; -- Farseer's Armor
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=2, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33024, `unit_flags2`=33556480, `VerifiedBuild`=22996 WHERE `entry`=106515; -- Stormcaller Mylra
UPDATE `creature_template` SET `npcflag`=1, `VerifiedBuild`=22996 WHERE `entry`=99428; -- Scouting Map
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106457; -- Summoner Morn
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=110, `faction`=35, `speed_walk`=1.2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106520; -- Duke Hydraxis
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=110, `faction`=35, `speed_walk`=1.2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106649; -- Baron Scaldius
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=110, `faction`=35, `speed_walk`=1.2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106524; -- Avalanchion the Unbroken
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=112199; -- Journeyman Goldmine
UPDATE `creature_template` SET `faction`=35, `npcflag`=1, `VerifiedBuild`=22996 WHERE `entry`=96747; -- Bath'rah the Windwatcher
UPDATE `creature_template` SET `speed_run`=1, `unit_flags`=33555200, `VerifiedBuild`=22996 WHERE `entry`=45222; -- Goblin Fire Totem
UPDATE `creature_template` SET `speed_run`=1, `unit_flags`=33555200, `VerifiedBuild`=22996 WHERE `entry`=45221; -- Goblin Earth Totem
UPDATE `creature_template` SET `faction`=35, `VerifiedBuild`=22996 WHERE `entry`=96745; -- Orono
UPDATE `creature_template` SET `faction`=35, `VerifiedBuild`=22996 WHERE `entry`=96758; -- Mackay Firebeard
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `speed_walk`=1.2, `speed_run`=0.7142857, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=113255; -- Lesser Elemental
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=110, `faction`=35, `speed_walk`=1.2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106521; -- Consular Celestos
UPDATE `creature_template` SET `minlevel`=105, `maxlevel`=110, `faction`=35, `npcflag`=3, `speed_walk`=1.2, `speed_run`=1.428571, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106519; -- Farseer Nobundo
UPDATE `creature_template` SET `minlevel`=110, `maxlevel`=110, `faction`=35, `npcflag`=3, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=112208; -- Felinda Frye
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags`=32768, `unit_flags2`=33556480, `VerifiedBuild`=22996 WHERE `entry`=112597; -- Earthcaller Yevaa
UPDATE `creature_template` SET `minlevel`=5, `maxlevel`=5, `faction`=35, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=33556480, `VerifiedBuild`=22996 WHERE `entry`=112606; -- Young Initiate
UPDATE `creature_template` SET `npcflag`=134217729, `VerifiedBuild`=22996 WHERE `entry`=112262; -- Gavan Grayfeather
UPDATE `creature_template` SET `minlevel`=5, `maxlevel`=5, `faction`=35, `npcflag`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=33556480, `VerifiedBuild`=22996 WHERE `entry`=112604; -- Young Initiate
UPDATE `creature_template` SET `unit_flags`=33536, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=99092; -- Immortal Stone Protector
UPDATE `creature_template` SET `minlevel`=5, `maxlevel`=5, `faction`=35, `npcflag`=1, `speed_run`=1, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=33024, `unit_flags2`=33556480, `VerifiedBuild`=22996 WHERE `entry`=112605; -- Young Initiate
UPDATE `creature_template` SET `minlevel`=100, `maxlevel`=100, `faction`=35, `BaseAttackTime`=2000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags`=32768, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=106518; -- Muln Earthfury
UPDATE `creature_template` SET `minlevel`=98, `maxlevel`=110, `faction`=35, `BaseAttackTime`=1000, `RangeAttackTime`=2000, `unit_class`=1, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=113257; -- Earthcaller
UPDATE `creature_template` SET `maxlevel`=3, `VerifiedBuild`=22996 WHERE `entry`=65472; -- Wu-Song Villager
UPDATE `creature_template` SET `minlevel`=4, `VerifiedBuild`=22996 WHERE `entry`=57132; -- Wu-Song Villager
UPDATE `creature_template` SET `unit_flags`=537166592, `unit_flags2`=2049, `VerifiedBuild`=22996 WHERE `entry`=54131; -- Fe-Feng Hozen
UPDATE `creature_template` SET `unit_flags`=33536, `VerifiedBuild`=22996 WHERE `entry`=53566; -- Master Shang Xi
UPDATE `creature_template` SET `unit_flags`=0, `unit_flags2`=2048, `VerifiedBuild`=22996 WHERE `entry`=883; -- Deer
UPDATE `creature_template` SET `minlevel`=3, `VerifiedBuild`=22996 WHERE `entry`=42339; -- Canal Crab
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=22996 WHERE `entry`=5499; -- Lilyssia Nightbreeze
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=22996 WHERE `entry`=5566; -- Tannysa
UPDATE `creature_template` SET `npcflag`=83, `VerifiedBuild`=22996 WHERE `entry`=5566; -- Tannysa
