DELETE FROM `spell_script_names` WHERE `ScriptName` IN (
'spell_dru_legion_abundance',
'spell_dru_legion_adaptive_fur',
'spell_dru_legion_ashamanes_bite',
'spell_dru_legion_ashamanes_rip',
'spell_dru_legion_ashamanes_energy',
'spell_dru_legion_ashamanes_frenzy',
'spell_dru_legion_ashamanes_frenzy_periodic',
'spell_dru_legion_barkskin',
'spell_dru_legion_bear_form',
'spell_dru_legion_blessing_of_the_ancients',
'spell_dru_legion_bloodtalons',
'spell_dru_legion_bloody_paws',
'spell_dru_legion_brambles',
'spell_dru_legion_bristling_fur',
'spell_dru_legion_brutal_slash',
'spell_dru_legion_cat_form',
'spell_dru_legion_celestial_alignment',
'spell_dru_legion_cenarion_ward',
'spell_dru_legion_clan_defender',
'spell_dru_legion_cultivation',
'spell_dru_legion_displacer_beast',
'spell_dru_legion_dreamwalker',
'spell_dru_legion_druid_of_the_claw',
'spell_dru_legion_druid_shapeshift_forms',
'spell_dru_legion_druid_travel_forms',
'spell_dru_legion_earthwarden',
'spell_dru_legion_earthwarden_proc',
'spell_dru_legion_echoing_stars',
'spell_dru_legion_efflorescence',
'spell_dru_legion_efflorescence_heal',
'spell_dru_legion_efflorescence_periodic',
'spell_dru_legion_entangling_roots',
'spell_dru_legion_empowerment',
'spell_dru_legion_enraged_maim',
'spell_dru_legion_feral_instinct',
'spell_dru_legion_ferocious_bite',
'spell_dru_legion_flourish',
'spell_dru_legion_frenzied_regeneration',
'spell_dru_legion_full_moon',
'spell_dru_legion_fury_of_elune_override',
'spell_dru_legion_fury_of_elune_periodic',
'spell_dru_legion_galactic_guardian',
'spell_dru_legion_gore',
'spell_dru_legion_healing_touch',
'spell_dru_legion_incarnation_tree_of_life',
'spell_dru_legion_innervate',
'spell_dru_legion_infected_wounds',
'spell_dru_legion_ironfur',
'spell_dru_legion_king_of_the_jungle',
'spell_dru_legion_lifebloom',
'spell_dru_legion_living_seed_passive',
'spell_dru_legion_living_seed_triggered',
'spell_dru_legion_lunar_strike',
'spell_dru_legion_maim',
'spell_dru_legion_mangle',
'spell_dru_legion_mark_of_shifting_periodic',
'spell_dru_legion_master_shapeshifter',
'spell_dru_legion_moonkin_form_balance_affinity',
'spell_dru_legion_moon_and_stars',
'spell_dru_legion_moon_and_stars_dps_proc',
'spell_dru_legion_moon_and_stars_remove',
'spell_dru_legion_moonkin_form',
'spell_dru_legion_moonfire_galactic_guardian',
'spell_dru_legion_moonfire_sunfire_initial',
'spell_dru_legion_moonfire',
'spell_dru_legion_natures_balance',
'spell_dru_legion_power_of_the_archdruid_trigger',
'spell_dru_legion_power_of_goldrinn',
'spell_dru_legion_predator',
'spell_dru_legion_predatory_swiftness',
'spell_dru_legion_predatory_swiftness_helper',
'spell_dru_legion_pulverize',
'spell_dru_legion_rage_of_the_sleeper',
'spell_dru_legion_rake',
'spell_dru_legion_rapid_innervation',
'spell_dru_legion_regrowth',
'spell_dru_legion_rejuvenation_soul_of_the_forest',
'spell_dru_legion_rend_and_tear',
'spell_dru_legion_rip',
'spell_dru_legion_rip_and_tear',
'spell_dru_legion_revitalize',
'spell_dru_legion_savage_roar',
'spell_dru_legion_shadow_thrash',
'spell_dru_legion_shadow_thrash_periodic',
'spell_dru_legion_shooting_stars',
'spell_dru_legion_shred',
'spell_dru_legion_skull_bash_interrupt',
'spell_dru_legion_solar_beam',
'spell_dru_legion_solar_wrath',
'spell_dru_legion_soul_of_the_forest',
'spell_dru_legion_stampeding_roar',
'spell_dru_legion_starfall_damage',
'spell_dru_legion_swiftmend',
'spell_dru_legion_swipe',
'spell_dru_legion_sunfire',
'spell_dru_legion_teleport_moonglade',
'spell_dru_legion_thrash',
'spell_dru_legion_thrash_periodic',
'spell_dru_legion_tooth_and_claw',
'spell_dru_legion_thrash_feral',
'spell_dru_legion_tranquility',
'spell_dru_legion_travel_form',
'spell_dru_legion_wild_growth',
'spell_dru_legion_wild_growth_soul_of_the_forest',
'spell_dru_legion_incarnation_chosen_of_elune',
'spell_dru_legion_yseras_gift_party_heal',
'spell_dru_legion_yseras_gift_periodic',
'spell_dru_legion_overgrowth',
'spell_dru_legion_thorns_damage',
'spell_dru_legion_survival_instincts',
'spell_dru_legion_killer_instinct'
);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(155777, 'spell_dru_legion_abundance'),
(774,    'spell_dru_legion_abundance'),
(200850, 'spell_dru_legion_adaptive_fur'),
(210702, 'spell_dru_legion_ashamanes_bite'),
(210705, 'spell_dru_legion_ashamanes_rip'),
(210579, 'spell_dru_legion_ashamanes_energy'),
(210722, 'spell_dru_legion_ashamanes_frenzy'),
(210723, 'spell_dru_legion_ashamanes_frenzy_periodic'),
(22812,  'spell_dru_legion_barkskin'),
(5487,   'spell_dru_legion_bear_form'),
(202360, 'spell_dru_legion_blessing_of_the_ancients'),
(8936,   'spell_dru_legion_bloodtalons'),
(200515, 'spell_dru_legion_bloody_paws'),
(203953, 'spell_dru_legion_brambles'),
(155835, 'spell_dru_legion_bristling_fur'),
(202028, 'spell_dru_legion_brutal_slash'),
(102351, 'spell_dru_legion_cenarion_ward'),
(213947, 'spell_dru_legion_clan_defender'),
(768,    'spell_dru_legion_cat_form'),
(194223, 'spell_dru_legion_celestial_alignment'),
(774,    'spell_dru_legion_cultivation'),
(155777, 'spell_dru_legion_cultivation'),
(102280, 'spell_dru_legion_displacer_beast'),
(189854, 'spell_dru_legion_dreamwalker'),
(209690, 'spell_dru_legion_druid_of_the_claw'),
(768,    'spell_dru_legion_druid_shapeshift_forms'),
(5487,   'spell_dru_legion_druid_shapeshift_forms'),
(24858,  'spell_dru_legion_druid_shapeshift_forms'),
(197625, 'spell_dru_legion_druid_shapeshift_forms'),
(1066,   'spell_dru_legion_druid_travel_forms'),
(33943,  'spell_dru_legion_druid_travel_forms'),
(40120,  'spell_dru_legion_druid_travel_forms'),
(165961, 'spell_dru_legion_druid_travel_forms'),
(203975, 'spell_dru_legion_earthwarden'),
(203974, 'spell_dru_legion_earthwarden_proc'),
(214519, 'spell_dru_legion_echoing_stars'),
(145205, 'spell_dru_legion_efflorescence'),
(81269,  'spell_dru_legion_efflorescence_heal'),
(81262,  'spell_dru_legion_efflorescence_periodic'),
(339,    'spell_dru_legion_entangling_roots'),
(164545, 'spell_dru_legion_empowerment'),
(164547, 'spell_dru_legion_empowerment'),
(236025, 'spell_dru_legion_enraged_maim'),
(210631, 'spell_dru_legion_feral_instinct'),
(22568,  'spell_dru_legion_ferocious_bite'),
(197721, 'spell_dru_legion_flourish'),
(22842,  'spell_dru_legion_frenzied_regeneration'),
(202771, 'spell_dru_legion_full_moon'),
(211547, 'spell_dru_legion_fury_of_elune_override'),
(202770, 'spell_dru_legion_fury_of_elune_periodic'),
(203964, 'spell_dru_legion_galactic_guardian'),
(210706, 'spell_dru_legion_gore'),
(5185,   'spell_dru_legion_healing_touch'),
(33891,  'spell_dru_legion_incarnation_tree_of_life'),
(29166,  'spell_dru_legion_innervate'),
(48484,  'spell_dru_legion_infected_wounds'),
(192081, 'spell_dru_legion_ironfur'),
(203052, 'spell_dru_legion_king_of_the_jungle'),
(33763,  'spell_dru_legion_lifebloom'),
(48500,  'spell_dru_legion_living_seed_passive'),
(48504,  'spell_dru_legion_living_seed_triggered'),
(194153, 'spell_dru_legion_lunar_strike'),
(197628, 'spell_dru_legion_lunar_strike'),
(22570,  'spell_dru_legion_maim'),
(33917,  'spell_dru_legion_mangle'),
(186370, 'spell_dru_legion_mark_of_shifting_periodic'),
(236144, 'spell_dru_legion_master_shapeshifter'),
(197625, 'spell_dru_legion_moonkin_form_balance_affinity'),
(202940, 'spell_dru_legion_moon_and_stars'),
(202941, 'spell_dru_legion_moon_and_stars_dps_proc'),
(102560, 'spell_dru_legion_moon_and_stars_remove'),
(194223, 'spell_dru_legion_moon_and_stars_remove'),
(24858,  'spell_dru_legion_moonkin_form'),
(164812, 'spell_dru_legion_moonfire_galactic_guardian'),
(197630, 'spell_dru_legion_moonfire_sunfire_initial'),
(8921,   'spell_dru_legion_moonfire_sunfire_initial'),
(93402,  'spell_dru_legion_moonfire_sunfire_initial'),
(164812, 'spell_dru_legion_moonfire'),
(194153, 'spell_dru_legion_natures_balance'),
(190984, 'spell_dru_legion_natures_balance'),
(203651, 'spell_dru_legion_overgrowth'),
(774,    'spell_dru_legion_power_of_the_archdruid_trigger'),
(8936,   'spell_dru_legion_power_of_the_archdruid_trigger'),
(155777, 'spell_dru_legion_power_of_the_archdruid_trigger'),
(202996, 'spell_dru_legion_power_of_goldrinn'),
(155722, 'spell_dru_legion_predator'),
(1079,   'spell_dru_legion_predator'),
(106830, 'spell_dru_legion_predator'),
(16974,  'spell_dru_legion_predatory_swiftness'),
(1079,   'spell_dru_legion_predatory_swiftness_helper'),
(22570,  'spell_dru_legion_predatory_swiftness_helper'),
(22568,  'spell_dru_legion_predatory_swiftness_helper'),
(52610,  'spell_dru_legion_predatory_swiftness_helper'),
(80313,  'spell_dru_legion_pulverize'),
(200851, 'spell_dru_legion_rage_of_the_sleeper'),
(1822,   'spell_dru_legion_rake'),
(202890, 'spell_dru_legion_rapid_innervation'),
(8936,   'spell_dru_legion_regrowth'),
(774,    'spell_dru_legion_rejuvenation_soul_of_the_forest'),
(203407, 'spell_dru_legion_revitalize'),
(204053, 'spell_dru_legion_rend_and_tear'),
(1079,   'spell_dru_legion_rip'),
(203242, 'spell_dru_legion_rip_and_tear'),
(52610,  'spell_dru_legion_savage_roar'),
(210676, 'spell_dru_legion_shadow_thrash'),
(210686, 'spell_dru_legion_shadow_thrash_periodic'),
(202342, 'spell_dru_legion_shooting_stars'),
(5221,   'spell_dru_legion_shred'),
(93985,  'spell_dru_legion_skull_bash_interrupt'),
(97547,  'spell_dru_legion_solar_beam'),
(190984, 'spell_dru_legion_solar_wrath'),
(197629, 'spell_dru_legion_solar_wrath'),
(1079,   'spell_dru_legion_soul_of_the_forest'),
(22570,  'spell_dru_legion_soul_of_the_forest'),
(22568,  'spell_dru_legion_soul_of_the_forest'),
(77761,  'spell_dru_legion_stampeding_roar'),
(77764,  'spell_dru_legion_stampeding_roar'),
(106898, 'spell_dru_legion_stampeding_roar'),
(191037, 'spell_dru_legion_starfall_damage'),
(18562,  'spell_dru_legion_swiftmend'),
(106785, 'spell_dru_legion_swipe'),
(213764, 'spell_dru_legion_swipe'),
(164815, 'spell_dru_legion_sunfire'),
(18960,  'spell_dru_legion_teleport_moonglade'),
(77758,  'spell_dru_legion_thrash'),
(192090, 'spell_dru_legion_thrash_periodic'),
(106830, 'spell_dru_legion_thrash_feral'),
(236019, 'spell_dru_legion_tooth_and_claw'),
(203728, 'spell_dru_legion_thorns_damage'),
(157982, 'spell_dru_legion_tranquility'),
(783,    'spell_dru_legion_travel_form'),
(48438,  'spell_dru_legion_wild_growth'),
(48438,  'spell_dru_legion_wild_growth_soul_of_the_forest'),
(102560, 'spell_dru_legion_incarnation_chosen_of_elune'),
(145110, 'spell_dru_legion_yseras_gift_party_heal'),
(145108, 'spell_dru_legion_yseras_gift_periodic'),
(61336,  'spell_dru_legion_survival_instincts'),
(108300, 'spell_dru_legion_killer_instinct');

DELETE FROM `spell_proc` WHERE `SpellId` IN (16870, 16974, 24858, 48484, 48500, 48504, 93622, 113043, 135700, 155578,159286,200515,200854,202342,202890,202941,202996,203407,203974,209740,210579,210631,210676,213680,213947,233673,210706);
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `ProcFlags`, `SpellTypeMask`, `SpellPhaseMask`, `HitMask`, `Chance`, `Charges`) VALUES
(16870,         0x8,       7,      0x0,         0x0,             0x0,             0x0,           0x4000,         0x0, 0x4, 0x0,       0, 1),
(16974,         0x1,       7,      0x0,         0x10000080,      0x200000,        0x1000,        0x0,            0x0, 0x2, 0x0,       0, 0),
(24858,         0x0,       0,      0x0,         0x0,             0x0,             0x0,           0x0,            0x1, 0x0, 0x0,       0, 0),
(48484,         0x0,       7,      0x1000,      0x0,             0x0,             0x0,           0x0,            0x0, 0x2, 0x0,       0, 0),
(48500,         0x8,       7,      0x60,        0x2,             0x0,             0x0,           0x0,            0x0, 0x2, 0x2,       0, 0),
(48504,         0x0,       0,      0x0,         0x0,             0x0,             0x0,           0x0,            0x1, 0x0, 0x0,       0, 0),
(93622,         0x0,       7,      0x0,         0x40,            0x0,             0x0,           0x0,            0x0, 0x4, 0x0,       0, 0),
(113043,        0x8,       7,      0x0,         0x1010,          0x0,             0x0,           0x40000,        0x0, 0x0, 0x0,       0, 0),
(135700,        0x0,       0,      0x0,         0x0,             0x0,             0x0,           0x0,            0x0, 0x4, 0x0,       0, 0),
(155578,        0x0,       7,      0x0,         0x40,            0x0,             0x0,           0x0,            0x0, 0x1, 0x0,       0, 0),
(159286,        0x41,      7,      0x9000,      0x0,             0x400,           0x1000008,     0x0,            0x0, 0x2, 0x2,       0, 0),
(200515,        0x0,       7,      0x0,         0x0,             0x400000,        0x0,           0x0,            0x0, 0x2, 0x0,       0, 0),
(200854,        0x0,       7,      0x0,         0x40,            0x0,             0x0,           0x0,            0x0, 0x4, 0x0,       0, 0),
(202342,        0x48,      7,      0x2,         0x0,             0x0,             0x0,           0x40000,        0x0, 0x0, 0x0,      10, 0),
(202890,        0x0,       7,      0x0,         0x0,             0x8000,          0x0,           0x0,            0x0, 0x1, 0x0,       0, 0),
(202941,        0x0,       7,      0x0,         0x0,             0x0,             0x0,           0x0,            0x0, 0x1, 0x0,       0, 0),
(202996,        0x0,       7,      0x0,         0x0,             0x2000000,       0x0,           0x0,            0x0, 0x2, 0x0,       0, 0),
(203407,        0x0,       7,      0x0,         0x0,             0x0,             0x0,           0x0,            0x0, 0x0, 0x2,       0, 0),
(203974,        0x1,       7,      0x0,         0x0,             0x8000000,       0x0,           0x0,            0x0, 0x2, 0x0,       0, 0),
(209740,        0x0,       7,      0x0,         0x0,             0x2000000,       0x0,           0x0,            0x0, 0x1, 0x0,       0, 0),
(210579,        0x0,       7,      0x0,         0x0,             0x800,           0x0,           0x0,            0x0, 0x1, 0x0,       0, 0),
(210631,        0x0,       7,      0x0,         0x800,           0x40,            0x0,           0x0,            0x0, 0x1, 0x0,       0, 0),
(210676,        0x0,       7,      0x0,         0x0,             0x400000,        0x0,           0x0,            0x0, 0x4, 0x0,       0, 0),
(210706,        0x0,       7,      0x802,       0x100000,        0x8000000,       0x0,           0x0,            0x0, 0x1, 0x0,       0, 0),
(213680,        0x0,       7,      0x0,         0x0,             0x20004000,      0x0,           0x0,            0x0, 0x4, 0x0,       0, 0),
(213947,        0x0,       7,      0x0,         0x0,             0x0,             0x0,           0x0,            0x0, 0x0, 0x2,       0, 0),
(233673,        0x0,       7,      0x0,         0x0,             0x2,             0x0,           0x0,            0x0, 0x2, 0x0,       0, 0);

-- New moon
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=202767 AND `spell_effect`=202787;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(202767, 202787, 0, 'Add new moon override after cast');

-- half moon
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=202768 AND `spell_effect`=202788;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(202768, 202788, 0, 'Add half moon override after cast');

-- half moon
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=202768 AND `spell_effect`=-202787;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(202768, -202787, 0, 'Add half moon override removes new moon override');

-- full moon
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=202771 AND `spell_effect`=-202788;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(202771, -202788, 0, 'Full moon removes half moon override');

-- Overrun
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=202249 AND `spell_effect`=202244;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(202249, 202244, 0, 'Cast overrun knockback on root cast');

-- Killer Instinct
DELETE FROM `spell_linked_spell` WHERE `spell_effect` IN (108300,-108300);
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
(5487, 108300, 0, 'Cast Killer Instinct when Bear Form is cast'),
(768, 108300, 0, 'Cast Killer Instinct when Cat Form is cast'),
(-5487, -108300, 0, 'Remove Killer Instinct when Bear Form is removed'),
(-768, -108300, 0, 'Remove Killer Instinct when Cat Form is removed');

DELETE FROM spell_linked_spell WHERE spell_trigger IN (155580, 106839, -768, 1850, 5215, -117679, 102547);
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
(155580, 155627, 2, 'Add Lunar Inspiration linked spell'),
(106839, 93985, 0, 'Cast skull bash interrupt on skull bash dummy cast'),
(106839, 221514, 0, 'Cast skull bash charge on skull bash dummy cast'),
(-768, -5215, 0, 'Remove prowl on cat form remove'),
(1850, 768, 0, 'Activate cat form on dash cast'),
(5215, 768, 0, 'Activate cat form on prowl cast'),
(-117679, -33891, 0, 'Remove tree of life on incarnation remove'),
(-768, -102547, 0, 'Remove prowl on cat form remove'),
(102547, 768, 0, 'Activate cat form on prowl cast');

DELETE FROM `spell_custom_attr` WHERE `entry` = 5221;

DELETE FROM `spell_areatrigger` WHERE `SpellMiscId` IN (4756, 983, 6887, 314, 5994, 7173, 5780, 9750, 9507);
INSERT INTO `spell_areatrigger` (`SpellMiscId`, `AreaTriggerId`, `ScaleCurveId`, `MorphCurveId`, `FacingCurveId`, `MoveCurveId`, `DecalPropertiesId`, `TimeToTarget`, `TimeToTargetScale`, `TimeToTargetExtraScale`, `VerifiedBuild`) VALUES
(4756,  9482, 0, 0, 0, 0, 0, 0,     0, 0, 22522), -- 191034
(983,   4744, 0, 0, 0, 0, 0, 0,  8000, 0, 22522), -- 78675
(6887, 11393, 0, 0, 0, 0, 0, 0,     0, 0, 22522), -- 202770
(314,   3020, 0, 0, 0, 0, 0, 0, 10000, 0, 22995), -- 102793
(5994, 10682, 0, 0, 0, 0, 0, 0,  8500, 0, 22522),
(7173, 50015, 0, 0, 0, 0, 0, 0,     0, 0, 0),     -- 213951  
(5780, 50016, 0, 0, 0, 0, 0, 0,     0, 0, 0),     -- 201940 
(9750, 50017, 0, 0, 0, 0, 0, 0,     0, 0, 0),     -- 236180
(9507, 13493, 0, 0, 0, 0, 0, 0, 10000, 0, 24461); -- SpellId: 233756

UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_dru_legion_lunar_beam' WHERE `Id`=10682;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_dru_legion_starfall' WHERE `Id`=9482;
UPDATE `areatrigger_template` SET `ScriptName`='areatrigger_dru_legion_ursols_vortex' WHERE `Id`=3020;

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (4744, 3020, 50015, 50016, 50017, 13493);
INSERT INTO `areatrigger_template_actions` (`AreaTriggerId`, `ActionType`, `ActionParam`, `TargetType`) VALUES 
(4744, 0,  81261, 2),
(3020, 0, 127797, 2),
(50015, 1, 213947, 3),
(50016, 1, 201939, 3),
(50017, 1, 236181, 3),
(13493, 1, 234084, 5);

DELETE FROM `areatrigger_template` WHERE `Id` IN (50015, 50016, 50017);
INSERT INTO `areatrigger_template` (`Id`, `Flags`, `Data0`, `Data1`, `ScriptName`) VALUES 
(50015, 4, 15, 15, ''), -- Clan Defender server side areatrigger
(50016, 4, 40, 40, ''), -- Protector of the Pack server side areatrigger
(50017, 4, 15, 15, ''); -- Den Mother server side areatrigger

DELETE FROM `areatrigger_template` WHERE `Id`=13493;
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `AIName`, `ScriptName`, `VerifiedBuild`) VALUES
(13493, 0, 0, 3, 3, 0, 0, 0, 0, '', '', 24461);

DELETE FROM `spell_areatrigger_scale` WHERE `SpellMiscId` = 9507;
INSERT INTO `spell_areatrigger_scale` (`SpellMiscId`, `ScaleCurveExtra5`, `ScaleCurveExtra6`) VALUES 
(9507, 1, 1);

UPDATE creature_template SET ScriptName = 'npc_dru_legion_force_of_nature' WHERE entry = 103822;