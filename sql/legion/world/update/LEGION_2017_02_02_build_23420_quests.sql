DELETE FROM `quest_offer_reward` WHERE `ID` IN (43889 /*The Nighthold: Vaults*/, 43896 /*Sealing Fate: Piles of Gold*/, 43895 /*Sealing Fate: Gold*/, 43892 /*Sealing Fate: Order Resources*/, 43179 /*The Kirin Tor of Dalaran*/, 44053 /*Friends With Benefits*/, 43318 /*Ly'leth's Champion*/, 43317 /*In the Bag*/, 43313 /*Rumor Has It*/, 43315 /*Death Becomes Him*/, 43311 /*Or Against Us*/, 41915 /*The Master's Legacy*/, 41146 /*Elegant Design*/, 41465 /*Estate Jewelry: A Braggart's Brooch*/, 41466 /*Estate Jewelry: Haute Claw-ture*/, 41320 /*Pry It From Their Cold, Feral Claws*/, 41307 /*All That Glitters*/, 41123 /*An Artisan's Mark*/, 41215 /*They Become The Hunted*/, 41230 /*Trapping Evolved*/, 40617 /*Turn Around, Nighteyes*/, 42223 /*Scouting the Crimson Thicket*/, 44562 /*Growing Strong*/, 42421 /*The Nightfallen*/, 42422 /*The Wardens*/, 42420 /*Court of Farondis*/, 39910 /*The Druid's Debt*/, 40972 /*Last Stand of the Moon Guard*/, 43994 /*Feed Thalyssra*/, 43808 /*Moon Guard Teleporter Online!*/, 40971 /*Overwhelming Distraction*/, 40970 /*The Orchestrator of Our Demise*/, 40969 /*Starweaver's Fate*/, 41032 /*Stop the Spell Seekers*/, 40965 /*Lay Waste, Lay Mines*/, 40967 /*Precious Little Left*/, 40964 /*The Rift Between*/, 40963 /*Take Them in Claw*/, 40883 /*Fate of the Guard*/, 40949 /*Not Their Last Stand*/, 41030 /*Sigil Reclamation*/, 41494 /*Eminent Grow-main*/, 41469 /*Return to Irongrove Retreat*/, 41480 /*Managazer*/, 41485 /*Moonwhisper Rescue*/, 41479 /*Natural Adversaries*/, 41478 /*The Final Blessing*/, 41475 /*Prongs and Fangs*/, 41474 /*Fertilizing the Future*/, 41473 /*Redhoof the Ancient*/, 41467 /*The Only Choice We Can Make*/, 41464 /*Not Here, Not Now, Not Ever*/, 41463 /*Missing Along the Way*/, 41197 /*You've Got to Be Kitten Me Right Meow*/, 41462 /*Trouble Has Huge Feet*/, 41453 /*Homeward Bounding*/, 41183 /*Academic Exploration*/, 41302 /*Work Order: Starlight Roses*/, 44719 /*-Unknown-*/, 44964 /*-Unknown-*/, 45269 /*-Unknown-*/, 45064 /*-Unknown-*/, 45066 /*-Unknown-*/, 45065 /*-Unknown-*/, 45062 /*-Unknown-*/, 45063 /*-Unknown-*/, 45067 /*-Unknown-*/, 44919 /*-Unknown-*/, 44918 /*-Unknown-*/, 45268 /*-Unknown-*/, 44833 /*-Unknown-*/, 44832 /*-Unknown-*/, 45209 /*-Unknown-*/, 44822 /*-Unknown-*/, 44736 /*-Unknown-*/, 45267 /*-Unknown-*/, 45317 /*-Unknown-*/, 41318 /*-Unknown-*/, 43897 /*Sealing Fate: Immense Fortune of Gold*/, 40795 /*The Fight Begins*/, 40793 /*A Matter of Planning*/, 40698 /*Purity of Form*/, 40570 /*Into The Heavens*/, 40634 /*Thunder on the Sands*/, 40633 /*Off To Adventure!*/, 40569 /*The Legend of the Sands*/, 40636 /*Prepare To Strike*/, 40236 /*The Dawning Light*/, 12103 /*Before the Storm*/, 44663 /*In the Blink of an Eye*/, 40605 /*Keep Your Friends Close*/, 40607 /*Demons Among Us*/, 40760 /*Emissary*/, 40522 /*Fate of the Horde*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(43889, 0, 0, 0, 0, 0, 0, 0, 0, 'Were you able to overcome the The Nighthold?', 23420), -- The Nighthold: Vaults
(43896, 1, 0, 0, 0, 0, 0, 0, 0, 'A simple exchange with complex results.  That''s the art of doing business.', 23420), -- Sealing Fate: Piles of Gold
(43895, 1, 0, 0, 0, 0, 0, 0, 0, 'A simple exchange with complex results.  That''s the art of doing business.', 23420), -- Sealing Fate: Gold
(43892, 6, 0, 0, 0, 0, 0, 0, 0, 'Feeling safe, stranger?', 23420), -- Sealing Fate: Order Resources
(43179, 0, 0, 0, 0, 0, 0, 0, 0, 'The kirin tor thank you.', 23420), -- The Kirin Tor of Dalaran
(44053, 1, 0, 0, 0, 0, 0, 0, 0, 'She certainly is a worthy companion.', 23420), -- Friends With Benefits
(43318, 66, 0, 0, 0, 0, 0, 0, 0, 'We made great strides today. I will not forget this.', 23420), -- Ly'leth's Champion
(43317, 0, 0, 0, 0, 0, 0, 0, 0, 'This may not end as cleanly as I would have preferred.', 23420), -- In the Bag
(43313, 1, 0, 0, 0, 0, 0, 0, 0, 'The city is buzzing with very interesting gossip, $n. Victory is that much closer.', 23420), -- Rumor Has It
(43315, 1, 0, 0, 0, 0, 0, 0, 0, 'It seemed as though you had that well in hand. I did not want to... interrupt.$b$bThat is one less power hoarding noble to worry about. Well done, $n.', 23420), -- Death Becomes Him
(43311, 1, 0, 0, 0, 0, 0, 0, 0, 'Very well. Let us talk strategy.', 23420), -- Or Against Us
(41915, 574, 0, 0, 0, 0, 0, 0, 0, 'Well well, looks like that old fool finally met his match?$b$bIt looks like the Promenade is in need of a new jeweler, and I think I have learned all I could from my former master.$b$bHere is something for your efforts.', 23420), -- The Master's Legacy
(41146, 603, 0, 0, 0, 0, 0, 0, 0, 'Lespin shall pay for his crimes!', 23420), -- Elegant Design
(41465, 0, 0, 0, 0, 0, 0, 0, 0, 'Ahh, see! I told you it would not tarnish. Now there''s nothing linking me to that fool...$b$bWhat''s that? A plot to weaken his position? Regardless, the damage has been done. Better safe than sorry.', 23420), -- Estate Jewelry: A Braggart's Brooch
(41466, 1, 0, 0, 0, 0, 0, 0, 0, 'By the Magistrix, I had forgotten how ugly that thing is. Impeccable workmanship, obviously. Just... ugh. I should melt the whole thing down.', 23420), -- Estate Jewelry: Haute Claw-ture
(41320, 1, 0, 0, 0, 0, 0, 0, 0, 'Those wretched creatures are a scourge on our beautiful lands. At least they had good taste in their accessories.$b$bOr maybe they just like shiny things.', 23420), -- Pry It From Their Cold, Feral Claws
(41307, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent. I shall have these melted down and turned into ingots for my next batch of commissions.', 23420), -- All That Glitters
(41123, 1, 0, 0, 0, 0, 0, 0, 0, 'Oh, thank the Magistrix! I was worried it was lost forever.$b$bI see that this ring has seen some... use. I do hope it doesn''t belong to anyone important. Or... vocal.', 23420), -- An Artisan's Mark
(41215, 1, 0, 0, 0, 0, 0, 0, 0, 'Excellent work. They do not respect the hunt, so they must be reminded of their place.', 23420), -- They Become The Hunted
(41230, 1, 0, 0, 0, 0, 0, 0, 0, 'These devices are peculiar. They are like the traps our hunters use, but can magically attract and trap nearby beasts.', 23420), -- Trapping Evolved
(40617, 603, 0, 0, 0, 0, 0, 0, 0, 'Thank you, $r. I will tend to Nighteyes'' wounds. Tell me what happened!', 23420), -- Turn Around, Nighteyes
(42223, 1, 0, 0, 0, 0, 0, 0, 0, 'You could not have arrived at a better time, $r.', 23420), -- Scouting the Crimson Thicket
(44562, 0, 0, 0, 0, 0, 0, 0, 0, 'We shall help the shal''dorei find harmony.', 23420), -- Growing Strong
(42421, 0, 0, 0, 0, 0, 0, 0, 0, 'The nightfallen thank you.', 23420), -- The Nightfallen
(42422, 0, 0, 0, 0, 0, 0, 0, 0, 'The wardens thank you.', 23420), -- The Wardens
(42420, 0, 0, 0, 0, 0, 0, 0, 0, 'The Court of Farondis thanks you.', 23420), -- Court of Farondis
(39910, 1, 1, 0, 0, 0, 0, 0, 0, '<Merrus takes the idols from you and clutches them closely to his chest.>$b$bYou''ve no idea how long it''s been since I''ve held these. Thank you, $n.$b$bYou have earned my respect and as promised, you shall have my marks.', 23420), -- The Druid's Debt
(40972, 1, 0, 0, 0, 0, 0, 0, 0, 'I called them here. I will see that they are taken care of after the horrors my people put them through.', 23420), -- Last Stand of the Moon Guard
(43994, 0, 0, 0, 0, 0, 0, 0, 0, '<She tenderly plucks the crystals from your hand one by one.>', 23420), -- Feed Thalyssra
(43808, 0, 0, 0, 0, 0, 0, 0, 0, '<The beacon glows as you infuse it with Ancient Mana.>', 23420), -- Moon Guard Teleporter Online!
(40971, 1, 0, 0, 0, 0, 0, 0, 0, 'We were able to make it here safely, thanks to you.', 23420), -- Overwhelming Distraction
(40970, 2, 0, 0, 0, 0, 0, 0, 0, 'They had no right to this power. Thank you.', 23420), -- The Orchestrator of Our Demise
(40969, 0, 0, 0, 0, 0, 0, 0, 0, 'Lothrius and Thalrenus live, then. There may be some hope yet. Thank you for braving the bridge, stranger.', 23420), -- Starweaver's Fate
(41032, 5, 0, 0, 0, 0, 0, 0, 0, 'Yes! I can feel their spells receding. Soon I will be able to rejoin Thalrenus!', 23420), -- Stop the Spell Seekers
(40965, 1, 574, 0, 0, 0, 0, 0, 0, 'A lesson we all must one day learn. Never assume your enemy dead until you see their corpse. <Lothrius grins.>', 23420), -- Lay Waste, Lay Mines
(40967, 5, 0, 0, 0, 0, 0, 0, 0, 'Thank the Mother Moon... They survived!', 23420), -- Precious Little Left
(40964, 1, 0, 0, 0, 0, 0, 0, 0, 'Serena sent you? Good... she still lives.', 23420), -- The Rift Between
(40963, 1, 0, 0, 0, 0, 0, 0, 0, 'I heard screams, and I was gratified that they belonged to the Nightborne. Well done.', 23420), -- Take Them in Claw
(40883, 1, 0, 0, 0, 0, 0, 0, 0, 'I thought the Moon Guard could endure anything...', 23420), -- Fate of the Guard
(40949, 1, 0, 0, 0, 0, 0, 0, 0, 'Thank you. I feel better knowing they will not continue to sacrifice themselves for us.', 23420), -- Not Their Last Stand
(41030, 273, 0, 0, 0, 1500, 0, 0, 0, 'You were wise to keep these out of our enemies'' hands, $c. I believe you can be trusted.', 23420), -- Sigil Reclamation
(41494, 2, 0, 0, 0, 200, 0, 0, 0, 'Your assistance made this all possible. Thank you.', 23420), -- Eminent Grow-main
(41469, 273, 0, 0, 0, 100, 0, 0, 0, 'Welcome back. I see we are ready to begin.', 23420), -- Return to Irongrove Retreat
(41480, 1, 0, 0, 0, 100, 0, 0, 0, 'Such a gluttonous creature.', 23420), -- Managazer
(41485, 2, 0, 0, 0, 200, 0, 0, 0, 'I am much relieved, $n.', 23420), -- Moonwhisper Rescue
(41479, 2, 0, 0, 0, 100, 0, 0, 0, 'Well done, $r.', 23420), -- Natural Adversaries
(41478, 273, 0, 0, 0, 100, 0, 0, 0, 'Welcome, $r.', 23420), -- The Final Blessing
(41475, 589, 0, 0, 0, 200, 0, 0, 0, 'In the end, they will all thrive- for with more Sablehorn, the wolves will eat better in the years to come.', 23420), -- Prongs and Fangs
(41474, 591, 0, 0, 0, 100, 0, 0, 0, 'They are looking better fed already!', 23420), -- Fertilizing the Future
(41473, 591, 0, 0, 0, 100, 0, 0, 0, 'This fine gentleman was just telling me how dearly he loves his forest. Let us assist him!', 23420), -- Redhoof the Ancient
(41467, 2, 0, 0, 0, 200, 0, 0, 0, 'What a disturbing tale!', 23420), -- The Only Choice We Can Make
(41464, 274, 0, 0, 0, 200, 0, 0, 0, 'I am relieved, though there remains more we must do.', 23420), -- Not Here, Not Now, Not Ever
(41463, 0, 0, 0, 0, 0, 0, 0, 0, 'How good of Mayruna to send help. I had nearly lost hope.', 23420), -- Missing Along the Way
(41197, 273, 0, 0, 0, 50, 0, 0, 0, 'They look so happy to be home. Excellent work.', 23420), -- You've Got to Be Kitten Me Right Meow
(41462, 273, 0, 0, 0, 200, 0, 0, 0, 'Ahhhh... Well done, I am sure our furry friends here are very thankful for your efforts.', 23420), -- Trouble Has Huge Feet
(41453, 1, 0, 0, 0, 200, 0, 0, 0, 'Well this is an interesting turn of events.', 23420), -- Homeward Bounding
(41183, 0, 0, 0, 0, 0, 0, 0, 0, 'Hello, $n. I''m glad you came.$b$bI''ve found something that may serve both of our interests.', 23420), -- Academic Exploration
(41302, 2, 0, 0, 0, 0, 0, 0, 0, 'You couldn''t have arrived at a better time, herbalist. Our people were running short on supplies.', 23420), -- Work Order: Starlight Roses
(44719, 0, 0, 0, 0, 0, 0, 0, 0, 'I must commend your knack for overcoming adversity. Take your time gathering your fellow champions, $n, but don''t take TOO long!', 23420), -- -Unknown-
(44964, 1, 0, 0, 0, 0, 0, 0, 0, 'The preparations are nearly complete. Oculeth will be ready to teleport us soon.', 23420), -- -Unknown-
(45269, 0, 0, 0, 0, 0, 0, 0, 0, 'Very well. Let us begin.', 23420), -- -Unknown-
(45064, 1, 0, 0, 0, 0, 0, 0, 0, 'Our efforts have proven quite successful. I believe we are just about ready to make our stand.', 23420), -- -Unknown-
(45066, 5, 0, 0, 0, 0, 0, 0, 0, 'Wonderful! Now the fun begins. You should speak with Thalyssra, I believe she is ready for more... testing.', 23420), -- -Unknown-
(45065, 1, 1, 0, 0, 0, 0, 0, 0, 'I will begin analyzing this data at once.$B$BFor now, let us turn our attention to more... <Oculeth touches his fingertips together in succession.>$B$B...interesting technological advances.', 23420), -- -Unknown-
(45062, 0, 0, 0, 0, 0, 0, 0, 0, 'Oculeth will be quite pleased with your findings. As you know, he''s been working diligently on a new device for the withered.', 23420), -- -Unknown-
(45063, 0, 0, 0, 0, 0, 0, 0, 0, 'The withered seem to be adapting quite well.', 23420), -- -Unknown-
(45067, 25, 1, 0, 0, 0, 0, 0, 0, 'Let us begin.', 23420), -- -Unknown-
(44919, 0, 0, 0, 0, 0, 0, 0, 0, 'The withered... defeated so easily.$B$BWe must find a way to negate the magic of the felborne. We cannot afford to fight if our own can be turned against us.', 23420), -- -Unknown-
(44918, 0, 0, 0, 0, 0, 0, 0, 0, 'I do not know what lies in store for us, but we cannot sit idly while Ly''leth is missing.$B$BWe must act now!', 23420), -- -Unknown-
(45268, 0, 0, 0, 0, 0, 0, 0, 0, 'I am glad we can still cure my people, but... I have troubling news.', 23420), -- -Unknown-
(44833, 0, 0, 0, 0, 0, 0, 0, 0, 'Valtrois told me everything. She is hard at work on a plan for bringing down that barrier.$B$BI am afraid all we can do for now is wait. Thank you for your help.', 23420), -- -Unknown-
(44832, 0, 0, 0, 0, 0, 0, 0, 0, 'It appears Ly''leth was correct. This is an entrance to the Nightwell - and it is well protected. Nothing is impenetrable though.$b$bLet us have a look at the ley lines powering this shield, shall we?', 23420), -- -Unknown-
(45209, 0, 0, 0, 0, 0, 0, 0, 0, 'So they did intercept her messages! I hope Ly''leth is okay...$B$BWe must investigate this breach she spoke of immediately!', 23420), -- -Unknown-
(44822, 0, 0, 0, 0, 0, 0, 0, 0, 'We got lucky that time, but trying again could risk all our lives for nothing. We must find another way.', 23420), -- -Unknown-
(44736, 0, 0, 0, 0, 0, 0, 0, 0, 'Thank you for saving me. It seems we underestimated Elisande''s power.', 23420), -- -Unknown-
(45267, 0, 0, 0, 0, 0, 0, 0, 0, 'Just in time. We are ready to begin.', 23420), -- -Unknown-
(45317, 0, 0, 0, 0, 0, 0, 0, 0, 'Leadership is a difficult burden, $n.$B$BWe must often split our attention - be sure to keep the big picture in mind.', 23420), -- -Unknown-
(41318, 2, 0, 0, 0, 0, 0, 0, 0, 'You couldn''t have arrived at a better time, $c. Our people were running short on supplies.', 23420), -- -Unknown-
(43897, 1, 0, 0, 0, 0, 0, 0, 0, 'A simple exchange with complex results.  That''s the art of doing business.', 23420), -- Sealing Fate: Immense Fortune of Gold
(40795, 1, 0, 0, 0, 0, 0, 0, 0, 'An excellent choice, $ct.', 23420), -- The Fight Begins
(40793, 1, 0, 0, 0, 0, 0, 0, 0, 'Grandmaster, I''ve prepared a brief report on all that I''ve heard about the Broken Isles.$b$bYou may find it quite interesting.', 23420), -- A Matter of Planning
(40698, 603, 1, 0, 0, 0, 0, 0, 0, 'This weapon is truly amazing, yet much of its potential remains untapped.$b$bIf we are to end the threat of the Burning Legion once and for all we will need to fully unlock its power.', 23420), -- Purity of Form
(40570, 1, 603, 0, 0, 0, 200, 0, 0, 'That was a fantastic trip, $n! We really should do this kind of thing more often!$b$bNow that you have those blades I bet there''s going to be a bunch of adventures ahead!$b$bMaybe I should just stick around here a while.', 23420), -- Into The Heavens
(40634, 0, 0, 0, 0, 0, 0, 0, 0, 'So, you found this stone upon his body?$b$bInteresting... Let me take a closer look.', 23420), -- Thunder on the Sands
(40633, 1, 603, 0, 0, 0, 1000, 0, 0, 'Hey there, $n. What took you so long?$b$bWhile you were traveling the long way I''ve been doing some research of my own. King Phaoris offered to help us as long as we can take care of a problem for him.$b$bI''m sure you''ll be able to handle whatever it is.', 23420), -- Off To Adventure!
(40569, 0, 0, 0, 0, 0, 0, 0, 0, 'Do you think that the story is true? I bet you could control the power of those blades!', 23420), -- The Legend of the Sands
(40636, 1, 2, 0, 0, 0, 1000, 0, 0, 'As you wish, Grand Master.', 23420), -- Prepare To Strike
(40236, 2, 0, 0, 0, 0, 0, 0, 0, 'Congratulations, Grand Master $n!', 23420), -- The Dawning Light
(12103, 1, 603, 0, 0, 0, 1500, 0, 0, 'Welcome back to the land of the living, $n. We nearly lost you in that explosion! Lucky for us that the Wandering Isle happened to be traveling nearby.$b$bThe people here have graciously allowed us to reside at the temple. It''s not like the Peak of Serenity, but this will make a fine new home.', 23420), -- Before the Storm
(44663, 1, 1, 0, 0, 0, 0, 0, 0, 'Ah, $n, it''s good to see you on your feet! The members of the Council of Six are recuperating from their ordeal.$b$bThe intensity of the teleportation spell can be disorienting. It seems you lost consciousness for a time, but at least you didn''t awaken to find yourself stuck in a wall!$b$bDalaran has been relocated to the Broken Isles. From here, we will spearhead the effort to acquire the Pillars of Creation and drive the Legion from Azeroth once and for all!', 23420), -- In the Blink of an Eye
(40605, 1, 1, 0, 0, 0, 0, 0, 0, 'Ah, so you''re the vaunted $c I''ve heard about. Good.$b$bWe shall see if your reputation holds true.', 23420), -- Keep Your Friends Close
(40607, 1, 1, 0, 0, 0, 0, 0, 0, 'It would seem the demon hunters'' concerns were well-founded. To think the demons would dare worm their way into our city during this period of mourning!$b$bThis does not bode well for the rest of my kingdom.', 23420), -- Demons Among Us
(40760, 396, 396, 0, 0, 0, 1000, 0, 0, '<Allari furrows her brow as she speaks.>$B$BYou didn''t think the leadership of the Horde could gather in one location and go unnoticed by the Burning Legion, did you?', 23420), -- Emissary
(40522, 1, 0, 0, 0, 0, 0, 0, 0, 'You have served the Horde well in the past, $n.$b$bMore will be asked of you... much more.', 23420); -- Fate of the Horde

DELETE FROM `quest_poi` WHERE (`QuestID`=45032 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=43314 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=45047 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=44719 AND `BlobIndex`=0 AND `Idx1`=3);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(45032, 0, 2, 32, 0, 0, 1220, 1033, 0, 0, 2, 0, 0, 0, 23420), -- -Unknown-
(43314, 0, 4, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1217604, 23420), -- Beware the Fury of a Patient Elf
(45047, 0, 2, 32, 0, 0, 1220, 1018, 0, 0, 2, 0, 0, 0, 23420), -- -Unknown-
(44719, 0, 3, 32, 0, 0, 1220, 1033, 0, 0, 0, 0, 0, 1261667, 23420); -- -Unknown-

UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=287400, `QuestObjectID`=115907, `VerifiedBuild`=23420 WHERE (`QuestID`=45032 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=288963, `QuestObjectID`=115906, `Flags`=0, `WoDUnk1`=1262816, `VerifiedBuild`=23420 WHERE (`QuestID`=45032 AND `BlobIndex`=0 AND `Idx1`=0); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectID`=56918, `VerifiedBuild`=23420 WHERE (`QuestID`=44265 AND `BlobIndex`=0 AND `Idx1`=11); -- Neltharion's Lair
UPDATE `quest_poi` SET `ObjectiveIndex`=31, `QuestObjectiveID`=0, `QuestObjectID`=0, `MapID`=1220, `WorldMapAreaId`=1033, `Flags`=2, `WoDUnk1`=0, `VerifiedBuild`=23420 WHERE (`QuestID`=43314 AND `BlobIndex`=0 AND `Idx1`=3); -- Beware the Fury of a Patient Elf
UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=286296, `QuestObjectID`=140757, `VerifiedBuild`=23420 WHERE (`QuestID`=43314 AND `BlobIndex`=1 AND `Idx1`=2); -- Beware the Fury of a Patient Elf
UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=286296, `QuestObjectID`=140757, `VerifiedBuild`=23420 WHERE (`QuestID`=43314 AND `BlobIndex`=0 AND `Idx1`=1); -- Beware the Fury of a Patient Elf
UPDATE `quest_poi` SET `QuestObjectID`=57386, `VerifiedBuild`=23420 WHERE (`QuestID`=42490 AND `BlobIndex`=0 AND `Idx1`=1); -- Opening the Arcway
UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=287409, `QuestObjectID`=115907, `VerifiedBuild`=23420 WHERE (`QuestID`=45047 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectiveID`=288969, `QuestObjectID`=115906, `Flags`=0, `WoDUnk1`=1266889, `VerifiedBuild`=23420 WHERE (`QuestID`=45047 AND `BlobIndex`=0 AND `Idx1`=0); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectID`=56918, `VerifiedBuild`=23420 WHERE (`QuestID`=44265 AND `BlobIndex`=0 AND `Idx1`=11); -- Neltharion's Lair
UPDATE `quest_poi` SET `ObjectiveIndex`=2, `QuestObjectiveID`=286888, `QuestObjectID`=115112, `Flags`=2, `PlayerConditionID`=46909, `WoDUnk1`=0, `VerifiedBuild`=23420 WHERE (`QuestID`=44719 AND `BlobIndex`=0 AND `Idx1`=2); -- -Unknown-
UPDATE `quest_poi` SET `ObjectiveIndex`=1, `QuestObjectiveID`=286887, `QuestObjectID`=115111, `Flags`=0, `PlayerConditionID`=0, `WoDUnk1`=1259141, `VerifiedBuild`=23420 WHERE (`QuestID`=44719 AND `BlobIndex`=0 AND `Idx1`=1); -- -Unknown-
UPDATE `quest_poi` SET `ObjectiveIndex`=0, `QuestObjectiveID`=286833, `QuestObjectID`=115110, `Flags`=2, `WoDUnk1`=0, `VerifiedBuild`=23420 WHERE (`QuestID`=44719 AND `BlobIndex`=0 AND `Idx1`=0); -- -Unknown-
UPDATE `quest_poi` SET `QuestObjectID`=56918, `VerifiedBuild`=23420 WHERE (`QuestID`=44265 AND `BlobIndex`=0 AND `Idx1`=11); -- Neltharion's Lair
DELETE FROM `quest_poi_points` WHERE (`QuestID`=45032 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=11) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=10) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=9) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=8) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=7) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=6) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=5) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=45047 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=44719 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=11) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=10) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=9) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=1);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(45032, 2, 0, 2650, 4355, 23420), -- -Unknown-
(43314, 3, 11, 992, 3843, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 10, 981, 3854, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 9, 973, 3873, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 8, 973, 3887, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 7, 988, 3909, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 6, 999, 3913, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 5, 1014, 3906, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 4, 1040, 3887, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 3, 1040, 3880, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 2, 1036, 3850, 23420), -- Beware the Fury of a Patient Elf
(43314, 3, 1, 1025, 3836, 23420), -- Beware the Fury of a Patient Elf
(45047, 2, 0, 3082, 6989, 23420), -- -Unknown-
(44719, 3, 0, 1785, 4630, 23420), -- -Unknown-
(44719, 2, 11, 1028, 4044, 23420), -- -Unknown-
(44719, 2, 10, 991, 4081, 23420), -- -Unknown-
(44719, 2, 9, 983, 4118, 23420), -- -Unknown-
(44719, 2, 8, 983, 4143, 23420), -- -Unknown-
(44719, 2, 7, 1000, 4176, 23420), -- -Unknown-
(44719, 2, 6, 1045, 4200, 23420), -- -Unknown-
(44719, 2, 5, 1086, 4204, 23420), -- -Unknown-
(44719, 2, 4, 1131, 4204, 23420), -- -Unknown-
(44719, 2, 3, 1185, 4151, 23420), -- -Unknown-
(44719, 2, 2, 1193, 4106, 23420), -- -Unknown-
(44719, 2, 1, 1115, 4052, 23420); -- -Unknown-

UPDATE `quest_poi_points` SET `Y`=4362, `VerifiedBuild`=23420 WHERE (`QuestID`=45032 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=2653, `Y`=4370, `VerifiedBuild`=23420 WHERE (`QuestID`=45032 AND `Idx1`=0 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=1720, `Y`=4611, `VerifiedBuild`=23420 WHERE (`QuestID`=43314 AND `Idx1`=4 AND `Idx2`=0); -- Beware the Fury of a Patient Elf
UPDATE `quest_poi_points` SET `X`=1010, `Y`=3836, `VerifiedBuild`=23420 WHERE (`QuestID`=43314 AND `Idx1`=3 AND `Idx2`=0); -- Beware the Fury of a Patient Elf
UPDATE `quest_poi_points` SET `X`=3080, `Y`=7004, `VerifiedBuild`=23420 WHERE (`QuestID`=45047 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=3089, `Y`=7008, `VerifiedBuild`=23420 WHERE (`QuestID`=45047 AND `Idx1`=0 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=1065, `Y`=4040, `VerifiedBuild`=23420 WHERE (`QuestID`=44719 AND `Idx1`=2 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=1778, `Y`=4623, `VerifiedBuild`=23420 WHERE (`QuestID`=44719 AND `Idx1`=1 AND `Idx2`=0); -- -Unknown-
UPDATE `quest_poi_points` SET `X`=1781, `Y`=4628, `VerifiedBuild`=23420 WHERE (`QuestID`=44719 AND `Idx1`=0 AND `Idx2`=0); -- -Unknown-

DELETE FROM `quest_greeting` WHERE (`ID`=98002 AND `Type`=0) OR (`ID`=103175 AND `Type`=0) OR (`ID`=103129 AND `Type`=0) OR (`ID`=102425 AND `Type`=0) OR (`ID`=101767 AND `Type`=0) OR (`ID`=103571 AND `Type`=0) OR (`ID`=103569 AND `Type`=0) OR (`ID`=103568 AND `Type`=0) OR (`ID`=103570 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(98002, 0, 0, 0, '', 23420), -- 98002
(103175, 0, 0, 0, 'Gold, silver... as currency they are trivial things. As symbols of status, they reveal their true value.', 23420), -- 103175
(103129, 0, 0, 0, 'Such betrayal is unthinkable, but we must survive.', 23420), -- 103129
(102425, 0, 0, 0, 'Beakbuck won''t let them get close. He''s such a fine companion.', 23420), -- 102425
(101767, 0, 0, 0, 'They came with superior numbers. How else could they have destroyed us so?', 23420), -- 101767
(103571, 0, 1, 100, 'This place is so full of life!', 23420), -- 103571
(103569, 0, 1, 100, 'Blessing of the wilds upon you.', 23420), -- 103569
(103568, 0, 1, 200, 'Mother Moon smile upon you.', 23420), -- 103568
(103570, 0, 3, 100, 'Greetings, friend!', 23420); -- 103570


DELETE FROM `quest_details` WHERE `ID` IN (44171 /*-Unknown-*/, 43889 /*The Nighthold: Vaults*/, 43314 /*Beware the Fury of a Patient Elf*/, 42490 /*Opening the Arcway*/, 44053 /*Friends With Benefits*/, 43318 /*Ly'leth's Champion*/, 43317 /*In the Bag*/, 43313 /*Rumor Has It*/, 43315 /*Death Becomes Him*/, 41915 /*The Master's Legacy*/, 41146 /*Elegant Design*/, 41466 /*Estate Jewelry: Haute Claw-ture*/, 41465 /*Estate Jewelry: A Braggart's Brooch*/, 41320 /*Pry It From Their Cold, Feral Claws*/, 41307 /*All That Glitters*/, 41123 /*An Artisan's Mark*/, 41215 /*They Become The Hunted*/, 41230 /*Trapping Evolved*/, 40617 /*Turn Around, Nighteyes*/, 44563 /*Redemption for the Fallen*/, 43808 /*Moon Guard Teleporter Online!*/, 40972 /*Last Stand of the Moon Guard*/, 40971 /*Overwhelming Distraction*/, 40970 /*The Orchestrator of Our Demise*/, 40969 /*Starweaver's Fate*/, 41032 /*Stop the Spell Seekers*/, 40965 /*Lay Waste, Lay Mines*/, 40967 /*Precious Little Left*/, 40968 /*Recovering Stolen Power*/, 40964 /*The Rift Between*/, 40963 /*Take Them in Claw*/, 40949 /*Not Their Last Stand*/, 41030 /*Sigil Reclamation*/, 42223 /*Scouting the Crimson Thicket*/, 41494 /*Eminent Grow-main*/, 41469 /*Return to Irongrove Retreat*/, 41480 /*Managazer*/, 41485 /*Moonwhisper Rescue*/, 41479 /*Natural Adversaries*/, 41478 /*The Final Blessing*/, 41475 /*Prongs and Fangs*/, 41474 /*Fertilizing the Future*/, 41473 /*Redhoof the Ancient*/, 41467 /*The Only Choice We Can Make*/, 41464 /*Not Here, Not Now, Not Ever*/, 41462 /*Trouble Has Huge Feet*/, 41197 /*You've Got to Be Kitten Me Right Meow*/, 42323 /*Fevered Letter*/, 41184 /*Tried and True*/, 45417 /*-Unknown-*/, 44719 /*-Unknown-*/, 44964 /*-Unknown-*/, 45269 /*-Unknown-*/, 45064 /*-Unknown-*/, 45066 /*-Unknown-*/, 45062 /*-Unknown-*/, 45065 /*-Unknown-*/, 45067 /*-Unknown-*/, 45063 /*-Unknown-*/, 44919 /*-Unknown-*/, 44918 /*-Unknown-*/, 45268 /*-Unknown-*/, 44833 /*-Unknown-*/, 44832 /*-Unknown-*/, 45209 /*-Unknown-*/, 44822 /*-Unknown-*/, 44736 /*-Unknown-*/, 45267 /*-Unknown-*/, 43548 /*Into the Nightmare: Il'gynoth*/, 45382 /*-Unknown-*/, 40795 /*The Fight Begins*/, 40793 /*A Matter of Planning*/, 40698 /*Purity of Form*/, 40570 /*Into The Heavens*/, 40634 /*Thunder on the Sands*/, 40633 /*Off To Adventure!*/, 40569 /*The Legend of the Sands*/, 40636 /*Prepare To Strike*/, 40236 /*The Dawning Light*/, 12103 /*Before the Storm*/, 44663 /*In the Blink of an Eye*/, 40605 /*Keep Your Friends Close*/, 40607 /*Demons Among Us*/, 40760 /*Emissary*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(44171, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(43889, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Nighthold: Vaults
(43314, 274, 0, 0, 0, 0, 0, 0, 0, 23420), -- Beware the Fury of a Patient Elf
(42490, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Opening the Arcway
(44053, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Friends With Benefits
(43318, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Ly'leth's Champion
(43317, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- In the Bag
(43313, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Rumor Has It
(43315, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Death Becomes Him
(41915, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Master's Legacy
(41146, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Elegant Design
(41466, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Estate Jewelry: Haute Claw-ture
(41465, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Estate Jewelry: A Braggart's Brooch
(41320, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Pry It From Their Cold, Feral Claws
(41307, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- All That Glitters
(41123, 6, 0, 0, 0, 0, 0, 0, 0, 23420), -- An Artisan's Mark
(41215, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- They Become The Hunted
(41230, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Trapping Evolved
(40617, 603, 0, 0, 0, 0, 0, 0, 0, 23420), -- Turn Around, Nighteyes
(44563, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Redemption for the Fallen
(43808, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Moon Guard Teleporter Online!
(40972, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Last Stand of the Moon Guard
(40971, 1, 66, 0, 0, 0, 0, 0, 0, 23420), -- Overwhelming Distraction
(40970, 603, 1, 0, 0, 0, 0, 0, 0, 23420), -- The Orchestrator of Our Demise
(40969, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Starweaver's Fate
(41032, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Stop the Spell Seekers
(40965, 274, 1, 574, 0, 0, 0, 0, 0, 23420), -- Lay Waste, Lay Mines
(40967, 1, 51, 0, 0, 0, 0, 0, 0, 23420), -- Precious Little Left
(40968, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Recovering Stolen Power
(40964, 1, 20, 603, 0, 0, 1000, 2000, 0, 23420), -- The Rift Between
(40963, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Take Them in Claw
(40949, 274, 0, 0, 0, 0, 0, 0, 0, 23420), -- Not Their Last Stand
(41030, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Sigil Reclamation
(42223, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Scouting the Crimson Thicket
(41494, 273, 0, 0, 0, 200, 0, 0, 0, 23420), -- Eminent Grow-main
(41469, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Return to Irongrove Retreat
(41480, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Managazer
(41485, 274, 0, 0, 0, 200, 0, 0, 0, 23420), -- Moonwhisper Rescue
(41479, 1, 0, 0, 0, 100, 0, 0, 0, 23420), -- Natural Adversaries
(41478, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Final Blessing
(41475, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Prongs and Fangs
(41474, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Fertilizing the Future
(41473, 1, 0, 0, 0, 200, 0, 0, 0, 23420), -- Redhoof the Ancient
(41467, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Only Choice We Can Make
(41464, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Not Here, Not Now, Not Ever
(41462, 1, 0, 0, 0, 200, 0, 0, 0, 23420), -- Trouble Has Huge Feet
(41197, 1, 0, 0, 0, 200, 0, 0, 0, 23420), -- You've Got to Be Kitten Me Right Meow
(42323, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Fevered Letter
(41184, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Tried and True
(45417, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44719, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44964, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45269, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45064, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45066, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45062, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45065, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45067, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45063, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44919, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44918, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45268, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44833, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44832, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45209, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44822, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(44736, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(45267, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- -Unknown-
(43548, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Into the Nightmare: Il'gynoth
(45382, 1, 0, 0, 0, 50, 0, 0, 0, 23420), -- -Unknown-
(40795, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Fight Begins
(40793, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- A Matter of Planning
(40698, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- Purity of Form
(40570, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Into The Heavens
(40634, 1, 1, 0, 0, 0, 1000, 0, 0, 23420), -- Thunder on the Sands
(40633, 1, 18, 20, 0, 0, 1000, 2000, 0, 23420), -- Off To Adventure!
(40569, 378, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Legend of the Sands
(40636, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Prepare To Strike
(40236, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- The Dawning Light
(12103, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Before the Storm
(44663, 1, 1, 0, 0, 0, 500, 0, 0, 23420), -- In the Blink of an Eye
(40605, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- Keep Your Friends Close
(40607, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- Demons Among Us
(40760, 1, 1, 0, 0, 0, 1000, 0, 0, 23420); -- Emissary

DELETE FROM `quest_request_items` WHERE `ID` IN (43889 /*The Nighthold: Vaults*/, 43896 /*Sealing Fate: Piles of Gold*/, 43895 /*Sealing Fate: Gold*/, 43892 /*Sealing Fate: Order Resources*/, 43318 /*Ly'leth's Champion*/, 43317 /*In the Bag*/, 41915 /*The Master's Legacy*/, 41465 /*Estate Jewelry: A Braggart's Brooch*/, 41466 /*Estate Jewelry: Haute Claw-ture*/, 41320 /*Pry It From Their Cold, Feral Claws*/, 41307 /*All That Glitters*/, 41123 /*An Artisan's Mark*/, 41230 /*Trapping Evolved*/, 40617 /*Turn Around, Nighteyes*/, 44563 /*Redemption for the Fallen*/, 39910 /*The Druid's Debt*/, 42421 /*The Nightfallen*/, 43994 /*Feed Thalyssra*/, 43808 /*Moon Guard Teleporter Online!*/, 40970 /*The Orchestrator of Our Demise*/, 40965 /*Lay Waste, Lay Mines*/, 40967 /*Precious Little Left*/, 41030 /*Sigil Reclamation*/, 41317 /*-Unknown-*/, 41302 /*Work Order: Starlight Roses*/, 41318 /*-Unknown-*/,
43897 /*Sealing Fate: Immense Fortune of Gold*/, 37257 /*Our Very Bones*/, 37728 /*Presentation is Everything*/, 37727 /*The Magister of Mixology*/, 37256 /*They Came From the Sea*/, 37959 /*The Hunger Returns*/, 37859 /*The Consumed*/, 42271 /*Their Dying Breaths*/, 37450 /*Saving Stellagosa*/, 40634 /*Thunder on the Sands*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(43889, 0, 0, 0, 0, 'How are you finding the challenges of the The Nighthold?', 23420), -- The Nighthold: Vaults
(43896, 1, 0, 0, 0, 'Twice the cost of the last, but maybe that''s worth the price. Or, perhaps a different currency would be easier for you?  I leave the choice up to you.\n\nRemember, you can only obtain three seals per week.  This counts as one of those three.  Next week, I will have others.', 23420), -- Sealing Fate: Piles of Gold
(43895, 1, 0, 0, 0, 'These seals provide an additional chance at treasure in dungeons and raids, but at what cost? Gold is indeed treasure, but additional treasure for gold?  That is the offer I present.\n\nRemember, you can only obtain three seals per week.  This counts as one of those three.  Next week, I will have others.', 23420), -- Sealing Fate: Gold
(43892, 1, 0, 0, 0, 'These seals provide an additional chance at treasure in dungeons and raids, but at what cost? Your order will certainly not miss these extra resources, but I must ask: When does it end?  Will you ever stop risking others lives just to satisfy your greedy nature?\n\nYou can only obtain three seals per week.  This counts as one of those three.  Next week, I will have others.', 23420), -- Sealing Fate: Order Resources
(43318, 0, 0, 0, 0, 'I know you will defend my honor, $n.', 23420), -- Ly'leth's Champion
(43317, 0, 0, 0, 0, 'I am prepared. It is nearly time.', 23420), -- In the Bag
(41915, 6, 0, 0, 0, 'Where is the master?', 23420), -- The Master's Legacy
(41465, 1, 0, 0, 0, 'That oaf will undo years of hard work. It is so much easier to fall than it is to climb...', 23420), -- Estate Jewelry: A Braggart's Brooch
(41466, 1, 0, 0, 0, 'He was so passionate, so driven. I saw his vision, created it to his exact specifications. Beautiful filigree and enamel put to a fearsome weapon...', 23420), -- Estate Jewelry: Haute Claw-ture
(41320, 6, 0, 0, 0, 'Do you have my necklaces? They are of no use to me on the necks of those... creatures.', 23420), -- Pry It From Their Cold, Feral Claws
(41307, 6, 0, 0, 0, 'Can you imagine, a Lespin original hiding under a pile of leaves? What a waste.', 23420), -- All That Glitters
(41123, 6, 0, 0, 0, 'Do you have it?', 23420), -- An Artisan's Mark
(41230, 6, 0, 0, 0, 'Do you have the snares, $r?', 23420), -- Trapping Evolved
(40617, 603, 0, 0, 0, 'You must find Nighteyes!', 23420), -- Turn Around, Nighteyes
(44563, 0, 0, 0, 0, 'There is much to be done.', 23420), -- Redemption for the Fallen
(39910, 6, 0, 0, 0, 'Have you found them? Are they still intact?', 23420), -- The Druid's Debt
(42421, 0, 0, 0, 0, 'Assist us, and we will reward you.', 23420), -- The Nightfallen
(43994, 0, 0, 0, 0, '<Thalyssra is shaking uncontrollably and can barely speak.>', 23420), -- Feed Thalyssra
(43808, 0, 0, 0, 0, '<The beacon is cold and dark.>', 23420), -- Moon Guard Teleporter Online!
(40970, 0, 0, 0, 0, 'Is it done?', 23420), -- The Orchestrator of Our Demise
(40965, 0, 0, 0, 0, 'How fares the education?', 23420), -- Lay Waste, Lay Mines
(40967, 0, 0, 0, 0, 'Did you locate them?', 23420), -- Precious Little Left
(41030, 0, 0, 0, 0, 'What is this?', 23420), -- Sigil Reclamation
(41317, 0, 0, 0, 0, 'Yes?', 23420), -- -Unknown-
(41302, 0, 0, 0, 0, 'Yes, outlander?', 23420), -- Work Order: Starlight Roses
(41318, 0, 0, 0, 0, 'Yes, outlander?', 23420), -- -Unknown-
(43897, 1, 0, 0, 0, 'Four times the cost of the first, but for only one seal.  Remember, I still offer more efficient options, but I never refuse someone who is insistent on paying me more. I leave you the choice.\n\nRemember, you can only obtain three seals per week.  This counts as one of those three.  Next week, I will have others.', 23420), -- Sealing Fate: Immense Fortune of Gold
(37257, 0, 0, 0, 0, 'My people may not care much for me, but that will not stop me from caring for them.', 23420), -- Our Very Bones
(37728, 6, 0, 0, 0, 'Yes? You have the eyes?', 23420), -- Presentation is Everything
(37727, 6, 0, 0, 0, 'Are we speaking again because you have my eggs?', 23420), -- The Magister of Mixology
(37256, 0, 0, 0, 0, '<The nightwatcher looks at you expectantly.>', 23420), -- They Came From the Sea
(37959, 0, 0, 0, 0, 'Do you have them? Please tell me you have them, my friend.', 23420), -- The Hunger Returns
(37859, 0, 0, 0, 0, 'Clever $r. Yes, a ley crystal should bring them back to their senses.', 23420), -- The Consumed
(42271, 0, 0, 0, 0, 'Oh no...', 23420), -- Their Dying Breaths
(37450, 1, 0, 0, 0, 'If there''s a remnant of the Blue Dragonflight nearby, we should do whatever we can to ally ourselves with them.', 23420), -- Saving Stellagosa
(40634, 0, 0, 0, 0, 'Have you found anything, $n?', 23420); -- Thunder on the Sands

DELETE FROM `quest_template` WHERE `ID`=45383;
INSERT INTO `quest_template` (`ID`, `QuestType`, `QuestLevel`, `QuestPackageID`, `MinLevel`, `QuestSortID`, `QuestInfoID`, `SuggestedGroupNum`, `RewardNextQuest`, `RewardXPDifficulty`, `RewardXPMultiplier`, `RewardMoney`, `RewardMoneyDifficulty`, `RewardMoneyMultiplier`, `RewardBonusMoney`, `RewardDisplaySpell1`, `RewardDisplaySpell2`, `RewardDisplaySpell3`, `RewardSpell`, `RewardHonor`, `RewardKillHonor`, `StartItem`, `RewardArtifactXPDifficulty`, `RewardArtifactXPMultiplier`, `RewardArtifactCategoryID`, `Flags`, `FlagsEx`, `RewardSkillLineID`, `RewardNumSkillUps`, `PortraitGiver`, `PortraitTurnIn`, `RewardItem1`, `RewardItem2`, `RewardItem3`, `RewardItem4`, `RewardAmount1`, `RewardAmount2`, `RewardAmount3`, `RewardAmount4`, `ItemDrop1`, `ItemDrop2`, `ItemDrop3`, `ItemDrop4`, `ItemDropQuantity1`, `ItemDropQuantity2`, `ItemDropQuantity3`, `ItemDropQuantity4`, `RewardChoiceItemID1`, `RewardChoiceItemID2`, `RewardChoiceItemID3`, `RewardChoiceItemID4`, `RewardChoiceItemID5`, `RewardChoiceItemID6`, `RewardChoiceItemQuantity1`, `RewardChoiceItemQuantity2`, `RewardChoiceItemQuantity3`, `RewardChoiceItemQuantity4`, `RewardChoiceItemQuantity5`, `RewardChoiceItemQuantity6`, `RewardChoiceItemDisplayID1`, `RewardChoiceItemDisplayID2`, `RewardChoiceItemDisplayID3`, `RewardChoiceItemDisplayID4`, `RewardChoiceItemDisplayID5`, `RewardChoiceItemDisplayID6`, `POIContinent`, `POIx`, `POIy`, `POIPriority`, `RewardTitle`, `RewardArenaPoints`, `RewardFactionID1`, `RewardFactionID2`, `RewardFactionID3`, `RewardFactionID4`, `RewardFactionID5`, `RewardFactionValue1`, `RewardFactionValue2`, `RewardFactionValue3`, `RewardFactionValue4`, `RewardFactionValue5`, `RewardFactionCapIn1`, `RewardFactionCapIn2`, `RewardFactionCapIn3`, `RewardFactionCapIn4`, `RewardFactionCapIn5`, `RewardFactionOverride1`, `RewardFactionOverride2`, `RewardFactionOverride3`, `RewardFactionOverride4`, `RewardFactionOverride5`, `RewardFactionFlags`, `AreaGroupID`, `TimeAllowed`, `AllowableRaces`, `QuestRewardID`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `QuestCompletionLog`, `RewardCurrencyID1`, `RewardCurrencyID2`, `RewardCurrencyID3`, `RewardCurrencyID4`, `RewardCurrencyQty1`, `RewardCurrencyQty2`, `RewardCurrencyQty3`, `RewardCurrencyQty4`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `AcceptedSoundKitID`, `CompleteSoundKitID`, `VerifiedBuild`) VALUES
(45383, 2, 110, 0, 110, 8025, 62, 0, 0, 6, 1, 388000, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33554752, 0, 0, 0, 0, 0, 141326, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 4773, 0, -1, 0, 'Talisman of the Shal''dorei', 'Collect 4 Echoes of Time from Elisande in The Nighthold on Mythic Difficulty.', 'When the Nighthold was built, my people forged a talisman that allowed access to these halls. With the talisman''s power, one could traverse the palace unhindered, taking the fight directly to the magistrix. Using fragments of the Nightwell''s power, we could locate the talisman and end this foolish war...', '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', 890, 878, 23420); -- -Unknown-

UPDATE `quest_template` SET `RewardSpell`=237602, `Flags`=2184773888, `RewardFactionValue1`=3, `RewardFactionValue2`=3, `RewardFactionOverride1`=0, `RewardFactionOverride2`=0, `VerifiedBuild`=23420 WHERE `ID`=45072; -- -Unknown-
UPDATE `quest_template` SET `RewardSpell`=237602, `Flags`=2184773888, `RewardFactionValue1`=3, `RewardFactionValue2`=3, `RewardFactionOverride1`=0, `RewardFactionOverride2`=0, `VerifiedBuild`=23420 WHERE `ID`=45071; -- -Unknown-
UPDATE `quest_template` SET `Flags`=34603008, `VerifiedBuild`=23420 WHERE `ID`=45111; -- -Unknown-
UPDATE `quest_template` SET `RewardSpell`=237602, `Flags`=2184773888, `RewardFactionValue1`=3, `RewardFactionValue2`=3, `RewardFactionOverride1`=0, `RewardFactionOverride2`=0, `VerifiedBuild`=23420 WHERE `ID`=45072; -- -Unknown-
UPDATE `quest_template` SET `RewardSpell`=237602, `Flags`=2184773888, `RewardFactionValue1`=3, `RewardFactionValue2`=3, `RewardFactionOverride1`=0, `RewardFactionOverride2`=0, `VerifiedBuild`=23420 WHERE `ID`=45072; -- -Unknown-
UPDATE `quest_template` SET `RewardMoney`=158000, `VerifiedBuild`=23420 WHERE `ID`=37496; -- Infiltrating Shipwreck Arena
UPDATE `quest_template` SET `RewardMoney`=158000, `VerifiedBuild`=23420 WHERE `ID`=37497; -- Trailing the Tidestone
UPDATE `quest_template` SET `RewardMoney`=316000, `VerifiedBuild`=23420 WHERE `ID`=37728; -- Presentation is Everything
UPDATE `quest_template` SET `RewardMoney`=158000, `VerifiedBuild`=23420 WHERE `ID`=37727; -- The Magister of Mixology
UPDATE `quest_template` SET `RewardMoney`=158000, `VerifiedBuild`=23420 WHERE `ID`=37492; -- A Rather Long Walk
UPDATE `quest_template` SET `RewardMoney`=158000, `VerifiedBuild`=23420 WHERE `ID`=37257; -- Our Very Bones
UPDATE `quest_template` SET `RewardMoney`=15800, `VerifiedBuild`=23420 WHERE `ID`=37733; -- Prince Farondis
UPDATE `quest_template` SET `RewardMoney`=158000, `VerifiedBuild`=23420 WHERE `ID`=37256; -- They Came From the Sea
UPDATE `quest_template` SET `RewardMoney`=474000, `VerifiedBuild`=23420 WHERE `ID`=42756; -- Hunger's End
UPDATE `quest_template` SET `RewardMoney`=158000, `VerifiedBuild`=23420 WHERE `ID`=42187; -- Rise, Champions
UPDATE `quest_template` SET `RewardMoney`=15800, `VerifiedBuild`=23420 WHERE `ID`=42567; -- Cursed to Wither
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=38014; -- Feasting on the Dragon
UPDATE `quest_template` SET `RewardMoney`=77000, `VerifiedBuild`=23420 WHERE `ID`=37862; -- Still Alive
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37860; -- You Scratch My Back...
UPDATE `quest_template` SET `RewardMoney`=462000, `VerifiedBuild`=23420 WHERE `ID`=37861; -- The Nightborne Prince
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37959; -- The Hunger Returns
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37960; -- Leyline Abuse
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37857; -- Runas Knows the Way
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37957; -- Runas the Shamed
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37859; -- The Consumed
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37856; -- The Withered
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=37855; -- The Last of the Last
UPDATE `quest_template` SET `RewardMoney`=77000, `VerifiedBuild`=23420 WHERE `ID`=42271; -- Their Dying Breaths
UPDATE `quest_template` SET `RewardMoney`=77000, `VerifiedBuild`=23420 WHERE `ID`=37991; -- Agapanthus
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37853; -- The Death of the Eldest
UPDATE `quest_template` SET `RewardMoney`=308000, `VerifiedBuild`=23420 WHERE `ID`=37449; -- Dark Revelations
UPDATE `quest_template` SET `RewardMoney`=308000, `VerifiedBuild`=23420 WHERE `ID`=37450; -- Saving Stellagosa
UPDATE `quest_template` SET `RewardMoney`=308000, `VerifiedBuild`=23420 WHERE `ID`=37656; -- Fel Machinations
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=36920; -- From Within
UPDATE `quest_template` SET `RewardMoney`=308000, `VerifiedBuild`=23420 WHERE `ID`=37660; -- The Scythe of Souls
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37658; -- Reignite the Wards
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=37653; -- Demon Souls
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=39733; -- The Lone Mountain
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=39864; -- Stormheim
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=39731; -- The Tranquil Forest
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=39718; -- Paradise Lost
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=40795; -- The Fight Begins
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=40793; -- A Matter of Planning
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=40698; -- Purity of Form
UPDATE `quest_template` SET `RewardMoney`=462000, `VerifiedBuild`=23420 WHERE `ID`=40570; -- Into The Heavens
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=40634; -- Thunder on the Sands
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=40633; -- Off To Adventure!
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=40569; -- The Legend of the Sands
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=40636; -- Prepare To Strike
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=40236; -- The Dawning Light
UPDATE `quest_template` SET `RewardMoney`=154000, `VerifiedBuild`=23420 WHERE `ID`=12103; -- Before the Storm
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=40122; -- Cenarius, Keeper of the Grove
UPDATE `quest_template` SET `RewardMoney`=15400, `VerifiedBuild`=23420 WHERE `ID`=44701; -- Stormheim

DELETE FROM `quest_objectives` WHERE `ID`=287893;
INSERT INTO `quest_objectives` (`ID`, `QuestID`, `Type`, `StorageIndex`, `ObjectID`, `Amount`, `Flags`, `Flags2`, `ProgressBarWeight`, `Description`, `VerifiedBuild`) VALUES
(287893, 45383, 1, 0, 143658, 4, 0, 1, 0, '', 23420); -- 287893

UPDATE `quest_objectives` SET `Type`=14, `ObjectID`=57394, `VerifiedBuild`=23420 WHERE `ID`=285584; -- 285584
UPDATE `quest_objectives` SET `Type`=14, `ObjectID`=57374, `VerifiedBuild`=23420 WHERE `ID`=284723; -- 284723
UPDATE `quest_objectives` SET `Type`=14, `ObjectID`=57374, `VerifiedBuild`=23420 WHERE `ID`=284723; -- 284723
UPDATE `quest_objectives` SET `Type`=14, `ObjectID`=57374, `VerifiedBuild`=23420 WHERE `ID`=284723; -- 284723
