# TrinityCore - WowPacketParser
# File name: FusionV7_1_0_23171deDE.pkt
# Detected build: V7_1_0_23171
# Detected locale: deDE
# Targeted database: Legion
# Parsing date: 02/03/2017 16:46:27

SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=42421 AND `locale`='deDE') OR (`ID`=42422 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(42421, 'deDE', 'Die Nachtsüchtigen', 'Schließt beliebige 4 Weltquests in Suramar ab.', 'Unterstützt die Nachtsüchtigen von Suramar, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zur Ersten Arkanistin Thalyssra in Suramar zurück.', 23171),
(42422, 'deDE', 'Die Wächterinnen', 'Schließt 4 Weltquests der Wächterinnen ab.', 'Unterstützt die Wächterinnen, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Marin Klingenflügel auf der Insel der Behüter zurück.', 23171);

UPDATE `quest_template_locale` SET `QuestCompletionLog`='', `VerifiedBuild`=23171 WHERE (`ID`=8147 AND `locale`='deDE');

SET NAMES 'latin1';
SET NAMES 'utf8';
DELETE FROM `quest_objectives_locale` WHERE (`ID`=284171 AND `locale`='deDE') OR (`ID`=284172 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(284171, 'deDE', 42421, 0, 'Schließt 4 Weltquests in Suramar ab', 23171),
(284172, 'deDE', 42422, 0, 'Schließt 4 Weltquests der Wächterinnen ab', 23171);


SET NAMES 'latin1';
