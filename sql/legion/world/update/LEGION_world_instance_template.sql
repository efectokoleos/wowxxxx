UPDATE `instance_template` SET `parent`=1220, `script`='instance_assault_on_violet_hold', `allowMount`=0 WHERE `map`=1544;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_black_rook_hold',        `allowMount`=0 WHERE `map`=1501;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_darkheart_thicket',      `allowMount`=1 WHERE `map`=1466;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_eye_of_azshara',         `allowMount`=1 WHERE `map`=1456;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_halls_of_valor',         `allowMount`=1 WHERE `map`=1477;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_maw_of_souls',           `allowMount`=1 WHERE `map`=1492;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_neltharions_lair',       `allowMount`=1 WHERE `map`=1458;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_the_arcway',             `allowMount`=0 WHERE `map`=1516;
UPDATE `instance_template` SET `parent`=1220, `script`='instance_vault_of_the_wardens',   `allowMount`=0 WHERE `map`=1493;

DELETE FROM `instance_template` WHERE `map` IN (1571, 1520, 1530);
INSERT INTO `instance_template` (`map`, `parent`, `script`, `allowMount`) VALUES
(1571, 1220, 'instance_court_of_stars',         0),
(1520, 1220, 'instance_the_emerald_nightmare',  1),
(1530, 1220, 'instance_the_nighthold',          0);

DELETE FROM `instance_template` WHERE `map` IN (1462, 1475, 1498, 1533, 1554);
