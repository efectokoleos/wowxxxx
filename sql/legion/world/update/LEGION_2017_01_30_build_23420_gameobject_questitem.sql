DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=266117 AND `Idx`=0) OR (`GameObjectEntry`=266118 AND `Idx`=0) OR (`GameObjectEntry`=266120 AND `Idx`=0) OR (`GameObjectEntry`=266123 AND `Idx`=0) OR (`GameObjectEntry`=266122 AND `Idx`=0) OR (`GameObjectEntry`=266119 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(266117, 0, 142258, 23420), -- The Rook's Gambit
(266118, 0, 142258, 23420), -- The Raven's Prey
(266120, 0, 142258, 23420), -- Zandalari - The True Enemy
(266123, 0, 142258, 23420), -- Spellbreaking, the Art of Shieldbashing
(266122, 0, 142258, 23420), -- Voodoo, Geomancy, and the Magic of Peasants
(266119, 0, 142258, 23420); -- Jungle Warfare: The Troll Problem
