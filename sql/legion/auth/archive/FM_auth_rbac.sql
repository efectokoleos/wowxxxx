
TRUNCATE `rbac_security_level_groups`;
TRUNCATE `rbac_group_roles`;
TRUNCATE `rbac_role_permissions`;
DELETE FROM `rbac_groups`;
DELETE FROM `rbac_roles`;
DELETE FROM `rbac_permissions`;

INSERT INTO `rbac_permissions` (`id`, `name`) VALUES
(1,  'Instant logout'),
(2,  'Skip Queue'),
(3,  'Join Normal Battleground'),
(4,  'Join Random Battleground'),
(5,  'Join Arenas'),
(6,  'Join Dungeon Finder'),
(7,  'Player Commands (Backward Compatibility)'),
-- 8
-- 9
(10, 'Use character templates'),
(11, 'Log GM trades'),
(12, 'Skip character creation demon hunter min level check'),
(13, 'Skip Instance required bosses check'),
(14, 'Skip character creation team mask check'),
(15, 'Skip character creation class mask check'),
(16, 'Skip character creation race mask check'),
(17, 'Skip character creation reserved name check'),
(18, 'Skip character creation death knight min level check'),
(19, 'Skip needed requirements to use channel check'),
(20, 'Skip disable map check'),
(21, 'Skip reset talents when used more than allowed check'),
(22, 'Skip spam chat check'),
(23, 'Skip over-speed ping check'),
(24, 'Two side faction characters on the same account'),
(25, 'Allow say chat between factions'),
(26, 'Allow channel chat between factions'),
(27, 'Two side mail interaction'),
(28, 'See two side who list'),
(29, 'Add friends of other faction'),
(30, 'Save character without delay with .save command'),
(31, 'Use params with .unstuck command'),
(32, 'Can be assigned tickets with .assign ticket command'),
(33, 'Notify if a command was not found'),
(34, 'Check if should appear in list using .gm ingame command'),
(35, 'See all security levels with who command'),
(36, 'Filter whispers'),
(37, 'Use staff badge in chat'),
(38, 'Resurrect with full Health Points'),
(39, 'Restore saved gm setting states'),
(40, 'Allows to add a gm to friend list'),
(41, 'Use Config option START_GM_LEVEL to assign new character level'),
(42, 'Allows to use CMSG_WORLD_TELEPORT opcode'),
(43, 'Allows to use CMSG_WHOIS opcode'),
(44, 'Receive global GM messages/texts'),
(45, 'Join channels without announce'),
(46, 'Change channel settings without being channel moderator'),
(47, 'Enables lower security than target check'),
(48, 'Enable IP, Last Login and EMail output in pinfo'),
(49, 'Forces to enter the email for confirmation on password change'),
(50, 'Allow user to check his own email with .account'),
(51, 'Allow trading between factions'),

(200,'Command: rbac'),
-- 201 - 206
(207,'Command: bnetaccount'),
(208,'Command: bnetaccount create'),
(209,'Command: bnetaccount lock country'),
(210,'Command: bnetaccount lock ip'),
(211,'Command: bnetaccount password'),
(212,'Command: bnetaccount set'),
(213,'Command: bnetaccount set password'),
(214,'Command: bnetaccount link'),
(215,'Command: bnetaccount unlink'),
(216,'Command: bnetaccount gameaccountcreate'),
(217,'Command: account'),
(218,'Command: account addon'),
(219,'Command: account create'),
(220,'Command: account delete'),
(221,'Command: account lock'),
(222,'Command: account lock country'),
(223,'Command: account lock ip'),
(224,'Command: account onlinelist'),
(225,'Command: account password'),
(226,'Command: account set'),
(227,'Command: account set addon'),
(228,'Command: account set gmlevel'),
(229,'Command: account set password'),
(230,'Command: achievement'),
(231,'Command: achievement add'),
(232,'Command: arena'),
(233,'Command: arena captain'),
(234,'Command: arena create'),
(235,'Command: arena disband'),
(236,'Command: arena info'),
(237,'Command: arena lookup'),
(238,'Command: arena rename'),
(239,'Command: ban'),
(240,'Command: ban account'),
(241,'Command: ban character'),
(242,'Command: ban ip'),
(243,'Command: ban playeraccount'),
(244,'Command: baninfo'),
(245,'Command: baninfo account'),
(246,'Command: baninfo character'),
(247,'Command: baninfo ip'),
(248,'Command: banlist'),
(249,'Command: banlist account'),
(250,'Command: banlist character'),
(251,'Command: banlist ip'),
(252,'Command: unban'),
(253,'Command: unban account'),
(254,'Command: unban character'),
(255,'Command: unban ip'),
(256,'Command: unban playeraccount'),
(257,'Command: bf'),
(258,'Command: bf start'),
(259,'Command: bf stop'),
(260,'Command: bf switch'),
(261,'Command: bf timer'),
(262,'Command: bf enable'),
(263,'Command: account email'),
(264,'Command: account set sec'),
(265,'Command: account set sec email'),
(266,'Command: account set sec regmail'),
(267,'Command: cast'),
(268,'Command: cast back'),
(269,'Command: cast dist'),
(270,'Command: cast self'),
(271,'Command: cast target'),
(272,'Command: cast dest'),
(273,'Command: character'),
(274,'Command: character customize'),
(275,'Command: character changefaction'),
(276,'Command: character changerace'),
(277,'Command: character deleted'),
-- 278
(279,'Command: character deleted list'),
(280,'Command: character deleted restore'),
-- 281
-- 282
(283,'Command: character level'),
(284,'Command: character rename'),
(285,'Command: character reputation'),
(286,'Command: character titles'),
(287,'Command: levelup'),
(288,'Command: pdump'),
(289,'Command: pdump load'),
(290,'Command: pdump write'),
(291,'Command: cheat'),
(292,'Command: cheat casttime'),
(293,'Command: cheat cooldown'),
(294,'Command: cheat explore'),
(295,'Command: cheat god'),
(296,'Command: cheat power'),
(297,'Command: cheat status'),
(298,'Command: cheat taxi'),
(299,'Command: cheat waterwalk'),
(300,'Command: debug'),
(301,'Command: debug anim'),
(302,'Command: debug areatriggers'),
(303,'Command: debug arena'),
(304,'Command: debug bg'),
(305,'Command: debug entervehicle'),
(306,'Command: debug getitemstate'),
(307,'Command: debug getitemvalue'),
(308,'Command: debug getvalue'),
(309,'Command: debug hostil'),
(310,'Command: debug itemexpire'),
(311,'Command: debug lootrecipient'),
(312,'Command: debug los'),
(313,'Command: debug mod32value'),
(314,'Command: debug moveflags'),
(315,'Command: debug play'),
(316,'Command: debug play cinematics'),
(317,'Command: debug play movie'),
(318,'Command: debug play sound'),
(319,'Command: debug send'),
(320,'Command: debug send buyerror'),
(321,'Command: debug send channelnotify'),
(322,'Command: debug send chatmessage'),
(323,'Command: debug send equiperror'),
(324,'Command: debug send largepacket'),
(325,'Command: debug send opcode'),
(326,'Command: debug send qinvalidmsg'),
(327,'Command: debug send qpartymsg'),
(328,'Command: debug send sellerror'),
(329,'Command: debug send setphaseshift'),
(330,'Command: debug send spellfail'),
(331,'Command: debug setaurastate'),
(332,'Command: debug setbit'),
(333,'Command: debug setitemvalue'),
(334,'Command: debug setvalue'),
(335,'Command: debug setvid'),
(336,'Command: debug spawnvehicle'),
(337,'Command: debug threat'),
(338,'Command: debug update'),
(339,'Command: debug uws'),
(340,'Command: wpgps'),
(341,'Command: deserter'),
(342,'Command: deserter bg'),
(343,'Command: deserter bg add'),
(344,'Command: deserter bg remove'),
(345,'Command: deserter instance'),
(346,'Command: deserter instance add'),
(347,'Command: deserter instance remove'),
(348,'Command: disable'),
(349,'Command: disable add'),
(350,'Command: disable add achievement_criteria'),
(351,'Command: disable add battleground'),
(352,'Command: disable add map'),
(353,'Command: disable add mmap'),
(354,'Command: disable add outdoorpvp'),
(355,'Command: disable add quest'),
(356,'Command: disable add spell'),
(357,'Command: disable add vmap'),
(358,'Command: disable remove'),
(359,'Command: disable remove achievement_criteria'),
(360,'Command: disable remove battleground'),
(361,'Command: disable remove map'),
(362,'Command: disable remove mmap'),
(363,'Command: disable remove outdoorpvp'),
(364,'Command: disable remove quest'),
(365,'Command: disable remove spell'),
(366,'Command: disable remove vmap'),
(367,'Command: event info'),
(368,'Command: event activelist'),
(369,'Command: event start'),
(370,'Command: event stop'),
(371,'Command: gm'),
(372,'Command: gm chat'),
(373,'Command: gm fly'),
(374,'Command: gm ingame'),
(375,'Command: gm list'),
(376,'Command: gm visible'),
(377,'Command: go'),
(378,'Command: go creature'),
(379,'Command: go graveyard'),
(380,'Command: go grid'),
(381,'Command: go object'),
(382,'Command: go taxinode'),
(383,'Command: go ticket'),
(384,'Command: go trigger'),
(385,'Command: go xyz'),
(386,'Command: go zonexy'),
(387,'Command: gobject'),
(388,'Command: gobject activate'),
(389,'Command: gobject add'),
(390,'Command: gobject add temp'),
(391,'Command: gobject delete'),
(392,'Command: gobject info'),
(393,'Command: gobject move'),
(394,'Command: gobject near'),
(395,'Command: gobject set'),
(396,'Command: gobject set phase'),
(397,'Command: gobject set state'),
(398,'Command: gobject target'),
(399,'Command: gobject turn'),
(400,'Command: debug transport'),
(401,'Command: guild'),
(402,'Command: guild create'),
(403,'Command: guild delete'),
(404,'Command: guild invite'),
(405,'Command: guild uninvite'),
(406,'Command: guild rank'),
(407,'Command: guild rename'),
(408,'Command: honor'),
(409,'Command: honor add'),
(410,'Command: honor add kill'),
(411,'Command: honor update'),
(412,'Command: instance'),
(413,'Command: instance listbinds'),
(414,'Command: instance unbind'),
(415,'Command: instance stats'),
(416,'Command: instance savedata'),
(417,'Command: learn'),
(418,'Command: learn all'),
(419,'Command: learn all my'),
(420,'Command: learn all my class'),
(421,'Command: learn all my pettalents'),
(422,'Command: learn all my spells'),
(423,'Command: learn all my talents'),
(424,'Command: learn all gm'),
(425,'Command: learn all crafts'),
(426,'Command: learn all default'),
(427,'Command: learn all lang'),
(428,'Command: learn all recipes'),
(429,'Command: unlearn'),
(430,'Command: lfg'),
(431,'Command: lfg player'),
(432,'Command: lfg group'),
(433,'Command: lfg queue'),
(434,'Command: lfg clean'),
(435,'Command: lfg options'),
(436,'Command: list'),
(437,'Command: list creature'),
(438,'Command: list item'),
(439,'Command: list object'),
(440,'Command: list auras'),
(441,'Command: list mail'),
(442,'Command: lookup'),
(443,'Command: lookup area'),
(444,'Command: lookup creature'),
(445,'Command: lookup event'),
(446,'Command: lookup faction'),
(447,'Command: lookup item'),
(448,'Command: lookup itemset'),
(449,'Command: lookup object'),
(450,'Command: lookup quest'),
(451,'Command: lookup player'),
(452,'Command: lookup player ip'),
(453,'Command: lookup player account'),
(454,'Command: lookup player email'),
(455,'Command: lookup skill'),
(456,'Command: lookup spell'),
(457,'Command: lookup spell id'),
(458,'Command: lookup taxinode'),
(459,'Command: lookup tele'),
(460,'Command: lookup title'),
(461,'Command: lookup map'),
(462,'Command: announce'),
(463,'Command: channel'),
(464,'Command: channel set'),
(465,'Command: channel set ownership'),
(466,'Command: gmannounce'),
(467,'Command: gmnameannounce'),
(468,'Command: gmnotify'),
(469,'Command: nameannounce'),
(470,'Command: notify'),
(471,'Command: whispers'),
(472,'Command: group'),
(473,'Command: group leader'),
(474,'Command: group disband'),
(475,'Command: group remove'),
(476,'Command: group join'),
(477,'Command: group list'),
(478,'Command: group summon'),
(479,'Command: pet'),
(480,'Command: pet create'),
(481,'Command: pet learn'),
(482,'Command: pet unlearn'),
(483,'Command: send'),
(484,'Command: send items'),
(485,'Command: send mail'),
(486,'Command: send message'),
(487,'Command: send money'),
(488,'Command: additem'),
(489,'Command: additemset'),
(490,'Command: appear'),
(491,'Command: aura'),
(492,'Command: bank'),
(493,'Command: bindsight'),
(494,'Command: combatstop'),
(495,'Command: cometome'),
(496,'Command: commands'),
(497,'Command: cooldown'),
(498,'Command: damage'),
(499,'Command: dev'),
(500,'Command: die'),
(501,'Command: dismount'),
(502,'Command: distance'),
(503,'Command: flusharenapoints'),
(504,'Command: freeze'),
(505,'Command: gps'),
(506,'Command: guid'),
(507,'Command: help'),
(508,'Command: hidearea'),
(509,'Command: itemmove'),
(510,'Command: kick'),
(511,'Command: linkgrave'),
(512,'Command: listfreeze'),
(513,'Command: maxskill'),
(514,'Command: movegens'),
(515,'Command: mute'),
(516,'Command: neargrave'),
(517,'Command: pinfo'),
(518,'Command: playall'),
(519,'Command: possess'),
(520,'Command: recall'),
(521,'Command: repairitems'),
(522,'Command: respawn'),
(523,'Command: revive'),
(524,'Command: saveall'),
(525,'Command: save'),
(526,'Command: setskill'),
(527,'Command: showarea'),
(528,'Command: summon'),
(529,'Command: unaura'),
(530,'Command: unbindsight'),
(531,'Command: unfreeze'),
(532,'Command: unmute'),
(533,'Command: unpossess'),
(534,'Command: unstuck'),
(535,'Command: wchange'),
(536,'Command: mmap'),
(537,'Command: mmap loadedtiles'),
(538,'Command: mmap loc'),
(539,'Command: mmap path'),
(540,'Command: mmap stats'),
(541,'Command: mmap testarea'),
(542,'Command: morph'),
(543,'Command: demorph'),
(544,'Command: modify'),
(545,'Command: modify arenapoints'),
(546,'Command: modify bit'),
(547,'Command: modify drunk'),
(548,'Command: modify energy'),
(549,'Command: modify faction'),
(550,'Command: modify gender'),
(551,'Command: modify honor'),
(552,'Command: modify hp'),
(553,'Command: modify mana'),
(554,'Command: modify money'),
(555,'Command: modify mount'),
(556,'Command: modify phase'),
(557,'Command: modify rage'),
(558,'Command: modify reputation'),
(559,'Command: modify runicpower'),
(560,'Command: modify scale'),
(561,'Command: modify speed'),
(562,'Command: modify speed all'),
(563,'Command: modify speed backwalk'),
(564,'Command: modify speed fly'),
(565,'Command: modify speed walk'),
(566,'Command: modify speed swim'),
(567,'Command: modify spell'),
(568,'Command: modify standstate'),
(569,'Command: modify talentpoints'),
(570,'Command: npc'),
(571,'Command: npc add'),
(572,'Command: npc add formation'),
(573,'Command: npc add item'),
(574,'Command: npc add move'),
(575,'Command: npc add temp'),
(576,'Command: npc add delete'),
(577,'Command: npc add delete item'),
(578,'Command: npc add follow'),
(579,'Command: npc add follow stop'),
(580,'Command: npc set'),
(581,'Command: npc set allowmove'),
(582,'Command: npc set entry'),
(583,'Command: npc set factionid'),
(584,'Command: npc set flag'),
(585,'Command: npc set level'),
(586,'Command: npc set link'),
(587,'Command: npc set model'),
(588,'Command: npc set movetype'),
(589,'Command: npc set phase'),
(590,'Command: npc set spawndist'),
(591,'Command: npc set spawntime'),
(592,'Command: npc set data'),
(593,'Command: npc info'),
(594,'Command: npc near'),
(595,'Command: npc move'),
(596,'Command: npc playemote'),
(597,'Command: npc say'),
(598,'Command: npc textemote'),
(599,'Command: npc whisper'),
(600,'Command: npc yell'),
(601,'Command: npc tame'),
(602,'Command: quest'),
(603,'Command: quest add'),
(604,'Command: quest complete'),
(605,'Command: quest remove'),
(606,'Command: quest reward'),
(607,'Command: reload'),
(608,'Command: reload access_requirement'),
(609,'Command: reload achievement_criteria_data'),
(610,'Command: reload achievement_reward'),
(611,'Command: reload all'),
(612,'Command: reload all achievement'),
(613,'Command: reload all area'),
(614,'Command: reload broadcast_text'),
(615,'Command: reload all gossips'),
(616,'Command: reload all item'),
(617,'Command: reload all locales'),
(618,'Command: reload all loot'),
(619,'Command: reload all npc'),
(620,'Command: reload all quest'),
(621,'Command: reload all scripts'),
(622,'Command: reload all spell'),
(623,'Command: reload areatrigger_involvedrelation'),
(624,'Command: reload areatrigger_tavern'),
(625,'Command: reload areatrigger_teleport'),
(626,'Command: reload auctions'),
(627,'Command: reload autobroadcast'),
(628,'Command: reload command'),
(629,'Command: reload conditions'),
(630,'Command: reload config'),
(631,'Command: reload battleground_template'),
(632,'Command: mutehistory'),
(633,'Command: reload creature_linked_respawn'),
(634,'Command: reload creature_loot_template'),
(635,'Command: reload creature_onkill_reputation'),
(636,'Command: reload creature_questender'),
(637,'Command: reload creature_queststarter'),
(638,'Command: reload creature_summon_groups'),
(639,'Command: reload creature_template'),
(640,'Command: reload creature_text'),
(641,'Command: reload disables'),
(642,'Command: reload disenchant_loot_template'),
(643,'Command: reload event_scripts'),
(644,'Command: reload fishing_loot_template'),
(645,'Command: reload game_graveyard_zone'),
(646,'Command: reload game_tele'),
(647,'Command: reload gameobject_questender'),
(648,'Command: reload gameobject_loot_template'),
(649,'Command: reload gameobject_queststarter'),
(650,'Command: reload gm_tickets'),
(651,'Command: reload gossip_menu'),
(652,'Command: reload gossip_menu_option'),
(653,'Command: reload item_enchantment_template'),
(654,'Command: reload item_loot_template'),
(655,'Command: reload item_set_names'),
(656,'Command: reload lfg_dungeon_rewards'),
(657,'Command: reload locales_achievement_reward'),
(658,'Command: reload locales_creature'),
(659,'Command: reload locales_creature_text'),
(660,'Command: reload locales_gameobject'),
(661,'Command: reload locales_gossip_menu_option'),
(662,'Command: reload locales_item'),
(663,'Command: reload locales_item_set_name'),
(664,'Command: reload locales_npc_text'),
(665,'Command: reload locales_page_text'),
(666,'Command: reload locales_points_of_interest'),
(667,'Command: reload locales_quest'),
(668,'Command: reload mail_level_reward'),
(669,'Command: reload mail_loot_template'),
(670,'Command: reload milling_loot_template'),
(671,'Command: reload npc_spellclick_spells'),
(672,'Command: reload npc_trainer'),
(673,'Command: reload npc_vendor'),
(674,'Command: reload page_text'),
(675,'Command: reload pickpocketing_loot_template'),
(676,'Command: reload points_of_interest'),
(677,'Command: reload prospecting_loot_template'),
(678,'Command: reload quest_poi'),
(679,'Command: reload quest_template'),
(680,'Command: reload rbac'),
(681,'Command: reload reference_loot_template'),
(682,'Command: reload reserved_name'),
(683,'Command: reload reputation_reward_rate'),
(684,'Command: reload reputation_spillover_template'),
(685,'Command: reload skill_discovery_template'),
(686,'Command: reload skill_extra_item_template'),
(687,'Command: reload skill_fishing_base_level'),
(688,'Command: reload skinning_loot_template'),
(689,'Command: reload smart_scripts'),
(690,'Command: reload spell_required'),
(691,'Command: reload spell_area'),
(692,'Command: reload spell_bonus_data'),
(693,'Command: reload spell_group'),
(694,'Command: reload spell_learn_spell'),
(695,'Command: reload spell_loot_template'),
(696,'Command: reload spell_linked_spell'),
(697,'Command: reload spell_pet_auras'),
-- 698
(699,'Command: reload spell_proc'),
(700,'Command: reload spell_scripts'),
(701,'Command: reload spell_target_position'),
(702,'Command: reload spell_threats'),
(703,'Command: reload spell_group_stack_rules'),
(704,'Command: reload trinity_string'),
(705,'Command: reload warden_action'),
(706,'Command: reload waypoint_scripts'),
(707,'Command: reload waypoint_data'),
(708,'Command: reload vehicle_accessory'),
(709,'Command: reload vehicle_template_accessory'),
(710,'Command: reset'),
(711,'Command: reset achievements'),
(712,'Command: reset honor'),
(713,'Command: reset level'),
(714,'Command: reset spells'),
(715,'Command: reset stats'),
(716,'Command: reset talents'),
(717,'Command: reset all'),
(718,'Command: server'),
(719,'Command: server corpses'),
(720,'Command: server exit'),
(721,'Command: server idlerestart'),
(722,'Command: server idlerestart cancel'),
(723,'Command: server idleshutdown'),
(724,'Command: server idleshutdown cancel'),
(725,'Command: server info'),
(726,'Command: server plimit'),
(727,'Command: server restart'),
(728,'Command: server restart cancel'),
(729,'Command: server set'),
(730,'Command: server set closed'),
(731,'Command: server set difftime'),
(732,'Command: server set loglevel'),
(733,'Command: server set motd'),
(734,'Command: server shutdown'),
(735,'Command: server shutdown cancel'),
(736,'Command: server motd'),
(737,'Command: tele'),
(738,'Command: tele add'),
(739,'Command: tele del'),
(740,'Command: tele name'),
(741,'Command: tele group'),
(742,'Command: ticket'),
(743,'Command: ticket assign'),
(744,'Command: ticket close'),
(745,'Command: ticket closedlist'),
(746,'Command: ticket comment'),
(747,'Command: ticket complete'),
(748,'Command: ticket delete'),
(749,'Command: ticket escalate'),
(750,'Command: ticket escalatedlist'),
(751,'Command: ticket list'),
(752,'Command: ticket onlinelist'),
(753,'Command: ticket reset'),
(754,'Command: ticket response'),
(755,'Command: ticket response append'),
(756,'Command: ticket response appendln'),
(757,'Command: ticket togglesystem'),
(758,'Command: ticket unassign'),
(759,'Command: ticket viewid'),
(760,'Command: ticket viewname'),
(761,'Command: titles'),
(762,'Command: titles add'),
(763,'Command: titles current'),
(764,'Command: titles remove'),
(765,'Command: titles set'),
(766,'Command: titles set mask'),
(767,'Command: wp'),
(768,'Command: wp add'),
(769,'Command: wp event'),
(770,'Command: wp load'),
(771,'Command: wp modify'),
(772,'Command: wp unload'),
(773,'Command: wp reload'),
(774,'Command: wp show'),
(775,'Command: modify currency'),
(776,'Command: debug phase'),
(777,'Command: mailbox'),
(778,'Command: ahbot'),
(779,'Command: ahbot items'),
(780,'Command: ahbot items gray'),
(781,'Command: ahbot items white'),
(782,'Command: ahbot items green'),
(783,'Command: ahbot items blue'),
(784,'Command: ahbot items purple'),
(785,'Command: ahbot items orange'),
(786,'Command: ahbot items yellow'),
(787,'Command: ahbot ratio'),
(788,'Command: ahbot ratio alliance'),
(789,'Command: ahbot ratio horde'),
(790,'Command: ahbot ratio neutral'),
(791,'Command: ahbot rebuild'),
(792,'Command: ahbot reload'),
(793,'Command: ahbot status'),
(794,'Command: guild info'),
(795,'Command: instance setbossstate'),
(796,'Command: instance getbossstate'),
(797,'Command: pvpstats'),
(798,'Command: mod xp'),
(799,'Command: go bugticket'),
(800,'Command: go complaintticket'),
(801,'Command: go suggestionticket'),
(802,'Command: ticket bug'),
(803,'Command: ticket complaint'),
(804,'Command: ticket suggestion'),
(805,'Command: ticket bug assign'),
(806,'Command: ticket bug close'),
(807,'Command: ticket bug closedlist'),
(808,'Command: ticket bug comment'),
(809,'Command: ticket bug delete'),
(810,'Command: ticket bug list'),
(811,'Command: ticket bug unassign'),
(812,'Command: ticket bug view'),
(813,'Command: ticket complaint assign'),
(814,'Command: ticket complaint close'),
(815,'Command: ticket complaint closedlist'),
(816,'Command: ticket complaint comment'),
(817,'Command: ticket complaint delete'),
(818,'Command: ticket complaint list'),
(819,'Command: ticket complaint unassign'),
(820,'Command: ticket complaint view'),
(821,'Command: ticket suggestion assign'),
(822,'Command: ticket suggestion close'),
(823,'Command: ticket suggestion closedlist'),
(824,'Command: ticket suggestion comment'),
(825,'Command: ticket suggestion delete'),
(826,'Command: ticket suggestion list'),
(827,'Command: ticket suggestion unassign'),
(828,'Command: ticket suggestion view'),
(829,'Command: ticket reset all'),
(830,'Command: bnetaccount listgameaccounts'),
(831,'Command: ticket reset bug'),
(832,'Command: ticket reset complaint'),
(833,'Command: ticket reset suggestion'),
(834,'Command: go quest'),
(835,'Command: debug loadcells'),
(836,'Command: debug boundary'),
(837,'Command: npc evade'),
(838,'Command: pet level'),
(839,'Command: server shutdown force'),
(840,'Command: server restart force'),
(841,'Command: debug neargraveyard'),
(842,'Command: reload character_template'),
(843,'Command: reload quest_greeting'),
(844,'Command: scene'),
(845,'Command: scene debug'),
(846,'Command: scene play'),
(847,'Command: scene play package'),
(848,'Command: scene cancel'),
(849,'Command: list scenes'),
(850,'Command: reload scenes'),
(851,'Command: reload areatrigger_template'),
(852,'Command: go offset'),
(853,'Command: reload conversation_template'),
(854,'Command: debug conversation'),

-- Custom permissions
(1001,'Command: donate'),
(1002,'Command: donate close'),
(1003,'Command: donate list'),
(1004,'Command: donate onlinelist'),
(1005,'Command: donate view'),
(1011,'Command: guildhouse'),
(1012,'Command: guildhouse add'),
(1013,'Command: guildhouse link'),
(1014,'Command: guildhouse delete'),
(1021,'Command: report'),
(1022,'Command: report complete'),
(1023,'Command: report quest'),
(1024,'Command: report remove'),
(1031,'Command: server shell'),
(1032,'Command: server shell update'),
(1033,'Command: server shell restore'),
(1034,'Command: server shell status'),
(1035,'Command: server shell switchbranch'),
(1036,'Command: server state'),
(1041,'Command: wargame'),
(1042,'Command: pvp'),
(1051,'Command: debug areatriggers spawn'),
(1061,'Command: instance bind'),
-- 1062
(1063,'Command: instance getdata'),
(1064,'Command: instance getdata64'),
(1065,'Command: instance setdata'),
-- Anticheat
(1101,'Command: anticheat'),
(1102,'Command: anticheat global'),
(1103,'Command: anticheat player'),
(1104,'Command: anticheat delete'),
(1105,'Command: anticheat handle'),
(1106,'Command: anticheat jail'),
(1107,'Command: anticheat warn'),
-- Misc
(1111,'Command: rbg'),
(1112,'Command: rbg join'),
(1113,'Command: rbg stats'),
(1114,'Command: rbg team'),
(1115,'Command: rbg opponent'),
(1121,'Command: spectator'),
(1122,'Command: spectator reset'),
(1123,'Command: spectator watch'),
(1124,'Command: spectator spectate'),
(1125,'Command: spectator version'),
(1126,'Command: spectator leave'),
(1131,'Command: event distribute'),
-- 1132
(1133,'Command: lookup guild'),
(1134,'Command: lookup player character'),
(1135,'Command: morphall'),
(1136,'Command: reload performanceconfig'),
(1137,'Command: title distribute'),
-- 1138
(1139,'Command: reload npc_text'),
(1140,'Command: reload calendar_dates'),
-- Soloqueue
(1141, 'Command: soloqueue'),
(1142, 'Command: soloqueue join'),
(1143, 'Command: soloqueue leave'),
(1144, 'Command: soloqueue stats'),

-- 1200
(1201,'Command: rbac account'),
(1202,'Command: rbac account group'),
(1203,'Command: rbac account group add'),
(1204,'Command: rbac account group remove'),
(1205,'Command: rbac account role'),
(1206,'Command: rbac account role grant'),
(1207,'Command: rbac account role deny'),
(1208,'Command: rbac account role revoke'),
(1209,'Command: rbac account permission'),
(1210,'Command: rbac account permission grant'),
(1211,'Command: rbac account permission deny'),
(1212,'Command: rbac account permission revoke'),
(1213,'Command: rbac list'),
(1214,'Command: rbac list groups'),
(1215,'Command: rbac list roles'),
(1216,'Command: rbac list permissions'),

(1300, 'Command: morpher'),
(1301, 'Command: morpher modify'),
(1302, 'Command: morpher modify name'),
(1303, 'Command: morpher modify displayid'),
(1304, 'Command: morpher modify price'),
(1305, 'Command: morpher reload'),
(1306, 'Command: morpher list'),
(1307, 'Command: morpher add'),
(1308, 'Command: morpher delete'),
(1309, 'Command: rotatedungeons'),
(1310, 'Command: feuerprobe'),
(1311, 'Command: movie'),

(1400,'Skip Check Evaluate Damage Done');

INSERT INTO `rbac_roles` (`id`, `name`) VALUES
(1, 'Player Commands'),
-- 2
-- 3
-- 4
(5,  'Quick Login/Logout'),
(6,  'Use Battleground/Arenas'),
(7,  'Use Dungeon Finder'),
(8,  'Log GM trades'),
(9,  'Skip Instance required bosses check'),
(10, 'Ticket management'),
(11, 'Instant .save'),
(12, 'Allow params with .unstuck'),
(13, 'Full HP after resurrect'),
(14, 'Appear in GM ingame list'),
(15, 'Use staff badge in chat'),
(16, 'Receive global GM messages/texts'),
(17, 'Skip over-speed ping check'),
(18, 'Allows Admin Opcodes'),
(19, 'Two side mail interaction'),
(20, 'Notify if a command was not found'),
(21, 'Enables lower security than target check'),
(22, 'Skip disable map check'),
(23, 'Skip reset talents when used more than allowed check'),
(24, 'Skip spam chat check'),
(25, 'Restore saved gm setting states'),
(26, 'Use Config option START_GM_LEVEL to assign new character level'),
(27, 'Skip needed requirements to use channel check'),
(28, 'Allow say chat between factions'),
(29, 'Filter whispers'),
(30, 'Allow channel chat between factions'),
(31, 'Join channels without announce'),
(32, 'Change channel settings without being channel moderator'),
(33, 'Skip character creation checks'),
(34, 'Two side faction characters on the same account'),
(35, 'See two side who list'),
(36, 'Add friends of other faction'),
(37, 'See all security levels with who command'),
(38, 'Allows to add a gm to friend list'),
(39, 'Enable IP, Last Login and EMail output in pinfo'),
(40, 'Forces to enter the email for confirmation on password change'),
(41, 'Allow user to check his own email with .account'),

-- 100
(101,'Tester Commands'),
(102,'Event-Team Commands'),
(103,'Moderator Commands'),
(104,'GameMaster Commands Level 1'),
(105,'GameMaster Commands Level 2'),
(106,'Donate Commands'),
(107,'Developer Commands Level 1'),
(108,'Developer Commands Level 2'),
(109,'Administrator Commands'),

(120,'Skip Check Evaluate Damage Done'),
(121,'Two side trade interaction'),
(122,'Use character templates');

SET @PLRC := 1;
SET @TESC := 101;
SET @EVTC := 102;
SET @MODC := 103;
SET @GMC1 := 104;
SET @GMC2 := 105;
SET @DONC := 106;
SET @DVC1 := 107;
SET @DVC2 := 108;
SET @ADMC := 109;

INSERT INTO `rbac_groups` (`id`, `name`) VALUES
(1, 'Player'),
(2, 'Moderator'),
(3, 'Tester'),
(4, 'Event-Team'),
(5, 'Test-GameMaster'),
(6, 'GameMaster'),
(7, 'Donate-GameMaster'),
(8, 'Developer'),
(9, 'Administrator');

INSERT INTO `rbac_role_permissions` (`roleId`, `permissionId`) VALUES
(5,  1),
(5,  2),
(6,  3),
(6,  4),
(6,  5),
(7,  6),
(1,  7),
-- 8
-- 9
(122,10), -- Use character templates
(8,  11),
(33, 12), -- Skip character creation demon hunter min level check
(9,  13),
(33, 14),
(33, 15),
(33, 16),
(33, 17),
(33, 18), -- Skip character creation death knight min level check
(27, 19),
(22, 20),
(23, 21),
(24, 22),
(17, 23),
(34, 24),
(28, 25),
(30, 26),
(19, 27),
(35, 28),
(36, 29),
(11, 30),
(12, 31),
(10, 32),
(20, 33),
(14, 34),
(37, 35),
(29, 36),
(15, 37),
(13, 38),
(25, 39),
(38, 40),
(26, 41),
(18, 42),
(18, 43),
(16, 44),
(31, 45),
(32, 46),
(21, 47),
(39, 48), -- Enable IP, Last Login and EMail output in pinfo
(40, 49), -- Forces to enter the email for confirmation on password change
(41, 50), -- Allow user to check his own email with .account
(121,51), -- Allow trading between factions

(@DVC2,200), -- Command: rbac
-- 201 - 206
(@GMC2,207), -- Command: bnetaccount
(@ADMC,208), -- Command: bnetaccount create
(@ADMC,209), -- Command: bnetaccount lock country
(@ADMC,210), -- Command: bnetaccount lock ip
(@ADMC,211), -- Command: bnetaccount password
(@ADMC,212), -- Command: bnetaccount set
(@ADMC,213), -- Command: bnetaccount set password
(@ADMC,214), -- Command: bnetaccount link
(@ADMC,215), -- Command: bnetaccount unlink
(@ADMC,216), -- Command: bnetaccount gameaccountcreate
(@ADMC,217), -- Command: account
(@ADMC,218), -- Command: account addon
(@ADMC,219), -- Command: account create
(@ADMC,220), -- Command: account delete
(@ADMC,221), -- Command: account lock
(@ADMC,222), -- Command: account lock country
(@ADMC,223), -- Command: account lock ip
(@ADMC,224), -- Command: account onlinelist
(@ADMC,225), -- Command: account password
(@ADMC,226), -- Command: account set
(@ADMC,227), -- Command: account set addon
(@ADMC,228), -- Command: account set gmlevel
(@ADMC,229), -- Command: account set password
(@DONC,230), -- Command: achievement
(@DONC,231), -- Command: achievement add
(@PLRC,232), -- Command: arena
(@DVC1,233), -- Command: arena captain
(@DVC1,234), -- Command: arena create
(@DVC1,235), -- Command: arena disband
(@GMC1,236), -- Command: arena info
(@GMC1,237), -- Command: arena lookup
(@DVC1,238), -- Command: arena rename
(@GMC2,239), -- Command: ban
(@GMC2,240), -- Command: ban account
(@GMC2,241), -- Command: ban character
(@GMC2,242), -- Command: ban ip
(@GMC2,243), -- Command: ban playeraccount
(@GMC2,244), -- Command: baninfo
(@GMC2,245), -- Command: baninfo account
(@GMC2,246), -- Command: baninfo character
(@GMC2,247), -- Command: baninfo ip
(@GMC2,248), -- Command: banlist
(@GMC2,249), -- Command: banlist account
(@GMC2,250), -- Command: banlist character
(@GMC2,251), -- Command: banlist ip
(@GMC2,252), -- Command: unban
(@GMC2,253), -- Command: unban account
(@GMC2,254), -- Command: unban character
(@GMC2,255), -- Command: unban ip
(@GMC2,256), -- Command: unban playeraccount
(@DVC2,257), -- Command: bf
(@DVC2,258), -- Command: bf start
(@DVC2,259), -- Command: bf stop
(@DVC2,260), -- Command: bf switch
(@DVC2,261), -- Command: bf timer
(@DVC2,262), -- Command: bf enable
(@ADMC,263), -- Command: account email
(@ADMC,264), -- Command: account set sec
(@ADMC,265), -- Command: account set sec email
(@ADMC,266), -- Command: account set sec regmail
(@DVC1,267), -- Command: cast
(@DVC1,268), -- Command: cast back
(@DVC1,269), -- Command: cast dist
(@DVC1,270), -- Command: cast self
(@DVC1,271), -- Command: cast target
(@DVC1,272), -- Command: cast dest
(@GMC2,273), -- Command: character
(@DONC,274), -- Command: character customize
(@DONC,275), -- Command: character changefaction
(@DONC,276), -- Command: character changerace
(@DVC1,277), -- Command: character deleted
-- 278
(@DVC1,279), -- Command: character deleted list
(@DVC1,280), -- Command: character deleted restore
-- 281
-- 282
(@DONC,283), -- Command: character level
(@GMC2,284), -- Command: character rename
(@GMC2,285), -- Command: character reputation
(@DVC2,286), -- Command: character titles
(@DONC,287), -- Command: levelup
(@DVC1,288), -- Command: pdump
(@DVC1,289), -- Command: pdump load
(@DVC1,290), -- Command: pdump write
(@DVC2,291), -- Command: cheat
(@DVC2,292), -- Command: cheat casttime
(@DVC2,293), -- Command: cheat cooldown
(@DVC2,294), -- Command: cheat explore
(@DVC2,295), -- Command: cheat god
(@DVC2,296), -- Command: cheat power
(@DVC2,297), -- Command: cheat status
(@DVC2,298), -- Command: cheat taxi
(@DVC2,299), -- Command: cheat waterwalk
(@DVC2,300), -- Command: debug
(@DVC2,301), -- Command: debug anim
(@DVC2,302), -- Command: debug areatriggers
(@DVC2,303), -- Command: debug arena
(@DVC2,304), -- Command: debug bg
(@DVC2,305), -- Command: debug entervehicle
(@DVC2,306), -- Command: debug getitemstate
(@DVC2,307), -- Command: debug getitemvalue
(@DVC2,308), -- Command: debug getvalue
(@DVC2,309), -- Command: debug hostil
(@DVC2,310), -- Command: debug itemexpire
(@DVC2,311), -- Command: debug lootrecipient
(@DVC2,312), -- Command: debug los
(@DVC2,313), -- Command: debug mod32value
(@DVC2,314), -- Command: debug moveflags
(@DVC2,315), -- Command: debug play
(@DVC2,316), -- Command: debug play cinematics
(@DVC2,317), -- Command: debug play movie
(@DVC2,318), -- Command: debug play sound
(@DVC2,319), -- Command: debug send
(@DVC2,320), -- Command: debug send buyerror
(@DVC2,321), -- Command: debug send channelnotify
(@DVC2,322), -- Command: debug send chatmessage
(@DVC2,323), -- Command: debug send equiperror
(@DVC2,324), -- Command: debug send largepacket
(@DVC2,325), -- Command: debug send opcode
(@DVC2,326), -- Command: debug send qinvalidmsg
(@DVC2,327), -- Command: debug send qpartymsg
(@DVC2,328), -- Command: debug send sellerror
(@DVC2,329), -- Command: debug send setphaseshift
(@DVC2,330), -- Command: debug send spellfail
(@DVC2,331), -- Command: debug setaurastate
(@DVC2,332), -- Command: debug setbit
(@DVC2,333), -- Command: debug setitemvalue
(@DVC2,334), -- Command: debug setvalue
(@DVC2,335), -- Command: debug setvid
(@DVC2,336), -- Command: debug spawnvehicle
(@DVC2,337), -- Command: debug threat
(@DVC2,338), -- Command: debug update
(@DVC2,339), -- Command: debug uws
(@DVC2,340), -- Command: wpgps
(@GMC2,341), -- Command: deserter
(@GMC2,342), -- Command: deserter bg
(@GMC2,343), -- Command: deserter bg add
(@GMC2,344), -- Command: deserter bg remove
(@GMC2,345), -- Command: deserter instance
(@GMC2,346), -- Command: deserter instance add
(@GMC2,347), -- Command: deserter instance remove
(@DVC2,348), -- Command: disable
(@DVC2,349), -- Command: disable add
(@DVC2,350), -- Command: disable add achievement_criteria
(@DVC2,351), -- Command: disable add battleground
(@DVC2,352), -- Command: disable add map
(@DVC2,353), -- Command: disable add mmap
(@DVC2,354), -- Command: disable add outdoorpvp
(@DVC2,355), -- Command: disable add quest
(@DVC2,356), -- Command: disable add spell
(@DVC2,357), -- Command: disable add vmap
(@DVC2,358), -- Command: disable remove
(@DVC2,359), -- Command: disable remove achievement_criteria
(@DVC2,360), -- Command: disable remove battleground
(@DVC2,361), -- Command: disable remove map
(@DVC2,362), -- Command: disable remove mmap
(@DVC2,363), -- Command: disable remove outdoorpvp
(@DVC2,364), -- Command: disable remove quest
(@DVC2,365), -- Command: disable remove spell
(@DVC2,366), -- Command: disable remove vmap
(@DVC1,367), -- Command: event info
(@DVC1,368), -- Command: event activelist
(@DVC1,369), -- Command: event start
(@DVC1,370), -- Command: event stop
(@MODC,371), -- Command: gm
(@MODC,372), -- Command: gm chat
(@MODC,373), -- Command: gm fly
(@MODC,374), -- Command: gm ingame
(@MODC,375), -- Command: gm list
(@MODC,376), -- Command: gm visible
(@GMC1,377), -- Command: go
(@GMC1,378), -- Command: go creature
(@GMC1,379), -- Command: go graveyard
(@GMC1,380), -- Command: go grid
(@GMC1,381), -- Command: go object
(@GMC1,382), -- Command: go taxinode
(@GMC1,383), -- Command: go ticket
(@GMC1,384), -- Command: go trigger
(@GMC1,385), -- Command: go xyz
(@GMC1,386), -- Command: go zonexy
(@GMC1,387), -- Command: gobject
(@GMC1,388), -- Command: gobject activate
(@DVC2,389), -- Command: gobject add
(@DVC2,390), -- Command: gobject add temp
(@DVC2,391), -- Command: gobject delete
(@GMC1,392), -- Command: gobject info
(@DVC2,393), -- Command: gobject move
(@GMC1,394), -- Command: gobject near
(@DVC2,395), -- Command: gobject set
(@DVC2,396), -- Command: gobject set phase
(@DVC2,397), -- Command: gobject set state
(@GMC1,398), -- Command: gobject target
(@DVC2,399), -- Command: gobject turn
(@DVC2,400), -- Command: debug transport
(@GMC1,401), -- Command: guild
(@GMC2,402), -- Command: guild create
(@DVC2,403), -- Command: guild delete
(@GMC1,404), -- Command: guild invite
(@GMC1,405), -- Command: guild uninvite
(@GMC2,406), -- Command: guild rank
(@GMC2,407), -- Command: guild rename
(@DVC2,408), -- Command: honor
(@DVC2,409), -- Command: honor add
(@DVC2,410), -- Command: honor add kill
(@DVC2,411), -- Command: honor update
(@GMC2,412), -- Command: instance
(@GMC2,413), -- Command: instance listbinds
(@GMC2,414), -- Command: instance unbind
(@GMC2,415), -- Command: instance stats
(@GMC2,416), -- Command: instance savedata
(@DVC1,417), -- Command: learn
(@DVC1,418), -- Command: learn all
(@DVC1,419), -- Command: learn all my
(@DVC1,420), -- Command: learn all my class
(@DVC1,421), -- Command: learn all my pettalents
(@DVC1,422), -- Command: learn all my spells
(@DVC1,423), -- Command: learn all my talents
(@DVC1,424), -- Command: learn all gm
(@DVC1,425), -- Command: learn all crafts
(@DVC1,426), -- Command: learn all default
(@DVC1,427), -- Command: learn all lang
(@DVC1,428), -- Command: learn all recipes
(@GMC2,429), -- Command: unlearn
(@DVC2,430), -- Command: lfg
(@DVC2,431), -- Command: lfg player
(@DVC2,432), -- Command: lfg group
(@DVC2,433), -- Command: lfg queue
(@DVC2,434), -- Command: lfg clean
(@DVC2,435), -- Command: lfg options
(@GMC2,436), -- Command: list
(@GMC2,437), -- Command: list creature
(@ADMC,438), -- Command: list item
(@GMC2,439), -- Command: list object
(@GMC2,440), -- Command: list auras
(@GMC2,441), -- Command: list mail
(@MODC,442), -- Command: lookup
(@MODC,443), -- Command: lookup area
(@MODC,444), -- Command: lookup creature
(@MODC,445), -- Command: lookup event
(@MODC,446), -- Command: lookup faction
(@MODC,447), -- Command: lookup item
(@MODC,448), -- Command: lookup itemset
(@MODC,449), -- Command: lookup object
(@MODC,450), -- Command: lookup quest
(@MODC,451), -- Command: lookup player
(@GMC1,452), -- Command: lookup player ip
(@GMC1,453), -- Command: lookup player account
(@GMC1,454), -- Command: lookup player email
(@MODC,455), -- Command: lookup skill
(@MODC,456), -- Command: lookup spell
(@MODC,457), -- Command: lookup spell id
(@MODC,458), -- Command: lookup taxinode
(@MODC,459), -- Command: lookup tele
(@MODC,460), -- Command: lookup title
(@MODC,461), -- Command: lookup map
(@GMC2,462), -- Command: announce
(@DVC1,463), -- Command: channel
(@DVC1,464), -- Command: channel set
(@DVC1,465), -- Command: channel set ownership
(@GMC1,466), -- Command: gmannounce
(@GMC1,467), -- Command: gmnameannounce
(@GMC1,468), -- Command: gmnotify
(@GMC2,469), -- Command: nameannounce
(@GMC2,470), -- Command: notify
(@MODC,471), -- Command: whispers
(@MODC,472), -- Command: group
(@GMC2,473), -- Command: group leader
(@GMC2,474), -- Command: group disband
(@GMC2,475), -- Command: group remove
(@GMC2,476), -- Command: group join
(@GMC2,477), -- Command: group list
(@MODC,478), -- Command: group summon
(@DVC1,479), -- Command: pet
(@DVC1,480), -- Command: pet create
(@DVC1,481), -- Command: pet learn
(@DVC1,482), -- Command: pet unlearn
(@GMC1,483), -- Command: send
(@DONC,484), -- Command: send items
(@GMC1,485), -- Command: send mail
(@GMC1,486), -- Command: send message
(@DONC,487), -- Command: send money
(@DONC,488), -- Command: additem
(@DONC,489), -- Command: additemset
(@MODC,490), -- Command: appear
(@GMC2,491), -- Command: aura
(@MODC,492), -- Command: bank
(@DVC2,493), -- Command: bindsight
(@GMC2,494), -- Command: combatstop
(@DVC2,495), -- Command: cometome
(@PLRC,496), -- Command: commands
(@DVC1,497), -- Command: cooldown
(@GMC1,498), -- Command: damage
(@DVC2,499), -- Command: dev
(@MODC,500), -- Command: die
(@GMC1,501), -- Command: dismount
(@DVC2,502), -- Command: distance
(@ADMC,503), -- Command: flusharenapoints
(@MODC,504), -- Command: freeze
(@MODC,505), -- Command: gps
(@MODC,506), -- Command: guid
(@PLRC,507), -- Command: help
(@GMC2,508), -- Command: hidearea
(@DVC2,509), -- Command: itemmove
(@GMC2,510), -- Command: kick
(@DVC2,511), -- Command: linkgrave
(@GMC2,512), -- Command: listfreeze
(@GMC2,513), -- Command: maxskill
(@DVC2,514), -- Command: movegens
(@MODC,515), -- Command: mute
(@GMC2,516), -- Command: neargrave
(@GMC1,517), -- Command: pinfo
(@DVC1,518), -- Command: playall
(@DVC2,519), -- Command: possess
(@MODC,520), -- Command: recall
(@MODC,521), -- Command: repairitems
(@MODC,522), -- Command: respawn
(@MODC,523), -- Command: revive
(@DVC2,524), -- Command: saveall
(@MODC,525), -- Command: save
(@DVC1,526), -- Command: setskill
(@GMC2,527), -- Command: showarea
(@MODC,528), -- Command: summon
(@MODC,529), -- Command: unaura
(@DVC2,530), -- Command: unbindsight
(@MODC,531), -- Command: unfreeze
(@GMC2,532), -- Command: unmute
(@DVC2,533), -- Command: unpossess
(@MODC,534), -- Command: unstuck
(@GMC2,535), -- Command: wchange
(@DVC2,536), -- Command: mmap
(@DVC2,537), -- Command: mmap loadedtiles
(@DVC2,538), -- Command: mmap loc
(@DVC2,539), -- Command: mmap path
(@DVC2,540), -- Command: mmap stats
(@DVC2,541), -- Command: mmap testarea
(@MODC,542), -- Command: morph
(@MODC,543), -- Command: demorph
(@MODC,544), -- Command: modify
(@DVC1,545), -- Command: modify arenapoints
(@DVC2,546), -- Command: modify bit
(@GMC1,547), -- Command: modify drunk
(@GMC1,548), -- Command: modify energy
(@DVC2,549), -- Command: modify faction
(@DVC2,550), -- Command: modify gender
(@DVC1,551), -- Command: modify honor
(@GMC1,552), -- Command: modify hp
(@GMC1,553), -- Command: modify mana
(@DVC1,554), -- Command: modify money
(@MODC,555), -- Command: modify mount
(@GMC2,556), -- Command: modify phase
(@GMC1,557), -- Command: modify rage
(@GMC2,558), -- Command: modify reputation
(@GMC1,559), -- Command: modify runicpower
(@MODC,560), -- Command: modify scale
(@MODC,561), -- Command: modify speed
(@MODC,562), -- Command: modify speed all
(@MODC,563), -- Command: modify speed backwalk
(@MODC,564), -- Command: modify speed fly
(@MODC,565), -- Command: modify speed walk
(@MODC,566), -- Command: modify speed swim
(@DVC2,567), -- Command: modify spell
(@GMC2,568), -- Command: modify standstate
(@DVC2,569), -- Command: modify talentpoints
(@MODC,570), -- Command: npc
(@DVC2,571), -- Command: npc add
(@DVC2,572), -- Command: npc add formation
(@DVC2,573), -- Command: npc add item
(@DVC2,574), -- Command: npc add move
(@GMC2,575), -- Command: npc add temp
(@DVC2,576), -- Command: npc add delete
(@DVC2,577), -- Command: npc add delete item
(@DVC2,578), -- Command: npc add follow
(@DVC2,579), -- Command: npc add follow stop
(@DVC2,580), -- Command: npc set
(@DVC2,581), -- Command: npc set allowmove
(@DVC2,582), -- Command: npc set entry
(@DVC2,583), -- Command: npc set factionid
(@DVC2,584), -- Command: npc set flag
(@DVC2,585), -- Command: npc set level
(@DVC2,586), -- Command: npc set link
(@DVC2,587), -- Command: npc set model
(@DVC2,588), -- Command: npc set movetype
(@DVC2,589), -- Command: npc set phase
(@DVC2,590), -- Command: npc set spawndist
(@DVC2,591), -- Command: npc set spawntime
(@DVC2,592), -- Command: npc set data
(@MODC,593), -- Command: npc info
(@DVC2,594), -- Command: npc near
(@DVC2,595), -- Command: npc move
(@GMC2,596), -- Command: npc playemote
(@GMC2,597), -- Command: npc say
(@GMC2,598), -- Command: npc textemote
(@GMC2,599), -- Command: npc whisper
(@GMC2,600), -- Command: npc yell
(@DVC2,601), -- Command: npc tame
(@GMC1,602), -- Command: quest
(@GMC1,603), -- Command: quest add
(@GMC1,604), -- Command: quest complete
(@GMC1,605), -- Command: quest remove
(@GMC2,606), -- Command: quest reward
(@DVC1,607), -- Command: reload
(@DVC1,608), -- Command: reload access_requirement
(@DVC1,609), -- Command: reload achievement_criteria_data
(@DVC1,610), -- Command: reload achievement_reward
(@DVC2,611), -- Command: reload all
(@DVC2,612), -- Command: reload all achievement
(@DVC2,613), -- Command: reload all area
(@DVC1,614), -- Command: reload broadcast_text
(@DVC2,615), -- Command: reload all gossips
(@DVC2,616), -- Command: reload all item
(@DVC2,617), -- Command: reload all locales
(@DVC2,618), -- Command: reload all loot
(@DVC2,619), -- Command: reload all npc
(@DVC2,620), -- Command: reload all quest
(@DVC2,621), -- Command: reload all scripts
(@DVC2,622), -- Command: reload all spell
(@DVC1,623), -- Command: reload areatrigger_involvedrelation
(@DVC1,624), -- Command: reload areatrigger_tavern
(@DVC1,625), -- Command: reload areatrigger_teleport
(@DVC1,626), -- Command: reload auctions
(@DVC1,627), -- Command: reload autobroadcast
(@DVC1,628), -- Command: reload command
(@DVC1,629), -- Command: reload conditions
(@DVC1,630), -- Command: reload config
(@DVC1,631), -- Command: reload battleground_template
(@MODC,632), -- Command: mutehistory
(@DVC1,633), -- Command: reload creature_linked_respawn
(@DVC1,634), -- Command: reload creature_loot_template
(@DVC1,635), -- Command: reload creature_onkill_reputation
(@DVC1,636), -- Command: reload creature_questender
(@DVC1,637), -- Command: reload creature_queststarter
(@DVC1,638), -- Command: reload creature_summon_groups
(@DVC1,639), -- Command: reload creature_template
(@DVC1,640), -- Command: reload creature_text
(@DVC1,641), -- Command: reload disables
(@DVC1,642), -- Command: reload disenchant_loot_template
(@DVC1,643), -- Command: reload event_scripts
(@DVC1,644), -- Command: reload fishing_loot_template
(@DVC1,645), -- Command: reload game_graveyard_zone
(@DVC1,646), -- Command: reload game_tele
(@DVC1,647), -- Command: reload gameobject_questender
(@DVC1,648), -- Command: reload gameobject_loot_template
(@DVC1,649), -- Command: reload gameobject_queststarter
(@DVC1,650), -- Command: reload support
(@DVC1,651), -- Command: reload gossip_menu
(@DVC1,652), -- Command: reload gossip_menu_option
(@DVC1,653), -- Command: reload item_enchantment_template
(@DVC1,654), -- Command: reload item_loot_template
(@DVC1,655), -- Command: reload item_set_names
(@DVC1,656), -- Command: reload lfg_dungeon_rewards
(@DVC1,657), -- Command: reload locales_achievement_reward
(@DVC1,658), -- Command: reload locales_creature
(@DVC1,659), -- Command: reload locales_creature_text
(@DVC1,660), -- Command: reload locales_gameobject
(@DVC1,661), -- Command: reload locales_gossip_menu_option
(@DVC1,662), -- Command: reload locales_item
(@DVC1,663), -- Command: reload locales_item_set_name
(@DVC1,664), -- Command: reload locales_npc_text
(@DVC1,665), -- Command: reload locales_page_text
(@DVC1,666), -- Command: reload locales_points_of_interest
(@DVC1,667), -- Command: reload locales_quest
(@DVC1,668), -- Command: reload mail_level_reward
(@DVC1,669), -- Command: reload mail_loot_template
(@DVC1,670), -- Command: reload milling_loot_template
(@DVC1,671), -- Command: reload npc_spellclick_spells
(@DVC1,672), -- Command: reload npc_trainer
(@DVC1,673), -- Command: reload npc_vendor
(@DVC1,674), -- Command: reload page_text
(@DVC1,675), -- Command: reload pickpocketing_loot_template
(@DVC1,676), -- Command: reload points_of_interest
(@DVC1,677), -- Command: reload prospecting_loot_template
(@DVC1,678), -- Command: reload quest_poi
(@DVC1,679), -- Command: reload quest_template
(@DVC1,680), -- Command: reload rbac
(@DVC1,681), -- Command: reload reference_loot_template
(@DVC1,682), -- Command: reload reserved_name
(@DVC1,683), -- Command: reload reputation_reward_rate
(@DVC1,684), -- Command: reload reputation_spillover_template
(@DVC1,685), -- Command: reload skill_discovery_template
(@DVC1,686), -- Command: reload skill_extra_item_template
(@DVC1,687), -- Command: reload skill_fishing_base_level
(@DVC1,688), -- Command: reload skinning_loot_template
(@DVC1,689), -- Command: reload smart_scripts
(@DVC1,690), -- Command: reload spell_required
(@DVC1,691), -- Command: reload spell_area
(@DVC1,692), -- Command: reload spell_bonus_data
(@DVC1,693), -- Command: reload spell_group
(@DVC1,694), -- Command: reload spell_learn_spell
(@DVC1,695), -- Command: reload spell_loot_template
(@DVC1,696), -- Command: reload spell_linked_spell
(@DVC1,697), -- Command: reload spell_pet_auras
-- 698
(@DVC1,699), -- Command: reload spell_proc
(@DVC1,700), -- Command: reload spell_scripts
(@DVC1,701), -- Command: reload spell_target_position
(@DVC1,702), -- Command: reload spell_threats
(@DVC1,703), -- Command: reload spell_group_stack_rules
(@DVC1,704), -- Command: reload trinity_string
(@DVC1,705), -- Command: reload warden_action
(@DVC1,706), -- Command: reload waypoint_scripts
(@DVC1,707), -- Command: reload waypoint_data
(@DVC1,708), -- Command: reload vehicle_accessory
(@DVC1,709), -- Command: reload vehicle_template_accessory
(@GMC2,710), -- Command: reset
-- 711 -- Command: reset achievements
(@GMC2,712), -- Command: reset honor
-- 713 -- Command: reset level
-- 714 -- Command: reset spells
(@GMC2,715), -- Command: reset stats
(@GMC2,716), -- Command: reset talents
-- 717 -- Command: reset all
(@MODC,718), -- Command: server
(@DVC2,719), -- Command: server corpses
(@DVC2,720), -- Command: server exit
(@DVC2,721), -- Command: server idlerestart
(@DVC2,722), -- Command: server idlerestart cancel
(@DVC2,723), -- Command: server idleshutdown
(@DVC2,724), -- Command: server idleshutdown cancel
(@MODC,725), -- Command: server info
(@ADMC,726), -- Command: server plimit
(@DVC2,727), -- Command: server restart
(@DVC2,728), -- Command: server restart cancel
(@DVC2,729), -- Command: server set
(@ADMC,730), -- Command: server set closed
(@ADMC,731), -- Command: server set difftime
(@DVC2,732), -- Command: server set loglevel
(@ADMC,733), -- Command: server set motd
(@DVC2,734), -- Command: server shutdown
(@DVC2,735), -- Command: server shutdown cancel
(@MODC,736), -- Command: server motd
(@MODC,737), -- Command: tele
(@DVC2,738), -- Command: tele add
(@DVC2,739), -- Command: tele del
(@MODC,740), -- Command: tele name
(@MODC,741), -- Command: tele group
(@GMC1,742), -- Command: ticket
(@GMC1,743), -- Command: ticket assign
(@GMC1,744), -- Command: ticket close
(@GMC1,745), -- Command: ticket closedlist
(@GMC1,746), -- Command: ticket comment
(@GMC1,747), -- Command: ticket complete
(@ADMC,748), -- Command: ticket delete
(@GMC1,749), -- Command: ticket escalate
(@GMC1,750), -- Command: ticket escalatedlist
(@GMC1,751), -- Command: ticket list
(@GMC1,752), -- Command: ticket onlinelist
(@DVC2,753), -- Command: ticket reset
(@GMC1,754), -- Command: ticket response
(@GMC1,755), -- Command: ticket response append
(@GMC1,756), -- Command: ticket response appendln
(@DVC2,757), -- Command: ticket togglesystem
(@GMC1,758), -- Command: ticket unassign
(@GMC1,759), -- Command: ticket viewid
(@GMC1,760), -- Command: ticket viewname
(@DVC2,761), -- Command: titles
(@DVC2,762), -- Command: titles add
(@DVC2,763), -- Command: titles current
(@DVC2,764), -- Command: titles remove
(@DVC2,765), -- Command: titles set
(@DVC2,766), -- Command: titles set mask
(@DVC2,767), -- Command: wp
(@DVC2,768), -- Command: wp add
(@DVC2,769), -- Command: wp event
(@DVC2,770), -- Command: wp load
(@DVC2,771), -- Command: wp modify
(@DVC2,772), -- Command: wp unload
(@DVC2,773), -- Command: wp reload
(@DVC2,774), -- Command: wp show
(@DVC1,775), -- Command: modify currency
(@DVC2,776), -- Command: debug phase
(@MODC,777), -- Command: mailbox
(@DVC2,778), -- Command: ahbot
(@DVC2,779), -- Command: ahbot items
(@DVC2,780), -- Command: ahbot items gray
(@DVC2,781), -- Command: ahbot items white
(@DVC2,782), -- Command: ahbot items green
(@DVC2,783), -- Command: ahbot items blue
(@DVC2,784), -- Command: ahbot items purple
(@DVC2,785), -- Command: ahbot items orange
(@DVC2,786), -- Command: ahbot items yellow
(@DVC2,787), -- Command: ahbot ratio
(@DVC2,788), -- Command: ahbot ratio alliance
(@DVC2,789), -- Command: ahbot ratio horde
(@DVC2,790), -- Command: ahbot ratio neutral
(@DVC2,791), -- Command: ahbot rebuild
(@DVC2,792), -- Command: ahbot reload
(@DVC2,793), -- Command: ahbot status
(@GMC1,794), -- Command: guild info
(@DVC2,795), -- Command: instance setbossstate
(@DVC2,796), -- Command: instance getbossstate
(@GMC2,797), -- Command: pvpstats
(@GMC2,798), -- Command: mod xp
(@GMC1,799), -- Command: go bugticket
(@GMC1,800), -- Command: go complaintticket
(@GMC1,801), -- Command: go suggestionticket
(@GMC1,802), -- Command: ticket bug
(@GMC1,803), -- Command: ticket complaint
(@GMC1,804), -- Command: ticket suggestion
(@GMC1,805), -- Command: ticket bug assign
(@GMC1,806), -- Command: ticket bug close
(@GMC1,807), -- Command: ticket bug closedlist
(@GMC1,808), -- Command: ticket bug comment
(@GMC1,809), -- Command: ticket bug delete
(@GMC1,810), -- Command: ticket bug list
(@GMC1,811), -- Command: ticket bug unassign
(@GMC1,812), -- Command: ticket bug view
(@GMC1,813), -- Command: ticket complaint assign
(@GMC1,814), -- Command: ticket complaint close
(@GMC1,815), -- Command: ticket complaint closedlist
(@GMC1,816), -- Command: ticket complaint comment
(@GMC1,817), -- Command: ticket complaint delete
(@GMC1,818), -- Command: ticket complaint list
(@GMC1,819), -- Command: ticket complaint unassign
(@GMC1,820), -- Command: ticket complaint view
(@GMC1,821), -- Command: ticket suggestion assign
(@GMC1,822), -- Command: ticket suggestion close
(@GMC1,823), -- Command: ticket suggestion closedlist
(@GMC1,824), -- Command: ticket suggestion comment
(@GMC1,825), -- Command: ticket suggestion delete
(@GMC1,826), -- Command: ticket suggestion list
(@GMC1,827), -- Command: ticket suggestion unassign
(@GMC1,828), -- Command: ticket suggestion view
(@DVC2,829), -- Command: ticket reset all
(@GMC2,830), -- Command: bnetaccount listgameaccounts
(@DVC2,831), -- Command: ticket reset bug
(@DVC2,832), -- Command: ticket reset complaint
(@DVC2,833), -- Command: ticket reset suggestion
(@GMC1,834), -- Command: go quest
(@DVC2,835), -- Command: debug loadcells
(@DVC2,836), -- Command: debug boundary
(@GMC2,837), -- Command: npc evade
(@DVC1,838), -- Command: pet level
(@ADMC,839), -- Command: server shutdown force
(@ADMC,840), -- Command: server restart force
(@DVC2,841), -- Command: debug neargraveyard
(@DVC2,842), -- Command: reload character_template
(@DVC2,843), -- Command: reload quest_greeting
(@DVC2,844), -- Command: scene
(@DVC2,845), -- Command: scene debug
(@DVC2,846), -- Command: scene play
(@DVC2,847), -- Command: scene play package
(@DVC2,848), -- Command: scene cancel
(@DVC2,849), -- Command: list scenes
(@DVC2,850), -- Command: reload scenes
(@DVC2,851), -- Command: reload areatrigger_template
(@GMC1,852), -- Command: go offset
(@DVC2,853), -- Command: reload conversation_template
(@DVC2,854), -- Command: debug conversation

-- Custom Permissions
(@DONC,1001), -- Command: donate
(@DONC,1002), -- Command: donate close
(@DONC,1003), -- Command: donate list
(@DONC,1004), -- Command: donate onlinelist
(@DONC,1005), -- Command: donate view
(@PLRC,1011), -- Command: guildhouse
(@DONC,1012), -- Command: guildhouse add
(@DONC,1013), -- Command: guildhouse link
(@DONC,1014), -- Command: guildhouse delete
(@PLRC,1021), -- Command: report
(@MODC,1022), -- Command: report complete
(@PLRC,1023), -- Command: report quest
(@MODC,1024), -- Command: report remove
(@DVC2,1031), -- Command: server shell
(@DVC2,1032), -- Command: server shell update
(@DVC2,1033), -- Command: server shell restore
(@DVC2,1034), -- Command: server shell status
(@DVC2,1035), -- Command: server shell switchbranch
(@DVC2,1036), -- Command: server state
(@PLRC,1041), -- Command: wargame
(@DVC2,1042), -- Command: pvp
(@DVC2,1051), -- Command: debug areatriggers spawn
(@DVC2,1061), -- Command: instance bind
-- 1062
(@DVC2,1063), -- Command: instance getdata
(@DVC2,1064), -- Command: instance getdata64
(@DVC2,1065), -- Command: instance setdata
(@GMC2,1101), -- Command: anticheat
(@GMC2,1102), -- Command: anticheat global
(@GMC2,1103), -- Command: anticheat player
(@DVC2,1104), -- Command: anticheat delete
(@DVC2,1105), -- Command: anticheat handle
(@GMC2,1106), -- Command: anticheat jail
(@GMC2,1107), -- Command: anticheat warn
(@PLRC,1111), -- Command: rbg
(@PLRC,1112), -- Command: rbg join
(@PLRC,1113), -- Command: rbg stats
(@PLRC,1114), -- Command: rbg team
(@PLRC,1115), -- Command: rbg opponent
(@PLRC,1121), -- Command: spectator
(@PLRC,1122), -- Command: spectator reset
(@PLRC,1123), -- Command: spectator watch
(@PLRC,1124), -- Command: spectator spectate
(@PLRC,1125), -- Command: spectator version
(@PLRC,1126), -- Command: spectator leave
(@MODC,1131), -- Command: event distribute
-- 1132
(@GMC1,1133), -- Command: lookup guild
(@GMC1,1134), -- Command: lookup player character
(@GMC1,1135), -- Command: morphall
(@DVC2,1136), -- Command: reload performanceconfig
(@DVC1,1137), -- Command: title distribute
-- 1138
(@DVC1,1139), -- Command: reload npc_text
(@DVC1,1140), -- Command: reload calendar_dates
(@PLRC,1141), -- Command: soloqueue
(@PLRC,1142), -- Command: soloqueue join
(@PLRC,1143), -- Command: soloqueue leave
(@PLRC,1144), -- Command: soloqueue stats

-- 1200
(@DVC2,1201), -- Command: rbac account
(@DVC2,1202), -- Command: rbac account group
(@ADMC,1203), -- Command: rbac account group add
(@ADMC,1204), -- Command: rbac account group remove
(@DVC2,1205), -- Command: rbac account role
(@ADMC,1206), -- Command: rbac account role grant
(@ADMC,1207), -- Command: rbac account role deny
(@ADMC,1208), -- Command: rbac account role revoke
(@DVC2,1209), -- Command: rbac account permission
(@ADMC,1210), -- Command: rbac account permission grant
(@ADMC,1211), -- Command: rbac account permission deny
(@ADMC,1212), -- Command: rbac account permission revoke
(@DVC2,1213), -- Command: rbac list
(@DVC2,1214), -- Command: rbac list groups
(@DVC2,1215), -- Command: rbac list roles
(@DVC2,1216), -- Command: rbac list permissions

(@DVC1,1300), -- Command: morpher
(@DVC1,1301), -- Command: morpher modify
(@DVC1,1302), -- Command: morpher modify name
(@DVC1,1303), -- Command: morpher modify displayid
(@DVC1,1304), -- Command: morpher modify price
(@DVC1,1305), -- Command: morpher reload
(@DVC1,1306), -- Command: morpher list
(@DVC1,1307), -- Command: morpher add
(@DVC1,1308), -- Command: morpher delete
(@DVC1,1309), -- Command: rotatedungeons
(@PLRC,1310), -- Command: feuerprobe
(@MODC,1311), -- Command: movie

(  120,1400), -- Skip Check Evaluate Damage Done

-- Tester Commands
(@TESC,230), -- Command: achievement
(@TESC,231), -- Command: achievement add
(@TESC,488), -- Command: additem
(@TESC,489), -- Command: additemset
(@TESC,491), -- Command: aura
(@TESC,267), -- Command: cast
(@TESC,270), -- Command: cast self
(@TESC,268), -- Command: cast back
(@TESC,291), -- Command: cheat
(@TESC,292), -- Command: cheat casttime
(@TESC,293), -- Command: cheat cooldown
(@TESC,294), -- Command: cheat explore
(@TESC,295), -- Command: cheat god
(@TESC,296), -- Command: cheat power
(@TESC,297), -- Command: cheat status
(@TESC,298), -- Command: cheat taxi
(@TESC,299), -- Command: cheat waterwalk
(@TESC,497), -- Command: cooldown
(@TESC,498), -- Command: damage
(@TESC,300), -- Command: debug
(@TESC,303), -- Command: debug arena
(@TESC,304), -- Command: debug bg
(@TESC,309), -- Command: debug hostil
(@TESC,501), -- Command: dismount
(@TESC,502), -- Command: distance
(@TESC,367), -- Command: event
(@TESC,368), -- Command: event activelist
(@TESC,369), -- Command: event start
(@TESC,370), -- Command: event stop
(@TESC,377), -- Command: go
(@TESC,378), -- Command: go creature
(@TESC,379), -- Command: go graveyard
(@TESC,380), -- Command: go grid
(@TESC,381), -- Command: go object
(@TESC,852), -- Command: go offset
(@TESC,382), -- Command: go taxinode
(@TESC,383), -- Command: go ticket
(@TESC,384), -- Command: go trigger
(@TESC,385), -- Command: go xyz
(@TESC,386), -- Command: go zonexy
(@TESC,834), -- Command: go quest
(@TESC,387), -- Command: gobject
(@TESC,388), -- Command: gobject activate
(@TESC,389), -- Command: gobject add
(@TESC,390), -- Command: gobject add temp
(@TESC,391), -- Command: gobject delete
(@TESC,392), -- Command: gobject info
(@TESC,393), -- Command: gobject move
(@TESC,394), -- Command: gobject near
(@TESC,395), -- Command: gobject set
(@TESC,396), -- Command: gobject set phase
(@TESC,397), -- Command: gobject set state
(@TESC,398), -- Command: gobject target
(@TESC,399), -- Command: gobject turn
(@TESC,474), -- Command: group disband
(@TESC,473), -- Command: group leader
(@TESC,409), -- Command: honor add
(@TESC,410), -- Command: honor add kill
(@TESC,412), -- Command: instance
(@TESC,413), -- Command: instance listbinds
(@TESC,414), -- Command: instance unbind
(@TESC,417), -- Command: learn
(@TESC,287), -- Command: levelup
(@TESC,436), -- Command: list
(@TESC,440), -- Command: list auras
(@TESC,437), -- Command: list creature
(@TESC,438), -- Command: list item
(@TESC,439), -- Command: list object
(@TESC,548), -- Command: modify energy
(@TESC,551), -- Command: modify honor
(@TESC,552), -- Command: modify hp
(@TESC,553), -- Command: modify mana
(@TESC,554), -- Command: modify money
(@TESC,557), -- Command: modify rage
(@TESC,556), -- Command: modify phase
(@TESC,558), -- Command: modify reputation
(@TESC,559), -- Command: modify runicpower
(@TESC,469), -- Command: nameannounce
(@TESC,571), -- Command: npc add
(@TESC,575), -- Command: npc add temp
(@TESC,576), -- Command: npc add delete
(@TESC,594), -- Command: npc near
(@TESC,584), -- Command: npc set flag
(@TESC,1042), -- Command: pvp
(@TESC,602), -- Command: quest
(@TESC,603), -- Command: quest add
(@TESC,604), -- Command: quest complete
(@TESC,605), -- Command: quest remove
(@TESC,606), -- Command: quest reward
(@TESC,715), -- Command: reset stats
(@TESC,716), -- Command: reset talents
(@TESC,727), -- Command: server restart
(@TESC,728), -- Command: server restart cancel
(@TESC,526), -- Command: setskill
(@TESC,429); -- Command: unlearn

INSERT INTO `rbac_group_roles` (`groupId`, `roleId`) VALUES
(1,  1),(2,  1),(3,  1),(4,  1),(5,  1),(6,  1),(7,  1),(8,  1),(9,  1), -- Player Commands
-- 2
-- 3
-- 4
        (2,  5),(3,  5),(4,  5),(5,  5),(6,  5),(7,  5),(8,  5),(9,  5), -- Quick Login/Logout
(1,  6),        (3,  6),                                (8,  6),(9,  6), -- Use Battleground/Arenas
(1,  7),        (3,  7),                                (8,  7),(9,  7), -- Use Dungeon Finder
        (2,  8),(3,  8),(4,  8),(5,  8),(6,  8),(7,  8),(8,  8),(9,  8), -- Log GM trades
        (2,  9),(3,  9),(4,  9),(5,  9),(6,  9),(7,  9),(8,  9),(9,  9), -- Skip Instance required bosses check
        (2, 10),(3, 10),(4, 10),(5, 10),(6, 10),(7, 10),(8, 10),(9, 10), -- Ticket management
        (2, 11),(3, 11),(4, 11),(5, 11),(6, 11),(7, 11),(8, 11),(9, 11), -- Instant .save
        (2, 12),(3, 12),(4, 12),(5, 12),(6, 12),(7, 12),(8, 12),(9, 12), -- Allow params with .unstuck
        (2, 13),(3, 13),(4, 13),(5, 13),(6, 13),(7, 13),(8, 13),(9, 13), -- Full HP after resurrect
        (2, 14),                (5, 14),(6, 14),(7, 14),                 -- Appear in GM ingame list
        (2, 15),(3, 15),(4, 15),(5, 15),(6, 15),(7, 15),(8, 15),(9, 15), -- Use staff badge in chat
        (2, 16),(3, 16),(4, 16),(5, 16),(6, 16),(7, 16),(8, 16),(9, 16), -- Receive global GM messages/texts
        (2, 17),(3, 17),(4, 17),(5, 17),(6, 17),(7, 17),(8, 17),(9, 17), -- Skip over-speed ping check
                                                        (8, 18),(9, 18), -- Allows Admin Opcodes
        (2, 19),(3, 19),(4, 19),(5, 19),(6, 19),(7, 19),(8, 19),(9, 19), -- Two side mail interaction
        (2, 20),(3, 20),(4, 20),(5, 20),(6, 20),(7, 20),(8, 20),(9, 20), -- Notify if a command was not found
        (2, 21),(3, 21),(4, 21),(5, 21),(6, 21),(7, 21),(8, 21),(9, 21), -- Enables lower security than target check
        (2, 22),(3, 22),(4, 22),(5, 22),(6, 22),(7, 22),(8, 22),(9, 22), -- Skip disable map check
                                                        (8, 23),(9, 23), -- Skip reset talents when used more than allowed check
        (2, 24),(3, 24),(4, 24),(5, 24),(6, 24),(7, 24),(8, 24),(9, 24), -- Skip spam chat check
        (2, 25),(3, 25),(4, 25),(5, 25),(6, 25),(7, 25),(8, 25),(9, 25), -- Restore saved gm setting states
        (2, 26),(3, 26),(4, 26),(5, 26),(6, 26),(7, 26),(8, 26),(9, 26), -- Use Config option START_GM_LEVEL to assign new character level
        (2, 27),(3, 27),(4, 27),(5, 27),(6, 27),(7, 27),(8, 27),(9, 27), -- Skip needed requirements to use channel check
        (2, 28),(3, 28),(4, 28),(5, 28),(6, 28),(7, 28),(8, 28),(9, 28), -- Allow say chat between factions
        (2, 29),(3, 29),(4, 29),(5, 29),(6, 29),(7, 29),(8, 29),(9, 29), -- Filter whispers
        (2, 30),(3, 30),(4, 30),(5, 30),(6, 30),(7, 30),(8, 30),(9, 30), -- Allow channel chat between factions
        (2, 31),(3, 31),(4, 31),(5, 31),(6, 31),(7, 31),(8, 31),(9, 31), -- Join channels without announce
        (2, 32),                (5, 32),(6, 32),(7, 32),(8, 32),(9, 32), -- Change channel settings without being channel moderator
        (2, 33),(3, 33),(4, 33),(5, 33),(6, 33),(7, 33),(8, 33),(9, 33), -- Skip character creation checks
(1, 34),(2, 34),(3, 34),(4, 34),(5, 34),(6, 34),(7, 34),(8, 34),(9, 34), -- Two side faction characters on the same account
        (2, 35),(3, 35),(4, 35),(5, 35),(6, 35),(7, 35),(8, 35),(9, 35), -- See two side who list
        (2, 36),(3, 36),(4, 36),(5, 36),(6, 36),(7, 36),(8, 36),(9, 36), -- Add friends of other faction
        (2, 37),(3, 37),(4, 37),(5, 37),(6, 37),(7, 37),(8, 37),(9, 37), -- See all security levels with who command
        (2, 38),(3, 38),(4, 38),(5, 38),(6, 38),(7, 38),(8, 38),(9, 38), -- Allows to add a gm to friend list
                                        (6, 39),(7, 39),(8, 39),(9, 39), -- Enable IP, Last Login and EMail output in pinfo
(1, 40),(2, 40),(3, 40),(4, 40),(5, 40),(6, 40),(7, 40),(8, 40),         -- Forces to enter the email for confirmation on password change
        (2, 41),(3, 41),(4, 41),(5, 41),(6, 41),(7, 41),(8, 41),(9, 41), -- Allow user to check his own email with .account

                (3,101),                                                 -- Tester Commands
                        (4,102),                                         -- Event-Team Commands
        (2,103),                (5,103),(6,103),(7,103),(8,103),(9,103), -- Moderator Commands
                                (5,104),(6,104),(7,104),(8,104),(9,104), -- GameMaster Commands Level 1
                                        (6,105),(7,105),(8,105),(9,105), -- GameMaster Commands Level 2
                                                (7,106),(8,106),(9,106), -- Donate Commands
                                                (7,107),(8,107),(9,107), -- Developer Commands Level 1
                                                        (8,108),(9,108), -- Developer Commands Level 2
                                                                (9,109), -- Administrator Commands

        (2,120),(3,120),(4,120),(5,120),(6,120),(7,120),(8,120),(9,120), -- Skip Check Evaluate Damage Done
                                                (7,121),(8,121),(9,121); -- Two side trade interaction

INSERT INTO `rbac_security_level_groups` (`secId`, `groupId`) VALUES
-- Player
(0, 1),
-- Moderator
(1, 2),
-- (1, 3),
-- (1, 4),
-- Test-GameMaster
(2, 5),
-- GameMaster
(3, 6),
-- Donate-GameMaster
(4, 6),
(4, 7),
-- Developer
(5, 8),
-- Administrator
(6, 9);
