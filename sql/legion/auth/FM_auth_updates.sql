-- Auth database update data
TRUNCATE TABLE `updates_include`;
INSERT INTO `updates_include` (`path`, `state`) VALUES
('$/sql/updates/auth', 'RELEASED'),
('$/sql/custom/auth', 'RELEASED'),
('$/sql/legion/auth/archive', 'ARCHIVED'),
('$/sql/legion/auth/update', 'RELEASED'),
('$/sql/old/6.x/auth', 'ARCHIVED'),
('$/sql/old/7/auth','RELEASED');

-- TRUNCATE TABLE `updates`;
