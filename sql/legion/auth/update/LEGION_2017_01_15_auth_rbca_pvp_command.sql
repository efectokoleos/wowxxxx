DELETE FROM `rbac_permissions` WHERE `id`=1042;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES
(1042, 'Command: pvp');

DELETE FROM `rbac_role_permissions` WHERE `permissionId`=1042;
INSERT INTO `rbac_role_permissions` (`roleId`, `permissionId`) VALUES
(108, 1042);
