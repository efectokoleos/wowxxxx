DELETE FROM `rbac_permissions` WHERE `id` = 1312;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES 
(1312, 'Command: reload spell_script_names');

DELETE FROM `rbac_role_permissions` WHERE `roleId` = 108 AND `permissionId` = 1312;
INSERT INTO `rbac_role_permissions` (`roleId`, `permissionId`) VALUES 
(108, 1312);
