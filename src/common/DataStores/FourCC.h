/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef FourCC_h__
#define FourCC_h__

#include "Define.h"

 /// Represents a magic value of 4 bytes
union FourCCMagic
{
    char asChar[4]; ///> Non-null terminated string
    uint32 asUInt;  ///> uint32 representation
};

static FourCCMagic const MD20Magic = { 'M', 'D', '2', '0' };
static FourCCMagic const MD21Magic = { 'M', 'D', '2', '1' };
static FourCCMagic const WMOMagic  = { 'R', 'E', 'V', 'M' };

#endif // FourCC_h__

