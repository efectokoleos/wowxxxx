/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "UnixCryptGeneration.h"

/// Returns the SHA512 unix hash of the given content.
std::string CalculateSHA512UnixHash(std::string const& content, std::string const& salt, bool fullHash /*= true*/)
{
    UnixCryptGenerator gen;
    return gen.GenerateSHA512PasswortHash(content, salt, fullHash);
}
