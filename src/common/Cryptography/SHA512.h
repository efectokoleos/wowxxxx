/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SHA512_h__
#define SHA512_h__

#include "Define.h"
#include <string>
#include <type_traits>
#include <openssl/sha.h>

class BigNumber;

class TC_COMMON_API SHA512Hash
{
    public:
        typedef std::integral_constant<uint32, SHA512_DIGEST_LENGTH> DigestLength;

        SHA512Hash();
        ~SHA512Hash();

        void UpdateBigNumbers(BigNumber* bn0, ...);

        void UpdateData(uint8 const* data, std::size_t len);
        void UpdateData(std::string const& str);

        void Initialize();
        void Finalize();

        uint8 const* GetDigest() const { return mDigest; }
        uint32 GetLength() const { return DigestLength::value; }

    private:
        SHA512_CTX mC;
        uint8 mDigest[DigestLength::value];
};

#endif // SHA512_h__
