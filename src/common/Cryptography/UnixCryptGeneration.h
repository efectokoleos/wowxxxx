/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UnixCryptGeneration_h__
#define UnixCryptGeneration_h__

#include "Define.h"
#include "SHA512.h"
#include <algorithm>
#include <cstring>
#include <sstream>
#include <vector>

// Define our magic string to mark salt for SHA512 "encryption" replacement.
static char const Sha512SaltPrefix[] = "$6$";

// Prefix for optional rounds specification.
static char const Sha512RoundsPrefix[] = "rounds=";

// Maximum salt string length.
static uint32 const SaltMaxLength = 16;

// Default number of rounds if not explicitly specified.
static uint32 const RoundsDefault = 5000;

// Minimum number of rounds.
static uint32 const RoundsMin = 1000;

// Maximum number of rounds.
static uint32 const RoundsMax = 999999999;

// Table with characters for base64 transformation.
static char const b64t[] = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

class TC_COMMON_API UnixCryptGenerator
{
    typedef SHA512Hash Hash;

public:
    UnixCryptGenerator() { }

    std::string GenerateSHA512PasswortHash(std::string const& key, std::string const& saltString, bool fullHash = true)
    {
        size_t cnt;

        // Default number of rounds.
        size_t rounds = RoundsDefault;
        bool roundsCustom = false;

        char const* salt = saltString.c_str();

        // Find beginning of salt string. The prefix should normally always be present. Just in case it is not.
        if (strncmp(Sha512SaltPrefix, salt, sizeof(Sha512SaltPrefix) - 1) == 0)
            // Skip salt prefix.
            salt += sizeof(Sha512SaltPrefix) - 1;

        if (strncmp(salt, Sha512RoundsPrefix, sizeof(Sha512RoundsPrefix) - 1) == 0)
        {
            char const* num = salt + sizeof(Sha512RoundsPrefix) - 1;
            char* endp;
            uint32 srounds = strtoul(num, &endp, 10);
            if (*endp == '$')
            {
                salt = endp + 1;
                rounds = std::max(RoundsMin, std::min(srounds, RoundsMax));
                roundsCustom = true;
            }
        }

        size_t saltLength = std::min<size_t>(strcspn(salt, "$"), SaltMaxLength);
        size_t keyLength = key.size();

        // Prepare for the real work.
        _hash.Initialize();

        // Add the key string.
        _hash.UpdateData(key);

        // The last part is the salt string. This must be at most 16
        // characters and it ends at the first '$' character (for
        // compatibility with existing implementations).
        _hash.UpdateData((uint8 const*)salt, saltLength);

        // Compute alternate SHA512 sum with input KEY, SALT, and KEY. The
        // final result will be added to the first context.
        _altHash.Initialize();

        // Add key.
        _altHash.UpdateData(key);

        // Add salt.
        _altHash.UpdateData((uint8 const*)salt, saltLength);

        // Add key again.
        _altHash.UpdateData(key);

        // Now get result of this (64 bytes) and add it to the other context.
        _altHash.Finalize();
        uint8 const* altDigest = _altHash.GetDigest();

        // Add for any character in the key one byte of the alternate sum.
        for (cnt = keyLength; cnt > Hash::DigestLength::value; cnt -= Hash::DigestLength::value)
            _hash.UpdateData(altDigest, Hash::DigestLength::value);
        _hash.UpdateData(altDigest, cnt);

        // Take the binary representation of the length of the key and for every
        // 1 add the alternate sum, for every 0 the key.
        for (cnt = keyLength; cnt > 0; cnt >>= 1)
        {
            if ((cnt & 1) != 0)
                _hash.UpdateData(altDigest, Hash::DigestLength::value);
            else
                _hash.UpdateData(key);
        }

        // Create intermediate result.
        _hash.Finalize();
        altDigest = _hash.GetDigest();

        // Start computation of P byte sequence.
        _altHash.Initialize();

        // For every character in the password add the entire password.
        for (cnt = 0; cnt < keyLength; ++cnt)
            _altHash.UpdateData(key);

        // Finish the digest.
        _altHash.Finalize();
        uint8 const* tempDigest = _altHash.GetDigest();

        // Create byte sequence P.
        std::vector<uint8> pBytes(keyLength);
        uint8* pBytesPtr = pBytes.data();
        for (cnt = keyLength; cnt >= Hash::DigestLength::value; cnt -= Hash::DigestLength::value)
        {
            memcpy(pBytesPtr, tempDigest, Hash::DigestLength::value);
            pBytesPtr += Hash::DigestLength::value;
        }
        memcpy(pBytesPtr, tempDigest, cnt);

        // Start computation of S byte sequence.
        _altHash.Initialize();

        // For every character in the password add the entire password.
        for (cnt = 0; cnt < 16u + altDigest[0]; ++cnt)
            _altHash.UpdateData((uint8 const*)salt, saltLength);

        // Finish the digest.
        _altHash.Finalize();
        tempDigest = _altHash.GetDigest();

        // Create byte sequence S.
        std::vector<uint8> sBytes(saltLength);
        uint8* sBytesPtr = sBytes.data();
        for (cnt = saltLength; cnt >= Hash::DigestLength::value; cnt -= Hash::DigestLength::value)
        {
            memcpy(sBytesPtr, tempDigest, Hash::DigestLength::value);
            sBytesPtr += Hash::DigestLength::value;
        }
        memcpy(sBytesPtr, tempDigest, cnt);

        // Repeatedly run the collected hash value through SHA512 to burn CPU cycles.
        for (cnt = 0; cnt < rounds; ++cnt)
        {
            // New context.
            _hash.Initialize();

            // Add key or last result.
            if ((cnt & 1) != 0)
                _hash.UpdateData(pBytesPtr, keyLength);
            else
                _hash.UpdateData(altDigest, Hash::DigestLength::value);

            // Add salt for numbers not divisible by 3.
            if (cnt % 3 != 0)
                _hash.UpdateData(sBytes.data(), saltLength);

            // Add key for numbers not divisible by 7.
            if (cnt % 7 != 0)
                _hash.UpdateData(pBytesPtr, keyLength);

            // Add key or last result.
            if ((cnt & 1) != 0)
                _hash.UpdateData(altDigest, Hash::DigestLength::value);
            else
                _hash.UpdateData(pBytesPtr, keyLength);

            // Create intermediate result.
            _hash.Finalize();
            altDigest = _hash.GetDigest();
        }

        // Now we can construct the result string. It consists of three parts.
        std::ostringstream ret;

        if (fullHash)
        {
            ret << Sha512SaltPrefix;

            if (roundsCustom)
            {
                ret << Sha512RoundsPrefix;
                ret << rounds;
                ret << '$';
            }

            ret.write(salt, saltLength);
            ret << '$';
        }

#define b64_from_24bit(B2, B1, B0, N)\
    do\
    {\
        unsigned int w = ((B2) << 16) | ((B1) << 8) | (B0);\
        int n = (N);\
        while (n-- > 0)\
        {\
            ret << b64t[w & 0x3f];\
            w >>= 6;\
        }\
    } while (0)

        b64_from_24bit(altDigest[0],  altDigest[21], altDigest[42], 4);
        b64_from_24bit(altDigest[22], altDigest[43], altDigest[1],  4);
        b64_from_24bit(altDigest[44], altDigest[2],  altDigest[23], 4);
        b64_from_24bit(altDigest[3],  altDigest[24], altDigest[45], 4);
        b64_from_24bit(altDigest[25], altDigest[46], altDigest[4],  4);
        b64_from_24bit(altDigest[47], altDigest[5],  altDigest[26], 4);
        b64_from_24bit(altDigest[6],  altDigest[27], altDigest[48], 4);
        b64_from_24bit(altDigest[28], altDigest[49], altDigest[7],  4);
        b64_from_24bit(altDigest[50], altDigest[8],  altDigest[29], 4);
        b64_from_24bit(altDigest[9],  altDigest[30], altDigest[51], 4);
        b64_from_24bit(altDigest[31], altDigest[52], altDigest[10], 4);
        b64_from_24bit(altDigest[53], altDigest[11], altDigest[32], 4);
        b64_from_24bit(altDigest[12], altDigest[33], altDigest[54], 4);
        b64_from_24bit(altDigest[34], altDigest[55], altDigest[13], 4);
        b64_from_24bit(altDigest[56], altDigest[14], altDigest[35], 4);
        b64_from_24bit(altDigest[15], altDigest[36], altDigest[57], 4);
        b64_from_24bit(altDigest[37], altDigest[58], altDigest[16], 4);
        b64_from_24bit(altDigest[59], altDigest[17], altDigest[38], 4);
        b64_from_24bit(altDigest[18], altDigest[39], altDigest[60], 4);
        b64_from_24bit(altDigest[40], altDigest[61], altDigest[19], 4);
        b64_from_24bit(altDigest[62], altDigest[20], altDigest[41], 4);
        b64_from_24bit(0,             0,             altDigest[63], 2);

#undef b64_from_24bit

        return ret.str();
    }

private:
    Hash _hash;
    Hash _altHash;
};

/// Returns the SHA512 unix hash of the given content.
TC_COMMON_API std::string CalculateSHA512UnixHash(std::string const& content, std::string const& salt, bool fullHash = true);

#endif // UnixCryptGeneration_h__
