/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AreaTriggerAI.h"
#include "AreaTrigger.h"
#include "CreatureAI.h"

AreaTriggerAI::AreaTriggerAI(AreaTrigger* a) : at(a)
{
}

AreaTriggerAI::~AreaTriggerAI()
{
}

int AreaTriggerAI::Permissible(AreaTrigger const* areaTrigger)
{
    if (areaTrigger->GetAIName() == "AreaTriggerAI")
        return PERMIT_BASE_SPECIAL;
    return PERMIT_BASE_NO;
}

int NullAreaTriggerAI::Permissible(AreaTrigger const* /*areaTrigger*/)
{
    return PERMIT_BASE_IDLE;
}
