/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GameTablesServer_h__
#define GameTablesServer_h__

#include "GameTables.h"

enum AvoidanceDiminishingType
{
    HORIZONTAL_SHIFT = 1,
    VERTICAL_STRETCH = 2
};

struct GtAvoidanceDiminishingEntry
{
    float Rogue = 0.0f;
    float Druid = 0.0f;
    float Hunter = 0.0f;
    float Mage = 0.0f;
    float Paladin = 0.0f;
    float Priest = 0.0f;
    float Shaman = 0.0f;
    float Warlock = 0.0f;
    float Warrior = 0.0f;
    float DeathKnight = 0.0f;
    float Monk = 0.0f;
    float DemonHunter = 0.0f;
};

struct GtChanceToDodgeEntry
{
    float Rogue = 0.0f;
    float Druid = 0.0f;
    float Hunter = 0.0f;
    float Mage = 0.0f;
    float Paladin = 0.0f;
    float Priest = 0.0f;
    float Shaman = 0.0f;
    float Warlock = 0.0f;
    float Warrior = 0.0f;
    float DeathKnight = 0.0f;
    float Monk = 0.0f;
    float DemonHunter = 0.0f;
};

struct GtChanceToDodgeBaseEntry
{
    float Rogue = 0.0f;
    float Druid = 0.0f;
    float Hunter = 0.0f;
    float Mage = 0.0f;
    float Paladin = 0.0f;
    float Priest = 0.0f;
    float Shaman = 0.0f;
    float Warlock = 0.0f;
    float Warrior = 0.0f;
    float DeathKnight = 0.0f;
    float Monk = 0.0f;
    float DemonHunter = 0.0f;
};

struct GtChanceToParryEntry
{
    float Rogue = 0.0f;
    float Druid = 0.0f;
    float Hunter = 0.0f;
    float Mage = 0.0f;
    float Paladin = 0.0f;
    float Priest = 0.0f;
    float Shaman = 0.0f;
    float Warlock = 0.0f;
    float Warrior = 0.0f;
    float DeathKnight = 0.0f;
    float Monk = 0.0f;
    float DemonHunter = 0.0f;
};

struct GtChanceToParryBaseEntry
{
    float Rogue = 0.0f;
    float Druid = 0.0f;
    float Hunter = 0.0f;
    float Mage = 0.0f;
    float Paladin = 0.0f;
    float Priest = 0.0f;
    float Shaman = 0.0f;
    float Warlock = 0.0f;
    float Warrior = 0.0f;
    float DeathKnight = 0.0f;
    float Monk = 0.0f;
    float DemonHunter = 0.0f;
};

struct GtRaceStatsEntry
{
    float Human = 0.0f;
    float Orc = 0.0f;
    float Dwarf = 0.0f;
    float NightElf = 0.0f;
    float Undead = 0.0f;
    float Tauren = 0.0f;
    float Gnome = 0.0f;
    float Troll = 0.0f;
    float Goblin = 0.0f;
    float BloodElf = 0.0f;
    float Draenei = 0.0f;
    float Worgen = 0.0f;
    float Pandaren = 0.0f;
};

TC_GAME_API extern GameTable<GtAvoidanceDiminishingEntry>   sAvoidanceDiminishingGameTable;
TC_GAME_API extern GameTable<GtChanceToDodgeEntry>          sChanceToDodgeGameTable;
TC_GAME_API extern GameTable<GtChanceToDodgeBaseEntry>      sChanceToDodgeBaseGameTable;
TC_GAME_API extern GameTable<GtChanceToParryEntry>          sChanceToParryGameTable;
TC_GAME_API extern GameTable<GtChanceToParryBaseEntry>      sChanceToParryBaseGameTable;
TC_GAME_API extern GameTable<GtRaceStatsEntry>              sRaceStatsGameTable;

TC_GAME_API void LoadGameTablesServer(std::string const& dataPath);

inline float GetRaceStatModifier(GtRaceStatsEntry const* row, uint32 race)
{
    switch (race)
    {
        case RACE_HUMAN:
            return row->Human;
        case RACE_ORC:
            return row->Orc;
        case RACE_DWARF:
            return row->Dwarf;
        case RACE_NIGHTELF:
            return row->NightElf;
        case RACE_UNDEAD_PLAYER:
            return row->Undead;
        case RACE_TAUREN:
            return row->Tauren;
        case RACE_GNOME:
            return row->Gnome;
        case RACE_TROLL:
            return row->Troll;
        case RACE_GOBLIN:
            return row->Goblin;
        case RACE_BLOODELF:
            return row->BloodElf;
        case RACE_DRAENEI:
            return row->Draenei;
        case RACE_WORGEN:
            return row->Worgen;
        case RACE_PANDAREN_NEUTRAL:
        case RACE_PANDAREN_ALLIANCE:
        case RACE_PANDAREN_HORDE:
            return row->Pandaren;
        default:
            break;
    }

    return 0.0f;
}

#endif // GameTablesServer_h__
