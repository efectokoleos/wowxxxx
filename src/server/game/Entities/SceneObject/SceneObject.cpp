/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "DB2Stores.h"
#include "Log.h"
#include "Map.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "Random.h"
#include "SceneObject.h"
#include "SpellInfo.h"
#include "UpdateData.h"

int32 const SceneObject::InfiniteDuration = -1;

SceneObject::SceneObject(bool isWorldObject /*= false*/)
    : WorldObject(isWorldObject), _creator(nullptr), _duration(InfiniteDuration), _spellInfo(nullptr)
{
    m_objectType   |= TYPEMASK_SCENEOBJECT;
    m_objectTypeId  = TYPEID_SCENEOBJECT;
    m_updateFlag    = UPDATEFLAG_STATIONARY_POSITION;
    m_valuesCount   = SCENEOBJECT_END;
}

SceneObject::~SceneObject() 
{
    // make sure all references were properly removed
    ASSERT(!_creator);
}

SceneObject* SceneObject::CreateSceneObject(uint32 scriptPackageID, Unit* creator, Position const& pos, SceneType type /*= SCENE_TYPE_NONE*/, SpellInfo const* spellInfo /*= nullptr*/)
{
    SceneScriptPackageEntry const* sceneScriptPackageEntry = sSceneScriptPackageStore.LookupEntry(scriptPackageID);
    if (!sceneScriptPackageEntry)
    {
        TC_LOG_ERROR("misc", "SceneObject (Entry: %u) not created: non-existing entry in `SceneScriptPackageEntry`. Map: %u (X: %f Y: %f Z: %f, O: %f)", scriptPackageID, creator->GetMapId(), pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation());
        return nullptr;
    }

    ObjectGuid::LowType lowGuid = creator->GetMap()->GenerateLowGuid<HighGuid::SceneObject>();

    SceneObject* sceneObject = new SceneObject(false);
    if (!sceneObject->Create(lowGuid, scriptPackageID, creator->GetMap(), creator, pos, type, spellInfo))
    {
        delete sceneObject;
        return nullptr;
    }

    return sceneObject;
}

bool SceneObject::Create(ObjectGuid::LowType lowGuid, uint32 scriptPackageID, Map* map, Unit* creator, Position const& pos, SceneType type /*= SCENE_TYPE_NONE*/, SpellInfo const* spellInfo /*= nullptr*/)
{
    SceneScriptPackageEntry const* sceneScriptPackageEntry = sSceneScriptPackageStore.LookupEntry(scriptPackageID);
    if (!sceneScriptPackageEntry)
    {
        TC_LOG_ERROR("misc", "SceneObject (Entry: %u) not created: non-existing entry in `SceneScriptPackageEntry`. Map: %u (X: %f Y: %f Z: %f, O: %f)", scriptPackageID, map->GetId(), pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation());
        return false;
    }

    SetMap(map);
    Relocate(pos);
    if (!IsPositionValid())
    {
        TC_LOG_ERROR("misc", "SceneObject (Entry: %u) not created. Suggested coordinates are invalid (X: %f, Y: %f, Z: %f, O: %f)", scriptPackageID, pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation());
        return false;
    }

    SetZoneScript();

    if (!GetLocalScript().empty())
        m_updateFlag |= UPDATEFLAG_SCENEOBJECT;

    Object::_Create(ObjectGuid::Create<HighGuid::SceneObject>(GetMapId(), scriptPackageID, lowGuid));

    SetEntry(scriptPackageID);
    SetObjectScale(1.0f);
    SetGuidValue(SCENEOBJECT_FIELD_CREATEDBY, creator->GetGUID());
    SetUInt32Value(SCENEOBJECT_FIELD_SCRIPT_PACKAGE_ID, scriptPackageID);
    SetUInt32Value(SCENEOBJECT_FIELD_SCENE_TYPE, type);
    SetUInt32Value(SCENEOBJECT_FIELD_RND_SEED_VAL, rand32() % 0xFFFFFFFF);
    // set name for logs usage, doesn't affect anything ingame
    SetName(sceneScriptPackageEntry->Name);
    if (spellInfo)
        _duration = spellInfo->CalcDuration();
    else
        _duration = InfiniteDuration;

    if (IsWorldObject())
        setActive(true);    // must before add to map to be put in world container

    CopyPhaseFrom(creator);

    return GetMap()->AddToMap(this);
}

void SceneObject::Remove()
{
    if (IsInWorld())
    {
        AddObjectToRemoveList(); // calls RemoveFromWorld
    }
}

void SceneObject::AddToWorld()
{
    if (!IsInWorld())
    {
        GetMap()->GetObjectsStore().Insert<SceneObject>(GetGUID(), this);
        WorldObject::AddToWorld();
        BindToCreator();
    }
}

void SceneObject::RemoveFromWorld()
{
    if (IsInWorld())
    {
        UnbindFromCreator();
        WorldObject::RemoveFromWorld();
        GetMap()->GetObjectsStore().Remove<DynamicObject>(GetGUID());
    }
}

void SceneObject::BindToCreator()
{
    ASSERT(!_creator);
    _creator = ObjectAccessor::GetUnit(*this, GetCreatorGUID());
    ASSERT(_creator);
    ASSERT(_creator->GetMap() == GetMap());
    _creator->_RegisterSceneObject(this);
}

void SceneObject::UnbindFromCreator()
{
    ASSERT(_creator);
    _creator->_UnregisterSceneObject(this);
    _creator = nullptr;
}

void SceneObject::Update(uint32 diff)
{
    // caster has to be always available and in the same map
    ASSERT(_creator);
    ASSERT(_creator->GetMap() == GetMap());

    if (_duration != InfiniteDuration)
    {
        if (_duration > int32(diff))
            _duration -= diff;
        else
            Remove(); // expired
    }

    WorldObject::Update(diff);
}

std::string const& SceneObject::GetLocalScript() const
{
    static std::string const localScript;
    return localScript;
}
