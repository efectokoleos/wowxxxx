/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "Object.h"
#include "MapObject.h"

enum SceneType : uint32
{
    SCENE_TYPE_NONE         = 0,
    SCENE_TYPE_BATTLEPET    = 1
};

class TC_GAME_API SceneObject : public WorldObject, public GridObject<SceneObject>, public MapObject
{
public:
    SceneObject(bool isWorldObject = false);
    virtual ~SceneObject();

    static SceneObject* CreateSceneObject(uint32 scriptPackageID, Unit* creator, Position const& pos, SceneType type = SCENE_TYPE_NONE, SpellInfo const* spellInfo = nullptr);
    bool Create(ObjectGuid::LowType lowGuid, uint32 scriptPackageID, Map* map, Unit* creator, Position const& pos, SceneType type = SCENE_TYPE_NONE, SpellInfo const* spellInfo = nullptr);

    void AddToWorld() override;
    void RemoveFromWorld() override;
    void Remove();
    void BindToCreator();
    void UnbindFromCreator();
    ObjectGuid const& GetCreatorGUID() const { return GetGuidValue(SCENEOBJECT_FIELD_CREATEDBY); }
    void Update(uint32 diff) override;
    Unit* GetCreator() const { return _creator; }
    bool IsCreatedBySpell() const { return _spellInfo != nullptr; }
    std::string const& GetLocalScript() const;

protected:
    Unit* _creator;
    int32 _duration;
    SpellInfo const* _spellInfo;

    static int32 const InfiniteDuration;
};

#endif // SCENEOBJECT_H
