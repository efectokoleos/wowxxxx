/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Unit.h"
#include "DB2Stores.h"
#include "Item.h"
#include "Player.h"
#include "Pet.h"
#include "Creature.h"
#include "GameTables.h"
#include "GameTablesServer.h"
#include "SharedDefines.h"
#include "SpellAuras.h"
#include "SpellAuraEffects.h"
#include "World.h"
#include <G3D/g3dmath.h>
#include <numeric>

inline bool _ModifyUInt32(bool apply, uint32& baseValue, int32& amount)
{
    // If amount is negative, change sign and value of apply.
    if (amount < 0)
    {
        apply = !apply;
        amount = -amount;
    }
    if (apply)
        baseValue += amount;
    else
    {
        // Make sure we do not get uint32 overflow.
        if (amount > int32(baseValue))
            amount = baseValue;
        baseValue -= amount;
    }
    return apply;
}

/*#######################################
########                         ########
########    UNIT STAT SYSTEM     ########
########                         ########
#######################################*/

void Unit::UpdateAllResistances()
{
    for (uint8 i = SPELL_SCHOOL_NORMAL; i < MAX_SPELL_SCHOOL; ++i)
        UpdateResistances(i);
}

void Unit::UpdateDamagePhysical(WeaponAttackType attType)
{
    float minDamage = 0.0f;
    float maxDamage = 0.0f;

    CalculateMinMaxDamage(attType, false, true, minDamage, maxDamage);

    switch (attType)
    {
        case BASE_ATTACK:
        default:
            SetStatFloatValue(UNIT_FIELD_MINDAMAGE, minDamage);
            SetStatFloatValue(UNIT_FIELD_MAXDAMAGE, maxDamage);
            break;
        case OFF_ATTACK:
            SetStatFloatValue(UNIT_FIELD_MINOFFHANDDAMAGE, minDamage);
            SetStatFloatValue(UNIT_FIELD_MAXOFFHANDDAMAGE, maxDamage);
            break;
        case RANGED_ATTACK:
            SetStatFloatValue(UNIT_FIELD_MINRANGEDDAMAGE, minDamage);
            SetStatFloatValue(UNIT_FIELD_MAXRANGEDDAMAGE, maxDamage);
            break;
    }
}

/*#######################################
########                         ########
########   PLAYERS STAT SYSTEM   ########
########                         ########
#######################################*/

bool Player::UpdateStats(Stats stat)
{
    // value = ((base_value * base_pct) + total_value) * total_pct
    float value  = GetTotalStatValue(stat);

    SetStat(stat, int32(value));

    if (stat == STAT_STAMINA || stat == STAT_INTELLECT || stat == STAT_STRENGTH)
    {
        Pet* pet = GetPet();
        if (pet)
            pet->UpdateStats(stat);
    }

    switch (stat)
    {
        case STAT_AGILITY:
            UpdateArmor();
            UpdateAllCritPercentages();
            UpdateDodgePercentage();
            break;
        case STAT_STAMINA:
            UpdateMaxHealth();
            break;
        case STAT_INTELLECT:
            UpdateSpellCritChance();
            UpdateArmor();                                  //SPELL_AURA_MOD_RESISTANCE_OF_INTELLECT_PERCENT, only armor currently
            break;
        default:
            break;
    }

    if (stat == STAT_STRENGTH)
        UpdateAttackPowerAndDamage(false);
    else if (stat == STAT_AGILITY)
    {
        UpdateAttackPowerAndDamage(false);
        UpdateAttackPowerAndDamage(true);
    }

    UpdateSpellDamageAndHealingBonus();
    UpdateManaRegen();

    // Update ratings in exist SPELL_AURA_MOD_RATING_FROM_STAT and only depends from stat
    uint32 mask = 0;
    AuraEffectList const& modRatingFromStat = GetAuraEffectsByType(SPELL_AURA_MOD_RATING_FROM_STAT);
    for (AuraEffectList::const_iterator i = modRatingFromStat.begin(); i != modRatingFromStat.end(); ++i)
        if (Stats((*i)->GetMiscValueB()) == stat)
            mask |= (*i)->GetMiscValue();
    if (mask)
    {
        for (uint32 rating = 0; rating < MAX_COMBAT_RATING; ++rating)
            if (mask & (1 << rating))
                ApplyRatingMod(CombatRating(rating), 0, true);
    }
    return true;
}

void Player::ApplySpellPowerBonus(int32 amount, bool apply)
{
    if (HasAuraType(SPELL_AURA_OVERRIDE_SPELL_POWER_BY_AP_PCT))
        return;

    apply = _ModifyUInt32(apply, m_baseSpellPower, amount);

    // For speed just update for client
    ApplyModUInt32Value(PLAYER_FIELD_MOD_HEALING_DONE_POS, amount, apply);
    for (int i = SPELL_SCHOOL_HOLY; i < MAX_SPELL_SCHOOL; ++i)
        ApplyModUInt32Value(PLAYER_FIELD_MOD_DAMAGE_DONE_POS + i, amount, apply);

    if (HasAuraType(SPELL_AURA_OVERRIDE_ATTACK_POWER_BY_SP_PCT))
    {
        UpdateAttackPowerAndDamage();
        UpdateAttackPowerAndDamage(true);
    }
}

void Player::UpdateSpellDamageAndHealingBonus()
{
    // Magic damage modifiers implemented in Unit::SpellDamageBonusDone
    // This information for client side use only
    // Get healing bonus for all schools
    SetStatInt32Value(PLAYER_FIELD_MOD_HEALING_DONE_POS, SpellBaseHealingBonusDone(SPELL_SCHOOL_MASK_ALL));
    // Get damage bonus for all schools
    Unit::AuraEffectList const& modDamageAuras = GetAuraEffectsByType(SPELL_AURA_MOD_DAMAGE_DONE);
    for (uint16 i = SPELL_SCHOOL_HOLY; i < MAX_SPELL_SCHOOL; ++i)
    {
        SetInt32Value(PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + i, std::accumulate(modDamageAuras.begin(), modDamageAuras.end(), 0, [i](int32 negativeMod, AuraEffect const* aurEff)
        {
            if (aurEff->GetAmount() < 0 && aurEff->GetMiscValue() & (1 << i))
                negativeMod += aurEff->GetAmount();
            return negativeMod;
        }));
        SetStatInt32Value(PLAYER_FIELD_MOD_DAMAGE_DONE_POS + i, SpellBaseDamageBonusDone(SpellSchoolMask(1 << i)) - GetInt32Value(PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + i));
    }

    if (HasAuraType(SPELL_AURA_OVERRIDE_ATTACK_POWER_BY_SP_PCT))
    {
        UpdateAttackPowerAndDamage();
        UpdateAttackPowerAndDamage(true);
    }
}

bool Player::UpdateAllStats()
{
    for (int8 i = STAT_STRENGTH; i < MAX_STATS; ++i)
    {
        float value = GetTotalStatValue(Stats(i));
        SetStat(Stats(i), int32(value));
    }

    UpdateArmor();
    // calls UpdateAttackPowerAndDamage() in UpdateArmor for SPELL_AURA_MOD_ATTACK_POWER_OF_ARMOR
    UpdateAttackPowerAndDamage(true);
    UpdateMaxHealth();

    for (uint8 i = POWER_MANA; i < MAX_POWERS; ++i)
        UpdateMaxPower(Powers(i));

    UpdateAllRatings();
    UpdateAllCritPercentages();
    UpdateSpellCritChance();
    UpdateBlockPercentage();
    UpdateParryPercentage();
    UpdateDodgePercentage();
    UpdateSpellDamageAndHealingBonus();
    UpdateManaRegen();
    UpdateExpertise(BASE_ATTACK);
    UpdateExpertise(OFF_ATTACK);
    RecalculateRating(CR_ARMOR_PENETRATION);
    UpdateAllResistances();

    return true;
}

void Player::ApplySpellPenetrationBonus(int32 amount, bool apply)
{
    ApplyModInt32Value(PLAYER_FIELD_MOD_TARGET_RESISTANCE, -amount, apply);
    m_spellPenetrationItemMod += apply ? amount : -amount;
}

void Player::UpdateResistances(uint32 school)
{
    if (school > SPELL_SCHOOL_NORMAL)
    {
        float value  = GetTotalAuraModValue(UnitMods(UNIT_MOD_RESISTANCE_START + school));
        SetResistance(SpellSchools(school), int32(value));

        Pet* pet = GetPet();
        if (pet)
            pet->UpdateResistances(school);
    }
    else
        UpdateArmor();
}

void Player::UpdateArmor()
{
    UnitMods unitMod = UNIT_MOD_ARMOR;

    float value = GetModifierValue(unitMod, BASE_VALUE);    // base armor (from items)
    value *= GetModifierValue(unitMod, BASE_PCT);           // armor percent from items
    value += GetModifierValue(unitMod, TOTAL_VALUE);

    //add dynamic flat mods
    AuraEffectList const& mResbyIntellect = GetAuraEffectsByType(SPELL_AURA_MOD_RESISTANCE_OF_STAT_PERCENT);
    for (AuraEffectList::const_iterator i = mResbyIntellect.begin(); i != mResbyIntellect.end(); ++i)
    {
        if ((*i)->GetMiscValue() & SPELL_SCHOOL_MASK_NORMAL)
            value += CalculatePct(GetStat(Stats((*i)->GetMiscValueB())), (*i)->GetAmount());
    }

    value *= GetModifierValue(unitMod, TOTAL_PCT);

    SetArmor(int32(value));

    Pet* pet = GetPet();
    if (pet)
        pet->UpdateArmor();

    UpdateAttackPowerAndDamage();                           // armor dependent auras update for SPELL_AURA_MOD_ATTACK_POWER_OF_ARMOR
}

float Player::GetHealthBonusFromStamina()
{
    // Taken from PaperDollFrame.lua - 6.0.3.19085
    float ratio = 10.0f;
    if (GtHpPerStaEntry const* hpBase = sHpPerStaGameTable.GetRow(getLevel()))
        ratio = hpBase->Health;

    float stamina = GetStat(STAT_STAMINA);

    return stamina * ratio;
}

void Player::UpdateMaxHealth()
{
    UnitMods unitMod = UNIT_MOD_HEALTH;

    float value = GetModifierValue(unitMod, BASE_VALUE) + GetCreateHealth();
    value *= GetModifierValue(unitMod, BASE_PCT);
    value += GetModifierValue(unitMod, TOTAL_VALUE) + GetHealthBonusFromStamina();
    value *= GetModifierValue(unitMod, TOTAL_PCT);

    SetMaxHealth((uint32)value);
}

void Player::UpdateMaxPower(Powers power)
{
    UnitMods unitMod = UnitMods(UNIT_MOD_POWER_START + power);

    float value = GetModifierValue(unitMod, BASE_VALUE) + GetCreatePowers(power);
    value *= GetModifierValue(unitMod, BASE_PCT);
    value += GetModifierValue(unitMod, TOTAL_VALUE);
    value *= GetModifierValue(unitMod, TOTAL_PCT);

    SetMaxPower(power, uint32(value));
}

void Player::UpdateAttackPowerAndDamage(bool ranged)
{
    float val2 = 0.0f;
    float level = float(getLevel());

    ChrClassesEntry const* entry = sChrClassesStore.AssertEntry(getClass());
    UnitMods unitMod = ranged ? UNIT_MOD_ATTACK_POWER_RANGED : UNIT_MOD_ATTACK_POWER;

    uint16 index = UNIT_FIELD_ATTACK_POWER;
    uint16 index_mod = UNIT_FIELD_ATTACK_POWER_MOD_POS;
    uint16 index_mult = UNIT_FIELD_ATTACK_POWER_MULTIPLIER;

    if (ranged)
    {
        index = UNIT_FIELD_RANGED_ATTACK_POWER;
        index_mod = UNIT_FIELD_RANGED_ATTACK_POWER_MOD_POS;
        index_mult = UNIT_FIELD_RANGED_ATTACK_POWER_MULTIPLIER;
    }

    if (!HasAuraType(SPELL_AURA_OVERRIDE_ATTACK_POWER_BY_SP_PCT))
    {
        if (!ranged)
        {
            float strengthValue = std::max(GetStat(STAT_STRENGTH) * entry->AttackPowerPerStrength, 0.0f);
            float agilityValue = std::max(GetStat(STAT_AGILITY) * entry->AttackPowerPerAgility, 0.0f);

            SpellShapeshiftFormEntry const* form = sSpellShapeshiftFormStore.LookupEntry(GetShapeshiftForm());
            // Directly taken from client, SHAPESHIFT_FLAG_AP_FROM_STRENGTH ?
            if (form && form->Flags & 0x20)
                agilityValue += std::max(GetStat(STAT_AGILITY) * entry->AttackPowerPerStrength, 0.0f);

            val2 = strengthValue + agilityValue;
        }
        else
            val2 = (level + std::max(GetStat(STAT_AGILITY), 0.0f)) * entry->RangedAttackPowerPerAgility;
    }
    else
    {
        int32 minSpellPower = GetInt32Value(PLAYER_FIELD_MOD_HEALING_DONE_POS);
        for (int i = SPELL_SCHOOL_HOLY; i < MAX_SPELL_SCHOOL; ++i)
            minSpellPower = std::min(minSpellPower, GetInt32Value(PLAYER_FIELD_MOD_DAMAGE_DONE_POS + i));

        val2 = CalculatePct(float(minSpellPower), GetFloatValue(PLAYER_FIELD_OVERRIDE_AP_BY_SPELL_POWER_PERCENT));
    }

    SetModifierValue(unitMod, BASE_VALUE, val2);

    float base_attPower  = GetModifierValue(unitMod, BASE_VALUE) * GetModifierValue(unitMod, BASE_PCT);
    float attPowerMod = GetModifierValue(unitMod, TOTAL_VALUE);
    float attPowerMultiplier = GetModifierValue(unitMod, TOTAL_PCT) - 1.0f;

    //add dynamic flat mods
    if (!ranged)
    {
        AuraEffectList const& mAPbyArmor = GetAuraEffectsByType(SPELL_AURA_MOD_ATTACK_POWER_OF_ARMOR);
        for (AuraEffectList::const_iterator iter = mAPbyArmor.begin(); iter != mAPbyArmor.end(); ++iter)
            // always: ((*i)->GetModifier()->m_miscvalue == 1 == SPELL_SCHOOL_MASK_NORMAL)
            attPowerMod += int32(GetArmor() / (*iter)->GetAmount());
    }

    SetInt32Value(index, (uint32)base_attPower);            //UNIT_FIELD_(RANGED)_ATTACK_POWER field
    SetInt32Value(index_mod, (uint32)attPowerMod);          //UNIT_FIELD_(RANGED)_ATTACK_POWER_MOD_POS field
    SetFloatValue(index_mult, attPowerMultiplier);          //UNIT_FIELD_(RANGED)_ATTACK_POWER_MULTIPLIER field

    Pet* pet = GetPet();                                //update pet's AP
    Guardian* guardian = GetGuardianPet();
    //automatically update weapon damage after attack power modification
    if (ranged)
    {
        UpdateDamagePhysical(RANGED_ATTACK);
        if (pet && pet->IsHunterPet()) // At ranged attack change for hunter pet
            pet->UpdateAttackPowerAndDamage();
    }
    else
    {
        UpdateDamagePhysical(BASE_ATTACK);
        if (Item* offhand = GetWeaponForAttack(OFF_ATTACK, true))
            if (CanDualWield() || offhand->GetTemplate()->GetFlags3() & ITEM_FLAG3_ALWAYS_ALLOW_DUAL_WIELD)
                UpdateDamagePhysical(OFF_ATTACK);

        if (HasAuraType(SPELL_AURA_MOD_SPELL_DAMAGE_OF_ATTACK_POWER) ||
            HasAuraType(SPELL_AURA_MOD_SPELL_HEALING_OF_ATTACK_POWER) ||
            HasAuraType(SPELL_AURA_OVERRIDE_SPELL_POWER_BY_AP_PCT))
            UpdateSpellDamageAndHealingBonus();

        if (pet && pet->IsPetGhoul()) // At melee attack power change for DK pet
            pet->UpdateAttackPowerAndDamage();

        if (guardian && guardian->IsSpiritWolf()) // At melee attack power change for Shaman feral spirit
            guardian->UpdateAttackPowerAndDamage();
    }
}

void Player::CalculateMinMaxDamage(WeaponAttackType attType, bool normalized, bool addTotalPct, float& minDamage, float& maxDamage)
{
    UnitMods unitMod;

    switch (attType)
    {
        case BASE_ATTACK:
        default:
            unitMod = UNIT_MOD_DAMAGE_MAINHAND;
            break;
        case OFF_ATTACK:
            unitMod = UNIT_MOD_DAMAGE_OFFHAND;
            break;
        case RANGED_ATTACK:
            unitMod = UNIT_MOD_DAMAGE_RANGED;
            break;
    }

    float baseValue  = GetModifierValue(unitMod, BASE_VALUE) + GetTotalAttackPowerValue(attType) / 3.5f;
    float basePct    = GetModifierValue(unitMod, BASE_PCT);
    float totalValue = GetModifierValue(unitMod, TOTAL_VALUE);
    float totalPct   = addTotalPct ? GetModifierValue(unitMod, TOTAL_PCT) : 1.0f;
    float weaponMinDamage = GetWeaponDamageRange(attType, MINDAMAGE);
    float weaponMaxDamage = GetWeaponDamageRange(attType, MAXDAMAGE);
    float weaponSpeed = std::max(GetAPMultiplier(attType, normalized), 0.25f);

    if (SpellShapeshiftFormEntry const* shapeshift = sSpellShapeshiftFormStore.LookupEntry(GetShapeshiftForm()))
    {
        if (shapeshift->WeaponDamageVariance)
        {
            weaponMinDamage *= shapeshift->WeaponDamageVariance;
            weaponMaxDamage *= shapeshift->WeaponDamageVariance;
        }

        // Weapon damage formula in animal forms (Bear and Cat). Note that if 1-handers are equipped, in animal form the main hand weapon damage is that of a 2-hander of equivalent item level.3
        // http://www.askmrrobot.com/wow/theory/mechanic/function/animalweapondamage?spec=DruidFeral&version=live
        if (!IsTwoHandUsed() && IsInFeralForm())
        {
            weaponMinDamage *= 1.2870013f;
            weaponMaxDamage *= 1.2870013f;
        }

        if (shapeshift->CombatRoundTime)
            weaponSpeed = shapeshift->CombatRoundTime / 1000.0f;
    }
    else if (!CanUseAttackType(attType)) // check if player not in form but still can't use (disarm case)
    {
        // cannot use ranged/off attack, set values to 0
        if (attType != BASE_ATTACK)
        {
            minDamage = 0;
            maxDamage = 0;
            return;
        }
        weaponMinDamage = BASE_MINDAMAGE;
        weaponMaxDamage = BASE_MAXDAMAGE;
    }

    if (IsInFeralForm())
    {
        minDamage = ((weaponMinDamage + baseValue) * basePct + totalValue) * weaponSpeed * totalPct;
        maxDamage = ((weaponMaxDamage + baseValue) * basePct + totalValue) * weaponSpeed * totalPct;
    }
    else
    {
        minDamage = ((weaponMinDamage + baseValue * weaponSpeed) * basePct + totalValue) * totalPct;
        maxDamage = ((weaponMaxDamage + baseValue * weaponSpeed) * basePct + totalValue) * totalPct;
    }
}

void Player::UpdateBlockPercentage()
{
    // No block
    float value = 0.0f;
    if (CanBlock())
    {
        // Base value
        value = 5.0f;
        // Increase from SPELL_AURA_MOD_BLOCK_PERCENT aura
        value += GetTotalAuraModifier(SPELL_AURA_MOD_BLOCK_PERCENT);
        // Increase from rating
        value += GetRatingBonusValue(CR_BLOCK);

        if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
             value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_BLOCK) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_BLOCK) : value;

        value = value < 0.0f ? 0.0f : value;
    }
    SetStatFloatValue(PLAYER_BLOCK_PERCENTAGE, value);
}

void Player::UpdateCritPercentage(WeaponAttackType attType)
{
    BaseModGroup modGroup;
    uint16 index;
    CombatRating cr;

    switch (attType)
    {
        case OFF_ATTACK:
            modGroup = OFFHAND_CRIT_PERCENTAGE;
            index = PLAYER_OFFHAND_CRIT_PERCENTAGE;
            cr = CR_CRIT_MELEE;
            break;
        case RANGED_ATTACK:
            modGroup = RANGED_CRIT_PERCENTAGE;
            index = PLAYER_RANGED_CRIT_PERCENTAGE;
            cr = CR_CRIT_RANGED;
            break;
        case BASE_ATTACK:
        default:
            modGroup = CRIT_PERCENTAGE;
            index = PLAYER_CRIT_PERCENTAGE;
            cr = CR_CRIT_MELEE;
            break;
    }

    float value = GetTotalPercentageModValue(modGroup) + GetRatingBonusValue(cr);
    // Modify crit from weapon skill and maximized defense skill of same level victim difference
    value += (int32(GetMaxSkillValueForLevel()) - int32(GetMaxSkillValueForLevel())) * 0.04f;

    if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
         value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_CRIT) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_CRIT) : value;

    value = value < 0.0f ? 0.0f : value;
    SetStatFloatValue(index, value);
}

void Player::UpdateAllCritPercentages()
{
    float value = 5.0f;

    SetBaseModValue(CRIT_PERCENTAGE, PCT_MOD, value);
    SetBaseModValue(OFFHAND_CRIT_PERCENTAGE, PCT_MOD, value);
    SetBaseModValue(RANGED_CRIT_PERCENTAGE, PCT_MOD, value);

    UpdateCritPercentage(BASE_ATTACK);
    UpdateCritPercentage(OFF_ATTACK);
    UpdateCritPercentage(RANGED_ATTACK);
}

void Player::UpdateMastery()
{
    if (!CanUseMastery())
    {
        SetFloatValue(PLAYER_MASTERY, 0.0f);
        return;
    }

    float value = GetTotalAuraModifier(SPELL_AURA_MASTERY);
    value += GetRatingBonusValue(CR_MASTERY);
    SetFloatValue(PLAYER_MASTERY, value);

    ChrSpecializationEntry const* chrSpec = sChrSpecializationStore.LookupEntry(GetUInt32Value(PLAYER_FIELD_CURRENT_SPEC_ID));
    if (!chrSpec)
        return;

    for (uint32 i = 0; i < MAX_MASTERY_SPELLS; ++i)
    {
        if (Aura* aura = GetAura(chrSpec->MasterySpellID[i]))
        {
            for (SpellEffectInfo const* effect : aura->GetSpellEffectInfos())
            {
                if (!effect)
                    continue;

                float mult = effect->BonusCoefficient;
                if (G3D::fuzzyEq(mult, 0.0f))
                    continue;

                aura->GetEffect(effect->EffectIndex)->ChangeAmount(int32(value * mult));
            }
        }
    }
}

void Player::UpdateParryPercentage()
{
    float valueFromAttributes = 0.0f;
    float value = 0.0f;

    if (CanParry())
    {
        GtChanceToParryBaseEntry const* chanceToDodgeBase = sChanceToParryBaseGameTable.GetRow(getLevel());
        GtChanceToParryEntry const* chanceToDodge = sChanceToParryGameTable.GetRow(getLevel());
        float baseParry = chanceToDodgeBase ? GetGameTableColumnForClass(chanceToDodgeBase, getClass()) : 0.0f;
        float parryPerStrength = chanceToDodgeBase ? GetGameTableColumnForClass(chanceToDodge, getClass()) : 0.0f;
        float raceBaseStrength = GetRaceStatModifier(ASSERT_NOTNULL(sRaceStatsGameTable.GetRow(STAT_STRENGTH)), getRace());

        float horizontalShift = 1.f / GetGameTableColumnForClass(ASSERT_NOTNULL(sAvoidanceDiminishingGameTable.GetRow(HORIZONTAL_SHIFT)), getClass());
        float verticalStretch = GetGameTableColumnForClass(ASSERT_NOTNULL(sAvoidanceDiminishingGameTable.GetRow(VERTICAL_STRETCH)), getClass());

        // Parry from SPELL_AURA_MOD_PARRY_PERCENT aura
        baseParry += GetTotalAuraModifier(SPELL_AURA_MOD_PARRY_PERCENT);

        // Parry Race Bonus
        if (G3D::fuzzyNe(parryPerStrength, 0.0f))
            baseParry += raceBaseStrength / parryPerStrength;

        // Parry from rating
        float bonusParry = GetRatingBonusValue(CR_PARRY);
        if (G3D::fuzzyNe(parryPerStrength, 0.0f))
            bonusParry += (GetStat(STAT_STRENGTH) - GetCreateStat(STAT_STRENGTH)) / parryPerStrength;

        // apply diminishing formula to diminishing parry chance
        valueFromAttributes = bonusParry;
        value = baseParry + bonusParry / (bonusParry * verticalStretch + horizontalShift);

        if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
            value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_PARRY) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_PARRY) : value;

        value = value < 0.0f ? 0.0f : value;
    }

    SetStatFloatValue(PLAYER_PARRY_PERCENTAGE_FROM_ATTRIBUTE, valueFromAttributes);
    SetStatFloatValue(PLAYER_PARRY_PERCENTAGE, value);
}

void Player::UpdateDodgePercentage()
{
    float valueFromAttributes = 0.0f;
    float value = 0.0f;

    if (HasSpell(81)) // Dodge
    {
        GtChanceToDodgeBaseEntry const* chanceToDodgeBase = sChanceToDodgeBaseGameTable.GetRow(getLevel());
        GtChanceToDodgeEntry const* chanceToDodge = sChanceToDodgeGameTable.GetRow(getLevel());
        float baseDodge = chanceToDodgeBase ? GetGameTableColumnForClass(chanceToDodgeBase, getClass()) : 0.0f;
        float dodgePerAgility = chanceToDodge ? GetGameTableColumnForClass(chanceToDodge, getClass()) : 0.0f;
        float raceBaseAgility = GetRaceStatModifier(ASSERT_NOTNULL(sRaceStatsGameTable.GetRow(STAT_AGILITY)), getRace());

        float horizontalShift = 1.f / GetGameTableColumnForClass(ASSERT_NOTNULL(sAvoidanceDiminishingGameTable.GetRow(HORIZONTAL_SHIFT)), getClass());
        float verticalStretch = GetGameTableColumnForClass(ASSERT_NOTNULL(sAvoidanceDiminishingGameTable.GetRow(VERTICAL_STRETCH)), getClass());

        // Dodge from SPELL_AURA_MOD_DODGE_PERCENT aura
        baseDodge += GetTotalAuraModifier(SPELL_AURA_MOD_DODGE_PERCENT);

        // Dodge Race Bonus
        if (G3D::fuzzyNe(dodgePerAgility, 0.0f))
            baseDodge += raceBaseAgility / dodgePerAgility;

        // Dodge from rating
        float bonusDodge = GetRatingBonusValue(CR_DODGE);
        if (G3D::fuzzyNe(dodgePerAgility, 0.0f))
            bonusDodge += (GetStat(STAT_AGILITY) - GetCreateStat(STAT_AGILITY)) / dodgePerAgility;

        // apply diminishing formula to diminishing dodge chance
        valueFromAttributes = bonusDodge;
        value = baseDodge + bonusDodge / (bonusDodge * verticalStretch + horizontalShift);

        if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
            value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_DODGE) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_DODGE) : value;

        value = value < 0.0f ? 0.0f : value;
    }

    SetStatFloatValue(PLAYER_DODGE_PERCENTAGE_FROM_ATTRIBUTE, valueFromAttributes);
    SetStatFloatValue(PLAYER_DODGE_PERCENTAGE, value);
}

void Player::UpdateSpellCritChance()
{
    float crit = 5.0f;
    // Increase crit from SPELL_AURA_MOD_SPELL_CRIT_CHANCE
    crit += GetTotalAuraModifier(SPELL_AURA_MOD_SPELL_CRIT_CHANCE);
    // Increase crit from SPELL_AURA_MOD_CRIT_PCT
    crit += GetTotalAuraModifier(SPELL_AURA_MOD_CRIT_PCT);
    // Increase crit from spell crit ratings
    crit += GetRatingBonusValue(CR_CRIT_SPELL);

    // Store crit value
    SetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1, crit);
}

void Player::UpdateArmorPenetration(int32 amount)
{
    // Store Rating Value
    SetUInt32Value(PLAYER_FIELD_COMBAT_RATING_1 + CR_ARMOR_PENETRATION, amount);
}

void Player::UpdateMeleeHitChances()
{
    m_modMeleeHitChance = 7.5f + (float)GetTotalAuraModifier(SPELL_AURA_MOD_HIT_CHANCE);
    m_modMeleeHitChance += GetRatingBonusValue(CR_HIT_MELEE);
}

void Player::UpdateRangedHitChances()
{
    m_modRangedHitChance = 7.5f + (float)GetTotalAuraModifier(SPELL_AURA_MOD_HIT_CHANCE);
    m_modRangedHitChance += GetRatingBonusValue(CR_HIT_RANGED);
}

void Player::UpdateSpellHitChances()
{
    m_modSpellHitChance = 15.0f + (float)GetTotalAuraModifier(SPELL_AURA_MOD_SPELL_HIT_CHANCE);
    m_modSpellHitChance += GetRatingBonusValue(CR_HIT_SPELL);
}

void Player::UpdateLeech()
{
    float leechPct = GetRatingBonusValue(CR_LIFESTEAL);
    leechPct += GetTotalAuraModifier(SPELL_AURA_MOD_LEECH);
    SetStatFloatValue(PLAYER_LIFESTEAL, leechPct);
}

void Player::UpdateAvoidancePercentage()
{
    SetStatFloatValue(PLAYER_AVOIDANCE, std::min(20.0f, GetRatingBonusValue(CR_AVOIDANCE)));
}

void Player::UpdateExpertise(WeaponAttackType attack)
{
    if (attack == RANGED_ATTACK)
        return;

    int32 expertise = int32(GetRatingBonusValue(CR_EXPERTISE));

    Item* weapon = GetWeaponForAttack(attack, true);

    AuraEffectList const& expAuras = GetAuraEffectsByType(SPELL_AURA_MOD_EXPERTISE);
    for (AuraEffectList::const_iterator itr = expAuras.begin(); itr != expAuras.end(); ++itr)
    {
        // item neutral spell
        if ((*itr)->GetSpellInfo()->EquippedItemClass == -1)
            expertise += (*itr)->GetAmount();
        // item dependent spell
        else if (weapon && weapon->IsFitToSpellRequirements((*itr)->GetSpellInfo()))
            expertise += (*itr)->GetAmount();
    }

    if (expertise < 0)
        expertise = 0;

    switch (attack)
    {
        case BASE_ATTACK:
            SetUInt32Value(PLAYER_EXPERTISE, expertise);
            break;
        case OFF_ATTACK:
            SetUInt32Value(PLAYER_OFFHAND_EXPERTISE, expertise);
            break;
        default:
            break;
    }
}

void Player::ApplyManaRegenBonus(int32 amount, bool apply)
{
    _ModifyUInt32(apply, m_baseManaRegen, amount);
    UpdateManaRegen();
}

void Player::ApplyHealthRegenBonus(int32 amount, bool apply)
{
    _ModifyUInt32(apply, m_baseHealthRegen, amount);
}

void Player::UpdateManaRegen()
{
    uint32 manaIndex = GetPowerIndex(POWER_MANA);
    if (manaIndex == MAX_POWERS)
         return;

    // some classes use alternate powers, yet they must be able to regenerate mana aswell (ie: Enhancement shamans with Maelstrom, Shadow priests with Insanity etc.)
    if (getPowerType() != POWER_MANA && !IsInFeralForm() && !HasAuraTypeWithMiscvalue(SPELL_AURA_MOD_POWER_DISPLAY, getPowerType()))
        return;

    uint32 base_mana = GetCreateMana() * GetTotalAuraMultiplier(SPELL_AURA_MOD_MANA_REGEN_FROM_MANA_PCT);
    float combatRegen = 0.004f * base_mana + (GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_POWER_REGEN, POWER_MANA) / 5.0f);
    combatRegen *= GetTotalAuraMultiplierByMiscValue(SPELL_AURA_MOD_POWER_REGEN_PERCENT, POWER_MANA);

    if (HasAuraType(SPELL_AURA_MOD_MANA_REGEN_BY_HASTE))
    {
        float meleeHaste = GetFloatValue(UNIT_FIELD_MOD_HASTE);
        combatRegen *= meleeHaste;
    }

    float multiplier = 1.f;
    switch (GetUInt32Value(PLAYER_FIELD_CURRENT_SPEC_ID))
    {
        case TALENT_SPEC_MAGE_ARCANE:
            multiplier = 3.142f;
            break;
        case TALENT_SPEC_DRUID_RESTORATION:
        case TALENT_SPEC_DRUID_BALANCE:
        case TALENT_SPEC_DRUID_CAT:
        case TALENT_SPEC_DRUID_BEAR:
        case TALENT_SPEC_MAGE_FIRE:
        case TALENT_SPEC_MAGE_FROST:
        case TALENT_SPEC_MONK_MISTWEAVER:
        case TALENT_SPEC_PALADIN_HOLY:
        case TALENT_SPEC_PALADIN_PROTECTION:
        case TALENT_SPEC_PRIEST_DISCIPLINE:
        case TALENT_SPEC_PRIEST_HOLY:
        case TALENT_SPEC_SHAMAN_RESTORATION:
        case TALENT_SPEC_WARLOCK_AFFLICTION:
        case TALENT_SPEC_WARLOCK_DEMONOLOGY:
        case TALENT_SPEC_WARLOCK_DESTRUCTION:
        case TALENT_SPEC_PALADIN_RETRIBUTION:
        case TALENT_SPEC_PRIEST_SHADOW:
        case TALENT_SPEC_SHAMAN_ELEMENTAL:
        case TALENT_SPEC_SHAMAN_ENHANCEMENT:
            multiplier = 2.5f;
            break;
        default:
            break;
    }

    combatRegen *= multiplier;
    SetStatFloatValue(UNIT_FIELD_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER, combatRegen);
    SetStatFloatValue(UNIT_FIELD_POWER_REGEN_FLAT_MODIFIER, combatRegen);
}

void Player::UpdateAllRunesRegen()
{
    if (getClass() != CLASS_DEATH_KNIGHT)
        return;

    uint32 runeIndex = GetPowerIndex(POWER_RUNES);
    if (runeIndex == MAX_POWERS)
        return;

    uint32 cooldown = GetRuneBaseCooldown();
    SetStatFloatValue(UNIT_FIELD_POWER_REGEN_FLAT_MODIFIER + runeIndex, float(1 * IN_MILLISECONDS) / float(cooldown));
    SetStatFloatValue(UNIT_FIELD_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + runeIndex, float(1 * IN_MILLISECONDS) / float(cooldown));
}

void Player::UpdateInsanityRegen()
{
    if (getClass() != CLASS_PRIEST)
        return;

    uint32 powerIndex = GetPowerIndex(POWER_INSANITY);
    if (powerIndex == MAX_POWERS)
        return;

    float totalValue = 0.f;
    AuraEffectList const& auras = GetAuraEffectsByType(SPELL_AURA_MOD_POWER_REGEN);
    for (AuraEffectList::const_iterator i = auras.begin(); i != auras.end(); ++i)
        if ((*i)->GetMiscValue() == POWER_INSANITY)
            totalValue += (*i)->GetAmount();

    totalValue = (totalValue / 500) * 100;

    // Void torrent - Don't allow any power updates except disable energy reg during this time
    if ((HasAura(205065) || HasAura(47585) || HasAura(219772)) && totalValue != 0)
        return;

    SetFloatValue(UNIT_FIELD_POWER_REGEN_FLAT_MODIFIER + powerIndex, totalValue);
    SetFloatValue(UNIT_FIELD_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + powerIndex, totalValue);
}

void Player::UpdateFuryRegen()
{
    if (getClass() != CLASS_DEMON_HUNTER)
        return;

    uint32 powerIndex = GetPowerIndex(POWER_FURY);
    if (powerIndex == MAX_POWERS)
        return;

    float totalValue = float(GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_POWER_REGEN, POWER_FURY) / 5);

    SetFloatValue(UNIT_FIELD_POWER_REGEN_FLAT_MODIFIER + powerIndex, totalValue);
    SetFloatValue(UNIT_FIELD_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + powerIndex, totalValue);
}

void Player::_ApplyAllStatBonuses()
{
    SetCanModifyStats(false);

    _ApplyAllAuraStatMods();
    _ApplyAllItemMods();

    SetCanModifyStats(true);

    UpdateAllStats();
}

void Player::_RemoveAllStatBonuses()
{
    SetCanModifyStats(false);

    _RemoveAllItemMods();
    _RemoveAllAuraStatMods();

    SetCanModifyStats(true);

    UpdateAllStats();
}

/*#######################################
########                         ########
########    MOBS STAT SYSTEM     ########
########                         ########
#######################################*/

bool Creature::UpdateStats(Stats /*stat*/)
{
    return true;
}

bool Creature::UpdateAllStats()
{
    UpdateMaxHealth();
    UpdateAttackPowerAndDamage();
    UpdateAttackPowerAndDamage(true);

    for (uint8 i = POWER_MANA; i < MAX_POWERS; ++i)
        UpdateMaxPower(Powers(i));

    UpdateAllResistances();

    return true;
}

void Creature::UpdateResistances(uint32 school)
{
    if (school > SPELL_SCHOOL_NORMAL)
    {
        float value  = GetTotalAuraModValue(UnitMods(UNIT_MOD_RESISTANCE_START + school));
        SetResistance(SpellSchools(school), int32(value));
    }
    else
        UpdateArmor();
}

void Creature::UpdateArmor()
{
    float value = GetTotalAuraModValue(UNIT_MOD_ARMOR);
    SetArmor(int32(value));
}

void Creature::UpdateMaxHealth()
{
    float value = GetTotalAuraModValue(UNIT_MOD_HEALTH);
    SetMaxHealth(uint32(value));
}

void Creature::UpdateMaxPower(Powers power)
{
    UnitMods unitMod = UnitMods(UNIT_MOD_POWER_START + power);

    float value  = GetTotalAuraModValue(unitMod);
    SetMaxPower(power, uint32(value));
}

void Creature::UpdateAttackPowerAndDamage(bool ranged)
{
    UnitMods unitMod = ranged ? UNIT_MOD_ATTACK_POWER_RANGED : UNIT_MOD_ATTACK_POWER;

    uint16 index = UNIT_FIELD_ATTACK_POWER;
    uint16 indexMulti = UNIT_FIELD_ATTACK_POWER_MULTIPLIER;

    if (ranged)
    {
        index = UNIT_FIELD_RANGED_ATTACK_POWER;
        indexMulti = UNIT_FIELD_RANGED_ATTACK_POWER_MULTIPLIER;
    }

    float baseAttackPower       = GetModifierValue(unitMod, BASE_VALUE) * GetModifierValue(unitMod, BASE_PCT);
    float attackPowerMultiplier = GetModifierValue(unitMod, TOTAL_PCT) - 1.0f;

    SetInt32Value(index, uint32(baseAttackPower));      // UNIT_FIELD_(RANGED)_ATTACK_POWER
    SetFloatValue(indexMulti, attackPowerMultiplier);   // UNIT_FIELD_(RANGED)_ATTACK_POWER_MULTIPLIER

    // automatically update weapon damage after attack power modification
    if (ranged)
        UpdateDamagePhysical(RANGED_ATTACK);
    else
    {
        UpdateDamagePhysical(BASE_ATTACK);
        UpdateDamagePhysical(OFF_ATTACK);
    }
}

void Creature::CalculateMinMaxDamage(WeaponAttackType attType, bool normalized, bool addTotalPct, float& minDamage, float& maxDamage)
{
    float variance = 1.0f;
    UnitMods unitMod;
    switch (attType)
    {
        case BASE_ATTACK:
        default:
            variance = GetCreatureTemplate()->BaseVariance;
            unitMod = UNIT_MOD_DAMAGE_MAINHAND;
            break;
        case OFF_ATTACK:
            variance = GetCreatureTemplate()->BaseVariance;
            unitMod = UNIT_MOD_DAMAGE_OFFHAND;
            break;
        case RANGED_ATTACK:
            variance = GetCreatureTemplate()->RangeVariance;
            unitMod = UNIT_MOD_DAMAGE_RANGED;
            break;
    }

    if (attType == OFF_ATTACK && !haveOffhandWeapon())
    {
        minDamage = 0.0f;
        maxDamage = 0.0f;
        return;
    }

    float weaponMinDamage = GetWeaponDamageRange(attType, MINDAMAGE);
    float weaponMaxDamage = GetWeaponDamageRange(attType, MAXDAMAGE);

    if (!CanUseAttackType(attType)) // disarm case
    {
        weaponMinDamage = 0.0f;
        weaponMaxDamage = 0.0f;
    }

    float attackPower      = GetTotalAttackPowerValue(attType);
    float attackSpeedMulti = GetAPMultiplier(attType, normalized);
    float baseValue        = GetModifierValue(unitMod, BASE_VALUE) + (attackPower / 3.5f) * variance;
    float basePct          = GetModifierValue(unitMod, BASE_PCT) * attackSpeedMulti;
    float totalValue       = GetModifierValue(unitMod, TOTAL_VALUE);
    float totalPct         = addTotalPct ? GetModifierValue(unitMod, TOTAL_PCT) : 1.0f;
    float dmgMultiplier    = GetCreatureTemplate()->ModDamage; // = ModDamage * _GetDamageMod(rank);

    minDamage = ((weaponMinDamage + baseValue) * dmgMultiplier * basePct + totalValue) * totalPct;
    maxDamage = ((weaponMaxDamage + baseValue) * dmgMultiplier * basePct + totalValue) * totalPct;
}

/*#######################################
########                         ########
########    PETS STAT SYSTEM     ########
########                         ########
#######################################*/

bool Guardian::UpdateStats(Stats stat)
{
    // value = ((base_value * base_pct) + total_value) * total_pct
    float value  = GetTotalStatValue(stat);
    ApplyStatBuffMod(stat, m_statFromOwner[stat], false);
    float ownersBonus = 0.0f;

    Unit* owner = GetOwner();
    // Handle Death Knight Glyphs and Talents
    float mod = 0.75f;
    if (IsPetGhoul() && (stat == STAT_STAMINA || stat == STAT_STRENGTH))
    {
        if (stat == STAT_STAMINA)
            mod = 0.3f; // Default Owner's Stamina scale
        else
            mod = 0.7f; // Default Owner's Strength scale

        ownersBonus = float(owner->GetStat(stat)) * mod;
        value += ownersBonus;
    }
    else if (stat == STAT_STAMINA)
    {
        ownersBonus = CalculatePct(owner->GetStat(STAT_STAMINA), 30);
        value += ownersBonus;
    }
                                                            //warlock's and mage's pets gain 30% of owner's intellect
    else if (stat == STAT_INTELLECT)
    {
        if (owner->getClass() == CLASS_MAGE)
        {
            ownersBonus = CalculatePct(owner->GetStat(stat), 30);
            value += ownersBonus;
        }
    }
/*
    else if (stat == STAT_STRENGTH)
    {
        if (IsPetGhoul())
            value += float(owner->GetStat(stat)) * 0.3f;
    }
*/

    SetStat(stat, int32(value));
    m_statFromOwner[stat] = ownersBonus;
    ApplyStatBuffMod(stat, m_statFromOwner[stat], true);

    switch (stat)
    {
        case STAT_STRENGTH:         UpdateAttackPowerAndDamage();                               break;
        case STAT_AGILITY:          UpdateArmor();                                              break;
        case STAT_STAMINA:          UpdateMaxHealth();                                          break;
        case STAT_INTELLECT:        UpdateMaxPower(POWER_MANA); UpdateAttackPowerAndDamage();   break;
        default:
            break;
    }

    return true;
}

bool Guardian::UpdateAllStats()
{
    for (uint8 i = STAT_STRENGTH; i < MAX_STATS; ++i)
        UpdateStats(Stats(i));

    for (uint8 i = POWER_MANA; i < MAX_POWERS; ++i)
        UpdateMaxPower(Powers(i));

    UpdateAllResistances();

    return true;
}

void Guardian::UpdateResistances(uint32 school)
{
    if (school > SPELL_SCHOOL_NORMAL)
    {
        float value  = GetTotalAuraModValue(UnitMods(UNIT_MOD_RESISTANCE_START + school));

        // hunter and warlock pets gain 40% of owner's resistance
        if (IsPet())
            value += float(CalculatePct(m_owner->GetResistance(SpellSchools(school)), 40));

        SetResistance(SpellSchools(school), int32(value));
    }
    else
        UpdateArmor();
}

void Guardian::UpdateArmor()
{
    float value = 0.0f;
    float bonus_armor = 0.0f;
    UnitMods unitMod = UNIT_MOD_ARMOR;

    // hunter pets gain 35% of owner's armor value, warlock pets gain 100% of owner's armor
    if (IsHunterPet())
        bonus_armor = float(CalculatePct(m_owner->GetArmor(), 70));
    else if (IsPet())
        bonus_armor = m_owner->GetArmor();

    value  = GetModifierValue(unitMod, BASE_VALUE);
    value *= GetModifierValue(unitMod, BASE_PCT);
    value += GetModifierValue(unitMod, TOTAL_VALUE) + bonus_armor;

    float pctFromOwnerArmor = 0.0f;
    PetType petType = IsHunterPet() ? HUNTER_PET : SUMMON_PET;

    // simplified armor scaling based on owner's
    switch (petType)
    {
        case SUMMON_PET:
            switch (GetEntry())
            {
                // Mage
                case ENTRY_WATER_ELEMENTAL:
                    pctFromOwnerArmor = 300.f;
                    break;
                // Warlock
                case ENTRY_FELGUARD:
                case ENTRY_VOIDWALKER:
                case ENTRY_INFERNAL:
                case ENTRY_INFERNAL_LORD_OF_FLAMES:
                case ENTRY_INFERNAL_PET:
                    pctFromOwnerArmor = 400.f;
                    break;
                case ENTRY_FELHUNTER:
                case ENTRY_IMP:
                case ENTRY_SUCCUBUS:
                case ENTRY_DOOMGUARD:
                case ENTRY_DOOMGUARD_PET:
                case ENTRY_DREADSTALKER:
                case ENTRY_WILD_IMP:
                case ENTRY_WILD_IMP_DREADSTALKER:
                    pctFromOwnerArmor = 300.f;
                    break;
                // Hunter
                case ENTRY_HATI_NORMAL:
                case ENTRY_HATI_HIDDEN_SKIN:
                case ENTRY_HATI_HIDDEN_SKIN_2:
                case ENTRY_HATI_HIDDEN_SKIN_3:
                case ENTRY_HATI_HIDDEN_SKIN_4:
                case ENTRY_DARK_MINION:
                    pctFromOwnerArmor = 170.0f;
                    break;
                case ENTRY_SPITTING_COBRA:
                    pctFromOwnerArmor = 125.00f;
                    break;
                default:
                    if (IsDireBeastSummon())
                    {
                        pctFromOwnerArmor = 125.00f;
                        break;
                    }
                    break;
            }
            break;
        case HUNTER_PET:
            pctFromOwnerArmor = 170.f;
            break;
        default:
            break;
    }

    if (pctFromOwnerArmor)
        value = CalculatePct(m_owner->GetArmor(), pctFromOwnerArmor);

    value *= GetModifierValue(unitMod, TOTAL_PCT);

    SetArmor(int32(value));
}

void Guardian::UpdateMaxHealth()
{
    UnitMods unitMod = UNIT_MOD_HEALTH;
    float stamina = GetStat(STAT_STAMINA) - GetCreateStat(STAT_STAMINA);
    float value = GetModifierValue(unitMod, BASE_VALUE) + GetCreateHealth();
    value *= GetModifierValue(unitMod, BASE_PCT);
    value += GetModifierValue(unitMod, TOTAL_VALUE) + stamina;
    value *= GetModifierValue(unitMod, TOTAL_PCT);

    float pctFromOwnerHealth = 0.0f;
    PetType petType = IsHunterPet() ? HUNTER_PET : SUMMON_PET;

    // simplified health scaling based on owner's
    switch (petType)
    {
        case SUMMON_PET:
        {
            switch (GetEntry())
            {
                // Deathknight
                case ENTRY_BLOODWORM:
                    pctFromOwnerHealth = 15.f;
                    break;
                case ENTRY_RISEN_SKULKER:
                    pctFromOwnerHealth = 20.f;
                    break;
                case ENTRY_ARMY_OF_THE_DEAD_GHOUL:
                case ENTRY_GHOUL:
                case ENTRY_ARMY_OF_THE_DEAD_APOCALYPSE:
                case ENTRY_SHAMBLING_HORROR:
                    pctFromOwnerHealth = 35.f;
                    break;
                case ENTRY_ABOMINATION:
                    pctFromOwnerHealth = 40.f;
                    break;
                case ENTRY_EBON_GARGOYLE:
                case ENTRY_VALKYR_BATTLEMAIDEN:
                    pctFromOwnerHealth = 50.f;
                    break;
                // Druid
                case ENTRY_TREANT_BALANCE:
                case ENTRY_TREANT_FERAL:
                case ENTRY_TREANT_GUARDIAN:
                case ENTRY_TREANT_RESTORATION:
                    pctFromOwnerHealth = 54.f;
                    break;
                // Mage
                case ENTRY_WATER_ELEMENTAL:
                    pctFromOwnerHealth = 50.f;
                    break;
                case ENTRY_ARCANE_FAMILIAR:
                    pctFromOwnerHealth = 75.f;
                    break;
                case ENTRY_MIRROR_IMAGE_ARCANE:
                case ENTRY_MIRROR_IMAGE_FIRE:
                case ENTRY_MIRROR_IMAGE_FROST:
                    pctFromOwnerHealth = 10.f;
                    break;
                // Monk
                case ENTRY_EARTH_SPIRIT:
                case ENTRY_FIRE_SPIRIT:
                case ENTRY_BLACK_OX_STATUE:
                case ENTRY_JADE_SERPENT_STATUE:
                    pctFromOwnerHealth = 50.f;
                    break;
                case ENTRY_XUEN:
                case ENTRY_NIUZAO:
                case ENTRY_CHI_JI:
                    pctFromOwnerHealth = 100.f;
                    break;
                // Priest
                case ENTRY_SHADOWFIEND:
                case ENTRY_MINDBENDER:
                    pctFromOwnerHealth = 30.f;
                    break;
                case ENTRY_PSYFIEND:
                    SetMaxHealth(10);
                    return;
                case ENTRY_TUURE:
                    pctFromOwnerHealth = 50.f;
                    break;
                // Shaman
                case ENTRY_GREATER_EARTH_ELEMENTAL:
                case ENTRY_PRIMAL_EARTH_ELEMENTAL:
                    pctFromOwnerHealth = 150.f;
                    break;
                case ENTRY_GREATER_FIRE_ELEMENTAL:
                case ENTRY_GREATER_STORM_ELEMENTAL:
                case ENTRY_GREATER_LIGHTNING_ELEMENTAL:
                case ENTRY_PRIMAL_FIRE_ELEMENTAL:
                case ENTRY_PRIMAL_STORM_ELEMENTAL:
                    pctFromOwnerHealth = 75.f;
                    break;
                case ENTRY_SPIRIT_WOLF:
                case ENTRY_SPIRIT_DOOM_WOLF:
                    pctFromOwnerHealth = 30.f;
                    break;
                // Rogue
                case ENTRY_SHADOW_REFLECTION:
                    pctFromOwnerHealth = 20.f;
                    break;
                // Warlock
                case ENTRY_FELGUARD:
                case ENTRY_VOIDWALKER:
                case ENTRY_INFERNAL:
                case ENTRY_INFERNAL_LORD_OF_FLAMES:
                case ENTRY_INFERNAL_PET:
                    pctFromOwnerHealth = 50.f;
                    break;
                case ENTRY_FELHUNTER:
                case ENTRY_SUCCUBUS:
                case ENTRY_DOOMGUARD:
                case ENTRY_DOOMGUARD_PET:
                case ENTRY_DREADSTALKER:
                case ENTRY_DARKGLARE:
                    pctFromOwnerHealth = 40.f;
                    break;
                case ENTRY_IMP:
                    pctFromOwnerHealth = 30.f;
                    break;
                case ENTRY_WILD_IMP:
                case ENTRY_WILD_IMP_DREADSTALKER:
                    pctFromOwnerHealth = 15.f;
                    break;
                case ENTRY_GUARDIAN_OF_THE_FORGOTTEN_QUEEN:
                    pctFromOwnerHealth = 300.00f;
                    break;
                // Hunter
                case ENTRY_HATI_NORMAL:
                case ENTRY_HATI_HIDDEN_SKIN:
                case ENTRY_HATI_HIDDEN_SKIN_2:
                case ENTRY_HATI_HIDDEN_SKIN_3:
                case ENTRY_HATI_HIDDEN_SKIN_4:
                case ENTRY_DARK_MINION:
                    pctFromOwnerHealth = 70.f;
                    break;
                case ENTRY_SPITTING_COBRA:
                    pctFromOwnerHealth = 20.00f;
                    break;
                default:
                    if (IsDireBeastSummon())
                    {
                        pctFromOwnerHealth = 20.00f;
                        break;
                    }
                    break;
            }
            break;
        }
        case HUNTER_PET:
            pctFromOwnerHealth = 70.f;
            break;
        default:
            break;
    }

    // Dire Beast (Hunter)
    if (m_Properties && m_Properties->ID == 3236)
        pctFromOwnerHealth = 20.f;

    if (pctFromOwnerHealth)
        if (Unit* owner = GetOwner())
            value = CalculatePct(owner->GetMaxHealth(), pctFromOwnerHealth);

    value *= GetModifierValue(unitMod, TOTAL_PCT);
    SetMaxHealth((uint32)value);
}

void Guardian::UpdateMaxPower(Powers power)
{
    UnitMods unitMod = UnitMods(UNIT_MOD_POWER_START + power);

    float addValue = (power == POWER_MANA) ? GetStat(STAT_INTELLECT) - GetCreateStat(STAT_INTELLECT) : 0.0f;
    float multiplicator = 15.0f;
    float value  = GetModifierValue(unitMod, BASE_VALUE) + GetCreatePowers(power);
    value *= GetModifierValue(unitMod, BASE_PCT);
    value += GetModifierValue(unitMod, TOTAL_VALUE) + addValue * multiplicator;
    value *= GetModifierValue(unitMod, TOTAL_PCT);

    SetMaxPower(power, uint32(value));
}

void Guardian::UpdateAttackPowerAndDamage(bool ranged)
{
    if (ranged)
        return;

    float val = 0.0f;
    float bonusAP = 0.0f;
    UnitMods unitMod = UNIT_MOD_ATTACK_POWER;

    if (GetEntry() == ENTRY_IMP)
        val = GetStat(STAT_STRENGTH) - 10.0f;
    else
        val = 2 * GetStat(STAT_STRENGTH) - 20.0f;

    Unit* owner = GetOwner();
    if (owner && owner->GetTypeId() == TYPEID_PLAYER)
    {
        switch (GetEntry())
        {
            // Mage
            case ENTRY_WATER_ELEMENTAL:
                // Water Elemental benefits from Mage's frost damage
                SetBonusDamage(int32(owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_FROST) * 0.75f));
                break;
            // Shaman
            case ENTRY_SPIRIT_WOLF:
            case ENTRY_SPIRIT_DOOM_WOLF:
                // Spirit Wolves have 200% of the master's AP.
                bonusAP = owner->GetTotalAttackPowerValue(BASE_ATTACK) * 2.0f;
                SetBonusDamage(int32(bonusAP));
                break;
            case ENTRY_GREATER_LIGHTNING_ELEMENTAL:
            case ENTRY_GREATER_FIRE_ELEMENTAL:
            case ENTRY_GREATER_STORM_ELEMENTAL:
            case ENTRY_GREATER_EARTH_ELEMENTAL:
                bonusAP = owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_MAGIC);
                SetBonusDamage(int32(bonusAP), false);
                break;
            case ENTRY_PRIMAL_EARTH_ELEMENTAL:
            case ENTRY_PRIMAL_FIRE_ELEMENTAL:
            case ENTRY_PRIMAL_STORM_ELEMENTAL:
                bonusAP = owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_MAGIC);
                SetBonusDamage(int32(bonusAP));
                break;
            // Monk
            case ENTRY_XUEN:
            case ENTRY_NIUZAO:
                // Xuen & Niuzao benefit from Monk's attack power
                bonusAP = owner->GetTotalAttackPowerValue(BASE_ATTACK) * 0.6f;
                break;
            // Druid
            case ENTRY_TREANT_FERAL:
            case ENTRY_TREANT_GUARDIAN:
                // Treant (Force of Nature) benefit from Druid's attack power
                bonusAP = owner->GetTotalAttackPowerValue(BASE_ATTACK) * 0.8f;
                break;
            // Death Knight
            case ENTRY_EBON_GARGOYLE:
                bonusAP = GetOwner()->GetTotalAttackPowerValue(BASE_ATTACK) * 0.5f;
                SetBonusDamage(int32(bonusAP));
                break;
            // Permanent warlock pets
            case ENTRY_FELGUARD:
            case ENTRY_FELHUNTER:
            case ENTRY_IMP:
            case ENTRY_VOIDWALKER:
            case ENTRY_INFERNAL_PET:
            case ENTRY_SUCCUBUS:
            case ENTRY_DOOMGUARD_PET:
            {
                float coef = 1.f;
                if (GetEntry() == ENTRY_SUCCUBUS)
                    coef = 0.5f;
                if (GetEntry() == ENTRY_DOOMGUARD_PET)
                    coef = 0.066f;
                bonusAP = owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_MAGIC) * coef;
                SetBonusDamage(int32(bonusAP));
                break;
            }
            // Temp warlock pets
            case ENTRY_INFERNAL:
            case ENTRY_INFERNAL_LORD_OF_FLAMES:
            case ENTRY_DOOMGUARD:
            case ENTRY_DREADSTALKER:
            case ENTRY_WILD_IMP:
            case ENTRY_WILD_IMP_DREADSTALKER:
            case ENTRY_DARKGLARE:
            case ENTRY_CHAOS_TEAR:
            case ENTRY_UNSTABLE_TEAR:
            case ENTRY_SHADOWY_TEAR:
            {
                float coef = GetEntry() == ENTRY_DOOMGUARD_PET ? 0.066f : 1.f;
                bonusAP = owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_MAGIC) * coef;
                SetBonusDamage(int32(bonusAP), false);
                break;
            }
            // Hunter
            case ENTRY_HATI_NORMAL:
            case ENTRY_HATI_HIDDEN_SKIN:
            case ENTRY_HATI_HIDDEN_SKIN_2:
            case ENTRY_HATI_HIDDEN_SKIN_3:
            case ENTRY_HATI_HIDDEN_SKIN_4:
                bonusAP = owner->GetTotalAttackPowerValue(RANGED_ATTACK);
                SetBonusDamage(int32(bonusAP), false);
                break;
            default:
                if (IsDireBeastSummon())
                {
                    bonusAP = owner->GetTotalAttackPowerValue(RANGED_ATTACK) * 1.4f;
                    SetBonusDamage(int32(bonusAP), false);
                    break;
                }
                bonusAP = owner->GetTotalAttackPowerValue(owner->getClass() == CLASS_HUNTER ? RANGED_ATTACK : BASE_ATTACK);
                SetBonusDamage(int32(bonusAP), IsHunterPet());
                break;
        }
    }

    SetModifierValue(UNIT_MOD_ATTACK_POWER, BASE_VALUE, val + bonusAP);

    //in BASE_VALUE of UNIT_MOD_ATTACK_POWER for creatures we store data of meleeattackpower field in DB
    float base_attPower  = GetModifierValue(unitMod, BASE_VALUE) * GetModifierValue(unitMod, BASE_PCT);
    float attPowerMultiplier = GetModifierValue(unitMod, TOTAL_PCT) - 1.0f;

    //UNIT_FIELD_(RANGED)_ATTACK_POWER field
    SetInt32Value(UNIT_FIELD_ATTACK_POWER, (int32)base_attPower);
    //UNIT_FIELD_(RANGED)_ATTACK_POWER_MULTIPLIER field
    SetFloatValue(UNIT_FIELD_ATTACK_POWER_MULTIPLIER, attPowerMultiplier);

    //automatically update weapon damage after attack power modification
    UpdateDamagePhysical(BASE_ATTACK);
}

void Guardian::UpdateDamagePhysical(WeaponAttackType attType)
{
    if (attType > BASE_ATTACK)
        return;

    float bonusDamage = 0.0f;
    UnitMods unitMod = UNIT_MOD_DAMAGE_MAINHAND;

    float att_speed = float(GetBaseAttackTime(BASE_ATTACK))/1000.0f;

    float base_value  = GetModifierValue(unitMod, BASE_VALUE) + GetTotalAttackPowerValue(attType)/ 3.5f * att_speed  + bonusDamage;
    float base_pct    = GetModifierValue(unitMod, BASE_PCT);
    float total_value = GetModifierValue(unitMod, TOTAL_VALUE);
    float total_pct   = GetModifierValue(unitMod, TOTAL_PCT);

    float weapon_mindamage = GetWeaponDamageRange(BASE_ATTACK, MINDAMAGE);
    float weapon_maxdamage = GetWeaponDamageRange(BASE_ATTACK, MAXDAMAGE);

    float mindamage = ((base_value + weapon_mindamage) * base_pct + total_value) * total_pct;
    float maxdamage = ((base_value + weapon_maxdamage) * base_pct + total_value) * total_pct;

    switch (GetEntry())
    {
        case ENTRY_FELHUNTER:
        case ENTRY_VOIDWALKER:
            mindamage = 1.2f * GetBonusDamage() / 3.5f * 2;
            maxdamage = mindamage;
            break;
        case ENTRY_FELGUARD:
            mindamage = 1.1f * GetBonusDamage() / 3.5f * 2;
            maxdamage = mindamage;
            break;
        case ENTRY_SUCCUBUS:
            mindamage = 0.9f * GetBonusDamage() / 3.5f * 2;
            maxdamage = mindamage;
            break;
        case ENTRY_DREADSTALKER:
            mindamage = 0.83f * GetBonusDamage() / 3.5f * 2;
            maxdamage = mindamage;
            break;
        case ENTRY_INFERNAL:
        case ENTRY_INFERNAL_LORD_OF_FLAMES:
        case ENTRY_INFERNAL_PET:
            mindamage = 1.0f * GetBonusDamage() / 3.5f * 2;
            maxdamage = mindamage;
            break;
        case ENTRY_SPIRIT_WOLF:
        case ENTRY_SPIRIT_DOOM_WOLF:
            mindamage = 1.0f * GetBonusDamage() / 3.5f * 1.5;
            maxdamage = mindamage;
            break;
        case ENTRY_DARK_MINION:
            mindamage = 0.6f * GetBonusDamage() / 3.5 * 2;
            maxdamage = mindamage;
            break;
        case ENTRY_DIRE_BASILISK:
            mindamage = 1.2f * GetBonusDamage() / 3.5f * 2.0f;
            maxdamage = mindamage;
            break;
        default:
            if (IsHunterPet() || IsHatiPet())
            {
                mindamage = 0.6f * GetBonusDamage() / 3.5f * 2.0f;
                maxdamage = mindamage;
                break;
            }
            else if (IsDireBeastSummon())
            {
                mindamage = 1.0f * GetBonusDamage() / 3.5f * 2;
                maxdamage = mindamage;
                break;
            }
            break;
    }

    AuraEffectList const& mModDamagePercentDone = GetAuraEffectsByType(SPELL_AURA_MOD_DAMAGE_PERCENT_DONE);
    for (AuraEffectList::const_iterator i = mModDamagePercentDone.begin(); i != mModDamagePercentDone.end(); ++i)
    {
        if ((*i)->GetMiscValue() & SPELL_SCHOOL_MASK_NORMAL)
        {
            AddPct(mindamage, (*i)->GetAmount());
            AddPct(maxdamage, (*i)->GetAmount());
        }
    }

    SetStatFloatValue(UNIT_FIELD_MINDAMAGE, mindamage);
    SetStatFloatValue(UNIT_FIELD_MAXDAMAGE, maxdamage);
}

void Guardian::SetBonusDamage(int32 damage, bool permanentPet /*= true*/)
{
    m_bonusSpellDamage = damage;
    if (permanentPet && GetOwner()->GetTypeId() == TYPEID_PLAYER)
        GetOwner()->SetUInt32Value(PLAYER_PET_SPELL_POWER, damage);
}
