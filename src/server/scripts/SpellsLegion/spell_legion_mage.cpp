/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_MAGE and SPELLFAMILY_GENERIC spells used by mage players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_mage_".
 */

#include "AreaTriggerAI.h"
#include "AreaTriggerTemplate.h"
#include "DB2Stores.h"
#include "GridNotifiers.h"
#include "Item.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"
#include "TemporarySummon.h"
#include <iostream>

enum MageSpells
{
    SPELL_HOTFIX_HOT_STREAK_FLAMESTRIKE_HELPER = 300005,
    SPELL_HOTFIX_HOT_STREAK_PYROBLAST_HELPER = 300004,
    SPELL_HOTFIX_NETHER_TEMPEST_DMG_HOLDER  = 300002,
    SPELL_MAGE_AEGWYNN_S_ASCENDANCE_DAMAGE  = 187677,
    SPELL_MAGE_AEGWYNN_S_ASCENDANCE_TRAIT   = 187680,
    SPELL_MAGE_AEGWYNN_S_ASCENDANCE_VISUAL  = 210847,
    SPELL_MAGE_AFTERSHOCKS_DAMAGE           = 194432,
    SPELL_MAGE_AFTERSHOCKS_TRAIT            = 194431,
    SPELL_MAGE_ALEXSTRASZA_S_FURY           = 235870,
    SPELL_MAGE_ARCANE_ASSAULT_DAMAGE        = 225119,
    SPELL_MAGE_ARCANE_BARRAGE               = 44425,
    SPELL_MAGE_ARCANE_BLAST                 = 30451,
    SPELL_MAGE_ARCANE_CHARGE                = 36032,
    SPELL_MAGE_ARCANE_MISSILES_ACTIVATOR    = 79683,
    SPELL_MAGE_ARCANE_MISSILES_ENERGIZE     = 208030,
    SPELL_MAGE_ARCANE_MISSILES_TRIGGERED    = 7268,
    SPELL_MAGE_ARCANE_ORB_DAMAGE            = 153640,
    SPELL_MAGE_ARCANE_POWER                 = 12042,
    SPELL_MAGE_ARCANE_REBOUND_DAMAGE        = 210817,
    SPELL_MAGE_ARCANE_REBOUND_TRAIT         = 188006,
    SPELL_MAGE_BLACK_ICE_TRAIT              = 195615,
    SPELL_MAGE_BLAZING_BARRIER              = 235313,
    SPELL_MAGE_BLAZING_BARRIER_TRIGGER      = 235314,
    SPELL_MAGE_BLAZING_SPEED                = 108843,
    SPELL_MAGE_BLINK                        = 1953,
    SPELL_MAGE_BLIZZARD_DAMAGE              = 190357,
    SPELL_MAGE_BLIZZARD_ORB_BONUS           = 236662,
    SPELL_MAGE_BONE_CHILLING_BONUS          = 205766,
    SPELL_MAGE_BONE_CHILLING_PROC           = 205027,
    SPELL_MAGE_BRAIN_FREEZE_LEVEL_BONUS     = 231584,
    SPELL_MAGE_BRAIN_FREEZE_PASSIVE         = 190447,
    SPELL_MAGE_BRAIN_FREEZE_PROC            = 190446,
    SPELL_MAGE_CAUTERIZE                    = 87023,
    SPELL_MAGE_CAUTERIZE_MARKER             = 87024,
    SPELL_MAGE_CAUTERIZING_BLINK_BONUS      = 194316,
    SPELL_MAGE_CAUTERIZING_BLINK_PROC       = 194318,
    SPELL_MAGE_CHILLED                      = 205708,
    SPELL_MAGE_CHILLED_TO_THE_BONE_DAMAGE   = 198127,
    SPELL_MAGE_CHILLED_TO_THE_BONE_DISPEL   = 198139,
    SPELL_MAGE_CHILLED_TO_THE_BONE_HONOR    = 198126,
    SPELL_MAGE_CHILLED_TO_THE_CORE_BONUS    = 195446,
    SPELL_MAGE_CHILLED_TO_THE_CORE_PROC     = 195448,
    SPELL_MAGE_CINDERSTORM                  = 198929,
    SPELL_MAGE_CINDERSTORM_DAMAGE           = 198928,
    SPELL_MAGE_COMBUSTION                   = 190319,
    SPELL_MAGE_COMET_STORM_DAMAGE           = 153596,
    SPELL_MAGE_COMET_STORM_VISUAL           = 228601,
    SPELL_MAGE_CONE_OF_COLD                 = 120,
    SPELL_MAGE_CONE_OF_COLD_FREEZE          = 212792,
    SPELL_MAGE_CONFLAGRATION_FLARE_UP       = 205345,
    SPELL_MAGE_CONFLAGRATION_PROC_AURA      = 205023,
    SPELL_MAGE_CONJURE_REFRESHMENT          = 176351,
    SPELL_MAGE_CONJURE_REFRESHMENT_GOB      = 167145,
    SPELL_MAGE_CONTROLLED_BURN              = 205033,
    SPELL_MAGE_DEEP_SHATTER_HONOR_TALENT    = 198123,
    SPELL_MAGE_DISPLACEMENT                 = 195676,
    SPELL_MAGE_DISPLACEMENT_BEACON          = 212799,
    SPELL_MAGE_DISPLACEMENT_SHIMMER         = 212801,
    SPELL_MAGE_DRAGONS_BREATH               = 31661,
    SPELL_MAGE_ENHANCED_PYROTECHNICS        = 157644,
    SPELL_MAGE_EROSION_DEBUFF               = 210134,
    SPELL_MAGE_EROSION_PERIODIC             = 210154,
    SPELL_MAGE_FINGERS_OF_FROST             = 44544,
    SPELL_MAGE_FINGERS_OF_FROST_BUFF        = 44544,
    SPELL_MAGE_FINGERS_OF_FROST_VISUAL_R    = 126084,
    SPELL_MAGE_FIRE_BLAST                   = 108853,
    SPELL_MAGE_FIREBALL                     = 133,
    SPELL_MAGE_FLAME_PATCH_AREATRIGGER      = 205470,
    SPELL_MAGE_FLAME_PATCH_DAMAGE           = 205472,
    SPELL_MAGE_FLAME_PATCH_TALENT           = 205037,
    SPELL_MAGE_FLAMECANNON_BONUS            = 203285,
    SPELL_MAGE_FLAMESTRIKE                  = 2120,
    SPELL_MAGE_FLURRY_DAMAGE                = 228596,
    SPELL_MAGE_FROST_BOMB                   = 112948,
    SPELL_MAGE_FROST_BOMB_DAMAGE            = 113092,
    SPELL_MAGE_FROST_NOVA                   = 122,
    SPELL_MAGE_FROSTBOLT_DAMAGE             = 228597,
    SPELL_MAGE_FROZEN_ORB_DAMAGE            = 84721,
    SPELL_MAGE_FROZEN_ORB_SUMMON            = 84714,
    SPELL_MAGE_FROZEN_ORB_SUMMON_STATIC     = 198149,
    SPELL_MAGE_FROZEN_TOUCH                 = 205030,
    SPELL_MAGE_GLACIAL_INSULATION           = 235297,
    SPELL_MAGE_GLACIAL_SPIKE                = 199786,
    SPELL_MAGE_GLACIAL_SPIKE_DAMAGE         = 228600,
    SPELL_MAGE_GLACIAL_SPIKE_USABLE         = 199844,
    SPELL_MAGE_HEATING_UP                   = 48107,
    SPELL_MAGE_HOT_STREAK                   = 48108,
    SPELL_MAGE_HOTFIX_BRAIN_FREEZE_HELPER   = 300000,
    SPELL_MAGE_ICE_BARRIER                  = 11426,
    SPELL_MAGE_ICE_BLOCK                    = 45438,
    SPELL_MAGE_ICE_LANCE_DAMAGE             = 228598,
    SPELL_MAGE_ICE_NINE                     = 214664,
    SPELL_MAGE_ICICLES_BLACK_1              = 214752,
    SPELL_MAGE_ICICLES_BLACK_2              = 214753,
    SPELL_MAGE_ICICLES_BLACK_3              = 214754,
    SPELL_MAGE_ICICLES_BLACK_4              = 214755,
    SPELL_MAGE_ICICLES_BLACK_5              = 214756,
    SPELL_MAGE_ICICLES_DAMAGE               = 148022,
    SPELL_MAGE_ICICLES_INFO_BUFF            = 205473,
    SPELL_MAGE_ICICLES_PERIODIC             = 148023,
    SPELL_MAGE_ICICLES_STATIC_VISUAL_1      = 214130,
    SPELL_MAGE_ICICLES_STATIC_VISUAL_2      = 214127,
    SPELL_MAGE_ICICLES_STATIC_VISUAL_3      = 214126,
    SPELL_MAGE_ICICLES_STATIC_VISUAL_4      = 214125,
    SPELL_MAGE_ICICLES_STATIC_VISUAL_5      = 214124,
    SPELL_MAGE_ICICLES_STORE_1              = 148012,
    SPELL_MAGE_ICICLES_STORE_2              = 148013,
    SPELL_MAGE_ICICLES_STORE_3              = 148014,
    SPELL_MAGE_ICICLES_STORE_4              = 148015,
    SPELL_MAGE_ICICLES_STORE_5              = 148016,
    SPELL_MAGE_ICICLES_VISUAL_1             = 148017,
    SPELL_MAGE_ICICLES_VISUAL_2             = 148018,
    SPELL_MAGE_ICICLES_VISUAL_3             = 148019,
    SPELL_MAGE_ICICLES_VISUAL_4             = 148020,
    SPELL_MAGE_ICICLES_VISUAL_5             = 148021,
    SPELL_MAGE_ICY_VEINS                    = 12472,
    SPELL_MAGE_IGNITE                       = 12654,
    SPELL_MAGE_IGNITE_TARGET_SELECTOR       = 197409, // server-side spell
    SPELL_MAGE_INCANTER_S_FLOW              = 116267,
    SPELL_MAGE_INVISIBILITY_DMG_REDUCE      = 113862,
    SPELL_MAGE_LIVING_BOMB_AOE              = 44461,
    SPELL_MAGE_LIVING_BOMB_PERIODIC         = 217694,
    SPELL_MAGE_MANA_SHIELD                  = 235463,
    SPELL_MAGE_MARK_OF_ALUNETH_DAMAGE       = 211076,
    SPELL_MAGE_MARK_OF_ALUNETH_PERIODIC     = 211088,
    SPELL_MAGE_MASTERY_ICICLES              = 76613,
    SPELL_MAGE_METEOR_DAMAGE                = 153564,
    SPELL_MAGE_METEOR_INITIAL               = 153561,
    SPELL_MAGE_METEOR_PRE_AREATRIGGER       = 177345,
    SPELL_MAGE_MIRROR_IMAGE_FRONT           = 58831,
    SPELL_MAGE_MIRROR_IMAGE_LEFT            = 58834,
    SPELL_MAGE_MIRROR_IMAGE_RIGHT           = 58833,
    SPELL_MAGE_NETHER_TEMPEST_DAMAGE        = 114954,
    SPELL_MAGE_NETHER_TEMPEST_DOT           = 114923,
    SPELL_MAGE_NETHER_TEMPEST_VISUAL        = 114956,
    SPELL_MAGE_OVERPOWERED                  = 155147,
    SPELL_MAGE_PET_WATER_JET                = 135029,
    SPELL_MAGE_PHOENIX_REBORN_DAMAGE        = 215775,
    SPELL_MAGE_PHOENIX_S_FLAMES             = 194466,
    SPELL_MAGE_PHOENIX_S_FLAMES_AOE         = 224637,
    SPELL_MAGE_PRISMATIC_CLOAK_BONUS        = 198065,
    SPELL_MAGE_PRISMATIC_CLOAK_PROC         = 198064,
    SPELL_MAGE_PYRETIC_INCANTATION_BONUS    = 194329,
    SPELL_MAGE_PYROBLAST                    = 11366,
    SPELL_MAGE_PYROMANIAC                   = 205020,
    SPELL_MAGE_RAY_OF_FROST                 = 205021,
    SPELL_MAGE_RAY_OF_FROST_BONUS           = 208141,
    SPELL_MAGE_RAY_OF_FROST_CHANNEL_CHECK   = 208166,
    SPELL_MAGE_RESONANCE                    = 205028,
    SPELL_MAGE_RING_OF_FROST_DUMMY          = 91264,
    SPELL_MAGE_RING_OF_FROST_FREEZE         = 82691,
    SPELL_MAGE_RING_OF_FROST_SUMMON         = 113724,
    SPELL_MAGE_RULE_OF_THREES_BONUS         = 187292,
    SPELL_MAGE_RULE_OF_THREES_TRAIT         = 215463,
    SPELL_MAGE_SCORCHED_EARTH_SPEED_BUFF    = 227482,
    SPELL_MAGE_SHIMMER                      = 212653,
    SPELL_MAGE_SLOOOOW_DOWN_BONUS           = 210732,
    SPELL_MAGE_SLOOOOW_DOWN_PROC            = 210730,
    SPELL_MAGE_SPLITTING_ICE                = 56377,
    SPELL_MAGE_TEMPORAL_DISPLACEMENT        = 80354,
    SPELL_MAGE_TEMPORAL_RIPPLES             = 198116,
    SPELL_MAGE_THERMAL_VOID                 = 155149,
    SPELL_MAGE_TIME_ANOMALY_ENERGIZE        = 210808,
    SPELL_MAGE_TINDER_DEBUFF                = 203278,
    SPELL_MAGE_TINDER_DUMMY_PROC            = 203277,
    SPELL_MAGE_TOUCH_OF_THE_MAGI_DAMAGE     = 210833,
    SPELL_MAGE_TOUCH_OF_THE_MAGI_TRAIT      = 210725,
    SPELL_MAGE_UNSTABLE_MAGIC_ARCANE        = 157979,
    SPELL_MAGE_UNSTABLE_MAGIC_FIRE          = 157977,
    SPELL_MAGE_UNSTABLE_MAGIC_FROST         = 157978,
    SPELL_MAGE_UNSTABLE_MAGIC_TALENT        = 157976,
    SPELL_MAGE_WINTER_S_CHILL               = 228358,
    SPELL_MAGE_WORDS_OF_POWER               = 205035,
};

struct IcicleSpells
{
    uint32 StoreSpellId;
    uint32 StaticSpellId;
    uint32 ExecuteSpellId;
    uint32 BlackIceSpellId;
};

static IcicleSpells constexpr const IcicleEvent[] =
{
    { SPELL_MAGE_ICICLES_STORE_1, SPELL_MAGE_ICICLES_STATIC_VISUAL_1, SPELL_MAGE_ICICLES_VISUAL_1, SPELL_MAGE_ICICLES_BLACK_1 },
    { SPELL_MAGE_ICICLES_STORE_2, SPELL_MAGE_ICICLES_STATIC_VISUAL_2, SPELL_MAGE_ICICLES_VISUAL_2, SPELL_MAGE_ICICLES_BLACK_2 },
    { SPELL_MAGE_ICICLES_STORE_3, SPELL_MAGE_ICICLES_STATIC_VISUAL_3, SPELL_MAGE_ICICLES_VISUAL_3, SPELL_MAGE_ICICLES_BLACK_3 },
    { SPELL_MAGE_ICICLES_STORE_4, SPELL_MAGE_ICICLES_STATIC_VISUAL_4, SPELL_MAGE_ICICLES_VISUAL_4, SPELL_MAGE_ICICLES_BLACK_4 },
    { SPELL_MAGE_ICICLES_STORE_5, SPELL_MAGE_ICICLES_STATIC_VISUAL_5, SPELL_MAGE_ICICLES_VISUAL_5, SPELL_MAGE_ICICLES_BLACK_5 }
};

enum MageMisc
{
    SPELL_HUNTER_INSANITY               = 95809,
    SPELL_SHAMAN_EXHAUSTION             = 57723,
    SPELL_SHAMAN_SATED                  = 57724,
    SPELL_PET_NETHERWINDS_FATIGUED      = 160455,
    ITEM_FELO_MELORN                    = 128820,
    ARTIFACT_POWER_ID_CAUTERIZING_BLINK = 756,
};

enum MageActions
{
    ACTION_RESHAPE_AREATRIGGER  = 1,
    ACTION_FINGERS_OF_FROST     = 2
};

// 44425 - Arcane Barrage
class spell_mage_legion_arcane_barrage : public SpellScriptLoader
{
    public:
        spell_mage_legion_arcane_barrage() : SpellScriptLoader("spell_mage_legion_arcane_barrage") { }

        class spell_mage_legion_arcane_barrage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_arcane_barrage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RESONANCE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _chainTargetCount = targets.size();
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                int32 damage = GetHitDamage();
                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_MAGE_RESONANCE, EFFECT_0))
                    AddPct(damage, ((1 + _chainTargetCount) * aurEff->GetAmount()));

                if (GetHitUnit() != GetExplTargetUnit())
                    if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_1))
                        damage = CalculatePct(damage, effInfo->CalcValue());

                if (GetHitUnit() == GetExplTargetUnit() && _chainTargetCount >= 2)
                    if (GetCaster()->HasAura(SPELL_MAGE_ARCANE_REBOUND_TRAIT))
                        GetCaster()->CastSpell(GetExplTargetUnit(), SPELL_MAGE_ARCANE_REBOUND_DAMAGE, true);

                SetHitDamage(damage);
            }

            void RemoveCharges()
            {
                GetCaster()->SetPower(POWER_ARCANE_CHARGES, 0);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_arcane_barrage_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_TARGET_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_arcane_barrage_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                AfterCast += SpellCastFn(spell_mage_legion_arcane_barrage_SpellScript::RemoveCharges);
            }

        private:
            uint32 _chainTargetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_arcane_barrage_SpellScript();
        }
};

// 30451 - Arcane Blast
class spell_mage_legion_arcane_blast : public SpellScriptLoader
{
    public:
        spell_mage_legion_arcane_blast() : SpellScriptLoader("spell_mage_legion_arcane_blast") { }

        class spell_mage_legion_arcane_blast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_arcane_blast_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_UNSTABLE_MAGIC_TALENT,
                    SPELL_MAGE_UNSTABLE_MAGIC_ARCANE,
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                // Unstable Magic (fire)
                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_MAGE_UNSTABLE_MAGIC_TALENT, EFFECT_0))
                    if (AuraEffect const* aurEffBp = GetCaster()->GetAuraEffect(SPELL_MAGE_UNSTABLE_MAGIC_TALENT, EFFECT_3))
                        if (roll_chance_i(aurEff->GetAmount()))
                        {
                            int32 damage = CalculatePct(GetHitDamage(), aurEffBp->GetAmount());
                            GetCaster()->CastCustomSpell(GetHitUnit(), SPELL_MAGE_UNSTABLE_MAGIC_ARCANE, &damage, NULL, NULL, true);
                        }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_arcane_blast_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_arcane_blast_SpellScript();
        }
};

// 1449 - Arcane Explosion
class spell_mage_legion_arcane_explosion : public SpellScriptLoader
{
    public:
        spell_mage_legion_arcane_explosion() : SpellScriptLoader("spell_mage_legion_arcane_explosion") { }

        class spell_mage_legion_arcane_explosion_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_arcane_explosion_SpellScript);

            void CheckTargets(std::list<WorldObject*>& targets)
            {
                preventEnergize = targets.empty();
            }

            void EffectHit(SpellEffIndex effIndex)
            {
                if (preventEnergize)
                    PreventHitDefaultEffect(effIndex);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_arcane_explosion_SpellScript::CheckTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                OnEffectLaunch += SpellEffectFn(spell_mage_legion_arcane_explosion_SpellScript::EffectHit, EFFECT_1, SPELL_EFFECT_ENERGIZE);
            }

        private:
            bool preventEnergize = true;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_arcane_explosion_SpellScript();
        }
};

// 210126 - Arcane Familiar
class spell_mage_legion_arcane_familiar : public SpellScriptLoader
{
    public:
        spell_mage_legion_arcane_familiar() : SpellScriptLoader("spell_mage_legion_arcane_familiar") { }

        class spell_mage_legion_arcane_familiar_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_arcane_familiar_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ARCANE_ASSAULT_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (!GetUnitOwner())
                    return false;

                return procInfo.GetActionTarget() && GetUnitOwner()->IsValidAttackTarget(procInfo.GetActionTarget());
            }

            void HandleProc(ProcEventInfo& procInfo)
            {
                lastProcTargetGuid = procInfo.GetActionTarget()->GetGUID();
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                amplitude = 2000;
                isPeriodic = true;
            }

            void HandleCustomTick(AuraEffect const* /*aurEff*/)
            {
                if (lastProcTargetGuid.IsEmpty())
                    return;

                if (!GetUnitOwner()->IsInCombat())
                {
                    lastProcTargetGuid = ObjectGuid::Empty;
                    return;
                }

                if (Unit* target = ObjectAccessor::GetUnit(*GetUnitOwner(), lastProcTargetGuid))
                {
                    if (GetUnitOwner()->GetExactDist(target) <= 40.00f)
                    {
                        if (GetUnitOwner()->IsWithinLOSInMap(target))
                            GetUnitOwner()->CastSpell(target, SPELL_MAGE_ARCANE_ASSAULT_DAMAGE, true);
                    }
                    else
                        lastProcTargetGuid = ObjectGuid::Empty;
                }
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_arcane_familiar_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_mage_legion_arcane_familiar_AuraScript::HandleProc);
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_mage_legion_arcane_familiar_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_MOD_MAX_POWER_PCT);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_arcane_familiar_AuraScript::HandleCustomTick, EFFECT_0, SPELL_AURA_MOD_MAX_POWER_PCT);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_mage_legion_arcane_familiar_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_MOD_MAX_POWER_PCT);
            }

            private:
                ObjectGuid lastProcTargetGuid;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_arcane_familiar_AuraScript();
        }
};

// 5143 - Arcane Missiles
class spell_mage_legion_arcane_missiles : public SpellScriptLoader
{
    public:
        spell_mage_legion_arcane_missiles() : SpellScriptLoader("spell_mage_legion_arcane_missiles") { }

        class spell_mage_legion_arcane_missiles_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_arcane_missiles_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RULE_OF_THREES_TRAIT,
                    SPELL_MAGE_RULE_OF_THREES_BONUS,
                    SPELL_MAGE_ARCANE_MISSILES_ACTIVATOR,
                    SPELL_MAGE_ARCANE_MISSILES_ENERGIZE
                });
            }

            void HandleTalent()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_ARCANE_MISSILES_ENERGIZE, true);
                GetCaster()->RemoveAuraFromStack(SPELL_MAGE_ARCANE_MISSILES_ACTIVATOR);
                if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_MAGE_RULE_OF_THREES_TRAIT, EFFECT_0))
                    if (roll_chance_f(float(aurEff->GetAmount() / 10)))
                        GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_RULE_OF_THREES_BONUS, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_mage_legion_arcane_missiles_SpellScript::HandleTalent);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_arcane_missiles_SpellScript();
        }

        class spell_mage_legion_arcane_missiles_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_arcane_missiles_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RULE_OF_THREES_BONUS
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->ClearDynamicValue(UNIT_DYNAMIC_FIELD_CHANNEL_OBJECTS);
                    caster->AddDynamicStructuredValue(UNIT_DYNAMIC_FIELD_CHANNEL_OBJECTS, &GetTarget()->GetGUID());
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_RULE_OF_THREES_BONUS);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_mage_legion_arcane_missiles_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_mage_legion_arcane_missiles_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_arcane_missiles_AuraScript();
        }
};

// 79684 - Arcane Missiles
class spell_mage_legion_arcane_missiles_passive : public SpellScriptLoader
{
    public:
        spell_mage_legion_arcane_missiles_passive() : SpellScriptLoader("spell_mage_legion_arcane_missiles_passive") { }

        class spell_mage_legion_arcane_missiles_passive_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_arcane_missiles_passive_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_WORDS_OF_POWER,
                    SPELL_MAGE_ARCANE_BLAST
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                // small cd check for prevent aoe spell mass checks
                if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;

                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(100));

                if (!procInfo.GetSpellInfo() || procInfo.GetSpellInfo()->Id == SPELL_MAGE_ARCANE_MISSILES_TRIGGERED
                    || procInfo.GetSpellInfo()->Id == SPELL_MAGE_NETHER_TEMPEST_DAMAGE)
                    return false;

                uint32 procChance = 0;
                if (AuraEffect const* baseChance = GetAura()->GetEffect(EFFECT_0))
                    procChance += baseChance->GetAmount();

                if (procInfo.GetSpellInfo()->Id == SPELL_MAGE_ARCANE_BLAST)
                    procChance *= 2;

                if (AuraEffect const* wordsOfPower = GetTarget()->GetAuraEffect(SPELL_MAGE_WORDS_OF_POWER, EFFECT_1))
                    procChance += floor(int32(GetTarget()->GetPowerPct(POWER_MANA)) / wordsOfPower->GetAmount());

                return roll_chance_i(procChance);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_arcane_missiles_passive_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_arcane_missiles_passive_AuraScript();
        }
};

// 235313 - Blazing Barrier
class spell_mage_legion_blazing_barrier : public SpellScriptLoader
{
    public:
        spell_mage_legion_blazing_barrier() : SpellScriptLoader("spell_mage_legion_blazing_barrier") { }

        class spell_mage_legion_blazing_barrier_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_blazing_barrier_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_BLAZING_BARRIER_TRIGGER
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                if (Player* caster = GetUnitOwner()->ToPlayer())
                {
                    float newAmount = float(caster->GetStat(STAT_INTELLECT)) * 7.0f * (1.0f + (caster->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE) / 100.0f));
                    amount += int32(newAmount);
                }
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                Unit* caster = eventInfo.GetDamageInfo()->GetVictim();
                Unit* target = eventInfo.GetDamageInfo()->GetAttacker();

                if (caster && target)
                    caster->CastSpell(target, SPELL_MAGE_BLAZING_BARRIER_TRIGGER, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_blazing_barrier_AuraScript::CheckProc);
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_mage_legion_blazing_barrier_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_blazing_barrier_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_blazing_barrier_AuraScript();
        }
};

// 235365 - Blazing Soul
class spell_mage_legion_blazing_soul : public SpellScriptLoader
{
    public:
        spell_mage_legion_blazing_soul() : SpellScriptLoader("spell_mage_legion_blazing_soul") { }

        class spell_mage_legion_blazing_soul_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_blazing_soul_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_MAGE_BLAZING_BARRIER
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (GetTarget()->GetDistance(eventInfo.GetActionTarget()) > GetEffect(EFFECT_1)->GetAmount())
                    return false;

                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage() > 0;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                if (Player* caster = GetUnitOwner()->ToPlayer())
                {
                    if (AuraEffect* barrier = caster->GetAuraEffect(SPELL_MAGE_BLAZING_BARRIER, EFFECT_0))
                    {
                        float maxAmount = float(caster->GetStat(STAT_INTELLECT)) * 7.0f * (1.0f + (caster->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE) / 100.0f));
                        barrier->ChangeAmount(std::min(barrier->GetAmount() + CalculatePct(int32(eventInfo.GetDamageInfo()->GetDamage()), aurEff->GetAmount()), int32(maxAmount)));
                    }
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_blazing_soul_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_blazing_soul_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_blazing_soul_AuraScript();
        }
};

// 1953 - Blink
// 212653 - Shimmer
class spell_mage_legion_blink : public SpellScriptLoader
{
    public:
        spell_mage_legion_blink() : SpellScriptLoader("spell_mage_legion_blink") { }

        class spell_mage_legion_blink_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_blink_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_DISPLACEMENT,
                    SPELL_MAGE_DISPLACEMENT_SHIMMER,
                    SPELL_MAGE_DISPLACEMENT_BEACON,
                    SPELL_MAGE_CAUTERIZING_BLINK_PROC,
                    SPELL_MAGE_CAUTERIZING_BLINK_BONUS,
                    SPELL_MAGE_PRISMATIC_CLOAK_PROC,
                    SPELL_MAGE_PRISMATIC_CLOAK_BONUS
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleDisplacementTrigger(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasSpell(SPELL_MAGE_DISPLACEMENT) || GetCaster()->HasSpell(SPELL_MAGE_DISPLACEMENT_SHIMMER))
                {
                    GetCaster()->RemoveAreaTrigger(SPELL_MAGE_DISPLACEMENT_BEACON);
                    GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_MAGE_DISPLACEMENT_BEACON, true);
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_DISPLACEMENT_BEACON, true);
                }
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_MAGE_CAUTERIZING_BLINK_PROC))
                {
                    // sad, calcvalue should be float not int32... ranks of this trait are float values... lets take them directly from db2
                    if (Item* felomelorn = GetCaster()->ToPlayer()->GetItemByEntry(ITEM_FELO_MELORN))
                    {
                        for (ItemDynamicFieldArtifactPowers const& artifactPower : felomelorn->GetArtifactPowers())
                        {
                            if (artifactPower.ArtifactPowerId != ARTIFACT_POWER_ID_CAUTERIZING_BLINK)
                                continue;

                            uint8 rank = artifactPower.CurrentRankWithBonus;
                            if (!rank)
                                continue;

                            if (sArtifactPowerStore.AssertEntry(artifactPower.ArtifactPowerId)->Flags & ARTIFACT_POWER_FLAG_SCALES_WITH_NUM_POWERS)
                                rank = 1;

                            ArtifactPowerRankEntry const* artifactPowerRank = sDB2Manager.GetArtifactPowerRank(artifactPower.ArtifactPowerId, rank - 1);
                            if (!artifactPowerRank)
                                continue;

                            GetCaster()->CastCustomSpell(SPELL_MAGE_CAUTERIZING_BLINK_BONUS, SPELLVALUE_BASE_POINT0, CalculatePct(GetCaster()->GetMaxHealth(), artifactPowerRank->Value), GetCaster(), TRIGGERED_FULL_MASK);
                            break;
                        }
                    }
                }

                if (GetCaster()->HasAura(SPELL_MAGE_PRISMATIC_CLOAK_PROC))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_PRISMATIC_CLOAK_BONUS, true);
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_mage_legion_blink_SpellScript::HandleDisplacementTrigger, EFFECT_0, SPELL_EFFECT_LEAP);
                AfterCast += SpellCastFn(spell_mage_legion_blink_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_blink_SpellScript();
        }
};

// 236662 - Blizzard
class spell_mage_legion_blizzard_bonus : public SpellScriptLoader
{
    public:
        spell_mage_legion_blizzard_bonus() : SpellScriptLoader("spell_mage_legion_blizzard_bonus") { }

        class spell_mage_legion_blizzard_bonus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_blizzard_bonus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_MAGE_FROZEN_ORB_SUMMON,
                    SPELL_MAGE_FROZEN_ORB_SUMMON_STATIC
                });
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_MAGE_FROZEN_ORB_SUMMON, -(aurEff->GetAmount() * 10));
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_MAGE_FROZEN_ORB_SUMMON_STATIC, -(aurEff->GetAmount() * 10));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_blizzard_bonus_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_blizzard_bonus_AuraScript();
        }
};

// 190357 - Blizzard
class spell_mage_legion_blizzard_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_blizzard_damage() : SpellScriptLoader("spell_mage_legion_blizzard_damage") { }

        class spell_mage_legion_blizzard_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_blizzard_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_MAGE_CHILLED,
                    SPELL_MAGE_BONE_CHILLING_PROC,
                    SPELL_MAGE_BONE_CHILLING_BONUS
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_CHILLED, true);

                if (GetCaster()->HasAura(SPELL_MAGE_BONE_CHILLING_PROC))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_BONE_CHILLING_BONUS, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_blizzard_damage_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_blizzard_damage_SpellScript();
        }
};

// 206431 - Burst of Cold
class spell_mage_legion_burst_of_cold : public SpellScriptLoader
{
    public:
        spell_mage_legion_burst_of_cold() : SpellScriptLoader("spell_mage_legion_burst_of_cold") { }

        class spell_mage_legion_burst_of_cold_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_burst_of_cold_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CONE_OF_COLD
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_MAGE_CONE_OF_COLD, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_burst_of_cold_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_burst_of_cold_AuraScript();
        }
};

// 86949 - Cauterize
class spell_mage_legion_cauterize : public SpellScriptLoader
{
    public:
        spell_mage_legion_cauterize() : SpellScriptLoader("spell_mage_legion_cauterize") { }

        class spell_mage_legion_cauterize_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_cauterize_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CAUTERIZE,
                    SPELL_MAGE_CAUTERIZE_MARKER,
                    SPELL_MAGE_BLAZING_SPEED
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                uint32 healtPct = GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(GetCaster());
                int32 remainingHealth = caster->GetHealth() - dmgInfo.GetDamage();
                int32 cauterizeHeal = caster->CountPctFromMaxHealth(healtPct);

                if (caster->GetSpellHistory()->HasCooldown(SPELL_MAGE_CAUTERIZE))
                    return;

                if (remainingHealth <= 0)
                {
                    absorbAmount = dmgInfo.GetDamage();
                    caster->CastCustomSpell(caster, SPELL_MAGE_CAUTERIZE, NULL, &cauterizeHeal, NULL, true, NULL, aurEff);
                    caster->CastSpell(caster, SPELL_MAGE_CAUTERIZE_MARKER, true);
                    caster->CastSpell(caster, SPELL_MAGE_BLAZING_SPEED, true);
                    caster->ToPlayer()->GetSpellHistory()->AddCooldown(SPELL_MAGE_CAUTERIZE, 0, std::chrono::minutes(2));
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_mage_legion_cauterize_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_mage_legion_cauterize_AuraScript::Absorb, EFFECT_0);
            }
        };

        class spell_mage_legion_cauterize_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_cauterize_SpellScript);

            void EffectHit(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_mage_legion_cauterize_SpellScript::EffectHit, EFFECT_2, SPELL_EFFECT_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_cauterize_AuraScript();
        }

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_cauterize_SpellScript();
        }
};

// 198928 - Cinderstorm
class spell_mage_legion_cinderstorm_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_cinderstorm_damage() : SpellScriptLoader("spell_mage_legion_cinderstorm_damage") { }

        class spell_mage_legion_cinderstorm_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_cinderstorm_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_IGNITE,
                    SPELL_MAGE_CINDERSTORM
                });
            }

            void HandleBonusDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->HasAura(SPELL_MAGE_IGNITE))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), sSpellMgr->AssertSpellInfo(SPELL_MAGE_CINDERSTORM)->GetEffect(EFFECT_0)->CalcValue(GetCaster())));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_cinderstorm_damage_SpellScript::HandleBonusDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_cinderstorm_damage_SpellScript();
        }
};

// 12472 - Icy Veins
// 198144 - Ice Form
class spell_mage_legion_chilled_to_the_core : public SpellScriptLoader
{
    public:
        spell_mage_legion_chilled_to_the_core() : SpellScriptLoader("spell_mage_legion_chilled_to_the_core") { }

        class spell_mage_legion_chilled_to_the_core_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_chilled_to_the_core_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CHILLED_TO_THE_CORE_PROC,
                    SPELL_MAGE_CHILLED_TO_THE_CORE_BONUS
                });
            }

            void HandleBrainFreeze()
            {
                if (GetCaster()->HasAura(SPELL_MAGE_CHILLED_TO_THE_CORE_PROC))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_CHILLED_TO_THE_CORE_BONUS, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_mage_legion_chilled_to_the_core_SpellScript::HandleBrainFreeze);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_chilled_to_the_core_SpellScript();
        }
};

// 235711 - Chrono Shift
class spell_mage_legion_chrono_shift : public SpellScriptLoader
{
    public:
        spell_mage_legion_chrono_shift() : SpellScriptLoader("spell_mage_legion_chrono_shift") { }

        class spell_mage_legion_chrono_shift_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_chrono_shift_AuraScript);

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(aurEff->GetEffIndex()))
                    GetTarget()->CastSpell(effInfo->EffectIndex == EFFECT_1 ? eventInfo.GetActionTarget() : GetTarget(), effInfo->TriggerSpell, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_chrono_shift_AuraScript::HandleProc, EFFECT_ALL, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_chrono_shift_AuraScript();
        }
};

// 235219 - Cold Snap
class spell_mage_legion_cold_snap : public SpellScriptLoader
{
    public:
        spell_mage_legion_cold_snap() : SpellScriptLoader("spell_mage_legion_cold_snap") { }

        class spell_mage_legion_cold_snap_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_cold_snap_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ICE_BARRIER,
                    SPELL_MAGE_FROST_NOVA,
                    SPELL_MAGE_CONE_OF_COLD,
                    SPELL_MAGE_ICE_BLOCK
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_MAGE_ICE_BARRIER, true);
                GetCaster()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_MAGE_FROST_NOVA)->ChargeCategoryId);
                GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_MAGE_CONE_OF_COLD, true);
                GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_MAGE_ICE_BLOCK, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_cold_snap_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_cold_snap_SpellScript();
        }
};

// 190319 - Combustion
class spell_mage_legion_combustion : public SpellScriptLoader
{
   public:
       spell_mage_legion_combustion() : SpellScriptLoader("spell_mage_legion_combustion") { }

       class spell_mage_legion_combustion_AuraScript : public AuraScript
       {
           PrepareAuraScript(spell_mage_legion_combustion_AuraScript);

           bool Load() override
           {
               return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
           }

           void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
           {
               canBeRecalculated = false;
               amount = int32(GetUnitOwner()->ToPlayer()->GetRatingBonusValue(CR_CRIT_SPELL) / GetUnitOwner()->ToPlayer()->GetRatingMultiplier(CR_CRIT_SPELL));
           }

           void Register() override
           {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_mage_legion_combustion_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_MOD_RATING);
           }
       };

       AuraScript* GetAuraScript() const override
       {
           return new spell_mage_legion_combustion_AuraScript();
       }
};

class CometStormEvent : public BasicEvent
{
    public:
        CometStormEvent(Unit* caster, Position const& dest) : _caster(caster), _dest(dest), _count(0) { }

        bool Execute(uint64 time, uint32 /*diff*/) override
        {
            _caster->CastSpell(_dest.GetPositionX() + frand(-3.0f, 3.0f), _dest.GetPositionY() + frand(-3.0f, 3.0f), _dest.GetPositionZ(), SPELL_MAGE_COMET_STORM_VISUAL, true);
            ++_count;

            if (_count >= 7)
                return true;

            _caster->m_Events.AddEvent(this, time + 200);
            return false;
        }

    private:
        Unit* _caster;
        Position _dest;
        uint8 _count;
};

// 153595 - Comet Storm (launch)
class spell_mage_legion_comet_storm : public SpellScriptLoader
{
    public:
        spell_mage_legion_comet_storm() : SpellScriptLoader("spell_mage_legion_comet_storm") { }

        class spell_mage_legion_comet_storm_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_comet_storm_SpellScript);

            void EffectHit(SpellEffIndex)
            {
                GetCaster()->m_Events.AddEvent(new CometStormEvent(GetCaster(), *GetHitDest()), GetCaster()->m_Events.CalculateTime(500));
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_mage_legion_comet_storm_SpellScript::EffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_comet_storm_SpellScript();
        }
};

// 228601 - Comet Storm (damage)
class spell_mage_legion_comet_storm_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_comet_storm_damage() : SpellScriptLoader("spell_mage_legion_comet_storm_damage") { }

        class spell_mage_legion_comet_storm_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_comet_storm_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_COMET_STORM_DAMAGE
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitDest()->GetPositionX(), GetHitDest()->GetPositionY(), GetHitDest()->GetPositionZ(), SPELL_MAGE_COMET_STORM_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_mage_legion_comet_storm_damage_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_comet_storm_damage_SpellScript();
        }
};

// 120 - Cone of Cold
class spell_mage_legion_cone_of_cold : public SpellScriptLoader
{
    public:
        spell_mage_legion_cone_of_cold() : SpellScriptLoader("spell_mage_legion_cone_of_cold") { }

        class spell_mage_legion_cone_of_cold_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_cone_of_cold_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CONE_OF_COLD_FREEZE
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_CONE_OF_COLD_FREEZE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_cone_of_cold_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_cone_of_cold_SpellScript();
        }
};

// 226757 - Conflagration
// 12654 - Ignite
class spell_mage_legion_conflagration_periodic : public SpellScriptLoader
{
    public:
        spell_mage_legion_conflagration_periodic() : SpellScriptLoader("spell_mage_legion_conflagration_periodic") { }

        class spell_mage_legion_conflagration_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_conflagration_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CONFLAGRATION_PROC_AURA,
                    SPELL_MAGE_CONFLAGRATION_FLARE_UP
                });
            }

            bool Load() override
            {
                if (Unit* caster = GetCaster())
                    return caster->HasAura(SPELL_MAGE_CONFLAGRATION_PROC_AURA);
                return false;
            }

            void HandlePeriodicTick(AuraEffect const* aurEff)
            {
                if (Unit* caster = GetCaster())
                    if (SpellEffectInfo const* procChance = sSpellMgr->AssertSpellInfo(SPELL_MAGE_CONFLAGRATION_PROC_AURA)->GetEffect(EFFECT_0))
                        if (roll_chance_i(procChance->CalcValue()))
                            caster->CastSpell(GetTarget(), SPELL_MAGE_CONFLAGRATION_FLARE_UP, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_conflagration_periodic_AuraScript::HandlePeriodicTick, EFFECT_FIRST_FOUND, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_conflagration_periodic_AuraScript();
        }
};

// 190336 - Conjure Refreshment
class spell_mage_legion_conjure_refreshment : public SpellScriptLoader
{
    public:
        spell_mage_legion_conjure_refreshment() : SpellScriptLoader("spell_mage_legion_conjure_refreshment") { }

        class spell_mage_legion_conjure_refreshment_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_conjure_refreshment_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CONJURE_REFRESHMENT_GOB,
                    SPELL_MAGE_CONJURE_REFRESHMENT
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->ToPlayer()->GetGroup())
                {
                    if (GameObject* table = GetCaster()->GetGameObject(SPELL_MAGE_CONJURE_REFRESHMENT_GOB))
                        table->Delete();

                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_CONJURE_REFRESHMENT_GOB, true);
                }
                else
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_CONJURE_REFRESHMENT, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_conjure_refreshment_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_conjure_refreshment_SpellScript();
        }
};

// 236788 - Dampened Magic
class spell_mage_legion_dampened_magic : public SpellScriptLoader
{
    public:
        spell_mage_legion_dampened_magic() : SpellScriptLoader("spell_mage_legion_dampened_magic") { }

        class spell_mage_legion_dampened_magic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_dampened_magic_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;
                return true;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (dmgInfo.GetDamageType() == DOT)
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), GetEffect(EFFECT_1)->GetAmount());
                else
                    absorbAmount = 0;
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_mage_legion_dampened_magic_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_mage_legion_dampened_magic_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_dampened_magic_AuraScript();
        }
};

// 195676 - Displacement
// 212801 - Displacement (Shimmer version)
class spell_mage_legion_displacement : public SpellScriptLoader
{
    public:
        spell_mage_legion_displacement() : SpellScriptLoader("spell_mage_legion_displacement") { }

        class spell_mage_legion_displacement_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_displacement_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_DISPLACEMENT_BEACON,
                    SPELL_MAGE_SHIMMER,
                    SPELL_MAGE_BLINK
                });
            }

            SpellCastResult CheckCast()
            {
                if (AreaTrigger* at = GetCaster()->GetAreaTrigger(SPELL_MAGE_DISPLACEMENT_BEACON))
                    if (!GetCaster()->IsWithinDistInMap(at, 100.0f))
                        return SPELL_FAILED_OUT_OF_RANGE;
                return SPELL_CAST_OK;
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                if (AreaTrigger* at = GetCaster()->GetAreaTrigger(SPELL_MAGE_DISPLACEMENT_BEACON))
                {
                    GetCaster()->NearTeleportTo(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), at->GetOrientation());
                    at->Remove();
                }
                GetCaster()->RemoveAurasDueToSpell(SPELL_MAGE_DISPLACEMENT_BEACON);
                if (GetCaster()->HasSpell(SPELL_MAGE_SHIMMER))
                    GetCaster()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_MAGE_SHIMMER)->ChargeCategoryId);
                else
                    GetCaster()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_MAGE_BLINK)->ChargeCategoryId);
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_mage_legion_displacement_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_displacement_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_displacement_SpellScript();
        }
};

// 31661 - Dragon's Breath (Level 110)
class spell_mage_legion_dragons_breath : public SpellScriptLoader
{
    public:
        spell_mage_legion_dragons_breath() : SpellScriptLoader("spell_mage_legion_dragons_breath") { }

        class spell_mage_legion_dragons_breath_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_dragons_breath_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == GetId() && eventInfo.GetDamageInfo()->GetAttacker()->GetGUID() == GetCasterGUID())
                    return false;
                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_dragons_breath_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_dragons_breath_AuraScript();
        }
};

// 214634 - Ebonbolt
class spell_mage_legion_ebonbolt : public SpellScriptLoader
{
    public:
        spell_mage_legion_ebonbolt() : SpellScriptLoader("spell_mage_legion_ebonbolt") { }

        class spell_mage_legion_ebonbolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_ebonbolt_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_BRAIN_FREEZE_PASSIVE,
                    SPELL_MAGE_BRAIN_FREEZE_PROC
                });
            }

            void HandleBrainFreeze()
            {
                if (GetCaster()->HasAura(SPELL_MAGE_BRAIN_FREEZE_PASSIVE))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_BRAIN_FREEZE_PROC, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_mage_legion_ebonbolt_SpellScript::HandleBrainFreeze);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_ebonbolt_SpellScript();
        }
};

// 157642 - Enhanced Pyrotechnics
class spell_mage_legion_enhanced_pyrotechnics : public SpellScriptLoader
{
    public:
        spell_mage_legion_enhanced_pyrotechnics() : SpellScriptLoader("spell_mage_legion_enhanced_pyrotechnics") { }

        class spell_mage_legion_enhanced_pyrotechnics_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_enhanced_pyrotechnics_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ENHANCED_PYROTECHNICS,
                    SPELL_MAGE_FIREBALL
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                // Needs special check here, spellfamflags affect more than fireball
                return (procInfo.GetSpellInfo() && procInfo.GetSpellInfo()->Id == SPELL_MAGE_FIREBALL);
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& procInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (procInfo.GetHitMask() & PROC_HIT_CRITICAL)
                    caster->RemoveAurasDueToSpell(SPELL_MAGE_ENHANCED_PYROTECHNICS);
                else
                    caster->AddAura(SPELL_MAGE_ENHANCED_PYROTECHNICS, caster);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_enhanced_pyrotechnics_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_enhanced_pyrotechnics_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_enhanced_pyrotechnics_AuraScript();
        }
};

// 210154 - Erosion
class spell_mage_legion_erosion_periodic : public SpellScriptLoader
{
    public:
        spell_mage_legion_erosion_periodic() : SpellScriptLoader("spell_mage_legion_erosion_periodic") { }

        class spell_mage_legion_erosion_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_erosion_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_EROSION_DEBUFF
                });
            }

            void HandlePeriodicTick(AuraEffect const* aurEff)
            {
                if (aurEff->GetTickNumber() > 3)
                    GetTarget()->RemoveAuraFromStack(SPELL_MAGE_EROSION_DEBUFF, GetCasterGUID());
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_erosion_periodic_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_erosion_periodic_AuraScript();
        }

        class spell_mage_legion_erosion_periodic_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_erosion_periodic_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_EROSION_DEBUFF
                });
            }

            void SetDuration()
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (Unit* target = GetHitUnit())
                {
                    int32 duration = GetSpellInfo()->GetMaxDuration();
                    if (Aura* erosionDebuff = target->GetAura(SPELL_MAGE_EROSION_DEBUFF, caster->GetGUID()))
                        duration += erosionDebuff->GetStackAmount() * 1000;

                    if (Aura* erosionPeriodic = target->GetAura(GetSpellInfo()->Id, caster->GetGUID()))
                    {
                        erosionPeriodic->SetMaxDuration(duration);
                        erosionPeriodic->SetDuration(duration);
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_mage_legion_erosion_periodic_SpellScript::SetDuration);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_erosion_periodic_SpellScript();
        }
};

// 205039 - Erosion
class spell_mage_legion_erosion_proc : public SpellScriptLoader
{
    public:
        spell_mage_legion_erosion_proc() : SpellScriptLoader("spell_mage_legion_erosion_proc") { }

        class spell_mage_legion_erosion_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_erosion_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_EROSION_PERIODIC
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_MAGE_EROSION_PERIODIC, true);
            }

            void Register() override
            {
                AfterEffectProc += AuraEffectProcFn(spell_mage_legion_erosion_proc_AuraScript::OnProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_erosion_proc_AuraScript();
        }
};

// 187301 - Everywhere At Once
class spell_mage_legion_everywhere_at_once : public SpellScriptLoader
{
    public:
        spell_mage_legion_everywhere_at_once() : SpellScriptLoader("spell_mage_legion_everywhere_at_once") { }

        class spell_mage_legion_everywhere_at_once_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_everywhere_at_once_AuraScript);

            void HandleEffectCalcSpellModDuration(AuraEffect const* aurEff, SpellModifier*& spellMod)
            {
                if (!spellMod)
                {
                    spellMod = new SpellModifier(GetAura());
                    spellMod->op = SPELLMOD_DURATION;
                    spellMod->type = SPELLMOD_FLAT;
                    spellMod->spellId = GetId();
                    spellMod->mask = GetSpellInfo()->GetEffect(aurEff->GetEffIndex())->SpellClassMask;
                }
                spellMod->value = aurEff->GetAmount() * 1000;
            }

            void HandleEffectCalcSpellModCooldown(AuraEffect const* aurEff, SpellModifier*& spellMod)
            {
                if (!spellMod)
                {
                    spellMod = new SpellModifier(GetAura());
                    spellMod->op = SPELLMOD_COOLDOWN;
                    spellMod->type = SPELLMOD_FLAT;
                    spellMod->spellId = GetId();
                    spellMod->mask = GetSpellInfo()->GetEffect(aurEff->GetEffIndex())->SpellClassMask;
                }
                spellMod->value = -((GetEffect(EFFECT_0)->GetAmount() * 2) * 1000);
            }

            void Register() override
            {
                DoEffectCalcSpellMod += AuraEffectCalcSpellModFn(spell_mage_legion_everywhere_at_once_AuraScript::HandleEffectCalcSpellModDuration, EFFECT_0, SPELL_AURA_ADD_FLAT_MODIFIER);
                DoEffectCalcSpellMod += AuraEffectCalcSpellModFn(spell_mage_legion_everywhere_at_once_AuraScript::HandleEffectCalcSpellModCooldown, EFFECT_1, SPELL_AURA_ADD_FLAT_MODIFIER);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_everywhere_at_once_AuraScript();
        }
};

// 12051 - Evocation
class spell_mage_legion_evocation : public SpellScriptLoader
{
    public:
        spell_mage_legion_evocation() : SpellScriptLoader("spell_mage_legion_evocation") { }

        class spell_mage_legion_evocation_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_evocation_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_AEGWYNN_S_ASCENDANCE_DAMAGE,
                    SPELL_MAGE_AEGWYNN_S_ASCENDANCE_TRAIT,
                    SPELL_MAGE_AEGWYNN_S_ASCENDANCE_VISUAL
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                manaAmountOnApply = GetTarget()->GetPower(POWER_MANA);
                if (GetTarget()->HasAura(SPELL_MAGE_AEGWYNN_S_ASCENDANCE_TRAIT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_AEGWYNN_S_ASCENDANCE_VISUAL, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (AuraEffect* aurEff = GetTarget()->GetAuraEffect(SPELL_MAGE_AEGWYNN_S_ASCENDANCE_TRAIT, EFFECT_0))
                {
                    GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_AEGWYNN_S_ASCENDANCE_VISUAL);
                    int32 damage = GetTarget()->GetPower(POWER_MANA) - manaAmountOnApply;
                    damage = CalculatePct(damage, aurEff->GetAmount());
                    GetTarget()->CastCustomSpell(SPELL_MAGE_AEGWYNN_S_ASCENDANCE_DAMAGE, SPELLVALUE_BASE_POINT0, damage, GetTarget(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_mage_legion_evocation_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_OBS_MOD_POWER, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_evocation_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_OBS_MOD_POWER, AURA_EFFECT_HANDLE_REAL);
            }

            private:
                int32 manaAmountOnApply;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_evocation_AuraScript();
        }
};

// 112965 - Fingers of Frost
class spell_mage_legion_fingers_of_frost : public SpellScriptLoader
{
    public:
        spell_mage_legion_fingers_of_frost() : SpellScriptLoader("spell_mage_legion_fingers_of_frost") { }

        class spell_mage_legion_fingers_of_frost_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_fingers_of_frost_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_FROZEN_TOUCH,
                    SPELL_MAGE_FROSTBOLT_DAMAGE,
                    SPELL_MAGE_FROZEN_ORB_DAMAGE,
                    SPELL_MAGE_BLIZZARD_DAMAGE,
                    SPELL_MAGE_FINGERS_OF_FROST_BUFF,
                    SPELL_MAGE_PET_WATER_JET,
                    SPELL_MAGE_DEEP_SHATTER_HONOR_TALENT
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (!procInfo.GetSpellInfo() || GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;

                uint32 procSpell = procInfo.GetSpellInfo()->Id;

                if (procSpell != SPELL_MAGE_FROSTBOLT_DAMAGE && procSpell != SPELL_MAGE_FROZEN_ORB_DAMAGE && procSpell != SPELL_MAGE_BLIZZARD_DAMAGE)
                    return false;

                int32 procChance = 0;
                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(100));  // small cooldown to avoid x hit checks

                if (procSpell == SPELL_MAGE_FROSTBOLT_DAMAGE && !GetTarget()->HasAura(SPELL_MAGE_DEEP_SHATTER_HONOR_TALENT))
                    if (Unit* pet = ObjectAccessor::GetUnit(*GetTarget(), GetTarget()->GetPetGUID()))
                        if (procInfo.GetActionTarget()->HasAura(SPELL_MAGE_PET_WATER_JET, pet->GetGUID()))
                            return true;

                if (procSpell == SPELL_MAGE_FROSTBOLT_DAMAGE || procSpell == SPELL_MAGE_FROZEN_ORB_DAMAGE)
                    if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_0))
                        procChance += effInfo->CalcValue(GetTarget());

                if (procSpell == SPELL_MAGE_BLIZZARD_DAMAGE)
                    if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_1))
                        procChance += effInfo->CalcValue(GetTarget());

                if (GetTarget()->HasAura(SPELL_MAGE_FROZEN_TOUCH))
                    AddPct(procChance, sSpellMgr->AssertSpellInfo(SPELL_MAGE_FROZEN_TOUCH)->GetEffect(EFFECT_0)->CalcValue(GetTarget()));

                return roll_chance_i(procChance);
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_FINGERS_OF_FROST_BUFF, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_fingers_of_frost_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_mage_legion_fingers_of_frost_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_fingers_of_frost_AuraScript();
        }
};

// 133 - Fireball
class spell_mage_legion_fireball : public SpellScriptLoader
{
    public:
        spell_mage_legion_fireball() : SpellScriptLoader("spell_mage_legion_fireball") { }

        class spell_mage_legion_fireball_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_fireball_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_UNSTABLE_MAGIC_TALENT,
                    SPELL_MAGE_UNSTABLE_MAGIC_FIRE
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                // Unstable Magic (fire)
                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_MAGE_UNSTABLE_MAGIC_TALENT, EFFECT_2))
                    if (AuraEffect const* aurEffBp = GetCaster()->GetAuraEffect(SPELL_MAGE_UNSTABLE_MAGIC_TALENT, EFFECT_3))
                        if (roll_chance_i(aurEff->GetAmount()))
                        {
                            int32 damage = CalculatePct(GetHitDamage(), aurEffBp->GetAmount());
                            GetCaster()->CastCustomSpell(GetHitUnit(), SPELL_MAGE_UNSTABLE_MAGIC_FIRE, &damage, NULL, NULL, true);
                        }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_fireball_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_fireball_SpellScript();
        }
};

// 203283 - Firestarter
class spell_mage_legion_firestarter : public SpellScriptLoader
{
    public:
        spell_mage_legion_firestarter() : SpellScriptLoader("spell_mage_legion_firestarter") { }

        class spell_mage_legion_firestarter_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_firestarter_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_COMBUSTION,
                    SPELL_MAGE_FIREBALL
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                return procInfo.GetSpellInfo() && procInfo.GetSpellInfo()->Id == SPELL_MAGE_FIREBALL;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_MAGE_COMBUSTION, -(aurEff->GetAmount() * 1000));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_firestarter_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_firestarter_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_firestarter_AuraScript();
        }
};

// 203284 - Flamecannon
class spell_mage_legion_flamecannon : public SpellScriptLoader
{
    public:
        spell_mage_legion_flamecannon() : SpellScriptLoader("spell_mage_legion_flamecannon") { }

        class spell_mage_legion_flamecannon_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_flamecannon_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_TIME_ANOMALY_ENERGIZE,
                    SPELL_MAGE_ARCANE_POWER
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                lastPos = GetTarget()->GetPosition();
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (!GetTarget()->IsInCombat())
                    return;

                Position currentPos(*GetTarget());
                if ((currentPos.GetPositionX() == lastPos.GetPositionX()) && (currentPos.GetPositionY() == lastPos.GetPositionY()))
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_FLAMECANNON_BONUS, true);
                else
                    lastPos = currentPos;
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_mage_legion_flamecannon_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_flamecannon_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }

            private:
                Position lastPos;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_flamecannon_AuraScript();
        }
};

// 205029 - Flame on
class spell_mage_legion_flame_on : public SpellScriptLoader
{
   public:
       spell_mage_legion_flame_on() : SpellScriptLoader("spell_mage_legion_flame_on") { }

       class spell_mage_legion_flame_on_AuraScript : public AuraScript
       {
           PrepareAuraScript(spell_mage_legion_flame_on_AuraScript);

           void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
           {
               canBeRecalculated = false;
               // I dont know what blizzard is doing here... for a 100% correct fix we need a float value here
               amount = -16;
           }

           void Register() override
           {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_mage_legion_flame_on_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_CHARGE_RECOVERY_MULTIPLIER);
           }
       };

       AuraScript* GetAuraScript() const override
       {
           return new spell_mage_legion_flame_on_AuraScript();
       }
};

// 2120 - Flamestrike
class spell_mage_legion_flamestrike : public SpellScriptLoader
{
    public:
        spell_mage_legion_flamestrike() : SpellScriptLoader("spell_mage_legion_flamestrike") { }

        class spell_mage_legion_flamestrike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_flamestrike_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_FLAME_PATCH_TALENT,
                    SPELL_MAGE_FLAME_PATCH_AREATRIGGER,
                    SPELL_MAGE_PYROMANIAC,
                    SPELL_MAGE_HOT_STREAK,
                    SPELL_MAGE_AFTERSHOCKS_TRAIT,
                    SPELL_MAGE_AFTERSHOCKS_DAMAGE,
                    SPELL_HOTFIX_HOT_STREAK_FLAMESTRIKE_HELPER
                });
            }

            void CheckHotStreak()
            {
                hotStreakActive = GetCaster()->HasAura(SPELL_MAGE_HOT_STREAK);

                if (hotStreakActive)
                    GetCaster()->CastSpell(GetCaster(), SPELL_HOTFIX_HOT_STREAK_FLAMESTRIKE_HELPER, true);
            }

            void EffectHit(SpellEffIndex /*effIndex*/)
            {
                WorldLocation* dest = GetHitDest();

                if (GetCaster()->HasAura(SPELL_MAGE_FLAME_PATCH_TALENT))
                    GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_MAGE_FLAME_PATCH_AREATRIGGER, true);

                if (GetCaster()->HasAura(SPELL_MAGE_AFTERSHOCKS_TRAIT))
                    GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_MAGE_AFTERSHOCKS_DAMAGE, true);

                if (AuraEffect const* pyromaniac = GetCaster()->GetAuraEffect(SPELL_MAGE_PYROMANIAC, EFFECT_0))
                    if (hotStreakActive && roll_chance_i(pyromaniac->GetAmount()))
                        GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_HOT_STREAK, true);
            }

            void Register() override
            {
                OnCast += SpellCastFn(spell_mage_legion_flamestrike_SpellScript::CheckHotStreak);
                OnEffectHit += SpellEffectFn(spell_mage_legion_flamestrike_SpellScript::EffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
            private:
                bool hotStreakActive = false;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_flamestrike_SpellScript();
        }
};

// 203282 - Flare Up
class spell_mage_legion_flare_up : public SpellScriptLoader
{
    public:
        spell_mage_legion_flare_up() : SpellScriptLoader("spell_mage_legion_flare_up") { }

        class spell_mage_legion_flare_up_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_flare_up_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_FIRE_BLAST,
                    SPELL_MAGE_IGNITE
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget()->HasAura(SPELL_MAGE_IGNITE, GetTarget()->GetGUID()))
                    GetTarget()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_MAGE_FIRE_BLAST)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_flare_up_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_flare_up_AuraScript();
        }
};

// 44544 - Fingers of Frosts Buff
class spell_mage_legion_fingers_of_frost_buff : public SpellScriptLoader
{
    public:
        spell_mage_legion_fingers_of_frost_buff() : SpellScriptLoader("spell_mage_legion_fingers_of_frost_buff") { }

        class spell_mage_legion_fingers_of_frost_buff_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_fingers_of_frost_buff_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_FINGERS_OF_FROST_VISUAL_R
                });
            }

            void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetStackAmount() == 2)
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_FINGERS_OF_FROST_VISUAL_R, true);
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_FINGERS_OF_FROST_VISUAL_R);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_mage_legion_fingers_of_frost_buff_AuraScript::HandleApply, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_mage_legion_fingers_of_frost_buff_AuraScript::HandleRemove, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_fingers_of_frost_buff_AuraScript();
        }
};

class FlurryEvent : public BasicEvent
{
    public:
        FlurryEvent(Unit* caster, ObjectGuid const& targetGUID, uint8 count) : _caster(caster), _targetGUID(targetGUID), _count(count)
        {
            ASSERT(_count, "Passed wrong counter.");
        }

        bool Execute(uint64 time, uint32 /*diff*/) override
        {
            Unit* target = ObjectAccessor::GetUnit(*_caster, _targetGUID);

            if (!target)
                return true;

            _caster->CastSpell(target, SPELL_MAGE_FLURRY_DAMAGE, true);

            if (!--_count)
                return true;

            _caster->m_Events.AddEvent(this, time + 140);
            return false;
        }

    private:
        Unit* _caster;
        ObjectGuid _targetGUID;
        uint8 _count;
};

// 44614 - Flurry
class spell_mage_legion_flurry : public SpellScriptLoader
{
    public:
        spell_mage_legion_flurry() : SpellScriptLoader("spell_mage_legion_flurry") { }

        class spell_mage_legion_flurry_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_flurry_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_FLURRY_DAMAGE
                });
            }

            void EffectHit(SpellEffIndex)
            {
                GetCaster()->m_Events.AddEvent(new FlurryEvent(GetCaster(), GetHitUnit()->GetGUID(), GetEffectValue()), GetCaster()->m_Events.CalculateTime(1));
                if (GetCaster()->HasAura(SPELL_MAGE_BRAIN_FREEZE_PROC))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_HOTFIX_BRAIN_FREEZE_HELPER, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_flurry_SpellScript::EffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_flurry_SpellScript();
        }
};

// 228354 - Flurry
class spell_mage_legion_flurry_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_flurry_damage() : SpellScriptLoader("spell_mage_legion_flurry_damage") { }

        class spell_mage_legion_flurry_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_flurry_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_BRAIN_FREEZE_PROC
                });
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* brainFreezeHelper = GetCaster()->GetAuraEffect(SPELL_MAGE_HOTFIX_BRAIN_FREEZE_HELPER, EFFECT_0))
                {
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), brainFreezeHelper->GetAmount()));

                    if (GetCaster()->HasAura(SPELL_MAGE_BRAIN_FREEZE_LEVEL_BONUS))
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_WINTER_S_CHILL, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_flurry_damage_SpellScript::HandleDamage, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_flurry_damage_SpellScript();
        }
};

// 33395 - Freeze (Level 110)
class spell_mage_legion_freeze_pet : public SpellScriptLoader
{
    public:
        spell_mage_legion_freeze_pet() : SpellScriptLoader("spell_mage_legion_freeze_pet") { }

        class spell_mage_legion_freeze_pet_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_freeze_pet_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_DEEP_SHATTER_HONOR_TALENT,
                    SPELL_MAGE_FINGERS_OF_FROST_BUFF
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                if (Unit* owner = GetCaster()->GetCharmerOrOwnerOrSelf())
                    if (!owner->HasAura(SPELL_MAGE_DEEP_SHATTER_HONOR_TALENT))
                        owner->CastSpell(owner, SPELL_MAGE_FINGERS_OF_FROST_BUFF, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_freeze_pet_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_freeze_pet_SpellScript();
        }

        class spell_mage_legion_freeze_pet_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_freeze_pet_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CHILLED_TO_THE_BONE_HONOR,
                    SPELL_MAGE_CHILLED_TO_THE_BONE_DAMAGE,
                    SPELL_MAGE_CHILLED_TO_THE_BONE_DISPEL
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (Unit* owner = caster->GetCharmerOrOwner())
                    {
                        if (owner->HasAura(SPELL_MAGE_CHILLED_TO_THE_BONE_HONOR))
                        {
                            if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_ENEMY_SPELL)
                                owner->CastSpell(GetTarget(), SPELL_MAGE_CHILLED_TO_THE_BONE_DISPEL, true);
                            else
                                owner->CastSpell(GetTarget(), SPELL_MAGE_CHILLED_TO_THE_BONE_DAMAGE, true);
                        }
                    }
                }
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_mage_legion_freeze_pet_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_ROOT_2, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_freeze_pet_AuraScript();
        }
};

// 122 - Frost Nova
class spell_mage_legion_frost_nova : public SpellScriptLoader
{
    public:
        spell_mage_legion_frost_nova() : SpellScriptLoader("spell_mage_legion_frost_nova") { }

        class spell_mage_legion_frost_nova_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_frost_nova_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CHILLED_TO_THE_BONE_HONOR,
                    SPELL_MAGE_CHILLED_TO_THE_BONE_DAMAGE,
                    SPELL_MAGE_CHILLED_TO_THE_BONE_DISPEL
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_MAGE_CHILLED_TO_THE_BONE_HONOR))
                    {
                        if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_ENEMY_SPELL)
                            caster->CastSpell(GetTarget(), SPELL_MAGE_CHILLED_TO_THE_BONE_DISPEL, true);
                        else
                            caster->CastSpell(GetTarget(), SPELL_MAGE_CHILLED_TO_THE_BONE_DAMAGE, true);
                    }
                }
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_mage_legion_frost_nova_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_MOD_ROOT_2, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_frost_nova_AuraScript();
        }
};

// 228597 - Frostbolt Damage
class spell_mage_legion_frostbolt_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_frostbolt_damage() : SpellScriptLoader("spell_mage_legion_frostbolt_damage") { }

        class spell_mage_legion_frostbolt_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_frostbolt_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_MAGE_UNSTABLE_MAGIC_TALENT,
                    SPELL_MAGE_UNSTABLE_MAGIC_FROST,
                    SPELL_MAGE_BRAIN_FREEZE_PASSIVE,
                    SPELL_MAGE_BRAIN_FREEZE_PROC,
                    SPELL_MAGE_CHILLED,
                    SPELL_MAGE_BONE_CHILLING_PROC,
                    SPELL_MAGE_BONE_CHILLING_BONUS,
                    SPELL_MAGE_DEEP_SHATTER_HONOR_TALENT
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                Player* caster = GetCaster()->ToPlayer();

                if (!caster || caster == GetHitUnit())
                    return;

                // Unstable Magic (frost)
                if (AuraEffect const* aurEff = caster->GetAuraEffect(SPELL_MAGE_UNSTABLE_MAGIC_TALENT, EFFECT_1))
                    if (AuraEffect const* aurEffBp = caster->GetAuraEffect(SPELL_MAGE_UNSTABLE_MAGIC_TALENT, EFFECT_3))
                        if (roll_chance_i(aurEff->GetAmount()))
                        {
                            int32 damage = CalculatePct(GetHitDamage(), aurEffBp->GetAmount());
                            caster->CastCustomSpell(GetHitUnit(), SPELL_MAGE_UNSTABLE_MAGIC_FROST, &damage, NULL, NULL, true);
                        }

                // Brain Freeze proc
                if (AuraEffect const* aurEff = caster->GetAuraEffect(SPELL_MAGE_BRAIN_FREEZE_PASSIVE, EFFECT_0))
                    if (roll_chance_i(aurEff->GetAmount()))
                        caster->CastSpell(caster, SPELL_MAGE_BRAIN_FREEZE_PROC, true);

                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_CHILLED, true);

                if (GetCaster()->HasAura(SPELL_MAGE_BONE_CHILLING_PROC))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_BONE_CHILLING_BONUS, true);

                if (AuraEffect* deepShatter = GetCaster()->GetAuraEffect(SPELL_MAGE_DEEP_SHATTER_HONOR_TALENT, EFFECT_0))
                    if (GetHitUnit()->HasAuraState(AURA_STATE_FROZEN))
                        SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), deepShatter->GetAmount()));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_frostbolt_damage_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_frostbolt_damage_SpellScript();
        }
};

// 84721 - Frozen Orb
class spell_mage_legion_frozen_orb_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_frozen_orb_damage() : SpellScriptLoader("spell_mage_legion_frozen_orb_damage") { }

        class spell_mage_legion_frozen_orb_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_frozen_orb_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_MAGE_CHILLED,
                    SPELL_MAGE_BONE_CHILLING_PROC,
                    SPELL_MAGE_BONE_CHILLING_BONUS
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_CHILLED, true);

                if (GetCaster()->HasAura(SPELL_MAGE_BONE_CHILLING_PROC))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_BONE_CHILLING_BONUS, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_frozen_orb_damage_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_frozen_orb_damage_SpellScript();
        }
};

// 195345 - Frozen Veins
class spell_mage_legion_frozen_veins : public SpellScriptLoader
{
    public:
        spell_mage_legion_frozen_veins() : SpellScriptLoader("spell_mage_legion_frozen_veins") { }

        class spell_mage_legion_frozen_veins_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_frozen_veins_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ICY_VEINS
                });
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction(); // prevent console spam
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_MAGE_ICY_VEINS, aurEff->GetAmount());
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_frozen_veins_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_frozen_veins_AuraScript();
        }
};

class spell_mage_legion_glacial_spike : public SpellScriptLoader
{
    public:
        spell_mage_legion_glacial_spike() : SpellScriptLoader("spell_mage_legion_glacial_spike") { }

        class spell_mage_legion_glacial_spike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_glacial_spike_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_GLACIAL_SPIKE_DAMAGE
                });
            }

            void HandleOnPrepare()
            {
                for (IcicleSpells const& spells : IcicleEvent)
                    GetCaster()->RemoveAurasDueToSpell(spells.StaticSpellId);
            }

            void HandleHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_GLACIAL_SPIKE_DAMAGE, true);
            }

            void Register() override
            {
                OnPrepare += SpellPrepareFn(spell_mage_legion_glacial_spike_SpellScript::HandleOnPrepare);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_glacial_spike_SpellScript::HandleHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_glacial_spike_SpellScript();
        }
};

// 228600 - Glacial Spike (damage)
class spell_mage_legion_glacial_spike_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_glacial_spike_damage() : SpellScriptLoader("spell_mage_legion_glacial_spike_damage") { }

        class spell_mage_legion_glacial_spike_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_glacial_spike_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ICICLES_STORE_1,
                    SPELL_MAGE_ICICLES_STORE_2,
                    SPELL_MAGE_ICICLES_STORE_3,
                    SPELL_MAGE_ICICLES_STORE_4,
                    SPELL_MAGE_ICICLES_STORE_5,
                    SPELL_MAGE_ICICLES_INFO_BUFF,
                    SPELL_MAGE_GLACIAL_SPIKE_USABLE,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_1,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_2,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_3,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_4,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_5
                });
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                int32 damage = GetHitDamage();
                for (IcicleSpells const& spells : IcicleEvent)
                {
                    if (AuraEffect const* icicle = GetCaster()->GetAuraEffect(spells.StoreSpellId, EFFECT_0))
                        damage += icicle->GetAmount();

                    GetCaster()->RemoveAurasDueToSpell(spells.StoreSpellId);
                    GetCaster()->RemoveAurasDueToSpell(spells.StaticSpellId);
                    GetCaster()->RemoveAuraFromStack(SPELL_MAGE_ICICLES_INFO_BUFF);
                }
                GetCaster()->RemoveAura(SPELL_MAGE_GLACIAL_SPIKE_USABLE);
                SetHitDamage(damage);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_glacial_spike_damage_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_glacial_spike_damage_SpellScript();
        }
};

// 110960 - Greater Invisibility
class spell_mage_legion_greater_invisibility : public SpellScriptLoader
{
    public:
        spell_mage_legion_greater_invisibility() : SpellScriptLoader("spell_mage_legion_greater_invisibility") { }

        class spell_mage_legion_greater_invisibility_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_greater_invisibility_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_INVISIBILITY_DMG_REDUCE
                });
            }

            void ApplyEffect(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_INVISIBILITY_DMG_REDUCE, true, nullptr, aurEff);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Aura* aura = GetTarget()->GetAura(SPELL_MAGE_INVISIBILITY_DMG_REDUCE))
                {
                    aura->SetMaxDuration(3000);
                    aura->SetDuration(3000);
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_mage_legion_greater_invisibility_AuraScript::ApplyEffect, EFFECT_1, SPELL_AURA_MOD_INVISIBILITY, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_greater_invisibility_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_MOD_INVISIBILITY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_greater_invisibility_AuraScript();
        }
};

// 203286 - Greater Pyroblast
class spell_mage_legion_greater_pyroblast : public SpellScriptLoader
{
    public:
        spell_mage_legion_greater_pyroblast() : SpellScriptLoader("spell_mage_legion_greater_pyroblast") { }

        class spell_mage_legion_greater_pyroblast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_greater_pyroblast_SpellScript);

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (Unit* target = GetHitUnit())
                    SetHitDamage(int32(target->CountPctFromMaxHealth(GetEffectValue())));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_greater_pyroblast_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_greater_pyroblast_SpellScript();
        }
};

// 48107 - Heaten Up (Needed for Controlled Burn)
class spell_mage_legion_heaten_up : public SpellScriptLoader
{
    public:
        spell_mage_legion_heaten_up() : SpellScriptLoader("spell_mage_legion_heaten_up") { }

        class spell_mage_legion_heaten_up_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_heaten_up_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_CONTROLLED_BURN,
                    SPELL_MAGE_HEATING_UP,
                    SPELL_MAGE_HOT_STREAK
                });
            }

            void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (AuraEffect const* controlledBurn = caster->GetAuraEffect(SPELL_MAGE_CONTROLLED_BURN, EFFECT_0))
                    if (roll_chance_i(controlledBurn->GetAmount()))
                    {
                        caster->RemoveAura(SPELL_MAGE_HEATING_UP);
                        caster->CastSpell(caster, SPELL_MAGE_HOT_STREAK, true);
                    }
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectRemoveFn(spell_mage_legion_heaten_up_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_heaten_up_AuraScript();
        }
};

// 44448 - Hot Streak
class spell_mage_legion_hot_streak : public SpellScriptLoader
{
    public:
        spell_mage_legion_hot_streak() : SpellScriptLoader("spell_mage_legion_hot_streak") { }

        class spell_mage_legion_hot_streak_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_hot_streak_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_DRAGONS_BREATH,
                    SPELL_MAGE_ALEXSTRASZA_S_FURY,
                    SPELL_MAGE_CINDERSTORM_DAMAGE,
                    SPELL_MAGE_HOT_STREAK,
                    SPELL_MAGE_HEATING_UP
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (procInfo.GetSpellInfo()->Id != SPELL_MAGE_DRAGONS_BREATH &&
                    ((procInfo.GetDamageInfo() && procInfo.GetDamageInfo()->GetDamageType() != SPELL_DIRECT_DAMAGE) || procInfo.GetSpellInfo()->IsTargetingArea(DIFFICULTY_NONE) || procInfo.GetSpellInfo()->IsAffectingArea(DIFFICULTY_NONE)))
                    return false;

                if (procInfo.GetSpellInfo()->Id == SPELL_MAGE_DRAGONS_BREATH)
                {
                    if (!GetTarget()->HasAura(SPELL_MAGE_ALEXSTRASZA_S_FURY))
                        return false;

                    if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                        return false;

                    GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(50));
                }

                // Cinderstorm should'nt proc hot streak
                if (procInfo.GetSpellInfo()->Id == SPELL_MAGE_CINDERSTORM_DAMAGE)
                    return false;

                return !GetTarget()->HasAura(SPELL_MAGE_HOT_STREAK);
            }

            void HandleProc(ProcEventInfo& procInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (procInfo.GetHitMask() & PROC_HIT_CRITICAL)
                {
                    if (!caster->HasAura(SPELL_MAGE_HEATING_UP))
                        caster->CastSpell(caster, SPELL_MAGE_HEATING_UP, true);
                    else
                    {
                        caster->RemoveAura(SPELL_MAGE_HEATING_UP);
                        caster->CastSpell(caster, SPELL_MAGE_HOT_STREAK, true);
                    }
                }
                else
                    caster->RemoveAura(SPELL_MAGE_HEATING_UP);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_hot_streak_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_mage_legion_hot_streak_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_hot_streak_AuraScript();
        }
};

// 11426 - Ice Barrier
class spell_mage_legion_ice_barrier : public SpellScriptLoader
{
   public:
       spell_mage_legion_ice_barrier() : SpellScriptLoader("spell_mage_legion_ice_barrier") { }

       class spell_mage_legion_ice_barrier_AuraScript : public AuraScript
       {
           PrepareAuraScript(spell_mage_legion_ice_barrier_AuraScript);

           bool Validate(SpellInfo const* spellInfo) override
           {
               if (!spellInfo->GetEffect(EFFECT_1))
                   return false;

               return ValidateSpellInfo
               ({
                   SPELL_MAGE_CHILLED
               });
           }

           void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
           {
               canBeRecalculated = false;
               if (Player* caster = GetUnitOwner()->ToPlayer())
               {
                   float amountEff2 = caster->ApplyEffectModifiers(GetSpellInfo(), EFFECT_1, float(GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(caster)));
                   float newAmount = float(caster->GetStat(STAT_INTELLECT)) * 10.0f * amountEff2 * (1.0f + (caster->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE) / 100.0f));

                   amount += int32(newAmount);
               }
           }

           void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
           {
               GetTarget()->CastSpell(eventInfo.GetActor(), SPELL_MAGE_CHILLED, true);
           }

           void Register() override
           {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_mage_legion_ice_barrier_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_ice_barrier_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_DUMMY);
           }
       };

       AuraScript* GetAuraScript() const override
       {
           return new spell_mage_legion_ice_barrier_AuraScript();
       }
};

// 45438 - Ice Block
class spell_mage_legion_ice_block : public SpellScriptLoader
{
    public:
        spell_mage_legion_ice_block() : SpellScriptLoader("spell_mage_legion_ice_block") { }

        class spell_mage_legion_ice_block_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_ice_block_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_MAGE_GLACIAL_INSULATION,
                    SPELL_MAGE_ICE_BARRIER
                });
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_MAGE_GLACIAL_INSULATION))
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_ICE_BARRIER, true);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_ice_block_AuraScript::AfterRemove, EFFECT_3, SPELL_AURA_OBS_MOD_HEALTH, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_ice_block_AuraScript();
        }
};

// 108839 - Ice Floes
class spell_mage_legion_ice_floes : public SpellScriptLoader
{
    public:
        spell_mage_legion_ice_floes() : SpellScriptLoader("spell_mage_legion_ice_floes") { }

        class spell_mage_legion_ice_floes_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_ice_floes_AuraScript);

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (!procInfo.GetSpellInfo() || !procInfo.GetProcSpell())
                    return false;

                return procInfo.GetSpellInfo()->CalcCastTime() != 0 && procInfo.GetProcSpell()->GetCastTime() < (GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(GetTarget()) * IN_MILLISECONDS);
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                GetTarget()->RemoveAuraFromStack(GetId());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_ice_floes_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_mage_legion_ice_floes_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_ice_floes_AuraScript();
        }
};

// 30455 - Ice Lance
class spell_mage_legion_ice_lance : public SpellScriptLoader
{
    public:
        spell_mage_legion_ice_lance() : SpellScriptLoader("spell_mage_legion_ice_lance") { }

        class spell_mage_legion_ice_lance_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_ice_lance_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ICE_LANCE_DAMAGE
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit() != GetExplTargetUnit())
                {
                    if (GetCaster()->HasAura(SPELL_MAGE_SPLITTING_ICE))
                    {
                        if (SpellEffectInfo const* splittingIce = sSpellMgr->AssertSpellInfo(SPELL_MAGE_SPLITTING_ICE)->GetEffect(EFFECT_1))
                        {
                            GetCaster()->CastCustomSpell(SPELL_MAGE_ICE_LANCE_DAMAGE, SPELLVALUE_BASE_POINT1, splittingIce->CalcValue(GetCaster()), GetHitUnit(), TRIGGERED_FULL_MASK);
                            return;
                        }
                    }
                }
                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_ICE_LANCE_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_ice_lance_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_ice_lance_SpellScript();
        }
};

// 148023 - Icicles (periodic)
class spell_mage_legion_mastery_icicles_periodic : public SpellScriptLoader
{
    public:
        static char constexpr const ScriptName[] = "spell_mage_legion_mastery_icicles_periodic";

        spell_mage_legion_mastery_icicles_periodic() : SpellScriptLoader(ScriptName) { }

        class spell_mage_legion_mastery_icicles_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_mastery_icicles_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ICICLES_STORE_1,
                    SPELL_MAGE_ICICLES_STORE_2,
                    SPELL_MAGE_ICICLES_STORE_3,
                    SPELL_MAGE_ICICLES_STORE_4,
                    SPELL_MAGE_ICICLES_STORE_5,
                    SPELL_MAGE_ICICLES_DAMAGE,
                    SPELL_MAGE_ICICLES_INFO_BUFF,
                    SPELL_MAGE_ICICLES_VISUAL_1,
                    SPELL_MAGE_ICICLES_VISUAL_2,
                    SPELL_MAGE_ICICLES_VISUAL_3,
                    SPELL_MAGE_ICICLES_VISUAL_4,
                    SPELL_MAGE_ICICLES_VISUAL_5,
                    SPELL_MAGE_ICICLES_BLACK_1,
                    SPELL_MAGE_ICICLES_BLACK_2,
                    SPELL_MAGE_ICICLES_BLACK_3,
                    SPELL_MAGE_ICICLES_BLACK_4,
                    SPELL_MAGE_ICICLES_BLACK_5,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_1,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_2,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_3,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_4,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_5
                });
            }

            void OnTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    Unit* target = ObjectAccessor::GetUnit(*GetTarget(), _targetGuid);
                    if (!target || !caster->IsValidAttackTarget(target))
                        return;

                    for (uint8 i = 0; i < 5; ++i)
                    {
                        if (AuraEffect const* icicle = caster->GetAuraEffect(IcicleEvent[i].StoreSpellId, EFFECT_0))
                        {
                            int32 amount = icicle->GetAmount();
                            bool blackIce = false;
                            if (AuraEffect* blackIceEff = GetTarget()->GetAuraEffect(SPELL_MAGE_BLACK_ICE_TRAIT, EFFECT_0))
                            {
                                if (roll_chance_i(20))
                                {
                                    blackIce = true;
                                    AddPct(amount, blackIceEff->GetAmount());
                                }
                            }

                            caster->CastSpell(target, blackIce ? IcicleEvent[i].BlackIceSpellId : IcicleEvent[i].ExecuteSpellId, true);
                            caster->CastCustomSpell(SPELL_MAGE_ICICLES_DAMAGE, SPELLVALUE_BASE_POINT0, amount, target, true);
                            caster->RemoveAurasDueToSpell(IcicleEvent[i].StoreSpellId);
                            caster->RemoveAurasDueToSpell(IcicleEvent[i].StaticSpellId);
                            caster->RemoveAuraFromStack(SPELL_MAGE_ICICLES_INFO_BUFF);
                            break;
                        }
                        if (i == 4)
                            SetDuration(0);
                    }
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_mastery_icicles_periodic_AuraScript::OnTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }

        public:
            void SetTarget(Unit* target)
            {
                _targetGuid = target->GetGUID();
            }

        private:
            ObjectGuid _targetGuid;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_mastery_icicles_periodic_AuraScript();
        }
};
char constexpr const spell_mage_legion_mastery_icicles_periodic::ScriptName[];

// 228598 - Ice Lance damage
class spell_mage_legion_ice_lance_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_ice_lance_damage() : SpellScriptLoader("spell_mage_legion_ice_lance_damage") { }
        using IciclesAuraScript = spell_mage_legion_mastery_icicles_periodic::spell_mage_legion_mastery_icicles_periodic_AuraScript;

        class spell_mage_legion_ice_lance_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_ice_lance_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_SPLITTING_ICE,
                    SPELL_MAGE_MASTERY_ICICLES,
                    SPELL_MAGE_GLACIAL_SPIKE,
                    SPELL_MAGE_ICICLES_PERIODIC,
                    SPELL_MAGE_FINGERS_OF_FROST_BUFF,
                    SPELL_MAGE_THERMAL_VOID,
                    SPELL_MAGE_ICY_VEINS,
                    SPELL_MAGE_FROST_BOMB,
                    SPELL_MAGE_FROST_BOMB_DAMAGE
                });
            }

            void SavePctValue(SpellEffIndex /*effIndex*/)
            {
                pctValue = GetEffectValue();
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (caster->HasAura(SPELL_MAGE_MASTERY_ICICLES) && !caster->HasSpell(SPELL_MAGE_GLACIAL_SPIKE))
                {
                    caster->CastSpell(caster, SPELL_MAGE_ICICLES_PERIODIC, true);

                    if (Aura const* aura = caster->GetAura(SPELL_MAGE_ICICLES_PERIODIC))
                        if (IciclesAuraScript* script = aura->GetScript<IciclesAuraScript>(spell_mage_legion_mastery_icicles_periodic::ScriptName))
                            script->SetTarget(GetHitUnit());
                }

                if ((GetHitUnit() == GetExplTargetUnit() && GetHitUnit()->HasAuraState(AURA_STATE_FROZEN)) || GetCaster()->HasAura(SPELL_MAGE_FINGERS_OF_FROST_BUFF) || GetHitUnit()->HasAura(SPELL_MAGE_WINTER_S_CHILL))
                {
                    if (caster->HasSpell(SPELL_MAGE_THERMAL_VOID))
                    {
                        if (Aura* icyVeins = caster->GetAura(SPELL_MAGE_ICY_VEINS))
                        {
                            SpellEffectInfo const* thermalVoid = sSpellMgr->AssertSpellInfo(SPELL_MAGE_THERMAL_VOID)->GetEffect(EFFECT_0);
                            if (!thermalVoid)
                                return;

                            int32 newDuration = icyVeins->GetDuration() + (thermalVoid->CalcValue(caster) * 1000);
                            if (newDuration > icyVeins->GetMaxDuration())
                                icyVeins->SetMaxDuration(newDuration);
                            icyVeins->SetDuration(newDuration);
                        }
                    }

                    // Frost Bomb
                    Unit::AuraList const& auras = GetCaster()->GetSingleCastAuras();
                    for (Unit::AuraList::const_iterator itr = auras.begin(); itr != auras.end(); ++itr)
                    {
                        if ((*itr)->GetId() == SPELL_MAGE_FROST_BOMB)
                        {
                            std::list<AuraApplication*> applications;
                            (*itr)->GetApplicationList(applications);
                            if (!applications.empty())
                                caster->CastSpell(applications.front()->GetTarget(), SPELL_MAGE_FROST_BOMB_DAMAGE, true);
                        }
                    }
                }

                if (GetHitUnit()->HasAuraState(AURA_STATE_FROZEN) || GetCaster()->HasAura(SPELL_MAGE_FINGERS_OF_FROST_BUFF) || GetHitUnit()->HasAura(SPELL_MAGE_WINTER_S_CHILL))
                {
                    // tested on retail - FoF is still removed if target is frozen or has winters chill
                    GetCaster()->RemoveAuraFromStack(SPELL_MAGE_FINGERS_OF_FROST_BUFF);
                    SetHitDamage(GetHitDamage() * 3);
                }

                SetHitDamage(CalculatePct(GetHitDamage(), pctValue));
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_mage_legion_ice_lance_damage_SpellScript::SavePctValue, EFFECT_1, SPELL_EFFECT_DUMMY);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_ice_lance_damage_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

            private:
                int32 pctValue = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_ice_lance_damage_SpellScript();
        }
};

// 157997 - Ice Nova
class spell_mage_legion_ice_nova : public SpellScriptLoader
{
    public:
        spell_mage_legion_ice_nova() : SpellScriptLoader("spell_mage_legion_ice_nova") { }

        class spell_mage_legion_ice_nova_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_ice_nova_SpellScript);

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit() == GetExplTargetUnit())
                {
                    int32 damage = GetHitDamage();
                    AddPct(damage, GetEffectInfo(EFFECT_0)->CalcValue(GetCaster()));
                    SetHitDamage(damage);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_ice_nova_SpellScript::HandleEffectHitTarget, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_ice_nova_SpellScript();
        }
};

// Ignite (target selector) : 197409
class spell_mage_legion_ignite_target_selector : public SpellScriptLoader
{
    public:
        spell_mage_legion_ignite_target_selector() : SpellScriptLoader("spell_mage_legion_ignite_target_selector") { }

        class spell_mage_legion_ignite_target_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_ignite_target_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_IGNITE
                });
            }

            void CheckTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                auto it = std::find_if(targets.begin(), targets.end(), [caster](WorldObject const* a)
                { return a->ToUnit() && a->ToUnit()->HasAura(SPELL_MAGE_IGNITE, caster->GetGUID()); });

                if (it != targets.end())
                    _targetGuid = (*it)->GetGUID();

                targets.remove_if([](WorldObject* target)
                {
                    Unit* unitTarget = target->ToUnit();
                    return !unitTarget || unitTarget->HasAura(SPELL_MAGE_IGNITE) || unitTarget->HasBreakableByDamageCrowdControlAura();
                });

                if (targets.size() > 1)
                    Trinity::Containers::RandomResize(targets, 1);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* initialTarget = ObjectAccessor::GetUnit(*GetCaster(), _targetGuid))
                {
                    if (AuraEffect const* ignite = initialTarget->GetAuraEffect(SPELL_MAGE_IGNITE, EFFECT_0, GetCaster()->GetGUID()))
                    {
                        CustomSpellValues values;
                        values.AddSpellMod(SPELLVALUE_AURA_DURATION, ignite->GetBase()->GetDuration());
                        values.AddSpellMod(SPELLVALUE_BASE_POINT0, ignite->GetAmount());
                        GetCaster()->CastCustomSpell(SPELL_MAGE_IGNITE, values, GetHitUnit(), TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_ignite_target_selector_SpellScript::CheckTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_ignite_target_selector_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }

        private:
            ObjectGuid _targetGuid;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_ignite_target_selector_SpellScript();
        }
};

// 1463 - Incanter's Flow
class spell_mage_legion_incanter_s_flow : public SpellScriptLoader
{
    public:
        spell_mage_legion_incanter_s_flow() : SpellScriptLoader("spell_mage_legion_incanter_s_flow") { }

        class spell_mage_legion_incanter_s_flow_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_incanter_s_flow_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_INCANTER_S_FLOW
                });
            }

            void HandlePeriodicTick(AuraEffect const* aurEff)
            {
                // Incanter's flow should not cycle out of combat
                if (!GetTarget()->IsInCombat())
                {
                    GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_INCANTER_S_FLOW);
                    return;
                }

                if (Aura* aura = GetTarget()->GetAura(SPELL_MAGE_INCANTER_S_FLOW))
                {
                    uint32 stacks = aura->GetStackAmount();
                    if (aurEff->GetAmount() == 0 || (stacks == 1 && aurEff->GetAmount() == 1)) // 0 = stack up, 1 = remove stack
                    {
                        if (stacks == 5)
                            const_cast<AuraEffect*>(aurEff)->SetAmount(1);
                        else if (aurEff->GetAmount() == 1)
                            const_cast<AuraEffect*>(aurEff)->SetAmount(0);
                        else
                            GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_INCANTER_S_FLOW, true, nullptr, aurEff);
                    }
                    else if (stacks > 1 && aurEff->GetAmount() == 1)
                        aura->ModStackAmount(-1);
                }
                else
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_INCANTER_S_FLOW, true, nullptr, aurEff);
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_INCANTER_S_FLOW);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_incanter_s_flow_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_incanter_s_flow_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_incanter_s_flow_AuraScript();
        }
};

// 214626 - Jouster
class spell_mage_legion_jouster : public SpellScriptLoader
{
    public:
        spell_mage_legion_jouster() : SpellScriptLoader("spell_mage_legion_jouster") { }

        class spell_mage_legion_jouster_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_jouster_AuraScript);

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->CastCustomSpell(aurEff->GetSpellEffectInfo()->TriggerSpell, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_jouster_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_jouster_AuraScript();
        }
};

// 155148 - Kindling
class spell_mage_legion_kindling : public SpellScriptLoader
{
    public:
        spell_mage_legion_kindling() : SpellScriptLoader("spell_mage_legion_kindling") { }

        class spell_mage_legion_kindling_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_kindling_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_COMBUSTION
                });
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_MAGE_COMBUSTION, -(aurEff->GetAmount() * IN_MILLISECONDS));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_kindling_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_kindling_AuraScript();
        }
};

// 44457 - Living Bomb
class spell_mage_legion_living_bomb : public SpellScriptLoader
{
    public:
        spell_mage_legion_living_bomb() : SpellScriptLoader("spell_mage_legion_living_bomb") { }

        class spell_mage_legion_living_bomb_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_living_bomb_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_LIVING_BOMB_PERIODIC
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_LIVING_BOMB_PERIODIC, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_living_bomb_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_living_bomb_SpellScript();
        }
};

// 44461 - Living Bomb (AOE)
class spell_mage_legion_living_bomb_aoe : public SpellScriptLoader
{
    public:
        spell_mage_legion_living_bomb_aoe() : SpellScriptLoader("spell_mage_legion_living_bomb_aoe") { }

        class spell_mage_legion_living_bomb_aoe_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_living_bomb_aoe_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_LIVING_BOMB_PERIODIC
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (GetEffectValue() == 1)
                    _spread = false;
            }

            void HandleSpread(SpellEffIndex /*effIndex*/)
            {
                if (_spread && GetHitUnit() != GetExplTargetUnit())
                {
                    CustomSpellValues values;
                    values.AddSpellMod(SPELLVALUE_BASE_POINT1, 1); // spread marker
                    GetCaster()->CastCustomSpell(SPELL_MAGE_LIVING_BOMB_PERIODIC, values, GetHitUnit(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_living_bomb_aoe_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_living_bomb_aoe_SpellScript::HandleSpread, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

         private:
            bool _spread = true;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_living_bomb_aoe_SpellScript();
        }
};

// 217694 - Living Bomb
class spell_mage_legion_living_bomb_periodic : public SpellScriptLoader
{
    public:
        spell_mage_legion_living_bomb_periodic() : SpellScriptLoader("spell_mage_legion_living_bomb_periodic") { }

        class spell_mage_legion_living_bomb_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_living_bomb_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_LIVING_BOMB_AOE
                });
            }

            void AfterRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                AuraRemoveMode removeMode = GetTargetApplication()->GetRemoveMode();
                if (removeMode != AURA_REMOVE_BY_ENEMY_SPELL && removeMode != AURA_REMOVE_BY_EXPIRE)
                    return;

                if (AuraEffect const* aurEff1 = GetEffect(EFFECT_1))
                    if (Unit* caster = GetCaster())
                        caster->CastCustomSpell(SPELL_MAGE_LIVING_BOMB_AOE, SPELLVALUE_BASE_POINT0, aurEff1->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK, nullptr, aurEff);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_living_bomb_periodic_AuraScript::AfterRemove, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_living_bomb_periodic_AuraScript();
        }
};

// 205024 - Lonely Winter
class spell_mage_legion_lonely_winter : public SpellScriptLoader
{
    public:
        spell_mage_legion_lonely_winter() : SpellScriptLoader("spell_mage_legion_lonely_winter") { }

        class spell_mage_legion_lonely_winter_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_lonely_winter_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Player* owner = GetUnitOwner()->ToPlayer();
                if (Pet* pet = owner->GetPet())
                    owner->RemovePet(pet, PET_SAVE_NOT_IN_SLOT);
                owner->SetTemporaryUnsummonedPetNumber(0);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_mage_legion_lonely_winter_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_lonely_winter_AuraScript();
        }
};

// 224968 - Mark of Aluneth
class spell_mage_legion_mark_of_aluneth : public SpellScriptLoader
{
    public:
        spell_mage_legion_mark_of_aluneth() : SpellScriptLoader("spell_mage_legion_mark_of_aluneth") { }

        class spell_mage_legion_mark_of_aluneth_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_mark_of_aluneth_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_MARK_OF_ALUNETH_PERIODIC,
                    SPELL_MAGE_MARK_OF_ALUNETH_DAMAGE
                });
            }

            void HandlePeriodicTick(AuraEffect const* aurEff)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_MAGE_MARK_OF_ALUNETH_PERIODIC, true, nullptr, aurEff);
            }

            void HandleRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    int32 damage = CalculatePct(caster->GetMaxPower(POWER_MANA), aurEff->GetAmount());
                    caster->CastCustomSpell(GetTarget(), SPELL_MAGE_MARK_OF_ALUNETH_DAMAGE, &damage, nullptr, nullptr, true, nullptr, aurEff);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_mark_of_aluneth_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_mark_of_aluneth_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_mark_of_aluneth_AuraScript();
        }
};

// 76613 - Mastery: Icicles
class spell_mage_legion_mastery_icicles : public SpellScriptLoader
{
    public:
        spell_mage_legion_mastery_icicles() : SpellScriptLoader("spell_mage_legion_mastery_icicles") { }

        class spell_mage_legion_mastery_icicles_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_mastery_icicles_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ICICLES_STORE_1,
                    SPELL_MAGE_ICICLES_STORE_2,
                    SPELL_MAGE_ICICLES_STORE_3,
                    SPELL_MAGE_ICICLES_STORE_4,
                    SPELL_MAGE_ICICLES_STORE_5,
                    SPELL_MAGE_ICICLES_DAMAGE,
                    SPELL_MAGE_ICICLES_INFO_BUFF,
                    SPELL_MAGE_GLACIAL_SPIKE_USABLE,
                    SPELL_MAGE_GLACIAL_SPIKE,
                    SPELL_MAGE_ICICLES_VISUAL_1,
                    SPELL_MAGE_ICICLES_VISUAL_2,
                    SPELL_MAGE_ICICLES_VISUAL_3,
                    SPELL_MAGE_ICICLES_VISUAL_4,
                    SPELL_MAGE_ICICLES_VISUAL_5,
                    SPELL_MAGE_ICICLES_BLACK_1,
                    SPELL_MAGE_ICICLES_BLACK_2,
                    SPELL_MAGE_ICICLES_BLACK_3,
                    SPELL_MAGE_ICICLES_BLACK_4,
                    SPELL_MAGE_ICICLES_BLACK_5,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_1,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_2,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_3,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_4,
                    SPELL_MAGE_ICICLES_STATIC_VISUAL_5
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                float mastery = GetTarget()->GetFloatValue(PLAYER_MASTERY) * GetAura()->GetSpellEffectInfo(EFFECT_0)->BonusCoefficient;
                int32 damage = CalculatePct((eventInfo.GetDamageInfo()->GetDamage() + eventInfo.GetDamageInfo()->GetAbsorb()), mastery);
                uint8 iciclesCount = 1;
                if (GetTarget()->HasAura(SPELL_MAGE_ICE_NINE) && roll_chance_i(20))
                    iciclesCount++;

                if (damage <= 0)
                    return;

                for (uint8 i = 0; i < iciclesCount; i++)
                {
                    for (uint32 j = 0; j < 5; ++j)
                    {
                        if (Aura* icicleAura = GetTarget()->GetAura(IcicleEvent[j].StoreSpellId))
                        {
                            if (j == 4)
                            {
                                if (AuraEffect const* icicle = icicleAura->GetEffect(EFFECT_0))
                                {
                                    int32 amount = icicle->GetAmount();
                                    bool blackIce = false;
                                    if (AuraEffect* blackIceEff = GetTarget()->GetAuraEffect(SPELL_MAGE_BLACK_ICE_TRAIT, EFFECT_0))
                                    {
                                        if (roll_chance_i(20))
                                        {
                                            blackIce = true;
                                            AddPct(amount, blackIceEff->GetAmount());
                                        }
                                    }

                                    GetTarget()->CastSpell(eventInfo.GetActionTarget(), blackIce ? IcicleEvent[j].BlackIceSpellId : IcicleEvent[j].ExecuteSpellId, true);

                                    GetTarget()->CastCustomSpell(SPELL_MAGE_ICICLES_DAMAGE, SPELLVALUE_BASE_POINT0, amount, eventInfo.GetActionTarget(), true);
                                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_ICICLES_INFO_BUFF, true);
                                    const_cast<AuraEffect*>(icicle)->ChangeAmount(damage);
                                }
                            }
                            icicleAura->RefreshDuration();
                            SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(IcicleEvent[j].StoreSpellId);
                            if (SpellEffectInfo const* effInfo = spellInfo->GetEffect(EFFECT_1))
                                if (Aura* triggeredAura = GetTarget()->GetAura(effInfo->TriggerSpell))
                                    triggeredAura->RefreshDuration();
                        }
                        else
                        {
                            GetTarget()->CastCustomSpell(IcicleEvent[j].StoreSpellId, SPELLVALUE_BASE_POINT0, damage, GetTarget(), true);
                            GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_ICICLES_INFO_BUFF, true);
                            break;
                        }
                    }
                }

                if (GetTarget()->HasSpell(SPELL_MAGE_GLACIAL_SPIKE))
                    if (Aura* icicleAuraInfo = GetTarget()->GetAura(SPELL_MAGE_ICICLES_INFO_BUFF))
                        if (icicleAuraInfo->GetStackAmount() == 5)
                            GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_GLACIAL_SPIKE_USABLE, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_mastery_icicles_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_mastery_icicles_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_mastery_icicles_AuraScript();
        }
};

// 12846 - Fire Mastery: Ignite
class spell_mage_legion_mastery_ignite : public SpellScriptLoader
{
    public:
        spell_mage_legion_mastery_ignite() : SpellScriptLoader("spell_mage_legion_mastery_ignite") { }

        class spell_mage_legion_mastery_ignite_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_mastery_ignite_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_IGNITE,
                    SPELL_MAGE_IGNITE_TARGET_SELECTOR
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetProcTarget() || !eventInfo.GetDamageInfo())
                    return false;

                if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_MAGE_METEOR_INITIAL)
                    return false;

                return eventInfo.GetDamageInfo()->GetDamage() > 0;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                // we need to split this both helper to prevent possible double use of this bonus
                float igniteMod = 1;
                if (eventInfo.GetSpellInfo()->Id == SPELL_MAGE_PYROBLAST)
                {
                    if (caster->HasAura(SPELL_HOTFIX_HOT_STREAK_PYROBLAST_HELPER))
                    {
                        igniteMod = 2;
                        caster->RemoveAurasDueToSpell(SPELL_HOTFIX_HOT_STREAK_PYROBLAST_HELPER);
                    }
                }

                // dont remove this helper flamestrike is a aoe effect - there is no way to cast it more than 1x in 500ms
                if (eventInfo.GetSpellInfo()->Id == SPELL_MAGE_FLAMESTRIKE)
                    if (caster->HasAura(SPELL_HOTFIX_HOT_STREAK_FLAMESTRIKE_HELPER))
                        igniteMod = 2;

                SpellInfo const* igniteDot = sSpellMgr->AssertSpellInfo(SPELL_MAGE_IGNITE);
                float mastery = caster->GetFloatValue(PLAYER_MASTERY) * 1.5f / 100.0f;
                int32 amount = int32(((eventInfo.GetDamageInfo()->GetDamage() * mastery / 2) * igniteMod) / igniteDot->GetMaxTicks(DIFFICULTY_NONE));
                amount += eventInfo.GetProcTarget()->GetRemainingPeriodicAmount(eventInfo.GetActor()->GetGUID(), SPELL_MAGE_IGNITE, SPELL_AURA_PERIODIC_DAMAGE);
                caster->CastCustomSpell(SPELL_MAGE_IGNITE, SPELLVALUE_BASE_POINT0, amount, eventInfo.GetProcTarget(), true, NULL, aurEff);
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                std::list<Unit*> inRangeUnits;
                for (auto aurApp : GetTarget()->GetTargetAuraApplications(SPELL_MAGE_IGNITE))
                    if (GetTarget()->GetDistance(aurApp->GetTarget()) < 40.00f)
                        inRangeUnits.push_back(aurApp->GetTarget());

                if (inRangeUnits.empty())
                    return;

                if (inRangeUnits.size() > 1)
                    Trinity::Containers::RandomResize(inRangeUnits, 1);

                if (Unit* target = inRangeUnits.front())
                    GetTarget()->CastSpell(target, SPELL_MAGE_IGNITE_TARGET_SELECTOR, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_mastery_ignite_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_mastery_ignite_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_mastery_ignite_AuraScript::HandlePeriodicTick, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_mastery_ignite_AuraScript();
        }
};

// 153564 - Meteor (damage)
class spell_mage_legion_meteor_damage : public SpellScriptLoader
{
    public:
        spell_mage_legion_meteor_damage() : SpellScriptLoader("spell_mage_legion_meteor_damage") { }

        class spell_mage_legion_meteor_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_meteor_damage_SpellScript);

            void CheckTargets(std::list<WorldObject*>& targets)
            {
                _targetsCount = targets.size();
            }

            void RecalculateDamage(SpellEffIndex /*effIndex*/)
            {
                if (_targetsCount)
                    SetHitDamage(GetHitDamage() / _targetsCount);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_meteor_damage_SpellScript::CheckTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_meteor_damage_SpellScript::RecalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

        private:
            uint8 _targetsCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_meteor_damage_SpellScript();
        }
};

// 153561 - Meteor
class spell_mage_legion_meteor_launch : public SpellScriptLoader
{
    public:
        spell_mage_legion_meteor_launch() : SpellScriptLoader("spell_mage_legion_meteor_launch") { }

        class spell_mage_legion_meteor_launch_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_meteor_launch_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_METEOR_PRE_AREATRIGGER
                });
            }

            void EffectHit(SpellEffIndex /*effIndex*/)
            {
                WorldLocation* dest = GetHitDest();
                GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_MAGE_METEOR_PRE_AREATRIGGER, true);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_mage_legion_meteor_launch_SpellScript::EffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_meteor_launch_SpellScript();
        }
};

// 55342 - Mirror Image
class spell_mage_legion_mirror_image : public SpellScriptLoader
{
    public:
        spell_mage_legion_mirror_image() : SpellScriptLoader("spell_mage_legion_mirror_image") { }

        class spell_mage_legion_mirror_image_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_mirror_image_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_MIRROR_IMAGE_FRONT,
                    SPELL_MAGE_MIRROR_IMAGE_RIGHT,
                    SPELL_MAGE_MIRROR_IMAGE_LEFT
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_MIRROR_IMAGE_FRONT, true);
                GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_MIRROR_IMAGE_RIGHT, true);
                GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_MIRROR_IMAGE_LEFT, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_mirror_image_SpellScript::HandleEffectHitTarget, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_mirror_image_SpellScript();
        }
};

// 114923 - Nether Tempest
class spell_mage_legion_nether_tempest : public SpellScriptLoader
{
    public:
        spell_mage_legion_nether_tempest() : SpellScriptLoader("spell_mage_legion_nether_tempest") { }

        class spell_mage_legion_nether_tempest_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_nether_tempest_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_NETHER_TEMPEST_DAMAGE,
                    SPELL_MAGE_NETHER_TEMPEST_VISUAL
                });
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_MAGE_NETHER_TEMPEST_DAMAGE, true);
                GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_NETHER_TEMPEST_VISUAL, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_nether_tempest_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_nether_tempest_AuraScript();
        }

        class spell_mage_legion_nether_tempest_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_nether_tempest_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ARCANE_CHARGE,
                    SPELL_HOTFIX_NETHER_TEMPEST_DMG_HOLDER
                });
            }

            void AddBonusDamage()
            {
                if (Aura* aura = GetHitAura())
                {
                    if (AuraEffect* aurEff = aura->GetEffect(EFFECT_0))
                    {
                        int32 damage = aurEff->GetDamage();
                        if (AuraEffect const* arcaneCharge = GetCaster()->GetAuraEffect(SPELL_MAGE_ARCANE_CHARGE, EFFECT_0))
                            damage = aurEff->GetDamage() + CalculatePct(aurEff->GetDamage(), arcaneCharge->GetAmount());

                        const_cast<AuraEffect*>(aurEff)->SetDamage(damage);
                        const_cast<AuraEffect*>(aurEff)->ChangeAmount(damage);

                        if (GetHitUnit())
                            GetCaster()->CastCustomSpell(SPELL_HOTFIX_NETHER_TEMPEST_DMG_HOLDER, SPELLVALUE_BASE_POINT0, damage, GetHitUnit(), TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_mage_legion_nether_tempest_SpellScript::AddBonusDamage);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_nether_tempest_SpellScript();
        }
};

// 114954 - Nether Tempest
class spell_mage_legion_nether_tempest_aoe : public SpellScriptLoader
{
    public:
        spell_mage_legion_nether_tempest_aoe() : SpellScriptLoader("spell_mage_legion_nether_tempest_aoe") { }

        class spell_mage_legion_nether_tempest_aoe_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_nether_tempest_aoe_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ARCANE_CHARGE
                });
            }

            void CountTargets(std::list<WorldObject*>& targets)
            {
                // why custom spell attr share damage doesnt work here??
                targetCount = targets.size();
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* damageHolder = GetExplTargetUnit()->GetAuraEffect(SPELL_HOTFIX_NETHER_TEMPEST_DMG_HOLDER, EFFECT_0, GetCaster()->GetGUID()))
                    SetHitDamage(damageHolder->GetAmount() / targetCount);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_nether_tempest_aoe_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_nether_tempest_aoe_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
            int32 targetCount;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_nether_tempest_aoe_SpellScript();
        }
};

// 215773 - Phoenix Reborn
class spell_mage_legion_phoenix_reborn : public SpellScriptLoader
{
    public:
        spell_mage_legion_phoenix_reborn() : SpellScriptLoader("spell_mage_legion_phoenix_reborn") { }

        class spell_mage_legion_phoenix_reborn_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_phoenix_reborn_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_PHOENIX_REBORN_DAMAGE,
                    SPELL_MAGE_PHOENIX_S_FLAMES
                });
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_MAGE_PHOENIX_REBORN_DAMAGE, true);
                GetTarget()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_MAGE_PHOENIX_S_FLAMES)->ChargeCategoryId, aurEff->GetAmount() * IN_MILLISECONDS);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_phoenix_reborn_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_phoenix_reborn_AuraScript();
        }
};

// 194466 Phoenix's Flames
class spell_mage_legion_phoenix_s_flames : public SpellScriptLoader
{
    public:
        spell_mage_legion_phoenix_s_flames() : SpellScriptLoader("spell_mage_legion_phoenix_s_flames") { }

        class spell_mage_legion_phoenix_s_flames_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_phoenix_s_flames_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_PHOENIX_S_FLAMES_AOE
                });
            }

            void HandleEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_MAGE_PHOENIX_S_FLAMES_AOE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_phoenix_s_flames_SpellScript::HandleEffectHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_phoenix_s_flames_SpellScript();
        }
};

// 224637 - Phoenix's Flames
class spell_mage_legion_phoenix_s_flames_aoe : public SpellScriptLoader
{
    public:
        spell_mage_legion_phoenix_s_flames_aoe() : SpellScriptLoader("spell_mage_legion_phoenix_s_flames_aoe") { }

        class spell_mage_legion_phoenix_s_flames_aoe_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_phoenix_s_flames_aoe_SpellScript);

            void RemoveExplTarget(std::list<WorldObject*>& targets)
            {
                // should not hit the explicit target
                targets.remove(GetExplTargetUnit());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_phoenix_s_flames_aoe_SpellScript::RemoveExplTarget, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_phoenix_s_flames_aoe_SpellScript();
        }
};

// 235450 - Prismatic Barrier
class spell_mage_legion_prismatic_barrier : public SpellScriptLoader
{
    public:
        spell_mage_legion_prismatic_barrier() : SpellScriptLoader("spell_mage_legion_prismatic_barrier") { }

        class spell_mage_legion_prismatic_barrier_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_prismatic_barrier_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_MAGE_MANA_SHIELD
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                if (Player* caster = GetUnitOwner()->ToPlayer())
                {
                    float amountEff2 = caster->ApplyEffectModifiers(GetSpellInfo(), EFFECT_1, float(GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(caster)));
                    float newAmount = float(caster->GetStat(STAT_INTELLECT)) * 7.0f * amountEff2 * (1.0f + (caster->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE) / 100.0f));

                    amount += int32(newAmount);
                }
            }

            void OnAbsorb(AuraEffect* /*aurEff*/, DamageInfo& /*dmgInfo*/, uint32& absorbAmount)
            {
                if (AuraEffect const* manaShield = GetTarget()->GetAuraEffect(SPELL_MAGE_MANA_SHIELD, EFFECT_0))
                    GetTarget()->ModifyPower(POWER_MANA, -int32(CalculatePct(absorbAmount, manaShield->GetAmount())));
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_mage_legion_prismatic_barrier_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_mage_legion_prismatic_barrier_AuraScript::OnAbsorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_prismatic_barrier_AuraScript();
        }
};

// 194331 - Pyretic Incantation
class spell_mage_legion_pyretic_incantation : public SpellScriptLoader
{
    public:
        spell_mage_legion_pyretic_incantation() : SpellScriptLoader("spell_mage_legion_pyretic_incantation") { }

        class spell_mage_legion_pyretic_incantation_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_pyretic_incantation_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_PYRETIC_INCANTATION_BONUS
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetHitMask() & PROC_HIT_CRITICAL)
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_PYRETIC_INCANTATION_BONUS, true);
                else
                    GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_PYRETIC_INCANTATION_BONUS);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_pyretic_incantation_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_pyretic_incantation_AuraScript();
        }
};

// 11366 - Pyroblast
class spell_mage_legion_pyroblast : public SpellScriptLoader
{
    public:
        spell_mage_legion_pyroblast() : SpellScriptLoader("spell_mage_legion_pyroblast") { }

        class spell_mage_legion_pyroblast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_pyroblast_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_PYROMANIAC,
                    SPELL_MAGE_HOT_STREAK,
                    SPELL_HOTFIX_HOT_STREAK_PYROBLAST_HELPER
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_MAGE_HOT_STREAK))
                {
                    hotSTreakActive = true;
                    ///@TODO: new proc system
                    // Currently hot streak is removed on hit instead of after cast
                    GetCaster()->RemoveAurasDueToSpell(SPELL_MAGE_HOT_STREAK);
                    GetCaster()->CastSpell(GetCaster(), SPELL_HOTFIX_HOT_STREAK_PYROBLAST_HELPER, true);
                }
            }

            void EffectHit()
            {
                if (AuraEffect const* pyromaniac = GetCaster()->GetAuraEffect(SPELL_MAGE_PYROMANIAC, EFFECT_0))
                    if (hotSTreakActive && roll_chance_i(pyromaniac->GetAmount()))
                        GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_HOT_STREAK, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_mage_legion_pyroblast_SpellScript::HandleAfterCast);
                AfterHit += SpellHitFn(spell_mage_legion_pyroblast_SpellScript::EffectHit);
            }

            private:
                bool hotSTreakActive = false;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_pyroblast_SpellScript();
        }
};

// 198924 - Quickening
class spell_mage_legion_quickening_triggered : public SpellScriptLoader
{
    public:
        spell_mage_legion_quickening_triggered() : SpellScriptLoader("spell_mage_legion_quickening_triggered") { }

        class spell_mage_legion_quickening_triggered_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_quickening_triggered_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_ARCANE_BARRAGE
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (!procInfo.GetSpellInfo() || procInfo.GetSpellInfo()->Id != SPELL_MAGE_ARCANE_BARRAGE)
                    return false;
                return true;
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                Remove();
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_quickening_triggered_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_mage_legion_quickening_triggered_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_quickening_triggered_AuraScript();
        }
};

// 205021 - Ray of Frost
class spell_mage_legion_ray_of_frost : public SpellScriptLoader
{
    public:
        spell_mage_legion_ray_of_frost() : SpellScriptLoader("spell_mage_legion_ray_of_frost") { }

        class spell_mage_legion_ray_of_frost_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_ray_of_frost_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RAY_OF_FROST_BONUS
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->CastSpell(caster, SPELL_MAGE_RAY_OF_FROST_BONUS, true);
                    // Crazy hack... seems there is currently no other way to recalculate EVERYTHING (including mods and and and)
                    GetAura()->SetStackAmount(1);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_ray_of_frost_AuraScript::HandleEffectPeriodic, EFFECT_1, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_ray_of_frost_AuraScript();
        }

        class spell_mage_legion_ray_of_frost_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_ray_of_frost_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RAY_OF_FROST_CHANNEL_CHECK
                });
            }

            void CastChannelCheck()
            {
                if (!GetCaster()->HasAura(SPELL_MAGE_RAY_OF_FROST_CHANNEL_CHECK))
                    GetCaster()->CastSpell(GetCaster(), SPELL_MAGE_RAY_OF_FROST_CHANNEL_CHECK, true);
            }

            void Register() override
            {
                OnCast += SpellCastFn(spell_mage_legion_ray_of_frost_SpellScript::CastChannelCheck);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_ray_of_frost_SpellScript();
        }
};

// 208166 - Ray of Frost
class spell_mage_legion_ray_of_frost_channel_check : public SpellScriptLoader
{
    public:
        spell_mage_legion_ray_of_frost_channel_check() : SpellScriptLoader("spell_mage_legion_ray_of_frost_channel_check") { }

        class spell_mage_legion_ray_of_frost_channel_check_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_ray_of_frost_channel_check_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RAY_OF_FROST_BONUS,
                    SPELL_MAGE_RAY_OF_FROST
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_RAY_OF_FROST_BONUS);
                if (Spell* rayOfFrost = GetTarget()->GetCurrentSpell(CURRENT_CHANNELED_SPELL))
                    if (rayOfFrost->GetSpellInfo()->Id == SPELL_MAGE_RAY_OF_FROST)
                        rayOfFrost->cancel();

                GetTarget()->GetSpellHistory()->HandleCooldowns(sSpellMgr->AssertSpellInfo(SPELL_MAGE_RAY_OF_FROST), nullptr, nullptr);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_mage_legion_ray_of_frost_channel_check_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_ray_of_frost_channel_check_AuraScript();
        }
};

// 136511 - Ring of Frost
class spell_mage_legion_ring_of_frost : public SpellScriptLoader
{
    public:
        spell_mage_legion_ring_of_frost() : SpellScriptLoader("spell_mage_legion_ring_of_frost") { }

        class spell_mage_legion_ring_of_frost_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_ring_of_frost_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RING_OF_FROST_DUMMY,
                    SPELL_MAGE_RING_OF_FROST_FREEZE,
                    SPELL_MAGE_RING_OF_FROST_SUMMON
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (TempSummon* ringOfFrost = GetRingOfFrostMinion())
                {
                    _ringOfFrostGUID = ringOfFrost->GetGUID();
                    GetTarget()->CastSpell(ringOfFrost->GetPositionX(), ringOfFrost->GetPositionY(), ringOfFrost->GetPositionZ(), SPELL_MAGE_RING_OF_FROST_FREEZE, true);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_ring_of_frost_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
            }

        private:
            TempSummon* GetRingOfFrostMinion() const
            {
                if (Creature* creature = ObjectAccessor::GetCreature(*GetOwner(), _ringOfFrostGUID))
                    return creature->ToTempSummon();

                for (Unit* minion : GetTarget()->m_Controlled)
                    if (minion->GetEntry() == uint32(sSpellMgr->AssertSpellInfo(SPELL_MAGE_RING_OF_FROST_SUMMON)->GetEffect(EFFECT_0)->MiscValue))
                        return minion->ToTempSummon();

                return nullptr;
            }

            ObjectGuid _ringOfFrostGUID;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_ring_of_frost_AuraScript();
        }
};

// 82691 - Ring of Frost (freeze efect)
class spell_mage_legion_ring_of_frost_freeze : public SpellScriptLoader
{
    public:
        spell_mage_legion_ring_of_frost_freeze() : SpellScriptLoader("spell_mage_legion_ring_of_frost_freeze") { }

        class spell_mage_legion_ring_of_frost_freeze_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_ring_of_frost_freeze_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RING_OF_FROST_SUMMON,
                    SPELL_MAGE_RING_OF_FROST_DUMMY,
                    SPELL_MAGE_RING_OF_FROST_FREEZE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                WorldLocation const* dest = GetExplTargetDest();
                float outRadius = sSpellMgr->AssertSpellInfo(SPELL_MAGE_RING_OF_FROST_SUMMON)->GetEffect(EFFECT_2)->CalcRadius();
                float inRadius  = 4.7f;
                uint32 maxTargets = 10;

                for (WorldObject* target : targets)
                    if (target->ToUnit() && target->ToUnit()->HasAura(GetSpellInfo()->Id, GetCaster()->GetGUID()))
                        --maxTargets;

                if (maxTargets <= 0)
                {
                    targets.clear();
                    return;
                }

                targets.remove_if([dest, outRadius, inRadius](WorldObject* target)
                {
                    Unit* unit = target->ToUnit();
                    if (!unit)
                        return true;
                    return unit->HasAura(SPELL_MAGE_RING_OF_FROST_DUMMY) || unit->HasAura(SPELL_MAGE_RING_OF_FROST_FREEZE) || unit->GetExactDist(dest) > outRadius || unit->GetExactDist(dest) < inRadius;
                });

                if (uint32(targets.size()) > maxTargets)
                    Trinity::Containers::RandomResize(targets, maxTargets);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_ring_of_frost_freeze_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_ring_of_frost_freeze_SpellScript();
        }

        class spell_mage_legion_ring_of_frost_freeze_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_ring_of_frost_freeze_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_RING_OF_FROST_DUMMY
                });
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE)
                    if (GetCaster())
                        GetCaster()->CastSpell(GetTarget(), SPELL_MAGE_RING_OF_FROST_DUMMY, true);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_ring_of_frost_freeze_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_ring_of_frost_freeze_AuraScript();
        }
};

// 227481 - Scorched Earth
class spell_mage_legion_scorched_earth : public SpellScriptLoader
{
    public:
        spell_mage_legion_scorched_earth() : SpellScriptLoader("spell_mage_legion_scorched_earth") { }

        class spell_mage_legion_scorched_earth_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_scorched_earth_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_SCORCHED_EARTH_SPEED_BUFF
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_SCORCHED_EARTH_SPEED_BUFF, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_scorched_earth_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_scorched_earth_AuraScript();
        }
};

// 210732 - Sloooow Down
class spell_mage_legion_sloooow_down_triggered : public SpellScriptLoader
{
    public:
        spell_mage_legion_sloooow_down_triggered() : SpellScriptLoader("spell_mage_legion_sloooow_down_triggered") { }

        class spell_mage_legion_sloooow_down_triggered_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_sloooow_down_triggered_SpellScript);

            void CheckTargets(std::list<WorldObject*>& targets)
            {
                // spell should only hit the targets around the initial target
                targets.remove(GetExplTargetUnit());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_sloooow_down_triggered_SpellScript::CheckTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_sloooow_down_triggered_SpellScript();
        }
};

// 157980 - Supernova
class spell_mage_legion_supernova : public SpellScriptLoader
{
    public:
        spell_mage_legion_supernova() : SpellScriptLoader("spell_mage_legion_supernova") { }

        class spell_mage_legion_supernova_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_supernova_SpellScript);

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetExplTargetUnit() == GetHitUnit())
                {
                    uint32 damage = GetHitDamage();
                    if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_0))
                        AddPct(damage, effInfo->CalcValue());
                    SetHitDamage(damage);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_mage_legion_supernova_SpellScript::HandleDamage, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_supernova_SpellScript();
        }
};

// 198111 - Temporal Shield (Level 110)
class spell_mage_legion_temporal_shield : public SpellScriptLoader
{
    public:
        spell_mage_legion_temporal_shield() : SpellScriptLoader("spell_mage_legion_temporal_shield") { }

        class spell_mage_legion_temporal_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_temporal_shield_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_TEMPORAL_RIPPLES
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (!procInfo.GetDamageInfo() || procInfo.GetDamageInfo()->GetDamage() == 0 || !procInfo.GetSpellInfo() || procInfo.GetSpellInfo()->IsPositive())
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                const_cast<AuraEffect*>(aurEff)->SetAmount(aurEff->GetAmount() + eventInfo.GetDamageInfo()->GetDamage());
            }

            void RemoveEffect(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastCustomSpell(SPELL_MAGE_TEMPORAL_RIPPLES, SPELLVALUE_BASE_POINT0, aurEff->GetAmount() - aurEff->GetBaseAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_temporal_shield_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_temporal_shield_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_temporal_shield_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_temporal_shield_AuraScript();
        }
};

// 210805 - Time Anomaly
class spell_mage_legion_time_anomaly : public SpellScriptLoader
{
    public:
        spell_mage_legion_time_anomaly() : SpellScriptLoader("spell_mage_legion_time_anomaly") { }

        class spell_mage_legion_time_anomaly_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_time_anomaly_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_TIME_ANOMALY_ENERGIZE,
                    SPELL_MAGE_ARCANE_POWER
                });
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (roll_chance_i(GetSpellInfo()->ProcChance))
                {
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_TIME_ANOMALY_ENERGIZE, true);
                    GetTarget()->AddAura(SPELL_MAGE_ARCANE_POWER, GetTarget());
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_time_anomaly_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_time_anomaly_AuraScript();
        }
};

// 80353 - Time Warp
class spell_mage_legion_time_warp : public SpellScriptLoader
{
    public:
        spell_mage_legion_time_warp() : SpellScriptLoader("spell_mage_legion_time_warp") { }

        class spell_mage_legion_time_warp_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_mage_legion_time_warp_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_TEMPORAL_DISPLACEMENT,
                    SPELL_HUNTER_INSANITY,
                    SPELL_SHAMAN_EXHAUSTION,
                    SPELL_SHAMAN_SATED
                });
            }

            void RemoveInvalidTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(true, SPELL_MAGE_TEMPORAL_DISPLACEMENT));
                targets.remove_if(Trinity::UnitAuraCheck(true, SPELL_HUNTER_INSANITY));
                targets.remove_if(Trinity::UnitAuraCheck(true, SPELL_SHAMAN_EXHAUSTION));
                targets.remove_if(Trinity::UnitAuraCheck(true, SPELL_SHAMAN_SATED));
            }

            void ApplyDebuff()
            {
                if (Unit* target = GetHitUnit())
                    target->CastSpell(target, SPELL_MAGE_TEMPORAL_DISPLACEMENT, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mage_legion_time_warp_SpellScript::RemoveInvalidTargets, EFFECT_ALL, TARGET_UNIT_CASTER_AREA_RAID);
                AfterHit += SpellHitFn(spell_mage_legion_time_warp_SpellScript::ApplyDebuff);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_mage_legion_time_warp_SpellScript();
        }
};

// 203275 - Tinder
class spell_mage_legion_tinder : public SpellScriptLoader
{
    public:
        spell_mage_legion_tinder() : SpellScriptLoader("spell_mage_legion_tinder") { }

        class spell_mage_legion_tinder_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_tinder_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_TINDER_DUMMY_PROC
                });
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (!GetTarget()->HasAura(SPELL_MAGE_TINDER_DEBUFF) && !GetTarget()->HasAura(SPELL_MAGE_TINDER_DUMMY_PROC))
                    GetTarget()->CastSpell(GetTarget(), SPELL_MAGE_TINDER_DUMMY_PROC, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_TINDER_DUMMY_PROC);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_mage_legion_tinder_AuraScript::HandlePeriodicTick, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_mage_legion_tinder_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_tinder_AuraScript();
        }
};

// 203277 - Tinder
class spell_mage_legion_tinder_dummy_proc : public SpellScriptLoader
{
    public:
        spell_mage_legion_tinder_dummy_proc() : SpellScriptLoader("spell_mage_legion_tinder_dummy_proc") { }

        class spell_mage_legion_tinder_dummy_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_tinder_dummy_proc_AuraScript);


            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), aurEff->GetAmount(), true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_tinder_dummy_proc_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_tinder_dummy_proc_AuraScript();
        }
};

// 203278 - Tinder
class spell_mage_legion_tinder_blocker : public SpellScriptLoader
{
    public:
        spell_mage_legion_tinder_blocker() : SpellScriptLoader("spell_mage_legion_tinder_blocker") { }

        class spell_mage_legion_tinder_blocker_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_tinder_blocker_AuraScript);


            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetAura()->RefreshDuration();
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_tinder_blocker_AuraScript::HandleProc, EFFECT_2, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_tinder_blocker_AuraScript();
        }
};

// 210824 - Touch of the Magi
class spell_mage_legion_touch_of_the_magi : public SpellScriptLoader
{
    public:
        spell_mage_legion_touch_of_the_magi() : SpellScriptLoader("spell_mage_legion_touch_of_the_magi") { }

        class spell_mage_legion_touch_of_the_magi_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_touch_of_the_magi_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_TOUCH_OF_THE_MAGI_TRAIT,
                    SPELL_MAGE_TOUCH_OF_THE_MAGI_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                return GetCaster() && procInfo.GetActor()->GetGUID() == GetCasterGUID() && procInfo.GetDamageInfo() != nullptr;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                if (AuraEffect* aurEff = GetEffect(EFFECT_0))
                {
                    if (SpellEffectInfo const* triggerEffInfo = sSpellMgr->AssertSpellInfo(SPELL_MAGE_TOUCH_OF_THE_MAGI_TRAIT)->GetEffect(EFFECT_0))
                    {
                        int32 bonusDamage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), triggerEffInfo->CalcValue(GetCaster()));
                        aurEff->SetAmount(aurEff->GetAmount() + bonusDamage);
                    }
                }
            }

            void RemoveEffect(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(SPELL_MAGE_TOUCH_OF_THE_MAGI_DAMAGE, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_touch_of_the_magi_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_mage_legion_touch_of_the_magi_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
                AfterEffectRemove += AuraEffectRemoveFn(spell_mage_legion_touch_of_the_magi_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_touch_of_the_magi_AuraScript();
        }
};

// 210725 - Touch of the Magi
class spell_mage_legion_touch_of_the_magi_trigger : public SpellScriptLoader
{
    public:
        spell_mage_legion_touch_of_the_magi_trigger() : SpellScriptLoader("spell_mage_legion_touch_of_the_magi_trigger") { }

        class spell_mage_legion_touch_of_the_magi_trigger_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_mage_legion_touch_of_the_magi_trigger_AuraScript);

            bool CheckProc(ProcEventInfo& procInfo)
            {
                return GetTarget() != procInfo.GetActionTarget();
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_mage_legion_touch_of_the_magi_trigger_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_mage_legion_touch_of_the_magi_trigger_AuraScript();
        }
};

class areatrigger_mage_legion_arcane_orb : public AreaTriggerEntityScript
{
    public:
        areatrigger_mage_legion_arcane_orb() : AreaTriggerEntityScript("areatrigger_mage_legion_arcane_orb") { }

        struct areatrigger_mage_legion_arcane_orbAI : public AreaTriggerAI
        {
            areatrigger_mage_legion_arcane_orbAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnDestinationReached() override
            {
                at->Remove();
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                    if (!caster->IsFriendlyTo(target))
                        caster->CastSpell(target, SPELL_MAGE_ARCANE_ORB_DAMAGE, true);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_mage_legion_arcane_orbAI(areaTrigger);
        }
};

class areatrigger_mage_legion_blizzard : public AreaTriggerEntityScript
{
    public:
        areatrigger_mage_legion_blizzard() : AreaTriggerEntityScript("areatrigger_mage_legion_blizzard") { }

        struct areatrigger_mage_legion_blizzardAI : public AreaTriggerAI
        {
            areatrigger_mage_legion_blizzardAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                int32 periodicTick = at->GetDuration() / 8;
                _scheduler.Schedule(Milliseconds(periodicTick), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_MAGE_BLIZZARD_DAMAGE, true);

                    task.Repeat();
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_mage_legion_blizzardAI(areaTrigger);
        }
};

class areatrigger_mage_legion_cinderstorm : public AreaTriggerEntityScript
{
    public:
        areatrigger_mage_legion_cinderstorm() : AreaTriggerEntityScript("areatrigger_mage_legion_cinderstorm") { }

        struct areatrigger_mage_legion_cinderstormAI : public AreaTriggerAI
        {
            areatrigger_mage_legion_cinderstormAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                    if (caster->IsValidAttackTarget(target))
                        caster->CastSpell(target, SPELL_MAGE_CINDERSTORM_DAMAGE, true);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_mage_legion_cinderstormAI(areaTrigger);
        }
};

// Created by spell 205470 - Flame Patch
class areatrigger_mage_legion_flame_patch : public AreaTriggerEntityScript
{
    public:
        areatrigger_mage_legion_flame_patch() : AreaTriggerEntityScript("areatrigger_mage_legion_flame_patch") { }

        struct areatrigger_mage_legion_flame_patchAI : public AreaTriggerAI
        {
            areatrigger_mage_legion_flame_patchAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                int32 periodicTick = at->GetDuration() / 8;
                _scheduler.Schedule(Milliseconds(periodicTick), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_MAGE_FLAME_PATCH_DAMAGE, true);

                    task.Repeat();
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_mage_legion_flame_patchAI(areaTrigger);
        }
};

class areatrigger_mage_legion_frozen_orb : public AreaTriggerEntityScript
{
    public:
        areatrigger_mage_legion_frozen_orb() : AreaTriggerEntityScript("areatrigger_mage_legion_frozen_orb") { }

        struct areatrigger_mage_legion_frozen_orbAI : public AreaTriggerAI
        {
            areatrigger_mage_legion_frozen_orbAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnDestinationReached() override
            {
                at->Remove();
            }

            void OnCreate() override
            {
                if (at->GetSpellId() == SPELL_MAGE_FROZEN_ORB_SUMMON_STATIC)
                    _reShaped = true;

                _scheduler.Schedule(Milliseconds(750), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_MAGE_FROZEN_ORB_DAMAGE, true);
                    task.Repeat();
                });
            }

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsValidAttackTarget(unit))
                    {
                        if (!_reShaped)
                        {
                            _reShaped = true;
                            if (AreaTriggerMiscTemplate const* areaTriggerMiscTemplate = at->GetMiscTemplate())
                                at->InitSplineOffsets(areaTriggerMiscTemplate->SplinePoints, 50000 - at->GetTimeSinceCreated());
                        }

                        if (!_fofCasted)
                        {
                            _fofCasted = true;
                            caster->CastSpell(caster, SPELL_MAGE_FINGERS_OF_FROST_BUFF, true);
                        }
                    }
                }
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
            bool _reShaped = false;
            bool _fofCasted = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_mage_legion_frozen_orbAI(areaTrigger);
        }
};

class areatrigger_mage_legion_meteor : public AreaTriggerEntityScript
{
    public:
        areatrigger_mage_legion_meteor() : AreaTriggerEntityScript("areatrigger_mage_legion_meteor") { }

        struct areatrigger_mage_legion_meteorAI : public AreaTriggerAI
        {
            areatrigger_mage_legion_meteorAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                _scheduler.Schedule(Seconds(2), [this](TaskContext /*task*/)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_MAGE_METEOR_DAMAGE, false);
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_mage_legion_meteorAI(areaTrigger);
        }
};

void AddSC_legion_mage_spell_scripts()
{
    new spell_mage_legion_arcane_barrage();
    new spell_mage_legion_arcane_blast();
    new spell_mage_legion_arcane_explosion(),
    new spell_mage_legion_arcane_familiar();
    new spell_mage_legion_arcane_missiles();
    new spell_mage_legion_arcane_missiles_passive();
    new spell_mage_legion_blazing_barrier();
    new spell_mage_legion_blazing_soul();
    new spell_mage_legion_blink();
    new spell_mage_legion_blizzard_bonus();
    new spell_mage_legion_blizzard_damage();
    new spell_mage_legion_burst_of_cold();
    new spell_mage_legion_cauterize();
    new spell_mage_legion_cinderstorm_damage();
    new spell_mage_legion_chilled_to_the_core();
    new spell_mage_legion_chrono_shift();
    new spell_mage_legion_cold_snap();
    new spell_mage_legion_combustion();
    new spell_mage_legion_comet_storm();
    new spell_mage_legion_comet_storm_damage();
    new spell_mage_legion_cone_of_cold();
    new spell_mage_legion_conflagration_periodic();
    new spell_mage_legion_conjure_refreshment();
    new spell_mage_legion_dampened_magic();
    new spell_mage_legion_displacement();
    new spell_mage_legion_dragons_breath();
    new spell_mage_legion_ebonbolt();
    new spell_mage_legion_enhanced_pyrotechnics();
    new spell_mage_legion_erosion_periodic();
    new spell_mage_legion_erosion_proc();
    new spell_mage_legion_everywhere_at_once();
    new spell_mage_legion_evocation();
    new spell_mage_legion_fingers_of_frost();
    new spell_mage_legion_fingers_of_frost_buff();
    new spell_mage_legion_fireball();
    new spell_mage_legion_firestarter();
    new spell_mage_legion_flamecannon();
    new spell_mage_legion_flame_on();
    new spell_mage_legion_flamestrike();
    new spell_mage_legion_flare_up();
    new spell_mage_legion_flurry();
    new spell_mage_legion_flurry_damage();
    new spell_mage_legion_freeze_pet();
    new spell_mage_legion_frost_nova();
    new spell_mage_legion_frostbolt_damage();
    new spell_mage_legion_frozen_orb_damage();
    new spell_mage_legion_frozen_veins();
    new spell_mage_legion_glacial_spike();
    new spell_mage_legion_glacial_spike_damage();
    new spell_mage_legion_greater_invisibility();
    new spell_mage_legion_greater_pyroblast();
    new spell_mage_legion_heaten_up();
    new spell_mage_legion_hot_streak();
    new spell_mage_legion_ice_barrier();
    new spell_mage_legion_ice_block();
    new spell_mage_legion_ice_floes();
    new spell_mage_legion_ice_lance();
    new spell_mage_legion_ice_lance_damage();
    new spell_mage_legion_ice_nova();
    new spell_mage_legion_ignite_target_selector();
    new spell_mage_legion_incanter_s_flow();
    new spell_mage_legion_jouster();
    new spell_mage_legion_kindling();
    new spell_mage_legion_living_bomb();
    new spell_mage_legion_living_bomb_aoe();
    new spell_mage_legion_living_bomb_periodic();
    new spell_mage_legion_lonely_winter();
    new spell_mage_legion_mark_of_aluneth();
    new spell_mage_legion_mastery_icicles();
    new spell_mage_legion_mastery_icicles_periodic();
    new spell_mage_legion_mastery_ignite();
    new spell_mage_legion_meteor_damage();
    new spell_mage_legion_meteor_launch();
    new spell_mage_legion_mirror_image();
    new spell_mage_legion_nether_tempest();
    new spell_mage_legion_nether_tempest_aoe();
    new spell_mage_legion_phoenix_reborn();
    new spell_mage_legion_phoenix_s_flames();
    new spell_mage_legion_phoenix_s_flames_aoe();
    new spell_mage_legion_prismatic_barrier();
    new spell_mage_legion_pyretic_incantation();
    new spell_mage_legion_pyroblast();
    new spell_mage_legion_quickening_triggered();
    new spell_mage_legion_ray_of_frost();
    new spell_mage_legion_ray_of_frost_channel_check();
    new spell_mage_legion_ring_of_frost();
    new spell_mage_legion_ring_of_frost_freeze();
    new spell_mage_legion_scorched_earth();
    new spell_mage_legion_sloooow_down_triggered();
    new spell_mage_legion_supernova();
    new spell_mage_legion_temporal_shield();
    new spell_mage_legion_time_anomaly();
    new spell_mage_legion_time_warp();
    new spell_mage_legion_tinder();
    new spell_mage_legion_tinder_dummy_proc();
    new spell_mage_legion_tinder_blocker();
    new spell_mage_legion_touch_of_the_magi();
    new spell_mage_legion_touch_of_the_magi_trigger();
    new areatrigger_mage_legion_arcane_orb();
    new areatrigger_mage_legion_blizzard();
    new areatrigger_mage_legion_cinderstorm();
    new areatrigger_mage_legion_flame_patch();
    new areatrigger_mage_legion_frozen_orb();
    new areatrigger_mage_legion_meteor();
}
