/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_SHAMAN and SPELLFAMILY_GENERIC spells used by shaman players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_legion_sha_".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "CellImpl.h"
#include "CreatureAIImpl.h"
#include "G3D/Vector3.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "MoveSplineInitArgs.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"
#include "Totem.h"

/*
    TODO:
          - Elemental(s) melee damage
*/

enum ShamanSpells
{
    SPELL_SHAMAN_AFTERSHOCK_ENERGIZE                = 210712,
    SPELL_SHAMAN_ANCESTRAL_GUIDANCE                 = 108281,
    SPELL_SHAMAN_ANCESTRAL_GUIDANCE_HEAL            = 114911,
    SPELL_SHAMAN_ASCENDANCE_ELEMENTAL               = 114050,
    SPELL_SHAMAN_BOTTOMLESS_DEPTHS                  = 197467,
    SPELL_SHAMAN_BOULDERFIST                        = 218825,
    SPELL_SHAMAN_CARESS_OF_THE_TIDEMOTHER_HEAL      = 209950,
    SPELL_SHAMAN_CARESS_OF_THE_TIDEMOTHER_TALENT    = 207354,
    SPELL_SHAMAN_CHAIN_HEAL                         = 1064,
    SPELL_SHAMAN_CHAIN_LIGHTNING                    = 188443,
    SPELL_SHAMAN_CHAIN_LIGHTNING_ENERGIZE           = 195897,
    SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD           = 45297,
    SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD_ENERGIZE  = 218558,
    SPELL_SHAMAN_CLOUDBURST_HEAL                    = 157503,
    SPELL_SHAMAN_CLOUDBURST_TOTEM                   = 157504,
    SPELL_SHAMAN_CONTROL_OF_LAVA_TALENT             = 204393,
    SPELL_SHAMAN_COUNTERSTRIKE_DAMAGE               = 233433,
    SPELL_SHAMAN_CRASH_LIGHTNING_BUFF               = 187878,
    SPELL_SHAMAN_CRASHING_STORM_AREATRIGGER         = 210797,
    SPELL_SHAMAN_CRASHING_STORM_DAMAGE              = 210801,
    SPELL_SHAMAN_CRASHING_STORM_TALENT              = 192246,
    SPELL_SHAMAN_CRASHING_WAVES                     = 197464,
    SPELL_SHAMAN_CUMULATIVE_UPKEEP                  = 207362,
    SPELL_SHAMAN_CUMULATIVE_UPKEEP_BUFF             = 208205,
    SPELL_SHAMAN_DELUGE_AURA                        = 200075,
    SPELL_SHAMAN_DELUGE_TALENT                      = 200076,
    SPELL_SHAMAN_DOOM_VORTEX_DAMAGE                 = 199116,
    SPELL_SHAMAN_DOOM_WINDS                         = 204945,
    SPELL_SHAMAN_DOOM_WOLVES                        = 198505,
    SPELL_SHAMAN_EARTH_ELEMENTAL_PRIMAL             = 118323,
    SPELL_SHAMAN_EARTH_ELEMENTAL_SUMMON             = 188616,
    SPELL_SHAMAN_EARTH_ELEMENTAL_TOTEM              = 198103,
    SPELL_SHAMAN_EARTH_SHIELD_HEAL                  = 204290,
    SPELL_SHAMAN_EARTH_SHOCK                        = 8042,
    SPELL_SHAMAN_EARTHBIND_FOR_EARTHGRAB_TOTEM      = 116947,
    SPELL_SHAMAN_EARTHEN_RAGE_DAMAGE                = 170379,
    SPELL_SHAMAN_EARTHEN_RAGE_PASSIVE               = 170374,
    SPELL_SHAMAN_EARTHEN_RAGE_PERIODIC              = 170377,
    SPELL_SHAMAN_EARTHEN_SHIELD_ABSORB              = 201633,
    SPELL_SHAMAN_EARTHEN_SHIELD_DAMAGE              = 201657,
    SPELL_SHAMAN_EARTHFURY                          = 204398,
    SPELL_SHAMAN_EARTHFURY_STUN                     = 204399,
    SPELL_SHAMAN_EARTHGRAB                          = 64695,
    SPELL_SHAMAN_EARTHGRAB_IMMUNITY                 = 116946,
    SPELL_SHAMAN_EARTHQUAKE_DAMAGE                  = 77478,
    SPELL_SHAMAN_EARTHQUAKE_KNOCKDOWN               = 77505,
    SPELL_SHAMAN_ECHO_OF_THE_ELEMENTS               = 108283,
    SPELL_SHAMAN_ELECTROCUTE                        = 206642,
    SPELL_SHAMAN_ELECTROCUTE_DAMAGE                 = 206647,
    SPELL_SHAMAN_ELEMENTAL_BLAST                    = 117014,
    SPELL_SHAMAN_ELEMENTAL_BLAST_CRIT               = 118522,
    SPELL_SHAMAN_ELEMENTAL_BLAST_HASTE              = 173183,
    SPELL_SHAMAN_ELEMENTAL_BLAST_MASTERY            = 173184,
    SPELL_SHAMAN_ELEMENTAL_BLAST_OVERLOAD           = 120588,
    SPELL_SHAMAN_ELEMENTAL_HEALING                  = 198248,
    SPELL_SHAMAN_ELEMENTAL_HEALING_HEAL             = 198249,
    SPELL_SHAMAN_ELEMENTAL_OVERLOAD                 = 168534,
    SPELL_SHAMAN_EMPOWERED_STORMLASH                = 210731,
    SPELL_SHAMAN_EXHAUSTION                         = 57723,
    SPELL_SHAMAN_FERAL_LUNGE_DAMAGE                 = 215802,
    SPELL_SHAMAN_FERAL_SPIRIT_ENHANCEMENT_SUMMON    = 228562,
    SPELL_SHAMAN_FIRE_ELEMENTAL_PRIMAL              = 118291,
    SPELL_SHAMAN_FIRE_ELEMENTAL_SUMMON              = 188592,
    SPELL_SHAMAN_FIRE_ELEMENTAL_TOTEM               = 198067,
    SPELL_SHAMAN_FIRE_NOVA                          = 198480,
    SPELL_SHAMAN_FLAME_SHOCK                        = 188389,
    SPELL_SHAMAN_FLAMETONGUE_ATTACK                 = 10444,
    SPELL_SHAMAN_FLAMETONGUE_BUFF                   = 194084,
    SPELL_SHAMAN_FORKED_LIGHTNING                   = 204349,
    SPELL_SHAMAN_FORKED_LIGHTNING_DAMAGE            = 204350,
    SPELL_SHAMAN_FROST_SHOCK                        = 196840,
    SPELL_SHAMAN_FROSTBRAND                         = 196834,
    SPELL_SHAMAN_FURY_OF_AIR                        = 197211,
    SPELL_SHAMAN_GATHERING_STORMS                   = 198299,
    SPELL_SHAMAN_GATHERING_STORMS_BUFF              = 198300,
    SPELL_SHAMAN_HAILSTORM                          = 210853,
    SPELL_SHAMAN_HAILSTORM_DAMAGE                   = 210854,
    SPELL_SHAMAN_HEALING_RAIN                       = 142923,
    SPELL_SHAMAN_HEALING_RAIN_TICK                  = 73921,
    SPELL_SHAMAN_HEALING_STREAM_TOTEM               = 5394,
    SPELL_SHAMAN_HEALING_SURGE                      = 8004,
    SPELL_SHAMAN_HEALING_SURGE_INSTANT_MARKER       = 190900,
    SPELL_SHAMAN_HEALING_TIDE_TOTEM_HEAL            = 114942,
    SPELL_SHAMAN_HEALING_WAVE                       = 77472,
    SPELL_SHAMAN_HEX                                = 196942,
    SPELL_SHAMAN_HEXAUSTION                         = 202318,
    SPELL_SHAMAN_ICEFURY                            = 210714,
    SPELL_SHAMAN_ICEFURY_OVERLOAD                   = 219271,
    SPELL_SHAMAN_LAVA_BURST                         = 51505,
    SPELL_SHAMAN_LAVA_BURST_OVERLOAD                = 77451,
    SPELL_SHAMAN_LAVA_LASH                          = 60103,
    SPELL_SHAMAN_LAVA_SURGE_PROC                    = 77762,
    SPELL_SHAMAN_LAVA_SURGE_TALENT                  = 77756,
    SPELL_SHAMAN_LIGHTNING_BOLT                     = 188196,
    SPELL_SHAMAN_LIGHTNING_BOLT_ENERGIZE            = 214815,
    SPELL_SHAMAN_LIGHTNING_BOLT_ENHANCEMENT         = 187837,
    SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD            = 45284,
    SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD_ENERGIZE   = 214816,
    SPELL_SHAMAN_LIGHTNING_ROD_DAMAGE               = 197568,
    SPELL_SHAMAN_LIGHTNING_ROD_DEBUFF               = 197209,
    SPELL_SHAMAN_LIGHTNING_ROD_PASSIVE              = 210689,
    SPELL_SHAMAN_LIQUID_MAGMA                       = 192231,
    SPELL_SHAMAN_MAELSTROM_WEAPON_ENERGIZE          = 187890,
    SPELL_SHAMAN_NATURES_ESSENCE                    = 191580,
    SPELL_SHAMAN_OVERCHARGE                         = 210727,
    SPELL_SHAMAN_PATH_OF_FLAMES_SPREAD              = 210621,
    SPELL_SHAMAN_PATH_OF_FLAMES_TALENT              = 201909,
    SPELL_SHAMAN_POWER_OF_THE_MAELSTROM             = 191877,
    SPELL_SHAMAN_PRIMAL_ELEMENTALIST                = 117013,
    SPELL_SHAMAN_PURIFYING_WATERS                   = 204247,
    SPELL_SHAMAN_PURIFYING_WATERS_HEAL              = 204248,
    SPELL_SHAMAN_QUEEN_ASCENDANT_BUFF               = 207288,
    SPELL_SHAMAN_QUEENS_DECREE                      = 207360,
    SPELL_SHAMAN_QUEENS_DECREE_HEAL                 = 208899,
    SPELL_SHAMAN_RAGING_STORMS                      = 192696,
    SPELL_SHAMAN_RAGING_STORMS_BUFF                 = 198361,
    SPELL_SHAMAN_RAINFALL                           = 215864,
    SPELL_SHAMAN_RAINFALL_TICK                      = 215871,
    SPELL_SHAMAN_REFRESHING_CURRENTS                = 207356,
    SPELL_SHAMAN_RESTORATIVE_MISTS                  = 114083,
    SPELL_SHAMAN_RESURGENCE_ENERGIZE                = 101033,
    SPELL_SHAMAN_RIPPLING_WATERS                    = 204269,
    SPELL_SHAMAN_RIPPLING_WATERS_HEAL               = 204281,
    SPELL_SHAMAN_RIPTIDE                            = 61295,
    SPELL_SHAMAN_RUSHING_STREAMS                    = 147074,
    SPELL_SHAMAN_SATED                              = 57724,
    SPELL_SHAMAN_SENSE_OF_URGENCY                   = 207355,
    SPELL_SHAMAN_SENSE_OF_URGENCY_BUFF              = 208416,
    SPELL_SHAMAN_SERVANT_OF_THE_QUEEN               = 207357,
    SPELL_SHAMAN_SERVANT_OF_THE_QUEEN_BUFF          = 207654,
    SPELL_SHAMAN_SHAMANISTIC_HEALING_AURA           = 191591,
    SPELL_SHAMAN_SNOWSTORM                          = 198483,
    SPELL_SHAMAN_SPECTRAL_RECOVERY                  = 204261,
    SPELL_SHAMAN_SPECTRAL_RECOVERY_HEAL             = 204262,
    SPELL_SHAMAN_SPIRIT_BOMB                        = 198455,
    SPELL_SHAMAN_SPIRIT_LINK                        = 98021,
    SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_ENERGIZE   = 204880,
    SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_PERIODIC   = 198240,
    SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_TRAIT      = 198238,
    SPELL_SHAMAN_STATIC_OVERLOAD                    = 191602,
    SPELL_SHAMAN_STORM_ELEMENTAL_PRIMAL             = 157319,
    SPELL_SHAMAN_STORM_ELEMENTAL_SUMMON             = 157299,
    SPELL_SHAMAN_STORM_ELEMENTAL_TOTEM              = 192249,
    SPELL_SHAMAN_STORMBRINGER_PROC                  = 201846,
    SPELL_SHAMAN_STORMFLURRY                        = 198367,
    SPELL_SHAMAN_STORMKEEPER                        = 205495,
    SPELL_SHAMAN_STORMLASH                          = 195255,
    SPELL_SHAMAN_STORMLASH_DAMAGE                   = 195256,
    SPELL_SHAMAN_STORMLASH_FAKE_DAMAGE              = 213307,
    SPELL_SHAMAN_STORMLASH_RAID_DUMMY_AURA          = 195222,
    SPELL_SHAMAN_STORMSTRIKE                        = 32175,
    SPELL_SHAMAN_STORMSTRIKE_OFFHAND                = 32176,
    SPELL_SHAMAN_STORMSTRIKE_TALENT                 = 17364,
    SPELL_SHAMAN_SUMMON_LIGHTNING_ELEMENTAL         = 191716,
    SPELL_SHAMAN_SWELLING_WAVES_HEAL                = 204266,
    SPELL_SHAMAN_THE_GROUND_TREMBLES                = 191499,
    SPELL_SHAMAN_THUNDER_BITE                       = 198485,
    SPELL_SHAMAN_THUNDERSTORM_ALLY                  = 204406,
    SPELL_SHAMAN_THUNDERSTORM_ALLY_DAMAGE           = 204408,
    SPELL_SHAMAN_THUNDERSTORM_ALLY_KNOCKBACK        = 228947,
    SPELL_SHAMAN_TIDAL_TOTEM_AREATRIGGER            = 233487,
    SPELL_SHAMAN_TIDAL_TOTEM_HEAL                   = 209069,
    SPELL_SHAMAN_TIDAL_TOTEM_VISUAL                 = 208932,
    SPELL_SHAMAN_TIDAL_WAVES                        = 53390,
    SPELL_SHAMAN_TOTEMIC_REVIVAL                    = 207553,
    SPELL_SHAMAN_TRAVELING_STORMS                   = 204403,
    SPELL_SHAMAN_UNDULATION                         = 216251,
    SPELL_SHAMAN_UNLEASH_LAVA                       = 199053,
    SPELL_SHAMAN_UNLEASH_LIFE                       = 73685,
    SPELL_SHAMAN_UNLEASH_LIGHTNING                  = 199054,
    SPELL_SHAMAN_VOLCANIC_INFERNO_DAMAGE            = 205533,
    SPELL_SHAMAN_WELLSPRING_HEAL                    = 197997,
    SPELL_SHAMAN_WELLSPRING_MISSILE_HEAL            = 198117,
    SPELL_SHAMAN_WIND_RUSH_TOTEM_BUFF               = 192082,
    SPELL_SHAMAN_WIND_STRIKE                        = 115356,
    SPELL_SHAMAN_WIND_STRIKE_MAINHAND               = 115357,
    SPELL_SHAMAN_WIND_STRIKE_OFFHAND                = 115360,
    SPELL_SHAMAN_WIND_STRIKES                       = 198292,
    SPELL_SHAMAN_WIND_STRIKES_BUFF                  = 198293,
    SPELL_SHAMAN_WINDFURY_ATTACK                    = 25504,
    SPELL_SHAMAN_WINDFURY_ATTACK_OFFHAND            = 33750,
    SPELL_SHAMAN_WINDFURY_ATTACK_VISUAL             = 205648,
    SPELL_SHAMAN_WINDSONG                           = 201898,
    SPELL_SHAMAN_WOLF_FORM                          = 2645,
};

enum SpellVisuals
{
    SPELL_VISUAL_SHAMAN_WELLSPRING = 51498, // 8x
};

enum MiscSpells
{
    SPELL_HUNTER_INSANITY                       = 95809,
    SPELL_MAGE_TEMPORAL_DISPLACEMENT            = 80354,
    SPELL_PET_NETHERWINDS_FATIGUED              = 160455,
    ACTION_LIGHTNING_BOLT                       = 1,
    ACTION_CHAIN_LIGHTNING                      = 2,
};

enum Totems
{
    NPC_HEALING_TIDE_TOTEM  = 59764,
    NPC_CLOUDBURST_TOTEM    = 78001,
};

enum WolfForms
{
    WOLF_NORMAL_FORM    = 0,
    WOLF_FIRE_FORM      = 1,
    WOLF_ICE_FORM       = 2,
    WOLF_LIGHTNING_FORM = 3
};

// 210707 - Aftershock
class spell_legion_sha_aftershock : public SpellScriptLoader
{
    public:
        spell_legion_sha_aftershock() : SpellScriptLoader("spell_legion_sha_aftershock") { }

        class spell_legion_sha_aftershock_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_aftershock_AuraScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_AFTERSHOCK_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetPowerCost(POWER_MAELSTROM);
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 energize = CalculatePct(eventInfo.GetProcSpell()->GetPowerCost(POWER_MAELSTROM), aurEff->GetAmount());
                GetTarget()->CastCustomSpell(SPELL_SHAMAN_AFTERSHOCK_ENERGIZE, SPELLVALUE_BASE_POINT0, energize, GetTarget(), true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_aftershock_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_aftershock_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_aftershock_AuraScript();
        }
};

// 198486 - Alpha Wolf
class spell_legion_sha_alpha_wolf : public SpellScriptLoader
{
    public:
        spell_legion_sha_alpha_wolf() : SpellScriptLoader("spell_legion_sha_alpha_wolf") { }

        class spell_legion_sha_alpha_wolf_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_alpha_wolf_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_SPIRIT_BOMB,
                    SPELL_SHAMAN_FIRE_NOVA,
                    SPELL_SHAMAN_SNOWSTORM,
                    SPELL_SHAMAN_THUNDER_BITE
                });
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (Creature* wolf = GetTarget()->ToCreature())
                {
                    if (!wolf->IsAIEnabled)
                        return;

                    switch (wolf->AI()->GetData(0))
                    {
                        case WOLF_FIRE_FORM:
                            wolf->CastSpell(wolf, SPELL_SHAMAN_FIRE_NOVA, true);
                            break;
                        case WOLF_ICE_FORM:
                            wolf->CastSpell(wolf, SPELL_SHAMAN_SNOWSTORM, true);
                            break;
                        case WOLF_LIGHTNING_FORM:
                            wolf->CastSpell(wolf, SPELL_SHAMAN_THUNDER_BITE, true);
                            break;
                        case WOLF_NORMAL_FORM:
                            wolf->CastSpell(wolf, SPELL_SHAMAN_SPIRIT_BOMB, true);
                            break;
                        default:
                            break;
                    }
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_alpha_wolf_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_alpha_wolf_AuraScript();
        }
};

// 108281 - Ancestral Guidance
class spell_legion_sha_ancestral_guidance : public SpellScriptLoader
{
    public:
        spell_legion_sha_ancestral_guidance() : SpellScriptLoader("spell_legion_sha_ancestral_guidance") { }

        class spell_legion_sha_ancestral_guidance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_ancestral_guidance_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_ANCESTRAL_GUIDANCE_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetHealInfo() && eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_ANCESTRAL_GUIDANCE_HEAL)
                    return false;

                if (!eventInfo.GetHealInfo() && !eventInfo.GetDamageInfo())
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                int32 bp0 = CalculatePct(int32(eventInfo.GetDamageInfo() ? eventInfo.GetDamageInfo()->GetDamage() : eventInfo.GetHealInfo()->GetHeal()), aurEff->GetAmount());
                if (bp0)
                    eventInfo.GetActor()->CastCustomSpell(SPELL_SHAMAN_ANCESTRAL_GUIDANCE_HEAL, SPELLVALUE_BASE_POINT0, bp0, eventInfo.GetActor(), true, NULL, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_ancestral_guidance_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_ancestral_guidance_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_ancestral_guidance_AuraScript();
        }
};

// 114911 - Ancestral Guidance Heal
class spell_legion_sha_ancestral_guidance_heal : public SpellScriptLoader
{
    public:
        spell_legion_sha_ancestral_guidance_heal() : SpellScriptLoader("spell_legion_sha_ancestral_guidance_heal") { }

        class spell_legion_sha_ancestral_guidance_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_ancestral_guidance_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_ANCESTRAL_GUIDANCE
                });
            }

            void ResizeTargets(std::list<WorldObject*>& targets)
            {
                Trinity::Containers::RandomResize(targets, 3);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_ancestral_guidance_heal_SpellScript::ResizeTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_ancestral_guidance_heal_SpellScript();
        }
};

class TotemicRevivalEvent : public BasicEvent
{
    public:
        TotemicRevivalEvent(Unit* owner) : _owner(owner) { }

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            _owner->CastSpell(_owner, SPELL_SHAMAN_TOTEMIC_REVIVAL, true);
            return true;
        }
    private:
        Unit* _owner;
};

// 207498 - Ancestral Protection (absorb)
class spell_legion_sha_ancestral_protection_absorb : public SpellScriptLoader
{
    public:
        spell_legion_sha_ancestral_protection_absorb() : SpellScriptLoader("spell_legion_sha_ancestral_protection_absorb") { }

        class spell_legion_sha_ancestral_protection_absorb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_ancestral_protection_absorb_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_TOTEMIC_REVIVAL
                });
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                absorbAmount = 0;
                uint32 damage = dmgInfo.GetDamage();
                Unit* owner = GetUnitOwner();

                if (owner->GetHealth() > damage)
                    return;

                if (damage > owner->GetMaxHealth() * 2)
                    return;

                owner->m_Events.AddEvent(new TotemicRevivalEvent(owner), owner->m_Events.CalculateTime(2 * IN_MILLISECONDS));

                if (Totem* totem = GetCaster()->ToTotem())
                    totem->UnSummon();
            }

            void Register() override
            {
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_sha_ancestral_protection_absorb_AuraScript::Absorb, EFFECT_1);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_ancestral_protection_absorb_AuraScript();
        }
};

// 2825 - Bloodlust
class spell_legion_sha_bloodlust : public SpellScriptLoader
{
    public:
        spell_legion_sha_bloodlust() : SpellScriptLoader("spell_legion_sha_bloodlust") { }

        class spell_legion_sha_bloodlust_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_bloodlust_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_SATED,
                    SPELL_HUNTER_INSANITY,
                    SPELL_MAGE_TEMPORAL_DISPLACEMENT,
                    SPELL_PET_NETHERWINDS_FATIGUED,
                    SPELL_SHAMAN_SENSE_OF_URGENCY,
                    SPELL_SHAMAN_SENSE_OF_URGENCY_BUFF
                });
            }

            void RemoveInvalidTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if([](WorldObject* target)
                {
                    Unit* unit = target->ToUnit();
                    return unit && (unit->HasAura(SPELL_SHAMAN_SATED)
                        || unit->HasAura(SPELL_HUNTER_INSANITY)
                        || unit->HasAura(SPELL_MAGE_TEMPORAL_DISPLACEMENT));
                });
            }

            void ApplyDebuff()
            {
                if (Unit* target = GetHitUnit())
                    target->CastSpell(target, SPELL_SHAMAN_SATED, true);
            }

            void HandleSenseOfEmergency()
            {
                if (GetCaster()->HasAura(SPELL_SHAMAN_SENSE_OF_URGENCY))
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_SENSE_OF_URGENCY_BUFF, true);
            }


            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_bloodlust_SpellScript::RemoveInvalidTargets, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_bloodlust_SpellScript::RemoveInvalidTargets, EFFECT_1, TARGET_UNIT_CASTER_AREA_RAID);
                AfterHit += SpellHitFn(spell_legion_sha_bloodlust_SpellScript::ApplyDebuff);
                OnCast += SpellCastFn(spell_legion_sha_bloodlust_SpellScript::HandleSenseOfEmergency);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_bloodlust_SpellScript();
        }
};

// 207354 - Caress of the Tidemother
class spell_legion_sha_caress_of_the_tidemother : public SpellScriptLoader
{
    public:
        spell_legion_sha_caress_of_the_tidemother() : SpellScriptLoader("spell_legion_sha_caress_of_the_tidemother") { }

        class spell_legion_sha_caress_of_the_tidemother_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_caress_of_the_tidemother_AuraScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_CARESS_OF_THE_TIDEMOTHER_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_HEALING_STREAM_TOTEM;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Aura* caressOfTheTidemotherTalent = GetTarget()->GetAuraOfRankedSpell(SPELL_SHAMAN_CARESS_OF_THE_TIDEMOTHER_TALENT))
                {
                    int32 bp = caressOfTheTidemotherTalent->GetEffect(EFFECT_0)->GetAmount();
                    GetTarget()->CastCustomSpell(GetTarget(), SPELL_SHAMAN_CARESS_OF_THE_TIDEMOTHER_HEAL, &bp, NULL, NULL, true);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_caress_of_the_tidemother_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_caress_of_the_tidemother_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_caress_of_the_tidemother_AuraScript();
        }
};

// 1064 - Chain Heal
class spell_legion_sha_chain_heal : public SpellScriptLoader
{
    public:
        spell_legion_sha_chain_heal() : SpellScriptLoader("spell_legion_sha_chain_heal") { }

        class spell_legion_sha_chain_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_chain_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_DELUGE_AURA
                });
            }

            void HandleDeluge(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect const* deluge = GetHitUnit()->GetAuraEffect(SPELL_SHAMAN_DELUGE_AURA, EFFECT_0, GetCaster()->GetGUID()))
                    SetHitHeal(GetHitHeal() + CalculatePct(GetHitHeal(), deluge->GetAmount()));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_chain_heal_SpellScript::HandleDeluge, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_chain_heal_SpellScript();
        }
};

// 188443 - Chain lightning
class spell_legion_sha_chain_lightning : public SpellScriptLoader
{
    public:
        spell_legion_sha_chain_lightning() : SpellScriptLoader("spell_legion_sha_chain_lightning") { }

        class spell_legion_sha_chain_lightning_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_chain_lightning_SpellScript);

            bool staticOverload = false;

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_CHAIN_LIGHTNING_ENERGIZE,
                    SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD,
                    SPELL_SHAMAN_STATIC_OVERLOAD
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_CHAIN_LIGHTNING_ENERGIZE, true);

                if (staticOverload)
                    if (Unit* target = GetHitUnit())
                        GetCaster()->CastSpell(target, SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD, true);
            }

            void HandleBeforeCast()
            {
                if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_SHAMAN_STATIC_OVERLOAD, EFFECT_0))
                    if (roll_chance_i(aurEff->GetAmount()))
                        staticOverload = true;
            }

            void Register() override
            {
                BeforeCast += SpellCastFn(spell_legion_sha_chain_lightning_SpellScript::HandleBeforeCast);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_chain_lightning_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_chain_lightning_SpellScript();
        }
};

// 45297 - Chain lightning overload
class spell_legion_sha_chain_lightning_overload : public SpellScriptLoader
{
    public:
        spell_legion_sha_chain_lightning_overload() : SpellScriptLoader("spell_legion_sha_chain_lightning_overload") { }

        class spell_legion_sha_chain_lightning_overload_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_chain_lightning_overload_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD_ENERGIZE
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_chain_lightning_overload_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_chain_lightning_overload_SpellScript();
        }
};

// 157504 - Cloudburst Totem
class spell_legion_sha_cloudburst_totem : public SpellScriptLoader
{
    public:
        spell_legion_sha_cloudburst_totem() : SpellScriptLoader("spell_legion_sha_cloudburst_totem") { }

        class spell_legion_sha_cloudburst_totem_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_cloudburst_totem_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_CLOUDBURST_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetHealInfo() != nullptr;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                SpellEffectInfo const* cloudBurst = sSpellMgr->AssertSpellInfo(SPELL_SHAMAN_CLOUDBURST_HEAL)->GetEffect(EFFECT_1);
                const_cast<AuraEffect*>(aurEff)->ChangeAmount(aurEff->GetAmount() + CalculatePct(eventInfo.GetHealInfo()->GetHeal(), cloudBurst->CalcValue()));
            }

            void OnRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (int32 amount = aurEff->GetAmount())
                {
                    GetUnitOwner()->CastCustomSpell(GetUnitOwner(), SPELL_SHAMAN_CLOUDBURST_HEAL, &amount, NULL, NULL, true);
                    const_cast<AuraEffect*>(aurEff)->ChangeAmount(0);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_cloudburst_totem_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_cloudburst_totem_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_sha_cloudburst_totem_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_cloudburst_totem_AuraScript();
        }
};

/// 157503 - Cloudburst
class spell_legion_sha_cloudburst_totem_heal : public SpellScriptLoader
{
    public:
        spell_legion_sha_cloudburst_totem_heal() : SpellScriptLoader("spell_legion_sha_cloudburst_totem_heal") { }

        class spell_legion_sha_cloudburst_totem_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_cloudburst_totem_heal_SpellScript);

            void CountTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = uint32(targets.size());
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (_targetCount > 0)
                    SetHitHeal(GetHitHeal() / _targetCount);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_cloudburst_totem_heal_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_cloudburst_totem_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        private:
            uint32 _targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_cloudburst_totem_heal_SpellScript();
        }
};

// 201764 - Recall Cloudburst Totem
class spell_legion_sha_cloudburst_totem_recall : public SpellScriptLoader
{
    public:
        spell_legion_sha_cloudburst_totem_recall() : SpellScriptLoader("spell_legion_sha_cloudburst_totem_recall") { }

        class spell_legion_sha_cloudburst_totem_recall_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_cloudburst_totem_recall_SpellScript);

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->GetAura(SPELL_SHAMAN_CLOUDBURST_TOTEM))
                    GetCaster()->RemoveAurasDueToSpell(SPELL_SHAMAN_CLOUDBURST_TOTEM);

                if (Unit* totem = GetCaster()->GetControlledCreature(NPC_CLOUDBURST_TOTEM))
                    totem->ToTotem()->UnSummon();
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_cloudburst_totem_recall_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_cloudburst_totem_recall_SpellScript();
        }
};

// 208997 - Counterstrike Totem Debuff
class spell_legion_sha_counterstrike_totem : public SpellScriptLoader
{
    public:
        spell_legion_sha_counterstrike_totem() : SpellScriptLoader("spell_legion_sha_counterstrike_totem") { }

        class spell_legion_sha_counterstrike_totem_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_counterstrike_totem_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_COUNTERSTRIKE_DAMAGE
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                int32 bp0 = CalculatePct(int32(eventInfo.GetDamageInfo()->GetDamage()), aurEff->GetAmount());
                if (bp0)
                    if (Unit* caster = GetCaster())
                        caster->CastCustomSpell(SPELL_SHAMAN_COUNTERSTRIKE_DAMAGE, SPELLVALUE_BASE_POINT0, bp0, GetTarget(), true, NULL, aurEff, caster->GetGUID());
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage() > 0;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_counterstrike_totem_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_counterstrike_totem_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_counterstrike_totem_AuraScript();
        }
};

// 187874 - Crash Lightning
class spell_legion_sha_crash_lightning : public SpellScriptLoader
{
    public:
        spell_legion_sha_crash_lightning() : SpellScriptLoader("spell_legion_sha_crash_lightning") { }

        class spell_legion_sha_crash_lightning_SpellScript: public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_crash_lightning_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_CRASH_LIGHTNING_BUFF,
                    SPELL_SHAMAN_CRASHING_STORM_TALENT,
                    SPELL_SHAMAN_CRASHING_STORM_AREATRIGGER,
                    SPELL_SHAMAN_GATHERING_STORMS,
                    SPELL_SHAMAN_GATHERING_STORMS_BUFF
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _targetsCount = int32(targets.size());
            }

            void HandleCrashingStorm()
            {
                if (GetCaster()->HasAura(SPELL_SHAMAN_CRASHING_STORM_TALENT))
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_CRASHING_STORM_AREATRIGGER, true);
            }

            void HandleAfterCast()
            {
                if (_targetsCount == 0)
                    return;

                if (_targetsCount >= 2)
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_CRASH_LIGHTNING_BUFF, true);

                if (AuraEffect* getheringStorms = GetCaster()->GetAuraEffect(SPELL_SHAMAN_GATHERING_STORMS, EFFECT_0))
                    GetCaster()->CastCustomSpell(SPELL_SHAMAN_GATHERING_STORMS_BUFF, SPELLVALUE_BASE_POINT0, getheringStorms->GetAmount() * _targetsCount, GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_crash_lightning_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_CONE_ENEMY_104);
                OnCast += SpellCastFn(spell_legion_sha_crash_lightning_SpellScript::HandleCrashingStorm);
                AfterCast += SpellCastFn(spell_legion_sha_crash_lightning_SpellScript::HandleAfterCast);
            }

        private:
            int32 _targetsCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_crash_lightning_SpellScript();
        }
};

// 187878 - Crash Lightning (Proc)
class spell_legion_sha_crash_lightning_proc : public SpellScriptLoader
{
    public:
        spell_legion_sha_crash_lightning_proc() : SpellScriptLoader("spell_legion_sha_crash_lightning_proc") { }

        class spell_legion_sha_crash_lightning_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_crash_lightning_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LAVA_LASH,
                    SPELL_SHAMAN_STORMSTRIKE
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                // Prevent double proc from StormStrike (2 spells with same flags)
                if (procInfo.GetSpellInfo() && (procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_STORMSTRIKE || procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_LAVA_LASH))
                    return true;
                return false;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_crash_lightning_proc_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_crash_lightning_proc_AuraScript();
        }
};

class AreatriggerReshapeEvent : public BasicEvent
{
    public:
        AreatriggerReshapeEvent(Unit* caster, uint32 areatriggerId, Position destination) : _caster(caster), _areatriggerId(areatriggerId), _destination(destination) { }

        bool Execute(uint64 /*execTime*/, uint32 /*diff*/) override
        {
            std::vector<AreaTrigger*> triggers = _caster->GetAreaTriggers(_areatriggerId);
            if (!triggers.empty())
            {
                if (AreaTrigger* trigger = triggers.back())
                {
                    Movement::PointsArray reshapePoints;
                    for (uint8 i = 0; i < 4; i++)
                    {
                        G3D::Vector3 point;
                        point.x = (i < 2) ? trigger->GetPositionX() : _destination.GetPositionX();
                        point.y = (i < 2) ? trigger->GetPositionY() : _destination.GetPositionY();
                        point.z = (i < 2) ? trigger->GetPositionZ() : _destination.GetPositionZ();
                        reshapePoints.push_back(point);
                    }
                    trigger->InitSplines(reshapePoints, uint32(trigger->GetDistance(_destination) / 2.735f) * IN_MILLISECONDS);
                }
            }
            return true;
        }

    private:
        Unit* _caster;
        uint32 _areatriggerId;
        Position _destination;
};

// 199107 - Doom Vortex
class spell_legion_sha_doom_vortex : public SpellScriptLoader
{
    public:
        spell_legion_sha_doom_vortex() : SpellScriptLoader("spell_legion_sha_doom_vortex") { }

        class spell_legion_sha_doom_vortex_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_doom_vortex_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_LIGHTNING_BOLT_ENHANCEMENT)
                    if (!GetTarget()->HasAura(SPELL_SHAMAN_OVERCHARGE))
                        return false;

                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void HandleAfterProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->m_Events.AddEvent(new AreatriggerReshapeEvent(GetTarget(), GetSpellInfo()->GetEffect(EFFECT_0)->TriggerSpell, eventInfo.GetActionTarget()->GetPosition()), GetTarget()->m_Events.CalculateTime(100));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_doom_vortex_AuraScript::CheckProc);
                AfterEffectProc += AuraEffectProcFn(spell_legion_sha_doom_vortex_AuraScript::HandleAfterProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_doom_vortex_AuraScript();
        }
};

// 204288 - Earth Shield
class spell_legion_sha_earth_shield : public SpellScriptLoader
{
    public:
        spell_legion_sha_earth_shield() : SpellScriptLoader("spell_legion_sha_earth_shield") { }

        class spell_legion_sha_earth_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_earth_shield_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_1)->IsAura(SPELL_AURA_DUMMY))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_EARTH_SHIELD_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo() || eventInfo.GetDamageInfo()->GetDamage() < uint32(GetTarget()->CountPctFromMaxHealth(GetEffect(EFFECT_1)->GetAmount())))
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_EARTH_SHIELD_HEAL, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_earth_shield_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_earth_shield_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_earth_shield_AuraScript();
        }
};

// 201633 - Earthen Shield
class spell_legion_sha_earthen_shield : public SpellScriptLoader
{
    public:
        spell_legion_sha_earthen_shield() : SpellScriptLoader("spell_legion_sha_earthen_shield") { }

        class spell_legion_sha_earthen_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_earthen_shield_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_EARTHEN_SHIELD_DAMAGE
                });
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool & /*canBeRecalculated*/)
            {
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
                amount = -1;
            }

            void HandleAbsorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                absorbAmount = std::min(dmgInfo.GetDamage(), uint32(aurEff->GetDamage()));
                if (Unit* caster = GetCaster())
                    GetTarget()->CastCustomSpell(SPELL_SHAMAN_EARTHEN_SHIELD_DAMAGE, SPELLVALUE_BASE_POINT0, absorbAmount, caster, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_sha_earthen_shield_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_sha_earthen_shield_AuraScript::HandleAbsorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_earthen_shield_AuraScript();
        }
};

// 8042 - Earth Shock
class spell_legion_sha_earth_shock : public SpellScriptLoader
{
    public:
        spell_legion_sha_earth_shock() : SpellScriptLoader("spell_legion_sha_earth_shock") { }

        class spell_legion_sha_earth_shock_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_earth_shock_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_EARTHFURY,
                    SPELL_SHAMAN_EARTHFURY_STUN
                });
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                // http://www.askmrrobot.com/wow/theory/mechanic/spell/earthshock?spec=ShamanElemental&version=7_0_3_21996
                int32 powerSpent = std::min(100, GetSpell()->GetPowerCost(POWER_MAELSTROM)); // 10 - 100
                int32 damage = CalculatePct(GetHitDamage(), powerSpent);
                SetHitDamage(damage);

                if (GetSpell()->GetPowerCost(POWER_MAELSTROM) >= 100 && GetCaster()->HasAura(SPELL_SHAMAN_EARTHFURY))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_EARTHFURY_STUN, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_earth_shock_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_earth_shock_SpellScript();
        }
};

// 170374 - Earthen Rage (Passive)
class spell_legion_sha_earthen_rage_passive : public SpellScriptLoader
{
    public:
        static char constexpr const ScriptName[] = "spell_legion_sha_earthen_rage_passive";
        spell_legion_sha_earthen_rage_passive() : SpellScriptLoader(ScriptName) { }

        class spell_legion_sha_earthen_rage_passive_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_earthen_rage_passive_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_SHAMAN_EARTHEN_RAGE_PERIODIC, SPELL_SHAMAN_EARTHEN_RAGE_DAMAGE });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                _procTargetGuid = eventInfo.GetProcTarget()->GetGUID();
                eventInfo.GetActor()->CastSpell(eventInfo.GetActor(), SPELL_SHAMAN_EARTHEN_RAGE_PERIODIC, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_earthen_rage_passive_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }

            ObjectGuid _procTargetGuid;

        public:
            ObjectGuid const& GetProcTargetGuid() const
            {
                return _procTargetGuid;
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_earthen_rage_passive_AuraScript();
        }
};

char constexpr const spell_legion_sha_earthen_rage_passive::ScriptName[];
// 170377 - Earthen Rage (Proc Aura)
class spell_legion_sha_earthen_rage_proc_aura : public SpellScriptLoader
{
    public:
        spell_legion_sha_earthen_rage_proc_aura() : SpellScriptLoader("spell_legion_sha_earthen_rage_proc_aura") { }
        using earthen_rage_script = spell_legion_sha_earthen_rage_passive::spell_legion_sha_earthen_rage_passive_AuraScript;

        class spell_legion_sha_earthen_rage_proc_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_earthen_rage_proc_aura_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_SHAMAN_EARTHEN_RAGE_PASSIVE, SPELL_SHAMAN_EARTHEN_RAGE_DAMAGE });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                Unit* owner = GetUnitOwner();
                if (Aura* aura = owner->GetAura(SPELL_SHAMAN_EARTHEN_RAGE_PASSIVE))
                    if (earthen_rage_script* script = aura->GetScript<earthen_rage_script>(spell_legion_sha_earthen_rage_passive::ScriptName))
                        if (Unit* procTarget = ObjectAccessor::GetUnit(*owner, script->GetProcTargetGuid()))
                            owner->CastSpell(procTarget, SPELL_SHAMAN_EARTHEN_RAGE_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_earthen_rage_proc_aura_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_earthen_rage_proc_aura_AuraScript();
        }
};

// 64695 - Earthgrab
class spell_legion_sha_earthgrab : public SpellScriptLoader
{
    public:
        spell_legion_sha_earthgrab() : SpellScriptLoader("spell_legion_sha_earthgrab") { }

        class spell_legion_sha_earthgrab_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_earthgrab_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_EARTHGRAB_IMMUNITY,
                    SPELL_SHAMAN_EARTHBIND_FOR_EARTHGRAB_TOTEM
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_EARTHGRAB_IMMUNITY, true);
            }

            void FilterTargets(std::list<WorldObject*>& unitList)
            {
                for (auto it = unitList.begin(); it != unitList.end();)
                {
                    Unit* target = (*it)->ToUnit();
                    if (target->HasAura(SPELL_SHAMAN_EARTHGRAB_IMMUNITY))
                    {
                        if (!target->HasAura(GetSpellInfo()->Id, GetCaster()->GetGUID()))
                            GetCaster()->CastSpell(target, SPELL_SHAMAN_EARTHBIND_FOR_EARTHGRAB_TOTEM, true);

                        it = unitList.erase(it);
                    }
                    else
                        ++it;
                }
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_legion_sha_earthgrab_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_earthgrab_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_earthgrab_SpellScript();
        }
};

// 77478 - Earthquake
class spell_legion_sha_earthquake : public SpellScriptLoader
{
    public:
        spell_legion_sha_earthquake() : SpellScriptLoader("spell_legion_sha_earthquake") { }

        class spell_legion_sha_earthquake_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_earthquake_SpellScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_EARTHQUAKE_KNOCKDOWN,
                    SPELL_SHAMAN_THE_GROUND_TREMBLES
                });
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                int32 sp = int32(GetCaster()->SpellBaseDamageBonusDone(GetSpellInfo()->GetSchoolMask()) * 0.3f * 10.0f);
                if (Aura* theGroundTrembles = GetCaster()->GetAuraOfRankedSpell(SPELL_SHAMAN_THE_GROUND_TREMBLES))
                {
                    int32 bp = theGroundTrembles->GetEffect(EFFECT_0)->GetAmount();
                    AddPct(bp, sp);
                }
                SetHitDamage(GetHitDamage() * sp);
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                int32 chance = 0;
                if (SpellEffectInfo const* effInfo1 = GetSpellInfo()->GetEffect(EFFECT_1))
                    chance = effInfo1->CalcValue(GetCaster());

                if (roll_chance_i(chance))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_EARTHQUAKE_KNOCKDOWN, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_earthquake_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_earthquake_SpellScript::HandleScriptEffect, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_earthquake_SpellScript();
        }
};

// 117014 - Elemental Blast
// 120588 - Elemental Blast (Overload)
class spell_legion_sha_elemental_blast : public SpellScriptLoader
{
    public:
        spell_legion_sha_elemental_blast() : SpellScriptLoader("spell_legion_sha_elemental_blast") { }

        class spell_legion_sha_elemental_blast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_elemental_blast_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_ELEMENTAL_BLAST_CRIT,
                    SPELL_SHAMAN_ELEMENTAL_BLAST_HASTE,
                    SPELL_SHAMAN_ELEMENTAL_BLAST_MASTERY
                });
            }

            void TriggerBuff()
            {
                std::list<uint32> buffs = { SPELL_SHAMAN_ELEMENTAL_BLAST_CRIT, SPELL_SHAMAN_ELEMENTAL_BLAST_HASTE, SPELL_SHAMAN_ELEMENTAL_BLAST_MASTERY };
                if (GetSpellInfo()->Id == SPELL_SHAMAN_ELEMENTAL_BLAST_OVERLOAD)
                {
                    buffs.remove_if([this](uint32 spellId) { return GetCaster()->HasAura(spellId); });
                    if (buffs.empty())
                        return;
                }
                uint32 spellId = Trinity::Containers::SelectRandomContainerElement(buffs);
                GetCaster()->CastSpell(GetCaster(), spellId, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_sha_elemental_blast_SpellScript::TriggerBuff);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_elemental_blast_SpellScript();
        }
};

// 198248 - Elemental Healing
class spell_legion_sha_elemental_healing : public SpellScriptLoader
{
    public:
        spell_legion_sha_elemental_healing() : SpellScriptLoader("spell_legion_sha_elemental_healing") { }

        class spell_legion_sha_elemental_healing_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_elemental_healing_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_ELEMENTAL_HEALING_HEAL
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastCustomSpell(SPELL_SHAMAN_ELEMENTAL_HEALING_HEAL, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_elemental_healing_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_elemental_healing_AuraScript();
        }
};

class RemoveWolfFormEvent : public BasicEvent
{
    public:
        RemoveWolfFormEvent(Unit* caster) : _caster(caster) { }

        bool Execute(uint64 /*execTime*/, uint32 /*diff*/) override
        {
            _caster->RemoveAurasDueToSpell(SPELL_SHAMAN_WOLF_FORM);
            return true;
        }

    private:
        Unit* _caster;
};

// 196884 - Feral Lunge
class spell_legion_sha_feral_lunge : public SpellScriptLoader
{
    public:
        spell_legion_sha_feral_lunge() : SpellScriptLoader("spell_legion_sha_feral_lunge") { }

        class spell_legion_sha_feral_lunge_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_feral_lunge_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_WOLF_FORM,
                    SPELL_SHAMAN_FERAL_LUNGE_DAMAGE
                });
            }

            void HandleOnHit()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_WOLF_FORM, true);
                GetCaster()->m_Events.AddEvent(new RemoveWolfFormEvent(GetCaster()), GetCaster()->m_Events.CalculateTime(500));
            }

            void AfterJump(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetExplTargetUnit(), SPELL_SHAMAN_FERAL_LUNGE_DAMAGE, true);
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_legion_sha_feral_lunge_SpellScript::HandleOnHit);
                OnEffectHit += SpellEffectFn(spell_legion_sha_feral_lunge_SpellScript::AfterJump, EFFECT_1, SPELL_EFFECT_JUMP_DEST);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_feral_lunge_SpellScript();
        }
};

// 51533 - Feral Spirit
class spell_legion_sha_feral_spirit : public SpellScriptLoader
{
    public:
        spell_legion_sha_feral_spirit() : SpellScriptLoader("spell_legion_sha_feral_spirit") { }

        class spell_legion_sha_feral_spirit_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_feral_spirit_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_FERAL_SPIRIT_ENHANCEMENT_SUMMON,
                    SPELL_SHAMAN_DOOM_WOLVES
                });
            }

            void HandleOnHit()
            {
                if (AuraEffect* doomWolves = GetCaster()->GetAuraEffect(SPELL_SHAMAN_DOOM_WOLVES, EFFECT_0))
                    GetCaster()->CastSpell(GetCaster()->GetPositionX(), GetCaster()->GetPositionY(), GetCaster()->GetPositionZ(), doomWolves->GetAmount(), true);
                else
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_FERAL_SPIRIT_ENHANCEMENT_SUMMON, true);
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_legion_sha_feral_spirit_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_feral_spirit_SpellScript();
        }
};

// 188389 - Flame Shock
class spell_legion_sha_flame_shock : public SpellScriptLoader
{
    public:
        spell_legion_sha_flame_shock() : SpellScriptLoader("spell_legion_sha_flame_shock") { }

        class spell_legion_sha_flame_shock_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_flame_shock_SpellScript);

            void HandleDuration(SpellEffIndex /*effIndex*/)
            {
                Aura* aura = GetHitAura();
                int32 flameshockDuration = GetSpellInfo()->GetDuration();

                // http://www.askmrrobot.com/wow/theory/mechanic/spell/flameshock?spec=ShamanElemental&version=7_0_3_22900
                if (GetSpell()->GetPowerCost(POWER_MAELSTROM) != 0)
                    aura->SetDuration(flameshockDuration * (1 + GetSpell()->GetPowerCost(POWER_MAELSTROM) / 20));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_flame_shock_SpellScript::HandleDuration, EFFECT_1, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_flame_shock_SpellScript();
        }
};

// 194084 - Flametongue
class spell_legion_sha_flametongue : public SpellScriptLoader
{
    public:
        spell_legion_sha_flametongue() : SpellScriptLoader("spell_legion_sha_flametongue") { }

        class spell_legion_sha_flametongue_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_flametongue_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_FLAMETONGUE_ATTACK
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* attacker = eventInfo.GetActor();
                int32 damage = int32(attacker->GetTotalAttackPowerValue(BASE_ATTACK) * 0.125f / 2600 * attacker->GetBaseAttackTime(BASE_ATTACK));
                attacker->CastCustomSpell(SPELL_SHAMAN_FLAMETONGUE_ATTACK, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetActionTarget(), TRIGGERED_FULL_MASK, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_flametongue_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_flametongue_AuraScript();
        }
};

// 204350 - Forked Lightning
class spell_legion_sha_forked_lightning : public SpellScriptLoader
{
    public:
        spell_legion_sha_forked_lightning() : SpellScriptLoader("spell_legion_sha_forked_lightning") { }

        class spell_legion_sha_forked_lightning_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_forked_lightning_SpellScript);

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.sort(Trinity::ObjectDistanceOrderPred(GetCaster(), false));

                if (targets.size() > 2)
                    targets.resize(2);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_forked_lightning_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_forked_lightning_SpellScript();
        }
};

// 196840 - Frost Shock
class spell_legion_sha_frost_shock : public SpellScriptLoader
{
    public:
        spell_legion_sha_frost_shock() : SpellScriptLoader("spell_legion_sha_frost_shock") { }

        class spell_legion_sha_frost_shock_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_frost_shock_SpellScript);

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetSpell()->GetPowerCost(POWER_MAELSTROM) != 0)
                {
                    int32 damage = GetHitDamage() * (1 + GetSpell()->GetPowerCost(POWER_MAELSTROM) / 20);
                    SetHitDamage(damage);
                }
            }

            void HandleDuration(SpellEffIndex /*effIndex*/)
            {
                Aura* aura = GetHitAura();
                int32 frostshockDuration = GetSpellInfo()->GetDuration();

                //http://www.askmrrobot.com/wow/theory/mechanic/spell/frostshock?spec=ShamanElemental&version=7_0_3_22900
                if (GetSpell()->GetPowerCost(POWER_MAELSTROM) != 0)
                    aura->SetDuration(frostshockDuration * (1 + GetSpell()->GetPowerCost(POWER_MAELSTROM) / 20));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_frost_shock_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_frost_shock_SpellScript::HandleDuration, EFFECT_1, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_frost_shock_SpellScript();
        }
};

// 196834 - Frostbrand
class spell_legion_sha_frostbrand : public SpellScriptLoader
{
    public:
        spell_legion_sha_frostbrand() : SpellScriptLoader("spell_legion_sha_frostbrand") { }

        class spell_legion_sha_frostbrand_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_frostbrand_AuraScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_HAILSTORM,
                    SPELL_SHAMAN_HAILSTORM_DAMAGE
                });
            }

            void HandleProc(ProcEventInfo& procInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (caster->HasAura(SPELL_SHAMAN_HAILSTORM))
                    caster->CastSpell(procInfo.GetActionTarget(), SPELL_SHAMAN_HAILSTORM_DAMAGE, true);
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_legion_sha_frostbrand_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_frostbrand_AuraScript();
        }
};

// 197211 - Fury of Air
class spell_legion_sha_fury_of_air : public SpellScriptLoader
{
    public:
        spell_legion_sha_fury_of_air() : SpellScriptLoader("spell_legion_sha_fury_of_air") { }

        class spell_legion_sha_fury_of_air_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_fury_of_air_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_FURY_OF_AIR
                });
            }

            void HandlePeriodicMaelstrom(AuraEffect const* /*aurEff*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                caster->SetPower(POWER_MAELSTROM, caster->GetPower(POWER_MAELSTROM) - 3);

                if (caster->GetPower(POWER_MAELSTROM) < 3)
                    caster->RemoveAura(SPELL_SHAMAN_FURY_OF_AIR);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_fury_of_air_AuraScript::HandlePeriodicMaelstrom, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_fury_of_air_AuraScript();
        }
};

// 191717 - Fury of the Storms
class spell_legion_sha_fury_of_the_storms : public SpellScriptLoader
{
    public:
        spell_legion_sha_fury_of_the_storms() : SpellScriptLoader("spell_legion_sha_fury_of_the_storms") { }

        class spell_legion_sha_fury_of_the_storms_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_fury_of_the_storms_AuraScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_SUMMON_LIGHTNING_ELEMENTAL
                });
            }

            Creature* GetLightningElemental()
            {
                std::set<Unit*> controlled = GetTarget()->m_Controlled;
                for (Unit* minion : controlled)
                    if (minion->GetEntry() == ENTRY_GREATER_LIGHTNING_ELEMENTAL)
                        return minion->ToCreature();

                return nullptr;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                switch (eventInfo.GetSpellInfo()->Id)
                {
                    case SPELL_SHAMAN_STORMKEEPER:
                        GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_SUMMON_LIGHTNING_ELEMENTAL, true);
                        break;
                    case SPELL_SHAMAN_LIGHTNING_BOLT:
                        if (Creature* lightningElemental = GetLightningElemental())
                            lightningElemental->AI()->DoAction(ACTION_LIGHTNING_BOLT);
                        break;
                    case SPELL_SHAMAN_CHAIN_LIGHTNING:
                        if (Creature* lightningElemental = GetLightningElemental())
                            lightningElemental->AI()->DoAction(ACTION_CHAIN_LIGHTNING);
                        break;
                    default:
                        break;
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_fury_of_the_storms_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_fury_of_the_storms_AuraScript();
        }
};

// 207524 - Ghost in the Mist
class spell_legion_sha_ghost_in_the_mist : public SpellScriptLoader
{
    public:
        spell_legion_sha_ghost_in_the_mist() : SpellScriptLoader("spell_legion_sha_ghost_in_the_mist") { }

        class spell_legion_sha_ghost_in_the_mist_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_ghost_in_the_mist_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (spellInfo->GetEffect(EFFECT_0) && spellInfo->GetEffect(EFFECT_0)->TriggerSpell == 0)
                    return false;
                return true;
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                GetTarget()->CastSpell(GetTarget(), aurEff->GetSpellEffectInfo()->TriggerSpell, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_ghost_in_the_mist_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_ghost_in_the_mist_AuraScript();
        }
};

// 2645 - Ghost Wolf
class spell_legion_sha_ghost_wolf : public SpellScriptLoader
{
    public:
        spell_legion_sha_ghost_wolf() : SpellScriptLoader("spell_legion_sha_ghost_wolf") { }

        class spell_legion_sha_ghost_wolf_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_ghost_wolf_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_SPECTRAL_RECOVERY,
                    SPELL_SHAMAN_SPECTRAL_RECOVERY_HEAL,
                    SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_TRAIT,
                    SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_PERIODIC
                });
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_SHAMAN_SPECTRAL_RECOVERY))
                    GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_SPECTRAL_RECOVERY_HEAL, true);
                if (GetTarget()->HasAura(SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_TRAIT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_PERIODIC, true);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_SHAMAN_SPECTRAL_RECOVERY_HEAL);
                GetTarget()->RemoveAurasDueToSpell(SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_PERIODIC);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_legion_sha_ghost_wolf_AuraScript::OnApply, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_sha_ghost_wolf_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_ghost_wolf_AuraScript();
        }
};

// 73920 - Healing Rain
class spell_legion_sha_healing_rain : public SpellScriptLoader
{
    public:
        spell_legion_sha_healing_rain() : SpellScriptLoader("spell_legion_sha_healing_rain") { }

        class spell_legion_sha_healing_rain_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_healing_rain_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_HEALING_RAIN,
                    SPELL_SHAMAN_HEALING_RAIN_TICK,
                    SPELL_SHAMAN_DELUGE_AURA,
                    SPELL_SHAMAN_DELUGE_TALENT
                });
            }

            void HandleOnHit()
            {
                if (WorldLocation const* loc = GetExplTargetDest())
                {
                    Unit* caster = GetCaster();
                    caster->RemoveDynObject(SPELL_SHAMAN_HEALING_RAIN);
                    caster->CastSpell(loc->GetPositionX(), loc->GetPositionY(), loc->GetPositionZ(), SPELL_SHAMAN_HEALING_RAIN, true);
                }
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_legion_sha_healing_rain_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_healing_rain_SpellScript();
        }

        class spell_legion_sha_healing_rain_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_healing_rain_AuraScript);

            void OnTick(AuraEffect const* /*aurEff*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (DynamicObject* dynObj = caster->GetDynObject(SPELL_SHAMAN_HEALING_RAIN))
                {
                    if (caster->HasAura(SPELL_SHAMAN_DELUGE_TALENT))
                        caster->CastSpell(dynObj->GetPositionX(), dynObj->GetPositionY(), dynObj->GetPositionZ(), SPELL_SHAMAN_DELUGE_AURA, true);

                    caster->CastSpell(dynObj->GetPositionX(), dynObj->GetPositionY(), dynObj->GetPositionZ(), SPELL_SHAMAN_HEALING_RAIN_TICK, true);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_healing_rain_AuraScript::OnTick, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_healing_rain_AuraScript();
        }
};

// 52042 - Healing Stream Totem
class spell_legion_sha_healing_stream_totem_heal : public SpellScriptLoader
{
    public:
        spell_legion_sha_healing_stream_totem_heal() : SpellScriptLoader("spell_legion_sha_healing_stream_totem_heal") { }

        class spell_legion_sha_healing_stream_totem_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_healing_stream_totem_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_QUEENS_DECREE,
                    SPELL_SHAMAN_QUEENS_DECREE_HEAL
                });
            }

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit())
                        if (Player* playerTarget = unitTarget->GetCharmerOrOwnerPlayerOrPlayerItself())
                            return !caster->IsInRaidWith(playerTarget);
                    return true;
                });

                if (targets.empty())
                    return;

                targets.sort(Trinity::HealthPctOrderPred());
                targets.resize(GetCaster()->HasAura(SPELL_SHAMAN_RUSHING_STREAMS) ? 2 : 1);
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->GetOwner()->HasAura(SPELL_SHAMAN_QUEENS_DECREE))
                {
                    if (Aura* decreeAura = GetHitUnit()->GetAura(SPELL_SHAMAN_QUEENS_DECREE_HEAL))
                    {
                        if (decreeAura->GetDuration() < 4 * IN_MILLISECONDS)
                            GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_QUEENS_DECREE_HEAL, true);
                    }
                    else
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_QUEENS_DECREE_HEAL, true);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_healing_stream_totem_heal_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_healing_stream_totem_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_healing_stream_totem_heal_SpellScript();
        }
};

// 108280 - Healing Tide Totem
class spell_legion_sha_healing_tide_totem : public SpellScriptLoader
{
    public:
        spell_legion_sha_healing_tide_totem() : SpellScriptLoader("spell_legion_sha_healing_tide_totem") { }

        class spell_legion_sha_healing_tide_totem_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_healing_tide_totem_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_CUMULATIVE_UPKEEP,
                    SPELL_SHAMAN_CUMULATIVE_UPKEEP_BUFF,
                    SPELL_SHAMAN_HEALING_TIDE_TOTEM_HEAL
                });
            }

            void OnPeriodic(AuraEffect const* /*aurEff*/)
            {
                for (Unit::ControlList::iterator itr = GetCaster()->m_Controlled.begin(); itr != GetCaster()->m_Controlled.end(); ++itr)
                    if ((*itr)->GetEntry() == NPC_HEALING_TIDE_TOTEM)
                    {
                        if (GetCaster()->HasAura(SPELL_SHAMAN_CUMULATIVE_UPKEEP))
                            (*itr)->CastSpell((*itr), SPELL_SHAMAN_CUMULATIVE_UPKEEP_BUFF, true);

                        (*itr)->CastSpell((*itr), SPELL_SHAMAN_HEALING_TIDE_TOTEM_HEAL, true);
                    }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_healing_tide_totem_AuraScript::OnPeriodic, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_healing_tide_totem_AuraScript();
        }
};

// 114942 - Healing Tide Totem (Heal)
class spell_legion_sha_healing_tide_totem_heal : public SpellScriptLoader
{
    public:
        spell_legion_sha_healing_tide_totem_heal() : SpellScriptLoader("spell_legion_sha_healing_tide_totem_heal") { }

        class spell_legion_sha_healing_tide_totem_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_healing_tide_totem_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_CUMULATIVE_UPKEEP_BUFF
                });
            }

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit())
                        if (Player* playerTarget = unitTarget->GetCharmerOrOwnerPlayerOrPlayerItself())
                            return !caster->IsInRaidWith(playerTarget);
                    return true;
                });
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* aurEff = GetHitUnit()->GetAuraEffect(SPELL_SHAMAN_CUMULATIVE_UPKEEP_BUFF, EFFECT_0))
                {
                    int32 heal = GetHitHeal();
                    AddPct(heal, aurEff->GetAmount());
                    SetHitHeal(heal);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_healing_tide_totem_heal_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_healing_tide_totem_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_healing_tide_totem_heal_SpellScript();
        }
};

// 32182 - Heroism
class spell_legion_sha_heroism : public SpellScriptLoader
{
    public:
        spell_legion_sha_heroism() : SpellScriptLoader("spell_legion_sha_heroism") { }

        class spell_legion_sha_heroism_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_heroism_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_EXHAUSTION,
                    SPELL_HUNTER_INSANITY,
                    SPELL_MAGE_TEMPORAL_DISPLACEMENT,
                    SPELL_PET_NETHERWINDS_FATIGUED,
                    SPELL_SHAMAN_SENSE_OF_URGENCY,
                    SPELL_SHAMAN_SENSE_OF_URGENCY_BUFF
                });
            }

            void RemoveInvalidTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if([](WorldObject* target)
                {
                    Unit* unit = target->ToUnit();
                    return unit && (unit->HasAura(SPELL_SHAMAN_EXHAUSTION)
                        || unit->HasAura(SPELL_HUNTER_INSANITY)
                        || unit->HasAura(SPELL_MAGE_TEMPORAL_DISPLACEMENT));
                });
            }

            void ApplyDebuff()
            {
                if (Unit* target = GetHitUnit())
                    target->CastSpell(target, SPELL_SHAMAN_EXHAUSTION, true);
            }

            void HandleSenseOfEmergency()
            {
                if (GetCaster()->HasAura(SPELL_SHAMAN_SENSE_OF_URGENCY))
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_SENSE_OF_URGENCY_BUFF, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_heroism_SpellScript::RemoveInvalidTargets, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_heroism_SpellScript::RemoveInvalidTargets, EFFECT_1, TARGET_UNIT_CASTER_AREA_RAID);
                AfterHit += SpellHitFn(spell_legion_sha_heroism_SpellScript::ApplyDebuff);
                OnCast += SpellCastFn(spell_legion_sha_heroism_SpellScript::HandleSenseOfEmergency);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_heroism_SpellScript();
        }
};

// 201900 - Hot Hand
class spell_legion_sha_hot_hand : public SpellScriptLoader
{
    public:
        spell_legion_sha_hot_hand() : SpellScriptLoader("spell_legion_sha_hot_hand") { }

        class spell_legion_sha_hot_hand_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_hot_hand_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_FLAMETONGUE_BUFF
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    return caster->HasAura(SPELL_SHAMAN_FLAMETONGUE_BUFF);
                return false;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_hot_hand_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_hot_hand_AuraScript();
        }
};

// 51505 - Lava burst
class spell_legion_sha_lava_burst : public SpellScriptLoader
{
    public:
        spell_legion_sha_lava_burst() : SpellScriptLoader("spell_legion_sha_lava_burst") { }

        class spell_legion_sha_lava_burst_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_lava_burst_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_PATH_OF_FLAMES_TALENT,
                    SPELL_SHAMAN_PATH_OF_FLAMES_SPREAD
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                // todo hitunit?
                if (Unit* target = GetExplTargetUnit())
                    if (GetCaster()->HasAura(SPELL_SHAMAN_PATH_OF_FLAMES_TALENT))
                        GetCaster()->CastSpell(target, SPELL_SHAMAN_PATH_OF_FLAMES_SPREAD, true);

                if (GetCaster()->HasAura(SPELL_SHAMAN_ASCENDANCE_ELEMENTAL))
                {
                    int32 damage = GetHitDamage();
                    float ascendantBonusDamage = GetCaster()->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1);

                    AddPct(damage, ascendantBonusDamage);
                    SetHitDamage(damage);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_lava_burst_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_lava_burst_SpellScript();
        }
};

// 77756 - Lava Surge
class spell_legion_sha_lava_surge : public SpellScriptLoader
{
    public:
        spell_legion_sha_lava_surge() : SpellScriptLoader("spell_legion_sha_lava_surge") { }

        class spell_legion_sha_lava_surge_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_lava_surge_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LAVA_SURGE_TALENT
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (!GetTarget()->HasAura(SPELL_SHAMAN_CONTROL_OF_LAVA_TALENT))
                {
                    GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_LAVA_SURGE_PROC, true);
                    if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_SHAMAN_LAVA_BURST))
                        GetTarget()->GetSpellHistory()->RestoreCharge(spellInfo->ChargeCategoryId);
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_lava_surge_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_lava_surge_AuraScript();
        }
};

// 188196 - Lightning Bolt
class spell_legion_sha_lightning_bolt : public SpellScriptLoader
{
    public:
        spell_legion_sha_lightning_bolt() : SpellScriptLoader("spell_legion_sha_lightning_bolt") { }

        class spell_legion_sha_lightning_bolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_lightning_bolt_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LIGHTNING_BOLT_ENERGIZE
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_LIGHTNING_BOLT_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_lightning_bolt_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_lightning_bolt_SpellScript();
        }
};

// 187837 - Lightning Bolt (Enhancement)
class spell_legion_sha_lightning_bolt_enhancement : public SpellScriptLoader
{
    public:
        spell_legion_sha_lightning_bolt_enhancement() : SpellScriptLoader("spell_legion_sha_lightning_bolt_enhancement") { }

        class spell_legion_sha_lightning_bolt_enhancement_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_lightning_bolt_enhancement_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_OVERCHARGE
                });
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_SHAMAN_OVERCHARGE))
                    if (GetSpell()->GetPowerCost(POWER_MAELSTROM) != 0)
                    {
                        // http://www.askmrrobot.com/wow/theory/mechanic/spell/lightningboltovercharge?spec=ShamanEnhancement&version=7_0_3_22900
                        int32 damage = GetHitDamage() * (1 + 15 * GetSpell()->GetPowerCost(POWER_MAELSTROM) / 45);
                        SetHitDamage(damage);
                    }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_lightning_bolt_enhancement_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_lightning_bolt_enhancement_SpellScript();
        }
};

// 45284 - Lightning Bolt overload
class spell_legion_sha_lightning_bolt_overload : public SpellScriptLoader
{
    public:
        spell_legion_sha_lightning_bolt_overload() : SpellScriptLoader("spell_legion_sha_lightning_bolt_overload") { }

        class spell_legion_sha_lightning_bolt_overload_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_lightning_bolt_overload_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD_ENERGIZE
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_lightning_bolt_overload_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_lightning_bolt_overload_SpellScript();
        }
};

// 210689 - Lightning Rod (Talent)
class spell_legion_sha_lightning_rod_passive : public SpellScriptLoader
{
    public:
        spell_legion_sha_lightning_rod_passive() : SpellScriptLoader("spell_legion_sha_lightning_rod_passive") { }

        class spell_legion_sha_lightning_rod_passive_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_lightning_rod_passive_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LIGHTNING_ROD_PASSIVE,
                    SPELL_SHAMAN_LIGHTNING_ROD_DEBUFF,
                    SPELL_SHAMAN_LIGHTNING_BOLT,
                    SPELL_SHAMAN_CHAIN_LIGHTNING
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo() || !eventInfo.GetSpellInfo() || !eventInfo.GetProcSpell())
                    return false;

                return eventInfo.GetSpellInfo()->Id != SPELL_SHAMAN_LIGHTNING_ROD_DAMAGE;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_LIGHTNING_BOLT || eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_CHAIN_LIGHTNING)
                {
                    // Only initial target
                    if (eventInfo.GetProcSpell()->m_targets.GetUnitTarget() != eventInfo.GetActionTarget())
                        return;

                    if (roll_chance_i(aurEff->GetAmount()))
                        GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_SHAMAN_LIGHTNING_ROD_DEBUFF, true);
                }
            }

            void HandleDamageProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
                Unit::AuraApplicationVector targets = GetTarget()->GetTargetAuraApplications(SPELL_SHAMAN_LIGHTNING_ROD_DEBUFF);
                for (AuraApplication* aurApp : targets)
                    GetTarget()->CastCustomSpell(SPELL_SHAMAN_LIGHTNING_ROD_DAMAGE, SPELLVALUE_BASE_POINT0, damage, aurApp->GetTarget(), true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_lightning_rod_passive_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_lightning_rod_passive_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_lightning_rod_passive_AuraScript::HandleDamageProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_lightning_rod_passive_AuraScript();
        }
};

// 192223 - Liquid Magma Totem (target selector)
class spell_legion_sha_liquid_magma_totem_target_selector : public SpellScriptLoader
{
    public:
        spell_legion_sha_liquid_magma_totem_target_selector() : SpellScriptLoader("spell_legion_sha_liquid_magma_totem_target_selector") { }

        class spell_legion_sha_liquid_magma_totem_target_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_liquid_magma_totem_target_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LIQUID_MAGMA
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (targets.size() > 1)
                    Trinity::Containers::RandomResize(targets, 1);
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_LIQUID_MAGMA, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_liquid_magma_totem_target_selector_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_liquid_magma_totem_target_selector_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_liquid_magma_totem_target_selector_SpellScript();
        }
};

// 187880 - Maelstrom Weapon
class spell_legion_sha_maelstrom_weapon_energize : public SpellScriptLoader
{
    public:
        spell_legion_sha_maelstrom_weapon_energize() : SpellScriptLoader("spell_legion_sha_maelstrom_weapon_energize") { }

        class spell_legion_sha_maelstrom_weapon_energize_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_maelstrom_weapon_energize_AuraScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_MAELSTROM_WEAPON_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (SpellInfo const* spellInfo = eventInfo.GetSpellInfo())
                    if (!(spellInfo->SpellFamilyFlags[0] & 0x800000) || !eventInfo.GetDamageInfo() || !eventInfo.GetDamageInfo()->GetDamage())
                        return false;

                return true;
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_SHAMAN_MAELSTROM_WEAPON_ENERGIZE, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_maelstrom_weapon_energize_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_sha_maelstrom_weapon_energize_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_maelstrom_weapon_energize_AuraScript();
        }
};

 // 45297  - Chain Lightning Overload
 // 120588 - Elemental Blast Overload
 // 219271 - Icefury Overload
 // 77451  - Lava Burst Overload
 // 45284  - Lightning Bolt Overload
 // temporary hack until we have the new proc system: casting the Overload Spell should be done right after OnCast(), but damage calculation is invalid at that point.
class spell_legion_sha_mastery_elemental_overload_hack : public SpellScriptLoader
{
    public:
        spell_legion_sha_mastery_elemental_overload_hack() : SpellScriptLoader("spell_legion_sha_mastery_elemental_overload_hack") { }

        class spell_legion_sha_mastery_elemental_overload_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_mastery_elemental_overload_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD,
                    SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD,
                    SPELL_SHAMAN_LAVA_BURST_OVERLOAD,
                    SPELL_SHAMAN_ICEFURY_OVERLOAD,
                    SPELL_SHAMAN_ELEMENTAL_BLAST_OVERLOAD
                });
            }

            void HandleHit()
            {
                if (GetHitUnit() != GetExplTargetUnit())
                    return;

                AuraEffect const* aurEff0 = GetCaster()->GetAuraEffect(168534, EFFECT_0);
                AuraEffect const* aurEff1 = GetCaster()->GetAuraEffect(168534, EFFECT_1);

                if (!aurEff0 || !aurEff1)
                    return;

                if (roll_chance_i(aurEff0->GetAmount()))
                {
                    uint32 spellId = 0;
                    switch (GetSpellInfo()->Id)
                    {
                        case SPELL_SHAMAN_LIGHTNING_BOLT:
                            spellId = SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_CHAIN_LIGHTNING:
                            spellId = SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_LAVA_BURST:
                            spellId = SPELL_SHAMAN_LAVA_BURST_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_ICEFURY:
                            spellId = SPELL_SHAMAN_ICEFURY_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_ELEMENTAL_BLAST:
                            spellId = SPELL_SHAMAN_ELEMENTAL_BLAST_OVERLOAD;
                            break;
                        default:
                            return;
                    }

                    int32 damage = CalculatePct(GetHitDamage(), aurEff1->GetAmount());
                    CustomSpellValues values;
                    values.AddSpellMod(SPELLVALUE_BASE_POINT0, damage);
                    values.AddSpellMod(SPELLVALUE_CUSTOM_FLAGS, CUSTOM_CAST_FLAG_NO_DONE_BONUS);
                    GetCaster()->CastCustomSpell(spellId, values, GetHitUnit(), TRIGGERED_FULL_MASK);

                    if (Aura* powerAura = GetCaster()->GetAura(SPELL_SHAMAN_POWER_OF_THE_MAELSTROM))
                    {
                        if (spellId == SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD)
                        {
                            powerAura->DropCharge();
                            GetCaster()->CastCustomSpell(spellId, values, GetHitUnit(), TRIGGERED_FULL_MASK);
                        }
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_legion_sha_mastery_elemental_overload_SpellScript::HandleHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_mastery_elemental_overload_SpellScript();
        }
};

// 168534 - Mastery: Elemental Overload
class spell_legion_sha_mastery_elemental_overload : public SpellScriptLoader
{
    public:
        spell_legion_sha_mastery_elemental_overload() : SpellScriptLoader("spell_legion_sha_mastery_elemental_overload") { }

        class spell_legion_sha_mastery_elemental_overload_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_mastery_elemental_overload_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD,
                    SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD,
                    SPELL_SHAMAN_LAVA_BURST_OVERLOAD,
                    SPELL_SHAMAN_ICEFURY_OVERLOAD,
                    SPELL_SHAMAN_ELEMENTAL_BLAST_OVERLOAD
                });
            }

            bool CheckProc(ProcEventInfo& /*procInfo*/)
            {
                /*if (!procInfo.GetSpellInfo())
                    return false;
                return true;*/
                return false;
            }

            /*void HandleProc(AuraEffect const* aurEff, ProcEventInfo& procInfo)
            {
                if (roll_chance_i(aurEff->GetMiscValue() + GetCaster()->ToPlayer()->GetFloatValue(PLAYER_MASTERY)))
                {
                    uint32 spellId = 0;
                    switch (procInfo.GetSpellInfo()->Id)
                    {
                        case SPELL_SHAMAN_LIGHTNING_BOLT:
                            spellId = SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_CHAIN_LIGHTNING:
                            spellId = SPELL_SHAMAN_CHAIN_LIGHTNING_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_LAVA_BURST:
                            spellId = SPELL_SHAMAN_LAVA_BURST_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_ICEFURY:
                            spellId = SPELL_SHAMAN_ICEFURY_OVERLOAD;
                            break;
                        case SPELL_SHAMAN_ELEMENTAL_BLAST:
                            spellId = SPELL_SHAMAN_ELEMENTAL_BLAST_OVERLOAD;
                            break;
                        default:
                            return;
                    }

                    procInfo.GetActor()->CastSpell(procInfo.GetActionTarget(), spellId, true, NULL, aurEff);
                    if (Aura* powerAura = procInfo.GetActor()->GetAura(SPELL_SHAMAN_POWER_OF_THE_MAELSTROM))
                    {
                        if (spellId == SPELL_SHAMAN_LIGHTNING_BOLT_OVERLOAD)
                        {
                            powerAura->DropCharge();
                            procInfo.GetActor()->CastSpell(procInfo.GetActionTarget(), spellId, true, NULL, aurEff);
                        }
                    }
                }
            }*/

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_mastery_elemental_overload_AuraScript::CheckProc);
                //OnEffectProc += AuraEffectProcFn(spell_legion_sha_mastery_elemental_overload_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_mastery_elemental_overload_AuraScript();
        }
};

// 210621 - Path of Flames Spread
class spell_legion_sha_path_of_flames_spread : public SpellScriptLoader
{
    public:
        spell_legion_sha_path_of_flames_spread() : SpellScriptLoader("spell_legion_sha_path_of_flames_spread") { }

        class spell_legion_sha_path_of_flames_spread_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_path_of_flames_spread_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_FLAME_SHOCK
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetExplTargetUnit());
                Trinity::Containers::RandomResize(targets, [this](WorldObject* target)
                {
                    return target->GetTypeId() == TYPEID_UNIT && !target->ToUnit()->HasAura(SPELL_SHAMAN_FLAME_SHOCK, GetCaster()->GetGUID());
                }, 1);
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                if (Unit* mainTarget = GetExplTargetUnit())
                {
                    if (Aura* flameShock = mainTarget->GetAura(SPELL_SHAMAN_FLAME_SHOCK, GetCaster()->GetGUID()))
                    {
                        if (Aura* newAura = GetCaster()->AddAura(SPELL_SHAMAN_FLAME_SHOCK, GetHitUnit()))
                        {
                            newAura->SetDuration(flameShock->GetDuration());
                            newAura->SetMaxDuration(flameShock->GetDuration());
                        }
                    }
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_path_of_flames_spread_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_path_of_flames_spread_SpellScript::HandleScript, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_path_of_flames_spread_SpellScript();
        }
};

// 370 - Purge
class spell_legion_sha_purge : public SpellScriptLoader
{
    public:
        spell_legion_sha_purge() : SpellScriptLoader("spell_legion_sha_purge") { }

        class spell_legion_sha_purge_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_purge_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_ELECTROCUTE,
                    SPELL_SHAMAN_ELECTROCUTE_DAMAGE,
                    SPELL_SHAMAN_PURIFYING_WATERS,
                    SPELL_SHAMAN_PURIFYING_WATERS_HEAL
                });
            }

            void OnSuccessfulDispel(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_SHAMAN_ELECTROCUTE))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_ELECTROCUTE_DAMAGE, true);

                if (GetCaster()->HasAura(SPELL_SHAMAN_PURIFYING_WATERS))
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_PURIFYING_WATERS_HEAL, true);
            }

            void Register() override
            {
                OnEffectSuccessfulDispel += SpellEffectFn(spell_legion_sha_purge_SpellScript::OnSuccessfulDispel, EFFECT_0, SPELL_EFFECT_DISPEL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_purge_SpellScript();
        }
};

// 207285 - Queen Ascendant
class spell_legion_sha_queen_ascendant : public SpellScriptLoader
{
    public:
        spell_legion_sha_queen_ascendant() : SpellScriptLoader("spell_legion_sha_queen_ascendant") { }

        class spell_legion_sha_queen_ascendant_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_queen_ascendant_AuraScript);

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (procInfo.GetDamageInfo() && procInfo.GetDamageInfo()->GetDamageType() == SPELL_DIRECT_DAMAGE)
                    return true;
                return false;
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_QUEEN_ASCENDANT_BUFF, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_queen_ascendant_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_sha_queen_ascendant_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_queen_ascendant_AuraScript();
        }
};

// 215864 - Rainfall
class spell_legion_sha_rainfall : public SpellScriptLoader
{
    public:
        spell_legion_sha_rainfall() : SpellScriptLoader("spell_legion_sha_rainfall") { }

        class spell_legion_sha_rainfall_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_rainfall_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_HEALING_RAIN,
                    SPELL_SHAMAN_STORMSTRIKE,
                    SPELL_SHAMAN_LAVA_LASH,
                    SPELL_SHAMAN_RAINFALL,
                    SPELL_SHAMAN_RAINFALL_TICK
                });
            }

            void HandleOnHit()
            {
                if (WorldLocation const* loc = GetExplTargetDest())
                {
                    // Share dynobject with healing rain
                    GetCaster()->RemoveDynObject(SPELL_SHAMAN_HEALING_RAIN);
                    GetCaster()->CastSpell(loc->GetPositionX(), loc->GetPositionY(), loc->GetPositionZ(), SPELL_SHAMAN_HEALING_RAIN, true);
                }
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_legion_sha_rainfall_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_rainfall_SpellScript();
        }

        class spell_legion_sha_rainfall_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_rainfall_AuraScript);

            bool CheckProc(ProcEventInfo& procInfo)
            {
                // Prevent double proc from StormStrike (2 spells with same flags)
                if (procInfo.GetSpellInfo() && (procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_STORMSTRIKE || procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_LAVA_LASH))
                    return true;
                return false;
            }

            void OnTick(AuraEffect const* /*aurEff*/)
            {
                if (!GetCaster())
                    return;

                if (DynamicObject* dynObj = GetCaster()->GetDynObject(SPELL_SHAMAN_HEALING_RAIN))
                    GetCaster()->CastSpell(dynObj->GetPositionX(), dynObj->GetPositionY(), dynObj->GetPositionZ(), SPELL_SHAMAN_RAINFALL_TICK, true);
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                Aura* rainFall = GetTarget()->GetAura(SPELL_SHAMAN_RAINFALL);
                SpellEffectInfo const* rainFallduration = sSpellMgr->AssertSpellInfo(SPELL_SHAMAN_RAINFALL)->GetEffect(EFFECT_2);

                if (!rainFall || !rainFallduration)
                    return;

                if (SpellEffectInfo const* rainFallMaxduration = sSpellMgr->AssertSpellInfo(SPELL_SHAMAN_RAINFALL)->GetEffect(EFFECT_3))
                {
                    int32 newDuration = std::min(rainFall->GetDuration() + (rainFallduration->CalcValue() * IN_MILLISECONDS), rainFallMaxduration->BasePoints * IN_MILLISECONDS);
                    rainFall->SetMaxDuration(newDuration);
                    rainFall->SetDuration(newDuration);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_rainfall_AuraScript::CheckProc);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_rainfall_AuraScript::OnTick, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
                OnProc += AuraProcFn(spell_legion_sha_rainfall_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_rainfall_AuraScript();
        }
};

// 21169 - Reincarnation
class spell_legion_sha_reincarnation : public SpellScriptLoader
{
    public:
        spell_legion_sha_reincarnation() : SpellScriptLoader("spell_legion_sha_reincarnation") { }

        class spell_legion_sha_reincarnation_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_reincarnation_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_SERVANT_OF_THE_QUEEN,
                    SPELL_SHAMAN_SERVANT_OF_THE_QUEEN_BUFF
                });
            }

            void TriggerBuff()
            {
                if (GetCaster()->HasAura(SPELL_SHAMAN_SERVANT_OF_THE_QUEEN))
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_SERVANT_OF_THE_QUEEN_BUFF, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_sha_reincarnation_SpellScript::TriggerBuff);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_reincarnation_SpellScript();
        }
};

// 114052 - Ascendance (Restoration)
class spell_legion_sha_ascendance_restoration : public SpellScriptLoader
{
    public:
        spell_legion_sha_ascendance_restoration() : SpellScriptLoader("spell_legion_sha_ascendance_restoration") { }

        class spell_legion_sha_ascendance_restoration_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_ascendance_restoration_AuraScript);

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (!procInfo.GetHealInfo() || !procInfo.GetSpellInfo())
                    return false;

                if (procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_RESTORATIVE_MISTS)
                    return false;

                if (!procInfo.GetHealInfo()->GetHeal())
                    return false;

                return true;
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& procInfo)
            {
                int32 heal = procInfo.GetHealInfo()->GetHeal();
                if (heal > 0)
                    procInfo.GetActor()->CastCustomSpell(SPELL_SHAMAN_RESTORATIVE_MISTS, SPELLVALUE_BASE_POINT0, heal, procInfo.GetActionTarget(), true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_ascendance_restoration_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_ascendance_restoration_AuraScript::OnProc, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_ascendance_restoration_AuraScript();
        }
};

// 114083 - Restorative Mists
class spell_legion_sha_restorative_mists : public SpellScriptLoader
{
    public:
        spell_legion_sha_restorative_mists() : SpellScriptLoader("spell_legion_sha_restorative_mists") { }

        class spell_legion_sha_restorative_mists_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_restorative_mists_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = uint32(targets.size());
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (_targetCount)
                    SetHitHeal(GetHitHeal() / _targetCount);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_restorative_mists_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_restorative_mists_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }

        private:
            uint32 _targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_restorative_mists_SpellScript();
        }
};

// 16196 - Resurgence
class spell_legion_sha_resurgence : public SpellScriptLoader
{
    public:
        spell_legion_sha_resurgence() : SpellScriptLoader("spell_legion_sha_resurgence") { }

        class spell_legion_sha_resurgence_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_resurgence_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_BOTTOMLESS_DEPTHS,
                    SPELL_SHAMAN_RESURGENCE_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                if (!procInfo.GetProcSpell())
                    return false;

                return procInfo.GetActionTarget();
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo &procInfo)
            {
                float pctEnergize = float(aurEff->GetAmount());
                AuraEffect const* bottomlessDepths = GetTarget()->GetAuraEffect(SPELL_SHAMAN_BOTTOMLESS_DEPTHS, EFFECT_0);

                if ((procInfo.GetHitMask() & PROC_HIT_CRITICAL) || (bottomlessDepths && procInfo.GetActionTarget()->HealthBelowPct(bottomlessDepths->GetAmount())))
                {
                    switch (procInfo.GetProcSpell()->GetSpellInfo()->Id)
                    {
                        case SPELL_SHAMAN_HEALING_WAVE:
                            // base amount
                            break;
                        case SPELL_SHAMAN_HEALING_SURGE:
                        case SPELL_SHAMAN_RIPTIDE:
                        case SPELL_SHAMAN_UNLEASH_LIFE:
                            pctEnergize *= 0.6f;
                            break;
                        case SPELL_SHAMAN_CHAIN_HEAL:
                            pctEnergize *= 0.25f;
                            if (AuraEffect* refreshingCurrents = GetTarget()->GetAuraEffect(SPELL_SHAMAN_REFRESHING_CURRENTS, EFFECT_0))
                                pctEnergize += CalculatePct(pctEnergize, refreshingCurrents->GetAmount());
                            break;
                        default:
                            pctEnergize = 0.f;
                            break;
                    }

                    if (pctEnergize > 0.f)
                        GetTarget()->CastCustomSpell(SPELL_SHAMAN_RESURGENCE_ENERGIZE, SPELLVALUE_BASE_POINT0, int32(pctEnergize), GetTarget(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_resurgence_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_resurgence_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_resurgence_AuraScript();
        }
};

// 61295 - Riptide
class spell_legion_sha_riptide : public SpellScriptLoader
{
    public:
        spell_legion_sha_riptide() : SpellScriptLoader("spell_legion_sha_riptide") { }

        class spell_legion_sha_riptide_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_riptide_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_DELUGE_AURA,
                    SPELL_SHAMAN_DELUGE_TALENT,
                    SPELL_SHAMAN_RIPPLING_WATERS,
                    SPELL_SHAMAN_RIPPLING_WATERS_HEAL
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    // Deluge aura is a area target cast but here it should only affect the aura target
                    if (caster->HasAura(SPELL_SHAMAN_DELUGE_TALENT))
                        caster->AddAura(SPELL_SHAMAN_DELUGE_AURA, GetTarget());
                }
            }

            void HandleEffectReApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_SHAMAN_RIPPLING_WATERS))
                        caster->CastSpell(GetTarget(), SPELL_SHAMAN_RIPPLING_WATERS_HEAL, true);
                }
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE
                    && GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_ENEMY_SPELL)
                    return;

                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_SHAMAN_RIPPLING_WATERS))
                        caster->CastSpell(GetTarget(), SPELL_SHAMAN_RIPPLING_WATERS_HEAL, true);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_riptide_AuraScript::HandleEffectPeriodic, EFFECT_1, SPELL_AURA_PERIODIC_HEAL);
                AfterEffectApply += AuraEffectApplyFn(spell_legion_sha_riptide_AuraScript::HandleEffectReApply, EFFECT_1, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAPPLY);
                AfterEffectRemove += AuraEffectRemoveFn(spell_legion_sha_riptide_AuraScript::OnRemove, EFFECT_1, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_riptide_AuraScript();
        }
};

// 191582 - Shamanistic Healing
class spell_legion_sha_shamanistic_healing : public SpellScriptLoader
{
    public:
        spell_legion_sha_shamanistic_healing() : SpellScriptLoader("spell_legion_sha_shamanistic_healing") { }

        class spell_legion_sha_shamanistic_healing_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_shamanistic_healing_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_NATURES_ESSENCE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget() || !eventInfo.GetDamageInfo())
                    return false;

                return eventInfo.GetActionTarget()->HealthBelowPctDamaged(GetEffect(EFFECT_1)->GetAmount(), eventInfo.GetDamageInfo()->GetDamage());
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*procInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_NATURES_ESSENCE, true);
                GetTarget()->GetSpellHistory()->AddCooldown(SPELL_SHAMAN_NATURES_ESSENCE, 0, std::chrono::seconds(aurEff->GetAmount()));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_shamanistic_healing_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_shamanistic_healing_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_shamanistic_healing_AuraScript();
        }
};

// 98020 - Spirit Link
class spell_legion_sha_spirit_link : public SpellScriptLoader
{
    public:
        spell_legion_sha_spirit_link() : SpellScriptLoader("spell_legion_sha_spirit_link") { }

        class spell_legion_sha_spirit_link_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_spirit_link_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_SPIRIT_LINK
                });
            }

            void CalculatePercentage(std::list<WorldObject*>& targets)
            {
                float totalHealthPct = 0;
                for (WorldObject* target : targets)
                    if (Unit* unitTarget = target->ToUnit())
                        totalHealthPct += unitTarget->GetHealthPct();

                _averageHealthPct = totalHealthPct / float(targets.size());
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                float playerHealthPct = GetHitUnit()->GetHealthPct();

                if (G3D::fuzzyEq(playerHealthPct, _averageHealthPct))
                    return;

                uint64 currentHealth = GetHitUnit()->GetHealth();
                uint64 redistributedHealth = GetHitUnit()->CountPctFromMaxHealth(int32(_averageHealthPct));
                GetCaster()->CastCustomSpell(SPELL_SHAMAN_SPIRIT_LINK, (playerHealthPct > _averageHealthPct) ? SPELLVALUE_BASE_POINT0 : SPELLVALUE_BASE_POINT1, int32(std::abs(int64(redistributedHealth) - int64(currentHealth))), GetHitUnit(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_spirit_link_SpellScript::CalculatePercentage, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_spirit_link_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_DUMMY);
            }

        private:
            float _averageHealthPct = 0.f;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_spirit_link_SpellScript();
        }
};

// 198240 - Spirit of the Maelstrom
class spell_legion_sha_spirit_of_the_maelstorm : public SpellScriptLoader
{
    public:
        spell_legion_sha_spirit_of_the_maelstorm() : SpellScriptLoader("spell_legion_sha_spirit_of_the_maelstorm") { }

        class spell_legion_sha_spirit_of_the_maelstorm_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_spirit_of_the_maelstorm_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_TRAIT,
                    SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_ENERGIZE
                });
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (AuraEffect* trait = GetTarget()->GetAuraEffect(SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_TRAIT, EFFECT_0))
                    GetTarget()->CastCustomSpell(SPELL_SHAMAN_SPIRIT_OF_THE_MAELSTORM_ENERGIZE, SPELLVALUE_BASE_POINT0, trait->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_sha_spirit_of_the_maelstorm_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_spirit_of_the_maelstorm_AuraScript();
        }
};

// 201845 - Stormbringer
class spell_legion_sha_stormbringer : public SpellScriptLoader
{
    public:
        spell_legion_sha_stormbringer() : SpellScriptLoader("spell_legion_sha_stormbringer") { }

        class spell_legion_sha_stormbringer_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_stormbringer_AuraScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_STORMBRINGER_PROC,
                    SPELL_SHAMAN_STORMSTRIKE_TALENT,
                    SPELL_SHAMAN_STORMSTRIKE_OFFHAND,
                    SPELL_SHAMAN_RAGING_STORMS,
                    SPELL_SHAMAN_RAGING_STORMS_BUFF,
                    SPELL_SHAMAN_WIND_STRIKES,
                    SPELL_SHAMAN_WIND_STRIKES_BUFF
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // can't proc off Stormstrike
                SpellInfo const* spellInfo = eventInfo.GetSpellInfo();
                if (spellInfo
                    && (spellInfo->Id == SPELL_SHAMAN_STORMSTRIKE || spellInfo->Id == SPELL_SHAMAN_STORMSTRIKE_OFFHAND
                        || spellInfo->Id == SPELL_SHAMAN_WIND_STRIKE_MAINHAND || spellInfo->Id == SPELL_SHAMAN_WIND_STRIKE_OFFHAND))
                    return false;

                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage();
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                caster->CastSpell(caster, SPELL_SHAMAN_STORMBRINGER_PROC, true);
                caster->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_SHAMAN_STORMSTRIKE_TALENT)->ChargeCategoryId);

                if (Aura* windStrikesTalent = caster->GetAuraOfRankedSpell(SPELL_SHAMAN_WIND_STRIKES))
                {
                    int32 bp = windStrikesTalent->GetEffect(EFFECT_0)->GetAmount();
                    caster->CastCustomSpell(caster, SPELL_SHAMAN_WIND_STRIKES_BUFF, &bp, NULL, NULL, true);
                }

                if (caster->HasAura(SPELL_SHAMAN_RAGING_STORMS))
                    caster->CastSpell(caster, SPELL_SHAMAN_RAGING_STORMS_BUFF, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_stormbringer_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_sha_stormbringer_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_stormbringer_AuraScript();
        }
};

// 205495 - Stormkeeper
class spell_legion_sha_stormkeeper : public SpellScriptLoader
{
    public:
        spell_legion_sha_stormkeeper() : SpellScriptLoader("spell_legion_sha_stormkeeper") { }

        class spell_legion_sha_stormkeeper_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_stormkeeper_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_LIGHTNING_BOLT,
                    SPELL_SHAMAN_CHAIN_LIGHTNING
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                // spellfam. is used for multiple spells
                return procInfo.GetSpellInfo() && (procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_LIGHTNING_BOLT || procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_CHAIN_LIGHTNING);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_stormkeeper_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_stormkeeper_AuraScript();
        }
};

// 195255 - Stormlash
class spell_legion_sha_stormlash : public SpellScriptLoader
{
    public:
        spell_legion_sha_stormlash() : SpellScriptLoader("spell_legion_sha_stormlash") { }

        class spell_legion_sha_stormlash_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_stormlash_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_FLAMETONGUE_BUFF,
                    SPELL_SHAMAN_FROSTBRAND,
                    SPELL_SHAMAN_WINDSONG,
                    SPELL_SHAMAN_BOULDERFIST,
                    SPELL_SHAMAN_CRASH_LIGHTNING_BUFF,
                    SPELL_SHAMAN_STORMLASH_RAID_DUMMY_AURA,
                    SPELL_SHAMAN_STORMLASH_FAKE_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    return (caster->HasAura(SPELL_SHAMAN_FLAMETONGUE_BUFF) ||
                        caster->HasAura(SPELL_SHAMAN_FROSTBRAND) ||
                        caster->HasAura(SPELL_SHAMAN_WINDSONG) ||
                        caster->HasAura(SPELL_SHAMAN_BOULDERFIST) ||
                        caster->HasAura(SPELL_SHAMAN_CRASH_LIGHTNING_BUFF));
                return false;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                Unit* target = eventInfo.GetActionTarget();
                SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_SHAMAN_STORMLASH_FAKE_DAMAGE);
                int32 potentialDamage = GetUnitOwner()->SpellDamageBonusDone(target, spellInfo, 0, SPELL_DIRECT_DAMAGE, spellInfo->GetEffect(EFFECT_0));

                CustomSpellValues values;
                values.AddSpellMod(SPELLVALUE_BASE_POINT0, potentialDamage);
                values.AddSpellMod(SPELLVALUE_BASE_POINT1, potentialDamage);
                GetUnitOwner()->CastCustomSpell(SPELL_SHAMAN_STORMLASH_RAID_DUMMY_AURA, values, GetUnitOwner(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_stormlash_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_stormlash_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_stormlash_AuraScript();
        }
};

// 195222 - Stormlash (raid dummy aura)
class spell_legion_sha_stormlash_buff : public SpellScriptLoader
{
    public:
        spell_legion_sha_stormlash_buff() : SpellScriptLoader("spell_legion_sha_stormlash_buff") { }

        class spell_legion_sha_stormlash_buff_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_stormlash_buff_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                uint32 spellId = GetSpellInfo()->Id;
                targets.remove_if([](WorldObject* target)
                {
                    Player* player = target->ToPlayer();
                    if (!player)
                        return true;

                    return player->GetRole() == TALENT_ROLE_HEALER;
                });

                targets.sort([spellId, caster](WorldObject* a, WorldObject* b)
                {
                    Unit* unitA = a->ToUnit();
                    Unit* unitB = b->ToUnit();

                    bool hasAura = unitA->HasAura(spellId);
                    if (hasAura != unitB->HasAura(spellId))
                        return !hasAura;

                    bool hasAuraFromCaster = unitA->HasAura(spellId, caster->GetGUID());
                    if (hasAuraFromCaster != unitB->HasAura(spellId, caster->GetGUID()))
                        return !hasAuraFromCaster;

                    return !hasAura || !hasAuraFromCaster;
                });

                if (AuraEffect const* stormlash = caster->GetAuraEffect(SPELL_SHAMAN_STORMLASH, EFFECT_0))
                {
                    uint32 maxTargets = stormlash->GetAmount();

                    if (AuraEffect const* empoweredStormlash = caster->GetAuraEffect(SPELL_SHAMAN_EMPOWERED_STORMLASH, EFFECT_0))
                        maxTargets += empoweredStormlash->GetAmount();

                    if (targets.size() > maxTargets)
                        targets.resize(maxTargets);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_sha_stormlash_buff_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_CASTER_AREA_RAID);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_stormlash_buff_SpellScript();
        }

        class spell_legion_sha_stormlash_buff_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_stormlash_buff_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_STORMLASH_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // can't proc from self
                if (SpellInfo const* spellInfo = eventInfo.GetSpellInfo())
                    if (spellInfo->Id == SPELL_SHAMAN_STORMLASH_DAMAGE)
                        return false;

                if (GetUnitOwner()->GetSpellHistory()->HasCooldown(SPELL_SHAMAN_STORMLASH_DAMAGE))
                    return false;

                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage() && eventInfo.GetActionTarget() && GetEffect(EFFECT_0) && GetEffect(EFFECT_1);
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                // http://www.wowhead.com/spell=195255/stormlash#comments for the explanation on how Stormlash works
                AuraEffect* effect0 = GetEffect(EFFECT_0);
                float expiredPct = float(GetMaxDuration() - GetDuration()) / GetMaxDuration() * 100.f;
                int32 damage = CalculatePct(GetEffect(EFFECT_1)->GetAmount(), expiredPct - _prevProcPct);
                effect0->ChangeAmount(effect0->GetAmount() - damage);
                _prevProcPct = expiredPct;

                GetUnitOwner()->CastCustomSpell(SPELL_SHAMAN_STORMLASH_DAMAGE, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetActionTarget(), true);
                GetUnitOwner()->GetSpellHistory()->AddCooldown(SPELL_SHAMAN_STORMLASH_DAMAGE, 0, std::chrono::milliseconds(100));

                if (effect0->GetAmount() <= 0)
                    Remove();
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_stormlash_buff_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_sha_stormlash_buff_AuraScript::HandleProc);
            }

        private:
            float _prevProcPct = 0.f;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_stormlash_buff_AuraScript();
        }
};

// 17364 - Stormstrike
class spell_legion_sha_stormstrike : public SpellScriptLoader
{
    public:
        spell_legion_sha_stormstrike() : SpellScriptLoader("spell_legion_sha_stormstrike") { }

        class spell_legion_sha_stormstrike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_stormstrike_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_FORKED_LIGHTNING,
                    SPELL_SHAMAN_FORKED_LIGHTNING_DAMAGE,
                    SPELL_SHAMAN_STORMFLURRY
                });
            }

            void HandleForkedLightning()
            {
                if (GetCaster()->HasAura(SPELL_SHAMAN_FORKED_LIGHTNING))
                    GetCaster()->CastSpell(GetCaster(), SPELL_SHAMAN_FORKED_LIGHTNING_DAMAGE, true);
            }

            void HandleOnEffectHit(SpellEffIndex /*effIdx*/)
            {
                if (AuraEffect* stormflurry = GetCaster()->GetAuraEffect(SPELL_SHAMAN_STORMFLURRY, EFFECT_0))
                {
                    if (roll_chance_i(stormflurry->GetAmount()))
                    {
                        GetCaster()->CastCustomSpell(GetEffectInfo()->TriggerSpell, SPELLVALUE_CUSTOM_FLAGS, CUSTOM_CAST_FLAG_SPECIAL_CALCULATION, GetHitUnit(), TRIGGERED_FULL_MASK);
                        GetCaster()->CastCustomSpell(GetEffectInfo(EFFECT_1)->TriggerSpell, SPELLVALUE_CUSTOM_FLAGS, CUSTOM_CAST_FLAG_SPECIAL_CALCULATION, GetHitUnit(), TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                OnCast += SpellCastFn(spell_legion_sha_stormstrike_SpellScript::HandleForkedLightning);
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_stormstrike_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_TRIGGER_SPELL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_stormstrike_SpellScript();
        }
};

// 32175 - Stormstrike
// 32176 - Stormstrike Off-Hand
class spell_legion_sha_stormstrike_damage : public SpellScriptLoader
{
    public:
        spell_legion_sha_stormstrike_damage() : SpellScriptLoader("spell_legion_sha_stormstrike_damage") { }

        class spell_legion_sha_stormstrike_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_stormstrike_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_STORMFLURRY
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIdx*/)
            {
                if (GetSpell()->HasCustomCastFlag(CUSTOM_CAST_FLAG_SPECIAL_CALCULATION))
                    if (AuraEffect* stormflurry = GetCaster()->GetAuraEffect(SPELL_SHAMAN_STORMFLURRY, EFFECT_1))
                        SetHitDamage(CalculatePct(GetHitDamage(), stormflurry->GetAmount()));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_stormstrike_damage_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_stormstrike_damage_SpellScript();
        }
};

// Earth Elemental Totem, Fire Elemental Totem, Storm Elemental Totem : 198103, 198067, 192249
class spell_legion_sha_summon_elemental_totem_generic : public SpellScriptLoader
{
    public:
        spell_legion_sha_summon_elemental_totem_generic() : SpellScriptLoader("spell_legion_sha_summon_elemental_totem_generic") { }

        class spell_legion_sha_summon_elemental_totem_generic_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_summon_elemental_totem_generic_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_PRIMAL_ELEMENTALIST,
                    SPELL_SHAMAN_EARTH_ELEMENTAL_PRIMAL,
                    SPELL_SHAMAN_FIRE_ELEMENTAL_PRIMAL,
                    SPELL_SHAMAN_STORM_ELEMENTAL_PRIMAL,
                    SPELL_SHAMAN_EARTH_ELEMENTAL_SUMMON,
                    SPELL_SHAMAN_FIRE_ELEMENTAL_SUMMON,
                    SPELL_SHAMAN_STORM_ELEMENTAL_SUMMON
                });
            }

            void HandleElementals(SpellEffIndex /*effIndex*/)
            {
                uint32 spellId = 0;
                bool hasPrimalElementalist = GetCaster()->HasAura(SPELL_SHAMAN_PRIMAL_ELEMENTALIST);

                switch (GetSpellInfo()->Id)
                {
                    case SPELL_SHAMAN_EARTH_ELEMENTAL_TOTEM:
                        spellId = hasPrimalElementalist ? SPELL_SHAMAN_EARTH_ELEMENTAL_PRIMAL : SPELL_SHAMAN_EARTH_ELEMENTAL_SUMMON;
                        break;
                    case SPELL_SHAMAN_FIRE_ELEMENTAL_TOTEM:
                        spellId = hasPrimalElementalist ? SPELL_SHAMAN_FIRE_ELEMENTAL_PRIMAL : SPELL_SHAMAN_FIRE_ELEMENTAL_SUMMON;
                        break;
                    case SPELL_SHAMAN_STORM_ELEMENTAL_TOTEM:
                        spellId = hasPrimalElementalist ? SPELL_SHAMAN_STORM_ELEMENTAL_PRIMAL : SPELL_SHAMAN_STORM_ELEMENTAL_SUMMON;
                        break;
                    default:
                        break;
                }

                if (spellId)
                    GetCaster()->CastSpell(GetCaster(), spellId, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_summon_elemental_totem_generic_SpellScript::HandleElementals, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_summon_elemental_totem_generic_SpellScript();
        }
};

class DelayedSwellingWavesHeal : public BasicEvent
{
    public:
        DelayedSwellingWavesHeal(Unit* caster, int32 heal) : _caster(caster), _heal(heal) { }

        bool Execute(uint64 /*execTime*/, uint32 /*diff*/) override
        {
            _caster->CastCustomSpell(_caster, SPELL_SHAMAN_SWELLING_WAVES_HEAL, &_heal, NULL, NULL, true);
            return true;
        }

    private:
        Unit* _caster;
        int32 _heal;
};

// 204264 - Swelling Waves
class spell_legion_sha_swelling_waves : public SpellScriptLoader
{
    public:
        spell_legion_sha_swelling_waves() : SpellScriptLoader("spell_legion_sha_swelling_waves") { }

        class spell_legion_sha_swelling_waves_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_swelling_waves_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_HEALING_SURGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetActor()->GetGUID() == GetTarget()->GetGUID() && eventInfo.GetHealInfo() && eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_HEALING_SURGE)
                    return true;
                return false;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->m_Events.AddEvent(new DelayedSwellingWavesHeal(GetTarget(), eventInfo.GetHealInfo()->GetHeal()), GetTarget()->m_Events.CalculateTime(aurEff->GetAmount() * IN_MILLISECONDS));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_swelling_waves_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_swelling_waves_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_swelling_waves_AuraScript();
        }
};

// 51490 - Thunderstorm
class spell_legion_sha_thunderstorm : public SpellScriptLoader
{
    public:
        spell_legion_sha_thunderstorm() : SpellScriptLoader("spell_legion_sha_thunderstorm") { }

        class spell_legion_sha_thunderstorm_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_thunderstorm_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_TRAVELING_STORMS,
                    SPELL_SHAMAN_THUNDERSTORM_ALLY
                });
            }

            void HandleDummy()
            {
                Unit* caster = GetCaster();
                if (Unit* unitTarget = GetHitUnit())
                {
                    if (!unitTarget->IsAlive())
                        return;

                    if (caster->IsFriendlyTo(unitTarget) && caster->HasAura(SPELL_SHAMAN_TRAVELING_STORMS))
                        caster->CastSpell(unitTarget, SPELL_SHAMAN_THUNDERSTORM_ALLY, false);
                }
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_legion_sha_thunderstorm_SpellScript::HandleDummy);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_thunderstorm_SpellScript();
        }
};

// 202188, 210651, 210657, 210660 - Resonance Totem, Storm Totem, Ember Totem, Tailwind Totem
class spell_legion_sha_totem_mastery : public SpellScriptLoader
{
    public:
        spell_legion_sha_totem_mastery() : SpellScriptLoader("spell_legion_sha_totem_mastery") { }

        class spell_legion_sha_totem_mastery_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_totem_mastery_SpellScript);

            void HandleCast(SpellEffIndex /*effIndex*/)
            {
                if (Unit* totem = GetCaster()->GetControlledCreature(GetEffectInfo()->MiscValue))
                    totem->ToTotem()->UnSummon();
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_legion_sha_totem_mastery_SpellScript::HandleCast, EFFECT_0, SPELL_EFFECT_SUMMON);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_totem_mastery_SpellScript();
        }
};

// 204406 - Thunderstorm (ally target)
class spell_legion_sha_thunderstorm_ally : public SpellScriptLoader
{
    public:
        spell_legion_sha_thunderstorm_ally() : SpellScriptLoader("spell_legion_sha_thunderstorm_ally") { }

        class spell_legion_sha_thunderstorm_ally_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_sha_thunderstorm_ally_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_THUNDERSTORM_ALLY_DAMAGE,
                    SPELL_SHAMAN_THUNDERSTORM_ALLY_KNOCKBACK
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit()->GetPositionX(), GetHitUnit()->GetPositionY(), GetHitUnit()->GetPositionZ(), SPELL_SHAMAN_THUNDERSTORM_ALLY_DAMAGE, true);
                GetCaster()->CastSpell(GetHitUnit()->GetPositionX(), GetHitUnit()->GetPositionY(), GetHitUnit()->GetPositionZ(), SPELL_SHAMAN_THUNDERSTORM_ALLY_KNOCKBACK, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_sha_thunderstorm_ally_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_sha_thunderstorm_ally_SpellScript();
        }
};

// 207358 - Tidal Pools
class spell_legion_sha_tidal_pools : public SpellScriptLoader
{
    public:
        spell_legion_sha_tidal_pools() : SpellScriptLoader("spell_legion_sha_tidal_pools") { }

        class spell_legion_sha_tidal_pools_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_tidal_pools_AuraScript);

            bool Validate(SpellInfo const* /*spellEntry*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_RIPTIDE,
                    SPELL_SHAMAN_TIDAL_TOTEM_VISUAL,
                    SPELL_SHAMAN_TIDAL_TOTEM_AREATRIGGER
                });
            }

            bool CheckProc(ProcEventInfo& procInfo)
            {
                // Prevent double proc from Riptide (2 spells with same flags)
                if (procInfo.GetSpellInfo()->Id == SPELL_SHAMAN_RIPTIDE)
                    return true;
                return false;
            }

            void HandleProc(ProcEventInfo& procInfo)
            {
                procInfo.GetActor()->CastSpell(procInfo.GetProcTarget(), SPELL_SHAMAN_TIDAL_TOTEM_VISUAL, true);
                procInfo.GetActor()->CastSpell(procInfo.GetProcTarget(), SPELL_SHAMAN_TIDAL_TOTEM_AREATRIGGER, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_tidal_pools_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_sha_tidal_pools_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_tidal_pools_AuraScript();
        }
};

// 51564 - Tidal Waves
class spell_legion_sha_tidal_waves : public SpellScriptLoader
{
    public:
        spell_legion_sha_tidal_waves() : SpellScriptLoader("spell_legion_sha_tidal_waves") { }

        class spell_legion_sha_tidal_waves_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_tidal_waves_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_TIDAL_WAVES,
                    SPELL_SHAMAN_RIPTIDE,
                    SPELL_SHAMAN_CHAIN_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo() && (eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_RIPTIDE || eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_CHAIN_HEAL))
                    return true;
                return false;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_TIDAL_WAVES, true);

                if (GetTarget()->HasAura(SPELL_SHAMAN_CRASHING_WAVES) && eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_RIPTIDE)
                    GetTarget()->CastSpell(GetTarget(), SPELL_SHAMAN_TIDAL_WAVES, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_tidal_waves_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_tidal_waves_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_tidal_waves_AuraScript();
        }
};

// 53390 - Tidal Waves (proc)
class spell_legion_sha_tidal_waves_proc : public SpellScriptLoader
{
    public:
        spell_legion_sha_tidal_waves_proc() : SpellScriptLoader("spell_legion_sha_tidal_waves_proc") { }

        class spell_legion_sha_tidal_waves_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_tidal_waves_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_RIPTIDE,
                    SPELL_SHAMAN_ECHO_OF_THE_ELEMENTS
                });
            }

            void HandleProc(ProcEventInfo& /*eventInfo*/)
            {
                if (AuraEffect const* echoOfTheElements = GetUnitOwner()->GetAuraEffect(SPELL_SHAMAN_ECHO_OF_THE_ELEMENTS, EFFECT_0))
                    if (roll_chance_i(echoOfTheElements->GetAmount()))
                        GetUnitOwner()->GetSpellHistory()->RestoreCharge(sSpellMgr->GetSpellInfo(SPELL_SHAMAN_RIPTIDE)->ChargeCategoryId);
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_legion_sha_tidal_waves_proc_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_tidal_waves_proc_AuraScript();
        }
};

// 200071 - Undulation
// This is the spell that should have the proc, instead of the aura proc (216251)
class spell_legion_sha_undulation : public SpellScriptLoader
{
    public:
        spell_legion_sha_undulation() : SpellScriptLoader("spell_legion_sha_undulation") { }

        class spell_legion_sha_undulation_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_undulation_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_UNDULATION
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                // SpellFam. flags are used for multiple spells we need to check that here
                return (eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_HEALING_SURGE || eventInfo.GetSpellInfo()->Id == SPELL_SHAMAN_HEALING_WAVE);
            }


            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                {
                    int32 amountCheckValue = aurEff->GetAmount() + aurEff->GetBaseAmount();

                    // 1: 50 + 50
                    // 2: 100 + 50
                    // 3: 150 + 50
                    if (amountCheckValue == (aurEff->GetBaseAmount() * 4))
                    {
                        caster->CastSpell(caster, SPELL_SHAMAN_UNDULATION, true);
                        amountCheckValue = aurEff->GetBaseAmount();
                    }

                    const_cast<AuraEffect*>(aurEff)->SetAmount(amountCheckValue);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_undulation_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_undulation_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_undulation_AuraScript();
        }
};

// 199055 - Unleash Doom (proc)
class spell_legion_sha_unleash_doom_proc : public SpellScriptLoader
{
    public:
        spell_legion_sha_unleash_doom_proc() : SpellScriptLoader("spell_legion_sha_unleash_doom_proc") { }

        class spell_legion_sha_unleash_doom_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_unleash_doom_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_SHAMAN_UNLEASH_LAVA,
                    SPELL_SHAMAN_UNLEASH_LIGHTNING
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), RAND(SPELL_SHAMAN_UNLEASH_LAVA, SPELL_SHAMAN_UNLEASH_LIGHTNING), true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_unleash_doom_proc_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_unleash_doom_proc_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_unleash_doom_proc_AuraScript();
        }
};

// 192630 - Volcanic Inferno
class spell_legion_sha_volcanic_inferno : public SpellScriptLoader
{
    public:
        spell_legion_sha_volcanic_inferno() : SpellScriptLoader("spell_legion_sha_volcanic_inferno") { }

        class spell_legion_sha_volcanic_inferno_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_volcanic_inferno_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(eventInfo.GetActionTarget()->GetPositionX(), eventInfo.GetActionTarget()->GetPositionY(), eventInfo.GetActionTarget()->GetPositionZ(), aurEff->GetSpellEffectInfo()->TriggerSpell, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_volcanic_inferno_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_volcanic_inferno_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_volcanic_inferno_AuraScript();
        }
};

// 33757 - Windfury
class spell_legion_sha_windfury : public SpellScriptLoader
{
    public:
        spell_legion_sha_windfury() : SpellScriptLoader("spell_sha_windfury") { }

        class spell_legion_sha_windfury_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_sha_windfury_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_SHAMAN_WINDFURY_ATTACK,
                    SPELL_SHAMAN_WINDFURY_ATTACK_VISUAL,
                    SPELL_SHAMAN_DOOM_WINDS,
                    SPELL_SHAMAN_WINDFURY_ATTACK_OFFHAND
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                int32 spellId = SPELL_SHAMAN_WINDFURY_ATTACK;
                if (eventInfo.GetDamageInfo()->GetAttackType() == OFF_ATTACK)
                {
                    if (!eventInfo.GetActor()->HasAura(SPELL_SHAMAN_DOOM_WINDS))
                        return;
                    spellId = SPELL_SHAMAN_WINDFURY_ATTACK_OFFHAND;
                }

                for (uint32 i = 0; i < 2; ++i)
                    eventInfo.GetActor()->CastSpell(eventInfo.GetProcTarget(), spellId, true, nullptr, aurEff);
                eventInfo.GetActor()->CastSpell(eventInfo.GetActor(), SPELL_SHAMAN_WINDFURY_ATTACK_VISUAL, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_sha_windfury_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_sha_windfury_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_sha_windfury_AuraScript();
        }
};

// 197995 - Wellspring
class spell_legion_sha_wellspring : public SpellScriptLoader
{
public:
    spell_legion_sha_wellspring() : SpellScriptLoader("spell_legion_sha_wellspring") { }

    class spell_legion_sha_wellspring_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_legion_sha_wellspring_SpellScript);

        bool Validate(SpellInfo const* /*spellInfo*/) override
        {
            return ValidateSpellInfo(
            {
                SPELL_SHAMAN_THUNDERSTORM_ALLY_DAMAGE
            });
        }

        void SendVisualWave(float orientationDiff)
        {
            GetCaster()->SendPlaySpellVisual(GetCaster()->GetNearPosition(29.5805f, Position::NormalizeOrientation(orientationDiff + 1.0f)), 0, SPELL_VISUAL_SHAMAN_WELLSPRING, 0, 0, 20, false);
        }

        void HandleDummy(SpellEffIndex /*effIndex*/)
        {
            if (!GetHitUnit())
                return;

            GetCaster()->CastSpell(GetHitUnit(), SPELL_SHAMAN_WELLSPRING_MISSILE_HEAL, true);
        }

        void SendVisualWaves()
        {
            SendVisualWave(-2.10511f);
            SendVisualWave(frand(-1.81188f,  -1.65976f));
            SendVisualWave(frand(-1.46794f,  -1.30585f));
            SendVisualWave(frand(-1.14473f,  -1.01333f));
            SendVisualWave(frand(-0.882909f, -0.701497f));
            SendVisualWave(frand(-0.5615f,   -0.4009f));
            SendVisualWave(frand(-0.882909f, -0.701497f));
            SendVisualWave(0.00744508f);
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_legion_sha_wellspring_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            OnCast += SpellCastFn(spell_legion_sha_wellspring_SpellScript::SendVisualWaves);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_legion_sha_wellspring_SpellScript();
    }
};


// 201846 - Stormbringer
class spell_legion_sha_stormbringer_proc : public SpellScriptLoader
{
public:
    spell_legion_sha_stormbringer_proc() : SpellScriptLoader("spell_legion_sha_stormbringer_proc") { }

    class spell_legion_sha_stormbringer_proc_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_legion_sha_stormbringer_proc_AuraScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            return ValidateSpellInfo(
            {
                SPELL_SHAMAN_STORMSTRIKE,
                SPELL_SHAMAN_STORMSTRIKE_OFFHAND,
                SPELL_SHAMAN_WIND_STRIKE_MAINHAND,
                SPELL_SHAMAN_WIND_STRIKE_OFFHAND
            });
        }

        bool CheckProc(ProcEventInfo& eventInfo)
        {
            if (!eventInfo.GetSpellInfo())
                return false;
            if (eventInfo.GetSpellInfo()->Id != SPELL_SHAMAN_STORMSTRIKE && eventInfo.GetSpellInfo()->Id != SPELL_SHAMAN_STORMSTRIKE_OFFHAND
                && eventInfo.GetSpellInfo()->Id != SPELL_SHAMAN_WIND_STRIKE_MAINHAND && eventInfo.GetSpellInfo()->Id != SPELL_SHAMAN_WIND_STRIKE_OFFHAND)
                return false;

            return eventInfo.GetDamageInfo()->GetDamage();
        }

        void HandleProc(ProcEventInfo& /*procInfo*/)
        {
            Unit* caster = GetCaster();
            if (!caster)
                return;

            caster->GetSpellHistory()->ResetCooldown(SPELL_SHAMAN_WIND_STRIKE, true);
            caster->GetSpellHistory()->ResetCooldown(SPELL_SHAMAN_STORMSTRIKE_TALENT, true);
        }


        void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_SHAMAN_WIND_STRIKE, true);
            GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_SHAMAN_STORMSTRIKE_TALENT, true);
        }

        void Register() override
        {
            OnEffectApply += AuraEffectApplyFn(spell_legion_sha_stormbringer_proc_AuraScript::OnApply, EFFECT_0, SPELL_AURA_408, AURA_EFFECT_HANDLE_REAL);
            DoCheckProc += AuraCheckProcFn(spell_legion_sha_stormbringer_proc_AuraScript::CheckProc);
            OnProc += AuraProcFn(spell_legion_sha_stormbringer_proc_AuraScript::HandleProc);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_legion_sha_stormbringer_proc_AuraScript();
    }
};

// 188070 - Healing Surge
class spell_legion_sha_healing_surge_enhancer : public SpellScriptLoader
{
public:
    spell_legion_sha_healing_surge_enhancer() : SpellScriptLoader("spell_legion_sha_healing_surge_enhancer") { }

    class spell_legion_sha_healing_surge_enhancer_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_legion_sha_healing_surge_enhancer_SpellScript);

        bool Validate(SpellInfo const* /*spellInfo*/) override
        {
            return ValidateSpellInfo({ SPELL_SHAMAN_HEALING_SURGE_INSTANT_MARKER });
        }

        bool Load() override
        {
            _consumeMaelstrom = GetCaster()->HasAura(SPELL_SHAMAN_HEALING_SURGE_INSTANT_MARKER);
            if (_consumeMaelstrom)
                GetSpell()->SetTriggerCastFlags(TRIGGERED_CAST_DIRECTLY);

            return true;
        }

        void HandleEnergize(SpellEffIndex /*effIndex*/)
        {
            if (_consumeMaelstrom)
                SetEffectValue(-GetSpellInfo()->GetEffect(EFFECT_2)->CalcValue());
        }

        void Register() override
        {
            OnEffectLaunchTarget += SpellEffectFn(spell_legion_sha_healing_surge_enhancer_SpellScript::HandleEnergize, EFFECT_1, SPELL_EFFECT_ENERGIZE);
        }

    private:
        bool _consumeMaelstrom = false;
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_legion_sha_healing_surge_enhancer_SpellScript();
    }
};

// Created by spell 210797 - Crashing Storm
class areatrigger_legion_sha_crashing_storm : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_sha_crashing_storm() : AreaTriggerEntityScript("areatrigger_legion_sha_crashing_storm") { }

        struct areatrigger_legion_sha_crashing_stormAI : public AreaTriggerAI
        {
            areatrigger_legion_sha_crashing_stormAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                int32 periodicTick = at->GetDuration() / 6;
                _scheduler.Schedule(Milliseconds(periodicTick), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_SHAMAN_CRASHING_STORM_DAMAGE, true);
                    task.Repeat();
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_sha_crashing_stormAI(areaTrigger);
        }
};

// 199121 - Doom Vortex
class areatrigger_legion_sha_doom_vortex : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_sha_doom_vortex() : AreaTriggerEntityScript("areatrigger_legion_sha_doom_vortex") { }

        struct areatrigger_legion_sha_doom_vortexAI : public AreaTriggerAI
        {
            areatrigger_legion_sha_doom_vortexAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_SHAMAN_DOOM_VORTEX_DAMAGE, true);
                    task.Repeat(Seconds(1));
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_sha_doom_vortexAI(areaTrigger);
        }
};

// Created by spell 61882 - Earthquake
class areatrigger_legion_sha_earthquake : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_sha_earthquake() : AreaTriggerEntityScript("areatrigger_legion_sha_earthquake") { }

        struct areatrigger_legion_sha_earthquakeAI : public AreaTriggerAI
        {
            areatrigger_legion_sha_earthquakeAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                int32 periodicTick = at->GetDuration() / 10;
                _scheduler.Schedule(Milliseconds(periodicTick), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_SHAMAN_EARTHQUAKE_DAMAGE, true);
                    task.Repeat();
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_sha_earthquakeAI(areaTrigger);
        }
};

// Created by spell 198839 - Earthen Shield
class areatrigger_legion_sha_earthen_shield_totem : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_sha_earthen_shield_totem() : AreaTriggerEntityScript("areatrigger_legion_sha_earthen_shield_totem") { }

        struct areatrigger_legion_sha_earthen_shield_totemAI : public AreaTriggerAI
        {
            areatrigger_legion_sha_earthen_shield_totemAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                    if (Unit* summoner = caster->GetOwner(false))
                        if (summoner->IsValidAssistTarget(unit))
                            caster->CastCustomSpell(SPELL_SHAMAN_EARTHEN_SHIELD_ABSORB, SPELLVALUE_BASE_POINT0, summoner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_SPELL), unit, TRIGGERED_FULL_MASK);
            }

            void OnUnitExit(Unit* unit) override
            {
                unit->RemoveAurasDueToSpell(SPELL_SHAMAN_EARTHEN_SHIELD_ABSORB);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_sha_earthen_shield_totemAI(areaTrigger);
        }
};

// Created by spell 233487 - Tidal Totem
class areatrigger_legion_sha_tidal_totem : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_sha_tidal_totem() : AreaTriggerEntityScript("areatrigger_legion_sha_tidal_totem") { }

        struct areatrigger_legion_sha_tidal_totemAI : public AreaTriggerAI
        {
            areatrigger_legion_sha_tidal_totemAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                int32 periodicTick = at->GetDuration() / 6;
                _scheduler.Schedule(Milliseconds(periodicTick), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_SHAMAN_TIDAL_TOTEM_HEAL, true);
                    task.Repeat();
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_sha_tidal_totemAI(areaTrigger);
        }
};

// Created by spell 196935 - Voodoo Totem
class areatrigger_legion_sha_voodoo_totem : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_sha_voodoo_totem() : AreaTriggerEntityScript("areatrigger_legion_sha_voodoo_totem") { }

        struct areatrigger_legion_sha_voodoo_totemAI : public AreaTriggerAI
        {
            areatrigger_legion_sha_voodoo_totemAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                    if (unit->IsValidAttackTarget(caster))
                    {
                        if (!unit->HasAura(SPELL_SHAMAN_HEXAUSTION))
                        {
                            caster->CastSpell(unit, SPELL_SHAMAN_HEX, true);
                            caster->AddAura(SPELL_SHAMAN_HEXAUSTION, unit);
                        }
                    }
            }

            void OnUnitExit(Unit* unit) override
            {
                unit->RemoveAurasDueToSpell(SPELL_SHAMAN_HEX);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_sha_voodoo_totemAI(areaTrigger);
        }
};

// 205532 - Volcanic Inferno
class areatrigger_legion_sha_volcanic_inferno : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_sha_volcanic_inferno() : AreaTriggerEntityScript("areatrigger_legion_sha_volcanic_inferno") { }

        struct areatrigger_legion_sha_volcanic_infernoAI : public AreaTriggerAI
        {
            areatrigger_legion_sha_volcanic_infernoAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_SHAMAN_VOLCANIC_INFERNO_DAMAGE, true);
                    task.Repeat(Seconds(1));
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_sha_volcanic_infernoAI(areaTrigger);
        }
};

// Created by spell 192078 - Wind Rush Totem
class areatrigger_legion_sha_wind_rush_totem : public AreaTriggerEntityScript
{
public:
    areatrigger_legion_sha_wind_rush_totem() : AreaTriggerEntityScript("areatrigger_legion_sha_wind_rush_totem") { }

    struct areatrigger_legion_sha_wind_rush_totemAI : public AreaTriggerAI
    {
        areatrigger_legion_sha_wind_rush_totemAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

        void OnUnitEnter(Unit* unit) override
        {
            if (Unit* caster = at->GetCaster())
                if (unit->GetTypeId() == TYPEID_PLAYER && unit->IsFriendlyTo(caster))
                    caster->CastSpell(unit, SPELL_SHAMAN_WIND_RUSH_TOTEM_BUFF, true);
        }
    };

    AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
    {
        return new areatrigger_legion_sha_wind_rush_totemAI(areaTrigger);
    }
};

void AddSC_legion_shaman_spell_scripts()
{
    new spell_legion_sha_aftershock();
    new spell_legion_sha_alpha_wolf();
    new spell_legion_sha_ancestral_guidance();
    new spell_legion_sha_ancestral_guidance_heal();
    new spell_legion_sha_ancestral_protection_absorb();
    new spell_legion_sha_ascendance_restoration();
    new spell_legion_sha_bloodlust();
    new spell_legion_sha_caress_of_the_tidemother();
    new spell_legion_sha_chain_heal();
    new spell_legion_sha_chain_lightning();
    new spell_legion_sha_chain_lightning_overload();
    new spell_legion_sha_cloudburst_totem();
    new spell_legion_sha_cloudburst_totem_heal();
    new spell_legion_sha_cloudburst_totem_recall();
    new spell_legion_sha_counterstrike_totem();
    new spell_legion_sha_crash_lightning();
    new spell_legion_sha_crash_lightning_proc();
    new spell_legion_sha_doom_vortex();
    new spell_legion_sha_earth_shield();
    new spell_legion_sha_earthen_shield();
    new spell_legion_sha_earth_shock();
    new spell_legion_sha_earthen_rage_passive();
    new spell_legion_sha_earthen_rage_proc_aura();
    new spell_legion_sha_earthgrab();
    new spell_legion_sha_earthquake();
    new spell_legion_sha_elemental_blast();
    new spell_legion_sha_elemental_healing();
    new spell_legion_sha_feral_lunge();
    new spell_legion_sha_feral_spirit();
    new spell_legion_sha_flame_shock();
    new spell_legion_sha_flametongue();
    new spell_legion_sha_forked_lightning();
    new spell_legion_sha_frost_shock();
    new spell_legion_sha_frostbrand();
    new spell_legion_sha_fury_of_air();
    new spell_legion_sha_fury_of_the_storms();
    new spell_legion_sha_ghost_in_the_mist();
    new spell_legion_sha_ghost_wolf();
    new spell_legion_sha_healing_rain();
    new spell_legion_sha_healing_stream_totem_heal();
    new spell_legion_sha_healing_surge_enhancer();
    new spell_legion_sha_healing_tide_totem();
    new spell_legion_sha_healing_tide_totem_heal();
    new spell_legion_sha_heroism();
    new spell_legion_sha_hot_hand();
    new spell_legion_sha_lava_burst();
    new spell_legion_sha_lava_surge();
    new spell_legion_sha_lightning_bolt();
    new spell_legion_sha_lightning_bolt_enhancement();
    new spell_legion_sha_lightning_bolt_overload();
    new spell_legion_sha_lightning_rod_passive();
    new spell_legion_sha_liquid_magma_totem_target_selector();
    new spell_legion_sha_maelstrom_weapon_energize();
    new spell_legion_sha_mastery_elemental_overload();
    new spell_legion_sha_mastery_elemental_overload_hack();
    new spell_legion_sha_path_of_flames_spread();
    new spell_legion_sha_purge();
    new spell_legion_sha_queen_ascendant();
    new spell_legion_sha_rainfall();
    new spell_legion_sha_reincarnation();
    new spell_legion_sha_restorative_mists();
    new spell_legion_sha_resurgence();
    new spell_legion_sha_riptide();
    new spell_legion_sha_shamanistic_healing();
    new spell_legion_sha_spirit_link();
    new spell_legion_sha_spirit_of_the_maelstorm();
    new spell_legion_sha_stormbringer();
    new spell_legion_sha_stormbringer_proc();
    new spell_legion_sha_stormkeeper();
    new spell_legion_sha_stormlash();
    new spell_legion_sha_stormlash_buff();
    new spell_legion_sha_stormstrike();
    new spell_legion_sha_stormstrike_damage();
    new spell_legion_sha_summon_elemental_totem_generic();
    new spell_legion_sha_swelling_waves();
    new spell_legion_sha_thunderstorm();
    new spell_legion_sha_thunderstorm_ally();
    new spell_legion_sha_tidal_pools();
    new spell_legion_sha_tidal_waves();
    new spell_legion_sha_tidal_waves_proc();
    new spell_legion_sha_totem_mastery();
    new spell_legion_sha_undulation();
    new spell_legion_sha_unleash_doom_proc();
    new spell_legion_sha_volcanic_inferno();
    new spell_legion_sha_wellspring();
    new spell_legion_sha_windfury();

    new areatrigger_legion_sha_crashing_storm();
    new areatrigger_legion_sha_doom_vortex();
    new areatrigger_legion_sha_earthquake();
    new areatrigger_legion_sha_earthen_shield_totem();
    new areatrigger_legion_sha_tidal_totem();
    new areatrigger_legion_sha_voodoo_totem();
    new areatrigger_legion_sha_volcanic_inferno();
    new areatrigger_legion_sha_wind_rush_totem();
}
