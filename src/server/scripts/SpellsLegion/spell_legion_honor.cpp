/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for honor talent spells.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_legion_honor_"
 */

#include "Player.h"
#include "Random.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellScript.h"

// 214027 - Adaptation
class spell_legion_honor_adaptation : public SpellScriptLoader
{
    public:
        spell_legion_honor_adaptation() : SpellScriptLoader("spell_legion_honor_adaptation") { }

        enum AdaptionSpells
        {
            SPELL_ADAPTATION_DEBUFF = 195901,
            SPELL_PVP_TRINKET       = 42292
        };

        class spell_legion_honor_adaptation_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_honor_adaptation_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->GetEffect(EFFECT_0) && ValidateSpellInfo(
                {
                    SPELL_ADAPTATION_DEBUFF,
                    SPELL_PVP_TRINKET
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                if (eventInfo.GetActionTarget()->HasAura(SPELL_ADAPTATION_DEBUFF))
                    return false;

                auto isLossOfControlSpell = [](SpellInfo const* spellInfo) -> bool
                {
                    return (spellInfo->GetAllEffectsMechanicMask() & MECHANIC_LOSS_CONTROL_MASK) != 0;
                };

                if (!eventInfo.GetSpellInfo() || !isLossOfControlSpell(eventInfo.GetSpellInfo()))
                    return false;

                int32 durationLimit = GetEffect(EFFECT_0)->GetAmount() * IN_MILLISECONDS;

                /// @workaround: we need to check the entire spell instead of the auras on the unit
                auto hasLossOfControlAuras = [&]() -> bool
                {
                    Unit::AuraApplicationMap const& appliedAuras = eventInfo.GetActionTarget()->GetAppliedAuras();
                    for (auto itr = appliedAuras.begin(); itr != appliedAuras.end(); ++itr)
                    {
                        Aura const* aura = itr->second->GetBase();

                        if (aura->GetDuration() < durationLimit)
                            continue;

                        if (isLossOfControlSpell(aura->GetSpellInfo()))
                            return true;
                    }

                    return false;
                };

                return hasLossOfControlAuras();
            }

            void Proc(ProcEventInfo& /*eventInfo*/)
            {
                Unit* target = GetTarget();
                target->CastSpell(target, SPELL_ADAPTATION_DEBUFF, true);
                target->CastSpell(target, SPELL_PVP_TRINKET, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_honor_adaptation_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_honor_adaptation_AuraScript::Proc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_honor_adaptation_AuraScript();
        }
};

// 195330 - Defender of the Weak
class spell_legion_honor_defender_of_the_weak : public SpellScriptLoader
{
    public:
        spell_legion_honor_defender_of_the_weak() : SpellScriptLoader("spell_legion_honor_defender_of_the_weak") { }

        class spell_legion_honor_defender_of_the_weak_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_honor_defender_of_the_weak_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->GetEffect(EFFECT_0) != nullptr;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                return eventInfo.GetActionTarget()->HealthBelowPct(GetEffect(EFFECT_0)->GetAmount());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_honor_defender_of_the_weak_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_honor_defender_of_the_weak_AuraScript();
        }
};

// 195416, 232047 - Hardiness
class spell_legion_honor_hardiness : public SpellScriptLoader
{
    public:
        spell_legion_honor_hardiness() : SpellScriptLoader("spell_legion_honor_hardiness") { }

        class spell_legion_honor_hardiness_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_honor_hardiness_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->GetEffect(EFFECT_1) != nullptr;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                absorbAmount = 0;

                if (GetTarget()->HealthAbovePct(GetEffect(EFFECT_1)->GetAmount()))
                    dmgInfo.AbsorbDamage(CalculatePct(dmgInfo.GetDamage(), aurEff->GetAmount()));
            }

            void Register() override
            {
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_honor_hardiness_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_honor_hardiness_AuraScript();
        }
};

// 42292 - PvP Trinket
// 195710 - Honorable Medallion
// 208683 - Gladiator's Medallion
class spell_legion_honor_medallion : public SpellScriptLoader
{
    public:
        spell_legion_honor_medallion() : SpellScriptLoader("spell_legion_honor_medallion") { }

        enum MedallionDummys
        {
            SPELL_GENERIC_PVP_TRINKET_DUMMY_ALLIANCE    = 97403,
            SPELL_GENERIC_PVP_TRINKET_DUMMY_HORDE       = 97404,

            VISUAL_KIT_ACTIVE_TRINKET                   = 701
        };

        class spell_legion_honor_medallion_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_honor_medallion_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_GENERIC_PVP_TRINKET_DUMMY_ALLIANCE,
                    SPELL_GENERIC_PVP_TRINKET_DUMMY_HORDE
                });
            }

            void TriggerDummy()
            {
                if (Player* player = GetCaster()->ToPlayer())
                {
                    if (player->GetTeamId() == TEAM_ALLIANCE)
                        player->CastSpell(player, SPELL_GENERIC_PVP_TRINKET_DUMMY_ALLIANCE, true);
                    else
                        player->CastSpell(player, SPELL_GENERIC_PVP_TRINKET_DUMMY_HORDE, true);

                    // send to caster only
                    player->SendPlaySpellVisualKit(VISUAL_KIT_ACTIVE_TRINKET, 0, 0);
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_honor_medallion_SpellScript::TriggerDummy);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_honor_medallion_SpellScript();
        }
};

// 195389 - Softened Blows
class spell_legion_honor_softened_blows : public SpellScriptLoader
{
    public:
        spell_legion_honor_softened_blows() : SpellScriptLoader("spell_legion_honor_softened_blows") { }

        class spell_legion_honor_softened_blows_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_honor_softened_blows_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->GetEffect(EFFECT_1) != nullptr;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                absorbAmount = 0;

                if (dmgInfo.GetDamage() < GetTarget()->CountPctFromMaxHealth(aurEff->GetAmount()))
                    dmgInfo.AbsorbDamage(CalculatePct(dmgInfo.GetDamage(), GetEffect(EFFECT_1)->GetAmount()));
            }

            void Register() override
            {
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_honor_softened_blows_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_honor_softened_blows_AuraScript();
        }
};

// 195425, 232045, 232245 - Sparring
class spell_legion_honor_sparring : public SpellScriptLoader
{
    public:
        spell_legion_honor_sparring() : SpellScriptLoader("spell_legion_honor_sparring") { }

        class spell_legion_honor_sparring_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_honor_sparring_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->GetEffect(EFFECT_1);
            }

            void Absorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                absorbAmount = 0;

                if (roll_chance_i(aurEff->GetAmount()))
                    dmgInfo.AbsorbDamage(CalculatePct(dmgInfo.GetDamage(), GetEffect(EFFECT_1)->GetAmount()));
            }

            void Register() override
            {
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_honor_sparring_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_honor_sparring_AuraScript();
        }
};

// 195637, 213542, 213543, 213545, 213548, 213551, 213554 - Train of Thought
class spell_legion_honor_train_of_thought : public SpellScriptLoader
{
    public:
        spell_legion_honor_train_of_thought() : SpellScriptLoader("spell_legion_honor_train_of_thought") { }

        class spell_legion_honor_train_of_thought_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_honor_train_of_thought_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage();
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_honor_train_of_thought_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_honor_train_of_thought_AuraScript();
        }
};

void AddSC_legion_honor_spell_scripts()
{
    new spell_legion_honor_adaptation();
    new spell_legion_honor_defender_of_the_weak();
    new spell_legion_honor_hardiness();
    new spell_legion_honor_medallion();
    new spell_legion_honor_softened_blows();
    new spell_legion_honor_sparring();
    new spell_legion_honor_train_of_thought();
}
