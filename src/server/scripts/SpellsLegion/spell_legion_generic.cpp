/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_GENERIC which cannot be included in AI script file
 * of creature using it or can't be bound to any player class.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_gen_"
 */

#include "Object.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "SpellAuras.h"
#include "SpellAuraEffects.h"
#include "SpellScript.h"
#include "World.h"

enum Spells
{
    SPELL_MAGE_FLAME_ORBS   = 226755,

    // Night elf racial
    SPELL_RACIAL_TOUCH_OF_ELUNE_NIGHT = 154797,
    SPELL_RACIAL_TOUCH_OF_ELUNE_DAY   = 154796
};

// 197886 - 7.0 Artifacts - All Weapons - General Weapon Equipped Passive (CSA)
class spell_gen_legion_all_artifact_weapons_aura : public SpellScriptLoader
{
    public:
        spell_gen_legion_all_artifact_weapons_aura() : SpellScriptLoader("spell_gen_legion_all_artifact_weapons_aura") { }

        class spell_gen_legion_all_artifact_weapons_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_gen_legion_all_artifact_weapons_aura_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_MAGE_FLAME_ORBS
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Player* target = GetUnitOwner()->ToPlayer())
                {
                    switch (target->GetUInt32Value(PLAYER_FIELD_CURRENT_SPEC_ID))
                    {
                        case TALENT_SPEC_MAGE_FIRE:
                            target->CastSpell(target, SPELL_MAGE_FLAME_ORBS, true);
                            break;
                        default:
                            break;
                    }
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_MAGE_FLAME_ORBS);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_gen_legion_all_artifact_weapons_aura_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_gen_legion_all_artifact_weapons_aura_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_gen_legion_all_artifact_weapons_aura_AuraScript();
        }
};

// Generic Spell for Auras that should remove dots on apply
// 118 - Polymorph
// 2094 - Blind
// 31661 - Dragon's Breath
// 105421 - Blinding Light
class spell_gen_legion_cc_remove_dots : public SpellScriptLoader
{
    public:
        spell_gen_legion_cc_remove_dots() : SpellScriptLoader("spell_gen_legion_cc_remove_dots") { }
    
        class spell_gen_legion_cc_remove_dots_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_gen_legion_cc_remove_dots_SpellScript);

            void RemoveDots()
            {
                GetHitUnit()->RemoveOwnedAuras([](Aura const* aura)
                {
                    SpellInfo const* spellInfo = aura->GetSpellInfo();
                    return spellInfo->HasAura(DIFFICULTY_NONE, SPELL_AURA_PERIODIC_DAMAGE) || spellInfo->HasAura(DIFFICULTY_NONE, SPELL_AURA_PERIODIC_DAMAGE);
                });
            }
    
            void Register() override
            {
                OnHit += SpellHitFn(spell_gen_legion_cc_remove_dots_SpellScript::RemoveDots);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_gen_legion_cc_remove_dots_SpellScript();
        }
};

// 154748 - Touch of Elune
class spell_gen_legion_racial_touch_of_elune : public SpellScriptLoader
{
    public:
        spell_gen_legion_racial_touch_of_elune() : SpellScriptLoader("spell_gen_legion_racial_touch_of_elune") { }

        class spell_gen_legion_racial_touch_of_elune_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_gen_legion_racial_touch_of_elune_AuraScript);

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 60 * IN_MILLISECONDS;
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                Unit* target = GetTarget();

                if (sWorld->IsDayTime() && !target->HasAura(SPELL_RACIAL_TOUCH_OF_ELUNE_DAY))
                    target->CastSpell(target, SPELL_RACIAL_TOUCH_OF_ELUNE_DAY, true);
                else if (!target->HasAura(SPELL_RACIAL_TOUCH_OF_ELUNE_NIGHT))
                    target->CastSpell(target, SPELL_RACIAL_TOUCH_OF_ELUNE_NIGHT, true);
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_gen_legion_racial_touch_of_elune_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_gen_legion_racial_touch_of_elune_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_gen_legion_racial_touch_of_elune_AuraScript();
        }
};

void AddSC_legion_generic_spell_scripts()
{
    new spell_gen_legion_all_artifact_weapons_aura();
    new spell_gen_legion_cc_remove_dots();
    new spell_gen_legion_racial_touch_of_elune();
}
