/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_DRUID and SPELLFAMILY_GENERIC spells used by druid players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_dru_legion".
 */


#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "Cell.h"
#include "CellImpl.h"
#include "Common.h"
#include "Containers.h"
#include "DB2Stores.h"
#include "GridNotifiers.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "Group.h"
#include "MoveSplineInitArgs.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"
#include <G3D/Vector3.h>

enum Spells
{
    SPELL_DRUID_ABUNDANCE                       = 207383,
    SPELL_DRUID_ABUNDANCE_EFFECT                = 207640,
    SPELL_DRUID_ADAPTIVE_FUR_ARCANE             = 200940,
    SPELL_DRUID_ADAPTIVE_FUR_FIRE               = 200944,
    SPELL_DRUID_ADAPTIVE_FUR_FROST              = 200942,
    SPELL_DRUID_ADAPTIVE_FUR_HOLY               = 200945,
    SPELL_DRUID_ADAPTIVE_FUR_NATURE             = 200943,
    SPELL_DRUID_ADAPTIVE_FUR_SHADOW             = 200941,
    SPELL_DRUID_AQUATIC_FORM_PASSIVE            = 5421,
    SPELL_DRUID_ASHAMANE_S_ENERGY_BUFF          = 210583,
    SPELL_DRUID_ASHAMANES_FRENZY                = 210723,
    SPELL_DRUID_ASHAMANES_RIP                   = 210705,
    SPELL_DRUID_BARKSKIN                        = 22812,
    SPELL_DRUID_BEAR_FORM                       = 5487,
    SPELL_DRUID_BLESSING_OF_AN_SHE              = 202739,
    SPELL_DRUID_BLESSING_OF_ELUNE               = 202737,
    SPELL_DRUID_BLOOD_FRENZY                    = 203962,
    SPELL_DRUID_BLOOD_FRENZY_ENERGIZE           = 203961,
    SPELL_DRUID_BLOODTALONS                     = 155672,
    SPELL_DRUID_BLOODTALONS_TRIGGERED           = 145152,
    SPELL_DRUID_BRAMBLES_AOE_DAMAGE             = 213709,
    SPELL_DRUID_BRAMBLES                        = 203953,
    SPELL_DRUID_BRAMBLES_DAMAGE                 = 203958,
    SPELL_DRUID_BRISTLING_FUR_ENERGIZE          = 204031,
    SPELL_DRUID_BRUTAL_SLASH                    = 202028,
    SPELL_DRUID_CAT_FORM                        = 768,
    SPELL_DRUID_CELESTIAL_ALIGNMENT             = 194223,
    SPELL_DRUID_CELESTIAL_DOWNPOUR              = 200726,
    SPELL_DRUID_CELESTIAL_GUARDIAN              = 233754,
    SPELL_DRUID_CELESTIAL_GUARDIAN_BONUS        = 234081,
    SPELL_DRUID_CHOSEN_OF_ELUNE                 = 102560,
    SPELL_DRUID_COMBO_POINT                     = 34071,
    SPELL_DRUID_CRESCENT_BURN                   = 200567,
    SPELL_DRUID_CRESCENT_BURN_HONOR_TALENT      = 200567,
    SPELL_DRUID_CULTIVATION                     = 200390,
    SPELL_DRUID_CULTIVATION_HEAL                = 200389,
    SPELL_DRUID_DASH                            = 1850,
    SPELL_DRUID_DEEP_ROOTS                      = 233755,
    SPELL_DRUID_DREAMWALKER_HEAL                = 189853,
    SPELL_DRUID_DREAMWALKER_PROC_AURA           = 189849,
    SPELL_DRUID_DYING_STARS                     = 232546,
    SPELL_DRUID_EARTHWARDEN                     = 203974,
    SPELL_DRUID_EARTHWARDEN_ABSORB              = 203975,
    SPELL_DRUID_ECHOING_STARS_DAMAGE            = 226104,
    SPELL_DRUID_ECHOING_STARS_TRAIT             = 214508,
    SPELL_DRUID_ECHOING_STARS_VISUAL            = 214519,
    SPELL_DRUID_ECLIPSE_AREATRIGGER             = 233756,
    SPELL_DRUID_ECLIPSE_HONOR_TALENT            = 233750,
    SPELL_DRUID_EFFLORESCENCE_HEAL              = 81269,
    SPELL_DRUID_EFFLORESCENCE_PERIODIC          = 81262,
    SPELL_DRUID_EFFLORESCENCE_SUMMON            = 145205,
    SPELL_DRUID_EMBRACE_OF_THE_NIGHTMARE        = 200855,
    SPELL_DRUID_ENCROACHING_VINES               = 200931,
    SPELL_DRUID_ENCROACHING_VINES_DEBUFF        = 200947,
    SPELL_DRUID_FERAL_AFFINITY                  = 202155,
    SPELL_DRUID_FERAL_INSTINCT                  = 210649,
    SPELL_DRUID_FEROCIOUS_BITE_RIP              = 231056,
    SPELL_DRUID_FEROCIOUS_WOUND                 = 236021,
    SPELL_DRUID_FEROCIOUS_WOUND_HONOR_TALENT    = 236020,
    SPELL_DRUID_FOCUSED_GROWTH                  = 203553,
    SPELL_DRUID_FOCUSED_GROWTH_BONUS            = 203554,
    SPELL_DRUID_FORCE_OF_NATURE_TAUNT           = 205644,
    SPELL_DRUID_FREEDOM_OF_THE_HERD_HONOR_TALENT= 213200,
    SPELL_DRUID_FURY_OF_ELUNE_DAMAGE            = 211545,
    SPELL_DRUID_FURY_OF_ELUNE_SUMMON            = 202770,
    SPELL_DRUID_GALACTIC_GUARDIAN_BONUS         = 213708,
    SPELL_DRUID_GERMINATION                     = 155675,
    SPELL_DRUID_GERMINATION_HEAL                = 155777,
    SPELL_DRUID_GORE_MANGLE_BONUS               = 93622,
    SPELL_DRUID_GUARDIAN_OF_ELUNE_BONUS         = 213680,
    SPELL_DRUID_INCARNATION                     = 117679,
    SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE     = 102560,
    SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE_VIS = 184747,
    SPELL_DRUID_KILLER_INSTINCT_DUMMY           = 108299,
    SPELL_DRUID_KING_OF_THE_JUNGLE              = 102543,
    SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS  = 203059,
    SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_TALENT = 203052,
    SPELL_DRUID_LIFEBLOOM                       = 33763,
    SPELL_DRUID_LIFEBLOOM_FINAL_HEAL            = 33778,
    SPELL_DRUID_LIGHT_OF_THE_SUN_TRAIT          = 202918,
    SPELL_DRUID_LIVING_SEED_HEAL                = 48503,
    SPELL_DRUID_LIVING_SEED_TRIGGERED           = 48504,
    SPELL_DRUID_LUNAR_BEAM_DAMAGE               = 204069,
    SPELL_DRUID_LUNAR_EMPOWERMENT               = 164547,
    SPELL_DRUID_LUNAR_STRIKE                    = 194153,
    SPELL_DRUID_LUNAR_STRIKE_RESTO              = 197628,
    SPELL_DRUID_MAIM_1_CP                       = 168877,
    SPELL_DRUID_MAIM_2_CP                       = 168878,
    SPELL_DRUID_MAIM_3_CP                       = 168879,
    SPELL_DRUID_MAIM_4_CP                       = 168880,
    SPELL_DRUID_MAIM_5_CP                       = 168881,
    SPELL_DRUID_MANGLE_BEAR                     = 33917,
    SPELL_DRUID_MANGLE_BLEED_BONUS              = 231064,
    SPELL_DRUID_MARK_OF_SHIFTING_HEAL           = 224392,
    SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC       = 186370,
    SPELL_DRUID_MARK_OF_SHIFTING_TRAIT          = 186372,
    SPELL_DRUID_MASTER_SHAPESHIFTER_BALANCE     = 236187,
    SPELL_DRUID_MASTER_SHAPESHIFTER_FERAL       = 236188,
    SPELL_DRUID_MASTER_SHAPESHIFTER_HONOR_TALENT= 236144,
    SPELL_DRUID_MASTER_SHAPESHIFTER_RESTO       = 236185,
    SPELL_DRUID_MOON_AND_STARS_DPS_PROC         = 202941,
    SPELL_DRUID_MOON_AND_STARS_TRAIT            = 202940,
    SPELL_DRUID_MOONFIRE                        = 8921,
    SPELL_DRUID_MOONFIRE_DOT                    = 164812,
    SPELL_DRUID_MOONFIRE_FERAL                  = 155625,
    SPELL_DRUID_MOONKIN_FORM                    = 24858,
    SPELL_DRUID_NATURES_BALANCE                 = 202430,
    SPELL_DRUID_NATURES_ESSENCE_HEAL            = 189800,
    SPELL_DRUID_NATURES_ESSENCE_TRAIT           = 189787,
    SPELL_DRUID_NOURISH                         = 203374,
    SPELL_DRUID_OVERGROWTH_AURA                 = 203652,
    SPELL_DRUID_OWLKIN_FRENZY                   = 157228,
    SPELL_DRUID_POWER_OF_THE_ARCHDRUID          = 189870,
    SPELL_DRUID_POWER_OF_THE_ARCHDRUID_AURA     = 189877,
    SPELL_DRUID_POWER_OF_THE_ARCHDRUID_EFF      = 213121,
    SPELL_DRUID_PREDATOR                        = 202021,
    SPELL_DRUID_PREDATORY_SWIFTNESS             = 69369,
    SPELL_DRUID_PREDATORY_SWIFTNESS_PASSIVE     = 16974,
    SPELL_DRUID_PRICKLING_THORNS                = 200549,
    SPELL_DRUID_PRICKLING_THORNS_DAMAGE         = 200550,
    SPELL_DRUID_PROTECTION_OF_ASHAMANE_BLOCKER  = 214274,
    SPELL_DRUID_PROTECTION_OF_ASHAMANE_BONUS    = 210655,
    SPELL_DRUID_PROTECTION_OF_ASHAMANE_TRAIT    = 210650,
    SPELL_DRUID_PROTECTOR_OF_THE_GROVE          = 209730,
    SPELL_DRUID_PROTECTOR_OF_THE_GROVE_BONUS    = 209731,
    SPELL_DRUID_PULVERIZE_BONUS                 = 158792,
    SPELL_DRUID_RAGE_OF_THE_SLEEPER_DAMAGE      = 219432,
    SPELL_DRUID_RAGING_FRENZY_ENERGIZE          = 236665,
    SPELL_DRUID_RAGING_FRENZY_HONOR_TALENT      = 236153,
    SPELL_DRUID_RAKE                            = 1822,
    SPELL_DRUID_RAKE_PERIODIC                   = 155722,
    SPELL_DRUID_RAKE_STEALTH_BONUS              = 231052,
    SPELL_DRUID_RAKE_STUN                       = 163505,
    SPELL_DRUID_REGROWTH                        = 8936,
    SPELL_DRUID_REJUVENATION                    = 774,
    SPELL_DRUID_RESTORATION_AFFINITY            = 197492,
    SPELL_DRUID_REVITALIZE_HEAL                 = 203406,
    SPELL_DRUID_REVITALIZE_PROC_AURA            = 203407,
    SPELL_DRUID_REVITALIZE_TALENT               = 203399,
    SPELL_DRUID_RIP                             = 1079,
    SPELL_DRUID_ROAR_OF_THE_CROWD_BONUS         = 214998,
    SPELL_DRUID_ROAR_OF_THE_CROWD_TRAIT         = 214996,
    SPELL_DRUID_SABERTOOTH                      = 202031,
    SPELL_DRUID_SAVAGE_MOMENTUM                 = 205673,
    SPELL_DRUID_SAVAGE_ROAR                     = 62071,
    SPELL_DRUID_SCENT_OF_BLOOD                  = 210663,
    SPELL_DRUID_SCENT_OF_BLOOD_BONUS            = 210664,
    SPELL_DRUID_SHADOW_THRASH_DAMAGE            = 210687,
    SPELL_DRUID_SHOOTING_STARS_DAMAGE           = 202497,
    SPELL_DRUID_SHRED                           = 5221,
    SPELL_DRUID_SHRED_BLEEDING_BONUS            = 231063,
    SPELL_DRUID_SHRED_STEALTH_BONUS             = 231057,
    SPELL_DRUID_SOLAR_BEAM_SUMMON               = 78675,
    SPELL_DRUID_SOLAR_EMPOWERMENT               = 164545,
    SPELL_DRUID_SOLAR_WRATH                     = 190984,
    SPELL_DRUID_SOLAR_WRATH_TALENTED            = 5176,
    SPELL_DRUID_SOLAR_WRATH_GUARDIAN            = 197629,
    SPELL_DRUID_SOUL_OF_THE_FOREST              = 158476,
    SPELL_DRUID_SOUL_OF_THE_FOREST_ENERGIZE     = 114113,
    SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD     = 114108,
    SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO        = 158478,
    SPELL_DRUID_SOUND_EFFECT_STAG_FORM          = 126056,
    SPELL_DRUID_SOUND_EFFECT_TRAVEL_FORM        = 126044,
    SPELL_DRUID_SPRING_BLOSSOMS                 = 207385,
    SPELL_DRUID_SPRING_BLOSSOMS_HEAL            = 207386,
    SPELL_DRUID_STAG_FORM                       = 115034,
    SPELL_DRUID_STAR_POWER                      = 202942,
    SPELL_DRUID_STARFALL_SUMMON                 = 191034,
    SPELL_DRUID_STARLORD                        = 202345,
    SPELL_DRUID_STARLORD_LUNAR_BONUS            = 202423,
    SPELL_DRUID_STARLORD_SOLAR_BONUS            = 202416,
    SPELL_DRUID_STARTFALL_DAMAGE                = 191037,
    SPELL_DRUID_STELLAR_BONUS                   = 231049,
    SPELL_DRUID_STELLAR_DRIFT                   = 202354,
    SPELL_DRUID_STELLAR_DRIFT_BONUS             = 202461,
    SPELL_DRUID_STELLAR_EMPOWERMENT             = 197637,
    SPELL_DRUID_SUNFIRE_DOT                     = 164815,
    SPELL_DRUID_SUNSET_ENERGIZE                 = 232549,
    SPELL_DRUID_SURVIVAL_INSTINCTS_BUFF         = 50322,
    SPELL_DRUID_SWIPE_BONUS_FERAL               = 231283,
    SPELL_DRUID_SWIPE_FERAL_SPEC                = 106785,
    SPELL_DRUID_THRASH_DOT                      = 192090,
    SPELL_DRUID_THRASH_PULVERIZE_MARKER         = 158790,
    SPELL_DRUID_TIGERS_FURY                     = 5217,
    SPELL_DRUID_TOOTH_AND_CLAW_BONUS            = 236440,
    SPELL_DRUID_TOOTH_AND_CLAW_HONOR_TALENT     = 236019,
    SPELL_DRUID_TRANQUILITY                     = 740,
    SPELL_DRUID_TRAVEL_FORM                     = 165961,
    SPELL_DRUID_TRAVEL_FORM_AQUATIC_FORM        = 1066,
    SPELL_DRUID_TRAVEL_FORM_BONUS               = 159456,
    SPELL_DRUID_TRAVEL_FORM_CONTROLLER          = 783,
    SPELL_DRUID_TRAVEL_FORM_FLIGHT_FORM         = 33943,
    SPELL_DRUID_TRAVEL_FORM_PASSIVE             = 5419,
    SPELL_DRUID_TRAVEL_FORM_SWIFT_FLIGHT_FORM   = 40120,
    SPELL_DRUID_TREE_OF_LIFE                    = 33891,
    SPELL_DRUID_TREE_OF_LIFE_CAST_BONUS         = 81097,
    SPELL_DRUID_TREE_OF_LIFE_MOD_BONUS          = 5420,
    SPELL_DRUID_URSOLS_VORTEX_GRIP              = 118283,
    SPELL_DRUID_WARRIOR_OF_ELUNE                = 202425,
    SPELL_DRUID_WILD_GROWTH                     = 48438,
    SPELL_DRUID_WILDFLESH                       = 200400,
    SPELL_DRUID_YSERAS_GIFT_PARTY_HEAL          = 145110,
    SPELL_DRUID_YSERAS_GIFT_SELF_HEAL           = 145109,
    SPELL_HOTFIX_COMBO_POINT_HOLDER             = 300001,
    SPELL_DRUID_OPEN_WOUNDS                     = 210666,
    SPELL_DRUID_OPEN_WOUNDS_DEBUFF              = 210670,
};

enum miscSpells
{
    SPELL_MISC_EXPERT_RIDING                = 34090,
    SPELL_MISC_ARTISAN_RIDING               = 34091
};

enum MiscData
{
    SUNFIRE_MAX_DURATION    = 24000,
    MOONFIRE_MAX_DURATION   = 29000,
    EFFLORESECENCE_TOTEM    = 47649,
    ZONE_MOONGLADE          = 493,
    ASHAMANES_FRENZY_VISUAL = 55457
};

static const uint32 MaimSpell[5] = { SPELL_DRUID_MAIM_1_CP, SPELL_DRUID_MAIM_2_CP, SPELL_DRUID_MAIM_3_CP, SPELL_DRUID_MAIM_4_CP, SPELL_DRUID_MAIM_5_CP };
static const uint32 RestoHotHelper[4] = { SPELL_DRUID_REJUVENATION, SPELL_DRUID_LIFEBLOOM, SPELL_DRUID_WILD_GROWTH, SPELL_DRUID_REGROWTH };

static std::unordered_map<uint32 /* schoolmask */, uint32 /* triggerSpell */> const adaptiveFurHelper =
{
    { SPELL_SCHOOL_MASK_ARCANE,  SPELL_DRUID_ADAPTIVE_FUR_ARCANE },
    { SPELL_SCHOOL_MASK_SHADOW,  SPELL_DRUID_ADAPTIVE_FUR_SHADOW },
    { SPELL_SCHOOL_MASK_FROST,   SPELL_DRUID_ADAPTIVE_FUR_FROST  },
    { SPELL_SCHOOL_MASK_NATURE,  SPELL_DRUID_ADAPTIVE_FUR_NATURE },
    { SPELL_SCHOOL_MASK_FIRE,    SPELL_DRUID_ADAPTIVE_FUR_FIRE   },
    { SPELL_SCHOOL_MASK_HOLY,    SPELL_DRUID_ADAPTIVE_FUR_HOLY   }
};

// 29166 - Innervate
class spell_dru_legion_innervate : public SpellScriptLoader
{
    public:
        spell_dru_legion_innervate() : SpellScriptLoader("spell_dru_legion_innervate") { }

        class spell_dru_legion_innervate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_innervate_SpellScript);

            SpellCastResult CheckIfHealerTarget()
            {
                if (!GetExplTargetUnit() || GetExplTargetUnit()->GetTypeId() != TYPEID_PLAYER || GetExplTargetUnit()->ToPlayer()->GetRole() != TALENT_ROLE_HEALER)
                    return SPELL_FAILED_BAD_TARGETS;

                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_dru_legion_innervate_SpellScript::CheckIfHealerTarget);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_innervate_SpellScript();
        }
};

// 48484 - Infected Wounds
class spell_dru_legion_infected_wounds : public SpellScriptLoader
{
    public:
        spell_dru_legion_infected_wounds() : SpellScriptLoader("spell_dru_legion_infected_wounds") { }

        class spell_dru_legion_infected_wounds_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_infected_wounds_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_RAKE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo() || !GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget()))
                    return false;

                // SpellFamMask is unsed on multiple spells
                return eventInfo.GetSpellInfo()->Id == SPELL_DRUID_RAKE;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_infected_wounds_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_infected_wounds_AuraScript();
        }
};

// 192081 - Ironfur
class spell_dru_legion_ironfur : public SpellScriptLoader
{
    public:
        spell_dru_legion_ironfur() : SpellScriptLoader("spell_dru_legion_ironfur") { }

        class spell_dru_legion_ironfur_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_ironfur_AuraScript);

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (stackRemoveTime.size() >= GetSpellInfo()->StackAmount)
                    stackRemoveTime.pop();

                stackRemoveTime.push(time(nullptr) + (GetAura()->GetMaxDuration() / 1000));
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 250;
            }

            void HandleCustomTick(AuraEffect const* /*aurEff*/)
            {
                if (!stackRemoveTime.empty())
                {
                    if (stackRemoveTime.front() <= time(nullptr))
                    {
                        stackRemoveTime.pop();
                        GetTarget()->RemoveAuraFromStack(GetId());
                    }
                }
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_dru_legion_ironfur_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_BASE_RESISTANCE_PCT, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_dru_legion_ironfur_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_MOD_BASE_RESISTANCE_PCT);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_ironfur_AuraScript::HandleCustomTick, EFFECT_0, SPELL_AURA_MOD_BASE_RESISTANCE_PCT);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_dru_legion_ironfur_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_MOD_BASE_RESISTANCE_PCT);
            }

            private:
                std::queue<time_t> stackRemoveTime;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_ironfur_AuraScript();
        }
};

// 108300 - Killer Instinct
class spell_dru_legion_killer_instinct : public SpellScriptLoader
{
    public:
        spell_dru_legion_killer_instinct() : SpellScriptLoader("spell_dru_legion_killer_instinct") { }

        class spell_dru_legion_killer_instinct_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_killer_instinct_SpellScript);

            SpellCastResult CheckCast()
            {
                /// @todo: remove hacky spell link (there is a tc commit for shapeshift boost improvement)
                Unit* caster = GetCaster();
                if (!caster->HasAura(SPELL_DRUID_KILLER_INSTINCT_DUMMY))
                    return SPELL_FAILED_DONT_REPORT;
                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_dru_legion_killer_instinct_SpellScript::CheckCast);
            };
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_killer_instinct_SpellScript();
        }

        class spell_dru_legion_killer_instinct_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_killer_instinct_AuraScript);

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                amount = 0;
                canBeRecalculated = false;

                if (!GetUnitOwner()->IsInFeralForm())
                    return;

                if (AuraEffect const* dummyEff = GetUnitOwner()->GetAuraEffect(SPELL_DRUID_KILLER_INSTINCT_DUMMY, EFFECT_0))
                    amount = CalculatePct(GetUnitOwner()->GetStat(STAT_INTELLECT), dummyEff->GetAmount());
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dru_legion_killer_instinct_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_MOD_STAT);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_killer_instinct_AuraScript();
        }
};

// 203052 - King of the Jungle
class spell_dru_legion_king_of_the_jungle : public SpellScriptLoader
{
    public:
        spell_dru_legion_king_of_the_jungle() : SpellScriptLoader("spell_dru_legion_king_of_the_jungle") { }

        class spell_dru_legion_king_of_the_jungle_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_king_of_the_jungle_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS
                });
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_king_of_the_jungle_AuraScript::AfterRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_king_of_the_jungle_AuraScript();
        }
};

// 8921 - Moonfire
// 93402 - Sunfire (Level 110)
class spell_dru_legion_moonfire_sunfire_initial : public SpellScriptLoader
{
    public:
        spell_dru_legion_moonfire_sunfire_initial() : SpellScriptLoader("spell_dru_legion_moonfire_sunfire_initial") { }

        class spell_dru_legion_moonfire_sunfire_initial_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_moonfire_sunfire_initial_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MOONFIRE,
                    SPELL_DRUID_MOONFIRE_DOT,
                    SPELL_DRUID_SUNFIRE_DOT
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), GetSpellInfo()->Id == SPELL_DRUID_MOONFIRE ? SPELL_DRUID_MOONFIRE_DOT : SPELL_DRUID_SUNFIRE_DOT, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_moonfire_sunfire_initial_SpellScript::HandleDummy, EFFECT_FIRST_FOUND, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_moonfire_sunfire_initial_SpellScript();
        }
};

// 203728 - Thorns
class spell_dru_legion_thorns_damage : public SpellScriptLoader
{
    public:
        spell_dru_legion_thorns_damage() : SpellScriptLoader("spell_dru_legion_thorns_damage") { }

        class spell_dru_legion_thorns_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_thorns_damage_SpellScript);

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (Unit* target = GetHitUnit())
                    SetHitDamage(int32(target->CountPctFromMaxHealth(GetEffectValue())));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_thorns_damage_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_thorns_damage_SpellScript();
        }
};

// 5185 - Healing Touch
class spell_dru_legion_healing_touch : public SpellScriptLoader
{
    public:
        spell_dru_legion_healing_touch() : SpellScriptLoader("spell_dru_legion_healing_touch") { }

        class spell_dru_legion_healing_touch_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_healing_touch_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_NOURISH
                });
            }

            SpellCastResult CheckIfLanceEquiped()
            {
                // For some reason bear form isn't removed on healing touch cast
                if (GetCaster()->IsInFeralForm())
                    GetCaster()->RemoveAurasByType(SPELL_AURA_MOD_SHAPESHIFT);
                return SPELL_CAST_OK;
            }

            void HandleHonorTalent(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_DRUID_NOURISH))
                    for (uint8 i = 0; i < 4; i++)
                        if (!GetHitUnit()->HasAura(RestoHotHelper[i], GetCaster()->GetGUID()))
                        {
                            GetCaster()->CastSpell(GetHitUnit(), RestoHotHelper[i], true);
                            break;
                        }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_dru_legion_healing_touch_SpellScript::CheckIfLanceEquiped);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_healing_touch_SpellScript::HandleHonorTalent, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_healing_touch_SpellScript();
        }
};

// 209690 - Druid of the Claw
class spell_dru_legion_druid_of_the_claw : public SpellScriptLoader
{
    public:
        spell_dru_legion_druid_of_the_claw() : SpellScriptLoader("spell_dru_legion_druid_of_the_claw") { }

        class spell_dru_legion_druid_of_the_claw_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_druid_of_the_claw_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_REJUVENATION
                });
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_REJUVENATION, true);
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_dru_legion_druid_of_the_claw_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_druid_of_the_claw_AuraScript();
        }
};

// 203651 - Overgrowth
class spell_dru_legion_overgrowth : public SpellScriptLoader
{
    public:
        spell_dru_legion_overgrowth() : SpellScriptLoader("spell_dru_legion_overgrowth") { }

        class spell_dru_legion_overgrowth_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_overgrowth_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_OVERGROWTH_AURA,
                    SPELL_DRUID_REJUVENATION,
                    SPELL_DRUID_LIFEBLOOM,
                    SPELL_DRUID_WILD_GROWTH,
                    SPELL_DRUID_REGROWTH
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* target = GetHitUnit())
                {
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_OVERGROWTH_AURA, true);
                    GetCaster()->CastSpell(target, SPELL_DRUID_REJUVENATION, true);
                    GetCaster()->CastSpell(target, SPELL_DRUID_LIFEBLOOM, true);
                    GetCaster()->CastSpell(target, SPELL_DRUID_WILD_GROWTH, true);
                    GetCaster()->CastSpell(target, SPELL_DRUID_REGROWTH, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_overgrowth_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_overgrowth_SpellScript();
        }
};

// 164812 - Moonfire Damage
class spell_dru_legion_moonfire : public SpellScriptLoader
{
    public:
        spell_dru_legion_moonfire() : SpellScriptLoader("spell_dru_legion_moonfire") { }

        class spell_dru_legion_moonfire_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_moonfire_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_CRESCENT_BURN
                });
            }

            void CheckIfAuraIsActive(SpellEffIndex /*effIndex*/)
            {
                moonfireActive = GetHitUnit()->HasAura(GetSpellInfo()->Id, GetCaster()->GetGUID());
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (!moonfireActive)
                    return;

                if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_DRUID_CRESCENT_BURN, EFFECT_0))
                {
                    int32 damage = GetHitDamage() + CalculatePct(GetHitDamage(), aurEff->GetAmount());
                    SetHitDamage(damage);
                }
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_dru_legion_moonfire_SpellScript::CheckIfAuraIsActive, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_moonfire_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

            private:
                bool moonfireActive = false;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_moonfire_SpellScript();
        }

        class spell_dru_legion_moonfire_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_moonfire_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({
                    SPELL_DRUID_DYING_STARS,
                    SPELL_DRUID_SUNSET_ENERGIZE
                });
            }

            void HandleHonorTalent(DispelInfo* /*dispelData*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_DRUID_DYING_STARS))
                        caster->CastSpell(caster, SPELL_DRUID_SUNSET_ENERGIZE, true);
            }

            void Register() override
            {
                OnDispel += AuraDispelFn(spell_dru_legion_moonfire_AuraScript::HandleHonorTalent);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_moonfire_AuraScript();
        }
};

// 164815 - Sunfire
class spell_dru_legion_sunfire : public SpellScriptLoader
{
    public:
        spell_dru_legion_sunfire() : SpellScriptLoader("spell_dru_legion_sunfire") { }

        class spell_dru_legion_sunfire_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_sunfire_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({
                    SPELL_DRUID_DYING_STARS,
                    SPELL_DRUID_SUNSET_ENERGIZE
                });
            }

            void HandleHonorTalent(DispelInfo* /*dispelData*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_DRUID_DYING_STARS))
                        caster->CastSpell(caster, SPELL_DRUID_SUNSET_ENERGIZE, true);
            }

            void Register() override
            {
                OnDispel += AuraDispelFn(spell_dru_legion_sunfire_AuraScript::HandleHonorTalent);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_sunfire_AuraScript();
        }
};

// 202996 - Power of Goldrinn
class spell_dru_legion_power_of_goldrinn : public SpellScriptLoader
{
    public:
        spell_dru_legion_power_of_goldrinn() : SpellScriptLoader("spell_dru_legion_power_of_goldrinn") { }

        class spell_dru_legion_power_of_goldrinn_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_power_of_goldrinn_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // sometimes power of goldrinn procs on caster itself (impossible!?)
                return !GetTarget()->IsFriendlyTo(eventInfo.GetActionTarget());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_power_of_goldrinn_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_power_of_goldrinn_AuraScript();
        }
};

// 102280 - Displacer Beast
class spell_dru_legion_displacer_beast : public SpellScriptLoader
{
    public:
        spell_dru_legion_displacer_beast() : SpellScriptLoader("spell_dru_legion_displacer_beast") { }

        class spell_dru_legion_displacer_beast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_displacer_beast_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_CAT_FORM
                });
            }

            void SetDest(SpellDestination& dest)
            {
                Position pos = GetCaster()->GetFirstCollisionPosition(GetSpellInfo()->GetEffect(EFFECT_0)->CalcRadius(GetCaster()), 0.00f);
                dest.Relocate(pos);
            }

            void ActivateCatForm()
            {
                if (GetCaster()->GetShapeshiftForm() != FORM_CAT_FORM)
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_CAT_FORM, true);
                else
                    GetCaster()->RemoveMovementImpairingAuras();
            }

            void Register() override
            {
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_dru_legion_displacer_beast_SpellScript::SetDest, EFFECT_0, TARGET_DEST_CASTER_FRONT);
                AfterCast += SpellCastFn(spell_dru_legion_displacer_beast_SpellScript::ActivateCatForm);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_displacer_beast_SpellScript();
        }
};

// 202360 - Blessing of the Ancients
class spell_dru_legion_blessing_of_the_ancients : public SpellScriptLoader
{
    public:
        spell_dru_legion_blessing_of_the_ancients() : SpellScriptLoader("spell_dru_legion_blessing_of_the_ancients") { }

        class spell_dru_legion_blessing_of_the_ancients_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_blessing_of_the_ancients_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BLESSING_OF_ELUNE,
                    SPELL_DRUID_BLESSING_OF_AN_SHE
                });
            }

            void HandleDummy()
            {
                if ((!GetCaster()->HasAura(SPELL_DRUID_BLESSING_OF_ELUNE) && !GetCaster()->HasAura(SPELL_DRUID_BLESSING_OF_AN_SHE))
                    || GetCaster()->HasAura(SPELL_DRUID_BLESSING_OF_AN_SHE))
                {
                    GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_BLESSING_OF_AN_SHE);
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_BLESSING_OF_ELUNE, true);
                }
                else
                {
                    GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_BLESSING_OF_ELUNE);
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_BLESSING_OF_AN_SHE, true);
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_blessing_of_the_ancients_SpellScript::HandleDummy);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_blessing_of_the_ancients_SpellScript();
        }
};

// 202342 - Shooting Stars
class spell_dru_legion_shooting_stars : public SpellScriptLoader
{
    public:
        spell_dru_legion_shooting_stars() : SpellScriptLoader("spell_dru_legion_shooting_stars") { }

        class spell_dru_legion_shooting_stars_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_shooting_stars_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SHOOTING_STARS_DAMAGE
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                caster->CastSpell(eventInfo.GetActionTarget(), SPELL_DRUID_SHOOTING_STARS_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_shooting_stars_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_shooting_stars_AuraScript();
        }
};

// 194153 - Lunar Strike
// 190984 - Solar Wrath
class spell_dru_legion_natures_balance : public SpellScriptLoader
{
    public:
        spell_dru_legion_natures_balance() : SpellScriptLoader("spell_dru_legion_natures_balance") { }

        class spell_dru_legion_natures_balance_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_natures_balance_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_NATURES_BALANCE,
                    SPELL_DRUID_LUNAR_STRIKE,
                    SPELL_DRUID_MOONFIRE_DOT,
                    SPELL_DRUID_SUNFIRE_DOT
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (!GetCaster()->HasAura(SPELL_DRUID_NATURES_BALANCE, GetCaster()->GetGUID()))
                    return;

                if (GetSpellInfo()->Id == SPELL_DRUID_LUNAR_STRIKE)
                {
                    if (AuraEffect* naturesBalance = GetCaster()->GetAuraEffect(SPELL_DRUID_NATURES_BALANCE, EFFECT_0, GetCaster()->GetGUID()))
                        if (Aura* moonfire = GetHitUnit()->GetAura(SPELL_DRUID_MOONFIRE_DOT, GetCaster()->GetGUID()))
                            moonfire->SetDuration(std::min(static_cast<int32>(MOONFIRE_MAX_DURATION), (moonfire->GetDuration() + (naturesBalance->GetAmount() * 2 * IN_MILLISECONDS))));
                }
                else
                {
                    if (AuraEffect* naturesBalance = GetCaster()->GetAuraEffect(SPELL_DRUID_NATURES_BALANCE, EFFECT_1, GetCaster()->GetGUID()))
                        for (auto itr : GetCaster()->GetTargetAuraApplications(SPELL_DRUID_SUNFIRE_DOT))
                            itr->GetBase()->SetDuration(std::min(static_cast<int32>(SUNFIRE_MAX_DURATION), (itr->GetBase()->GetDuration() + (naturesBalance->GetAmount() * 2 * IN_MILLISECONDS))));
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_natures_balance_SpellScript::HandleDummy, EFFECT_FIRST_FOUND, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_natures_balance_SpellScript();
        }
};

// 339 - Entangling Roots
class spell_dru_legion_entangling_roots : public SpellScriptLoader
{
    public:
        spell_dru_legion_entangling_roots() : SpellScriptLoader("spell_dru_legion_entangling_roots") { }

        class spell_dru_legion_entangling_roots_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_entangling_roots_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({
                    SPELL_DRUID_ENCROACHING_VINES,
                    SPELL_DRUID_ENCROACHING_VINES_DEBUFF,
                    SPELL_DRUID_DEEP_ROOTS,
                    SPELL_DRUID_PRICKLING_THORNS,
                    SPELL_DRUID_PRICKLING_THORNS_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    return !caster->HasAura(SPELL_DRUID_DEEP_ROOTS);
                return true;
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_DRUID_ENCROACHING_VINES))
                        caster->CastSpell(GetTarget(), SPELL_DRUID_ENCROACHING_VINES_DEBUFF, true);

                    if (caster->HasAura(SPELL_DRUID_PRICKLING_THORNS))
                        caster->CastSpell(GetTarget(), SPELL_DRUID_PRICKLING_THORNS_DAMAGE, true);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_entangling_roots_AuraScript::CheckProc);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_entangling_roots_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_ROOT_2, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_entangling_roots_AuraScript();
        }
};

// 24858 - Moonkin Form
class spell_dru_legion_moonkin_form : public SpellScriptLoader
{
    public:
        spell_dru_legion_moonkin_form() : SpellScriptLoader("spell_dru_legion_moonkin_form") { }

        class spell_dru_legion_moonkin_form_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_moonkin_form_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_OWLKIN_FRENZY,
                    SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE,
                    SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE_VIS
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE_VIS, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE_VIS);
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_OWLKIN_FRENZY, true);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_moonkin_form_AuraScript::ApplyEffect, EFFECT_1, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_moonkin_form_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
                OnProc += AuraProcFn(spell_dru_legion_moonkin_form_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_moonkin_form_AuraScript();
        }
};

// 203407 - Revitalize
class spell_dru_legion_revitalize : public SpellScriptLoader
{
    public:
        spell_dru_legion_revitalize() : SpellScriptLoader("spell_dru_legion_revitalize") { }

        class spell_dru_legion_revitalize_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_revitalize_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_REVITALIZE_HEAL,
                    SPELL_DRUID_REJUVENATION,
                    SPELL_DRUID_GERMINATION_HEAL
                });
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->CastSpell(GetTarget(), SPELL_DRUID_REVITALIZE_HEAL, true);
                    if (Aura* auraRejuvenation = GetTarget()->GetAura(SPELL_DRUID_REJUVENATION, caster->GetGUID()))
                    {
                        // If Target has Rejuvenation Germination, refresh it if its shorter
                        if (Aura* germinationAura = GetTarget()->GetAura(SPELL_DRUID_GERMINATION_HEAL, caster->GetGUID()))
                            if (germinationAura->GetDuration() < auraRejuvenation->GetDuration())
                            {
                                germinationAura->RefreshDuration();
                                return;
                            }

                        auraRejuvenation->RefreshDuration();
                    }
                }
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_dru_legion_revitalize_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_revitalize_AuraScript();
        }
};

// 194153 - Lunar Strike
// 197628 - Lunar Strike
class spell_dru_legion_lunar_strike : public SpellScriptLoader
{
    public:
        spell_dru_legion_lunar_strike() : SpellScriptLoader("spell_dru_legion_lunar_strike") { }

        class spell_dru_legion_lunar_strike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_lunar_strike_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_LUNAR_STRIKE_RESTO,
                    SPELL_DRUID_WARRIOR_OF_ELUNE,
                    SPELL_DRUID_OWLKIN_FRENZY
                });
            }

            void CalculateBonusDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetExplTargetUnit()->GetGUID() != GetHitUnit()->GetGUID())
                    if (SpellEffectInfo const* effInfo = GetEffectInfo(GetSpellInfo()->Id == SPELL_DRUID_LUNAR_STRIKE_RESTO ? EFFECT_1 : EFFECT_2))
                        SetHitDamage(CalculatePct(GetHitDamage(), effInfo->CalcValue(GetCaster())));
            }

            void HandleBeforeCast()
            {
                if (!GetCaster()->HasAura(SPELL_DRUID_WARRIOR_OF_ELUNE) && GetCaster()->HasAura(SPELL_DRUID_OWLKIN_FRENZY))
                    GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_OWLKIN_FRENZY);
            }

            void Register() override
            {
                BeforeCast += SpellCastFn(spell_dru_legion_lunar_strike_SpellScript::HandleBeforeCast);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_lunar_strike_SpellScript::CalculateBonusDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_lunar_strike_SpellScript();
        }
};

class areatrigger_dru_legion_starfall : public AreaTriggerEntityScript
{
    public:
        areatrigger_dru_legion_starfall() : AreaTriggerEntityScript("areatrigger_dru_legion_starfall") { }

        struct areatrigger_dru_legion_starfallAI : public AreaTriggerAI
        {
            areatrigger_dru_legion_starfallAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->HasAura(SPELL_DRUID_CELESTIAL_DOWNPOUR))
                        for (AreaTrigger* starfallAt : at->GetCaster()->GetAreaTriggers(SPELL_DRUID_STARFALL_SUMMON))
                        {
                            if (starfallAt->GetGUID() != at->GetGUID())
                                starfallAt->Remove();
                        }
                }

                int32 periodicTick = at->GetDuration() / 9;
                scheduler.Schedule(Milliseconds(periodicTick), [this](TaskContext task)
                {
                    for (ObjectGuid const& itr : at->GetInsideUnits())
                    {
                        if (Unit* caster = at->GetCaster())
                        {
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, itr))
                                if (!target->IsFriendlyTo(caster))
                                {
                                    if (caster->HasAura(SPELL_DRUID_STELLAR_BONUS))
                                        caster->CastSpell(target, SPELL_DRUID_STELLAR_EMPOWERMENT, true);
                                    caster->CastSpell(target, SPELL_DRUID_STARTFALL_DAMAGE, true);
                                }
                        }
                    }
                    task.Repeat();
                });
            }

            void OnUnitEnter(Unit* unit) override
            {
                if (at->GetCasterGuid() == unit->GetGUID())
                    if (unit->HasAura(SPELL_DRUID_STELLAR_DRIFT))
                        unit->CastSpell(unit, SPELL_DRUID_STELLAR_DRIFT_BONUS, true);
            }

            void OnUnitExit(Unit* unit) override
            {
                if (unit->GetGUID() == at->GetCasterGuid())
                {
                    std::vector<AreaTrigger*> triggers = unit->GetAreaTriggers(at->GetSpellId());
                    for (AreaTrigger const* areatrigger : triggers)
                    {
                        if (at->GetGUID() == areatrigger->GetGUID())
                            continue;

                        if (std::find(areatrigger->GetInsideUnits().begin(), areatrigger->GetInsideUnits().end(), unit->GetGUID()) != areatrigger->GetInsideUnits().end())
                            return;
                    }

                    unit->RemoveAurasDueToSpell(SPELL_DRUID_STELLAR_DRIFT_BONUS);
                }
            }

            void OnUpdate(uint32 diff) override
            {
                scheduler.Update(diff);
            }

        private:
            TaskScheduler scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_dru_legion_starfallAI(areaTrigger);
        }
};

// 211547 - Fury of Elune
class spell_dru_legion_fury_of_elune_override : public SpellScriptLoader
{
    public:
        spell_dru_legion_fury_of_elune_override() : SpellScriptLoader("spell_dru_legion_fury_of_elune_override") { }

        class spell_dru_legion_fury_of_elune_override_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_fury_of_elune_override_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_FURY_OF_ELUNE_SUMMON
                });
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                if (!GetHitDest())
                    return;

                if (AreaTrigger* fury = GetCaster()->GetAreaTrigger(SPELL_DRUID_FURY_OF_ELUNE_SUMMON))
                {
                    Movement::PointsArray furyPoints;
                    for (uint8 i = 0; i < 4; i++)
                    {
                        G3D::Vector3 point;
                        point.x = (i < 2) ? fury->GetPositionX() : GetHitDest()->GetPositionX();
                        point.y = (i < 2) ? fury->GetPositionY() : GetHitDest()->GetPositionY();
                        point.z = (i < 2) ? fury->GetPositionZ() : GetHitDest()->GetPositionZ();
                        furyPoints.push_back(point);
                    }
                    fury->InitSplines(furyPoints, uint32((fury->GetExactDist(GetHitDest()->GetPosition()) / 6.0f) * IN_MILLISECONDS));
                }
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_dru_legion_fury_of_elune_override_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_fury_of_elune_override_SpellScript();
        }
};

// 202770 - Fury of Elune
class spell_dru_legion_fury_of_elune_periodic : public SpellScriptLoader
{
    public:
        spell_dru_legion_fury_of_elune_periodic() : SpellScriptLoader("spell_dru_legion_fury_of_elune_periodic") { }

        class spell_dru_legion_fury_of_elune_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_fury_of_elune_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_FURY_OF_ELUNE_SUMMON,
                    SPELL_DRUID_FURY_OF_ELUNE_DAMAGE
                });
            }

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                if (AreaTrigger* fury = GetTarget()->GetAreaTrigger(SPELL_DRUID_FURY_OF_ELUNE_SUMMON))
                {
                    if (aurEff->GetTickNumber() > 1)
                    {
                        if ((aurEff->GetTickNumber() % 2 == 0))
                        {
                            if (GetTarget()->GetPower(POWER_LUNAR_POWER) >= 120)
                                GetTarget()->SetPower(POWER_LUNAR_POWER, GetTarget()->GetPower(POWER_LUNAR_POWER) - 120);
                            else
                            {
                                GetAura()->Remove();
                                return;
                            }
                        }
                    }
                    GetTarget()->CastSpell(fury->GetPositionX(), fury->GetPositionY(), fury->GetPositionZ(), SPELL_DRUID_FURY_OF_ELUNE_DAMAGE, true, nullptr, aurEff);
                }
                else
                    GetAura()->Remove();
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (AreaTrigger* fury = GetTarget()->GetAreaTrigger(SPELL_DRUID_FURY_OF_ELUNE_SUMMON))
                    fury->Remove();
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_fury_of_elune_periodic_AuraScript::HandleEffectPeriodic, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_fury_of_elune_periodic_AuraScript::RemoveEffect, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_fury_of_elune_periodic_AuraScript();
        }
};

// 164545 - Solar Empowerment
// 164547 - Lunar Empowerment
class spell_dru_legion_empowerment : public SpellScriptLoader
{
    public:
        spell_dru_legion_empowerment() : SpellScriptLoader("spell_dru_legion_empowerment") { }

        class spell_dru_legion_empowerment_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_empowerment_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_STARLORD,
                    SPELL_DRUID_LUNAR_EMPOWERMENT,
                    SPELL_DRUID_STARLORD_LUNAR_BONUS,
                    SPELL_DRUID_STARLORD_SOLAR_BONUS,
                    SPELL_DRUID_LUNAR_STRIKE,
                    SPELL_DRUID_LUNAR_STRIKE_RESTO,
                    SPELL_DRUID_SOLAR_WRATH,
                    SPELL_DRUID_SOLAR_WRATH_TALENTED
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_STARLORD))
                {
                    if (GetId() == SPELL_DRUID_LUNAR_EMPOWERMENT && !GetTarget()->HasAura(SPELL_DRUID_STARLORD_LUNAR_BONUS))
                    {
                        int32 bonus = sSpellMgr->AssertSpellInfo(SPELL_DRUID_STARLORD_LUNAR_BONUS)->GetEffect(EFFECT_0)->CalcValue(GetTarget()) * 2; // if someone want to fix that on a better/correct way dont forget that solar empowerment add + 20% for a working aura tooltip...
                        GetTarget()->CastCustomSpell(GetTarget(), SPELL_DRUID_STARLORD_LUNAR_BONUS, &bonus, nullptr, nullptr, true);
                    }
                    else if (!GetTarget()->HasAura(SPELL_DRUID_STARLORD_SOLAR_BONUS))
                    {
                        int32 bonus = sSpellMgr->AssertSpellInfo(SPELL_DRUID_STARLORD_SOLAR_BONUS)->GetEffect(EFFECT_0)->CalcValue(GetTarget()) * 2;
                        GetTarget()->CastCustomSpell(GetTarget(), SPELL_DRUID_STARLORD_SOLAR_BONUS, &bonus, nullptr, nullptr, true);
                    }
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetId() == SPELL_DRUID_LUNAR_EMPOWERMENT)
                    GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_STARLORD_LUNAR_BONUS);
                else
                    GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_STARLORD_SOLAR_BONUS);
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                if (GetUnitOwner()->HasAura(SPELL_DRUID_STARLORD))
                {
                    if (GetId() == SPELL_DRUID_LUNAR_EMPOWERMENT)
                        amount = abs(sSpellMgr->AssertSpellInfo(SPELL_DRUID_STARLORD_LUNAR_BONUS)->GetEffect(EFFECT_0)->CalcValue(GetUnitOwner()));
                    else
                        amount = abs(sSpellMgr->AssertSpellInfo(SPELL_DRUID_STARLORD_SOLAR_BONUS)->GetEffect(EFFECT_0)->CalcValue(GetUnitOwner()));
                }
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                uint32 spellId = eventInfo.GetSpellInfo()->Id;
                if (GetId() == SPELL_DRUID_LUNAR_EMPOWERMENT)
                {
                    if (GetTarget()->GetSpellHistory()->HasCooldown(GetSpellInfo()))
                        return false;

                    if (eventInfo.GetSpellInfo() && (spellId == SPELL_DRUID_LUNAR_STRIKE || spellId == SPELL_DRUID_LUNAR_STRIKE_RESTO))
                    {
                        // small cooldown for split damage
                        GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(100));
                        return true;
                    }
                }

                if (GetId() == SPELL_DRUID_SOLAR_EMPOWERMENT)
                    return (eventInfo.GetSpellInfo() && (spellId == SPELL_DRUID_SOLAR_WRATH || spellId == SPELL_DRUID_SOLAR_WRATH_TALENTED || spellId == SPELL_DRUID_SOLAR_WRATH_GUARDIAN));

                return false;
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                GetAura()->ModStackAmount(-1);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_dru_legion_empowerment_AuraScript::ApplyEffect, EFFECT_1, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_empowerment_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dru_legion_empowerment_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_ADD_PCT_MODIFIER);
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_empowerment_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_dru_legion_empowerment_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_empowerment_AuraScript();
        }
};

// 236025 - Enraged Maim
class spell_dru_legion_enraged_maim : public SpellScriptLoader
{
    public:
        spell_dru_legion_enraged_maim() : SpellScriptLoader("spell_dru_legion_enraged_maim") { }

        class spell_dru_legion_enraged_maim_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_enraged_maim_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == 236026)
                    return false;
                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_enraged_maim_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_enraged_maim_AuraScript();
        }
};

// 190984 - Solar Wrath
// 197629 - Solar Wrath
// Related to spell_dru_legion_empowerment
class spell_dru_legion_solar_wrath : public SpellScriptLoader
{
    public:
        spell_dru_legion_solar_wrath() : SpellScriptLoader("spell_dru_legion_solar_wrath") { }

        class spell_dru_legion_solar_wrath_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_solar_wrath_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SOLAR_EMPOWERMENT,
                    SPELL_DRUID_STARLORD_SOLAR_BONUS
                });
            }

            void HandleHackFix()
            {
                // Remove casting time mod already directly after the solar wrath cast to avoid a possible casting time exploit because solar empowerment is removed AFTER spellhit because of the damage pct mod
                if (Aura* aura = GetCaster()->GetAura(SPELL_DRUID_SOLAR_EMPOWERMENT))
                    if (aura->GetStackAmount() == 1)
                        GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_STARLORD_SOLAR_BONUS);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_dru_legion_solar_wrath_SpellScript::HandleHackFix);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_solar_wrath_SpellScript();
        }
};

// 191037 - Starfall
class spell_dru_legion_starfall_damage : public SpellScriptLoader
{
    public:
        spell_dru_legion_starfall_damage() : SpellScriptLoader("spell_dru_legion_starfall_damage") { }

        class spell_dru_legion_starfall_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_starfall_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_ECHOING_STARS_TRAIT,
                    SPELL_DRUID_STARFALL_SUMMON,
                    SPELL_DRUID_ECHOING_STARS_VISUAL
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_DRUID_ECHOING_STARS_TRAIT))
                {
                    if (AreaTrigger* starfall = GetCaster()->GetAreaTrigger(SPELL_DRUID_STARFALL_SUMMON))
                    {
                        GuidUnorderedSet targets;
                        for (ObjectGuid const& guid : starfall->GetInsideUnits())
                        {
                            if (GetHitUnit()->GetGUID() == guid)
                                continue;

                            if (Unit* target = ObjectAccessor::GetUnit(*GetCaster(), guid))
                                if (GetCaster()->IsValidAttackTarget(target))
                                    targets.insert(target->GetGUID());
                        }

                        if (targets.empty())
                            return;

                        if (Unit* echoTarget = ObjectAccessor::GetUnit(*GetCaster(), Trinity::Containers::SelectRandomContainerElement(targets)))
                            GetHitUnit()->CastSpell(echoTarget, SPELL_DRUID_ECHOING_STARS_VISUAL, true, nullptr, nullptr, GetCaster()->GetGUID());
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_starfall_damage_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_starfall_damage_SpellScript();
        }
};

// 214519 - Echoing Stars
class spell_dru_legion_echoing_stars : public SpellScriptLoader
{
    public:
        spell_dru_legion_echoing_stars() : SpellScriptLoader("spell_dru_legion_echoing_stars") { }

        class spell_dru_legion_echoing_stars_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_echoing_stars_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_ECHOING_STARS_DAMAGE
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetSpell()->GetOriginalCaster())
                    caster->CastSpell(GetHitUnit(), SPELL_DRUID_ECHOING_STARS_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_echoing_stars_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_echoing_stars_SpellScript();
        }
};

// 202842 - Rapid Innervation
class spell_dru_legion_rapid_innervation : public SpellScriptLoader
{
    public:
        spell_dru_legion_rapid_innervation() : SpellScriptLoader("spell_dru_legion_rapid_innervation") { }

        class spell_dru_legion_rapid_innervation_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_rapid_innervation_AuraScript);

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                eventInfo.GetActionTarget()->CastSpell(eventInfo.GetActionTarget(), GetSpellInfo()->GetEffect(EFFECT_0)->TriggerSpell, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_rapid_innervation_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_rapid_innervation_AuraScript();
        }
};

// 202940 - Moon and Stars
class spell_dru_legion_moon_and_stars : public SpellScriptLoader
{
    public:
        spell_dru_legion_moon_and_stars() : SpellScriptLoader("spell_dru_legion_moon_and_stars") { }

        class spell_dru_legion_moon_and_stars_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_moon_and_stars_AuraScript);

            ///@TODO: new proc system
            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                //if (!eventInfo.GetSpellInfo())
                //    return false;

                //// We need to check that here instead of spell_proc_event because spellfamflags are shared with some other positive druid spells
                //return (eventInfo.GetSpellInfo()->Id == SPELL_DRUID_CHOSEN_OF_ELUNE || eventInfo.GetSpellInfo()->Id == SPELL_DRUID_CELESTIAL_ALIGNMENT);
                return false;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_moon_and_stars_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_moon_and_stars_AuraScript();
        }
};

// 202941 - Moon and Stars
class spell_dru_legion_moon_and_stars_dps_proc : public SpellScriptLoader
{
    public:
        spell_dru_legion_moon_and_stars_dps_proc() : SpellScriptLoader("spell_dru_legion_moon_and_stars_dps_proc") { }

        class spell_dru_legion_moon_and_stars_dps_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_moon_and_stars_dps_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_CHOSEN_OF_ELUNE,
                    SPELL_DRUID_STAR_POWER,
                    SPELL_DRUID_STARTFALL_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                return eventInfo.GetSpellInfo()->Id != SPELL_DRUID_STARTFALL_DAMAGE;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                // each damaging Balance spell you cast grants $?s102560[${$s1/3}][$s1]% haste.
                if (GetTarget()->HasSpell(SPELL_DRUID_CHOSEN_OF_ELUNE))
                {
                    if (SpellEffectInfo const* starPowerEff = sSpellMgr->AssertSpellInfo(SPELL_DRUID_STAR_POWER)->GetEffect(EFFECT_0))
                        GetTarget()->CastCustomSpell(aurEff->GetSpellEffectInfo()->TriggerSpell, SPELLVALUE_BASE_POINT0, int32(starPowerEff->CalcValue(GetTarget()) / 3), GetTarget(), TRIGGERED_FULL_MASK);
                }
                else
                    GetTarget()->CastSpell(GetTarget(), aurEff->GetSpellEffectInfo()->TriggerSpell, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_moon_and_stars_dps_proc_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_moon_and_stars_dps_proc_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_moon_and_stars_dps_proc_AuraScript();
        }
};

// 102560 - Incarnation: Chosen of Elune
// 194223 - Celestial Alignment
class spell_dru_legion_moon_and_stars_remove : public SpellScriptLoader
{
    public:
        spell_dru_legion_moon_and_stars_remove() : SpellScriptLoader("spell_dru_legion_moon_and_stars_remove") { }

        class spell_dru_legion_moon_and_stars_remove_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_moon_and_stars_remove_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MOON_AND_STARS_TRAIT,
                    SPELL_DRUID_MOON_AND_STARS_DPS_PROC,
                    SPELL_DRUID_STAR_POWER
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_MOON_AND_STARS_TRAIT))
                {
                    if (Aura* moonAndStars = GetTarget()->AddAura(SPELL_DRUID_MOON_AND_STARS_DPS_PROC, GetTarget()))
                    {
                        moonAndStars->SetMaxDuration(GetAura()->GetDuration());
                        moonAndStars->SetDuration(GetAura()->GetDuration());
                    }
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_STAR_POWER);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_dru_legion_moon_and_stars_remove_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_moon_and_stars_remove_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_moon_and_stars_remove_AuraScript();
        }
};

class SolarBeamCooldownEvent : public BasicEvent
{
    public:
        SolarBeamCooldownEvent(Unit* owner) : _owner(owner) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            if (AuraEffect* lightOfTheSun = _owner->GetAuraEffect(SPELL_DRUID_LIGHT_OF_THE_SUN_TRAIT, EFFECT_0))
                _owner->GetSpellHistory()->ModifyCooldown(SPELL_DRUID_SOLAR_BEAM_SUMMON, -(lightOfTheSun->GetAmount() * 1000));
            return true;
        }

    private:
        Unit* _owner;
};

// 97547 - Solar Beam
class spell_dru_legion_solar_beam : public SpellScriptLoader
{
    public:
        spell_dru_legion_solar_beam() : SpellScriptLoader("spell_dru_legion_solar_beam") { }

        class spell_dru_legion_solar_beam_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_solar_beam_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_LIGHT_OF_THE_SUN_TRAIT,
                    SPELL_DRUID_SOLAR_BEAM_SUMMON
                });
            }

            void CheckIfSilenceInterrupt(SpellEffIndex /*effIndex*/)
            {
                for (uint32 i = CURRENT_MELEE_SPELL; i < CURRENT_MAX_SPELL; ++i)
                {
                    if (Spell* spell = GetHitUnit()->GetCurrentSpell(CurrentSpellTypes(i)))
                    {
                        if (spell->m_spellInfo->InterruptFlags & SPELL_INTERRUPT_FLAG_INTERRUPT)
                        {
                            // ModifyCooldown is too early here... let's delay this a little bit
                            GetCaster()->m_Events.AddEvent(new SolarBeamCooldownEvent(GetCaster()), GetCaster()->m_Events.CalculateTime(50));
                            break;
                        }
                    }
                }
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_dru_legion_solar_beam_SpellScript::CheckIfSilenceInterrupt, EFFECT_0, SPELL_EFFECT_INTERRUPT_CAST);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_solar_beam_SpellScript();
        }
};

// Feral
// 768 - Cat Form
class spell_dru_legion_cat_form : public SpellScriptLoader
{
    public:
        spell_dru_legion_cat_form() : SpellScriptLoader("spell_dru_legion_cat_form") { }

        class spell_dru_legion_cat_form_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_cat_form_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({
                    SPELL_DRUID_PROTECTION_OF_ASHAMANE_TRAIT,
                    SPELL_DRUID_PROTECTION_OF_ASHAMANE_BONUS,
                    SPELL_DRUID_PROTECTION_OF_ASHAMANE_BLOCKER,
                    SPELL_DRUID_DASH,
                    SPELL_DRUID_MASTER_SHAPESHIFTER_HONOR_TALENT,
                    SPELL_DRUID_MASTER_SHAPESHIFTER_FERAL,
                    SPELL_DRUID_FERAL_AFFINITY
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_PROTECTION_OF_ASHAMANE_BONUS);

                if (AuraEffect* dash = GetTarget()->GetAuraEffect(SPELL_DRUID_DASH, EFFECT_0))
                    dash->ChangeAmount(dash->GetBaseAmount());

                if (GetTarget()->HasAura(SPELL_DRUID_MASTER_SHAPESHIFTER_HONOR_TALENT) && GetTarget()->HasAura(SPELL_DRUID_FERAL_AFFINITY))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_MASTER_SHAPESHIFTER_FERAL, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_PROTECTION_OF_ASHAMANE_TRAIT))
                {
                    if (!GetTarget()->HasAura(SPELL_DRUID_PROTECTION_OF_ASHAMANE_BLOCKER))
                    {
                        GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_PROTECTION_OF_ASHAMANE_BLOCKER, true);
                        GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_PROTECTION_OF_ASHAMANE_BONUS, true);
                    }
                }

                if (AuraEffect* dash = GetTarget()->GetAuraEffect(SPELL_DRUID_DASH, EFFECT_0))
                    dash->ChangeAmount(0);

                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_MASTER_SHAPESHIFTER_FERAL);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_dru_legion_cat_form_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_cat_form_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_cat_form_AuraScript();
        }
};

// 194223 - Celestial Alignment
class spell_dru_legion_celestial_alignment : public SpellScriptLoader
{
    public:
        spell_dru_legion_celestial_alignment() : SpellScriptLoader("spell_dru_legion_celestial_alignment") { }

        class spell_dru_legion_celestial_alignment_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_celestial_alignment_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_ECLIPSE_HONOR_TALENT,
                    SPELL_DRUID_ECLIPSE_AREATRIGGER
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_ECLIPSE_HONOR_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_ECLIPSE_AREATRIGGER, true);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_celestial_alignment_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_celestial_alignment_AuraScript();
        }
};

// 210631 - Feral Instinct
class spell_dru_legion_feral_instinct : public SpellScriptLoader
{
    public:
        spell_dru_legion_feral_instinct() : SpellScriptLoader("spell_dru_legion_feral_instinct") { }

        class spell_dru_legion_feral_instinct_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_feral_instinct_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_FERAL_INSTINCT
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastCustomSpell(SPELL_DRUID_FERAL_INSTINCT, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_feral_instinct_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_feral_instinct_AuraScript();
        }
};

// 22568 - Ferocious Bite
class spell_dru_legion_ferocious_bite : public SpellScriptLoader
{
    public:
        spell_dru_legion_ferocious_bite() : SpellScriptLoader("spell_dru_legion_ferocious_bite") { }

        class spell_dru_legion_ferocious_bite_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_ferocious_bite_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_FEROCIOUS_BITE_RIP,
                    SPELL_DRUID_SABERTOOTH,
                    SPELL_DRUID_RIP
                });
            }

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                // When used on a target below 25% health, Ferocious Bite will refresh the duration of your Rip on your target.
                // Dummy aura is applied if druid spell level is >= 66
                if ((GetHitUnit()->GetHealthPct() < 25 && GetCaster()->HasAura(SPELL_DRUID_FEROCIOUS_BITE_RIP)) || GetCaster()->HasAura(SPELL_DRUID_SABERTOOTH))
                    if (Aura* rip = GetHitUnit()->GetAura(SPELL_DRUID_RIP, GetCaster()->GetGUID()))
                        rip->RefreshDuration();

                int32 damage = int32(GetHitDamage() * GetSpell()->GetPowerCost(POWER_COMBO_POINTS));
                // converts each extra point of energy ( up to 25 energy ) into additional damage
                AddPct(damage, abs(GetCaster()->ModifyPower(POWER_ENERGY, -25) * 4));

                SetHitDamage(damage / 5);

                if (GetCaster()->HasAura(SPELL_DRUID_FEROCIOUS_WOUND_HONOR_TALENT))
                    if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_DRUID_FEROCIOUS_WOUND_HONOR_TALENT)->GetEffect(EFFECT_1))
                        if (GetSpell()->GetPowerCost(POWER_COMBO_POINTS) == effInfo->CalcValue(GetCaster()))
                            GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_FEROCIOUS_WOUND, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_ferocious_bite_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_ferocious_bite_SpellScript();
        }
};

// 1079 - Rip
class spell_dru_legion_rip : public SpellScriptLoader
{
    public:
        spell_dru_legion_rip() : SpellScriptLoader("spell_dru_legion_rip") { }

        class spell_dru_legion_rip_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_rip_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_OPEN_WOUNDS,
                    SPELL_DRUID_OPEN_WOUNDS_DEBUFF,
                    SPELL_HOTFIX_COMBO_POINT_HOLDER,
                    SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_TALENT,
                    SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS,

                });
            }

            void HandleEffectApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                int32 amount = GetTarget()->HasAura(SPELL_HOTFIX_COMBO_POINT_HOLDER, GetCasterGUID()) ? 5 : GetAura()->GetPowerCost(POWER_COMBO_POINTS);
                const_cast<AuraEffect*>(aurEff)->ChangeAmount(amount);
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
            }

            void HandleHonorTalentApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_TALENT))
                    {
                        if (Aura* bonus = caster->GetAura(SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS))
                        {
                            size_t ripsActive = caster->GetTargetAuraApplications(SPELL_DRUID_RIP).size();
                            if (ripsActive > bonus->GetStackAmount())
                                caster->CastSpell(caster, SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS, true);
                            else
                                bonus->RefreshDuration();
                        }
                        else
                            caster->CastSpell(caster, SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS, true);
                    }

                    if (caster->HasAura(SPELL_DRUID_OPEN_WOUNDS))
                        caster->CastSpell(GetTarget(), SPELL_DRUID_OPEN_WOUNDS_DEBUFF, true);
                }
            }

            void HandleHonorTalentRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (Aura* bonus = caster->GetAura(SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS))
                    {
                        size_t ripsActive = caster->GetTargetAuraApplications(SPELL_DRUID_RIP).size();
                        if (ripsActive < bonus->GetStackAmount())
                            caster->RemoveAuraFromStack(SPELL_DRUID_KING_OF_THE_JUNGLE_HONOR_BONUS);
                    }
                }
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_OPEN_WOUNDS_DEBUFF);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_dru_legion_rip_AuraScript::HandleHonorTalentApply, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_rip_AuraScript::HandleHonorTalentRemove, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_rip_AuraScript::HandleEffectApply, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_rip_AuraScript();
        }

        class spell_dru_legion_rip_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_rip_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BLOODTALONS_TRIGGERED
                });
            }

            void CalculateAmount()
            {
                if (Unit* target = GetHitUnit())
                {
                    if (AuraEffect* aurEff = target->GetAuraEffect(GetSpellInfo()->Id, EFFECT_0, GetCaster()->GetGUID()))
                    {
                        int32 damage = aurEff->GetDamage() * (target->HasAura(SPELL_HOTFIX_COMBO_POINT_HOLDER, GetCaster()->GetGUID()) ? 5 : GetSpell()->GetPowerCost(POWER_COMBO_POINTS));
                        if (AuraEffect const* bloodTalons = GetCaster()->GetAuraEffect(SPELL_DRUID_BLOODTALONS_TRIGGERED, EFFECT_0))
                        {
                            AddPct(damage, bloodTalons->GetAmount());
                            bloodTalons->GetBase()->DropCharge();
                        }
                        aurEff->SetDamage(damage);
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_rip_SpellScript::CalculateAmount);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_rip_SpellScript();
        }
};

// 203242 - Rip and Tear
class spell_dru_legion_rip_and_tear : public SpellScriptLoader
{
    public:
        spell_dru_legion_rip_and_tear() : SpellScriptLoader("spell_dru_legion_rip_and_tear") { }

        class spell_dru_legion_rip_and_tear_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_rip_and_tear_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_RAKE,
                    SPELL_DRUID_RIP
                });
            }

            bool Load() override
            {
                StealthActive = GetCaster()->HasStealthAura();
                return true;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Aura* rakeAura = GetCaster()->AddAura(SPELL_DRUID_RAKE_PERIODIC, GetHitUnit()))
                {
                    if (AuraEffect* rakePeriodic = rakeAura->GetEffect(EFFECT_0))
                    {
                        int32 pctBonus = 0;
                        int32 amount = rakePeriodic->GetAmount();
                        int32 damage = rakePeriodic->GetDamage();

                        // Feral(Level 25)
                        // While stealthed, Rake will also stun the target for 4 sec, and deal 100 % increased damage
                        if (StealthActive || GetCaster()->HasAura(SPELL_DRUID_KING_OF_THE_JUNGLE))
                        {
                            if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_DRUID_RAKE)->GetEffect(EFFECT_3))
                            {
                                GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_RAKE_STUN, true);
                                pctBonus += effInfo->CalcValue(GetCaster());
                            }
                        }

                        if (AuraEffect* bloodtalons = GetCaster()->GetAuraEffect(SPELL_DRUID_BLOODTALONS_TRIGGERED, EFFECT_0))
                        {
                            pctBonus += bloodtalons->GetAmount();
                            bloodtalons->GetBase()->DropCharge();
                        }

                        AddPct(amount, pctBonus);
                        rakePeriodic->SetAmount(amount);
                        AddPct(damage, pctBonus);
                        rakePeriodic->SetDamage(damage);
                    }
                }
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HOTFIX_COMBO_POINT_HOLDER, true);
                GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_RIP, true);
                GetCaster()->SetPower(POWER_COMBO_POINTS, 0);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_rip_and_tear_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }

            private:
                bool StealthActive;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_rip_and_tear_SpellScript();
        }
};

// 22570 - Maim (Level 110)
class spell_dru_legion_maim : public SpellScriptLoader
{
    public:
        spell_dru_legion_maim() : SpellScriptLoader("spell_dru_legion_maim") { }

        class spell_dru_legion_maim_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_maim_SpellScript);

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                // we cant check it on bool Load() because powercosts is not filled at this time and energy already removed :<
                if (GetSpell()->GetPowerCost(POWER_COMBO_POINTS) != 0)
                    GetCaster()->CastCustomSpell(MaimSpell[GetSpell()->GetPowerCost(POWER_COMBO_POINTS) - 1], SPELLVALUE_AURA_DURATION, (1000 * GetSpell()->GetPowerCost(POWER_COMBO_POINTS)), GetHitUnit(), TRIGGERED_FULL_MASK);
            }

            void PreventEffect(SpellEffIndex effIndex)
            {
                PreventHitEffect(effIndex);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_maim_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                // Initial spell is just a "hit dummy"
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_maim_SpellScript::PreventEffect, EFFECT_1, SPELL_EFFECT_WEAPON_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_maim_SpellScript::PreventEffect, EFFECT_2, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_maim_SpellScript();
        }
};

// 210702 - Ashamane's Bite
class spell_dru_legion_ashamanes_bite : public SpellScriptLoader
{
    public:
        spell_dru_legion_ashamanes_bite() : SpellScriptLoader("spell_dru_legion_ashamanes_bite") { }

        class spell_dru_legion_ashamanes_bite_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_ashamanes_bite_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_ASHAMANES_RIP,
                    SPELL_DRUID_RIP,
                    SPELL_DRUID_BRUTAL_SLASH,
                    SPELL_DRUID_RAKE,
                    SPELL_DRUID_SHRED,
                    SPELL_DRUID_SWIPE_FERAL_SPEC,
                    SPELL_DRUID_MOONFIRE_FERAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo() || !eventInfo.GetActionTarget() || GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;

                // Proc should be only checked on initial damage hit not on every damage hit (swipe)
                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(100));

                switch (eventInfo.GetSpellInfo()->Id)
                {
                    // Most of the spells share the spellfamilyflags with other damage spells thats why we now check all proc spells here.
                    case SPELL_DRUID_BRUTAL_SLASH:
                    case SPELL_DRUID_RAKE:
                    case SPELL_DRUID_SHRED:
                    case SPELL_DRUID_SWIPE_FERAL_SPEC:
                    case SPELL_DRUID_MOONFIRE_FERAL:
                        return eventInfo.GetActionTarget()->HasAura(SPELL_DRUID_RIP, GetTarget()->GetGUID());
                    default:
                        return false;
                }
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_DRUID_ASHAMANES_RIP, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_ashamanes_bite_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_ashamanes_bite_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_ashamanes_bite_AuraScript();
        }
};

// 210705 - Ashamane's Rip
class spell_dru_legion_ashamanes_rip : public SpellScriptLoader
{
    public:
        spell_dru_legion_ashamanes_rip() : SpellScriptLoader("spell_dru_legion_ashamanes_rip") { }

        class spell_dru_legion_ashamanes_rip_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_ashamanes_rip_AuraScript);

            void HandleEffectApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (AuraEffect* rip = GetTarget()->GetAuraEffect(SPELL_DRUID_RIP, EFFECT_1, GetCasterGUID()))
                {
                    const_cast<AuraEffect*>(aurEff)->SetAmount(rip->GetAmount());
                    const_cast<AuraEffect*>(aurEff)->SetDamage(rip->GetAmount());
                }
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_ashamanes_rip_AuraScript::HandleEffectApply, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_ashamanes_rip_AuraScript();
        }

        class spell_dru_legion_ashamanes_rip_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_ashamanes_rip_SpellScript);

            void CalculateAmount()
            {
                if (Unit* target = GetHitUnit())
                    if (AuraEffect* aurEff = target->GetAuraEffect(GetSpellInfo()->Id, EFFECT_0, GetCaster()->GetGUID()))
                        if (AuraEffect* rip = target->GetAuraEffect(SPELL_DRUID_RIP, EFFECT_0, GetCaster()->GetGUID()))
                            aurEff->SetDamage(rip->GetDamage());
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_ashamanes_rip_SpellScript::CalculateAmount);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_ashamanes_rip_SpellScript();
        }
};

// 210579 - Ashamane's Energy
class spell_dru_legion_ashamanes_energy : public SpellScriptLoader
{
    public:
        spell_dru_legion_ashamanes_energy() : SpellScriptLoader("spell_dru_legion_ashamanes_energy") { }

        class spell_dru_legion_ashamanes_energy_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_ashamanes_energy_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_ASHAMANE_S_ENERGY_BUFF
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastCustomSpell(SPELL_DRUID_ASHAMANE_S_ENERGY_BUFF, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_ashamanes_energy_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_ashamanes_energy_AuraScript();
        }
};

// 210722 - Ashamane's Frenzy
class spell_dru_legion_ashamanes_frenzy : public SpellScriptLoader
{
    public:
        spell_dru_legion_ashamanes_frenzy() : SpellScriptLoader("spell_dru_legion_ashamanes_frenzy") { }

        class spell_dru_legion_ashamanes_frenzy_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_ashamanes_frenzy_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_ASHAMANES_FRENZY,
                    ASHAMANES_FRENZY_VISUAL
                });
            }

            bool Load() override
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return false;

                if (caster->GetVictim())
                {
                    targetGUID = caster->GetVictim()->GetGUID();
                    return true;
                }
                return false;
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (Unit* target = ObjectAccessor::GetUnit(*caster, targetGUID))
                    {
                        caster->CastSpell(target, SPELL_DRUID_ASHAMANES_FRENZY, true);
                        caster->SendPlaySpellVisual(caster->GetGUID(), ASHAMANES_FRENZY_VISUAL, SPELL_MISS_NONE, SPELL_MISS_NONE, 0.0f, false);
                    }
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_ashamanes_frenzy_AuraScript::HandleEffectPeriodic, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }

            private:
                ObjectGuid targetGUID;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_ashamanes_frenzy_AuraScript();
        }
};

// 210723 - Ashamane's Frenzy
class spell_dru_legion_ashamanes_frenzy_periodic : public SpellScriptLoader
{
    public:
        spell_dru_legion_ashamanes_frenzy_periodic() : SpellScriptLoader("spell_dru_legion_ashamanes_frenzy_periodic") { }

        class spell_dru_legion_ashamanes_frenzy_periodic_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_ashamanes_frenzy_periodic_SpellScript);

            void CalculateAmount()
            {
                if (Unit* target = GetHitUnit())
                    if (AuraEffect* aurEff1 = target->GetAuraEffect(GetSpellInfo()->Id, EFFECT_1, GetCaster()->GetGUID()))
                        if (AuraEffect* aurEff2 = target->GetAuraEffect(GetSpellInfo()->Id, EFFECT_2, GetCaster()->GetGUID()))
                        {
                            aurEff1->SetDamage(aurEff2->GetDamage() * 3);
                            aurEff2->SetDamage(0);
                            aurEff2->GetBase()->SetNeedClientUpdateForTargets();
                        }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_ashamanes_frenzy_periodic_SpellScript::CalculateAmount);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_ashamanes_frenzy_periodic_SpellScript();
        }
};

// 22812 - Barkskin
class spell_dru_legion_barkskin : public SpellScriptLoader
{
    public:
        spell_dru_legion_barkskin() : SpellScriptLoader("spell_dru_legion_barkskin") { }

        class spell_dru_legion_barkskin_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_barkskin_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BRAMBLES_AOE_DAMAGE,
                    SPELL_DRUID_BRAMBLES
                });
            }

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_BRAMBLES) && GetTarget()->GetTypeId() == TYPEID_PLAYER)
                    if (GetTarget()->ToPlayer()->GetRole() == TALENT_ROLE_TANK)
                        GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_BRAMBLES_AOE_DAMAGE, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_barkskin_AuraScript::HandleEffectPeriodic, EFFECT_4, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_barkskin_AuraScript();
        }
};

// 1079 - Rip
// 22570 - Maim (Level 110)
// 22568 - Ferocious Bite
class spell_dru_legion_soul_of_the_forest : public SpellScriptLoader
{
    public:
        spell_dru_legion_soul_of_the_forest() : SpellScriptLoader("spell_dru_legion_soul_of_the_forest") { }

        class spell_dru_legion_soul_of_the_forest_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_soul_of_the_forest_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SOUL_OF_THE_FOREST,
                    SPELL_DRUID_SOUL_OF_THE_FOREST_ENERGIZE
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_DRUID_SOUL_OF_THE_FOREST, GetCaster()->GetGUID());
            }

            void CalculateAmount()
            {
                int32 amount = GetSpell()->GetPowerCost(POWER_COMBO_POINTS) * sSpellMgr->AssertSpellInfo(SPELL_DRUID_SOUL_OF_THE_FOREST)->GetEffect(EFFECT_0)->CalcValue(GetCaster());
                GetCaster()->CastCustomSpell(SPELL_DRUID_SOUL_OF_THE_FOREST_ENERGIZE, SPELLVALUE_BASE_POINT0, amount, GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_dru_legion_soul_of_the_forest_SpellScript::CalculateAmount);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_soul_of_the_forest_SpellScript();
        }
};

// 52610 - Savage Roar
class spell_dru_legion_savage_roar : public SpellScriptLoader
{
    public:
        spell_dru_legion_savage_roar() : SpellScriptLoader("spell_dru_legion_savage_roar") { }

        class spell_dru_legion_savage_roar_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_savage_roar_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SAVAGE_ROAR
                });
            }

            void AfterApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_SAVAGE_ROAR, true, NULL, aurEff, GetCasterGUID());
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_SAVAGE_ROAR);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_savage_roar_AuraScript::AfterApply, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_savage_roar_AuraScript::AfterRemove, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_savage_roar_AuraScript();
        }
};

// 210676 - Shadow Thrash
class spell_dru_legion_shadow_thrash : public SpellScriptLoader
{
    public:
        spell_dru_legion_shadow_thrash() : SpellScriptLoader("spell_dru_legion_shadow_thrash") { }

        class spell_dru_legion_shadow_thrash_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_shadow_thrash_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MARK_OF_SHIFTING_HEAL,
                    SPELL_DRUID_LIVING_SEED_TRIGGERED
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                // prevent procs on multiple hits
                if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;

                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(200));
                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_shadow_thrash_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_shadow_thrash_AuraScript();
        }
};

// 210686 - Shadow Thrash
class spell_dru_legion_shadow_thrash_periodic : public SpellScriptLoader
{
    public:
        spell_dru_legion_shadow_thrash_periodic() : SpellScriptLoader("spell_dru_legion_shadow_thrash_periodic") { }

        class spell_dru_legion_shadow_thrash_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_shadow_thrash_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SHADOW_THRASH_DAMAGE
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_SHADOW_THRASH_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_shadow_thrash_periodic_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_shadow_thrash_periodic_AuraScript();
        }
};

// 155722 - Rake
// 1079 - Rip
// 106830 - Thrash
class spell_dru_legion_predator : public SpellScriptLoader
{
    public:
        spell_dru_legion_predator() : SpellScriptLoader("spell_dru_legion_predator") { }

        class spell_dru_legion_predator_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_predator_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_TIGERS_FURY
                });
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    caster->GetSpellHistory()->ResetCooldown(SPELL_DRUID_TIGERS_FURY, true);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_predator_AuraScript::AfterRemove, EFFECT_FIRST_FOUND, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_predator_AuraScript();
        }
};

// 8936 - Regrowth
class spell_dru_legion_bloodtalons : public SpellScriptLoader
{
    public:
        spell_dru_legion_bloodtalons() : SpellScriptLoader("spell_dru_legion_bloodtalons") { }

        class spell_dru_legion_bloodtalons_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_bloodtalons_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BLOODTALONS,
                    SPELL_DRUID_BLOODTALONS_TRIGGERED
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_DRUID_BLOODTALONS, GetCaster()->GetGUID());
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_BLOODTALONS_TRIGGERED, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_dru_legion_bloodtalons_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_bloodtalons_SpellScript();
        }
};

// 200515 - Bloody Paws
class spell_dru_legion_bloody_paws : public SpellScriptLoader
{
    public:
        spell_dru_legion_bloody_paws() : SpellScriptLoader("spell_dru_legion_bloody_paws") { }

        class spell_dru_legion_bloody_paws_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_bloody_paws_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // sometimes Bloody Paws procs on caster itself ?!
                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_bloody_paws_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_bloody_paws_AuraScript();
        }
};

// 5221 - Shred (Level 110)
class spell_dru_legion_shred : public SpellScriptLoader
{
    public:
        spell_dru_legion_shred() : SpellScriptLoader("spell_dru_legion_shred") { }

        class spell_dru_legion_shred_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_shred_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SHRED_BLEEDING_BONUS,
                    SPELL_DRUID_SHRED_STEALTH_BONUS,
                    SPELL_DRUID_KING_OF_THE_JUNGLE
                });
            }

            bool Load() override
            {
                StealthActive = GetCaster()->HasStealthAura();
                return true;
            }

            void CalculateBonusDamage(SpellEffIndex /*effIndex*/)
            {
                int32 pctBonus = 0;
                int32 damage = GetHitDamage();

                // Feral (Level 44)
                // Deals 20 % increased damage against bleeding targets
                if (GetCaster()->HasAura(SPELL_DRUID_SHRED_BLEEDING_BONUS) && GetHitUnit()->HasAuraState(AURA_STATE_BLEEDING))
                    if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_4))
                        pctBonus += effInfo->CalcValue(GetCaster());

                // Feral (Level 56)
                // While stealthed, Shred deals 50 % increased damage, and has double the chance to critically strike [Implemented at Unit::IsSpellCrit]
                if (GetCaster()->HasAura(SPELL_DRUID_SHRED_STEALTH_BONUS) && (StealthActive || GetCaster()->HasAura(SPELL_DRUID_KING_OF_THE_JUNGLE)))
                    if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_3))
                        pctBonus += effInfo->CalcValue(GetCaster());

                AddPct(damage, pctBonus);
                SetHitDamage(damage);
            }

            private:
                bool StealthActive;

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_shred_SpellScript::CalculateBonusDamage, EFFECT_2, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_shred_SpellScript();
        }
};

// 93985 - Skull Bash
class spell_dru_legion_skull_bash_interrupt : public SpellScriptLoader
{
    public:
        spell_dru_legion_skull_bash_interrupt() : SpellScriptLoader("spell_dru_legion_skull_bash_interrupt") { }

        class spell_dru_legion_skull_bash_interrupt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_skull_bash_interrupt_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SAVAGE_MOMENTUM,
                    SPELL_DRUID_TIGERS_FURY
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_DRUID_SAVAGE_MOMENTUM);
            }

            SpellCastResult CheckCast()
            {
                if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
                    return SPELL_FAILED_ROOTED;
                return SPELL_CAST_OK;
            }

            void CheckIfSilenceInterrupt(SpellEffIndex /*effIndex*/)
            {
                for (uint32 i = CURRENT_MELEE_SPELL; i < CURRENT_MAX_SPELL; ++i)
                {
                    if (Spell* spell = GetHitUnit()->GetCurrentSpell(CurrentSpellTypes(i)))
                    {
                        if (spell->m_spellInfo->InterruptFlags & SPELL_INTERRUPT_FLAG_INTERRUPT)
                        {
                            GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_DRUID_TIGERS_FURY, true);
                            break;
                        }
                    }
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_dru_legion_skull_bash_interrupt_SpellScript::CheckCast);
                OnEffectLaunchTarget += SpellEffectFn(spell_dru_legion_skull_bash_interrupt_SpellScript::CheckIfSilenceInterrupt, EFFECT_0, SPELL_EFFECT_INTERRUPT_CAST);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_skull_bash_interrupt_SpellScript();
        }
};

// 1822 - Rake
class spell_dru_legion_rake : public SpellScriptLoader
{
    public:
        spell_dru_legion_rake() : SpellScriptLoader("spell_dru_legion_rake") { }

        class spell_dru_legion_rake_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_rake_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_RAKE_STEALTH_BONUS,
                    SPELL_DRUID_KING_OF_THE_JUNGLE,
                    SPELL_DRUID_BLOODTALONS_TRIGGERED,
                    SPELL_DRUID_RAKE_STUN,
                    SPELL_DRUID_RAKE_PERIODIC
                });
            }

            bool Load() override
            {
                StealthActive = GetCaster()->HasStealthAura();
                return (GetCaster()->HasAura(SPELL_DRUID_RAKE_STEALTH_BONUS) || GetCaster()->HasAura(SPELL_DRUID_KING_OF_THE_JUNGLE) || GetCaster()->HasAura(SPELL_DRUID_BLOODTALONS_TRIGGERED));
            }

            void CalculateBonusDamage(SpellEffIndex /*effIndex*/)
            {
                int32 damage = GetHitDamage();
                int32 pctBonus = 0;
                // Feral(Level 25)
                // While stealthed, Rake will also stun the target for 4 sec, and deal 100 % increased damage
                if (StealthActive || GetCaster()->HasAura(SPELL_DRUID_KING_OF_THE_JUNGLE))
                {
                    if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_3))
                    {
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_RAKE_STUN, true);
                        pctBonus += effInfo->CalcValue(GetCaster());
                    }
                }

                if (AuraEffect* bloodtalons = GetCaster()->GetAuraEffect(SPELL_DRUID_BLOODTALONS_TRIGGERED, EFFECT_0))
                    pctBonus += bloodtalons->GetAmount();

                AddPct(damage, pctBonus);
                SetHitDamage(damage);
            }

            void HandleDotBonus()
            {
                if (!GetHitUnit())
                    return;

                if (AuraEffect const* rakePeriodic = GetHitUnit()->GetAuraEffect(SPELL_DRUID_RAKE_PERIODIC, EFFECT_0, GetCaster()->GetGUID()))
                {
                    int32 pctBonus = 0;
                    int32 amount = rakePeriodic->GetAmount();
                    int32 damage = rakePeriodic->GetDamage();

                    // Feral(Level 25)
                    // While stealthed, Rake will also stun the target for 4 sec, and deal 100 % increased damage
                    if (StealthActive || GetCaster()->HasAura(SPELL_DRUID_KING_OF_THE_JUNGLE))
                    {
                        if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_3))
                        {
                            GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_RAKE_STUN, true);
                            pctBonus += effInfo->CalcValue(GetCaster());
                        }
                    }

                    if (AuraEffect* bloodtalons = GetCaster()->GetAuraEffect(SPELL_DRUID_BLOODTALONS_TRIGGERED, EFFECT_0))
                    {
                        pctBonus += bloodtalons->GetAmount();
                        bloodtalons->GetBase()->DropCharge();
                    }

                    AddPct(amount, pctBonus);
                    const_cast<AuraEffect*>(rakePeriodic)->SetAmount(amount);
                    AddPct(damage, pctBonus);
                    const_cast<AuraEffect*>(rakePeriodic)->SetDamage(damage);
                }
            }

            private:
                bool StealthActive;

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_rake_SpellScript::CalculateBonusDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                AfterHit += SpellHitFn(spell_dru_legion_rake_SpellScript::HandleDotBonus);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_rake_SpellScript();
        }
};

// 16974 - Predatory Swiftness
class spell_dru_legion_predatory_swiftness : public SpellScriptLoader
{
    public:
        spell_dru_legion_predatory_swiftness() : SpellScriptLoader("spell_dru_legion_predatory_swiftness") { }

        class spell_dru_legion_predatory_swiftness_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_predatory_swiftness_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_PREDATORY_SWIFTNESS
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                if (AuraEffect* aurEff = GetEffect(EFFECT_2))
                    return roll_chance_i(aurEff->GetAmount());
                return false;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_PREDATORY_SWIFTNESS, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_predatory_swiftness_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_predatory_swiftness_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_predatory_swiftness_AuraScript();
        }
};

// 1079 - Rip
// 22570 - Maim (Level 110)
// 22568 - Ferocious Bite
// 52610 - Savage Roar
class spell_dru_legion_predatory_swiftness_helper : public SpellScriptLoader
{
    public:
        spell_dru_legion_predatory_swiftness_helper() : SpellScriptLoader("spell_dru_legion_predatory_swiftness_helper") { }

        class spell_dru_legion_predatory_swiftness_helper_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_predatory_swiftness_helper_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_PREDATORY_SWIFTNESS_PASSIVE
                });
            }

            void AddComboPoints()
            {
                if (AuraEffect* predatory = GetCaster()->GetAuraEffect(SPELL_DRUID_PREDATORY_SWIFTNESS_PASSIVE, EFFECT_2))
                    predatory->SetAmount(GetSpell()->GetPowerCost(POWER_COMBO_POINTS) * predatory->GetBaseAmount());
            }

            void Register() override
            {
                BeforeCast += SpellCastFn(spell_dru_legion_predatory_swiftness_helper_SpellScript::AddComboPoints);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_predatory_swiftness_helper_SpellScript();
        }
};

// 106785 - Swipe
// 213764 - Swipe
class spell_dru_legion_swipe : public SpellScriptLoader
{
    public:
        spell_dru_legion_swipe() : SpellScriptLoader("spell_dru_legion_swipe") { }

        class spell_dru_legion_swipe_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_swipe_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SWIPE_BONUS_FERAL,
                    SPELL_DRUID_COMBO_POINT
                });
            }

            void RewardComboPoints(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_COMBO_POINT, true);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_DRUID_SWIPE_BONUS_FERAL) && GetHitUnit()->HasAuraState(AURA_STATE_BLEEDING))
                {
                    if (SpellEffectInfo const* eff1Info = GetEffectInfo(EFFECT_1))
                    {
                        int32 damage = GetHitDamage();
                        AddPct(damage, eff1Info->CalcValue(GetCaster()));
                        SetHitDamage(damage);
                    }
                }
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_dru_legion_swipe_SpellScript::RewardComboPoints, EFFECT_0, SPELL_EFFECT_DUMMY);
                if (m_scriptSpellId == SPELL_DRUID_SWIPE_FERAL_SPEC)
                    OnEffectHitTarget += SpellEffectFn(spell_dru_legion_swipe_SpellScript::HandleDummy, EFFECT_2, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_swipe_SpellScript();
        }
};

// 202028 - Brutal Slash
class spell_dru_legion_brutal_slash : public SpellScriptLoader
{
    public:
        spell_dru_legion_brutal_slash() : SpellScriptLoader("spell_dru_legion_brutal_slash") { }

        class spell_dru_legion_brutal_slash_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_brutal_slash_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_COMBO_POINT
                });
            }

            void AddComboPoint(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_COMBO_POINT, true);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_dru_legion_brutal_slash_SpellScript::AddComboPoint, EFFECT_2, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_brutal_slash_SpellScript();
        }
};

// 106830 - Thrash
class spell_dru_legion_thrash_feral : public SpellScriptLoader
{
    public:
        spell_dru_legion_thrash_feral() : SpellScriptLoader("spell_dru_legion_thrash_feral") { }

        class spell_dru_legion_thrash_feral_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_thrash_feral_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SCENT_OF_BLOOD_BONUS,
                    SPELL_DRUID_SCENT_OF_BLOOD
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_DRUID_SCENT_OF_BLOOD);
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = uint32(targets.size());
            }

            void HandleAfterCast()
            {
                if (_targetCount > 0)
                    GetCaster()->CastCustomSpell(SPELL_DRUID_SCENT_OF_BLOOD_BONUS, SPELLVALUE_BASE_POINT0, -int32(_targetCount * sSpellMgr->AssertSpellInfo(SPELL_DRUID_SCENT_OF_BLOOD)->GetEffect(EFFECT_0)->CalcValue(GetCaster())), GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_thrash_feral_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                AfterCast += SpellCastFn(spell_dru_legion_thrash_feral_SpellScript::HandleAfterCast);
            }

        private:
            uint32 _targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_thrash_feral_SpellScript();
        }
};

// 61336 - Survival Instincts
class spell_dru_legion_survival_instincts : public SpellScriptLoader
{
    public:
        spell_dru_legion_survival_instincts() : SpellScriptLoader("spell_dru_legion_survival_instincts") { }

        class spell_dru_legion_survival_instincts_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_survival_instincts_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SURVIVAL_INSTINCTS_BUFF
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_SURVIVAL_INSTINCTS_BUFF, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_SURVIVAL_INSTINCTS_BUFF);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_survival_instincts_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_survival_instincts_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_survival_instincts_AuraScript();
        }
};

// Restoration
// 157982 - Tranquility (Level 110)
class spell_dru_legion_tranquility : public SpellScriptLoader
{
    public:
        spell_dru_legion_tranquility() : SpellScriptLoader("spell_dru_legion_tranquility") { }

        class spell_dru_legion_tranquility_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_tranquility_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->GetEffect(EFFECT_2) && ValidateSpellInfo(
                {
                    SPELL_DRUID_TRANQUILITY
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER && !(GetCaster()->ToPlayer()->GetGroup() && GetCaster()->ToPlayer()->GetGroup()->isRaidGroup());
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_DRUID_TRANQUILITY)->GetEffect(EFFECT_2))
                {
                    int32 heal = GetHitHeal();
                    AddPct(heal, effInfo->CalcValue(GetCaster()));
                    SetHitHeal(heal);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_tranquility_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_tranquility_SpellScript();
        }
};

// 33763 - Lifebloom
class spell_dru_legion_lifebloom : public SpellScriptLoader
{
    public:
        spell_dru_legion_lifebloom() : SpellScriptLoader("spell_dru_legion_lifebloom") { }

        class spell_dru_legion_lifebloom_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_lifebloom_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_LIFEBLOOM_FINAL_HEAL,
                    SPELL_DRUID_FOCUSED_GROWTH_BONUS
                });
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (!GetCaster() || !GetUnitOwner())
                    return;

                // Final heal only on duration end
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_EXPIRE || GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_ENEMY_SPELL)
                    GetCaster()->CastSpell(GetUnitOwner(), SPELL_DRUID_LIFEBLOOM_FINAL_HEAL, true);

                // Check if another lifebloom is still active
                if (!GetUnitOwner()->HasAura(GetId()))
                    GetUnitOwner()->RemoveAurasDueToSpell(SPELL_DRUID_FOCUSED_GROWTH_BONUS);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_lifebloom_AuraScript::AfterRemove, EFFECT_0, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_lifebloom_AuraScript();
        }

        class spell_dru_legion_lifebloom_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_lifebloom_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_LIFEBLOOM_FINAL_HEAL,
                    SPELL_DRUID_FOCUSED_GROWTH,
                    SPELL_DRUID_FOCUSED_GROWTH_BONUS
                });
            }

            void CheckRestDuration()
            {
                // If rest duration is < 30% of max duration on refresh the final heal should be casted
                if (Unit* target = GetExplTargetUnit())
                {
                    if (Aura* lifebloom = target->GetAura(GetSpellInfo()->Id, GetCaster()->GetGUID()))
                        if (lifebloom->GetDuration() < CalculatePct(lifebloom->GetMaxDuration(), 30))
                            GetCaster()->CastSpell(target, SPELL_DRUID_LIFEBLOOM_FINAL_HEAL, true);

                    if (GetCaster()->HasAura(SPELL_DRUID_FOCUSED_GROWTH))
                        GetCaster()->CastSpell(target, SPELL_DRUID_FOCUSED_GROWTH_BONUS, true);
                }
            }

            void Register() override
            {
                OnCast += SpellCastFn(spell_dru_legion_lifebloom_SpellScript::CheckRestDuration);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_lifebloom_SpellScript();
        }
};

// 145205 - Efflorescence
class spell_dru_legion_efflorescence : public SpellScriptLoader
{
    public:
        spell_dru_legion_efflorescence() : SpellScriptLoader("spell_dru_legion_efflorescence") { }

        class spell_dru_legion_efflorescence_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_efflorescence_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_EFFLORESCENCE_PERIODIC
                });
            }

            void HandleTotem()
            {
                for (Unit::ControlList::iterator itr = GetCaster()->m_Controlled.begin(); itr != GetCaster()->m_Controlled.end(); ++itr)
                {
                    if ((*itr)->GetEntry() == EFFLORESECENCE_TOTEM)
                    {
                        (*itr)->CastSpell((*itr), SPELL_DRUID_EFFLORESCENCE_PERIODIC, true);
                        (*itr)->SetDisplayId((*itr)->ToCreature()->GetCreatureTemplate()->Modelid1); // Restore display id, changed at totem init
                    }
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_dru_legion_efflorescence_SpellScript::HandleTotem);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_efflorescence_SpellScript();
        }
};

// 81262 - Efflorescence
class spell_dru_legion_efflorescence_periodic : public SpellScriptLoader
{
    public:
        spell_dru_legion_efflorescence_periodic() : SpellScriptLoader("spell_dru_legion_efflorescence_periodic") { }

        class spell_dru_legion_efflorescence_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_efflorescence_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_EFFLORESCENCE_HEAL
                });
            }

            void HandleDummy(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetTarget()->GetCharmerOrOwnerPlayerOrPlayerItself())
                    caster->CastSpell(GetTarget(), SPELL_DRUID_EFFLORESCENCE_HEAL, false);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_efflorescence_periodic_AuraScript::HandleDummy, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_efflorescence_periodic_AuraScript();
        }
};

// 81269 - Efflorescence
class spell_dru_legion_efflorescence_heal : public SpellScriptLoader
{
    public:
        spell_dru_legion_efflorescence_heal() : SpellScriptLoader("spell_dru_legion_efflorescence_heal") { }

        class spell_dru_legion_efflorescence_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_efflorescence_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SPRING_BLOSSOMS,
                    SPELL_DRUID_SPRING_BLOSSOMS_HEAL
                });
            }

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.sort(Trinity::HealthPctOrderPred());
                if (targets.size() > 3)
                    targets.resize(3);
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (Unit* owner = GetCaster()->GetCharmerOrOwnerPlayerOrPlayerItself())
                    if (owner->HasAura(SPELL_DRUID_SPRING_BLOSSOMS))
                        GetHitUnit()->CastSpell(GetHitUnit(), SPELL_DRUID_SPRING_BLOSSOMS_HEAL, true, nullptr, nullptr, owner->GetGUID());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_efflorescence_heal_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_efflorescence_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_efflorescence_heal_SpellScript();
        }
};

// 48438 - Wild Growth
class spell_dru_legion_wild_growth : public SpellScriptLoader
{
    public:
        spell_dru_legion_wild_growth() : SpellScriptLoader("spell_dru_legion_wild_growth") { }

        class spell_dru_legion_wild_growth_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_wild_growth_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_TREE_OF_LIFE,
                    SPELL_DRUID_NATURES_ESSENCE_TRAIT,
                    SPELL_DRUID_NATURES_ESSENCE_HEAL,
                    SPELL_DRUID_DREAMWALKER_PROC_AURA,
                    SPELL_DRUID_POWER_OF_THE_ARCHDRUID
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                uint32 maxTargets = GetCaster()->HasAura(SPELL_DRUID_TREE_OF_LIFE) ? 8 : 6;

                if (targets.size() > maxTargets)
                {
                    targets.sort(Trinity::HealthPctOrderPred());
                    targets.resize(maxTargets);
                }

                _targets = targets;
            }

            void SetTargets(std::list<WorldObject*>& targets)
            {
                targets = _targets;
            }

            void HandleNaturesEssence(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_DRUID_NATURES_ESSENCE_TRAIT))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_NATURES_ESSENCE_HEAL, true);
            }

            /// @TODO: new proc system
            void HandleTraitHackfixes()
            {
                // currently impossible to proc with "14 Successful cast positive magic spell" because spell doesn't have proc_ex_internal_hot on cast
                if (GetCaster()->HasAura(SPELL_DRUID_DREAMWALKER_PROC_AURA))
                    if (roll_chance_i(50))
                        GetCaster()->CastSpell(GetCaster(), sSpellMgr->AssertSpellInfo(SPELL_DRUID_DREAMWALKER_PROC_AURA)->GetEffect(EFFECT_0)->TriggerSpell, true);

                // Same here
                if (GetCaster()->HasAura(SPELL_DRUID_POWER_OF_THE_ARCHDRUID))
                    if (roll_chance_i(25))
                        GetCaster()->CastSpell(GetCaster(), sSpellMgr->AssertSpellInfo(SPELL_DRUID_POWER_OF_THE_ARCHDRUID)->GetEffect(EFFECT_0)->TriggerSpell, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_wild_growth_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_wild_growth_SpellScript::SetTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_wild_growth_SpellScript::HandleNaturesEssence, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
                AfterCast += SpellCastFn(spell_dru_legion_wild_growth_SpellScript::HandleTraitHackfixes);
            }

        private:
            std::list<WorldObject*> _targets;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_wild_growth_SpellScript();
        }
};

// 102560 - Incarnation: Chosen of Elune
class spell_dru_legion_incarnation_chosen_of_elune : public SpellScriptLoader
{
    public:
        spell_dru_legion_incarnation_chosen_of_elune() : SpellScriptLoader("spell_dru_legion_incarnation_chosen_of_elune") { }

        class spell_dru_legion_incarnation_chosen_of_elune_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_incarnation_chosen_of_elune_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE_VIS,
                    SPELL_DRUID_MOONKIN_FORM,
                    SPELL_DRUID_ECLIPSE_HONOR_TALENT,
                    SPELL_DRUID_ECLIPSE_AREATRIGGER
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_MOONKIN_FORM))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE_VIS, true);

                if (GetTarget()->HasAura(SPELL_DRUID_ECLIPSE_HONOR_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_ECLIPSE_AREATRIGGER, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_INCARNATION_CHOSEN_OF_ELUNE_VIS);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_incarnation_chosen_of_elune_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_incarnation_chosen_of_elune_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_incarnation_chosen_of_elune_AuraScript();
        }
};

// 33891 - Incarnation: Tree of Life
class spell_dru_legion_incarnation_tree_of_life : public SpellScriptLoader
{
    public:
        spell_dru_legion_incarnation_tree_of_life() : SpellScriptLoader("spell_dru_legion_incarnation_tree_of_life") { }

        class spell_dru_legion_incarnation_tree_of_life_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_incarnation_tree_of_life_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_TREE_OF_LIFE_MOD_BONUS,
                    SPELL_DRUID_TREE_OF_LIFE_CAST_BONUS,
                    SPELL_DRUID_INCARNATION,
                    SPELL_DRUID_INCARNATION
                });
            }

            void AfterApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TREE_OF_LIFE_MOD_BONUS, true, NULL, aurEff, GetCasterGUID());
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TREE_OF_LIFE_CAST_BONUS, true, NULL, aurEff, GetCasterGUID());

                // Don't reapply incarnation
                if (!GetTarget()->HasAura(SPELL_DRUID_INCARNATION))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_INCARNATION, true);
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TREE_OF_LIFE_MOD_BONUS);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TREE_OF_LIFE_CAST_BONUS);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_incarnation_tree_of_life_AuraScript::AfterApply, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_incarnation_tree_of_life_AuraScript::AfterRemove, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_incarnation_tree_of_life_AuraScript();
        }
};

// 102351 - Cenarion Ward
class spell_dru_legion_cenarion_ward : public SpellScriptLoader
{
    public:
        spell_dru_legion_cenarion_ward() : SpellScriptLoader("spell_dru_legion_cenarion_ward") { }

        class spell_dru_legion_cenarion_ward_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_cenarion_ward_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_0))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(GetTarget(), GetSpellInfo()->GetEffect(EFFECT_0)->TriggerSpell, true, nullptr, aurEff, GetCasterGUID());
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_cenarion_ward_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_cenarion_ward_AuraScript();
        }
};

// 213947 - Clan Defender
class spell_dru_legion_clan_defender : public SpellScriptLoader
{
    public:
        spell_dru_legion_clan_defender() : SpellScriptLoader("spell_dru_legion_clan_defender") { }

        class spell_dru_legion_clan_defender_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_clan_defender_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MANGLE_BEAR,
                    SPELL_DRUID_GORE_MANGLE_BONUS
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->GetSpellHistory()->ResetCooldown(SPELL_DRUID_MANGLE_BEAR, true);
                    caster->CastSpell(caster, SPELL_DRUID_GORE_MANGLE_BONUS, true);
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_clan_defender_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_clan_defender_AuraScript();
        }
};

// 774 - Rejuvenation
// 155777 - Rejuvenation (Germination)
class spell_dru_legion_abundance : public SpellScriptLoader
{
    public:
        spell_dru_legion_abundance() : SpellScriptLoader("spell_dru_legion_abundance") { }

        class spell_dru_legion_abundance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_abundance_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_ABUNDANCE,
                    SPELL_DRUID_ABUNDANCE_EFFECT
                });
            }

            bool Load() override
            {
                return GetCaster() && GetCaster()->HasAura(SPELL_DRUID_ABUNDANCE);
            }

            void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (Aura* abundance = caster->GetAura(SPELL_DRUID_ABUNDANCE_EFFECT))
                        abundance->ModStackAmount(1);
                    else
                        caster->CastSpell(caster, SPELL_DRUID_ABUNDANCE_EFFECT, true);
                }
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (Aura* abundance = caster->GetAura(SPELL_DRUID_ABUNDANCE_EFFECT))
                        abundance->ModStackAmount(-1);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_abundance_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_abundance_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_abundance_AuraScript();
        }
};

// 774 - Rejuvenation
// 155777 - Rejuvenation (Germination)
class spell_dru_legion_cultivation : public SpellScriptLoader
{
    public:
        spell_dru_legion_cultivation() : SpellScriptLoader("spell_dru_legion_cultivation") { }

        class spell_dru_legion_cultivation_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_cultivation_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_CULTIVATION,
                    SPELL_DRUID_CULTIVATION_HEAL
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    if (AuraEffect const* aurEff = caster->GetAuraEffect(SPELL_DRUID_CULTIVATION, EFFECT_0))
                        if (GetTarget()->HealthBelowPct(aurEff->GetAmount()))
                            caster->CastSpell(GetTarget(), SPELL_DRUID_CULTIVATION_HEAL, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_cultivation_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_HEAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_cultivation_AuraScript();
        }
};

// 197721 - Flourish
class spell_dru_legion_flourish : public SpellScriptLoader
{
    public:
        spell_dru_legion_flourish() : SpellScriptLoader("spell_dru_legion_flourish") { }

        class spell_dru_legion_flourish_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_flourish_SpellScript);

            void HandleScriptEffect(SpellEffIndex effIndex)
            {
                uint32 bonusDuration = GetEffectInfo(effIndex)->CalcValue(GetCaster()) * 1000;
                Unit::AuraMap const& auras = GetHitUnit()->GetOwnedAuras();
                for (Unit::AuraMap::const_iterator itr = auras.begin(); itr != auras.end(); ++itr)
                    if (itr->second->GetCasterGUID() == GetCaster()->GetGUID())
                        if (itr->second->HasEffectType(SPELL_AURA_PERIODIC_HEAL))
                            itr->second->SetDuration(itr->second->GetDuration() + bonusDuration);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_flourish_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_flourish_SpellScript();
        }
};

// 774 - Rejuvenation
class spell_dru_legion_rejuvenation_soul_of_the_forest : public SpellScriptLoader
{
    public:
        spell_dru_legion_rejuvenation_soul_of_the_forest() : SpellScriptLoader("spell_dru_legion_rejuvenation_soul_of_the_forest") { }

        class spell_dru_legion_rejuvenation_soul_of_the_forest_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_rejuvenation_soul_of_the_forest_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO,
                    SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO);
            }

            void AddTalentBonus()
            {
                if (!GetHitUnit())
                    return;

                if (AuraEffect* aurEff = GetHitUnit()->GetAuraEffect(GetSpellInfo()->Id, EFFECT_0, GetCaster()->GetGUID()))
                {
                    if (AuraEffect* forestAurEff = GetCaster()->GetAuraEffect(SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD, EFFECT_1))
                    {
                        int32 damage = aurEff->GetDamage();
                        AddPct(damage, forestAurEff->GetAmount());
                        aurEff->SetDamage(damage);
                        GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD);
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_rejuvenation_soul_of_the_forest_SpellScript::AddTalentBonus);
            }
        };

        class spell_dru_legion_rejuvenation_soul_of_the_forest_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_rejuvenation_soul_of_the_forest_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_REVITALIZE_TALENT,
                    SPELL_DRUID_REVITALIZE_PROC_AURA
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_DRUID_REVITALIZE_TALENT))
                        caster->CastSpell(GetTarget(), SPELL_DRUID_REVITALIZE_PROC_AURA, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_REVITALIZE_PROC_AURA);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_dru_legion_rejuvenation_soul_of_the_forest_AuraScript::ApplyEffect, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_rejuvenation_soul_of_the_forest_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_rejuvenation_soul_of_the_forest_AuraScript();
        }

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_rejuvenation_soul_of_the_forest_SpellScript();
        }
};

// 8936 - Regrowth
class spell_dru_legion_regrowth : public SpellScriptLoader
{
    public:
        spell_dru_legion_regrowth() : SpellScriptLoader("spell_dru_legion_regrowth") { }

        class spell_dru_legion_regrowth_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_regrowth_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO,
                    SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD,
                    SPELL_DRUID_PROTECTOR_OF_THE_GROVE,
                    SPELL_DRUID_PROTECTOR_OF_THE_GROVE_BONUS
                });
            }

            void AddTalentBonus(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO))
                {
                    if (AuraEffect* forestAurEff = GetCaster()->GetAuraEffect(SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD, EFFECT_1))
                    {
                        int32 heal = GetHitHeal();
                        AddPct(heal, forestAurEff->GetAmount());
                        SetHitHeal(heal);
                    }
                }
            }

            void AddTalentDotBonus()
            {
                if (Unit* target = GetHitUnit())
                {
                    if (GetCaster()->HasAura(SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO))
                    {
                        if (AuraEffect* aurEff = target->GetAuraEffect(GetSpellInfo()->Id, EFFECT_1, GetCaster()->GetGUID()))
                        {
                            if (AuraEffect* forestAurEff = GetCaster()->GetAuraEffect(SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD, EFFECT_1))
                            {
                                int32 damage = aurEff->GetDamage();
                                AddPct(damage, forestAurEff->GetAmount());
                                aurEff->SetDamage(damage);
                                GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD);
                            }
                        }
                    }

                    if (target != GetCaster())
                        if (GetCaster()->HasAura(SPELL_DRUID_PROTECTOR_OF_THE_GROVE))
                            GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_PROTECTOR_OF_THE_GROVE_BONUS, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_regrowth_SpellScript::AddTalentBonus, EFFECT_0, SPELL_EFFECT_HEAL);
                AfterHit += SpellHitFn(spell_dru_legion_regrowth_SpellScript::AddTalentDotBonus);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_regrowth_SpellScript();
        }
};

// 18562 - Swiftmend
class spell_dru_legion_swiftmend : public SpellScriptLoader
{
    public:
        spell_dru_legion_swiftmend() : SpellScriptLoader("spell_dru_legion_swiftmend") { }

        class spell_dru_legion_swiftmend_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_swiftmend_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO,
                    SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD,
                    SPELL_DRUID_RESTORATION_AFFINITY
                });
            }

            void HandleSoulOfTheForest(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO))
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD, true);
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_DRUID_MASTER_SHAPESHIFTER_HONOR_TALENT) && GetCaster()->HasAura(SPELL_DRUID_RESTORATION_AFFINITY))
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_MASTER_SHAPESHIFTER_RESTO, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_swiftmend_SpellScript::HandleSoulOfTheForest, EFFECT_0, SPELL_EFFECT_HEAL);
                AfterCast += SpellCastFn(spell_dru_legion_swiftmend_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_swiftmend_SpellScript();
        }
};

// 48438 - Wild Growth
class spell_dru_legion_wild_growth_soul_of_the_forest : public SpellScriptLoader
{
    public:
        spell_dru_legion_wild_growth_soul_of_the_forest() : SpellScriptLoader("spell_dru_legion_wild_growth_soul_of_the_forest") { }

        class spell_dru_legion_wild_growth_soul_of_the_forest_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_wild_growth_soul_of_the_forest_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO,
                    SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_DRUID_SOUL_OF_THE_FOREST_RESTO);
            }

            void HandleSoulOfTheForest()
            {
                if (!GetHitUnit())
                    return;

                if (AuraEffect* aurEff = GetHitUnit()->GetAuraEffect(GetSpellInfo()->Id, EFFECT_0, GetCaster()->GetGUID()))
                {
                    if (AuraEffect* forestAurEff = GetCaster()->GetAuraEffect(SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD, EFFECT_2))
                    {
                        int32 damage = aurEff->GetDamage();
                        AddPct(damage, forestAurEff->GetAmount());
                        aurEff->SetDamage(damage);
                    }
                }
            }

            void RemoveSoulOfTheForest()
            {
                GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_SOUL_OF_THE_FOREST_HEAL_MOD);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_wild_growth_soul_of_the_forest_SpellScript::HandleSoulOfTheForest);
                AfterCast += SpellCastFn(spell_dru_legion_wild_growth_soul_of_the_forest_SpellScript::RemoveSoulOfTheForest);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_wild_growth_soul_of_the_forest_SpellScript();
        }
};

// 145108 - Ysera's Gift
class spell_dru_legion_yseras_gift_periodic : public SpellScriptLoader
{
    public:
        spell_dru_legion_yseras_gift_periodic() : SpellScriptLoader("spell_dru_legion_yseras_gift_periodic") { }

        class spell_dru_legion_yseras_gift_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_yseras_gift_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_YSERAS_GIFT_SELF_HEAL,
                    SPELL_DRUID_YSERAS_GIFT_PARTY_HEAL
                });
            }

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                int32 healAmount = int32(GetTarget()->CountPctFromMaxHealth(aurEff->GetAmount()));
                if (GetTarget()->GetHealth() != GetTarget()->GetMaxHealth())
                    GetTarget()->CastCustomSpell(GetTarget(), SPELL_DRUID_YSERAS_GIFT_SELF_HEAL, &healAmount, nullptr, nullptr, true, nullptr, aurEff);
                else if (GetTarget()->GetTypeId() == TYPEID_PLAYER && GetTarget()->ToPlayer()->GetGroup())
                    GetTarget()->CastCustomSpell(GetTarget(), SPELL_DRUID_YSERAS_GIFT_PARTY_HEAL, &healAmount, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_yseras_gift_periodic_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_yseras_gift_periodic_AuraScript();
        }
};

// 145110 - Ysera's Gift
class spell_dru_legion_yseras_gift_party_heal : public SpellScriptLoader
{
    public:
        spell_dru_legion_yseras_gift_party_heal() : SpellScriptLoader("spell_dru_legion_yseras_gift_party_heal") { }

        class spell_dru_legion_yseras_gift_party_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_yseras_gift_party_heal_SpellScript);

            void SelectTarget(std::list<WorldObject*>& targets)
            {
                targets.remove_if([](WorldObject* target) -> bool
                {
                    return target->ToUnit()->GetMaxHealth() == target->ToUnit()->GetHealth();
                });

                if (targets.size() > 1)
                    targets.resize(1);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_yseras_gift_party_heal_SpellScript::SelectTarget, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_yseras_gift_party_heal_SpellScript();
        }
};

// 48500 - Living Seed
class spell_dru_legion_living_seed_passive : public SpellScriptLoader
{
    public:
        spell_dru_legion_living_seed_passive() : SpellScriptLoader("spell_dru_legion_living_seed_passive") { }

        class spell_dru_legion_living_seed_passive_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_living_seed_passive_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MARK_OF_SHIFTING_HEAL,
                    SPELL_DRUID_LIVING_SEED_TRIGGERED
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetHealInfo())
                    return false;

                if (!eventInfo.GetSpellInfo() || eventInfo.GetSpellInfo()->Id == SPELL_DRUID_MARK_OF_SHIFTING_HEAL)
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 procAmount = CalculatePct(int32(eventInfo.GetHealInfo()->GetHeal()), aurEff->GetAmount());
                if (AuraEffect const* seed = GetTarget()->GetAuraEffect(SPELL_DRUID_LIVING_SEED_TRIGGERED, EFFECT_0, GetCasterGUID()))
                    procAmount += seed->GetAmount();
                procAmount = std::min(procAmount, int32(eventInfo.GetActor()->CountPctFromMaxHealth(50)));
                GetTarget()->CastCustomSpell(eventInfo.GetActionTarget(), SPELL_DRUID_LIVING_SEED_TRIGGERED, &procAmount, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_living_seed_passive_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_living_seed_passive_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_living_seed_passive_AuraScript();
        }
};

// 48504 - Living Seed
class spell_dru_legion_living_seed_triggered : public SpellScriptLoader
{
    public:
        spell_dru_legion_living_seed_triggered() : SpellScriptLoader("spell_dru_legion_living_seed_triggered") { }

        class spell_dru_legion_living_seed_triggered_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_living_seed_triggered_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_LIVING_SEED_HEAL
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                int32 healAmount = aurEff->GetAmount();
                GetTarget()->CastCustomSpell(GetTarget(), SPELL_DRUID_LIVING_SEED_HEAL, &healAmount, nullptr, nullptr, true, nullptr, aurEff, GetCasterGUID());
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_living_seed_triggered_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_living_seed_triggered_AuraScript();
        }
};

// 186370 - Mark of Shifting
class spell_dru_legion_mark_of_shifting_periodic : public SpellScriptLoader
{
    public:
        spell_dru_legion_mark_of_shifting_periodic() : SpellScriptLoader("spell_dru_legion_mark_of_shifting_periodic") { }

        class spell_dru_legion_mark_of_shifting_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_mark_of_shifting_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MARK_OF_SHIFTING_HEAL
                });
            }

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_MARK_OF_SHIFTING_HEAL, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_mark_of_shifting_periodic_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_mark_of_shifting_periodic_AuraScript();
        }
};

// 236144 - Master Shapeshifter
class spell_dru_legion_master_shapeshifter : public SpellScriptLoader
{
    public:
        spell_dru_legion_master_shapeshifter() : SpellScriptLoader("spell_dru_legion_master_shapeshifter") { }

        class spell_dru_legion_master_shapeshifter_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_master_shapeshifter_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MASTER_SHAPESHIFTER_BALANCE,
                    SPELL_DRUID_MASTER_SHAPESHIFTER_FERAL,
                    SPELL_DRUID_MASTER_SHAPESHIFTER_RESTO
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_MASTER_SHAPESHIFTER_BALANCE);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_MASTER_SHAPESHIFTER_FERAL);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_MASTER_SHAPESHIFTER_RESTO);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_master_shapeshifter_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_master_shapeshifter_AuraScript();
        }
};

// 197625 - Moonkin Form
class spell_dru_legion_moonkin_form_balance_affinity : public SpellScriptLoader
{
    public:
        spell_dru_legion_moonkin_form_balance_affinity() : SpellScriptLoader("spell_dru_legion_moonkin_form_balance_affinity") { }

        class spell_dru_legion_moonkin_form_balance_affinity_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_moonkin_form_balance_affinity_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MASTER_SHAPESHIFTER_HONOR_TALENT,
                    SPELL_DRUID_MASTER_SHAPESHIFTER_BALANCE
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_DRUID_MASTER_SHAPESHIFTER_HONOR_TALENT))
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_MASTER_SHAPESHIFTER_BALANCE, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_dru_legion_moonkin_form_balance_affinity_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_moonkin_form_balance_affinity_SpellScript();
        }
};

// 189854 - Dreamwalker
class spell_dru_legion_dreamwalker : public SpellScriptLoader
{
    public:
        spell_dru_legion_dreamwalker() : SpellScriptLoader("spell_dru_legion_dreamwalker") { }

        class spell_dru_legion_dreamwalker_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_dreamwalker_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_REJUVENATION,
                    SPELL_DRUID_GERMINATION_HEAL,
                    SPELL_DRUID_DREAMWALKER_HEAL
                });
            }

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                ObjectGuid const& casterGuid = GetCaster()->GetGUID();
                targets.remove_if([casterGuid](WorldObject* worldObj) -> bool
                {
                    if (Unit* target = worldObj->ToUnit())
                        if (target->HasAura(SPELL_DRUID_REJUVENATION, casterGuid) || target->HasAura(SPELL_DRUID_GERMINATION_HEAL, casterGuid))
                            return false;
                    return true;
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_DREAMWALKER_HEAL, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_dreamwalker_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_dreamwalker_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_dreamwalker_SpellScript();
        }
};

// 774 - Rejuvenation
// 8936 - Regrowth
// 155777 - Rejuvenation (Germination)
/// @TODO: new proc system
class spell_dru_legion_power_of_the_archdruid_trigger : public SpellScriptLoader
{
    public:
        spell_dru_legion_power_of_the_archdruid_trigger() : SpellScriptLoader("spell_dru_legion_power_of_the_archdruid_trigger") { }

        class spell_dru_legion_power_of_the_archdruid_trigger_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_power_of_the_archdruid_trigger_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_POWER_OF_THE_ARCHDRUID_AURA
                });
            }

            void HandleAfterHit()
            {
                if (GetCaster()->HasAura(SPELL_DRUID_POWER_OF_THE_ARCHDRUID_AURA))
                {
                    GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_POWER_OF_THE_ARCHDRUID_AURA);
                    if (Unit* target = GetExplTargetUnit())
                    {
                        std::list<Unit*> targets;
                        Trinity::AnyFriendlyUnitInObjectRangeCheck checker(target, GetCaster(), 20.0f);
                        Trinity::UnitListSearcher<Trinity::AnyFriendlyUnitInObjectRangeCheck> searcher(target, targets, checker);
                        Cell::VisitAllObjects(target, searcher, 20.0f);
                        targets.remove_if(Trinity::UnitAuraCheck(true, GetSpellInfo()->Id, GetCaster()->GetGUID()));
                        targets.sort(Trinity::ObjectDistanceOrderPred(target));
                        if (targets.size() > 2)
                            targets.resize(2);

                        for (Unit* unitTarget : targets)
                            GetCaster()->CastSpell(unitTarget, GetSpellInfo()->Id, true);
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_power_of_the_archdruid_trigger_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_power_of_the_archdruid_trigger_SpellScript();
        }
};

// 102793 - Ursol's Vortex
class areatrigger_dru_legion_ursols_vortex : public AreaTriggerEntityScript
{
    public:
        areatrigger_dru_legion_ursols_vortex() : AreaTriggerEntityScript("areatrigger_dru_legion_ursols_vortex") { }

        struct areatrigger_dru_legion_ursols_vortexAI : public AreaTriggerAI
        {
            areatrigger_dru_legion_ursols_vortexAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitExit(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!at->IsRemoved() && !caster->HasAura(SPELL_DRUID_URSOLS_VORTEX_GRIP) && !caster->IsFriendlyTo(unit))
                    {
                        unit->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DRUID_URSOLS_VORTEX_GRIP, true, nullptr, nullptr, at->GetCasterGuid());
                        // HACKFIX: spell 118283 should already apply this aura on caster
                        caster->AddAura(SPELL_DRUID_URSOLS_VORTEX_GRIP, caster);
                    }
                }
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_dru_legion_ursols_vortexAI(areaTrigger);
        }
};

// 200850 - Adaptive Fur
class spell_dru_legion_adaptive_fur : public SpellScriptLoader
{
    public:
        spell_dru_legion_adaptive_fur() : SpellScriptLoader("spell_dru_legion_adaptive_fur") { }

        class spell_dru_legion_adaptive_fur_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_adaptive_fur_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_DRUID_ADAPTIVE_FUR_ARCANE,
                    SPELL_DRUID_ADAPTIVE_FUR_SHADOW,
                    SPELL_DRUID_ADAPTIVE_FUR_FROST,
                    SPELL_DRUID_ADAPTIVE_FUR_NATURE,
                    SPELL_DRUID_ADAPTIVE_FUR_FIRE,
                    SPELL_DRUID_ADAPTIVE_FUR_HOLY
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                auto itr = adaptiveFurHelper.find(eventInfo.GetSchoolMask());
                if (itr != adaptiveFurHelper.end())
                    GetTarget()->CastSpell(GetTarget(), itr->second, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_adaptive_fur_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_adaptive_fur_AuraScript();
        }
};

// Guardian
// 77758 - Thrash
class spell_dru_legion_thrash : public SpellScriptLoader
{
    public:
        spell_dru_legion_thrash() : SpellScriptLoader("spell_dru_legion_thrash") { }

        class spell_dru_legion_thrash_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_thrash_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_THRASH_DOT,
                    SPELL_DRUID_BLOOD_FRENZY,
                    SPELL_DRUID_BLOOD_FRENZY_ENERGIZE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                // TODO: check if we need this script for a custom cast (effect 1 + 2) or if spell_linked_spell is enough
                GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_THRASH_DOT, true);

                if (GetCaster()->HasAura(SPELL_DRUID_BLOOD_FRENZY))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_DRUID_BLOOD_FRENZY_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_thrash_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_thrash_SpellScript();
        }
};

// 192090 - Thrash
class spell_dru_legion_thrash_periodic : public SpellScriptLoader
{
    public:
        spell_dru_legion_thrash_periodic() : SpellScriptLoader("spell_dru_legion_thrash_periodic") { }

        class spell_dru_legion_thrash_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_thrash_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BLOOD_FRENZY,
                    SPELL_DRUID_BLOOD_FRENZY_ENERGIZE,
                    SPELL_DRUID_THRASH_PULVERIZE_MARKER
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_DRUID_BLOOD_FRENZY))
                        caster->CastSpell(caster, SPELL_DRUID_BLOOD_FRENZY_ENERGIZE, true);
                }
            }

            void HandleEffectApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetStackAmount() >= 2)
                    if (Unit* caster = GetCaster())
                        caster->CastSpell(GetTarget(), SPELL_DRUID_THRASH_PULVERIZE_MARKER, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_thrash_periodic_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_thrash_periodic_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_thrash_periodic_AuraScript();
        }
};

// 236019 - Tooth and Claw
class spell_dru_legion_tooth_and_claw : public SpellScriptLoader
{
    public:
        spell_dru_legion_tooth_and_claw() : SpellScriptLoader("spell_dru_legion_tooth_and_claw") { }

        class spell_dru_legion_tooth_and_claw_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_tooth_and_claw_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_TOOTH_AND_CLAW_BONUS
                });
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TOOTH_AND_CLAW_BONUS);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_dru_legion_tooth_and_claw_AuraScript::AfterRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_tooth_and_claw_AuraScript();
        }
};

// 33917 - Mangle
class spell_dru_legion_mangle : public SpellScriptLoader
{
    public:
        spell_dru_legion_mangle() : SpellScriptLoader("spell_dru_legion_mangle") { }

        class spell_dru_legion_mangle_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_mangle_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_2))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MANGLE_BLEED_BONUS
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_DRUID_MANGLE_BLEED_BONUS);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->HasAuraState(AURA_STATE_BLEEDING))
                {
                    int32 damage = GetHitDamage();
                    AddPct(damage, GetSpellInfo()->GetEffect(EFFECT_2)->CalcValue());
                    SetHitDamage(damage);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_mangle_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_mangle_SpellScript();
        }
};

// 22842 - Frenzied Regeneration
class spell_dru_legion_frenzied_regeneration : public SpellScriptLoader
{
    public:
        spell_dru_legion_frenzied_regeneration() : SpellScriptLoader("spell_dru_legion_frenzied_regeneration") { }

        class spell_dru_legion_frenzied_regeneration_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_frenzied_regeneration_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_2) || !spellInfo->GetEffect(EFFECT_3))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_DRUID_GUARDIAN_OF_ELUNE_BONUS
                });
            }

            void CalculateHeal()
            {
                if (Player* caster = GetCaster()->ToPlayer())
                {
                    // Based on http://www.askmrrobot.com/wow/theory/mechanic/spell/frenziedregeneration?spec=DruidGuardian&version=live
                    // Build 24015
                    if (AuraEffect* aurEff = caster->GetAuraEffect(GetSpellInfo()->Id, EFFECT_0, caster->GetGUID()))
                    {
                        time_t lastFiveSec = time(nullptr) - GetEffectInfo(EFFECT_2)->CalcValue();
                        uint64 heal = std::max(CalculatePct(caster->GetDamageHistory().CountDamageTaken(lastFiveSec), GetEffectInfo(EFFECT_1)->CalcValue(caster)), caster->CountPctFromMaxHealth(GetEffectInfo(EFFECT_3)->CalcValue()));
                        heal += CalculatePct(heal, caster->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE));

                        if (AuraEffect* wildflesh = caster->GetAuraEffect(SPELL_DRUID_WILDFLESH, EFFECT_0))
                            heal += CalculatePct(heal, wildflesh->GetAmount());

                        heal /= GetSpellInfo()->GetMaxTicks(DIFFICULTY_NONE);

                        if (AuraEffect const* GuardianOfElune = caster->GetAuraEffect(SPELL_DRUID_GUARDIAN_OF_ELUNE_BONUS, EFFECT_1))
                        {
                            heal += CalculatePct(heal, GuardianOfElune->GetAmount());
                            GuardianOfElune->GetBase()->DropCharge();
                        }
                        aurEff->SetDamage(int32(heal));
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_dru_legion_frenzied_regeneration_SpellScript::CalculateHeal);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_frenzied_regeneration_SpellScript();
        }

        class spell_dru_legion_frenzied_regeneration_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_frenzied_regeneration_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_RAGING_FRENZY_HONOR_TALENT,
                    SPELL_DRUID_RAGING_FRENZY_ENERGIZE
                });
            }

            bool Load() override
            {
                return GetUnitOwner()->HasAura(SPELL_DRUID_RAGING_FRENZY_HONOR_TALENT);
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_RAGING_FRENZY_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_frenzied_regeneration_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_HEAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_frenzied_regeneration_AuraScript();
        }
};

// 202771 - Full Moon
class spell_dru_legion_full_moon : public SpellScriptLoader
{
    public:
        spell_dru_legion_full_moon() : SpellScriptLoader("spell_dru_legion_full_moon") { }

        class spell_dru_legion_full_moon_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_full_moon_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = uint32(targets.size());
            }

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit() == GetExplTargetUnit())
                    return;

                SetHitDamage(GetHitDamage() / _targetCount);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_full_moon_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_full_moon_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

        private:
            uint32 _targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_full_moon_SpellScript();
        }
};

// 155835 - Bristling Fur
class spell_dru_legion_bristling_fur : public SpellScriptLoader
{
    public:
        spell_dru_legion_bristling_fur() : SpellScriptLoader("spell_dru_legion_bristling_fur") { }

        class spell_dru_legion_bristling_fur_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_bristling_fur_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BRISTLING_FUR_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 energizeAmount = (100 * eventInfo.GetDamageInfo()->GetDamage() / int32(GetTarget()->GetMaxHealth())) * 10;
                GetTarget()->CastCustomSpell(GetTarget(), SPELL_DRUID_BRISTLING_FUR_ENERGIZE, &energizeAmount, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_bristling_fur_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_bristling_fur_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_bristling_fur_AuraScript();
        }
};

// 203953 - Brambles
class spell_dru_legion_brambles : public SpellScriptLoader
{
    public:
        spell_dru_legion_brambles() : SpellScriptLoader("spell_dru_legion_brambles") { }

        class spell_dru_legion_brambles_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_brambles_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BRAMBLES_DAMAGE,
                    SPELL_DRUID_BARKSKIN
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                absorbAmount = std::min(uint32(GetTarget()->GetTotalAttackPowerValue(BASE_ATTACK) * 0.24f), dmgInfo.GetDamage());

                if (Unit* attacker = dmgInfo.GetAttacker())
                {
                    int32 damage = absorbAmount;
                    GetTarget()->CastCustomSpell(attacker, SPELL_DRUID_BRAMBLES_DAMAGE, &damage, nullptr, nullptr, true, nullptr, aurEff);
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dru_legion_brambles_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_dru_legion_brambles_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_brambles_AuraScript();
        }
};

// 203964 - Galactic Guardian
/// @TODO: new proc system
class spell_dru_legion_galactic_guardian : public SpellScriptLoader
{
    public:
        spell_dru_legion_galactic_guardian() : SpellScriptLoader("spell_dru_legion_galactic_guardian") { }

        class spell_dru_legion_galactic_guardian_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_galactic_guardian_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MOONFIRE_DOT,
                    SPELL_DRUID_BRAMBLES_AOE_DAMAGE,
                    SPELL_DRUID_BRAMBLES_DAMAGE,
                    SPELL_DRUID_LUNAR_BEAM_DAMAGE,
                    SPELL_DRUID_MOONFIRE,
                    SPELL_DRUID_GALACTIC_GUARDIAN_BONUS
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo())
                {
                    uint32 procSpell = eventInfo.GetSpellInfo()->Id;
                    if (procSpell == SPELL_DRUID_MOONFIRE_DOT || procSpell == SPELL_DRUID_BRAMBLES_AOE_DAMAGE || procSpell == SPELL_DRUID_BRAMBLES_DAMAGE
                        || procSpell == SPELL_DRUID_LUNAR_BEAM_DAMAGE)
                        return false;
                }
                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                ///@TODO: Currently this moonfire cast benefits from galactic guardian (if a old bonus is active) but it shouldn't...
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_DRUID_MOONFIRE, true);
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_GALACTIC_GUARDIAN_BONUS, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_galactic_guardian_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_galactic_guardian_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_galactic_guardian_AuraScript();
        }
};

// 164812 - Moonfire
class spell_dru_legion_moonfire_galactic_guardian : public SpellScriptLoader
{
    public:
        spell_dru_legion_moonfire_galactic_guardian() : SpellScriptLoader("spell_dru_legion_moonfire_galactic_guardian") { }

        class spell_dru_legion_moonfire_galactic_guardian_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_moonfire_galactic_guardian_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_GALACTIC_GUARDIAN_BONUS
                });
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_DRUID_GALACTIC_GUARDIAN_BONUS, EFFECT_2))
                {
                    int32 damage = GetHitDamage();
                    AddPct(damage, aurEff->GetAmount());
                    SetHitDamage(damage);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_moonfire_galactic_guardian_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_moonfire_galactic_guardian_SpellScript();
        }
};

// 203975 - Earthwarden
class spell_dru_legion_earthwarden : public SpellScriptLoader
{
    public:
        spell_dru_legion_earthwarden() : SpellScriptLoader("spell_dru_legion_earthwarden") { }

        class spell_dru_legion_earthwarden_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_earthwarden_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_EARTHWARDEN
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                if (dmgInfo.GetDamageType() != DIRECT_DAMAGE)
                {
                    absorbAmount = 0;
                    return;
                }

                if (AuraEffect* earthWardenPeriodic = GetTarget()->GetAuraEffect(SPELL_DRUID_EARTHWARDEN, EFFECT_0))
                {
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), earthWardenPeriodic->GetAmount());
                    if (GetAura()->GetStackAmount() == 1)
                        GetAura()->Remove(AURA_REMOVE_BY_ENEMY_SPELL);
                    else
                        GetAura()->SetStackAmount(GetAura()->GetStackAmount() - 1);
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dru_legion_earthwarden_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_dru_legion_earthwarden_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_earthwarden_AuraScript();
        }
};

// 203974 - Earthwarden
class spell_dru_legion_earthwarden_proc : public SpellScriptLoader
{
    public:
        spell_dru_legion_earthwarden_proc() : SpellScriptLoader("spell_dru_legion_earthwarden_proc") { }

        class spell_dru_legion_earthwarden_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_earthwarden_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_EARTHWARDEN_ABSORB
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo())
                    return false;

                return eventInfo.GetDamageInfo()->GetDamage() != 0;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_EARTHWARDEN_ABSORB, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_dru_legion_earthwarden_proc_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_earthwarden_proc_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_earthwarden_proc_AuraScript();
        }
};

// 80313 - Pulverize
class spell_dru_legion_pulverize : public SpellScriptLoader
{
    public:
        spell_dru_legion_pulverize() : SpellScriptLoader("spell_dru_legion_pulverize") { }

        class spell_dru_legion_pulverize_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_pulverize_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_THRASH_DOT,
                    SPELL_DRUID_PULVERIZE_BONUS,
                    SPELL_DRUID_THRASH_PULVERIZE_MARKER
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Aura* aura = GetHitUnit()->GetAura(SPELL_DRUID_THRASH_DOT, GetCaster()->GetGUID()))
                {
                    if (aura->GetStackAmount() > GetEffectValue())
                        aura->SetStackAmount(aura->GetStackAmount() - uint8(GetEffectValue()));
                    else
                        aura->Remove();
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_PULVERIZE_BONUS, true);
                    GetHitUnit()->RemoveAurasDueToSpell(SPELL_DRUID_THRASH_PULVERIZE_MARKER, GetCaster()->GetGUID());
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_pulverize_SpellScript::HandleDummy, EFFECT_2, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_pulverize_SpellScript();
        }
};

// 210706 - Gore
class spell_dru_legion_gore : public SpellScriptLoader
{
    public:
        spell_dru_legion_gore() : SpellScriptLoader("spell_dru_legion_gore") { }

        class spell_dru_legion_gore_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_gore_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MANGLE_BEAR,
                    SPELL_DRUID_GORE_MANGLE_BONUS
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_DRUID_MANGLE_BEAR, true);
                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_GORE_MANGLE_BONUS, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_dru_legion_gore_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_gore_AuraScript();
        }
};

// 77761 - Stampeding Roar
// 77764 - Stampeding Roar
// 106898 - Stampeding Roar
class spell_dru_legion_stampeding_roar : public SpellScriptLoader
{
    public:
        spell_dru_legion_stampeding_roar() : SpellScriptLoader("spell_dru_legion_stampeding_roar") { }

        class spell_dru_legion_stampeding_roar_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_stampeding_roar_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_BEAR_FORM,
                    SPELL_DRUID_ROAR_OF_THE_CROWD_TRAIT,
                    SPELL_DRUID_ROAR_OF_THE_CROWD_BONUS,
                    SPELL_DRUID_FREEDOM_OF_THE_HERD_HONOR_TALENT
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targetCount = int32(targets.size());
            }

            void HandleHonorTalent(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_DRUID_FREEDOM_OF_THE_HERD_HONOR_TALENT))
                    GetHitUnit()->RemoveMovementImpairingAuras();
            }

            void HandleEffects()
            {
                if (Player* caster = GetCaster()->ToPlayer())
                {
                    if (caster->GetRole() == TALENT_ROLE_TANK && !caster->IsInFeralForm())
                        GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_BEAR_FORM, true);

                    // Blizz send just one aura with the final stack amount, our proc system cant count targets thats why it is directly handled here
                    if (GetCaster()->HasAura(SPELL_DRUID_ROAR_OF_THE_CROWD_TRAIT))
                        GetCaster()->CastCustomSpell(SPELL_DRUID_ROAR_OF_THE_CROWD_BONUS, SPELLVALUE_AURA_STACK, std::min(targetCount, int32(sSpellMgr->AssertSpellInfo(SPELL_DRUID_ROAR_OF_THE_CROWD_BONUS)->StackAmount)), GetCaster(), TRIGGERED_FULL_MASK);
                }
           }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dru_legion_stampeding_roar_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_dru_legion_stampeding_roar_SpellScript::HandleHonorTalent, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
                AfterCast += SpellCastFn(spell_dru_legion_stampeding_roar_SpellScript::HandleEffects);
            }

            private:
                int32 targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_stampeding_roar_SpellScript();
        }
};

// 204053 - Rend and Tear
class spell_dru_legion_rend_and_tear : public SpellScriptLoader
{
    public:
        spell_dru_legion_rend_and_tear() : SpellScriptLoader("spell_dru_legion_rend_and_tear") { }

        class spell_dru_legion_rend_and_tear_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_rend_and_tear_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_THRASH_DOT
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                if (Unit* attacker = dmgInfo.GetAttacker())
                    if (Aura* thrashDot = attacker->GetAura(SPELL_DRUID_THRASH_DOT, GetTarget()->GetGUID()))
                        if (AuraEffect const* reducePct = GetAura()->GetEffect(EFFECT_1))
                        {
                            absorbAmount = CalculatePct(dmgInfo.GetDamage(), (reducePct->GetAmount() * thrashDot->GetStackAmount()));
                            return;
                        }
                absorbAmount = 0;
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dru_legion_rend_and_tear_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_dru_legion_rend_and_tear_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_rend_and_tear_AuraScript();
        }
};

// 200851 - Rage of the Sleeper
class spell_dru_legion_rage_of_the_sleeper : public SpellScriptLoader
{
    public:
        spell_dru_legion_rage_of_the_sleeper() : SpellScriptLoader("spell_dru_legion_rage_of_the_sleeper") { }

        class spell_dru_legion_rage_of_the_sleeper_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_rage_of_the_sleeper_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_RAGE_OF_THE_SLEEPER_DAMAGE
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                if (SpellEffectInfo const* eff3Info = GetSpellInfo()->GetEffect(EFFECT_3))
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), eff3Info->CalcValue(GetTarget()));
                GetTarget()->CastSpell(dmgInfo.GetAttacker(), SPELL_DRUID_RAGE_OF_THE_SLEEPER_DAMAGE, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dru_legion_rage_of_the_sleeper_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_dru_legion_rage_of_the_sleeper_AuraScript::Absorb, EFFECT_1);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_rage_of_the_sleeper_AuraScript();
        }

        class spell_dru_legion_rage_of_the_sleeper_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_rage_of_the_sleeper_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_EMBRACE_OF_THE_NIGHTMARE
                });
            }

            void DisableAura(WorldObject*& target)
            {
                if (!GetCaster()->HasAura(SPELL_DRUID_EMBRACE_OF_THE_NIGHTMARE))
                    target = nullptr;
            }

            void Register() override
            {
                OnObjectTargetSelect += SpellObjectTargetSelectFn(spell_dru_legion_rage_of_the_sleeper_SpellScript::DisableAura, EFFECT_2, TARGET_UNIT_CASTER);
                OnObjectTargetSelect += SpellObjectTargetSelectFn(spell_dru_legion_rage_of_the_sleeper_SpellScript::DisableAura, EFFECT_3, TARGET_UNIT_CASTER);
                OnObjectTargetSelect += SpellObjectTargetSelectFn(spell_dru_legion_rage_of_the_sleeper_SpellScript::DisableAura, EFFECT_4, TARGET_UNIT_CASTER);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_rage_of_the_sleeper_SpellScript();
        }
};

// 204066 - Lunar Beam
class areatrigger_dru_legion_lunar_beam : public AreaTriggerEntityScript
{
    public:
        areatrigger_dru_legion_lunar_beam() : AreaTriggerEntityScript("areatrigger_dru_legion_lunar_beam") { }

        struct areatrigger_dru_legion_lunar_beamAI : public AreaTriggerAI
        {
            areatrigger_dru_legion_lunar_beamAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DRUID_LUNAR_BEAM_DAMAGE, true);
                    task.Repeat();
                });
            }

            void OnUpdate(uint32 diff) override
            {
                scheduler.Update(diff);
            }

        private:
            TaskScheduler scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_dru_legion_lunar_beamAI(areaTrigger);
        }
};

// 783 - Travel Form
class spell_dru_legion_travel_form : public SpellScriptLoader
{
    public:
        spell_dru_legion_travel_form() : SpellScriptLoader("spell_dru_legion_travel_form") { }

        class spell_dru_legion_travel_form_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_travel_form_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_TRAVEL_FORM_FLIGHT_FORM,
                    SPELL_DRUID_TRAVEL_FORM_SWIFT_FLIGHT_FORM,
                    SPELL_DRUID_TRAVEL_FORM_AQUATIC_FORM,
                    SPELL_DRUID_AQUATIC_FORM_PASSIVE,
                    SPELL_DRUID_TRAVEL_FORM,
                    SPELL_MISC_ARTISAN_RIDING,
                    SPELL_MISC_EXPERT_RIDING,
                    SPELL_DRUID_SOUND_EFFECT_STAG_FORM,
                    SPELL_DRUID_TRAVEL_FORM_BONUS
                });
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 250;
            }

            void RemoveFormAuras()
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TRAVEL_FORM_FLIGHT_FORM);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TRAVEL_FORM_SWIFT_FLIGHT_FORM);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TRAVEL_FORM_AQUATIC_FORM);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_AQUATIC_FORM_PASSIVE);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TRAVEL_FORM);
            }

            bool canActivateFlyForm()
            {
                return !GetTarget()->IsInCombat() && (GetTarget()->HasSpell(SPELL_MISC_ARTISAN_RIDING) || GetTarget()->HasSpell(SPELL_MISC_EXPERT_RIDING));
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                if (!GetTarget()->GetMap()->IsOutdoors(GetTarget()->GetPositionX(), GetTarget()->GetPositionY(), GetTarget()->GetPositionZ()))
                {
                    RemoveFormAuras();
                    Remove();
                    return;
                }

                if (GetTarget()->IsInWater() && !GetTarget()->HasAura(SPELL_DRUID_TRAVEL_FORM_AQUATIC_FORM))
                {
                    RemoveFormAuras();
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TRAVEL_FORM_AQUATIC_FORM, true);
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_AQUATIC_FORM_PASSIVE, true);
                }
                else if (!GetTarget()->IsInWater())
                {
                    if (!GetTarget()->HasAura(SPELL_DRUID_TRAVEL_FORM_FLIGHT_FORM) && !GetTarget()->HasAura(SPELL_DRUID_TRAVEL_FORM_SWIFT_FLIGHT_FORM) && !GetTarget()->HasAura(SPELL_DRUID_TRAVEL_FORM))
                    {
                        if (canActivateFlyForm() && GetTarget()->ToPlayer()->CanFlyInZone(GetTarget()->GetMapId(), GetTarget()->GetZoneId()))
                        {
                            RemoveFormAuras();
                            if (GetTarget()->HasSpell(SPELL_MISC_ARTISAN_RIDING))
                                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TRAVEL_FORM_SWIFT_FLIGHT_FORM, true);
                            else if (GetTarget()->HasSpell(SPELL_MISC_EXPERT_RIDING))
                                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TRAVEL_FORM_FLIGHT_FORM, true);
                        }
                        else
                        {
                            RemoveFormAuras();
                            GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_SOUND_EFFECT_STAG_FORM, true);
                            if (AuraEffect* speedBonus = GetTarget()->GetAuraEffect(SPELL_DRUID_TRAVEL_FORM_BONUS, EFFECT_0))
                            {
                                AreaTableEntry const* area = sAreaTableStore.LookupEntry(GetTarget()->GetAreaId());
                                bool isPvpOutdoorArea = area && (area->Flags[0] & AREA_FLAG_ARENA);

                                if (!GetTarget()->GetMap()->IsBattlegroundOrArena() && !isPvpOutdoorArea && !GetTarget()->IsInCombat())
                                {
                                    if (SpellEffectInfo const* travelInfo = sSpellMgr->AssertSpellInfo(SPELL_DRUID_TRAVEL_FORM)->GetEffect(EFFECT_2))
                                        GetTarget()->CastCustomSpell(SPELL_DRUID_TRAVEL_FORM, SPELLVALUE_BASE_POINT2, travelInfo->CalcValue(GetTarget()) + speedBonus->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
                                }
                                else
                                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TRAVEL_FORM, true);
                            }
                            else
                                GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TRAVEL_FORM, true);
                        }
                    }
                }
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                RemoveFormAuras();
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC);
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_dru_legion_travel_form_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dru_legion_travel_form_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_dru_legion_travel_form_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_travel_form_AuraScript::AfterRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_travel_form_AuraScript();
        }

        class spell_dru_legion_travel_form_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_travel_form_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MARK_OF_SHIFTING_TRAIT,
                    SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC
                });
            }

            SpellCastResult CheckIndoor()
            {
                if (!GetCaster()->GetMap()->IsOutdoors(GetCaster()->GetPositionX(), GetCaster()->GetPositionY(), GetCaster()->GetPositionZ()))
                    return SPELL_FAILED_ONLY_OUTDOORS;
                return SPELL_CAST_OK;
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_DRUID_MARK_OF_SHIFTING_TRAIT))
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC, true);
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_dru_legion_travel_form_SpellScript::CheckIndoor);
                AfterCast += SpellCastFn(spell_dru_legion_travel_form_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_travel_form_SpellScript();
        }
};

// 1066 - Travel Form
// 33943 - Travel Form
// 40120 - Travel Form
// 165961 - Travel Form
class spell_dru_legion_druid_travel_forms : public SpellScriptLoader
{
    public:
        spell_dru_legion_druid_travel_forms() : SpellScriptLoader("spell_dru_legion_druid_travel_forms") { }

        class spell_dru_legion_druid_travel_forms_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_druid_travel_forms_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_TRAVEL_FORM_CONTROLLER
                });
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEFAULT)
                    caster->RemoveAurasDueToSpell(SPELL_DRUID_TRAVEL_FORM_CONTROLLER);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_druid_travel_forms_AuraScript::AfterRemove, EFFECT_FIRST_FOUND, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_druid_travel_forms_AuraScript();
        }
};

// 768 - Cat Form
// 5487 - Bear Form
// 24858 - Moonkin Form
// 197625 - Moonkin Form
class spell_dru_legion_druid_shapeshift_forms : public SpellScriptLoader
{
    public:
        spell_dru_legion_druid_shapeshift_forms() : SpellScriptLoader("spell_dru_legion_druid_shapeshift_forms") { }

        class spell_dru_legion_druid_shapeshift_forms_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_druid_shapeshift_forms_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_TRAVEL_FORM_CONTROLLER,
                    SPELL_DRUID_MARK_OF_SHIFTING_TRAIT,
                    SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->RemoveAurasDueToSpell(SPELL_DRUID_TRAVEL_FORM_CONTROLLER);

                if (GetCaster()->HasAura(SPELL_DRUID_MARK_OF_SHIFTING_TRAIT))
                    GetCaster()->CastSpell(GetCaster(), SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_dru_legion_druid_shapeshift_forms_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_druid_shapeshift_forms_SpellScript();
        }

        class spell_dru_legion_druid_shapeshift_forms_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_druid_shapeshift_forms_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC
                });
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_MARK_OF_SHIFTING_PERIODIC);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_druid_shapeshift_forms_AuraScript::AfterRemove, EFFECT_FIRST_FOUND, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_druid_shapeshift_forms_AuraScript();
        }
};

// 18960 - Teleport: Moonglade
class spell_dru_legion_teleport_moonglade : public SpellScriptLoader
{
    public:
        spell_dru_legion_teleport_moonglade() : SpellScriptLoader("spell_dru_legion_teleport_moonglade") { }

        class spell_dru_legion_teleport_moonglade_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dru_legion_teleport_moonglade_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void SetDest(SpellDestination& dest)
            {
                if (GetCaster()->GetZoneId() == ZONE_MOONGLADE)
                    dest._position = GetCaster()->ToPlayer()->GetRecallPosition();
                else
                    GetCaster()->ToPlayer()->SaveRecallPosition();
            }

            void Register() override
            {
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_dru_legion_teleport_moonglade_SpellScript::SetDest, EFFECT_0, TARGET_DEST_DB);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dru_legion_teleport_moonglade_SpellScript();
        }
};

// 5487 - Bear Form
class spell_dru_legion_bear_form : public SpellScriptLoader
{
    public:
        spell_dru_legion_bear_form() : SpellScriptLoader("spell_dru_legion_bear_form") { }

        class spell_dru_legion_bear_form_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dru_legion_bear_form_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DRUID_CELESTIAL_GUARDIAN,
                    SPELL_DRUID_CELESTIAL_GUARDIAN_BONUS,
                    SPELL_DRUID_TOOTH_AND_CLAW_HONOR_TALENT,
                    SPELL_DRUID_TOOTH_AND_CLAW_BONUS
                });
            }

            void AfterApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_DRUID_CELESTIAL_GUARDIAN))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_CELESTIAL_GUARDIAN_BONUS, true);

                if (GetTarget()->HasAura(SPELL_DRUID_TOOTH_AND_CLAW_HONOR_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_DRUID_TOOTH_AND_CLAW_BONUS, true);

                GetTarget()->SetPower(POWER_RAGE, 200); // rage is always set to 20 after entering bear form
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_CELESTIAL_GUARDIAN_BONUS);
                GetTarget()->RemoveAurasDueToSpell(SPELL_DRUID_TOOTH_AND_CLAW_BONUS);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_dru_legion_bear_form_AuraScript::AfterApply, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_dru_legion_bear_form_AuraScript::AfterRemove, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dru_legion_bear_form_AuraScript();
        }
};

class npc_dru_legion_force_of_nature : public CreatureScript
{
    public:
        npc_dru_legion_force_of_nature() : CreatureScript("npc_dru_legion_force_of_nature") { }

        struct npc_dru_legion_force_of_natureAI : public ScriptedAI
        {
            npc_dru_legion_force_of_natureAI(Creature* creature) : ScriptedAI(creature) { }

            void IsSummonedBy(Unit* owner)  override
            {
                if (Unit* target = owner->getAttackerForHelper())
                    me->AI()->AttackStart(target);

                _scheduler.Schedule(Milliseconds(500), [this](TaskContext task)
                {
                    DoCastVictim(SPELL_DRUID_FORCE_OF_NATURE_TAUNT);
                    task.Repeat(Seconds(1));
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);

                if (!UpdateVictim())
                    return;

                DoMeleeAttackIfReady();
            }

        private:
            TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_dru_legion_force_of_natureAI(creature);
        }
};

void AddSC_legion_druid_spell_scripts()
{
    new spell_dru_legion_abundance();
    new spell_dru_legion_adaptive_fur();
    new spell_dru_legion_ashamanes_bite();
    new spell_dru_legion_ashamanes_rip();
    new spell_dru_legion_ashamanes_energy();
    new spell_dru_legion_ashamanes_frenzy();
    new spell_dru_legion_ashamanes_frenzy_periodic();
    new spell_dru_legion_barkskin();
    new spell_dru_legion_bear_form();
    new spell_dru_legion_blessing_of_the_ancients();
    new spell_dru_legion_bloodtalons();
    new spell_dru_legion_bloody_paws();
    new spell_dru_legion_brambles();
    new spell_dru_legion_bristling_fur();
    new spell_dru_legion_brutal_slash();
    new spell_dru_legion_cat_form();
    new spell_dru_legion_celestial_alignment();
    new spell_dru_legion_cenarion_ward();
    new spell_dru_legion_clan_defender();
    new spell_dru_legion_cultivation();
    new spell_dru_legion_displacer_beast();
    new spell_dru_legion_dreamwalker();
    new spell_dru_legion_druid_of_the_claw();
    new spell_dru_legion_druid_shapeshift_forms();
    new spell_dru_legion_druid_travel_forms();
    new spell_dru_legion_earthwarden();
    new spell_dru_legion_earthwarden_proc();
    new spell_dru_legion_echoing_stars();
    new spell_dru_legion_efflorescence();
    new spell_dru_legion_efflorescence_heal();
    new spell_dru_legion_efflorescence_periodic();
    new spell_dru_legion_empowerment();
    new spell_dru_legion_enraged_maim();
    new spell_dru_legion_entangling_roots();
    new spell_dru_legion_feral_instinct();
    new spell_dru_legion_ferocious_bite();
    new spell_dru_legion_flourish();
    new spell_dru_legion_frenzied_regeneration();
    new spell_dru_legion_full_moon();
    new spell_dru_legion_fury_of_elune_override();
    new spell_dru_legion_fury_of_elune_periodic();
    new spell_dru_legion_galactic_guardian();
    new spell_dru_legion_gore();
    new spell_dru_legion_healing_touch();
    new spell_dru_legion_incarnation_chosen_of_elune();
    new spell_dru_legion_incarnation_tree_of_life();
    new spell_dru_legion_innervate();
    new spell_dru_legion_infected_wounds();
    new spell_dru_legion_ironfur();
    new spell_dru_legion_killer_instinct();
    new spell_dru_legion_king_of_the_jungle();
    new spell_dru_legion_lifebloom();
    new spell_dru_legion_living_seed_passive();
    new spell_dru_legion_living_seed_triggered();
    new spell_dru_legion_lunar_strike();
    new spell_dru_legion_maim();
    new spell_dru_legion_mangle();
    new spell_dru_legion_mark_of_shifting_periodic();
    new spell_dru_legion_master_shapeshifter();
    new spell_dru_legion_moonkin_form_balance_affinity();
    new spell_dru_legion_moon_and_stars();
    new spell_dru_legion_moon_and_stars_dps_proc();
    new spell_dru_legion_moon_and_stars_remove();
    new spell_dru_legion_moonfire();
    new spell_dru_legion_moonfire_galactic_guardian();
    new spell_dru_legion_moonfire_sunfire_initial();
    new spell_dru_legion_moonkin_form();
    new spell_dru_legion_natures_balance();
    new spell_dru_legion_overgrowth();
    new spell_dru_legion_power_of_goldrinn();
    new spell_dru_legion_power_of_the_archdruid_trigger();
    new spell_dru_legion_predator();
    new spell_dru_legion_predatory_swiftness();
    new spell_dru_legion_predatory_swiftness_helper();
    new spell_dru_legion_pulverize();
    new spell_dru_legion_rage_of_the_sleeper();
    new spell_dru_legion_rake();
    new spell_dru_legion_rapid_innervation();
    new spell_dru_legion_regrowth();
    new spell_dru_legion_rejuvenation_soul_of_the_forest();
    new spell_dru_legion_rend_and_tear();
    new spell_dru_legion_revitalize();
    new spell_dru_legion_rip();
    new spell_dru_legion_rip_and_tear();
    new spell_dru_legion_savage_roar();
    new spell_dru_legion_shadow_thrash();
    new spell_dru_legion_shadow_thrash_periodic();
    new spell_dru_legion_shooting_stars();
    new spell_dru_legion_shred();
    new spell_dru_legion_skull_bash_interrupt();
    new spell_dru_legion_solar_beam();
    new spell_dru_legion_solar_wrath();
    new spell_dru_legion_soul_of_the_forest();
    new spell_dru_legion_stampeding_roar();
    new spell_dru_legion_starfall_damage();
    new spell_dru_legion_sunfire();
    new spell_dru_legion_survival_instincts();
    new spell_dru_legion_swiftmend();
    new spell_dru_legion_swipe();
    new spell_dru_legion_teleport_moonglade();
    new spell_dru_legion_thorns_damage();
    new spell_dru_legion_thrash();
    new spell_dru_legion_thrash_feral();
    new spell_dru_legion_thrash_periodic();
    new spell_dru_legion_tooth_and_claw();
    new spell_dru_legion_tranquility();
    new spell_dru_legion_travel_form();
    new spell_dru_legion_wild_growth();
    new spell_dru_legion_wild_growth_soul_of_the_forest();
    new spell_dru_legion_yseras_gift_party_heal();
    new spell_dru_legion_yseras_gift_periodic();

    new areatrigger_dru_legion_lunar_beam();
    new areatrigger_dru_legion_starfall();
    new areatrigger_dru_legion_ursols_vortex();

    new npc_dru_legion_force_of_nature();
}
