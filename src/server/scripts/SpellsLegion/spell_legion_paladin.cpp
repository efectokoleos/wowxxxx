/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_PALADIN and SPELLFAMILY_GENERIC spells used by paladin players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_pal_".
 */

#include "AreaTriggerAI.h"
#include "Cell.h"
#include "CellImpl.h"
#include "Common.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"

enum Spells
{
    SPELL_PALADIN_ASHES_TO_ASHES                        = 179546,
    SPELL_PALADIN_AURA_MASTERY                          = 31821,
    SPELL_PALADIN_AURA_OF_MERCY                         = 183415,
    SPELL_PALADIN_AURA_OF_SACRIFICE                     = 183416,
    SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB              = 210372,
    SPELL_PALADIN_AURA_OF_SACRIFICE_DAMAGE              = 210380,
    SPELL_PALADIN_AURA_OF_SACRIFICE_HEAL                = 210383,
    SPELL_PALADIN_AVENGERS_SHIELD                       = 31935,
    SPELL_PALADIN_AVENGING_CRUSADER_GROUP_HEAL          = 216371,
    SPELL_PALADIN_AVENGING_CRUSADER_SELF_HEAL           = 216372,
    SPELL_PALADIN_AVENGING_LIGHT_HEAL                   = 199443,
    SPELL_PALADIN_AVENGING_WRATH                        = 31884,
    SPELL_PALADIN_BEACON_OF_FAITH                       = 156910,
    SPELL_PALADIN_BEACON_OF_LIGHT                       = 53563,
    SPELL_PALADIN_BEACON_OF_LIGHT_ENERGIZE              = 88852,
    SPELL_PALADIN_BEACON_OF_LIGHT_HEAL                  = 53652,
    SPELL_PALADIN_BEACON_OF_LIGHT_MANA_REFUND_BONUS     = 231642,
    SPELL_PALADIN_BEACON_OF_VIRTUE                      = 200025,
    SPELL_PALADIN_BLADE_OF_JUSTICE                      = 184575,
    SPELL_PALADIN_BLESSED_HAMMER_DAMAGE                 = 204301,
    SPELL_PALADIN_BLESSING_OF_FREEDOM                   = 1044,
    SPELL_PALADIN_BLESSING_OF_PROTECTION                = 1022,
    SPELL_PALADIN_BLINDING_LIGHT_TRIGGERED              = 105421,
    SPELL_PALADIN_CLEANSE_THE_WEAK_DISPEL               = 199360,
    SPELL_PALADIN_CLEANSE_THE_WEAK_HONOR_TALENT         = 199330,
    SPELL_PALADIN_CLEANSE_THE_WEAK_VISUAL               = 228473,
    SPELL_PALADIN_CONSECRATION_HOLY_PROT                = 26573,
    SPELL_PALADIN_CONSECRATION_PROT_BONUS               = 188370,
    SPELL_PALADIN_CONSERCRATED_DEBUFF                   = 204242,
    SPELL_PALADIN_CONSERCRATED_GROUND                   = 204054,
    SPELL_PALADIN_CONSERCRATED_HAMMER                   = 203785,
    SPELL_PALADIN_CONSERCRATED_HEAL                     = 204241,
    SPELL_PALADIN_CONSERCRATION_DAMAGE                  = 81297,
    SPELL_PALADIN_CRUSADE                               = 231895,
    SPELL_PALADIN_CRUSADER_STRIKE                       = 35395,
    SPELL_PALADIN_CRUSADERS_JUDGMENT                    = 204023,
    SPELL_PALADIN_DARKEST_BEFORE_THE_DAWN_BONUS         = 210391,
    SPELL_PALADIN_DEVOTION_AURA                         = 210320,
    SPELL_PALADIN_DEVOTION_AURA_AREATRIGGER             = 183425,
    SPELL_PALADIN_DIVINE_HAMMER                         = 198034,
    SPELL_PALADIN_DIVINE_PUNISHER_ENERGIZE              = 216762,
    SPELL_PALADIN_DIVINE_PURPOSE_HOLY_SHOCK             = 216411,
    SPELL_PALADIN_DIVINE_PURPOSE_LIGHT_OF_DAWN          = 216413,
    SPELL_PALADIN_DIVINE_SHIELD                         = 642,
    SPELL_PALADIN_DIVINE_STEED_BLOODELF                 = 221886,
    SPELL_PALADIN_DIVINE_STEED_DRAENEI                  = 221887,
    SPELL_PALADIN_DIVINE_STEED_HUMAN                    = 221883,
    SPELL_PALADIN_DIVINE_STEED_TAUREN                   = 221885,
    SPELL_PALADIN_DIVINE_STORM_DAMAGE                   = 224239,
    SPELL_PALADIN_DIVINE_TEMPEST_SUMMON                 = 186775,
    SPELL_PALADIN_DIVINE_TEMPEST_TRAIT                  = 186773,
    SPELL_PALADIN_EYE_FOR_AN_EYE_TRIGGERED_SPELL        = 205202,
    SPELL_PALADIN_FAITH_S_ARMOR_TRAIT_AURA              = 209225,
    SPELL_PALADIN_FERVENT_MARTYR                        = 223316,
    SPELL_PALADIN_FINAL_STAND                           = 204077,
    SPELL_PALADIN_FINAL_STAND_TAUNT                     = 204079,
    SPELL_PALADIN_FIRST_AVENGER                         = 203776,
    SPELL_PALADIN_FIST_OF_JUSTICE_RETRI                 = 234299,
    SPELL_PALADIN_FLASH_OF_LIGHT                        = 19750,
    SPELL_PALADIN_FORBEARANCE                           = 25771,
    SPELL_PALADIN_GRAND_CRUSADER_BONUS                  = 85416,
    SPELL_PALADIN_GREATER_BLESSING_OF_KINGS             = 203538,
    SPELL_PALADIN_GREATER_BLESSING_OF_WISDOM            = 203539,
    SPELL_PALADIN_GREATER_JUDGMENT                      = 218178,
    SPELL_PALADIN_HALLOWED_GROUND_HONOR_TALENT          = 216868,
    SPELL_PALADIN_HALLOWED_GROUND_IMMUNITY              = 236198,
    SPELL_PALADIN_HAMMER_OF_JUSTICE                     = 853,
    SPELL_PALADIN_HAMMER_OF_RECKONING                   = 204939,
    SPELL_PALADIN_HAMMER_OF_RECKONING_BONUS             = 204940,
    SPELL_PALADIN_HAMMER_OF_THE_RIGHTEOUS               = 53595,
    SPELL_PALADIN_HAMMER_OF_THE_RIGHTEOUS_AOE           = 88263,
    SPELL_PALADIN_HAND_OF_HINDRANCE                     = 183218,
    SPELL_PALADIN_HAND_OF_THE_PROTECTOR                 = 213652,
    SPELL_PALADIN_HEALING_STORM_HEAL                    = 215257,
    SPELL_PALADIN_HEALING_STORM_TRAIT                   = 193058,
    SPELL_PALADIN_HOLY_LIGHT                            = 82326,
    SPELL_PALADIN_HOLY_POWER_ENERGIZE                   = 228231,
    SPELL_PALADIN_HOLY_PRISM_AREA_VISUAL                = 121551,
    SPELL_PALADIN_HOLY_PRISM_TARGET_ALLY                = 114871,
    SPELL_PALADIN_HOLY_PRISM_TARGET_ENEMY               = 114852,
    SPELL_PALADIN_HOLY_PRISM_TARGET_VISUAL              = 114862,
    SPELL_PALADIN_HOLY_RITUAL_HEAL                      = 199423,
    SPELL_PALADIN_HOLY_RITUAL_HONOR_TALENT              = 199422,
    SPELL_PALADIN_HOLY_SHOCK                            = 20473,
    SPELL_PALADIN_HOLY_SHOCK_DAMAGE                     = 25912,
    SPELL_PALADIN_HOLY_SHOCK_HEAL                       = 25914,
    SPELL_PALADIN_JUDGMENT                              = 20271,
    SPELL_PALADIN_JUDGMENT_ENERGIZE                     = 220637,
    SPELL_PALADIN_JUDGMENT_EXTRA_TARGET                 = 228288,
    SPELL_PALADIN_JUDGMENT_HOLY_DEBUFF                  = 214222,
    SPELL_PALADIN_JUDGMENT_HOLY_DEBUFF_MARKER           = 231644,
    SPELL_PALADIN_JUDGMENT_PROT_BONUS                   = 231657,
    SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF           = 197277,
    SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF_MARKER    = 231663,
    SPELL_PALADIN_JUDGMENT_TARGET_BONUS                 = 231661,
    SPELL_PALADIN_KNIGHT_OF_THE_SILVER_HAND_BONUS       = 211422,
    SPELL_PALADIN_KNIGHT_OF_THE_SILVER_HAND_TRAIT       = 200302,
    SPELL_PALADIN_LAST_DEFENDER                         = 203791,
    SPELL_PALADIN_LAW_AND_ORDER_TRAIT                   = 204934,
    SPELL_PALADIN_LAY_ON_HANDS                          = 633,
    SPELL_PALADIN_LIGHT_OF_DAWN                         = 85222,
    SPELL_PALADIN_LIGHT_OF_DAWN_HEAL                    = 225311,
    SPELL_PALADIN_LIGHT_OF_THE_MARTYR                   = 183998,
    SPELL_PALADIN_LIGHT_OF_THE_MARTYR_DAMAGE            = 196917,
    SPELL_PALADIN_LIGHT_OF_THE_PROTECTOR                = 184092,
    SPELL_PALADIN_LUMINESCENCE_HEAL                     = 199435,
    SPELL_PALADIN_POWER_OF_THE_SILVER_HAND_BUFF         = 200657,
    SPELL_PALADIN_PURE_OF_HEART_DISPEL                  = 199427,
    SPELL_PALADIN_RETRIBUTION                           = 183436,
    SPELL_PALADIN_RETRIBUTION_AURA_DAMAGE               = 204011,
    SPELL_PALADIN_RIGHTEOUS_PROTECTOR                   = 204074,
    SPELL_PALADIN_SCATTER_THE_SHADOWS_TRAIT             = 209223,
    SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS               = 53600,
    SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS_BONUS         = 132403,
    SPELL_PALADIN_SHIELD_OF_VENGEANCE_DAMAGE            = 184689,
    SPELL_PALADIN_SPREADING_THE_WORD_FREEDOM            = 199508,
    SPELL_PALADIN_SPREADING_THE_WORD_PROTECTION         = 199507,
    SPELL_PALADIN_STEED_OF_GLORY_HONOR_TALENT           = 199542,
    SPELL_PALADIN_STEED_OF_GLORY_IMMUNITY               = 199545,
    SPELL_PALADIN_TEMPLARS_VERDICT_DAMAGE               = 224266,
    SPELL_PALADIN_THE_LIGHT_SAVES_BLOCKER               = 211426,
    SPELL_PALADIN_THE_LIGHT_SAVES_BONUS                 = 200423,
    SPELL_PALADIN_THE_LIGHT_SAVES_TRAIT                 = 200421,
    SPELL_PALADIN_TYRS_DELIVERANCE_HEAL                 = 200654,
    SPELL_PALADIN_ULTIMATE_SACRIFICE_DOT                = 199450,
    SPELL_PALADIN_UNBREAKABLE_WILL_BLOCKER              = 182496,
    SPELL_PALADIN_UNBREAKABLE_WILL_CC_INFO              = 182497,
    SPELL_PALADIN_UNBREAKABLE_WILL_CC_REMOVE            = 182531,
    SPELL_PALADIN_VENGEANCE_AURA_BONUS                  = 210324,
    SPELL_PALADIN_WAKE_OF_ASHES_ENERGIZE                = 218001,
    SPELL_PALADIN_WAKE_OF_ASHES_STUN                    = 205290,
    SPELL_PALADIN_WORD_OF_GLORY_SELFHEAL                = 214894,
};

enum PaladinActions
{
    ACTION_AURA_MASTERY_APPLY   = 1,
    ACTION_AURA_MASTERY_REMOVE  = 2
};

enum PaladinMisc
{
    VISUAL_KIT_DIVINE_STORM = 73892,
};

// 31850 - Ardent Defender
class spell_pal_legion_ardent_defender : public SpellScriptLoader
{
    public:
        spell_pal_legion_ardent_defender() : SpellScriptLoader("spell_pal_legion_ardent_defender") { }

        class spell_pal_legion_ardent_defender_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_ardent_defender_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_1)->IsAura(SPELL_AURA_DUMMY))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_DIVINE_SHIELD,
                    SPELL_PALADIN_FORBEARANCE
                });
            }

            void CalculateAmount(AuraEffect const* aurEff, int32 & amount, bool & /*canBeRecalculated*/)
            {
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
                amount = -1;
            }

            void HandleAbsorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                absorbAmount = CalculatePct(dmgInfo.GetDamage(), aurEff->GetBaseAmount());

                if (dmgInfo.GetDamage() >= GetTarget()->GetHealth())
                {
                    absorbAmount = dmgInfo.GetDamage();
                    GetTarget()->SetHealth(GetTarget()->CountPctFromMaxHealth(GetEffect(EFFECT_1)->GetAmount()));
                    Remove(AURA_REMOVE_BY_ENEMY_SPELL);
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_ardent_defender_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_ardent_defender_AuraScript::HandleAbsorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_ardent_defender_AuraScript();
        }
};

// 31821 - Aura Mastery
class spell_pal_legion_aura_mastery : public SpellScriptLoader
{
    public:
        spell_pal_legion_aura_mastery() : SpellScriptLoader("spell_pal_legion_aura_mastery") { }

        class spell_pal_legion_aura_mastery_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_aura_mastery_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_DEVOTION_AURA_AREATRIGGER
                });
            }

            void HandleAreatriggerApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (AreaTrigger* devotionTrigger = GetTarget()->GetAreaTrigger(SPELL_PALADIN_DEVOTION_AURA_AREATRIGGER))
                    devotionTrigger->AI()->DoAction(ACTION_AURA_MASTERY_APPLY);
            }

            void HandleAreatriggersRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (AreaTrigger* devotionTrigger = GetTarget()->GetAreaTrigger(SPELL_PALADIN_DEVOTION_AURA_AREATRIGGER))
                    devotionTrigger->AI()->DoAction(ACTION_AURA_MASTERY_REMOVE);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_pal_legion_aura_mastery_AuraScript::HandleAreatriggerApply, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_pal_legion_aura_mastery_AuraScript::HandleAreatriggersRemove, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_aura_mastery_AuraScript();
        }
};

// 210291 - Aura of Mercy
class spell_pal_legion_aura_of_mercy : public SpellScriptLoader
{
    public:
        spell_pal_legion_aura_of_mercy() : SpellScriptLoader("spell_pal_legion_aura_of_mercy") { }

        class spell_pal_legion_aura_of_mercy_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_aura_of_mercy_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1)
                    || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_AURA_MASTERY))
                    return false;
                return true;
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                uint32 const maxTargets = GetEffectInfo(EFFECT_1)->CalcValue();

                if (targets.size() > maxTargets && !GetCaster()->HasAura(SPELL_PALADIN_AURA_MASTERY, GetCaster()->GetGUID()))
                {
                    targets.sort(Trinity::HealthPctOrderPred());
                    targets.resize(maxTargets);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_aura_of_mercy_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_aura_of_mercy_SpellScript();
        }
};

// 183416 - Aura of Sacrifice
class spell_pal_legion_aura_of_sacrifice : public SpellScriptLoader
{
    public:
        spell_pal_legion_aura_of_sacrifice() : SpellScriptLoader("spell_pal_legion_aura_of_sacrifice") { }

        class spell_pal_legion_aura_of_sacrifice_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_aura_of_sacrifice_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_AURA_MASTERY)
                    || !spellInfo->GetEffect(EFFECT_3))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetHealInfo() || !eventInfo.GetSpellInfo() || eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_BEACON_OF_LIGHT_HEAL)
                    return false;

                // Only count caster heals
                if (eventInfo.GetActor()->GetGUID() != GetCasterGUID())
                    return false;

                return GetTarget()->HasAura(SPELL_PALADIN_AURA_MASTERY, GetCasterGUID()) && eventInfo.GetHealInfo()->GetHeal() > 0;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 healAmount = CalculatePct(eventInfo.GetHealInfo()->GetHeal(), GetAura()->GetEffect(EFFECT_3)->GetAmount());
                if (AreaTrigger* at = GetTarget()->GetAreaTrigger(SPELL_PALADIN_AURA_OF_SACRIFICE))
                    for (ObjectGuid guid : at->GetInsideUnits())
                        if (Unit* target = ObjectAccessor::GetUnit(*GetTarget(), guid))
                            if (target->HasAura(SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB, GetTarget()->GetGUID()))
                                target->CastCustomSpell(target, SPELL_PALADIN_AURA_OF_SACRIFICE_HEAL, &healAmount, nullptr, nullptr, true, nullptr, aurEff, GetTarget()->GetGUID());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_aura_of_sacrifice_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_aura_of_sacrifice_AuraScript::HandleEffectProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_aura_of_sacrifice_AuraScript();
        }
};

// 210372 - Aura of Sacrifice
class spell_pal_legion_aura_of_sacrifice_absorb : public SpellScriptLoader
{
    public:
        spell_pal_legion_aura_of_sacrifice_absorb() : SpellScriptLoader("spell_pal_legion_aura_of_sacrifice_absorb") { }

        class spell_pal_legion_aura_of_sacrifice_absorb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_aura_of_sacrifice_absorb_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_AURA_OF_SACRIFICE,
                    SPELL_PALADIN_AURA_OF_SACRIFICE_DAMAGE
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (Unit* caster = GetCaster())
                {
                    if (SpellInfo const* auraOfSacrifice = sSpellMgr->GetSpellInfo(SPELL_PALADIN_AURA_OF_SACRIFICE))
                    {
                        SpellEffectInfo const* healthInfo = auraOfSacrifice->GetEffect(EFFECT_2);
                        SpellEffectInfo const* absorbPct = auraOfSacrifice->GetEffect(EFFECT_0);

                        if (!healthInfo || !absorbPct)
                            return;

                        if (caster->GetHealthPct() < healthInfo->CalcValue(caster))
                            return;

                        absorbAmount = CalculatePct(dmgInfo.GetDamage(), absorbPct->CalcValue(caster));
                        int32 damage = int32(absorbAmount);
                        caster->CastCustomSpell(caster, SPELL_PALADIN_AURA_OF_SACRIFICE_DAMAGE, &damage, nullptr, nullptr, true);
                    }
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_aura_of_sacrifice_absorb_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_aura_of_sacrifice_absorb_AuraScript::HandleAbsorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_aura_of_sacrifice_absorb_AuraScript();
        }
};

// 216331 - Avenging Crusader
class spell_pal_legion_avenging_crusader : public SpellScriptLoader
{
    public:
        spell_pal_legion_avenging_crusader() : SpellScriptLoader("spell_pal_legion_avenging_crusader") { }

        class spell_pal_legion_avenging_crusader_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_avenging_crusader_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_AVENGING_CRUSADER_SELF_HEAL,
                    SPELL_PALADIN_AVENGING_CRUSADER_GROUP_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastCustomSpell(SPELL_PALADIN_AVENGING_CRUSADER_SELF_HEAL, SPELLVALUE_BASE_POINT0, CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()), GetTarget(), TRIGGERED_FULL_MASK);
                GetTarget()->CastCustomSpell(SPELL_PALADIN_AVENGING_CRUSADER_GROUP_HEAL, SPELLVALUE_BASE_POINT0, CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_avenging_crusader_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_avenging_crusader_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_avenging_crusader_AuraScript();
        }
};

// 216371 - Avenging Crusader (Level 110)
class spell_pal_legion_avenging_crusader_group_heal : public SpellScriptLoader
{
    public:
        spell_pal_legion_avenging_crusader_group_heal() : SpellScriptLoader("spell_pal_legion_avenging_crusader_group_heal") { }

        class spell_pal_legion_avenging_crusader_group_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_avenging_crusader_group_heal_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetCaster());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_avenging_crusader_group_heal_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_avenging_crusader_group_heal_SpellScript();
        }
};

// 31935 - Avenger's Shield
class spell_pal_legion_avengers_shield : public SpellScriptLoader
{
    public:
        spell_pal_legion_avengers_shield() : SpellScriptLoader("spell_pal_legion_avengers_shield") { }

        class spell_pal_legion_avengers_shield_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_avengers_shield_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_FIRST_AVENGER
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                // After the last chain target is hit the shield is flying back to the caster
                // (Cast)[0] HitTarget: Full: 0x203AE8BAA05E52C0004BEF00016BA397 Creature / 0 R3770 / S19439 Map : Vault of the Wardens Entry : Felsworn Infester Low : 23831447
                // (Cast)[1] HitTarget : Full : 0x203AE8BAA0609B80004BEF00026BA397 Creature / 0 R3770 / S19439 Map : Vault of the Wardens Entry : 98926 (Shadow Hunter) Low : 40608663
                // (Cast)[2] HitTarget : Full : 0x203AE8BAA0609B80004BEF0000EBA397 Creature / 0 R3770 / S19439 Map : Vault of the Wardens Entry : 98926 (Shadow Hunter) Low : 15442839
                // (Cast)[3] HitTarget : Full : 0x0839DC00000000000000000005DB6997 Player / 0 R3703 / S0 Map : Eastern Kingdoms Low : 98265495
                targets.push_back(GetCaster());
            }

            void PreventCasterDamage(SpellEffIndex effIndex)
            {
                // Prevent self damage
                if (GetHitUnit() == GetCaster())
                    PreventHitDefaultEffect(effIndex);
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (_firstTarget)
                {
                    _firstTarget = false;
                    uint32 damage = GetHitDamage();
                    if (AuraEffect const* firstAvenger = GetCaster()->GetAuraEffect(SPELL_PALADIN_FIRST_AVENGER, EFFECT_0))
                        AddPct(damage, firstAvenger->GetAmount());
                    SetHitDamage(damage);
                }
            }

            void HandleEffect(SpellEffIndex effIndex)
            {
                // interrupting the non-Player target for $d
                if (GetHitUnit()->GetTypeId() != TYPEID_UNIT)
                    PreventHitDefaultEffect(effIndex);
            }

            void HandleAfterHit()
            {
                // silencing the non-Player target for $d
                if (GetHitUnit()->GetTypeId() != TYPEID_UNIT)
                    PreventHitAura();
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_avengers_shield_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_TARGET_ENEMY);
                OnEffectLaunchTarget += SpellEffectFn(spell_pal_legion_avengers_shield_SpellScript::PreventCasterDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_avengers_shield_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnEffectLaunchTarget += SpellEffectFn(spell_pal_legion_avengers_shield_SpellScript::HandleEffect, EFFECT_1, SPELL_EFFECT_INTERRUPT_CAST);
                AfterHit += SpellHitFn(spell_pal_legion_avengers_shield_SpellScript::HandleAfterHit);
            }

        private:
            bool _firstTarget = true;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_avengers_shield_SpellScript();
        }
};

// 199441 - Avenging Light
class spell_pal_legion_avenging_light : public SpellScriptLoader
{
    public:
        spell_pal_legion_avenging_light() : SpellScriptLoader("spell_pal_legion_avenging_light") { }

        class spell_pal_legion_avenging_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_avenging_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_AVENGING_LIGHT_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetHealInfo() != nullptr;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastCustomSpell(SPELL_PALADIN_AVENGING_LIGHT_HEAL, SPELLVALUE_BASE_POINT0, CalculatePct(eventInfo.GetHealInfo()->GetHeal(), aurEff->GetAmount()), eventInfo.GetActionTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_avenging_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_avenging_light_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_avenging_light_AuraScript();
        }
};

// 204035 - Bastion of Light
class spell_pal_legion_bastion_of_light : public SpellScriptLoader
{
    public:
        spell_pal_legion_bastion_of_light() : SpellScriptLoader("spell_pal_legion_bastion_of_light") { }

        class spell_pal_legion_bastion_of_light_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_bastion_of_light_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS
                });
            }

            void ResetCharges(SpellEffIndex /*effIndex*/)
            {
                if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS))
                    GetCaster()->GetSpellHistory()->ResetCharges(spellInfo->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_bastion_of_light_SpellScript::ResetCharges, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_bastion_of_light_SpellScript();
        }
};

// 53563  - Beacon of Light
// 156910 - Beacon of Faith
// 177168 - Beacon of Light
// 200025 - Beacon of Virtue
class spell_pal_legion_beacon_of_light : public SpellScriptLoader
{
    public:
        spell_pal_legion_beacon_of_light() : SpellScriptLoader("spell_pal_legion_beacon_of_light") { }

        class spell_pal_legion_beacon_of_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_beacon_of_light_AuraScript);

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return (GetId() == SPELL_PALADIN_BEACON_OF_LIGHT || GetId() == SPELL_PALADIN_BEACON_OF_VIRTUE);
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_PALADIN_THE_LIGHT_SAVES_BLOCKER))
                        return;

                    if (AuraEffect* theLightSaves = caster->GetAuraEffect(SPELL_PALADIN_THE_LIGHT_SAVES_TRAIT, EFFECT_0))
                    {
                        if (GetTarget()->HealthBelowPct(theLightSaves->GetAmount()))
                        {
                            caster->CastSpell(caster, SPELL_PALADIN_THE_LIGHT_SAVES_BONUS, true);
                            caster->CastSpell(caster, SPELL_PALADIN_THE_LIGHT_SAVES_BLOCKER, true);
                        }
                    }
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_beacon_of_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_beacon_of_light_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_beacon_of_light_AuraScript();
        }
};

// 231642 - Beacon of Light
class spell_pal_legion_beacon_of_light_power_refund : public SpellScriptLoader
{
    public:
        spell_pal_legion_beacon_of_light_power_refund() : SpellScriptLoader("spell_pal_legion_beacon_of_light_power_refund") { }

        class spell_pal_legion_beacon_of_light_power_refund_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_beacon_of_light_power_refund_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_BEACON_OF_LIGHT,
                    SPELL_PALADIN_BEACON_OF_FAITH,
                    SPELL_PALADIN_BEACON_OF_VIRTUE,
                    SPELL_PALADIN_BEACON_OF_LIGHT_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget() || !eventInfo.GetProcSpell())
                    return false;
                if (!eventInfo.GetActionTarget()->HasAura(SPELL_PALADIN_BEACON_OF_LIGHT, GetTarget()->GetGUID())
                    && !eventInfo.GetActionTarget()->HasAura(SPELL_PALADIN_BEACON_OF_FAITH, GetTarget()->GetGUID())
                    && !eventInfo.GetActionTarget()->HasAura(SPELL_PALADIN_BEACON_OF_VIRTUE, GetTarget()->GetGUID()))
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                SpellInfo const* energizeInfo = sSpellMgr->AssertSpellInfo(SPELL_PALADIN_BEACON_OF_LIGHT_ENERGIZE);
                if (SpellEffectInfo const* effInfo = energizeInfo->GetEffect(EFFECT_0))
                {
                    int32 manaCost = eventInfo.GetProcSpell()->GetPowerCost(POWER_MANA);
                    ApplyPct(manaCost, effInfo->CalcValue(GetTarget()));
                    GetTarget()->CastCustomSpell(SPELL_PALADIN_BEACON_OF_LIGHT_ENERGIZE, SPELLVALUE_BASE_POINT0, manaCost, GetTarget(), true);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_beacon_of_light_power_refund_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_beacon_of_light_power_refund_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_beacon_of_light_power_refund_AuraScript();
        }
};

// 200025 - Beacon of Virtue
class spell_pal_legion_beacon_of_virtue : public SpellScriptLoader
{
    public:
        spell_pal_legion_beacon_of_virtue() : SpellScriptLoader("spell_pal_legion_beacon_of_virtue") { }

        class spell_pal_legion_beacon_of_virtue_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_beacon_of_virtue_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                uint32 const maxTargets = GetEffectInfo(EFFECT_1)->CalcValue();
                // Expltarget aura is applied with effect 0
                targets.remove(GetExplTargetUnit());

                if (targets.size() > maxTargets)
                {
                    targets.sort(Trinity::HealthPctOrderPred());
                    targets.resize(maxTargets);
                }
                sharedTargets = targets;
            }

            void ShareTargets(std::list<WorldObject*>& targets)
            {
                targets = sharedTargets;
            }

            void Register() override
            {
                // @todo: replace shared targets with effect_all?
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_beacon_of_virtue_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_beacon_of_virtue_SpellScript::ShareTargets, EFFECT_2, TARGET_UNIT_DEST_AREA_ALLY);
            }

        private:
            std::list<WorldObject*> sharedTargets;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_beacon_of_virtue_SpellScript();
        }
};

// 184575 - Blade of Justice
class spell_pal_legion_blade_of_justice : public SpellScriptLoader
{
    public:
        spell_pal_legion_blade_of_justice() : SpellScriptLoader("spell_pal_legion_blade_of_justice") { }

        class spell_pal_legion_blade_of_justice_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_blade_of_justice_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HAND_OF_HINDRANCE
                });
            }

            void HandleHonorTrait(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (caster->HasAura(SPELL_PALADIN_LAW_AND_ORDER_TRAIT))
                    caster->CastCustomSpell(SPELL_PALADIN_HAND_OF_HINDRANCE, SPELLVALUE_AURA_DURATION, 3000, GetHitUnit(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_blade_of_justice_SpellScript::HandleHonorTrait, EFFECT_0, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_blade_of_justice_SpellScript();
        }
};

// 231843 - Blade of Wrath!
class spell_pal_legion_blade_of_wrath : public SpellScriptLoader
{
    public:
        spell_pal_legion_blade_of_wrath() : SpellScriptLoader("spell_pal_legion_blade_of_wrath") { }

        class spell_pal_legion_blade_of_wrath_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_blade_of_wrath_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_BLADE_OF_JUSTICE
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                caster->GetSpellHistory()->ResetCooldown(SPELL_PALADIN_BLADE_OF_JUSTICE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_blade_of_wrath_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_blade_of_wrath_SpellScript();
        }
};

// 231832 - Blade of Wrath
class spell_pal_legion_blade_of_wrath_proc : public SpellScriptLoader
{
    public:
        spell_pal_legion_blade_of_wrath_proc() : SpellScriptLoader("spell_pal_legion_blade_of_wrath_proc") { }

        class spell_pal_legion_blade_of_wrath_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_blade_of_wrath_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_BLADE_OF_JUSTICE
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return GetTarget()->GetSpellHistory()->HasCooldown(SPELL_PALADIN_BLADE_OF_JUSTICE);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_blade_of_wrath_proc_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_blade_of_wrath_proc_AuraScript();
        }
};

// 229976 - Blessed Hammer
class spell_pal_legion_blessed_hammer_absorb : public SpellScriptLoader
{
    public:
        spell_pal_legion_blessed_hammer_absorb() : SpellScriptLoader("spell_pal_legion_blessed_hammer_absorb") { }

        class spell_pal_legion_blessed_hammer_absorb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_blessed_hammer_absorb_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_BLESSED_HAMMER_DAMAGE
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (dmgInfo.GetAttackType() != BASE_ATTACK || !dmgInfo.GetAttacker()->HasAura(SPELL_PALADIN_BLESSED_HAMMER_DAMAGE, GetCasterGUID()))
                {
                    absorbAmount = 0;
                    return;
                }

                if (AuraEffect const* hammerPct = dmgInfo.GetAttacker()->GetAuraEffect(SPELL_PALADIN_BLESSED_HAMMER_DAMAGE, EFFECT_1, GetCasterGUID()))
                {
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), hammerPct->GetAmount());
                    hammerPct->GetBase()->Remove();
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_blessed_hammer_absorb_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_blessed_hammer_absorb_AuraScript::HandleAbsorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_blessed_hammer_absorb_AuraScript();
        }
};

// 1044 - Blessing of Freedom
class spell_pal_legion_blessing_of_freedom : public SpellScriptLoader
{
    public:
        spell_pal_legion_blessing_of_freedom() : SpellScriptLoader("spell_pal_legion_blessing_of_freedom") { }

        class spell_pal_legion_blessing_of_freedom_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_blessing_of_freedom_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HOLY_RITUAL_HONOR_TALENT,
                    SPELL_PALADIN_HOLY_RITUAL_HEAL
                });
            }

            bool Load() override
            {
                return GetCaster() && GetCaster()->HasAura(SPELL_PALADIN_HOLY_RITUAL_HONOR_TALENT);
            }

            void HandleHeal(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_PALADIN_HOLY_RITUAL_HEAL, true);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_pal_legion_blessing_of_freedom_AuraScript::HandleHeal, EFFECT_2, SPELL_AURA_MOD_INCREASE_SPEED, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_pal_legion_blessing_of_freedom_AuraScript::HandleHeal, EFFECT_2, SPELL_AURA_MOD_INCREASE_SPEED, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_blessing_of_freedom_AuraScript();
        }
};

// 1022 - Blessing of Protection
class spell_pal_legion_blessing_of_protection : public SpellScriptLoader
{
    public:
        spell_pal_legion_blessing_of_protection() : SpellScriptLoader("spell_pal_legion_blessing_of_protection") { }

        class spell_pal_legion_blessing_of_protection_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_blessing_of_protection_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HOLY_RITUAL_HONOR_TALENT,
                    SPELL_PALADIN_HOLY_RITUAL_HEAL
                });
            }

            bool Load() override
            {
                return GetCaster() && GetCaster()->HasAura(SPELL_PALADIN_HOLY_RITUAL_HONOR_TALENT);
            }

            void HandleHeal(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_PALADIN_HOLY_RITUAL_HEAL, true);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_pal_legion_blessing_of_protection_AuraScript::HandleHeal, EFFECT_2, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_pal_legion_blessing_of_protection_AuraScript::HandleHeal, EFFECT_2, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_blessing_of_protection_AuraScript();
        }
};

// 6940 - Blessing of Sacrifice
class spell_pal_legion_blessing_of_sacrifice : public SpellScriptLoader
{
    public:
        spell_pal_legion_blessing_of_sacrifice() : SpellScriptLoader("spell_pal_legion_blessing_of_sacrifice") { }

        class spell_pal_legion_blessing_of_sacrifice_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_blessing_of_sacrifice_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HOLY_RITUAL_HONOR_TALENT,
                    SPELL_PALADIN_HOLY_RITUAL_HEAL
                });
            }

            void Split(AuraEffect* /*aurEff*/, DamageInfo& /*dmgInfo*/, uint32& /*splitAmount*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (SpellEffectInfo const* pctInfo = GetSpellInfo()->GetEffect(EFFECT_2))
                    {
                        if (caster->HealthBelowPct(pctInfo->CalcValue(caster)))
                        {
                            Remove();
                            caster->RemoveAurasDueToSpell(GetId());
                        }
                    }
                }
            }

            void HandleHeal(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_PALADIN_HOLY_RITUAL_HONOR_TALENT))
                        caster->CastSpell(GetTarget(), SPELL_PALADIN_HOLY_RITUAL_HEAL, true);
            }

            void Register() override
            {
                OnEffectSplit += AuraEffectSplitFn(spell_pal_legion_blessing_of_sacrifice_AuraScript::Split, EFFECT_0);
                AfterEffectApply += AuraEffectApplyFn(spell_pal_legion_blessing_of_sacrifice_AuraScript::HandleHeal, EFFECT_0, SPELL_AURA_SPLIT_DAMAGE_PCT, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_pal_legion_blessing_of_sacrifice_AuraScript::HandleHeal, EFFECT_0, SPELL_AURA_SPLIT_DAMAGE_PCT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_blessing_of_sacrifice_AuraScript();
        }
};

// 210256 - Blessing of Sanctuary
class spell_pal_legion_blessing_of_sanctuary : public SpellScriptLoader
{
    public:
        spell_pal_legion_blessing_of_sanctuary() : SpellScriptLoader("spell_pal_legion_blessing_of_sanctuary") { }

        class spell_pal_legion_blessing_of_sanctuary_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_blessing_of_sanctuary_SpellScript);

            void RemoveEffects(SpellEffIndex /*effIndex*/)
            {
                GetHitUnit()->RemoveAurasByType(SPELL_AURA_MOD_STUN);
                GetHitUnit()->RemoveAurasByType(SPELL_AURA_MOD_SILENCE);
                GetHitUnit()->RemoveAurasByType(SPELL_AURA_MOD_FEAR);
                GetHitUnit()->RemoveAurasByType(SPELL_AURA_MOD_FEAR_2);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_blessing_of_sanctuary_SpellScript::RemoveEffects, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_blessing_of_sanctuary_SpellScript();
        }
};

// 115750 - Blinding Light
class spell_pal_legion_blinding_light : public SpellScriptLoader
{
    public:
        spell_pal_legion_blinding_light() : SpellScriptLoader("spell_pal_legion_blinding_light") { }

        class spell_pal_legion_blinding_light_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_blinding_light_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_BLINDING_LIGHT_TRIGGERED
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                caster->CastSpell(caster, SPELL_PALADIN_BLINDING_LIGHT_TRIGGERED, true);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_pal_legion_blinding_light_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_blinding_light_SpellScript();
        }
};

// 105421 - Blinding Light
class spell_pal_legion_blinding_light_proc : public SpellScriptLoader
{
    public:
        spell_pal_legion_blinding_light_proc() : SpellScriptLoader("spell_pal_legion_blinding_light_proc") { }

        class spell_pal_legion_blinding_light_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_blinding_light_proc_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // Not proc from holy damage
                return eventInfo.GetDamageInfo() && !(eventInfo.GetDamageInfo()->GetSchoolMask() & SPELL_SCHOOL_MASK_HOLY);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_blinding_light_proc_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_blinding_light_proc_AuraScript();
        }
};

// 209389 - Bulwark of Order
class spell_pal_legion_bulwark_of_order : public SpellScriptLoader
{
    public:
        spell_pal_legion_bulwark_of_order() : SpellScriptLoader("spell_pal_legion_bulwark_of_order") { }

        class spell_pal_legion_bulwark_of_order_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_bulwark_of_order_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if (SpellInfo const* triggerSpell = sSpellMgr->GetSpellInfo(aurEff->GetSpellEffectInfo()->TriggerSpell))
                {
                    int32 absorb = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
                    if (absorb > 0)
                        GetTarget()->CastCustomSpell(triggerSpell->Id, SPELLVALUE_BASE_POINT0, absorb, GetTarget(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_bulwark_of_order_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_bulwark_of_order_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_bulwark_of_order_AuraScript();
        }
};

// 4987 - Cleanse
class spell_pal_legion_cleanse : public SpellScriptLoader
{
    public:
        spell_pal_legion_cleanse() : SpellScriptLoader("spell_pal_legion_cleanse") { }

        class spell_pal_legion_cleanse_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_cleanse_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_CLEANSE_THE_WEAK_HONOR_TALENT,
                    SPELL_PALADIN_CLEANSE_THE_WEAK_VISUAL,
                    SPELL_PALADIN_CLEANSE_THE_WEAK_DISPEL
                });
            }

            void OnSuccessfulDispel(SpellEffIndex effIndex)
            {
                if (GetCaster()->HasAura(SPELL_PALADIN_CLEANSE_THE_WEAK_HONOR_TALENT))
                {
                    if (GetHitUnit()->HasAura(SPELL_PALADIN_DEVOTION_AURA, GetCaster()->GetGUID())
                        || GetHitUnit()->HasAura(SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB, GetCaster()->GetGUID())
                        || GetHitUnit()->HasAura(SPELL_PALADIN_AURA_OF_MERCY, GetCaster()->GetGUID()))
                    {
                        castVisual = true;
                        // all 3 spells share the same charge category we need to reset the charge after every cast to make that working here...
                        GetCaster()->GetSpellHistory()->RestoreCharge(GetSpellInfo()->ChargeCategoryId);
                        // When you dispel an ally within your aura, all allies within your aura are dispelled of the same effect.
                        CustomSpellValues values;
                        values.AddSpellMod(SPELLVALUE_BASE_POINT0, effIndex == EFFECT_0 ? 100 : 0);
                        values.AddSpellMod(SPELLVALUE_BASE_POINT1, effIndex == EFFECT_1 ? 100 : 0);
                        values.AddSpellMod(SPELLVALUE_BASE_POINT2, effIndex == EFFECT_2 ? 100 : 0);
                        GetCaster()->CastCustomSpell(SPELL_PALADIN_CLEANSE_THE_WEAK_DISPEL, values, GetCaster(), TRIGGERED_FULL_MASK);
                    }
                }
            }

            void CastVisual()
            {
                // make sure the visual is only casted one time
                if (castVisual)
                {
                    GetCaster()->GetSpellHistory()->RestoreCharge(GetSpellInfo()->ChargeCategoryId);
                    GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_CLEANSE_THE_WEAK_VISUAL, true);
                }
            }

            void Register() override
            {
                OnEffectSuccessfulDispel += SpellEffectFn(spell_pal_legion_cleanse_SpellScript::OnSuccessfulDispel, EFFECT_ALL, SPELL_EFFECT_DISPEL);
                AfterHit += SpellHitFn(spell_pal_legion_cleanse_SpellScript::CastVisual);
            }
            private:
                bool castVisual = false;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_cleanse_SpellScript();
        }
};

// 199360 - Cleanse the Weak
// 228473 - Cleanse the Weak
class spell_pal_legion_cleanse_the_weak_selector : public SpellScriptLoader
{
    public:
        spell_pal_legion_cleanse_the_weak_selector() : SpellScriptLoader("spell_pal_legion_cleanse_the_weak_selector") { }

        class spell_pal_legion_cleanse_the_weak_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_cleanse_the_weak_selector_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                ObjectGuid const& casterGuid = GetCaster()->GetGUID();
                targets.remove_if([casterGuid](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit())
                        if (!unitTarget->HasAura(SPELL_PALADIN_DEVOTION_AURA, casterGuid) &&
                            !unitTarget->HasAura(SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB, casterGuid) &&
                            !unitTarget->HasAura(SPELL_PALADIN_AURA_OF_MERCY, casterGuid))
                            return true;
                    return false;
                });
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_cleanse_the_weak_selector_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_cleanse_the_weak_selector_SpellScript();
        }
};

// 26573 - Consecration
class spell_pal_legion_consecration : public SpellScriptLoader
{
    public:
        spell_pal_legion_consecration() : SpellScriptLoader("spell_pal_legion_consecration") { }

        class spell_pal_legion_consecration_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_consecration_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_CONSERCRATED_GROUND,
                    SPELL_PALADIN_CONSERCRATED_HEAL,
                    SPELL_PALADIN_CONSERCRATION_DAMAGE
                });
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (aurEff->GetTickNumber() == 1)
                    return;

                if (AreaTrigger* at = GetTarget()->GetAreaTrigger(GetId()))
                {
                    if (GetTarget()->HasAura(SPELL_PALADIN_CONSERCRATED_GROUND, GetTarget()->GetGUID()))
                        GetTarget()->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_PALADIN_CONSERCRATED_HEAL, true);
                    GetTarget()->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_PALADIN_CONSERCRATION_DAMAGE, true);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pal_legion_consecration_AuraScript::HandleDummyTick, EFFECT_FIRST_FOUND, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_consecration_AuraScript();
        }
};

// 204241 - Consecration
class spell_pal_legion_consecration_heal : public SpellScriptLoader
{
    public:
        spell_pal_legion_consecration_heal() : SpellScriptLoader("spell_pal_legion_consecration_heal") { }

        class spell_pal_legion_consecration_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_consecration_heal_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (targets.size() > 6)
                {
                    targets.sort(Trinity::HealthPctOrderPred());
                    targets.resize(6);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_consecration_heal_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_consecration_heal_SpellScript();
        }
};

// Many spells :>
class spell_pal_legion_crusade_helper : public SpellScriptLoader
{
    public:
        spell_pal_legion_crusade_helper() : SpellScriptLoader("spell_pal_legion_crusade_helper") { }

        class spell_pal_legion_crusade_helper_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_crusade_helper_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_CRUSADE
                });
            }

            void HandleCrusadeAmount()
            {
                // This handling is a hack - original it is handled by crusade itself (with spell proc) but currently we cant get the power costs there
                if (Aura* crusade = GetCaster()->GetAura(SPELL_PALADIN_CRUSADE))
                {
                    if (crusade->GetStackAmount() == 15)
                        return;

                    uint8 stacks = uint8(GetSpell()->GetPowerCost(POWER_HOLY_POWER));
                    crusade->SetStackAmount(std::min(15, crusade->GetStackAmount() + stacks));
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pal_legion_crusade_helper_SpellScript::HandleCrusadeAmount);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_crusade_helper_SpellScript();
        }
};

// 196926 - Crusader's Might
class spell_pal_legion_crusaders_might : public SpellScriptLoader
{
    public:
        spell_pal_legion_crusaders_might() : SpellScriptLoader("spell_pal_legion_crusaders_might") { }

        class spell_pal_legion_crusaders_might_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_crusaders_might_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_CRUSADER_STRIKE,
                    SPELL_PALADIN_LIGHT_OF_DAWN,
                    SPELL_PALADIN_HOLY_SHOCK
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_CRUSADER_STRIKE;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_PALADIN_LIGHT_OF_DAWN, aurEff->GetAmount());
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_PALADIN_HOLY_SHOCK, aurEff->GetAmount());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_crusaders_might_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_crusaders_might_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_crusaders_might_AuraScript();
        }
};

// 210378 - Darkest before the Dawn
class spell_pal_legion_darkest_before_the_dawn : public SpellScriptLoader
{
    public:
        spell_pal_legion_darkest_before_the_dawn() : SpellScriptLoader("spell_pal_legion_darkest_before_the_dawn") { }

        class spell_pal_legion_darkest_before_the_dawn_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_darkest_before_the_dawn_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LIGHT_OF_DAWN,
                    SPELL_PALADIN_DARKEST_BEFORE_THE_DAWN_BONUS
                });
            }

            void OnPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (!GetTarget()->GetSpellHistory()->HasCooldown(SPELL_PALADIN_LIGHT_OF_DAWN))
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_DARKEST_BEFORE_THE_DAWN_BONUS, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pal_legion_darkest_before_the_dawn_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_darkest_before_the_dawn_AuraScript();
        }
};

// 198034 - Divine Hammer
class spell_pal_legion_divine_hammer : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_hammer() : SpellScriptLoader("spell_pal_legion_divine_hammer") { }

        class spell_pal_legion_divine_hammer_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_divine_hammer_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HOLY_POWER_ENERGIZE
                });
            }

            void HandleEnergize(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_HOLY_POWER_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_divine_hammer_SpellScript::HandleEnergize, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_divine_hammer_SpellScript();
        }
};

// 198137 - Divine Hammer
class spell_pal_legion_divine_hammer_aoe : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_hammer_aoe() : SpellScriptLoader("spell_pal_legion_divine_hammer_aoe") { }

        class spell_pal_legion_divine_hammer_aoe_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_divine_hammer_aoe_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_DIVINE_HAMMER,
                    SPELL_PALADIN_LAW_AND_ORDER_TRAIT,
                    SPELL_PALADIN_HAND_OF_HINDRANCE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIdx*/)
            {
                if (AuraEffect* divineHammer = GetCaster()->GetAuraEffect(SPELL_PALADIN_DIVINE_HAMMER, EFFECT_0))
                {
                    if (divineHammer->GetTickNumber() == 1)
                    {
                        if (GetCaster()->HasAura(SPELL_PALADIN_LAW_AND_ORDER_TRAIT))
                            GetCaster()->CastCustomSpell(SPELL_PALADIN_HAND_OF_HINDRANCE, SPELLVALUE_AURA_DURATION, 3000, GetHitUnit(), TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_divine_hammer_aoe_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_divine_hammer_aoe_SpellScript();
        }
};

// 213313 - Divine Intervention
class spell_pal_legion_divine_intervention : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_intervention() : SpellScriptLoader("spell_pal_legion_divine_intervention") { }

        class spell_pal_legion_divine_intervention_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_divine_intervention_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_1)->IsAura(SPELL_AURA_DUMMY))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_DIVINE_SHIELD,
                    SPELL_PALADIN_FORBEARANCE
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (GetTarget()->GetSpellHistory()->HasCooldown(SPELL_PALADIN_DIVINE_SHIELD) || GetTarget()->HasAura(SPELL_PALADIN_FORBEARANCE))
                    return;

                if (dmgInfo.GetDamage() >= GetTarget()->GetHealth())
                {
                    absorbAmount = dmgInfo.GetDamage();
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_DIVINE_SHIELD, false);
                    GetTarget()->SetHealth(GetTarget()->CountPctFromMaxHealth(GetEffect(EFFECT_1)->GetAmount()));
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_divine_intervention_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_divine_intervention_AuraScript::HandleAbsorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_divine_intervention_AuraScript();
        }
};

// 204914 - Divine Punisher
class spell_pal_legion_divine_punisher : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_punisher() : SpellScriptLoader("spell_pal_legion_divine_punisher") { }

        class spell_pal_legion_divine_punisher_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_divine_punisher_AuraScript);

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                if (lastActionTargetGuid == eventInfo.GetActionTarget()->GetGUID())
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_DIVINE_PUNISHER_ENERGIZE, true);
                else
                    lastActionTargetGuid = eventInfo.GetActionTarget()->GetGUID();
            }

            private:
                ObjectGuid lastActionTargetGuid;

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_divine_punisher_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_divine_punisher_AuraScript();
        }
};

// 197646 - Divine Purpose
class spell_pal_legion_divine_purpose_holy : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_purpose_holy() : SpellScriptLoader("spell_pal_legion_divine_purpose_holy") { }

        class spell_pal_legion_divine_purpose_holy_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_divine_purpose_holy_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LIGHT_OF_DAWN,
                    SPELL_PALADIN_DIVINE_PURPOSE_LIGHT_OF_DAWN,
                    SPELL_PALADIN_DIVINE_PURPOSE_HOLY_SHOCK
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;
                return eventInfo.GetSpellInfo()->Id != SPELL_PALADIN_LIGHT_OF_DAWN_HEAL;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                uint32 triggeredSpell = eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_LIGHT_OF_DAWN ? SPELL_PALADIN_DIVINE_PURPOSE_LIGHT_OF_DAWN : SPELL_PALADIN_DIVINE_PURPOSE_HOLY_SHOCK;
                GetTarget()->CastSpell(GetTarget(), triggeredSpell, true);
                GetTarget()->GetSpellHistory()->ResetCooldown(eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_LIGHT_OF_DAWN ? SPELL_PALADIN_LIGHT_OF_DAWN : SPELL_PALADIN_HOLY_SHOCK, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_divine_purpose_holy_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_divine_purpose_holy_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_divine_purpose_holy_AuraScript();
        }
};

// 223817 - Divine Purpose
class spell_pal_legion_divine_purpose_retri : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_purpose_retri() : SpellScriptLoader("spell_pal_legion_divine_purpose_retri") { }

        class spell_pal_legion_divine_purpose_retri_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_divine_purpose_retri_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // Only let it proc if the cast wasnt free
                return eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetPowerCost(POWER_HOLY_POWER) != 0;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_divine_purpose_retri_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_divine_purpose_retri_AuraScript();
        }
};

// 642 - Divine Shield
class spell_pal_legion_divine_shield : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_shield() : SpellScriptLoader("spell_pal_legion_divine_shield") { }

        class spell_pal_legion_divine_shield_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_divine_shield_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_FINAL_STAND,
                    SPELL_PALADIN_FINAL_STAND_TAUNT
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_PALADIN_FINAL_STAND);
            }

            void CastTaunt()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_FINAL_STAND_TAUNT, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pal_legion_divine_shield_SpellScript::CastTaunt);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_divine_shield_SpellScript();
        }
};

// 190784 - Divine Steed
class spell_pal_legion_divine_steed : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_steed() : SpellScriptLoader("spell_pal_legion_divine_steed") { }

        class spell_pal_legion_divine_steed_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_divine_steed_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_DIVINE_STEED_HUMAN,
                    SPELL_PALADIN_DIVINE_STEED_TAUREN,
                    SPELL_PALADIN_DIVINE_STEED_BLOODELF,
                    SPELL_PALADIN_DIVINE_STEED_DRAENEI,
                    SPELL_PALADIN_STEED_OF_GLORY_HONOR_TALENT,
                    SPELL_PALADIN_STEED_OF_GLORY_IMMUNITY
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                switch (GetCaster()->getRace())
                {
                    case RACE_HUMAN:
                        GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_DIVINE_STEED_HUMAN, true);
                        break;
                    case RACE_TAUREN:
                        GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_DIVINE_STEED_TAUREN, true);
                        break;
                    case RACE_BLOODELF:
                        GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_DIVINE_STEED_BLOODELF, true);
                        break;
                    case RACE_DRAENEI:
                        GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_DIVINE_STEED_DRAENEI, true);
                        break;
                    default:
                        break;
                }
                if (GetCaster()->HasAura(SPELL_PALADIN_STEED_OF_GLORY_HONOR_TALENT))
                {
                    if (SpellInfo const* steedOfGlory = sSpellMgr->AssertSpellInfo(SPELL_PALADIN_STEED_OF_GLORY_IMMUNITY))
                    {
                        if (SpellEffectInfo const* eff0Info = steedOfGlory->GetEffect(EFFECT_0))
                            GetCaster()->RemoveAurasWithMechanic(eff0Info->MiscValue);

                        if (SpellEffectInfo const* eff2Info = steedOfGlory->GetEffect(EFFECT_2))
                            GetCaster()->RemoveAurasWithMechanic(1 << eff2Info->MiscValue);
                    }
                    GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_STEED_OF_GLORY_IMMUNITY, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_divine_steed_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_divine_steed_SpellScript();
        }
};

// 221886, 221887, 221883, 221885 - Divine Steed
class spell_pal_legion_divine_steed_mount : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_steed_mount() : SpellScriptLoader("spell_pal_legion_divine_steed_mount") { }

        class spell_pal_legion_divine_steed_mount_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_divine_steed_mount_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_STEED_OF_GLORY_IMMUNITY
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_PALADIN_STEED_OF_GLORY_IMMUNITY);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_pal_legion_divine_steed_mount_AuraScript::HandleRemove, EFFECT_FIRST_FOUND, SPELL_AURA_MOUNTED, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_divine_steed_mount_AuraScript();
        }
};

// 53385 - Divine Storm
class spell_pal_legion_divine_storm : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_storm() : SpellScriptLoader("spell_pal_legion_divine_storm") { }

        class spell_pal_legion_divine_storm_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_divine_storm_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_DIVINE_STORM_DAMAGE,
                    SPELL_PALADIN_HEALING_STORM_TRAIT,
                    SPELL_PALADIN_HEALING_STORM_HEAL
                });
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                // If divine tempest trait is skilled the damage is triggered by areatrigger spawn (areatrigger and expltarget unit position are the same - areatrigger and divine storm using the same damage radius)
                if (!GetCaster()->HasAura(SPELL_PALADIN_DIVINE_TEMPEST_TRAIT))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_DIVINE_STORM_DAMAGE, true);
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (AuraEffect* healingStorm = GetCaster()->GetAuraEffect(SPELL_PALADIN_HEALING_STORM_TRAIT, EFFECT_0))
                {
                    targets.sort(Trinity::HealthPctOrderPred());
                    if (int32(targets.size()) > healingStorm->GetAmount())
                        targets.resize(healingStorm->GetAmount());
                }
                else
                    targets.clear();
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_PALADIN_HEALING_STORM_TRAIT))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_HEALING_STORM_HEAL, true);
            }

            void HandleAfterCast()
            {
                GetCaster()->SendPlaySpellVisualKit(VISUAL_KIT_DIVINE_STORM, 0, 0);

                if (GetCaster()->HasAura(SPELL_PALADIN_DIVINE_TEMPEST_TRAIT))
                    GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_DIVINE_TEMPEST_SUMMON, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_divine_storm_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_divine_storm_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_SRC_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_divine_storm_SpellScript::HandleHeal, EFFECT_1, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_pal_legion_divine_storm_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_divine_storm_SpellScript();
        }
};

// 205191 - Eye for an Eye
class spell_pal_legion_eye_for_an_eye : public SpellScriptLoader
{
    public:
        spell_pal_legion_eye_for_an_eye() : SpellScriptLoader("spell_pal_legion_eye_for_an_eye") { }

        class spell_pal_legion_eye_for_an_eye_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_eye_for_an_eye_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_EYE_FOR_AN_EYE_TRIGGERED_SPELL
                });
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActor(), SPELL_PALADIN_EYE_FOR_AN_EYE_TRIGGERED_SPELL, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_eye_for_an_eye_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_eye_for_an_eye_AuraScript();
        }
};

// 211903 - Faith's Armor
class spell_pal_legion_faith_s_armor : public SpellScriptLoader
{
    public:
        spell_pal_legion_faith_s_armor() : SpellScriptLoader("spell_pal_legion_faith_s_armor") { }

        class spell_pal_legion_faith_s_armor_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_faith_s_armor_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_FAITH_S_ARMOR_TRAIT_AURA
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool & /*canBeRecalculated*/)
            {
                if (AuraEffect* traitAmount = GetUnitOwner()->GetAuraEffect(SPELL_PALADIN_FAITH_S_ARMOR_TRAIT_AURA, EFFECT_1))
                    amount = traitAmount->GetAmount();
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_faith_s_armor_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_MOD_RESISTANCE_PCT);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_faith_s_armor_AuraScript();
        }
};

// 196923 - Fervent Martyr
class spell_pal_legion_fervent_martyr : public SpellScriptLoader
{
    public:
        spell_pal_legion_fervent_martyr() : SpellScriptLoader("spell_pal_legion_fervent_martyr") { }

        class spell_pal_legion_fervent_martyr_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_fervent_martyr_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_BEACON_OF_LIGHT,
                    SPELL_PALADIN_BEACON_OF_FAITH,
                    SPELL_PALADIN_BEACON_OF_VIRTUE,
                    SPELL_PALADIN_FERVENT_MARTYR
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                Unit* caster = GetCaster();
                if (!caster)
                    return false;

                if (Unit* target = eventInfo.GetActionTarget())
                    if (!target->HasAura(SPELL_PALADIN_BEACON_OF_LIGHT, caster->GetGUID()) && !target->HasAura(SPELL_PALADIN_BEACON_OF_FAITH, caster->GetGUID())
                        && !target->HasAura(SPELL_PALADIN_BEACON_OF_VIRTUE, caster->GetGUID()))
                        return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_FERVENT_MARTYR, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_fervent_martyr_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_fervent_martyr_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_fervent_martyr_AuraScript();
        }
};

// 198054 - Fist of Justice
class spell_pal_legion_fist_of_justice : public SpellScriptLoader
{
    public:
        spell_pal_legion_fist_of_justice() : SpellScriptLoader("spell_pal_legion_fist_of_justice") { }

        class spell_pal_legion_fist_of_justice_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_fist_of_justice_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HAMMER_OF_JUSTICE
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (SpellEffectInfo const* effInfo = GetAura()->GetSpellEffectInfo(EFFECT_0))
                    caster->GetSpellHistory()->ModifyCooldown(SPELL_PALADIN_HAMMER_OF_JUSTICE, -(effInfo->CalcValue(GetTarget()) * 1000));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_fist_of_justice_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_fist_of_justice_AuraScript();
        }
};

// 234299 - Fist of Justice
class spell_pal_legion_fist_of_justice_retri : public SpellScriptLoader
{
    public:
        spell_pal_legion_fist_of_justice_retri() : SpellScriptLoader("spell_pal_legion_fist_of_justice_retri") { }

        class spell_pal_legion_fist_of_justice_retri_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_fist_of_justice_retri_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HAMMER_OF_JUSTICE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetPowerCost(POWER_HOLY_POWER) != 0;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 cdRed = -(aurEff->GetAmount() * eventInfo.GetProcSpell()->GetPowerCost(POWER_HOLY_POWER) * 100);
                GetCaster()->GetSpellHistory()->ModifyCooldown(SPELL_PALADIN_HAMMER_OF_JUSTICE, cdRed);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_fist_of_justice_retri_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_fist_of_justice_retri_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_fist_of_justice_retri_AuraScript();
        }
};

// 19750 - Flash of Light
class spell_pal_legion_flash_of_light : public SpellScriptLoader
{
    public:
        spell_pal_legion_flash_of_light() : SpellScriptLoader("spell_pal_legion_flash_of_light") { }

        class spell_pal_legion_flash_of_light_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_flash_of_light_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_THE_LIGHT_SAVES_BONUS,
                    SPELL_PALADIN_BEACON_OF_LIGHT,
                    SPELL_PALADIN_BEACON_OF_VIRTUE
                });
            }

            void ResetCharges(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->HasAura(SPELL_PALADIN_BEACON_OF_LIGHT) || GetHitUnit()->HasAura(SPELL_PALADIN_BEACON_OF_VIRTUE))
                {
                    if (AuraEffect* theLightSaves = GetCaster()->GetAuraEffect(SPELL_PALADIN_THE_LIGHT_SAVES_BONUS, EFFECT_0))
                    {
                        SetHitHeal(GetHitHeal() + CalculatePct(GetHitHeal(), theLightSaves->GetAmount()));
                        theLightSaves->GetBase()->Remove();
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_flash_of_light_SpellScript::ResetCharges, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_flash_of_light_SpellScript();
        }
};

// 85043 - Grand Crusader
class spell_pal_legion_grand_crusader : public SpellScriptLoader
{
    public:
        spell_pal_legion_grand_crusader() : SpellScriptLoader("spell_pal_legion_grand_crusader") { }

        class spell_pal_legion_grand_crusader_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_grand_crusader_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HAMMER_OF_THE_RIGHTEOUS,
                    SPELL_PALADIN_GRAND_CRUSADER_BONUS,
                    SPELL_PALADIN_AVENGERS_SHIELD,
                    SPELL_PALADIN_CRUSADERS_JUDGMENT,
                    SPELL_PALADIN_JUDGMENT
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (((eventInfo.GetHitMask() & (PROC_HIT_DODGE | PROC_HIT_PARRY)) && eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetAttackType() == BASE_ATTACK)
                    || (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_HAMMER_OF_THE_RIGHTEOUS))
                    return true;
                return false;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_GRAND_CRUSADER_BONUS, true);
                GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_PALADIN_AVENGERS_SHIELD, true);

                if (GetTarget()->HasAura(SPELL_PALADIN_CRUSADERS_JUDGMENT, GetTarget()->GetGUID()))
                    if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PALADIN_JUDGMENT))
                        GetTarget()->GetSpellHistory()->RestoreCharge(spellInfo->ChargeCategoryId);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_grand_crusader_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_grand_crusader_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_grand_crusader_AuraScript();
        }
};

// 203538 - Greater Blessing of Kings
class spell_pal_legion_greater_blessing_of_kings : public SpellScriptLoader
{
    public:
        spell_pal_legion_greater_blessing_of_kings() : SpellScriptLoader("spell_pal_legion_greater_blessing_of_kings") { }

        class spell_pal_legion_greater_blessing_of_kings_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_greater_blessing_of_kings_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HAMMER_OF_RECKONING,
                    SPELL_PALADIN_HAMMER_OF_RECKONING_BONUS
                });
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = true;
                if (Unit* caster = GetCaster())
                {
                    amount = caster->GetTotalAttackPowerValue(BASE_ATTACK) * 2.7f;
                    const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
                }
            }

            void OnPeriodic(AuraEffect const* aurEff)
            {
                const_cast<AuraEffect*>(aurEff)->SetAmount(aurEff->GetDamage());
                if (AuraEffect const* aurEff1 = GetEffect(EFFECT_1))
                    const_cast<AuraEffect*>(aurEff1)->SetDamage(aurEff->GetAmount());
                GetAura()->SetNeedClientUpdateForTargets();
            }

            void CalculateAmountEff1(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (AuraEffect const* aurEff0 = GetEffect(EFFECT_0))
                {
                    absorbAmount = std::min(uint32(aurEff0->GetAmount()), dmgInfo.GetDamage());
                    const_cast<AuraEffect*>(aurEff0)->SetAmount(std::max(aurEff0->GetAmount() - int32(absorbAmount), 0));
                    aurEff->SetDamage(aurEff0->GetAmount());
                }
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo() || eventInfo.GetDamageInfo()->GetDamage() == 0)
                    return false;

                return GetCaster() && GetCaster()->HasSpell(SPELL_PALADIN_HAMMER_OF_RECKONING);
            }

            void OnSpellProc(ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_PALADIN_HAMMER_OF_RECKONING_BONUS, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_greater_blessing_of_kings_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pal_legion_greater_blessing_of_kings_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_greater_blessing_of_kings_AuraScript::CalculateAmountEff1, EFFECT_1, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_greater_blessing_of_kings_AuraScript::Absorb, EFFECT_1);
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_greater_blessing_of_kings_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_pal_legion_greater_blessing_of_kings_AuraScript::OnSpellProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_greater_blessing_of_kings_AuraScript();
        }
};

// 203539 - Greater Blessing of Wisdom
class spell_pal_legion_greater_blessing_of_wisdom : public SpellScriptLoader
{
    public:
        spell_pal_legion_greater_blessing_of_wisdom() : SpellScriptLoader("spell_pal_legion_greater_blessing_of_wisdom") { }

        class spell_pal_legion_greater_blessing_of_wisdom_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_greater_blessing_of_wisdom_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_2))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HAMMER_OF_RECKONING,
                    SPELL_PALADIN_HAMMER_OF_RECKONING_BONUS
                });
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& /*isPeriodic*/, int32& amplitude)
            {
                amplitude = GetSpellInfo()->GetEffect(EFFECT_2)->CalcValue(GetUnitOwner()) * 1000;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo() || eventInfo.GetDamageInfo()->GetDamage() == 0)
                    return false;

                return GetCaster() && GetCaster()->HasSpell(SPELL_PALADIN_HAMMER_OF_RECKONING);
            }

            void OnSpellProc(ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_PALADIN_HAMMER_OF_RECKONING_BONUS, true);
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_pal_legion_greater_blessing_of_wisdom_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_OBS_MOD_POWER);
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_pal_legion_greater_blessing_of_wisdom_AuraScript::CalcPeriodic, EFFECT_1, SPELL_AURA_OBS_MOD_HEALTH);
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_greater_blessing_of_wisdom_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_pal_legion_greater_blessing_of_wisdom_AuraScript::OnSpellProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_greater_blessing_of_wisdom_AuraScript();
        }
};

// 204939 - Hammer of Reckoning
class spell_pal_legion_hammer_of_reckoning : public SpellScriptLoader
{
    public:
        spell_pal_legion_hammer_of_reckoning() : SpellScriptLoader("spell_pal_legion_hammer_of_reckoning") { }

        class spell_pal_legion_hammer_of_reckoning_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_hammer_of_reckoning_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HAMMER_OF_RECKONING_BONUS
                });
            }

            void RemoveBonus()
            {
                GetCaster()->RemoveAurasDueToSpell(SPELL_PALADIN_HAMMER_OF_RECKONING_BONUS);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_pal_legion_hammer_of_reckoning_SpellScript::RemoveBonus);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_hammer_of_reckoning_SpellScript();
        }
};

// 53595 - Hammer of the Righteous
class spell_pal_legion_hammer_of_the_righteous : public SpellScriptLoader
{
    public:
        spell_pal_legion_hammer_of_the_righteous() : SpellScriptLoader("spell_pal_legion_hammer_of_the_righteous") { }

        class spell_pal_legion_hammer_of_the_righteous_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_hammer_of_the_righteous_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_CONSECRATION_PROT_BONUS,
                    SPELL_PALADIN_CONSERCRATED_HAMMER,
                    SPELL_PALADIN_HAMMER_OF_THE_RIGHTEOUS_AOE
                });
            }

            void HandleAoeHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_PALADIN_CONSECRATION_PROT_BONUS, GetCaster()->GetGUID()) || GetCaster()->HasAura(SPELL_PALADIN_CONSERCRATED_HAMMER, GetCaster()->GetGUID()))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_HAMMER_OF_THE_RIGHTEOUS_AOE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_hammer_of_the_righteous_SpellScript::HandleAoeHit, EFFECT_1, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_hammer_of_the_righteous_SpellScript();
        }
};

// 183218 - Hand of Hindrance
class spell_pal_legion_hand_of_hindrance : public SpellScriptLoader
{
    public:
        spell_pal_legion_hand_of_hindrance() : SpellScriptLoader("spell_pal_legion_hand_of_hindrance") { }

        class spell_pal_legion_hand_of_hindrance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_hand_of_hindrance_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LAW_AND_ORDER_TRAIT
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_ENEMY_SPELL)
                    return;

                if (Unit* caster = GetCaster())
                    if (AuraEffect* lawAndOrder = caster->GetAuraEffect(SPELL_PALADIN_LAW_AND_ORDER_TRAIT, EFFECT_0))
                        caster->GetSpellHistory()->ModifyCooldown(GetSpellInfo()->Id, -(lawAndOrder->GetAmount() * 1000));
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_pal_legion_hand_of_hindrance_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_MOD_DECREASE_SPEED, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_hand_of_hindrance_AuraScript();
        }
};

// 82326 - Holy Light
class spell_pal_legion_holy_light : public SpellScriptLoader
{
    public:
        spell_pal_legion_holy_light() : SpellScriptLoader("spell_pal_legion_holy_light") { }

        class spell_pal_legion_holy_light_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_holy_light_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_THE_LIGHT_SAVES_BONUS,
                    SPELL_PALADIN_BEACON_OF_LIGHT,
                    SPELL_PALADIN_BEACON_OF_VIRTUE
                });
            }

            void ResetCharges(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->HasAura(SPELL_PALADIN_BEACON_OF_LIGHT) || GetHitUnit()->HasAura(SPELL_PALADIN_BEACON_OF_VIRTUE))
                {
                    if (AuraEffect* theLightSaves = GetCaster()->GetAuraEffect(SPELL_PALADIN_THE_LIGHT_SAVES_BONUS, EFFECT_0))
                    {
                        SetHitHeal(GetHitHeal() + CalculatePct(GetHitHeal(), theLightSaves->GetAmount()));
                        theLightSaves->GetBase()->Remove();
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_holy_light_SpellScript::ResetCharges, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_holy_light_SpellScript();
        }
};

// 114165 - Holy Prism
class spell_pal_legion_holy_prism : public SpellScriptLoader
{
    public:
        spell_pal_legion_holy_prism() : SpellScriptLoader("spell_pal_legion_holy_prism") { }

        class spell_pal_legion_holy_prism_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_holy_prism_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HOLY_PRISM_TARGET_ALLY,
                    SPELL_PALADIN_HOLY_PRISM_TARGET_ENEMY,
                    SPELL_PALADIN_HOLY_PRISM_TARGET_VISUAL
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->IsFriendlyTo(GetHitUnit()))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_HOLY_PRISM_TARGET_ALLY, true);
                else
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_HOLY_PRISM_TARGET_ENEMY , true);
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_HOLY_PRISM_TARGET_VISUAL, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_holy_prism_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_holy_prism_SpellScript();
        }
};

// 114852 - Holy Prism
// 114871 - Holy Prism
class spell_pal_legion_holy_prism_selector : public SpellScriptLoader
{
    public:
        spell_pal_legion_holy_prism_selector() : SpellScriptLoader("spell_pal_legion_holy_prism_selector") { }

        class spell_pal_legion_holy_prism_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_holy_prism_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HOLY_PRISM_TARGET_ALLY,
                    SPELL_PALADIN_HOLY_PRISM_AREA_VISUAL
                });
            }

            void SaveTargetGuid(SpellEffIndex /*effIndex*/)
            {
                _targetGUID = GetHitUnit()->GetGUID();
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (GetSpellInfo()->Id == SPELL_PALADIN_HOLY_PRISM_TARGET_ALLY)
                    targets.sort(Trinity::HealthPctOrderPred());

                if (targets.size() > 5)
                {
                    if (GetSpellInfo()->Id == SPELL_PALADIN_HOLY_PRISM_TARGET_ALLY)
                        targets.resize(5);
                    else
                        Trinity::Containers::RandomResize(targets, 5);
                }

                _sharedTargets = targets;
            }

            void ShareTargets(std::list<WorldObject*>& targets)
            {
                targets = _sharedTargets;
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                if (Unit* initialTarget = ObjectAccessor::GetUnit(*GetCaster(), _targetGUID))
                    initialTarget->CastSpell(GetHitUnit(), SPELL_PALADIN_HOLY_PRISM_AREA_VISUAL, true);
            }

            void Register() override
            {
                if (m_scriptSpellId == SPELL_PALADIN_HOLY_PRISM_TARGET_ENEMY)
                {
                    OnEffectHitTarget += SpellEffectFn(spell_pal_legion_holy_prism_selector_SpellScript::SaveTargetGuid, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                    OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_holy_prism_selector_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
                }

                if (m_scriptSpellId == SPELL_PALADIN_HOLY_PRISM_TARGET_ALLY)
                {
                    OnEffectHitTarget += SpellEffectFn(spell_pal_legion_holy_prism_selector_SpellScript::SaveTargetGuid, EFFECT_0, SPELL_EFFECT_HEAL);
                    OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_holy_prism_selector_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
                }

                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_holy_prism_selector_SpellScript::ShareTargets, EFFECT_2, TARGET_UNIT_DEST_AREA_ENTRY);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_holy_prism_selector_SpellScript::HandleScript, EFFECT_2, SPELL_EFFECT_SCRIPT_EFFECT);
            }

        private:
            std::list<WorldObject*> _sharedTargets;
            ObjectGuid _targetGUID;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_holy_prism_selector_SpellScript();
        }
};

// 152261 - Holy Shield
class spell_pal_legion_holy_shield : public SpellScriptLoader
{
    public:
        spell_pal_legion_holy_shield() : SpellScriptLoader("spell_pal_legion_holy_shield") { }

        class spell_pal_legion_holy_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_holy_shield_AuraScript);

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
            }

            void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (!dmgInfo.GetSpellInfo() || dmgInfo.GetDamageType() == DOT)
                    return;

                if (dmgInfo.GetSchoolMask() & SPELL_SCHOOL_MASK_MAGIC)
                {
                    if (dmgInfo.GetAttacker()->isSpellBlocked(GetTarget(), dmgInfo.GetSpellInfo(), dmgInfo.GetAttackType()))
                    {
                        absorbAmount = CalculatePct(dmgInfo.GetDamage(), GetTarget()->GetBlockPercent());
                        GetTarget()->CastSpell(dmgInfo.GetAttacker(), GetSpellInfo()->GetEffect(EFFECT_1)->TriggerSpell, true);
                    }
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_holy_shield_AuraScript::CalculateAmount, EFFECT_2, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_holy_shield_AuraScript::HandleEffectProc, EFFECT_1, SPELL_AURA_PROC_TRIGGER_SPELL);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_holy_shield_AuraScript::HandleAbsorb, EFFECT_2);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_holy_shield_AuraScript();
        }
};

// 20473 - Holy Shock
class spell_pal_legion_holy_shock : public SpellScriptLoader
{
    public:
        spell_pal_legion_holy_shock() : SpellScriptLoader("spell_pal_legion_holy_shock") { }

        class spell_pal_legion_holy_shock_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_holy_shock_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_HOLY_SHOCK_HEAL,
                    SPELL_PALADIN_HOLY_SHOCK_DAMAGE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->IsFriendlyTo(GetHitUnit()))
                {
                    if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_PALADIN_POWER_OF_THE_SILVER_HAND_BUFF, EFFECT_0))
                    {
                        int32 damage = sSpellMgr->GetSpellInfo(SPELL_PALADIN_HOLY_SHOCK_HEAL)->GetEffect(EFFECT_0)->CalcValue(GetCaster()) + aurEff->GetAmount();
                        GetCaster()->CastCustomSpell(SPELL_PALADIN_HOLY_SHOCK_HEAL, SPELLVALUE_BASE_POINT0, damage, GetHitUnit(), true);
                        aurEff->GetBase()->Remove();
                    }
                    else
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_HOLY_SHOCK_HEAL, true);
                }
                else
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_HOLY_SHOCK_DAMAGE, true);
            }

            SpellCastResult CheckCast()
            {
                if (Unit* target = GetExplTargetUnit())
                {
                    if (!GetCaster()->IsFriendlyTo(target))
                    {
                        if (!GetCaster()->IsValidAttackTarget(target))
                            return SPELL_FAILED_BAD_TARGETS;

                        if (!GetCaster()->isInFront(target))
                            return SPELL_FAILED_UNIT_NOT_INFRONT;
                    }
                }
                else
                    return SPELL_FAILED_BAD_TARGETS;

                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_pal_legion_holy_shock_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_holy_shock_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_holy_shock_SpellScript();
        }
};

// 210220 - Holy Wrath
class spell_pal_legion_holy_wrath : public SpellScriptLoader
{
    public:
        spell_pal_legion_holy_wrath() : SpellScriptLoader("spell_pal_legion_holy_wrath") { }

        class spell_pal_legion_holy_wrath_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_holy_wrath_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_2) || !spellInfo->GetEffect(EFFECT_3) || !spellInfo->GetEffect(EFFECT_4))
                    return false;
                return true;
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                SpellEffIndex index = GetHitUnit()->GetTypeId() == TYPEID_UNIT ? EFFECT_2 : EFFECT_3;
                SpellEffectInfo const* pctAmountInfo = GetEffectInfo(index);
                int32 damage = std::min(int32(GetCaster()->GetMaxHealth() - GetCaster()->GetHealth()), INT32_MAX);

                if (GetHitUnit()->GetTypeId() == TYPEID_UNIT)
                {
                    // Deals $s3% of your missing health in Holy damage to $s2 nearby enemies, up to ${$s3*$s5/100}% of your maximum health.
                    SpellEffectInfo const* mod = GetEffectInfo(EFFECT_4);
                    int32 cap = GetCaster()->CountPctFromMaxHealth(pctAmountInfo->CalcValue() * mod->CalcValue() / 100);
                    damage = std::min(CalculatePct(damage, pctAmountInfo->CalcValue()), cap);
                }
                else
                    damage = CalculatePct(damage, pctAmountInfo->CalcValue()); // Deals $s4% of missing health against enemy players.

                SetHitDamage(damage);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_holy_wrath_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_holy_wrath_SpellScript();
        }
};

// 20271 - Judgment
class spell_pal_legion_judgment : public SpellScriptLoader
{
    public:
        spell_pal_legion_judgment() : SpellScriptLoader("spell_pal_legion_judgment") { }

        class spell_pal_legion_judgment_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_judgment_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF_MARKER,
                    SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF,
                    SPELL_PALADIN_JUDGMENT_TARGET_BONUS,
                    SPELL_PALADIN_GREATER_JUDGMENT,
                    SPELL_PALADIN_JUDGMENT_EXTRA_TARGET
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF_MARKER))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF, true);

                if (GetCaster()->HasAura(SPELL_PALADIN_JUDGMENT_HOLY_DEBUFF_MARKER))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_JUDGMENT_HOLY_DEBUFF, true);

                uint8 additionalTargets = 0;
                if (AuraEffect* levelBonus = GetCaster()->GetAuraEffect(SPELL_PALADIN_JUDGMENT_TARGET_BONUS, EFFECT_0))
                    additionalTargets += levelBonus->GetAmount();
                if (AuraEffect* greaterJudgment = GetCaster()->GetAuraEffect(SPELL_PALADIN_GREATER_JUDGMENT, EFFECT_1))
                    additionalTargets += greaterJudgment->GetAmount();

                if (additionalTargets == 0)
                    return;

                std::list<Unit*> targets;
                Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(GetHitUnit(), GetCaster(), 30.0f);
                Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(GetHitUnit(), targets, u_check);
                Cell::VisitAllObjects(GetHitUnit(), searcher, 30.0f);
                targets.remove(GetHitUnit());
                targets.sort(Trinity::ObjectDistanceOrderPred(GetHitUnit()));

                if (targets.size() > additionalTargets)
                    targets.resize(additionalTargets);

                ObjectGuid lastHitGUID;
                /// @TODO: Handle this like a chain effect
                for (Unit* target : targets)
                {
                    GetCaster()->CastSpell(target, SPELL_PALADIN_JUDGMENT_EXTRA_TARGET, true);

                    if (GetCaster()->HasAura(SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF_MARKER))
                        GetCaster()->CastSpell(target, SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF, true);

                    if (GetCaster()->HasAura(SPELL_PALADIN_JUDGMENT_HOLY_DEBUFF_MARKER))
                        GetCaster()->CastSpell(target, SPELL_PALADIN_JUDGMENT_HOLY_DEBUFF, true);

                    if (!lastHitGUID.IsEmpty())
                    {
                        if (Unit* lastHitUnit = ObjectAccessor::GetUnit(*GetCaster(), lastHitGUID))
                            lastHitUnit->SendPlaySpellVisual(target->GetGUID(), 61781, SPELL_MISS_NONE, SPELL_MISS_NONE, 35.0f, false);
                    }
                    else
                        GetHitUnit()->SendPlaySpellVisual(target->GetGUID(), 61781, SPELL_MISS_NONE, SPELL_MISS_NONE, 35.0f, false);

                    lastHitGUID = target->GetGUID();
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_judgment_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_judgment_SpellScript();
        }
};

// 196941 - Judgment of Light
class spell_pal_legion_judgment_of_light_proc : public SpellScriptLoader
{
    public:
        spell_pal_legion_judgment_of_light_proc() : SpellScriptLoader("spell_pal_legion_judgment_of_light_proc") { }

        class spell_pal_legion_judgment_of_light_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_judgment_of_light_proc_AuraScript);

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                eventInfo.GetActor()->CastSpell(eventInfo.GetActor(), GetSpellInfo()->GetEffect(aurEff->GetEffIndex())->TriggerSpell, true, nullptr, aurEff, GetCasterGUID());
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_judgment_of_light_proc_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_judgment_of_light_proc_AuraScript();
        }
};

// 216869 - Judgments of the Pure
class spell_pal_legion_judgments_of_the_pure : public SpellScriptLoader
{
    public:
        spell_pal_legion_judgments_of_the_pure() : SpellScriptLoader("spell_pal_legion_judgments_of_the_pure") { }

        class spell_pal_legion_judgments_of_the_pure_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_judgments_of_the_pure_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetCaster());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_judgments_of_the_pure_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_judgments_of_the_pure_SpellScript();
        }
};

// 231657 - Judgment
class spell_pal_legion_judgment_prot_bonus : public SpellScriptLoader
{
    public:
        spell_pal_legion_judgment_prot_bonus() : SpellScriptLoader("spell_pal_legion_judgment_prot_bonus") { }

        class spell_pal_legion_judgment_prot_bonus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_judgment_prot_bonus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                bool isCrit = (eventInfo.GetHitMask() & PROC_HIT_CRITICAL) != 0;
                GetTarget()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS)->ChargeCategoryId, isCrit ? (aurEff->GetAmount() * 2 * 1000) : (aurEff->GetAmount() * 1000));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_judgment_prot_bonus_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_judgment_prot_bonus_AuraScript();
        }
};

// 200302 - Knight of the Silver Hand
class spell_pal_legion_knight_of_the_silver_hand : public SpellScriptLoader
{
    public:
        spell_pal_legion_knight_of_the_silver_hand() : SpellScriptLoader("spell_pal_legion_knight_of_the_silver_hand") { }

        class spell_pal_legion_knight_of_the_silver_hand_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_knight_of_the_silver_hand_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_KNIGHT_OF_THE_SILVER_HAND_BONUS
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastCustomSpell(SPELL_PALADIN_KNIGHT_OF_THE_SILVER_HAND_BONUS, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_knight_of_the_silver_hand_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_knight_of_the_silver_hand_AuraScript();
        }
};

// 203791 - Last Defender
class spell_pal_legion_last_defender : public SpellScriptLoader
{
    public:
        spell_pal_legion_last_defender() : SpellScriptLoader("spell_pal_legion_last_defender") { }

        class spell_pal_legion_last_defender_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_last_defender_AuraScript);

            int32 CalculatePctBonus()
            {
                int32 insideTargetCount = 0;
                if (AreaTrigger* at = GetTarget()->GetAreaTrigger(GetId()))
                    for (ObjectGuid insideGUID : at->GetInsideUnits())
                        if (Unit* target = ObjectAccessor::GetUnit(*GetTarget(), insideGUID))
                            if (!GetTarget()->IsFriendlyTo(target) && GetTarget()->CanSeeOrDetect(target) && !target->IsCritter() && GetTarget()->IsValidAttackTarget(target))
                                insideTargetCount++;
                return (1 - std::pow(0.97, insideTargetCount)) * 100;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                absorbAmount = CalculatePct(dmgInfo.GetDamage(), CalculatePctBonus());
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 500;
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                const_cast<AuraEffect*>(aurEff)->ChangeAmount(CalculatePctBonus());
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetTarget());
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_last_defender_AuraScript::CalculateAmount, EFFECT_3, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_last_defender_AuraScript::HandleAbsorb, EFFECT_3);
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_pal_legion_last_defender_AuraScript::CalcPeriodic, EFFECT_4, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pal_legion_last_defender_AuraScript::HandleDummyTick, EFFECT_4, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_pal_legion_last_defender_AuraScript::HandleUpdatePeriodic, EFFECT_4, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_last_defender_AuraScript();
        }
};

// 216527 - Lawbringer
class spell_pal_legion_lawbringer : public SpellScriptLoader
{
    public:
        spell_pal_legion_lawbringer() : SpellScriptLoader("spell_pal_legion_lawbringer") { }

        class spell_pal_legion_lawbringer_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_lawbringer_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_PALADIN_JUDGMENT_RETRIBUTION_DEBUFF, GetCaster()->GetGUID()));
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(CalculatePct(GetHitUnit()->GetMaxHealth(), GetEffectValue()));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_lawbringer_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_lawbringer_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_lawbringer_SpellScript();
        }
};

// 85222 - Light of Dawn
class spell_pal_legion_light_of_dawn : public SpellScriptLoader
{
    public:
        spell_pal_legion_light_of_dawn() : SpellScriptLoader("spell_pal_legion_light_of_dawn") { }

        class spell_pal_legion_light_of_dawn_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_light_of_dawn_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LIGHT_OF_DAWN_HEAL
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.sort(Trinity::HealthPctOrderPred());
                if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_0))
                {
                    uint32 maxTargets = uint32(effInfo->CalcValue());
                    if (targets.size() > maxTargets)
                        targets.resize(maxTargets);
                }
            }

            void ClearTargets(std::list<WorldObject*>& targets)
            {
                // this effect is no longer used - clear the list to prevent a spell visual behind the paladin
                targets.clear();
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_LIGHT_OF_DAWN_HEAL, TRIGGERED_FULL_MASK);
            }

            void HandleAfterCast()
            {
                GetCaster()->RemoveAurasDueToSpell(SPELL_PALADIN_DIVINE_PURPOSE_LIGHT_OF_DAWN);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_light_of_dawn_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_CONE_ALLY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_light_of_dawn_SpellScript::ClearTargets, EFFECT_1, TARGET_UNIT_SRC_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_light_of_dawn_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_pal_legion_light_of_dawn_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_light_of_dawn_SpellScript();
        }
};

// 219562 - Light of the Martyr
class spell_pal_legion_light_of_the_martyr : public SpellScriptLoader
{
    public:
        spell_pal_legion_light_of_the_martyr() : SpellScriptLoader("spell_pal_legion_light_of_the_martyr") { }

        class spell_pal_legion_light_of_the_martyr_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_light_of_the_martyr_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LIGHT_OF_THE_MARTYR,
                    SPELL_PALADIN_LIGHT_OF_THE_MARTYR_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetHealInfo() && eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_LIGHT_OF_THE_MARTYR;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 damage = CalculatePct(eventInfo.GetHealInfo()->GetHeal(), aurEff->GetAmount());
                GetTarget()->CastCustomSpell(GetTarget(), SPELL_PALADIN_LIGHT_OF_THE_MARTYR_DAMAGE, &damage, nullptr, nullptr, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_light_of_the_martyr_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_light_of_the_martyr_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_light_of_the_martyr_AuraScript();
        }
};

// 184092 - Light of the Protector
// 213652 - Hand of the Protector
class spell_pal_legion_light_of_the_protector : public SpellScriptLoader
{
    public:
        spell_pal_legion_light_of_the_protector() : SpellScriptLoader("spell_pal_legion_light_of_the_protector") { }

        class spell_pal_legion_light_of_the_protector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_light_of_the_protector_SpellScript);

            void HandleHeal(SpellEffIndex effIndex)
            {
                int32 missingHealth = GetHitUnit()->GetHealth() - GetHitUnit()->GetMaxHealth();
                SetEffectValue(CalculatePct(abs(missingHealth), GetEffectInfo(effIndex)->CalcValue(GetCaster())));
            }

            void HandleBonusHeal(SpellEffIndex /*effIndex*/)
            {
                // Increases healing done by Light of the Protector by x%.
                if (AuraEffect* scatterTheShadows = GetCaster()->GetAuraEffect(SPELL_PALADIN_SCATTER_THE_SHADOWS_TRAIT, EFFECT_0))
                    SetHitHeal(GetHitHeal() + CalculatePct(GetHitHeal(), scatterTheShadows->GetAmount()));
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_pal_legion_light_of_the_protector_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_light_of_the_protector_SpellScript::HandleBonusHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_light_of_the_protector_SpellScript();
        }
};

// 53651 - Light's Beacon
class spell_pal_legion_light_s_beacon : public SpellScriptLoader
{
    public:
        spell_pal_legion_light_s_beacon() : SpellScriptLoader("spell_pal_legion_light_s_beacon") { }

        class spell_pal_legion_light_s_beacon_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_light_s_beacon_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_BEACON_OF_LIGHT_HEAL,
                    SPELL_PALADIN_LIGHT_OF_THE_MARTYR,
                    SPELL_PALADIN_BEACON_OF_LIGHT,
                    SPELL_PALADIN_BEACON_OF_FAITH,
                    SPELL_PALADIN_BEACON_OF_VIRTUE,
                    SPELL_PALADIN_BEACON_OF_LIGHT_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget() || !eventInfo.GetHealInfo())
                    return false;
                if (eventInfo.GetSpellInfo() && (eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_BEACON_OF_LIGHT_HEAL
                    || eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_LIGHT_OF_THE_MARTYR))
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                uint32 heal = CalculatePct(eventInfo.GetHealInfo()->GetHeal(), aurEff->GetAmount());
                bool faithActive = GetTarget()->GetTargetAuraApplications(SPELL_PALADIN_BEACON_OF_FAITH).size() > 0;
                bool lightActive = GetTarget()->GetTargetAuraApplications(SPELL_PALADIN_BEACON_OF_LIGHT).size() > 0;

                if (faithActive && lightActive)
                    if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PALADIN_BEACON_OF_FAITH))
                        if (SpellEffectInfo const* effInfo = spellInfo->GetEffect(EFFECT_3))
                            heal = CalculatePct(heal, (100 - effInfo->CalcValue()));

                for (auto itr : GetTarget()->GetTargetAuraApplications(SPELL_PALADIN_BEACON_OF_LIGHT))
                {
                    if (itr->GetTarget()->GetGUID() == eventInfo.GetActionTarget()->GetGUID())
                        continue;

                    GetTarget()->CastCustomSpell(SPELL_PALADIN_BEACON_OF_LIGHT_HEAL, SPELLVALUE_BASE_POINT0, heal, itr->GetTarget(), true);
                }

                for (auto itr : GetTarget()->GetTargetAuraApplications(SPELL_PALADIN_BEACON_OF_FAITH))
                {
                    if (itr->GetTarget()->GetGUID() == eventInfo.GetActionTarget()->GetGUID())
                        continue;

                    GetTarget()->CastCustomSpell(SPELL_PALADIN_BEACON_OF_LIGHT_HEAL, SPELLVALUE_BASE_POINT0, heal, itr->GetTarget(), true);
                }

                for (auto itr : GetTarget()->GetTargetAuraApplications(SPELL_PALADIN_BEACON_OF_VIRTUE))
                {
                    if (itr->GetTarget()->GetGUID() == eventInfo.GetActionTarget()->GetGUID())
                        continue;

                    GetTarget()->CastCustomSpell(SPELL_PALADIN_BEACON_OF_LIGHT_HEAL, SPELLVALUE_BASE_POINT0, heal, itr->GetTarget(), true);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_light_s_beacon_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_light_s_beacon_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_light_s_beacon_AuraScript();
        }
};

// 199428 - Luminescence
class spell_pal_legion_luminescence : public SpellScriptLoader
{
    public:
        spell_pal_legion_luminescence() : SpellScriptLoader("spell_pal_legion_luminescence") { }

        class spell_pal_legion_luminescence_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_luminescence_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LUMINESCENCE_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetHealInfo() || eventInfo.GetHealInfo()->GetHeal() == 0)
                    return false;

                return eventInfo.GetHealInfo()->GetHealer() && eventInfo.GetHealInfo()->GetHealer()->GetGUID() != GetCasterGUID();
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastCustomSpell(SPELL_PALADIN_LUMINESCENCE_HEAL, SPELLVALUE_BASE_POINT0, CalculatePct(eventInfo.GetHealInfo()->GetHeal(), aurEff->GetAmount()), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_luminescence_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_luminescence_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_luminescence_AuraScript();
        }
};

// 210540 - Pure of Heart
class spell_pal_legion_pure_of_heart : public SpellScriptLoader
{
    public:
        spell_pal_legion_pure_of_heart() : SpellScriptLoader("spell_pal_legion_pure_of_heart") { }

        class spell_pal_legion_pure_of_heart_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_pure_of_heart_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_PURE_OF_HEART_DISPEL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetHealInfo() && eventInfo.GetHealInfo()->GetHeal() > 0;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_PALADIN_PURE_OF_HEART_DISPEL, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_pure_of_heart_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_pure_of_heart_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_pure_of_heart_AuraScript();
        }
};

// 20066 - Repentance
class spell_pal_legion_repentance : public SpellScriptLoader
{
    public:
        spell_pal_legion_repentance() : SpellScriptLoader("spell_pal_legion_repentance") { }

        class spell_pal_legion_repentance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_repentance_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // Don't let Repentance break on selfdamage (effect 1)
                return eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id != GetId();
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_repentance_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_repentance_AuraScript();
        }
};

// 183435 - Retribution
class spell_pal_legion_retribution : public SpellScriptLoader
{
    public:
        spell_pal_legion_retribution() : SpellScriptLoader("spell_pal_legion_retribution") { }

        class spell_pal_legion_retribution_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_retribution_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_RETRIBUTION
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_PALADIN_RETRIBUTION, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_retribution_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_retribution_AuraScript();
        }
};

// 203797 - Retribution Aura
class spell_pal_legion_retribution_aura : public SpellScriptLoader
{
    public:
        spell_pal_legion_retribution_aura() : SpellScriptLoader("spell_pal_legion_retribution_aura") { }

        class spell_pal_legion_retribution_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_retribution_aura_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_RETRIBUTION_AURA_DAMAGE
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActor(), SPELL_PALADIN_RETRIBUTION_AURA_DAMAGE, true, nullptr, aurEff, GetCasterGUID());
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_retribution_aura_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_retribution_aura_AuraScript();
        }
};

// 152262 - Seraphim
class spell_pal_legion_seraphim : public SpellScriptLoader
{
    public:
        spell_pal_legion_seraphim() : SpellScriptLoader("spell_pal_legion_seraphim") { }

        class spell_pal_legion_seraphim_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_seraphim_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS
                });
            }

            SpellCastResult CheckCast()
            {
                if (!GetCaster()->GetSpellHistory()->HasCharge(sSpellMgr->AssertSpellInfo(SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS)->ChargeCategoryId))
                    return SPELL_FAILED_NO_CHARGES_REMAIN;

                return SPELL_CAST_OK;
            }

            void HandleDummy(SpellEffIndex effIndex)
            {
                if (SpellEffectInfo const* effInfo = GetEffectInfo(effIndex))
                {
                    uint32 consumedCharges = 0;
                    for (int32 i = 0; i < effInfo->CalcValue(); i++)
                        if (GetCaster()->GetSpellHistory()->ConsumeCharge(sSpellMgr->AssertSpellInfo(SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS)->ChargeCategoryId, true))
                            consumedCharges++;

                    if (consumedCharges > 0)
                    {
                        if (Aura* aura = GetHitAura())
                        {
                            aura->SetMaxDuration(aura->GetMaxDuration() * consumedCharges);
                            aura->SetDuration(aura->GetDuration() * consumedCharges);
                        }
                    }
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_pal_legion_seraphim_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_seraphim_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_seraphim_SpellScript();
        }
};

// 53600 - Shield of the Righteous
class spell_pal_legion_shield_of_the_righteous : public SpellScriptLoader
{
    public:
        spell_pal_legion_shield_of_the_righteous() : SpellScriptLoader("spell_pal_legion_shield_of_the_righteous") { }

        class spell_pal_legion_shield_of_the_righteous_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_shield_of_the_righteous_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS_BONUS,
                    SPELL_PALADIN_RIGHTEOUS_PROTECTOR,
                    SPELL_PALADIN_LIGHT_OF_THE_PROTECTOR,
                    SPELL_PALADIN_AVENGING_WRATH,
                    SPELL_PALADIN_HAND_OF_THE_PROTECTOR
                });
            }

            void AddBonus()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_SHIELD_OF_THE_RIGHTEOUS_BONUS, true);

                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_PALADIN_RIGHTEOUS_PROTECTOR, EFFECT_0))
                {
                    GetCaster()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_PALADIN_LIGHT_OF_THE_PROTECTOR)->ChargeCategoryId, aurEff->GetAmount() * 1000);
                    GetCaster()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_PALADIN_HAND_OF_THE_PROTECTOR)->ChargeCategoryId, aurEff->GetAmount() * 1000);
                    GetCaster()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_PALADIN_AVENGING_WRATH)->ChargeCategoryId, aurEff->GetAmount() * 1000);
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pal_legion_shield_of_the_righteous_SpellScript::AddBonus);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_shield_of_the_righteous_SpellScript();
        }
};

// 184662 - Shield of Vengeance
class spell_pal_legion_shield_of_vengeance : public SpellScriptLoader
{
    public:
        spell_pal_legion_shield_of_vengeance() : SpellScriptLoader("spell_pal_legion_shield_of_vengeance") { }

        class spell_pal_legion_shield_of_vengeance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_shield_of_vengeance_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_SHIELD_OF_VENGEANCE_DAMAGE
                });
            }

            void CalculateAmount(AuraEffect const* aurEff, int32 & amount, bool & /*canBeRecalculated*/)
            {
                amount = GetUnitOwner()->GetTotalAttackPowerValue(BASE_ATTACK) * GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(GetUnitOwner());
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
            }

            void HandleRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_ENEMY_SPELL)
                    return;

                GetTarget()->CastCustomSpell(SPELL_PALADIN_SHIELD_OF_VENGEANCE_DAMAGE, SPELLVALUE_BASE_POINT0, aurEff->GetDamage(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_shield_of_vengeance_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectRemove += AuraEffectApplyFn(spell_pal_legion_shield_of_vengeance_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_shield_of_vengeance_AuraScript();
        }
};

// 199456 - Spreading the Word
class spell_pal_legion_spreading_the_word : public SpellScriptLoader
{
    public:
        spell_pal_legion_spreading_the_word() : SpellScriptLoader("spell_pal_legion_spreading_the_word") { }

        class spell_pal_legion_spreading_the_word_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_spreading_the_word_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_SPREADING_THE_WORD_FREEDOM,
                    SPELL_PALADIN_SPREADING_THE_WORD_PROTECTION,
                    SPELL_PALADIN_BLESSING_OF_FREEDOM,
                    SPELL_PALADIN_BLESSING_OF_PROTECTION
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetSpellInfo() != nullptr;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_BLESSING_OF_FREEDOM)
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_SPREADING_THE_WORD_FREEDOM);
                else if (eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_BLESSING_OF_PROTECTION)
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_SPREADING_THE_WORD_PROTECTION);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_spreading_the_word_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_spreading_the_word_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_spreading_the_word_AuraScript();
        }
};

// 199507 - Spreading The Word: Protection
// 199508 - Spreading The Word: Freedom
class spell_pal_legion_spreading_the_world_selector : public SpellScriptLoader
{
    public:
        spell_pal_legion_spreading_the_world_selector() : SpellScriptLoader("spell_pal_legion_spreading_the_world_selector") { }

        class spell_pal_legion_spreading_the_world_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_spreading_the_world_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_DEVOTION_AURA,
                    SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB,
                    SPELL_PALADIN_AURA_OF_MERCY
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit())
                        if (!unitTarget->HasAura(SPELL_PALADIN_DEVOTION_AURA, caster->GetGUID()) &&
                            !unitTarget->HasAura(SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB, caster->GetGUID()) &&
                            !unitTarget->HasAura(SPELL_PALADIN_AURA_OF_MERCY, caster->GetGUID()))
                            return true;
                    return false;
                });
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_spreading_the_world_selector_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_spreading_the_world_selector_SpellScript();
        }
};

// 200482 - Second Sunrise
class spell_pal_legion_second_sunrise : public SpellScriptLoader
{
    public:
        spell_pal_legion_second_sunrise() : SpellScriptLoader("spell_pal_legion_second_sunrise") { }

        class spell_pal_legion_second_sunrise_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_second_sunrise_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LIGHT_OF_DAWN
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_LIGHT_OF_DAWN;
            }

            void OnEffProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                if (roll_chance_i(aurEff->GetAmount()))
                {
                    // triggered cast should ignore the cooldown in this case?! but it doesnt
                    GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_PALADIN_LIGHT_OF_DAWN);
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_LIGHT_OF_DAWN, true);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_second_sunrise_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_second_sunrise_AuraScript::OnEffProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_second_sunrise_AuraScript();
        }
};

// 200656 - Power of the silver hand
class spell_pal_legion_power_of_the_silver_hand : public SpellScriptLoader
{
    public:
        spell_pal_legion_power_of_the_silver_hand() : SpellScriptLoader("spell_pal_legion_power_of_the_silver_hand") { }

        class spell_pal_legion_power_of_the_silver_hand_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_power_of_the_silver_hand_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_LAY_ON_HANDS,
                    SPELL_PALADIN_AURA_OF_MERCY,
                    SPELL_PALADIN_POWER_OF_THE_SILVER_HAND_BUFF
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo() || !eventInfo.GetSpellInfo())
                    return false;
                if (eventInfo.GetDamageInfo()->GetDamage() <= 0)
                    return false;
                if (eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_LAY_ON_HANDS ||
                    eventInfo.GetSpellInfo()->Id == SPELL_PALADIN_AURA_OF_MERCY)
                    return false;

                return true;
            }

            void OnEffProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                damageSaved += CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
            }

            void OnEffRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(SPELL_PALADIN_POWER_OF_THE_SILVER_HAND_BUFF, SPELLVALUE_BASE_POINT0, damageSaved, caster, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_power_of_the_silver_hand_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_power_of_the_silver_hand_AuraScript::OnEffProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_pal_legion_power_of_the_silver_hand_AuraScript::OnEffRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }

        private:
            int32 damageSaved = 0;

        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_power_of_the_silver_hand_AuraScript();
        }
};

// 85256 - Templar's Verdict
class spell_pal_legion_templars_verdict : public SpellScriptLoader
{
    public:
        spell_pal_legion_templars_verdict() : SpellScriptLoader("spell_pal_legion_templars_verdict") { }

        class spell_pal_legion_templars_verdict_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_templars_verdict_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_TEMPLARS_VERDICT_DAMAGE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_TEMPLARS_VERDICT_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_templars_verdict_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_templars_verdict_SpellScript();
        }
};

// 200653 - Tyr's Deliverance
class spell_pal_legion_tyrs_deliverance : public SpellScriptLoader
{
    public:
        spell_pal_legion_tyrs_deliverance() : SpellScriptLoader("spell_pal_legion_tyrs_deliverance") { }

        class spell_pal_legion_tyrs_deliverance_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_tyrs_deliverance_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_TYRS_DELIVERANCE_HEAL
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.sort(Trinity::HealthPctOrderPred());
                if (targets.size() > 1)
                    targets.resize(1);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_TYRS_DELIVERANCE_HEAL, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_tyrs_deliverance_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_tyrs_deliverance_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_tyrs_deliverance_SpellScript();
        }
};

// 199448 - Ultimate Sacrifice
class spell_pal_legion_ultimate_sacrifice : public SpellScriptLoader
{
    public:
        spell_pal_legion_ultimate_sacrifice() : SpellScriptLoader("spell_pal_legion_ultimate_sacrifice") { }

        class spell_pal_legion_ultimate_sacrifice_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_ultimate_sacrifice_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_0))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_ULTIMATE_SACRIFICE_DOT
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                amount = -1;
            }

            void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (Unit* caster = GetCaster())
                {
                    if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PALADIN_ULTIMATE_SACRIFICE_DOT))
                    {
                        absorbAmount = CalculatePct(dmgInfo.GetDamage(), GetSpellInfo()->GetEffect(EFFECT_0)->CalcValue());
                        int32 dotDamage = int32(absorbAmount / (spellInfo->GetDuration() / IN_MILLISECONDS));
                        if (AuraEffect* aurEffDot = caster->GetAuraEffect(SPELL_PALADIN_ULTIMATE_SACRIFICE_DOT, EFFECT_0))
                        {
                            aurEffDot->GetBase()->RefreshDuration();
                            int32 newValue = caster->GetRemainingPeriodicAmount(caster->GetGUID(), SPELL_PALADIN_ULTIMATE_SACRIFICE_DOT, SPELL_AURA_PERIODIC_DAMAGE, EFFECT_0) + dotDamage;
                            aurEffDot->SetDamage(newValue);
                            aurEffDot->ChangeAmount(newValue);
                        }
                        else
                            caster->CastCustomSpell(SPELL_PALADIN_ULTIMATE_SACRIFICE_DOT, SPELLVALUE_BASE_POINT0, dotDamage, caster, true);
                    }
                    return;
                }
                absorbAmount = 0;
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_legion_ultimate_sacrifice_AuraScript::CalculateAmount, EFFECT_2, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_legion_ultimate_sacrifice_AuraScript::HandleAbsorb, EFFECT_2);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_ultimate_sacrifice_AuraScript();
        }
};

// 182234 - Unbreakable Will
class spell_pal_legion_unbreakable_will : public SpellScriptLoader
{
    public:
        spell_pal_legion_unbreakable_will() : SpellScriptLoader("spell_pal_legion_unbreakable_will") { }

        class spell_pal_legion_unbreakable_will_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_unbreakable_will_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_UNBREAKABLE_WILL_BLOCKER,
                    SPELL_PALADIN_UNBREAKABLE_WILL_CC_INFO
                });
            }

            bool hasMechanicWithMinDurationActive(int32 minDuration)
            {
                Unit::AuraApplicationMap unitAuras = GetTarget()->GetAppliedAuras();
                for (auto itr : unitAuras)
                {
                    SpellInfo const* spellInfo = itr.second->GetBase()->GetSpellInfo();
                    if (spellInfo->Mechanic && (MECHANIC_LOSS_CONTROL_MASK & (1 << spellInfo->Mechanic)))
                        if (itr.second->GetBase()->GetDuration() >= minDuration)
                            return true;

                    for (SpellEffectInfo const* effect : itr.second->GetBase()->GetSpellEffectInfos())
                        if (effect && effect->Effect && effect->Mechanic)
                            if (MECHANIC_LOSS_CONTROL_MASK & (1 << effect->Mechanic))
                                if (itr.second->GetBase()->GetDuration() >= minDuration)
                                    return true;
                }
                return false;
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 500;
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (GetTarget()->HasAura(SPELL_PALADIN_UNBREAKABLE_WILL_BLOCKER) || GetTarget()->HasAura(SPELL_PALADIN_UNBREAKABLE_WILL_CC_INFO))
                    return;

                if (hasMechanicWithMinDurationActive(aurEff->GetAmount() * 1000))
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_UNBREAKABLE_WILL_CC_INFO, true);
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_pal_legion_unbreakable_will_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pal_legion_unbreakable_will_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_pal_legion_unbreakable_will_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_unbreakable_will_AuraScript();
        }
};

// 182497 - Unbreakable Will
class spell_pal_legion_unbreakable_will_cc_info : public SpellScriptLoader
{
    public:
        spell_pal_legion_unbreakable_will_cc_info() : SpellScriptLoader("spell_pal_legion_unbreakable_will_cc_info") { }

        class spell_pal_legion_unbreakable_will_cc_info_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_unbreakable_will_cc_info_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_UNBREAKABLE_WILL_CC_REMOVE
                });
            }

            bool hasMechanicActive()
            {
                Unit::AuraApplicationMap unitAuras = GetTarget()->GetAppliedAuras();
                for (auto itr : unitAuras)
                {
                    SpellInfo const* spellInfo = itr.second->GetBase()->GetSpellInfo();
                    if (spellInfo->Mechanic && (MECHANIC_LOSS_CONTROL_MASK & (1 << spellInfo->Mechanic)))
                        return true;

                    for (SpellEffectInfo const* effect : itr.second->GetBase()->GetSpellEffectInfos())
                        if (effect && effect->Effect && effect->Mechanic)
                            if (MECHANIC_LOSS_CONTROL_MASK & (1 << effect->Mechanic))
                                return true;
                }
                return false;
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH)
                    return;

                if (hasMechanicActive())
                {
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_UNBREAKABLE_WILL_BLOCKER, true);
                    GetTarget()->RemoveAurasWithMechanic(MECHANIC_LOSS_CONTROL_MASK);
                    GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_UNBREAKABLE_WILL_CC_REMOVE, true);
                }
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_pal_legion_unbreakable_will_cc_info_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_unbreakable_will_cc_info_AuraScript();
        }
};

// 210323 - Vengeance Aura
class spell_pal_legion_vengeance_aura : public SpellScriptLoader
{
    public:
        spell_pal_legion_vengeance_aura() : SpellScriptLoader("spell_pal_legion_vengeance_aura") { }

        class spell_pal_legion_vengeance_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_vengeance_aura_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_GREATER_BLESSING_OF_KINGS,
                    SPELL_PALADIN_GREATER_BLESSING_OF_WISDOM,
                    SPELL_PALADIN_VENGEANCE_AURA_BONUS
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                return (eventInfo.GetSpellInfo()->GetAllEffectsMechanicMask() & MECHANIC_LOSS_CONTROL_MASK);
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_PALADIN_VENGEANCE_AURA_BONUS, true);
            }

            bool CheckAreaTarget(Unit* target)
            {
                return (GetCasterGUID() == target->GetGUID() || target->HasAura(SPELL_PALADIN_GREATER_BLESSING_OF_KINGS, GetCasterGUID()) || target->HasAura(SPELL_PALADIN_GREATER_BLESSING_OF_WISDOM, GetCasterGUID()));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_legion_vengeance_aura_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_legion_vengeance_aura_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
                DoCheckAreaTarget += AuraCheckAreaTargetFn(spell_pal_legion_vengeance_aura_AuraScript::CheckAreaTarget);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_vengeance_aura_AuraScript();
        }
};

// 205273 - Wake of Ashes
class spell_pal_legion_wake_of_ashes : public SpellScriptLoader
{
    public:
        spell_pal_legion_wake_of_ashes() : SpellScriptLoader("spell_pal_legion_wake_of_ashes") { }

        class spell_pal_legion_wake_of_ashes_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_wake_of_ashes_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_ASHES_TO_ASHES
                });
            }

            void HandleEffectApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (!caster->HasAura(SPELL_PALADIN_ASHES_TO_ASHES))
                    {
                        GetEffect(aurEff->GetEffIndex())->SetDamage(0);
                        GetEffect(aurEff->GetEffIndex())->SetAmount(0);
                    }
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pal_legion_wake_of_ashes_AuraScript::HandleEffectApply, EFFECT_2, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_wake_of_ashes_AuraScript();
        }

        class spell_pal_legion_wake_of_ashes_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_wake_of_ashes_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_ASHES_TO_ASHES,
                    SPELL_PALADIN_WAKE_OF_ASHES_ENERGIZE,
                    SPELL_PALADIN_WAKE_OF_ASHES_STUN
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_PALADIN_ASHES_TO_ASHES))
                    GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_WAKE_OF_ASHES_ENERGIZE, true);
            }

            void HandleStun(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->GetCreatureTypeMask() & CREATURE_TYPEMASK_DEMON_OR_UNDEAD)
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PALADIN_WAKE_OF_ASHES_STUN, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pal_legion_wake_of_ashes_SpellScript::HandleAfterCast);
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_wake_of_ashes_SpellScript::HandleStun, EFFECT_3, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_wake_of_ashes_SpellScript();
        }
};

// 210191 - Word of Glory
class spell_pal_legion_word_of_glory : public SpellScriptLoader
{
    public:
        spell_pal_legion_word_of_glory() : SpellScriptLoader("spell_pal_legion_word_of_glory") { }

        class spell_pal_legion_word_of_glory_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_word_of_glory_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PALADIN_WORD_OF_GLORY_SELFHEAL
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.sort(Trinity::HealthPctOrderPred());
                if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_1))
                    if (int32(targets.size()) > effInfo->CalcValue())
                        targets.resize(effInfo->CalcValue());
            }

            void HandleSelfHeal()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_PALADIN_WORD_OF_GLORY_SELFHEAL, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_word_of_glory_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
                AfterCast += SpellCastFn(spell_pal_legion_word_of_glory_SpellScript::HandleSelfHeal);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_word_of_glory_SpellScript();
        }
};

// 217020 - Zeal
class spell_pal_legion_zeal : public SpellScriptLoader
{
    public:
        spell_pal_legion_zeal() : SpellScriptLoader("spell_pal_legion_zeal") { }

        class spell_pal_legion_zeal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_zeal_SpellScript);

            void HandleDamageReduction(SpellEffIndex /*effIndex*/)
            {
                if (firstDamage == 0)
                {
                    firstDamage = GetHitDamage();
                    return;
                }

                if (SpellEffectInfo const* effInfo = GetEffectInfo(EFFECT_5))
                {
                    firstDamage = CalculatePct(firstDamage, effInfo->CalcValue());
                    SetHitDamage(firstDamage);
                }
            }

            private:
                int32 firstDamage = 0;

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_legion_zeal_SpellScript::HandleDamageReduction, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_zeal_SpellScript();
        }
};

class areatrigger_pal_legion_aura_of_sacrifice : public AreaTriggerEntityScript
{
    public:
        areatrigger_pal_legion_aura_of_sacrifice() : AreaTriggerEntityScript("areatrigger_pal_legion_aura_of_sacrifice") { }

        struct areatrigger_pal_legion_aura_of_sacrificeAI : public AreaTriggerAI
        {
            areatrigger_pal_legion_aura_of_sacrificeAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!caster->IsFriendlyTo(unit) || (unit->GetTypeId() == TYPEID_UNIT && unit->GetOwnerGUID().IsEmpty()))
                        return;

                    caster->CastSpell(unit, SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB, true);
                }
            }

            void OnUnitExit(Unit* unit) override
            {
                unit->RemoveAurasDueToSpell(SPELL_PALADIN_AURA_OF_SACRIFICE_ABSORB, at->GetCasterGuid());
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pal_legion_aura_of_sacrificeAI(areaTrigger);
        }
};

class areatrigger_pal_legion_blessed_hammer : public AreaTriggerEntityScript
{
    public:
        areatrigger_pal_legion_blessed_hammer() : AreaTriggerEntityScript("areatrigger_pal_legion_blessed_hammer") { }

        struct areatrigger_pal_legion_blessed_hammerAI : public AreaTriggerAI
        {
            areatrigger_pal_legion_blessed_hammerAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                    if (caster->IsValidAttackTarget(unit))
                    {
                        caster->CastSpell(unit, SPELL_PALADIN_BLESSED_HAMMER_DAMAGE, true);

                        if (caster->HasAura(SPELL_PALADIN_LAW_AND_ORDER_TRAIT))
                            caster->CastSpell(unit, SPELL_PALADIN_HAND_OF_HINDRANCE, true);
                    }
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pal_legion_blessed_hammerAI(areaTrigger);
        }
};

class areatrigger_pal_legion_consecration : public AreaTriggerEntityScript
{
    public:
        areatrigger_pal_legion_consecration() : AreaTriggerEntityScript("areatrigger_pal_legion_consecration") { }

        struct areatrigger_pal_legion_consecrationAI : public AreaTriggerAI
        {
            areatrigger_pal_legion_consecrationAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->GetTypeId() == TYPEID_PLAYER)
                        if (unit == caster && !caster->HasAura(SPELL_PALADIN_CONSERCRATED_HAMMER) && caster->ToPlayer()->GetRole() == TALENT_ROLE_TANK)
                            caster->CastSpell(caster, SPELL_PALADIN_CONSECRATION_PROT_BONUS, true);

                    if (caster->IsValidAttackTarget(unit))
                        caster->CastSpell(unit, SPELL_PALADIN_CONSERCRATED_DEBUFF, true);

                    if (caster->HasAura(SPELL_PALADIN_HALLOWED_GROUND_HONOR_TALENT))
                        if (caster->IsValidAssistTarget(unit))
                            caster->CastSpell(unit, SPELL_PALADIN_HALLOWED_GROUND_IMMUNITY, true);
                }
            }

            void OnUnitExit(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                    caster->RemoveAurasDueToSpell(SPELL_PALADIN_CONSECRATION_PROT_BONUS);

                unit->RemoveAurasDueToSpell(SPELL_PALADIN_CONSERCRATED_DEBUFF, at->GetCasterGuid());
                unit->RemoveAurasDueToSpell(SPELL_PALADIN_HALLOWED_GROUND_IMMUNITY, at->GetCasterGuid());

                // dat makes no sense!
                if (at->IsRemoved())
                    return;
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pal_legion_consecrationAI(areaTrigger);
        }
};

// 183425 - Devotion Aura
class areatrigger_pal_legion_devotion_aura : public AreaTriggerEntityScript
{
    public:
        areatrigger_pal_legion_devotion_aura() : AreaTriggerEntityScript("areatrigger_pal_legion_devotion_aura") { }

        struct areatrigger_pal_legion_devotion_auraAI : public AreaTriggerAI
        {
            areatrigger_pal_legion_devotion_auraAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void RecalculateAllDevotionAuras()
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->HasAura(SPELL_PALADIN_AURA_MASTERY))
                        return;

                    int32 auraCount = 0;
                    int32 newAmount = 0;
                    for (auto& guid : at->GetInsideUnits())
                        if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                            if (caster->IsFriendlyTo(target) && (target->GetTypeId() == TYPEID_PLAYER || (target->GetTypeId() == TYPEID_UNIT && !target->GetOwnerGUID().IsEmpty())))
                                auraCount++;

                    if (auraCount > 0)
                    {
                        if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PALADIN_DEVOTION_AURA))
                            if (SpellEffectInfo const* effInfo = spellInfo->GetEffect(EFFECT_0))
                                newAmount = std::min(int32(effInfo->CalcValue(caster) / auraCount), -1);

                        for (ObjectGuid const& guid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                                if (AuraEffect* aurEff = target->GetAuraEffect(SPELL_PALADIN_DEVOTION_AURA, EFFECT_0, at->GetCasterGuid()))
                                    aurEff->ChangeAmount(newAmount);
                    }
                }
            }

            void DoAction(int32 action) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (action == ACTION_AURA_MASTERY_APPLY)
                    {
                        for (auto& guid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                                if (AuraEffect* aurEff = target->GetAuraEffect(SPELL_PALADIN_DEVOTION_AURA, EFFECT_0, at->GetCasterGuid()))
                                    aurEff->ChangeAmount(aurEff->GetSpellEffectInfo()->CalcValue(caster));
                    }
                    else if (action == ACTION_AURA_MASTERY_REMOVE)
                        RecalculateAllDevotionAuras();
                }
            }

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!caster->IsFriendlyTo(unit) || !caster->CanSeeOrDetect(unit) || (unit->GetTypeId() == TYPEID_UNIT && (unit->GetOwnerGUID().IsEmpty() || !unit->GetOwnerGUID().IsPlayer())))
                        return;

                    if (caster->HasAura(SPELL_PALADIN_AURA_MASTERY))
                    {
                        caster->CastSpell(unit, SPELL_PALADIN_DEVOTION_AURA, true);
                        return;
                    }

                    int32 auraCount = 0;
                    int32 newAmount = 0;
                    for (auto& guid : at->GetInsideUnits())
                        if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                            if (caster->IsFriendlyTo(target) && (target->GetTypeId() == TYPEID_PLAYER || (target->GetTypeId() == TYPEID_UNIT && !target->GetOwnerGUID().IsEmpty())))
                                auraCount++;

                    if (auraCount > 0)
                    {
                        if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PALADIN_DEVOTION_AURA))
                            if (SpellEffectInfo const* effInfo = spellInfo->GetEffect(EFFECT_0))
                                newAmount = std::min(int32(effInfo->CalcValue(caster) / auraCount), -1);

                        for (auto& guid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                                if (AuraEffect* aurEff = target->GetAuraEffect(SPELL_PALADIN_DEVOTION_AURA, EFFECT_0, at->GetCasterGuid()))
                                    aurEff->ChangeAmount(newAmount);

                        caster->CastCustomSpell(unit, SPELL_PALADIN_DEVOTION_AURA, &newAmount, nullptr, nullptr, true);
                    }
                }
            }

            void OnUnitExit(Unit* unit) override
            {
                unit->RemoveAurasDueToSpell(SPELL_PALADIN_DEVOTION_AURA, at->GetCasterGuid());

                if (at->IsRemoved())
                    return;

                RecalculateAllDevotionAuras();
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pal_legion_devotion_auraAI(areaTrigger);
        }
};

class areatrigger_pal_legion_divine_tempest : public AreaTriggerEntityScript
{
    public:
        areatrigger_pal_legion_divine_tempest() : AreaTriggerEntityScript("areatrigger_pal_legion_divine_tempest") { }

        struct areatrigger_pal_legion_divine_tempestAI : public AreaTriggerAI
        {
            areatrigger_pal_legion_divine_tempestAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsValidAttackTarget(unit))
                        caster->CastSpell(unit, SPELL_PALADIN_DIVINE_STORM_DAMAGE, true);
                }
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pal_legion_divine_tempestAI(areaTrigger);
        }
};

class areatrigger_pal_legion_last_defender : public AreaTriggerEntityScript
{
    public:
        areatrigger_pal_legion_last_defender() : AreaTriggerEntityScript("areatrigger_pal_legion_last_defender") { }

        struct areatrigger_pal_legion_last_defenderAI : public AreaTriggerAI
        {
            areatrigger_pal_legion_last_defenderAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!caster->IsValidAttackTarget(unit))
                        return;

                    uint32 insideTargetCount = 0;
                    for (ObjectGuid insideGUID : at->GetInsideUnits())
                        if (Unit* target = ObjectAccessor::GetUnit(*caster, insideGUID))
                            if (!target->IsCritter() && caster->IsValidAttackTarget(target))
                                insideTargetCount++;

                    uint32 bonus = (1 - std::pow(0.97, insideTargetCount)) * 100;
                    if (AuraEffect* lastDefender = caster->GetAuraEffect(SPELL_PALADIN_LAST_DEFENDER, EFFECT_4))
                        lastDefender->ChangeAmount(bonus);
                }
            }

            void OnUnitExit(Unit* unit) override
            {
                if (at->IsRemoved())
                    return;

                if (Unit* caster = at->GetCaster())
                {
                    if (!caster->IsValidAttackTarget(unit))
                        return;

                    uint32 insideTargetCount = 0;
                    for (ObjectGuid insideGUID : at->GetInsideUnits())
                        if (Unit* target = ObjectAccessor::GetUnit(*caster, insideGUID))
                            if (!target->IsCritter() && caster->IsValidAttackTarget(target))
                                insideTargetCount++;

                    uint32 bonus = (1 - std::pow(0.97, insideTargetCount)) * 100;

                    if (AuraEffect* lastDefender = caster->GetAuraEffect(SPELL_PALADIN_LAST_DEFENDER, EFFECT_4))
                        lastDefender->ChangeAmount(bonus);
                }
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pal_legion_last_defenderAI(areaTrigger);
        }
};

void AddSC_legion_paladin_spell_scripts()
{
    new spell_pal_legion_ardent_defender();
    new spell_pal_legion_aura_mastery();
    new spell_pal_legion_aura_of_mercy();
    new spell_pal_legion_aura_of_sacrifice();
    new spell_pal_legion_aura_of_sacrifice_absorb();
    new spell_pal_legion_avenging_crusader();
    new spell_pal_legion_avenging_crusader_group_heal();
    new spell_pal_legion_avengers_shield();
    new spell_pal_legion_avenging_light();
    new spell_pal_legion_bastion_of_light();
    new spell_pal_legion_beacon_of_light();
    new spell_pal_legion_beacon_of_light_power_refund();
    new spell_pal_legion_beacon_of_virtue();
    new spell_pal_legion_blade_of_justice();
    new spell_pal_legion_blade_of_wrath();
    new spell_pal_legion_blade_of_wrath_proc();
    new spell_pal_legion_blessed_hammer_absorb();
    new spell_pal_legion_blessing_of_freedom();
    new spell_pal_legion_blessing_of_protection();
    new spell_pal_legion_blessing_of_sacrifice();
    new spell_pal_legion_blessing_of_sanctuary();
    new spell_pal_legion_blinding_light();
    new spell_pal_legion_blinding_light_proc();
    new spell_pal_legion_bulwark_of_order();
    new spell_pal_legion_cleanse();
    new spell_pal_legion_cleanse_the_weak_selector();
    new spell_pal_legion_consecration();
    new spell_pal_legion_consecration_heal();
    new spell_pal_legion_crusade_helper();
    new spell_pal_legion_crusaders_might();
    new spell_pal_legion_darkest_before_the_dawn();
    new spell_pal_legion_divine_hammer();
    new spell_pal_legion_divine_hammer_aoe();
    new spell_pal_legion_divine_intervention();
    new spell_pal_legion_divine_punisher();
    new spell_pal_legion_divine_purpose_holy();
    new spell_pal_legion_divine_purpose_retri();
    new spell_pal_legion_divine_shield();
    new spell_pal_legion_divine_steed();
    new spell_pal_legion_divine_steed_mount();
    new spell_pal_legion_divine_storm();
    new spell_pal_legion_eye_for_an_eye();
    new spell_pal_legion_faith_s_armor();
    new spell_pal_legion_fervent_martyr();
    new spell_pal_legion_fist_of_justice();
    new spell_pal_legion_fist_of_justice_retri();
    new spell_pal_legion_flash_of_light();
    new spell_pal_legion_grand_crusader();
    new spell_pal_legion_greater_blessing_of_kings();
    new spell_pal_legion_greater_blessing_of_wisdom();
    new spell_pal_legion_hammer_of_reckoning();
    new spell_pal_legion_hammer_of_the_righteous();
    new spell_pal_legion_hand_of_hindrance();
    new spell_pal_legion_holy_light();
    new spell_pal_legion_holy_prism();
    new spell_pal_legion_holy_prism_selector();
    new spell_pal_legion_holy_shield();
    new spell_pal_legion_holy_shock();
    new spell_pal_legion_holy_wrath();
    new spell_pal_legion_judgment();
    new spell_pal_legion_judgment_of_light_proc();
    new spell_pal_legion_judgments_of_the_pure();
    new spell_pal_legion_judgment_prot_bonus();
    new spell_pal_legion_knight_of_the_silver_hand();
    new spell_pal_legion_last_defender();
    new spell_pal_legion_lawbringer();
    new spell_pal_legion_light_of_dawn();
    new spell_pal_legion_light_of_the_martyr();
    new spell_pal_legion_light_of_the_protector();
    new spell_pal_legion_light_s_beacon();
    new spell_pal_legion_luminescence();
    new spell_pal_legion_pure_of_heart();
    new spell_pal_legion_power_of_the_silver_hand();
    new spell_pal_legion_repentance();
    new spell_pal_legion_retribution();
    new spell_pal_legion_retribution_aura();
    new spell_pal_legion_seraphim();
    new spell_pal_legion_second_sunrise();
    new spell_pal_legion_shield_of_the_righteous();
    new spell_pal_legion_shield_of_vengeance();
    new spell_pal_legion_spreading_the_word();
    new spell_pal_legion_spreading_the_world_selector();
    new spell_pal_legion_templars_verdict();
    new spell_pal_legion_tyrs_deliverance();
    new spell_pal_legion_ultimate_sacrifice();
    new spell_pal_legion_unbreakable_will();
    new spell_pal_legion_unbreakable_will_cc_info();
    new spell_pal_legion_vengeance_aura();
    new spell_pal_legion_wake_of_ashes();
    new spell_pal_legion_word_of_glory();
    new spell_pal_legion_zeal();

    new areatrigger_pal_legion_aura_of_sacrifice();
    new areatrigger_pal_legion_blessed_hammer();
    new areatrigger_pal_legion_consecration();
    new areatrigger_pal_legion_devotion_aura();
    new areatrigger_pal_legion_divine_tempest();
    new areatrigger_pal_legion_last_defender();
}
