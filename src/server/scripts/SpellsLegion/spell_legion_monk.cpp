/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_MONK and SPELLFAMILY_GENERIC spells used by monk players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_legion_monk_".
 */

#include "AreaTriggerAI.h"
#include "AreaTriggerTemplate.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "SpellHistory.h"
#include "Unit.h"
#include "Containers.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "ObjectGuid.h"
#include "TaskScheduler.h"
#include "ObjectAccessor.h"
#include "SpellMgr.h"
#include "Chat.h"

enum MonkSpells
{
    SPELL_MONK_AFTERLIFE_HEALING_SPHERE                 = 117032,
    SPELL_MONK_ANCIENT_MW_ARTS                          = 209520,
    SPELL_MONK_BLACK_OX_BREW                            = 115399,
    SPELL_MONK_BLACKOUT_COMBO_AURA                      = 228563,
    SPELL_MONK_BLACKOUT_KICK                            = 100784,
    SPELL_MONK_BLACKOUT_KICK_PROC                       = 116768,
    SPELL_MONK_BLACKOUT_KICK_TRIGGERED                  = 228649,
    SPELL_MONK_BLACKOUT_STRIKE                          = 205523,
    SPELL_MONK_BREATH_OF_FIRE                           = 115181,
    SPELL_MONK_BREATH_OF_FIRE_DOT                       = 123725,
    SPELL_MONK_CELESTIAL_BREATH                         = 199641,
    SPELL_MONK_CELESTIAL_BREATH_AURA                    = 199640,
    SPELL_MONK_CELESTIAL_BREATH_DEBUFF                  = 214411,
    SPELL_MONK_CELESTIAL_FORTUNE_AURA                   = 216519,
    SPELL_MONK_CELESTIAL_FORTUNE_HEAL                   = 216521,
    SPELL_MONK_CHI_JI_CRANE_HEAL_JUMP                   = 198756,
    SPELL_MONK_CHI_ORBIT_1                              = 196744,
    SPELL_MONK_CHI_ORBIT_2                              = 196745,
    SPELL_MONK_CHI_ORBIT_3                              = 196746,
    SPELL_MONK_CHI_ORBIT_4                              = 196747,
    SPELL_MONK_CHI_ORBIT_DAMAGE                         = 196748,
    SPELL_MONK_CHI_SPHERE                               = 163271,
    SPELL_MONK_CHI_WAVE                                 = 115098,
    SPELL_MONK_CHI_WAVE_DAMAGE                          = 132467,
    SPELL_MONK_CHI_WAVE_HEAL                            = 132463,
    SPELL_MONK_CHI_WAVE_HEAL_VISUAL                     = 132464,
    SPELL_MONK_CHI_WAVE_TARGET_SELECT                   = 132466,
    SPELL_MONK_COMBO_BREAKER                            = 137384,
    SPELL_MONK_COUNTERACT_MAGIC                         = 202428,
    SPELL_MONK_CRACKLING_JADE_LIGHTNING_CHANNEL         = 117952,
    SPELL_MONK_CRACKLING_JADE_LIGHTNING_KNOCKBACK       = 117962,
    SPELL_MONK_CRACKLING_JADE_LIGHTNING_KNOCKBACK_CD    = 117953,
    SPELL_MONK_CRANE_HEAL_ALLY_SEARCH                   = 198766,
    SPELL_MONK_CRANE_HEAL_DUMMY                         = 198764,
    SPELL_MONK_CRANE_HEAL_JUMP                          = 198756,
    SPELL_MONK_CYCLONE_STRIKES                          = 220357,
    SPELL_MONK_DAMPEN_HARM                              = 122278,
    SPELL_MONK_DARK_SIDE_OF_THE_MOON                    = 213062,
    SPELL_MONK_DARK_SIDE_OF_THE_MOON_BUFF               = 213063,
    SPELL_MONK_DAMPEN_HARM_VISUAL                       = 123715,
    SPELL_MONK_DANCING_MISTS                            = 199573,
    SPELL_MONK_DEATH_ART                                = 195266,
    SPELL_MONK_DISABLE                                  = 116095,
    SPELL_MONK_DISABLE_ROOT                             = 116706,
    SPELL_MONK_DOME_OF_MISTS_TALENT                     = 202577,
    SPELL_MONK_DOME_OF_MISTS_BUFF                       = 205655,
    SPELL_MONK_DOUBLE_BARREL                            = 202335,
    SPELL_MONK_DOUBLE_BARREL_STUN                       = 202346,
    SPELL_MONK_DRAGONFIRE_BREW                          = 213183,
    SPELL_MONK_DRAGONFIRE_BREW_DAMAGE                   = 227681,
    SPELL_MONK_EARIE_FERMINATION                        = 205147,
    SPELL_MONK_EARIE_FERMINATION_BUFF                   = 243943,
    SPELL_MONK_EFFUSE                                   = 116694,
    SPELL_MONK_ELUSIVE_BRAWLER                          = 195630,
    SPELL_MONK_ELUSIVE_DANCE                            = 196738,
    SPELL_MONK_ELUSIVE_DANCE_BUFF                       = 196739,
    SPELL_MONK_ENVELOPING_MIST                          = 124682,
    SPELL_MONK_ENVELOPING_MIST_HONOR_TALENT             = 227345,
    SPELL_MONK_ESSENCE_FONT                             = 191837,
    SPELL_MONK_ESSENCE_FONT_HEAL                        = 191840,
    SPELL_MONK_THUNDER_FOCUS_TEA_ESSENCE_FONT           = 197218,
    SPELL_MONK_FACE_PALM                                = 213116,
    SPELL_MONK_FACE_PALM_EFFECT                         = 227679,
    SPELL_MONK_FISTS_OF_FURY                            = 113656,
    SPELL_MONK_FISTS_OF_FURY_DAMAGE                     = 117418,
    SPELL_MONK_FISTS_OF_FURY_STUN                       = 232055,
    SPELL_MONK_FISTS_OF_FURY_VISUAL                     = 123154,
    SPELL_MONK_FLYING_SERPENT_KICK                      = 101545,
    SPELL_MONK_FLYING_SERPENT_KICK_DAMAGE               = 123586,
    SPELL_MONK_FLYING_SERPENT_KICK_SECOND               = 115057,
    SPELL_MONK_FORTIFYING_BREW                          = 115203,
    SPELL_MONK_FORTIFYING_BREW_TRIGGER                  = 120954,
    SPELL_MONK_FORTIFYING_BREW_STAGGER                  = 115203,
    SPELL_MONK_GALE_BURST_TALENT                        = 195399,
    SPELL_MONK_GALE_BURST_TARGET_AURA                   = 195403,
    SPELL_MONK_GIFT_OF_THE_MISTS                        = 196719,
    SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEALING_SPHERE    = 214418,
    SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEALING_SPHERE_1  = 214420,
    SPELL_MONK_GIFT_OF_THE_OX_HEALING_SPHERE            = 124503,
    SPELL_MONK_GIFT_OF_THE_OX_HEALING_SPHERE_1          = 124506,
    SPELL_MONK_GIFT_OF_THE_OX_HEAL                      = 124507,
    SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEAL              = 213464,
    SPELL_MONK_GIFT_OF_THE_OX_HEAL_EXPIRED              = 178173,
    SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEAL_EXPIRED      = 214417,
    SPELL_MONK_GOOD_KARMA                               = 195295,
    SPELL_MONK_GOOD_KARMA_HEAL                          = 195318,
    SPELL_MONK_HEALING_ELIXIR                           = 122281,
    SPELL_MONK_HEALING_ELIXIR_PASSIVE                   = 122280,
    SPELL_MONK_HEALING_WINDS                            = 195380,
    SPELL_MONK_HEALING_WINDS_HEAL                       = 195381,
    SPELL_MONK_HEAVY_HANDED_STRIKES                     = 205003,
    SPELL_MONK_HIGH_TOLERANCE                           = 196737,
    SPELL_MONK_HOT_TRUB                                 = 202126,
    SPELL_MONK_HOT_TRUB_DAMAGE                          = 202127,
    SPELL_MONK_INCENDIARY_BREATH                        = 202272,
    SPELL_MONK_INCENDIARY_BREATH_DEBUFF                 = 202274,
    SPELL_MONK_IRONSKIN_BREW                            = 115308,
    SPELL_MONK_IRONSKIN_BREW_BUFF                       = 215479,
    SPELL_MONK_KEG_SMASH                                = 121253,
    SPELL_MONK_LIFE_COCOON                              = 116849,
    SPELL_MONK_LIFECYCLES                               = 197915,
    SPELL_MONK_LIFECYCLES_ENVELOPING_MIST               = 197919,
    SPELL_MONK_LIFECYCLES_VIVIFY                        = 197916,
    SPELL_MONK_LIGHT_ON_YOUR_FEET                       = 199401,
    SPELL_MONK_LIGHT_ON_YOUR_FEET_BUFF                  = 199407,
    SPELL_MONK_LUNAR_GLIDE                              = 217153,
    SPELL_MONK_MARK_OF_THE_CRANE                        = 228287,
    SPELL_MONK_MASTERY_GUST_OF_MISTS                    = 117907,
    SPELL_MONK_MASTERY_HEAL                             = 191894,
    SPELL_MONK_MISTS_OF_LIFE                            = 199563,
    SPELL_MONK_MORTAL_WOUNDS                            = 115804,
    SPELL_MONK_NIUZAOS_ESSENCE                          = 232876,
    SPELL_MONK_NIUZAOS_ESSENCE_BUFF                     = 232877,
    SPELL_MONK_OVERFLOW                                 = 213180,
    SPELL_MONK_POWER_STRIKES_BUFF                       = 129914,
    SPELL_MONK_POWER_STRIKES_ENERGIZE                   = 121283,
    SPELL_MONK_POWER_STRIKES_TALENT                     = 121817,
    SPELL_MONK_PROTECTION_OF_SHAOHAO                    = 199367,
    SPELL_MONK_PROVOKE_AOE                              = 118635,
    SPELL_MONK_PROVOKE_SINGLE_TARGET                    = 116189,
    SPELL_MONK_PURIFYING_BREW                           = 119582,
    SPELL_MONK_RENEWING_MIST                            = 115151,
    SPELL_MONK_RENEWING_MIST_HOT                        = 119611,
    SPELL_MONK_RENEWING_MIST_JUMP                       = 119607,
    SPELL_MONK_VISUAL_RENEWING_MIST                     = 24599,
    SPELL_MONK_REFRESHING_BREEZE                        = 202523,
    SPELL_MONK_RING_OF_PEACE                            = 116844,
    SPELL_MONK_RING_OF_PEACE_KNOCKBACK                  = 142895,
    SPELL_MONK_RISING_SUN_KICK                          = 107428,
    SPELL_MONK_RISING_SUN_KICK_TRIGGERED                = 185099,
    SPELL_MONK_ROLL_FEATHER_FALL                        = 79636,    // Used in Sniff, not sure what it does yet
    SPELL_MONK_ROLL_TRIGGER                             = 107427,
    SPELL_MONK_RUSHING_JADE_WIND_PERIODIC               = 116847,
    SPELL_MONK_SHROUD_OF_MIST                           = 214478,
    SPELL_MONK_SHROUD_OF_MIST_AURA                      = 199365,
    SPELL_MONK_SOOTHING_MIST                            = 115175,
    SPELL_MONK_SOOTHING_MIST_PASSIVE                    = 193884,
    SPELL_MONK_SOOTHING_MIST_STATUE                     = 198533,
    SPELL_MONK_SOOTHING_MIST_HONOR_TALENT               = 209525,
    SPELL_MONK_SPAWN_SMOKE                              = 36747,    // Used in Sniff, spawn animation for chi-ji
    SPELL_MONK_SPECIAL_DELIVERY_AURA                    = 196733,
    SPELL_MONK_SPECIAL_DELIVERY_KEG                     = 196732,
    SPELL_MONK_SPECIAL_DELIVERY_TARGET_SELECT           = 196734,
    SPELL_MONK_SPIRIT_OF_THE_CRANE_MANA                 = 210803,
    SPELL_MONK_SPIRIT_OF_THE_CRANE_PASSIVE              = 210802,
    SPELL_MONK_STAGGER_BASE                             = 115069,   // Absorbs the according damage and puts it in the DoT
    SPELL_MONK_STAGGER_DOT                              = 124255,   // Deals damage absorbed by Base Stagger Aura and applies color buffs
    SPELL_MONK_STAGGER_HIGH                             = 124273,   // Over 66% of max HP
    SPELL_MONK_STAGGER_LIGHT                            = 124275,   // Less than 33% of max HP
    SPELL_MONK_STAGGER_MODERATE                         = 124274,   // Between 33%-66% of max HP
    SPELL_MONK_STAGGERING_AROUND                        = 213055,
    SPELL_MONK_TEACHINGS_OF_THE_MONASTERY               = 202090,
    SPELL_MONK_TEACHINGS_OF_THE_MONASTERY_PASSIVE       = 116645,
    SPELL_MONK_THUNDER_FOCUS_TEA                        = 116680,
    SPELL_MONK_TIGER_PALM                               = 100780,
    SPELL_MONK_TIGERS_LUST                              = 116841,
    SPELL_MONK_TORNADO_KICKS                            = 196082,
    SPELL_MONK_TOUCH_OF_DEATH                           = 115080,
    SPELL_MONK_TOUCH_OF_DEATH_DAMAGE                    = 229980,
    SPELL_MONK_TOUCH_OF_KARMA                           = 122470,
    SPELL_MONK_TOUCH_OF_KARMA_BUFF                      = 125174,
    SPELL_MONK_TOUCH_OF_KARMA_DAMAGE                    = 124280,
    SPELL_MONK_TRANSCENDENCE                            = 101643,
    SPELL_MONK_TRANSCENDENCE_CLONE                      = 119051,
    SPELL_MONK_TRANSCENDENCE_TRANSFER                   = 119996,
    SPELL_MONK_TRANSCENDENCE_VISUAL                     = 119053,
    SPELL_MONK_UPLIFTING_TRANCE                         = 197206,
    SPELL_MONK_VIVIFY                                   = 116670,
    SPELL_MONK_WAY_OF_THE_CRANE                         = 216113,
    SPELL_MONK_WAY_OF_THE_CRANE_HEAL                    = 216161,
    SPELL_MONK_WHIRLING_DRAGON_PUNCH                    = 152175,
    SPELL_MONK_WHIRLING_DRAGON_PUNCH_DAMAGE             = 158221,
    SPELL_MONK_WHIRLING_DRAGON_PUNCH_ENABLE             = 196742,
    SPELL_MONK_WINDWALKING_BUFF                         = 166646,
    SPELL_MONK_YU_LONGS_GIFT                            = 159535,
    SPELL_MONK_YU_LONS_GIFT_AURA                        = 159534,
    SPELL_MONK_ZEN_PILGRIMAGE_OVERRIDE_AURA             = 126896,
    SPELL_MONK_ZEN_PULSE_HEAL                           = 198487,
    SPELL_MONK_SPIRIT_TETHER_SLOW                       = 199387,
    SPELL_MONK_SHEILUNS_GIFT                            = 205406,
    SPELL_MONK_SHEILUNS_GIFT_AREATRIGGER                = 214501,
    SPELL_MONK_SHEILUNS_GIFT_COUNTER                    = 214502,
    SPELL_MONK_BLESSING_OF_YULON                        = 199665,
    SPELL_MONK_BLESSING_OF_YULON_DRAGON                 = 199671,
    SPELL_MONK_BLESSING_OF_YULON_PERIODIC_HEAL          = 199668,
    SPELL_MONK_GUST_OF_MISTS                            = 191894,

    // Visual spells
    SPELL_MONK_VISUAL_CHI_WAVE_DAMAGE_FIRST_CAST        = 24491,
    SPELL_MONK_VISUAL_CHI_WAVE_HEAL_FIRST_CAST          = 24490,
    SPELL_MONK_VISUAL_CHI_WAVE_DAMAGE                   = 38378,
    SPELL_MONK_VISUAL_CHI_WAVE_HEAL                     = 38379
};

enum MonkNPCs
{
    NPC_MONK_JADE_SERPENT_STATUE = 60849
};

enum MonkSpecAuras
{
    AURA_BREWMASTER_MONK = 137023,
    AURA_WINDWALKER_MONK = 137025,
    AURA_MISTWEAVER_MONK = 137024
};

static uint32 const colorBuffs[3] = { SPELL_MONK_STAGGER_LIGHT, SPELL_MONK_STAGGER_MODERATE, SPELL_MONK_STAGGER_HIGH };
static uint32 const chiOrbitHelper[4] = { SPELL_MONK_CHI_ORBIT_1, SPELL_MONK_CHI_ORBIT_2, SPELL_MONK_CHI_ORBIT_3, SPELL_MONK_CHI_ORBIT_4 };

// 116092 - Afterlife

class spell_legion_monk_afterlife : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_afterlife);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_AFTERLIFE_HEALING_SPHERE,
            SPELL_MONK_CHI_SPHERE
        });
    }
    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        GetUnitOwner()->CastSpell(GetUnitOwner(), SPELL_MONK_AFTERLIFE_HEALING_SPHERE, true);

        if (SpellInfo const* spellInfo = eventInfo.GetSpellInfo())
            if (spellInfo->Id == SPELL_MONK_BLACKOUT_KICK && roll_chance_i(aurEff->GetAmount()))
                GetUnitOwner()->CastSpell(GetUnitOwner(), SPELL_MONK_CHI_SPHERE, true);
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_afterlife::HandleProc, EFFECT_1, SPELL_AURA_DUMMY);
    }
};

// Fortification Proc Artefacttrait
class spell_legion_monk_fortification : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_fortification);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_FORTIFYING_BREW
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_MONK_FORTIFYING_BREW)
            return true;
        return false;
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_fortification::CheckProc);
    }
};

// 115399 - Black Ox Brew
class spell_legion_monk_black_ox_brew : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_black_ox_brew);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_IRONSKIN_BREW,
            SPELL_MONK_PURIFYING_BREW
        });
    }

    void HandleEnergize(SpellEffIndex /*effIndex*/)
    {
        GetCaster()->GetSpellHistory()->ResetCharges(sSpellMgr->AssertSpellInfo(SPELL_MONK_IRONSKIN_BREW)->ChargeCategoryId);
        GetCaster()->GetSpellHistory()->ResetCharges(sSpellMgr->AssertSpellInfo(SPELL_MONK_PURIFYING_BREW)->ChargeCategoryId);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_black_ox_brew::HandleEnergize, EFFECT_0, SPELL_EFFECT_ENERGIZE);
    }
};

class DelayedArtifactDamage : public BasicEvent
{
public:
    DelayedArtifactDamage(Unit* caster) : _caster(caster) { }

    bool Execute(uint64 /*execTime*/, uint32 /*diff*/) override
    {
        _caster->CastSpell(_caster, SPELL_MONK_DRAGONFIRE_BREW_DAMAGE, TRIGGERED_FULL_MASK);
        return true;
    }

private:
    Unit* _caster;
};

// 115181 - Breath of Fire
class spell_legion_monk_breath_of_fire : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_breath_of_fire);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_BREATH_OF_FIRE_DOT,
            SPELL_MONK_KEG_SMASH,
            SPELL_MONK_SPECIAL_DELIVERY_AURA,
            SPELL_MONK_DRAGONFIRE_BREW,
            SPELL_MONK_INCENDIARY_BREATH,
            SPELL_MONK_INCENDIARY_BREATH_DEBUFF
        });
    }

    void HandleTarget(std::list<WorldObject*>& targets)
    {
        dotTargets = targets;
        dotTargets.remove_if([this](WorldObject* target)
        {
            return !target->ToUnit()->HasAura(SPELL_MONK_KEG_SMASH) && !target->ToUnit()->HasAura(SPELL_MONK_SPECIAL_DELIVERY_AURA);
        });
    }

    void HandleDoT(SpellEffIndex /*effIndex*/)
    {
        for (WorldObject* target : dotTargets)
            if (Unit* targetUnit = target->ToUnit())
                GetCaster()->CastSpell(targetUnit, SPELL_MONK_BREATH_OF_FIRE_DOT, true);
    }

    void HandleArtifact()
    {
        if (AuraEffect const* dragonfireBrew = GetCaster()->GetAuraEffect(SPELL_MONK_DRAGONFIRE_BREW, EFFECT_0))
            for (int8 i = 1; i <= dragonfireBrew->GetAmount(); ++i)
                GetCaster()->m_Events.AddEvent(new DelayedArtifactDamage(GetCaster()), GetCaster()->m_Events.CalculateTime(i * 250));

        if (GetCaster()->HasAura(SPELL_MONK_INCENDIARY_BREATH))
            GetCaster()->CastSpell(GetCaster(), SPELL_MONK_INCENDIARY_BREATH_DEBUFF, true);
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_breath_of_fire::HandleTarget, EFFECT_0, TARGET_UNIT_CONE_ENEMY_104);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_breath_of_fire::HandleDoT, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
        AfterHit += SpellHitFn(spell_legion_monk_breath_of_fire::HandleArtifact);
    }
private:
    std::list<WorldObject*> dotTargets;
};

// 115098 - Chi Wave
class spell_legion_monk_chi_wave : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_chi_wave);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CHI_WAVE_DAMAGE,
            SPELL_MONK_CHI_WAVE_HEAL_VISUAL
        });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        uint32 spellId = GetCaster()->IsValidAttackTarget(GetHitUnit()) ? SPELL_MONK_CHI_WAVE_DAMAGE : SPELL_MONK_CHI_WAVE_HEAL_VISUAL;
        uint32 spellVisualId = spellId == SPELL_MONK_CHI_WAVE_DAMAGE ? SPELL_MONK_VISUAL_CHI_WAVE_DAMAGE_FIRST_CAST : SPELL_MONK_VISUAL_CHI_WAVE_HEAL_FIRST_CAST;

        GetCaster()->SendPlaySpellVisual(GetHitUnit()->GetGUID(), spellVisualId, SPELL_MISS_NONE, SPELL_MISS_NONE, GetSpellInfo()->Speed);
        GetCaster()->CastSpell(GetHitUnit(), spellId, false);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_wave::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 132464, 132467 - Chi Wave (heal visual & damage markers)
class spell_legion_monk_chi_wave_markers : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_chi_wave_markers);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CHI_WAVE_DAMAGE,
            SPELL_MONK_CHI_WAVE_HEAL_VISUAL,
            SPELL_MONK_CHI_WAVE_HEAL,
            SPELL_MONK_CHI_WAVE,
            SPELL_MONK_CHI_WAVE_TARGET_SELECT
        });
    }

    void HandleDamage(SpellEffIndex /*effIndex*/)
    {
        // HACK: tooltip hardcoded
        float damage = GetCaster()->GetTotalAttackPowerValue(BASE_ATTACK) * 0.867f;
        if (Player* player = GetCaster()->ToPlayer())
            AddPct(damage, player->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE));
        SetHitDamage(int32(damage));
    }

    void HandleBounces(SpellEffIndex /*effIndex*/)
    {
        int32 bounces = GetEffectValue() + 1;

        Unit* caster = GetOriginalCaster() ? GetOriginalCaster() : GetCaster();

        if (GetSpellInfo()->Id == SPELL_MONK_CHI_WAVE_HEAL_VISUAL)
            caster->CastSpell(GetHitUnit(), SPELL_MONK_CHI_WAVE_HEAL, true);

        if (SpellEffectInfo const* effect = sSpellMgr->AssertSpellInfo(SPELL_MONK_CHI_WAVE)->GetEffect(EFFECT_0))
            if (bounces < effect->CalcValue(caster))
                caster->CastCustomSpell(SPELL_MONK_CHI_WAVE_TARGET_SELECT, SPELLVALUE_BASE_POINT1, bounces, caster, true);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_MONK_CHI_WAVE_HEAL_VISUAL)
            OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_wave_markers::HandleBounces, EFFECT_1, SPELL_EFFECT_APPLY_AURA);
        else if (m_scriptSpellId == SPELL_MONK_CHI_WAVE_DAMAGE)
        {
            OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_wave_markers::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_wave_markers::HandleBounces, EFFECT_1, SPELL_EFFECT_APPLY_AURA);
        }
    }
};

// 132463 - Chi Wave (heal)
class spell_legion_monk_chi_wave_heal : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_chi_wave_heal);

    void HandleHeal(SpellEffIndex /*effIndex*/)
    {
        // HACK: tooltip hardcoded
        float heal = GetCaster()->GetTotalAttackPowerValue(BASE_ATTACK) * 0.867f;
        AddPct(heal, GetCaster()->ToPlayer()->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE));
        SetHitHeal(int32(heal));
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_wave_heal::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
    }
};

// 132466 - Chi Wave (target select)
class spell_legion_monk_chi_wave_target_select : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_chi_wave_target_select);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CHI_WAVE_DAMAGE,
            SPELL_MONK_CHI_WAVE_HEAL_VISUAL,
            SPELL_MONK_CHI_WAVE,
            SPELL_MONK_VISUAL_CHI_WAVE_DAMAGE,
            SPELL_MONK_VISUAL_CHI_WAVE_HEAL
        });
    }

    uint32 GetMarkerId(Unit* caster, Unit* target)
    {
        return caster->IsValidAttackTarget(target) ? SPELL_MONK_CHI_WAVE_DAMAGE : SPELL_MONK_CHI_WAVE_HEAL_VISUAL;
    }

    void FilterTargets(std::list<WorldObject*>& targets)
    {
        Unit* caster = GetCaster();
        std::list<WorldObject*> tmpTargets = targets;

        tmpTargets.remove_if([](WorldObject* a) { return !a->ToUnit(); });

        if (tmpTargets.empty())
        {
            targets.clear();
            return;
        }

        tmpTargets.sort([caster, this](WorldObject* a, WorldObject* b)
        {
            Unit* unitA = a->ToUnit();
            Unit* unitB = b->ToUnit();

            Aura* aura1 = unitA->GetAura(GetMarkerId(caster, unitA), caster->GetGUID());
            Aura* aura2 = unitB->GetAura(GetMarkerId(caster, unitB), caster->GetGUID());

            return aura1 && aura1->GetDuration() > (aura2 ? aura2->GetDuration() : 0);
        });

        Unit* previousTarget = tmpTargets.front()->ToUnit();
        _previousTargetGuid = previousTarget->GetGUID();

        bool enemy = !caster->IsValidAttackTarget(previousTarget);
        tmpTargets.remove_if([caster, enemy](WorldObject* a) { return caster->IsValidAttackTarget(a->ToUnit()) != enemy; });

        // Chi Wave can heal or damage the same target multiple times but not consecutively (must alternate between an ally and an enemy)
        // if we end up here that condition isn't met anymore and we should stop
        if (tmpTargets.empty())
        {
            targets.clear();
            return;
        }

        if (tmpTargets.size() > 1)
        {
            if (!enemy)
            {
                tmpTargets.sort(Trinity::HealthPctOrderPred());
                tmpTargets.resize(1);
            }
            else
                Trinity::Containers::RandomResize(tmpTargets, 1);
        }

        targets.swap(tmpTargets);
    }

    void HandleTarget(SpellEffIndex /*effIndex*/)
    {
        uint32 spellId = GetMarkerId(GetCaster(), GetHitUnit());
        uint32 spellVisualId = spellId == SPELL_MONK_CHI_WAVE_DAMAGE ? SPELL_MONK_VISUAL_CHI_WAVE_DAMAGE : SPELL_MONK_VISUAL_CHI_WAVE_HEAL;

        if (Unit* previousTarget = ObjectAccessor::GetUnit(*GetCaster(), _previousTargetGuid))
        {
            previousTarget->SendPlaySpellVisual(GetHitUnit()->GetGUID(), spellVisualId, SPELL_MISS_NONE, SPELL_MISS_NONE, sSpellMgr->AssertSpellInfo(SPELL_MONK_CHI_WAVE)->Speed);
            previousTarget->CastCustomSpell(spellId, SPELLVALUE_BASE_POINT1, GetEffectValue(), GetHitUnit(), false, NULL, nullptr, GetCaster()->GetGUID());
        }
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_chi_wave_target_select::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENTRY);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_wave_target_select::HandleTarget, EFFECT_1, SPELL_EFFECT_DUMMY);
    }
private:
    ObjectGuid _previousTargetGuid;
};

// 227344 - Effuse (Ancient MW Arts Honor Talents)
class spell_legion_monk_effuse_honor_talent : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_effuse_honor_talent);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_SOOTHING_MIST_HONOR_TALENT
        });
    }

    void HandleOnPrepare()
    {
        if (GetCaster()->GetCurrentSpell(CURRENT_CHANNELED_SPELL) && GetCaster()->GetCurrentSpell(CURRENT_CHANNELED_SPELL)->GetSpellInfo()->Id == SPELL_MONK_SOOTHING_MIST_HONOR_TALENT)
        {
            TriggerCastFlags castFlags = TriggerCastFlags(GetSpell()->GetTriggerCastFlags() | TRIGGERED_CAST_DIRECTLY);
            GetSpell()->SetTriggerCastFlags(castFlags);
            SpellCastTargets targets = GetCaster()->GetCurrentSpell(CURRENT_CHANNELED_SPELL)->m_targets;
            GetSpell()->InitExplicitTargets(targets);
        }
    }

    void Register() override
    {
        OnPrepare += SpellPrepareFn(spell_legion_monk_effuse_honor_talent::HandleOnPrepare);
    }
};

// 121253 - Keg Smash
class spell_legion_monk_keg_smash : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_keg_smash);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_IRONSKIN_BREW,
            SPELL_MONK_PURIFYING_BREW,
            SPELL_MONK_BLACK_OX_BREW,
            SPELL_MONK_FORTIFYING_BREW,
            SPELL_MONK_BLACKOUT_COMBO_AURA,
            SPELL_MONK_DOUBLE_BARREL,
            SPELL_MONK_DOUBLE_BARREL_STUN
        });
    }

    void HandleBrewCD(SpellEffIndex /*effIndex*/)
    {
        int32 cdReduce = GetEffectValue() * IN_MILLISECONDS;
        if (Aura* blackoutAura = GetCaster()->GetAura(SPELL_MONK_BLACKOUT_COMBO_AURA))
            if (AuraEffect* aurEff = blackoutAura->GetEffect(EFFECT_2))
                cdReduce += aurEff->GetAmount() * IN_MILLISECONDS;
        GetCaster()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_MONK_PURIFYING_BREW)->ChargeCategoryId, cdReduce);
        GetCaster()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_MONK_IRONSKIN_BREW)->ChargeCategoryId, cdReduce);
        GetCaster()->GetSpellHistory()->ModifyCooldown(SPELL_MONK_BLACK_OX_BREW, -cdReduce);
        GetCaster()->GetSpellHistory()->ModifyCooldown(SPELL_MONK_FORTIFYING_BREW, -cdReduce);
    }

    void HandleProcs(SpellEffIndex /*effIndex*/)
    {
        if (GetCaster()->HasAura(SPELL_MONK_DOUBLE_BARREL))
            GetCaster()->CastSpell(GetHitUnit(), SPELL_MONK_DOUBLE_BARREL_STUN, true);
    }

    void RemoveAura()
    {
        GetCaster()->RemoveAurasDueToSpell(SPELL_MONK_DOUBLE_BARREL);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_keg_smash::HandleBrewCD, EFFECT_2, SPELL_EFFECT_DUMMY);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_keg_smash::HandleProcs, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
        AfterHit += SpellHitFn(spell_legion_monk_keg_smash::RemoveAura);
    }
};

// 216519 - Celestial Fortune (Passive)
class spell_legion_monk_celestial_fortune : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_celestial_fortune);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CELESTIAL_FORTUNE_HEAL
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetDamageInfo() != nullptr;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        float critChance = GetTarget()->GetFloatValue(PLAYER_CRIT_PERCENTAGE);
        if (roll_chance_i(critChance))
        {
            int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
            eventInfo.GetActionTarget()->CastCustomSpell(SPELL_MONK_CELESTIAL_FORTUNE_HEAL, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetActionTarget(), true);
        }
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_celestial_fortune::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_celestial_fortune::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 195403 - Gale Burst Target Aura
class spell_legion_monk_gale_burst : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_gale_burst);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TOUCH_OF_DEATH,
            SPELL_MONK_GALE_BURST_TALENT
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetDamageInfo() != nullptr;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        if (AuraEffect* aurEff = GetTarget()->GetAuraEffect(SPELL_MONK_TOUCH_OF_DEATH, EFFECT_0))
            if (eventInfo.GetDamageInfo()->GetDamage() > 0)
                if (AuraEffect* aurEffGaleBurstTalent = eventInfo.GetActor()->GetAuraEffect(SPELL_MONK_GALE_BURST_TALENT, EFFECT_0))
                {
                    int32 damage = aurEff->GetAmount() + CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEffGaleBurstTalent->GetAmount());
                    aurEff->SetDamage(damage);
                    aurEff->SetAmount(damage);
                }
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_gale_burst::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_gale_burst::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 124502 - Gift of the Ox
class spell_legion_monk_gift_of_the_ox : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_gift_of_the_ox);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_GIFT_OF_THE_OX_HEALING_SPHERE,
            SPELL_MONK_GIFT_OF_THE_OX_HEALING_SPHERE_1
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        // can't proc off Stagger
        if (SpellInfo const* spellInfo = eventInfo.GetSpellInfo())
            if (spellInfo->Id == SPELL_MONK_STAGGER_DOT)
                return false;

        return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage();
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        Unit* owner = GetUnitOwner();

        uint64 peekHealth = owner->GetHealth() + eventInfo.GetDamageInfo()->GetDamage();
        uint64 peekDamage = eventInfo.GetDamageInfo()->GetDamage();

        _procChance = (float(CalculatePct(peekDamage, 75.f)) / float(owner->GetMaxHealth())) * (3.f - 2.f * owner->GetHealthPct() / 100.0f);

        // Gift of the Mists (talent)
        if (AuraEffect const* giftOfTheMists = owner->GetAuraEffect(SPELL_MONK_GIFT_OF_THE_MISTS, EFFECT_0))
        {
            _procChance *= 1.f + CalculatePct(1.f - (float(peekHealth) - float(peekDamage)) / float(owner->GetMaxHealth()), giftOfTheMists->GetAmount());
        }

        if (roll_chance_f(_procChance * 100.f))
        {
            _procChance = 0;

            static const uint32 sphere[] = { SPELL_MONK_GIFT_OF_THE_OX_HEALING_SPHERE, SPELL_MONK_GIFT_OF_THE_OX_HEALING_SPHERE_1 };
            static const uint32 greaterSphere[] = { SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEALING_SPHERE, SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEALING_SPHERE_1 };

            uint32 spellId = Trinity::Containers::SelectRandomContainerElement(sphere);

            // Overflow (Artifact trait)
            if (AuraEffect const* overflow = GetUnitOwner()->GetAuraEffect(SPELL_MONK_OVERFLOW, EFFECT_0))
                if (roll_chance_i(overflow->GetAmount()))
                    spellId = Trinity::Containers::SelectRandomContainerElement(greaterSphere);

            GetUnitOwner()->CastSpell(GetUnitOwner(), spellId, true);
        }
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_gift_of_the_ox::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_gift_of_the_ox::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
private:
    float _procChance = 0.f;
};

// 178173 - Gift of the Ox (when healing sphere expires before being used)
// 214417 - Greater Gift of the Ox
class spell_legion_monk_gift_of_the_ox_healing_sphere_expired : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_gift_of_the_ox_healing_sphere_expired);

    void HandleTarget(std::list<WorldObject*>& targets)
    {
        Unit* caster = GetCaster();
        targets.remove_if([caster](WorldObject* target)
        {
            return caster->GetGUID() != target->GetGUID();
        });
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_gift_of_the_ox_healing_sphere_expired::HandleTarget, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
    }
};

// 196082 - Tornado Kicks
class spell_legion_monk_tornado_kicks : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_tornado_kicks);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_RISING_SUN_KICK_TRIGGERED
        });
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        GetTarget()->CastCustomSpell(SPELL_MONK_RISING_SUN_KICK_TRIGGERED, SPELLVALUE_CUSTOM_FLAGS, CUSTOM_CAST_FLAG_SPECIAL_CALCULATION, eventInfo.GetProcTarget(), TRIGGERED_FULL_MASK);
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_tornado_kicks::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 124682 - Enveloping Mist
// 227345 - Enveloping Mist (Replacer Honor Talents)
class spell_legion_monk_enveloping_mist : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_enveloping_mist);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_SOOTHING_MIST_PASSIVE,
            SPELL_MONK_SOOTHING_MIST,
            SPELL_MONK_LIFECYCLES,
            SPELL_MONK_LIFECYCLES_VIVIFY,
            SPELL_MONK_MASTERY_HEAL
        });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();
        Unit* explTarget = GetExplTargetUnit();

        if (caster->HasAura(SPELL_MONK_LIFECYCLES))
            caster->CastSpell(caster, SPELL_MONK_LIFECYCLES_VIVIFY, true);

        if (caster->HasAura(SPELL_MONK_SOOTHING_MIST_PASSIVE) && !caster->HasAura(SPELL_MONK_ANCIENT_MW_ARTS))
            caster->CastSpell(explTarget, SPELL_MONK_SOOTHING_MIST, true);
    }

    void HandleOnPrepare()
    {
        if (GetCaster()->GetCurrentSpell(CURRENT_CHANNELED_SPELL) && GetCaster()->GetCurrentSpell(CURRENT_CHANNELED_SPELL)->GetSpellInfo()->Id == SPELL_MONK_SOOTHING_MIST_HONOR_TALENT)
        {
            TriggerCastFlags castFlags = TriggerCastFlags(GetSpell()->GetTriggerCastFlags() | TRIGGERED_CAST_DIRECTLY);
            GetSpell()->SetTriggerCastFlags(castFlags);
            SpellCastTargets targets = GetCaster()->GetCurrentSpell(CURRENT_CHANNELED_SPELL)->m_targets;
            GetSpell()->InitExplicitTargets(targets);
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_enveloping_mist::HandleDummy, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
        OnPrepare += SpellPrepareFn(spell_legion_monk_enveloping_mist::HandleOnPrepare);
    }
};

class spell_legion_monk_enveloping_mist_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_enveloping_mist_AuraScript);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_DOME_OF_MISTS_TALENT,
            SPELL_MONK_DOME_OF_MISTS_BUFF
        });
    }

    void HandleDispel(DispelInfo* /*dispelInfo*/)
    {
        if (Unit* caster = GetCaster())
        {
            if (caster->HasAura(SPELL_MONK_DOME_OF_MISTS_TALENT))
            {
                if (AuraEffect* aurEff = caster->GetAuraEffect(SPELL_MONK_ENVELOPING_MIST_HONOR_TALENT, EFFECT_0, GetCasterGUID()))
                {
                    int32 remainingAmount = aurEff->GetDamage() * (aurEff->GetTotalTicks() - aurEff->GetTickNumber());
                    if (AuraEffect* aurEffDome = GetUnitOwner()->GetAuraEffect(SPELL_MONK_DOME_OF_MISTS_BUFF, EFFECT_0, GetCasterGUID()))
                        remainingAmount += aurEffDome->GetAmount();
                    caster->CastCustomSpell(SPELL_MONK_DOME_OF_MISTS_BUFF, SPELLVALUE_BASE_POINT0, remainingAmount, GetUnitOwner(), true);
                }
                else if (AuraEffect* envelopingMist = caster->GetAuraEffect(SPELL_MONK_ENVELOPING_MIST, EFFECT_0, GetCasterGUID()))
                {
                    int32 remainingAmount = envelopingMist->GetDamage() * (envelopingMist->GetTotalTicks() - envelopingMist->GetTickNumber());
                    if (AuraEffect* aurEffDome = GetUnitOwner()->GetAuraEffect(SPELL_MONK_DOME_OF_MISTS_BUFF, EFFECT_0, GetCasterGUID()))
                        remainingAmount += aurEffDome->GetAmount();
                    caster->CastCustomSpell(SPELL_MONK_DOME_OF_MISTS_BUFF, SPELLVALUE_BASE_POINT0, remainingAmount, GetUnitOwner(), true);
                }
            }
        }
    }

    void Register() override
    {
        OnDispel += AuraDispelFn(spell_legion_monk_enveloping_mist_AuraScript::HandleDispel);
    }
};

// 191840 - Essence Font Heal
class spell_legion_monk_essence_font_heal : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_essence_font_heal);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_ESSENCE_FONT
        });
    }

    void HandleTarget(std::list<WorldObject*>& targets)
    {
        targets.push_back(GetCaster());
        Trinity::Containers::RandomResize(targets, [this](WorldObject* target) {
            if (target->GetTypeId() == TYPEID_UNIT || target->GetTypeId() == TYPEID_PLAYER)
            {
                // When heal aura is already on target for a second(Amplitude * 6), it becomes eligable again
                if (Aura* essenceFontAura = target->ToUnit()->GetAura(SPELL_MONK_ESSENCE_FONT_HEAL, GetCaster()->GetGUID()))
                    if (essenceFontAura->GetDuration() < essenceFontAura->GetMaxDuration() - 1 * IN_MILLISECONDS)
                        return true;
                // Else we filter targets with aura already
                return !target->ToUnit()->HasAura(SPELL_MONK_ESSENCE_FONT_HEAL, GetCaster()->GetGUID());
            }
            return false;
        }, 1);
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_essence_font_heal::HandleTarget, EFFECT_ALL, TARGET_UNIT_DEST_AREA_ALLY);
    }
};

// 122278 - Dampen Harm
class spell_legion_monk_dampen_harm : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_dampen_harm);

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        amount = -1;
    }

    void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
    {
        uint64 maxHPLoss = uint64((dmgInfo.GetDamage() * 100)) / GetTarget()->GetMaxHealth();
        if (AuraEffect* aurEff1 = GetEffect(EFFECT_1))
            if (AuraEffect* aurEff2 = GetEffect(EFFECT_2))
            {
                // Calc Reduction, but not higher than 50%
                uint32 reduction = std::min(uint32(aurEff2->GetAmount()), (uint32(aurEff2->GetAmount()) / 25) * uint32(maxHPLoss));
                // ... and not lower than 20%
                reduction = std::max(uint32(aurEff1->GetAmount()), reduction);
                absorbAmount = CalculatePct(dmgInfo.GetDamage(), reduction);
                return;
            }

        absorbAmount = 0;
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_dampen_harm::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_monk_dampen_harm::HandleAbsorb, EFFECT_0);
    }
};

// 239307 - Dark side of the moon
class spell_legion_monk_dark_side_of_the_moon : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_dark_side_of_the_moon);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetDamageInfo() && eventInfo.GetSpellInfo() &&
            eventInfo.GetSpellInfo()->Id == SPELL_MONK_BLACKOUT_STRIKE)
            return true;
        return false;
    }

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        amount = -1;
    }

    void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
    {
        if (dmgInfo.GetDamageType() == DIRECT_DAMAGE)
            if (Unit* attacker = dmgInfo.GetAttacker())
                if (AuraEffect* aurEffDarkSideTarget = attacker->GetAuraEffect(SPELL_MONK_DARK_SIDE_OF_THE_MOON_BUFF, EFFECT_0, GetTarget()->GetGUID()))
                {
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), aurEffDarkSideTarget->GetAmount());
                    aurEffDarkSideTarget->GetBase()->Remove();
                    return;
                }

        absorbAmount = 0;   // Don't absorb anything
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_dark_side_of_the_moon::CheckProc);
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_dark_side_of_the_moon::CalculateAmount, EFFECT_1, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_monk_dark_side_of_the_moon::HandleAbsorb, EFFECT_1);
    }
};

// 122783 - Diffuse Magic
class spell_legion_monk_diffuse_magic : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_diffuse_magic);

    void HandleOnHit()
    {
        if (Player* monk = GetCaster()->ToPlayer())
        {
            Unit::AuraApplicationMap auraList = monk->GetAppliedAuras();
            for (Unit::AuraApplicationMap::iterator iter = auraList.begin(); iter != auraList.end(); ++iter)
            {
                Aura* aura = iter->second->GetBase();
                if (!aura)
                    continue;

                Unit* caster = aura->GetCaster();
                if (!caster || caster->GetGUID() == monk->GetGUID())
                    continue;

                if (!caster->IsWithinDist(monk, 40.0f))
                    continue;

                if (aura->GetSpellInfo()->IsPositive())
                    continue;

                if (!(aura->GetSpellInfo()->GetSchoolMask() & SPELL_SCHOOL_MASK_MAGIC))
                    continue;

                if (!(aura->GetSpellInfo()->GetDispelMask() & 1 << DISPEL_MAGIC))
                    continue;

                monk->AddAura(aura->GetSpellInfo()->Id, caster);

                if (Aura* targetAura = caster->GetAura(aura->GetSpellInfo()->Id, monk->GetGUID()))
                {
                    for (int i = 0; i < MAX_SPELL_EFFECTS; ++i)
                    {
                        if (targetAura->GetEffect(i) && aura->GetEffect(i))
                        {
                            AuraEffect const* auraEffect = monk->GetAuraEffect(aura->GetSpellInfo()->Id, i);
                            if (!auraEffect)
                                continue;

                            int32 damage = auraEffect->GetAmount();

                            if (auraEffect->GetAuraType() == SPELL_AURA_PERIODIC_DAMAGE ||
                                auraEffect->GetAuraType() == SPELL_AURA_PERIODIC_DAMAGE_PERCENT)
                                damage = caster->SpellDamageBonusDone(monk, aura->GetSpellInfo(), damage, DOT, auraEffect->GetSpellEffectInfo(), auraEffect->GetBase()->GetStackAmount());

                            targetAura->SetDuration(aura->GetDuration());
                            targetAura->GetEffect(i)->SetAmount(damage);
                        }
                    }
                }

                monk->RemoveAura(aura);
            }
        }
    }

    void Register() override
    {
        OnHit += SpellHitFn(spell_legion_monk_diffuse_magic::HandleOnHit);
    }
};

// 116095 - Disable
class spell_legion_monk_disable : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_disable);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_DISABLE_ROOT
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (GetCasterGUID() == eventInfo.GetActor()->GetGUID())
            return true;
        return false;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        GetAura()->RefreshDuration();
    }

    void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();
        Unit* target = GetTarget();

        if (caster && HasAuraWithMechanic(1 << MECHANIC_SNARE, true))
            caster->CastSpell(target, SPELL_MONK_DISABLE_ROOT, true);
    }

    void HandleReapply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();
        Unit* target = GetTarget();

        if (caster && HasAuraWithMechanic(1 << MECHANIC_SNARE, false))
            caster->CastSpell(target, SPELL_MONK_DISABLE_ROOT, true);
    }

    bool HasAuraWithMechanic(uint32 mechanicMask, bool exceptThisSpell)
    {
        for (auto const& iter : GetTarget()->GetAppliedAuras())
        {
            SpellInfo const* spellInfo = iter.second->GetBase()->GetSpellInfo();
            if (exceptThisSpell && spellInfo == GetSpellInfo())
                continue;

            if (spellInfo->Mechanic && (mechanicMask & (1 << spellInfo->Mechanic)))
                return true;

            for (SpellEffectInfo const* effect : iter.second->GetBase()->GetSpellEffectInfos())
                if (effect && effect->Effect && effect->Mechanic)
                    if (mechanicMask & (1 << effect->Mechanic))
                        return true;
        }

        return false;
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_disable::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_disable::HandleProc, EFFECT_0, SPELL_AURA_MOD_DECREASE_SPEED);
        OnEffectApply += AuraEffectApplyFn(spell_legion_monk_disable::HandleApply, EFFECT_0, SPELL_AURA_MOD_DECREASE_SPEED, AURA_EFFECT_HANDLE_REAL);
        OnEffectApply += AuraEffectApplyFn(spell_legion_monk_disable::HandleReapply, EFFECT_0, SPELL_AURA_MOD_DECREASE_SPEED, AURA_EFFECT_HANDLE_REAPPLY);
    }
};

// 196736 - Blackout Combo
class spell_legion_monk_blackout_combo : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_blackout_combo);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_BLACKOUT_STRIKE
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetSpellInfo()->Id != SPELL_MONK_BLACKOUT_STRIKE)
            return false;
        return true;
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_blackout_combo::CheckProc);
    }
};

// 195630 - Elusive Brawler Aura
class spell_legion_monk_elusive_brawler : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_elusive_brawler);

    void DoProc(ProcEventInfo& /*eventInfo*/)
    {
        GetAura()->Remove();
    }

    void Register() override
    {
        OnProc += AuraProcFn(spell_legion_monk_elusive_brawler::DoProc);
    }
};

// 113656 - Fists of Fury
class spell_legion_monk_fists_of_fury : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_fists_of_fury);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_FISTS_OF_FURY_DAMAGE,
            SPELL_MONK_HEAVY_HANDED_STRIKES,
            SPELL_MONK_FISTS_OF_FURY_STUN,
            SPELL_MONK_RISING_SUN_KICK,
            SPELL_MONK_WHIRLING_DRAGON_PUNCH_ENABLE
        });
    }

    void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
    {
        isPeriodic = true;
        amplitude = GetAura()->GetDuration() / 3;
    }

    void HandlePeriodicDamage(AuraEffect const* /*aurEff*/)
    {
        PreventDefaultAction();
        if (Unit* caster = GetCaster())
            if (SpellEffectInfo const* eff4 = GetSpellInfo()->GetEffect(EFFECT_4))
            {
                // Hardcoded calculation FIX ME HACK
                int32 damage = caster->GetTotalAttackPowerValue(BASE_ATTACK) * eff4->BonusCoefficientFromAP;
                caster->CastCustomSpell(SPELL_MONK_FISTS_OF_FURY_DAMAGE, SPELLVALUE_BASE_POINT0, damage, caster, true);
            }
    }

    void HandlePeriodicStun(AuraEffect const* aurEff)
    {
        if (Unit* caster = GetCaster())
            if (caster->HasAura(SPELL_MONK_HEAVY_HANDED_STRIKES))
                if (aurEff->GetTickNumber() == 1)
                    caster->CastSpell(caster, SPELL_MONK_FISTS_OF_FURY_STUN, true);
    }

    void HandleDragonPunch(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Unit* caster = GetCaster())
        {
            if (!caster->GetSpellHistory()->HasCharge(sSpellMgr->AssertSpellInfo(SPELL_MONK_RISING_SUN_KICK)->ChargeCategoryId))
            {
                int32 risingSunKickCD = caster->GetSpellHistory()->GetChargeRecoveryTime(sSpellMgr->AssertSpellInfo(SPELL_MONK_RISING_SUN_KICK)->ChargeCategoryId);
                int32 fistsCD = caster->GetSpellHistory()->GetRemainingCooldown(sSpellMgr->AssertSpellInfo(SPELL_MONK_FISTS_OF_FURY));
                int32 duration = std::min(fistsCD, risingSunKickCD);
                if (Aura* enabler = caster->AddAura(SPELL_MONK_WHIRLING_DRAGON_PUNCH_ENABLE, caster))
                {
                    enabler->SetMaxDuration(duration);
                    enabler->SetDuration(duration);
                }
            }
        }
    }

    void HandleRemoveVisual(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Unit* caster = GetCaster())
            for (AuraApplication* aurApp : caster->GetTargetAuraApplications(SPELL_MONK_FISTS_OF_FURY_VISUAL))
                aurApp->GetBase()->Remove();
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_fists_of_fury::HandlePeriodicStun, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_fists_of_fury::HandlePeriodicDamage, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
        OnEffectApply += AuraEffectApplyFn(spell_legion_monk_fists_of_fury::HandleDragonPunch, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
        DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_legion_monk_fists_of_fury::CalcPeriodic, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
        OnEffectRemove += AuraEffectApplyFn(spell_legion_monk_fists_of_fury::HandleRemoveVisual, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 202162 - Guard
class spell_legion_monk_guard : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_guard);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_STAGGER_DOT,
            SPELL_MONK_STAGGER_HIGH
        });
    }

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        amount = -1;
    }

    void HandleAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
    {
        if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_1))
            if (Unit* caster = GetCaster())
            {
                int32 finalAbsorb = CalculatePct(dmgInfo.GetDamage(), effInfo->CalcValue());
                absorbAmount = finalAbsorb;
                int32 ticks = ((sSpellMgr->AssertSpellInfo(SPELL_MONK_STAGGER_HIGH)->GetDuration() / 1000) * 2);
                int32 bp1 = int32(absorbAmount);
                int32 bp0 = std::max(1, bp1 / ticks);

                if (Aura* dotAura = caster->GetAura(SPELL_MONK_STAGGER_DOT))
                {
                    if (AuraEffect* eff0 = dotAura->GetEffect(EFFECT_0))
                        if (AuraEffect* eff1 = dotAura->GetEffect(EFFECT_1))
                        {
                            // Can only stagger 100% of own HP
                            uint64 maxStaggerAmount = caster->GetMaxHealth();
                            // Calculated Stagger is higher than Max, reduce absorbed Amount
                            if (uint64(absorbAmount + eff1->GetAmount()) >= maxStaggerAmount)
                                absorbAmount = maxStaggerAmount > uint64(eff1->GetAmount()) ? uint32(maxStaggerAmount - uint64(eff1->GetAmount())) : 0;
                            bp1 = int32(absorbAmount) + eff1->GetAmount();
                            bp0 = std::max(1, bp1 / ticks);

                            eff0->SetDamage(bp0);
                            eff0->SetAmount(bp0);
                            eff1->SetDamage(bp1);
                            eff1->SetAmount(bp1);

                            for (uint32 colorSpellId : colorBuffs)
                                if (Aura* colorAura = caster->GetAura(colorSpellId))
                                    colorAura->RefreshDuration();

                            return;
                        }
                }
                else
                {
                    caster->CastCustomSpell(caster, SPELL_MONK_STAGGER_DOT, &bp0, &bp1, 0, true, NULL);
                    return;
                }
            }

        absorbAmount = 0; // No caster, no stagger, absorb nothing
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_guard::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_monk_guard::HandleAbsorb, EFFECT_0);
    }
};

// 115057 - Flying Serpent Kick (Second Cast)
class spell_legion_monk_flying_serpent_kick_second_cast : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_flying_serpent_kick_second_cast);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_FLYING_SERPENT_KICK_DAMAGE,
            SPELL_MONK_FLYING_SERPENT_KICK
        });
    }

    void HandlePeriodic(AuraEffect const* /*aurEff*/)
    {
        if (Unit* caster = GetCaster())
        {
            caster->CastSpell(caster, SPELL_MONK_FLYING_SERPENT_KICK_DAMAGE, true);
            caster->RemoveAura(SPELL_MONK_FLYING_SERPENT_KICK);
        }
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_flying_serpent_kick_second_cast::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
    }
};

// 115203 - Fortifying Brew
class spell_legion_monk_fortifying_brew : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_fortifying_brew);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_FORTIFYING_BREW_TRIGGER
        });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        GetCaster()->CastCustomSpell(SPELL_MONK_FORTIFYING_BREW_TRIGGER, SPELLVALUE_BASE_POINT0, int32(GetCaster()->CountPctFromMaxHealth(GetEffectValue())), GetCaster(), true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_fortifying_brew::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 122280 - Healing Elixir Passive
class spell_legion_monk_healing_elixir : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_healing_elixir);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_HEALING_ELIXIR
        });
    }

    bool CheckProc(ProcEventInfo& /*eventInfo*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return false;

        if (SpellEffectInfo const* spellEffInfo = GetSpellInfo()->GetEffect(EFFECT_0))
            if (caster->HealthBelowPct(spellEffInfo->CalcValue()))
                return true;
        return false;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        PreventDefaultAction();

        Unit* caster = GetCaster();
        if (!caster)
            return;

        if (caster->GetTypeId() == TYPEID_PLAYER && !caster->ToPlayer()->GetSpellHistory()->HasCooldown(SPELL_MONK_HEALING_ELIXIR))
            caster->CastSpell(GetCaster(), SPELL_MONK_HEALING_ELIXIR, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_healing_elixir::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_healing_elixir::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 115308 - Ironskin Brew
class spell_legion_monk_ironskin_brew : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_ironskin_brew);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_IRONSKIN_BREW_BUFF
        });
    }

    void HandleAfterHit()
    {
        GetCaster()->CastSpell(GetCaster(), SPELL_MONK_IRONSKIN_BREW_BUFF, true);
    }

    void Register() override
    {
        AfterHit += SpellHitFn(spell_legion_monk_ironskin_brew::HandleAfterHit);
    }
};

// 116849 - Life Cocoon
class spell_legion_monk_life_cocoon : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_life_cocoon);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_SOOTHING_MIST_PASSIVE,
            SPELL_MONK_SOOTHING_MIST
        });
    }

    void CalcAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        if (Unit* caster = GetCaster())
        {
            amount = int32(31.164f * caster->SpellBaseDamageBonusDone(GetSpellInfo()->GetSchoolMask())); // * (1 + TotalVersatility) * (1 + 0.05 * ArtifactTraitRank(ProtectionOfShaohao)) * (1 + 0.01 * BuffStack(CollidusTheWarpWatchersGaze))
            if (Aura* aurApp = caster->GetAura(SPELL_MONK_PROTECTION_OF_SHAOHAO))
                if (AuraEffect* aurEff = aurApp->GetEffect(EFFECT_0))
                    AddPct(amount, aurEff->GetAmount());
        }
    }

    void HandleAfterHit(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        if (caster->HasAura(SPELL_MONK_MISTS_OF_LIFE))
        {
            caster->AddAura(SPELL_MONK_RENEWING_MIST_HOT, GetTarget());
            caster->AddAura(SPELL_MONK_ENVELOPING_MIST, GetTarget());
        }
        if (caster->HasAura(SPELL_MONK_ANCIENT_MW_ARTS))
            caster->CastSpell(GetTarget(), SPELL_MONK_SOOTHING_MIST_HONOR_TALENT, true);
        else if (caster->HasAura(SPELL_MONK_SOOTHING_MIST_PASSIVE))
            caster->CastSpell(GetTarget(), SPELL_MONK_SOOTHING_MIST, true);
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_life_cocoon::CalcAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
        AfterEffectApply += AuraEffectApplyFn(spell_legion_monk_life_cocoon::HandleAfterHit, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
    }
};

// 197915 - Lifecycles (Talent)
// 197916 - Lifecycles Vivify
// 197919 - Lifecycles Enveloping Mist
class spell_legion_monk_lifecycles : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_lifecycles);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_VIVIFY,
            SPELL_MONK_ENVELOPING_MIST,
            SPELL_MONK_LIFECYCLES_VIVIFY,
            SPELL_MONK_LIFECYCLES_ENVELOPING_MIST
        });
    }

    bool CheckProc(ProcEventInfo& /*eventInfo*/)
    {
        // if (eventInfo.GetSpellInfo()->Id != SPELL_MONK_VIVIFY &&
        //     eventInfo.GetSpellInfo()->Id != SPELL_MONK_ENVELOPING_MIST)
        //     return false;
        // Basically this should never proc because then it drops a charge and unauras itself
        // Proc system again hinders me from fixing this correctly:
        // These spells are handled in Vivify and Enveloping Mist themselves for now
        return false;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        PreventDefaultAction();
        // if (eventInfo.GetSpellInfo()->Id == SPELL_MONK_VIVIFY)
        //     GetTarget()->CastSpell(GetTarget(), SPELL_MONK_LIFECYCLES_ENVELOPING_MIST, true);
        // else if (eventInfo.GetSpellInfo()->Id == SPELL_MONK_ENVELOPING_MIST)
        //     GetTarget()->CastSpell(GetTarget(), SPELL_MONK_LIFECYCLES_VIVIFY, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_lifecycles::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_lifecycles::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 117907 - Mastery: Gust of Mists
class spell_legion_monk_gust_of_mists_mastery : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_gust_of_mists_mastery);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_EFFUSE,
            SPELL_MONK_VIVIFY,
            SPELL_MONK_ENVELOPING_MIST,
            SPELL_MONK_RENEWING_MIST,
            SPELL_MONK_MASTERY_HEAL
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (!eventInfo.GetProcSpell())
            return false;

        switch (eventInfo.GetSpellInfo()->Id)
        {
            case SPELL_MONK_EFFUSE:
            case SPELL_MONK_ENVELOPING_MIST:
            case SPELL_MONK_VIVIFY:
            case SPELL_MONK_RENEWING_MIST:
                // Only proc on initial target
                return eventInfo.GetProcSpell()->m_targets.GetUnitTarget() == eventInfo.GetProcTarget();
            default:
                return false;
        }
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        Unit* owner = GetUnitOwner();
        Unit* target = eventInfo.GetActionTarget();

        // based on retail testing, Gust of Mists' healing is rougly equal to: (spellPower * mastery percent) * versatility
        int32 heal = CalculatePct(owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_NATURE), aurEff->GetAmount());

        // "Effuse, Renewing Mist, Enveloping Mist, and Vivify also cause a gust of healing mists, instantly healing the primary target for $191894s1."
        owner->CastCustomSpell(SPELL_MONK_GUST_OF_MISTS, SPELLVALUE_BASE_POINT0, heal, target, true);

        // Essence Font - "Gust of Mists will heal affected targets twice."
        if (target->HasAura(SPELL_MONK_ESSENCE_FONT_HEAL, owner->GetGUID()))
            owner->CastCustomSpell(SPELL_MONK_GUST_OF_MISTS, SPELLVALUE_BASE_POINT0, heal, target, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_gust_of_mists_mastery::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_gust_of_mists_mastery::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 117959 - Crackling Jade Lightning
class spell_legion_monk_crackling_jade_lightning_aura : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_crackling_jade_lightning_aura);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CRACKLING_JADE_LIGHTNING_KNOCKBACK,
            SPELL_MONK_CRACKLING_JADE_LIGHTNING_KNOCKBACK_CD
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (GetTarget()->HasAura(SPELL_MONK_CRACKLING_JADE_LIGHTNING_KNOCKBACK_CD))
            return false;

        if (eventInfo.GetActor()->HasAura(SPELL_MONK_CRACKLING_JADE_LIGHTNING_CHANNEL, GetTarget()->GetGUID()))
            return false;

        Spell* currentChanneledSpell = GetTarget()->GetCurrentSpell(CURRENT_CHANNELED_SPELL);
        if (!currentChanneledSpell || currentChanneledSpell->GetSpellInfo()->Id != SPELL_MONK_CRACKLING_JADE_LIGHTNING_CHANNEL)
            return false;

        return true;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        GetTarget()->CastSpell(eventInfo.GetActor(), SPELL_MONK_CRACKLING_JADE_LIGHTNING_KNOCKBACK, TRIGGERED_FULL_MASK);
        GetTarget()->CastSpell(GetTarget(), SPELL_MONK_CRACKLING_JADE_LIGHTNING_KNOCKBACK_CD, TRIGGERED_FULL_MASK);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_crackling_jade_lightning_aura::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_crackling_jade_lightning_aura::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 115546 - Provoke
class spell_legion_monk_provoke : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_provoke);

    static uint32 const BlackOxStatusEntry = 61146;

    bool Validate(SpellInfo const* spellInfo) override
    {
        if (!(spellInfo->GetExplicitTargetMask() & TARGET_FLAG_UNIT_MASK)) // ensure GetExplTargetUnit() will return something meaningful during CheckCast
            return false;

        return ValidateSpellInfo(
        {
            SPELL_MONK_PROVOKE_SINGLE_TARGET,
            SPELL_MONK_PROVOKE_AOE
        });
    }

    SpellCastResult CheckExplicitTarget()
    {
        if (GetExplTargetUnit()->GetEntry() != BlackOxStatusEntry)
        {
            SpellInfo const* singleTarget = sSpellMgr->AssertSpellInfo(SPELL_MONK_PROVOKE_SINGLE_TARGET);
            SpellCastResult singleTargetExplicitResult = singleTarget->CheckExplicitTarget(GetCaster(), GetExplTargetUnit());
            if (singleTargetExplicitResult != SPELL_CAST_OK)
                return singleTargetExplicitResult;
        }
        else if (GetExplTargetUnit()->GetOwnerGUID() != GetCaster()->GetGUID())
            return SPELL_FAILED_BAD_TARGETS;

        return SPELL_CAST_OK;
    }

    void HandleDummy(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        if (GetHitUnit()->GetEntry() != BlackOxStatusEntry)
            GetCaster()->CastSpell(GetHitUnit(), SPELL_MONK_PROVOKE_SINGLE_TARGET, true);
        else
            GetCaster()->CastSpell(GetHitUnit(), SPELL_MONK_PROVOKE_AOE, true);
    }

    void Register() override
    {
        OnCheckCast += SpellCheckCastFn(spell_legion_monk_provoke::CheckExplicitTarget);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_provoke::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 119582 - Purifying Brew
class spell_legion_monk_purifying_brew : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_purifying_brew);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_STAGGER_DOT,
            SPELL_MONK_STAGGER_HIGH,
            SPELL_MONK_STAGGER_MODERATE,
            SPELL_MONK_STAGGER_LIGHT
        });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        if (Aura* staggerDot = GetCaster()->GetAura(SPELL_MONK_STAGGER_DOT))
            if (AuraEffect* staggerDotEff1 = staggerDot->GetEffect(EFFECT_1))
                if (AuraEffect* staggerDotEff0 = staggerDot->GetEffect(EFFECT_0))
                {
                    int32 oldAmount = staggerDotEff1->GetAmount();
                    int32 newAmount = CalculatePct(oldAmount, GetEffectValue());
                    staggerDotEff1->ChangeAmount(newAmount);
                    int32 ticks = (sSpellMgr->AssertSpellInfo(SPELL_MONK_STAGGER_HIGH)->GetDuration() / 1000) * 2;
                    int32 periodic = newAmount / ticks;
                    staggerDotEff0->ChangeAmount(periodic);
                    staggerDotEff0->SetDamage(periodic);

                    for (uint32 colorSpellId : colorBuffs)
                        if (Aura* colorAura = GetCaster()->GetAura(colorSpellId))
                            colorAura->RefreshDuration();

                    if (AuraEffect* aurEffHotTrub = GetCaster()->GetAuraEffect(SPELL_MONK_HOT_TRUB, EFFECT_0))
                    {
                        int32 damage = CalculatePct(oldAmount, aurEffHotTrub->GetAmount());
                        GetCaster()->CastCustomSpell(SPELL_MONK_HOT_TRUB_DAMAGE, SPELLVALUE_BASE_POINT0, damage, GetCaster(), TRIGGERED_FULL_MASK);
                    }
                    if (AuraEffect* aurEffElusive = GetCaster()->GetAuraEffect(SPELL_MONK_ELUSIVE_DANCE, EFFECT_0))
                    {
                        // Minimum is in basepoints (5% 7.2)
                        int32 value = aurEffElusive->GetAmount();
                        if (oldAmount > int32(GetCaster()->CountPctFromMaxHealth(60)))
                            value *= 4;
                        else if (oldAmount > int32(GetCaster()->CountPctFromMaxHealth(30)))
                            value *= 2;
                        CustomSpellValues values;
                        values.AddSpellMod(SPELLVALUE_BASE_POINT0, value);
                        values.AddSpellMod(SPELLVALUE_BASE_POINT1, value);
                        GetCaster()->CastCustomSpell(SPELL_MONK_ELUSIVE_DANCE_BUFF, values, GetCaster(), TRIGGERED_FULL_MASK);
                    }
                }

        if (Aura* blackoutAura = GetCaster()->GetAura(SPELL_MONK_BLACKOUT_COMBO_AURA))
        {
            GetCaster()->CastSpell(GetCaster(), SPELL_MONK_ELUSIVE_BRAWLER, true);
            blackoutAura->Remove();
        }

        if (GetCaster()->HasAura(SPELL_MONK_NIUZAOS_ESSENCE))
            GetCaster()->CastSpell(GetCaster(), SPELL_MONK_NIUZAOS_ESSENCE_BUFF, true);
    }

    void Register() override
    {
        OnEffectHit += SpellEffectFn(spell_legion_monk_purifying_brew::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 115175 - Soothing Mist
// 209525 - Soothing mist (Ancient MW Arts Honor Talent)
class spell_legion_monk_soothing_mist : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_soothing_mist);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_SOOTHING_MIST_STATUE,
            SPELL_MONK_SHROUD_OF_MIST,
            SPELL_MONK_SHROUD_OF_MIST_AURA
        });
    }

    void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Unit* caster = GetCaster())
        {
            if (AuraEffect* aurEff = caster->GetAuraEffect(SPELL_MONK_SHROUD_OF_MIST_AURA, EFFECT_0))
                caster->CastCustomSpell(SPELL_MONK_SHROUD_OF_MIST, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), caster, true);

            if (Unit* serpentStatue = ObjectAccessor::GetUnit(*caster, caster->m_SummonSlot[1]))
                if (serpentStatue->GetEntry() == NPC_MONK_JADE_SERPENT_STATUE)
                    serpentStatue->CastSpell(GetTarget(), SPELL_MONK_SOOTHING_MIST_STATUE, true, nullptr, nullptr, GetCasterGUID());
        }
    }

    void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Unit* caster = GetCaster())
        {
            caster->RemoveAurasDueToSpell(SPELL_MONK_SHROUD_OF_MIST);

            if (caster->HasAura(SPELL_MONK_ANCIENT_MW_ARTS))
                if (Unit* serpentStatue = ObjectAccessor::GetUnit(*caster, caster->m_SummonSlot[1]))
                    if (serpentStatue->GetEntry() == NPC_MONK_JADE_SERPENT_STATUE)
                        serpentStatue->CastStop();
        }
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_legion_monk_soothing_mist::HandleApply, EFFECT_0, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL);
        OnEffectRemove += AuraEffectApplyFn(spell_legion_monk_soothing_mist::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL);
    }
};

// 193884 - Soothing Mist (Passive)
class spell_legion_monk_soothing_mist_passive : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_soothing_mist_passive);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_EFFUSE,
            SPELL_MONK_VIVIFY,
            SPELL_MONK_ENVELOPING_MIST,
            SPELL_MONK_SOOTHING_MIST,
            SPELL_MONK_LIFE_COCOON,
            SPELL_MONK_SHEILUNS_GIFT
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        switch (eventInfo.GetSpellInfo()->Id)
        {
            case SPELL_MONK_SHEILUNS_GIFT:
                if (eventInfo.GetActor()->HasAura(SPELL_MONK_ANCIENT_MW_ARTS))
                    return false;
                return true;
            case SPELL_MONK_EFFUSE:
                // HACK
                // These spells are handled in their own spell script due to limitation (primary target)
                // of our proc system
                // case SPELL_MONK_ENVELOPING_MIST:
                // case SPELL_MONK_VIVIFY:
                // case SPELL_MONK_LIFE_COCOON:
                return true;
            default:
                return false;
        }
    }

    void HandleProc(AuraEffect const* /*aufEff*/, ProcEventInfo& eventInfo)
    {
        PreventDefaultAction();
        eventInfo.GetActor()->CastSpell(eventInfo.GetProcTarget(), SPELL_MONK_SOOTHING_MIST, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_soothing_mist_passive::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_soothing_mist_passive::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 196730 - Special Delivery Aura
// 214372 - Brew-stache
class spell_legion_monk_special_delivery_aura : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_special_delivery_aura);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_PURIFYING_BREW,
            SPELL_MONK_IRONSKIN_BREW
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetSpellInfo()->Id == SPELL_MONK_PURIFYING_BREW ||
            eventInfo.GetSpellInfo()->Id == SPELL_MONK_IRONSKIN_BREW)
            return true;
        return false;
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_special_delivery_aura::CheckProc);
    }
};

// 196734 - Special Delivery
class spell_legion_monk_special_delivery : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_special_delivery);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_SPECIAL_DELIVERY_KEG
        });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
            GetCaster()->CastSpell(target, SPELL_MONK_SPECIAL_DELIVERY_KEG, true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_special_delivery::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 107270 - Spinning Crane Kick
class spell_legion_monk_spinning_crane_kick : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_spinning_crane_kick);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_MARK_OF_THE_CRANE
        });
    }

    void HandleDamage(SpellEffIndex /*effIndex*/)
    {
        if (AuraEffect* markOfTheCrane = GetHitUnit()->GetAuraEffect(SPELL_MONK_MARK_OF_THE_CRANE, EFFECT_0, GetCaster()->GetGUID()))
            SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), markOfTheCrane->GetAmount()));
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_spinning_crane_kick::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
    }
};

// 210802 - Spirit of the Crane (Passive)
class spell_legion_monk_spirit_of_the_crane_passive : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_spirit_of_the_crane_passive);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_SPIRIT_OF_THE_CRANE_MANA,
            SPELL_MONK_BLACKOUT_KICK_TRIGGERED
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetSpellInfo()->Id != SPELL_MONK_BLACKOUT_KICK_TRIGGERED)
            return false;
        return true;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        GetTarget()->CastSpell(GetTarget(), SPELL_MONK_SPIRIT_OF_THE_CRANE_MANA, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_spirit_of_the_crane_passive::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_spirit_of_the_crane_passive::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 115069 - Stagger Base Aura (Absorb)
class spell_legion_monk_stagger_base : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_stagger_base);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_STAGGER_DOT,
            SPELL_MONK_STAGGER_HIGH,
            SPELL_MONK_STAGGER_MODERATE,
            SPELL_MONK_STAGGER_LIGHT,
            SPELL_MONK_IRONSKIN_BREW,
            SPELL_MONK_FORTIFYING_BREW_STAGGER
        });
    }

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
    {
        amount = -1;
        canBeRecalculated = true;
    }

    void HandleAbsorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        if (SpellInfo const* damageSpellInfo = dmgInfo.GetSpellInfo())
            if (damageSpellInfo->Id == SPELL_MONK_STAGGER_DOT)
                return;

        if (SpellEffectInfo const* eff8 = GetSpellInfo()->GetEffect(EFFECT_8))
        {
            int32 percentageStaggered = eff8->CalcValue();

            // High Tolerance staggers additional 10%
            if (Aura* highToleranceAura = caster->GetAura(SPELL_MONK_HIGH_TOLERANCE))
                if (AuraEffect* highToleranceEff0 = highToleranceAura->GetEffect(EFFECT_0))
                    percentageStaggered += highToleranceEff0->GetAmount();

            // Fortifying Brew staggers additional percent
            if (caster->HasAura(SPELL_MONK_FORTIFYING_BREW_TRIGGER))
            {
                if (SpellEffectInfo const* fortiBrewEff = sSpellMgr->AssertSpellInfo(SPELL_MONK_FORTIFYING_BREW_STAGGER)->GetEffect(EFFECT_0))
                {
                    percentageStaggered += fortiBrewEff->CalcValue();
                    if (AuraEffect* aurEffStaggeringAround = caster->GetAuraEffect(SPELL_MONK_STAGGERING_AROUND, EFFECT_0))
                        percentageStaggered += aurEffStaggeringAround->GetAmount();
                }
            }

            // Ironskin brew additional staggers damage
            if (AuraEffect* ironSkin = caster->GetAuraEffect(SPELL_MONK_IRONSKIN_BREW_BUFF, EFFECT_0))
                percentageStaggered += ironSkin->GetAmount();

            // Effect 2 for physical, effect 3 for magical at half effectiveness
            if (aurEff->GetEffIndex() == EFFECT_2)
                absorbAmount = std::max(1, CalculatePct(int32(dmgInfo.GetDamage()), percentageStaggered));
            else
            {
                // magical effects are %effect_0% effective
                percentageStaggered = CalculatePct(percentageStaggered, GetEffect(EFFECT_0)->GetAmount());
                absorbAmount = std::max(1, CalculatePct(int32(dmgInfo.GetDamage()), percentageStaggered));
            }

            // Duration is 10 seconds and periodic is 500ms resulting in 20 ticks
            int32 ticks = ((sSpellMgr->AssertSpellInfo(SPELL_MONK_STAGGER_HIGH)->GetDuration() / 1000) * 2);
            int32 bp1 = int32(absorbAmount);
            int32 bp0 = std::max(1, bp1 / ticks);

            if (Aura* dotAura = caster->GetAura(SPELL_MONK_STAGGER_DOT))
            {
                if (AuraEffect* eff0 = dotAura->GetEffect(EFFECT_0))
                    if (AuraEffect* eff1 = dotAura->GetEffect(EFFECT_1))
                    {
                        // Can only stagger 100% of own HP
                        uint64 maxStaggerAmount = caster->GetMaxHealth();
                        // Calculated Stagger is higher than Max, reduce absorbed Amount
                        if (uint64(absorbAmount + eff1->GetAmount()) >= maxStaggerAmount)
                            absorbAmount = maxStaggerAmount > uint64(eff1->GetAmount()) ? uint32(maxStaggerAmount - uint64(eff1->GetAmount())) : 0;
                        bp1 = int32(absorbAmount) + eff1->GetAmount();
                        bp0 = std::max(1, bp1 / ticks);

                        eff0->SetDamage(bp0);
                        eff0->SetAmount(bp0);
                        eff1->SetDamage(bp1);
                        eff1->SetAmount(bp1);

                        for (uint32 colorSpellId : colorBuffs)
                            if (Aura* colorAura = caster->GetAura(colorSpellId))
                                colorAura->RefreshDuration();
                    }
            }
            else
            {
                caster->CastCustomSpell(caster, SPELL_MONK_STAGGER_DOT, &bp0, &bp1, 0, true, NULL, aurEff);
            }
        }
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_stagger_base::CalculateAmount, EFFECT_2, SPELL_AURA_SCHOOL_ABSORB);
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_stagger_base::CalculateAmount, EFFECT_3, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_monk_stagger_base::HandleAbsorb, EFFECT_2);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_monk_stagger_base::HandleAbsorb, EFFECT_3);;
    }
};

// 124255 - Stagger DoT Aura
class spell_legion_monk_stagger_dot : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_stagger_dot);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_STAGGER_HIGH,
            SPELL_MONK_STAGGER_MODERATE,
            SPELL_MONK_STAGGER_LIGHT,
        });
    }

    void HandlePeriodic(AuraEffect const* aurEff)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        if (AuraEffect* eff1 = GetAura()->GetEffect(EFFECT_1))
        {
            if (eff1->GetAmount() >= aurEff->GetAmount() && eff1->GetAmount() > 0)
            {
                int32 periodicTickDamage = aurEff->GetAmount();
                uint64 staggerDamage = uint64(eff1->GetAmount());

                eff1->ChangeAmount(staggerDamage - periodicTickDamage);
                eff1->SetDamage(staggerDamage - periodicTickDamage);

                periodicTickDamage = aurEff->GetAmount();
                staggerDamage = uint64(eff1->GetAmount());
                int32 staggerDamageValue = eff1->GetAmount();

                uint32 staggerSpellIdColor = SPELL_MONK_STAGGER_LIGHT;
                // Blizz Blue Post about periodic debuff color: Red means >6%, Yellow means >3%, Green means >0%.
                if (staggerDamage > caster->CountPctFromMaxHealth(60))
                    staggerSpellIdColor = SPELL_MONK_STAGGER_HIGH;
                else if (staggerDamage > caster->CountPctFromMaxHealth(30))
                    staggerSpellIdColor = SPELL_MONK_STAGGER_MODERATE;

                // Honor Trait: Based on stagger level, get reduction/move speed
                if (SpellEffectInfo const* aurEffEarieMoveSpeed = sSpellMgr->AssertSpellInfo(SPELL_MONK_EARIE_FERMINATION)->GetEffect(EFFECT_4))
                    if (SpellEffectInfo const* aurEffEarieMagicReduce = sSpellMgr->AssertSpellInfo(SPELL_MONK_EARIE_FERMINATION)->GetEffect(EFFECT_5))
                    {
                        CustomSpellValues values;
                        int32 moveSpeed = aurEffEarieMoveSpeed->CalcValue() / 3;
                        int32 magicReduction = aurEffEarieMagicReduce->CalcValue() / 3;

                        switch (staggerSpellIdColor)
                        {
                            case SPELL_MONK_STAGGER_LIGHT:
                                break;
                            case SPELL_MONK_STAGGER_MODERATE:
                                moveSpeed *= 2;
                                magicReduction *= 2;
                                break;
                            case SPELL_MONK_STAGGER_HIGH:
                                moveSpeed *= 3;
                                magicReduction *= 3;
                            default:
                                break;
                        }
                        values.AddSpellMod(SPELLVALUE_BASE_POINT0, moveSpeed);
                        values.AddSpellMod(SPELLVALUE_BASE_POINT1, magicReduction);
                        caster->CastCustomSpell(SPELL_MONK_EARIE_FERMINATION_BUFF, values, caster, TRIGGERED_FULL_MASK);
                    }

                for (uint32 colorSpellId : colorBuffs)
                    if (Aura* colorAura = caster->GetAura(colorSpellId))
                        if (AuraEffect* aurEffColor0 = colorAura->GetEffect(EFFECT_0))
                            if (AuraEffect* aurEffColor1 = colorAura->GetEffect(EFFECT_1))
                            {
                                if (colorSpellId == staggerSpellIdColor)
                                {
                                    aurEffColor0->ChangeAmount(periodicTickDamage);
                                    aurEffColor1->ChangeAmount(int32(staggerDamage));
                                }
                                else
                                {
                                    int32 duration = colorAura->GetDuration();
                                    colorAura->Remove();
                                    caster->CastCustomSpell(caster, staggerSpellIdColor, &periodicTickDamage, &staggerDamageValue, 0, true, NULL, aurEff);
                                    if (Aura* newColorAura = caster->GetAura(staggerSpellIdColor))
                                        newColorAura->SetDuration(duration);
                                }
                                return;
                            }

                caster->CastCustomSpell(caster, staggerSpellIdColor, &periodicTickDamage, &staggerDamageValue, 0, true, NULL, aurEff);
            }
            else
            {
                GetAura()->Remove();
            }
        }
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_stagger_dot::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
    }
};

// 116645 - Teachings of the monastery (Passive)
class spell_legion_monk_teachings_of_the_monastery_passive : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_teachings_of_the_monastery_passive);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TEACHINGS_OF_THE_MONASTERY,
            SPELL_MONK_TIGER_PALM,
            SPELL_MONK_BLACKOUT_KICK,
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetSpellInfo()->Id != SPELL_MONK_TIGER_PALM &&
            eventInfo.GetSpellInfo()->Id != SPELL_MONK_BLACKOUT_KICK &&
            eventInfo.GetSpellInfo()->Id != SPELL_MONK_BLACKOUT_KICK_TRIGGERED)
            return false;

        return true;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetSpellInfo()->Id == SPELL_MONK_TIGER_PALM)
            GetTarget()->CastSpell(GetTarget(), SPELL_MONK_TEACHINGS_OF_THE_MONASTERY, true);
        else if (roll_chance_i(aurEff->GetAmount()))
            GetTarget()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_MONK_RISING_SUN_KICK)->ChargeCategoryId);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_teachings_of_the_monastery_passive::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_teachings_of_the_monastery_passive::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 202090 - Teachings of the monastery (Buff)
class spell_legion_monk_teachings_of_the_monastery_buff : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_teachings_of_the_monastery_buff);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TEACHINGS_OF_THE_MONASTERY_PASSIVE,
            SPELL_MONK_BLACKOUT_KICK_TRIGGERED,
            SPELL_MONK_BLACKOUT_KICK
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (!GetTarget()->HasAura(SPELL_MONK_TEACHINGS_OF_THE_MONASTERY_PASSIVE))
            return false;

        if (eventInfo.GetSpellInfo()->Id != SPELL_MONK_BLACKOUT_KICK)
            return false;

        return true;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        if (Aura* monasteryBuff = GetAura())
        {
            for (uint8 i = 0; i < monasteryBuff->GetStackAmount(); ++i)
                GetTarget()->CastSpell(eventInfo.GetProcTarget(), SPELL_MONK_BLACKOUT_KICK_TRIGGERED);
            monasteryBuff->Remove();
        }
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_teachings_of_the_monastery_buff::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_teachings_of_the_monastery_buff::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 115080 - Touch of Death
class spell_legion_monk_touch_of_death : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_touch_of_death);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TOUCH_OF_DEATH_DAMAGE,
            SPELL_MONK_GALE_BURST_TARGET_AURA
        });
    }

    void OnPeriodic(AuraEffect const* aurEff)
    {
        if (Unit* caster = GetCaster())
        {
            int32 damage = aurEff->GetAmount();

            if (AuraEffect* galeBurst = GetTarget()->GetAuraEffect(SPELL_MONK_GALE_BURST_TARGET_AURA, EFFECT_0, GetCasterGUID()))
                damage += galeBurst->GetAmount();

            caster->CastCustomSpell(SPELL_MONK_TOUCH_OF_DEATH_DAMAGE, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), true);
        }
    }

    void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
    {
        canBeRecalculated = true;
        if (Unit* caster = GetCaster())
            if (SpellEffectInfo const* effInfo = GetAura()->GetSpellEffectInfo(EFFECT_1))
            {
                amount = int32(caster->CountPctFromMaxHealth(effInfo->CalcValue()));
                if (Unit* target = GetTarget())
                    if (target->GetTypeId() == TYPEID_PLAYER)
                        amount /= 2;
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
            }
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH)
            if (Unit* caster = GetCaster())
                if (AuraEffect* aurEff = caster->GetAuraEffect(SPELL_MONK_DEATH_ART, EFFECT_0))
                    caster->GetSpellHistory()->ModifyCooldown(SPELL_MONK_TOUCH_OF_DEATH, -int32(CalculatePct(caster->GetSpellHistory()->GetRemainingCooldown(GetSpellInfo()), aurEff->GetAmount())));
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_touch_of_death::CalculateAmount, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_touch_of_death::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
        OnEffectRemove += AuraEffectRemoveFn(spell_legion_monk_touch_of_death::OnRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 191837 - Essence Font
class spell_legion_monk_essence_font : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_essence_font);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_LIGHT_ON_YOUR_FEET,
            SPELL_MONK_LIGHT_ON_YOUR_FEET_BUFF
        });
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_EXPIRE)
            if (AuraEffect* aurEff = GetTarget()->GetAuraEffect(SPELL_MONK_LIGHT_ON_YOUR_FEET, EFFECT_0))
                GetTarget()->CastCustomSpell(SPELL_MONK_LIGHT_ON_YOUR_FEET_BUFF, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), true);
    }

    void Register() override
    {
        OnEffectRemove += AuraEffectRemoveFn(spell_legion_monk_essence_font::OnRemove, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL, AURA_EFFECT_HANDLE_REAL);
    }
};

// 122470 - Touch of Karma
class spell_legion_monk_touch_of_karma : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_touch_of_karma);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TOUCH_OF_KARMA_BUFF,
            SPELL_MONK_TOUCH_OF_KARMA_DAMAGE
        });
    }

    void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
    {
        canBeRecalculated = true;
        if (Unit* caster = GetCaster())
            if (SpellEffectInfo const* effInfo = GetAura()->GetSpellEffectInfo(EFFECT_2))
            {
                amount = int32(caster->CountPctFromMaxHealth(effInfo->CalcValue()));
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
            }
    }

    void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        GetCaster()->CastSpell(GetCaster(), SPELL_MONK_TOUCH_OF_KARMA_BUFF, true);
    }

    void HandleAbsorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& /*absorbAmount*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        for (AuraApplication* aurApp : caster->GetTargetAuraApplications(SPELL_MONK_TOUCH_OF_KARMA))
            if (aurApp->GetTarget() != caster)
            {
                int32 periodicDamage = int32(dmgInfo.GetDamage() / sSpellMgr->AssertSpellInfo(SPELL_MONK_TOUCH_OF_KARMA_DAMAGE)->GetMaxTicks(DIFFICULTY_NONE));
                periodicDamage += int32(aurApp->GetTarget()->GetRemainingPeriodicAmount(GetCasterGUID(), SPELL_MONK_TOUCH_OF_KARMA_DAMAGE, SPELL_AURA_PERIODIC_DAMAGE));
                caster->CastCustomSpell(SPELL_MONK_TOUCH_OF_KARMA_DAMAGE, SPELLVALUE_BASE_POINT0, periodicDamage, aurApp->GetTarget(), true, NULL, aurEff);
            }

        if (caster->HasAura(SPELL_MONK_GOOD_KARMA))
            caster->CastCustomSpell(SPELL_MONK_GOOD_KARMA_HEAL, SPELLVALUE_BASE_POINT0, dmgInfo.GetDamage(), GetCaster(), true);
    }

    void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        caster->RemoveAura(SPELL_MONK_TOUCH_OF_KARMA_BUFF);
        for (AuraApplication* aurApp : caster->GetTargetAuraApplications(SPELL_MONK_TOUCH_OF_KARMA))
            if (Aura* targetAura = aurApp->GetBase())
                targetAura->Remove();
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_legion_monk_touch_of_karma::HandleApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_monk_touch_of_karma::CalculateAmount, EFFECT_1, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_monk_touch_of_karma::HandleAbsorb, EFFECT_1);
        OnEffectRemove += AuraEffectRemoveFn(spell_legion_monk_touch_of_karma::HandleRemove, EFFECT_1, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
        OnEffectRemove += AuraEffectRemoveFn(spell_legion_monk_touch_of_karma::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 125174 - Touch of Karma Buff
class spell_legion_monk_touch_of_karma_buff : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_touch_of_karma_buff);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TOUCH_OF_KARMA
        });
    }

    void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        for (AuraApplication* aurApp : caster->GetTargetAuraApplications(SPELL_MONK_TOUCH_OF_KARMA))
            if (Aura* targetAura = aurApp->GetBase())
                targetAura->Remove();
    }

    void Register() override
    {
        OnEffectRemove += AuraEffectRemoveFn(spell_legion_monk_touch_of_karma_buff::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 101643 - Transcendence
class spell_legion_monk_transcendence : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_transcendence);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TRANSCENDENCE_CLONE,
            SPELL_MONK_TRANSCENDENCE_VISUAL
        });
    }

    void RemoveSpirits()
    {
        if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_0))
            GetCaster()->RemoveAllMinionsByEntry(uint32(effInfo->MiscValue));
    }

    void HandleDummy()
    {
        for (Unit* summon : GetCaster()->m_Controlled)
            if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_0))
                if (summon->GetEntry() == uint32(effInfo->MiscValue))
                {
                    GetCaster()->CastSpell(summon, SPELL_MONK_TRANSCENDENCE_CLONE, true);
                    summon->CastSpell(summon, SPELL_MONK_TRANSCENDENCE_VISUAL, true);
                    summon->SetAIAnimKitId(2223); // Sniff Data
                    summon->SetDisableGravity(true);
                    summon->SetControlled(true, UNIT_STATE_ROOT);
                    return;
                }
    }

    void Register() override
    {
        BeforeCast += SpellCastFn(spell_legion_monk_transcendence::RemoveSpirits);
        AfterHit += SpellHitFn(spell_legion_monk_transcendence::HandleDummy);
    }
};

class spell_legion_monk_transcendence_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_transcendence_AuraScript);

    void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes mode)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        if (mode != AURA_EFFECT_HANDLE_REAPPLY)
            if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_0))
                caster->RemoveAllMinionsByEntry(uint32(effInfo->MiscValue));
    }

    void Register() override
    {
        OnEffectRemove += AuraEffectRemoveFn(spell_legion_monk_transcendence_AuraScript::HandleRemove, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 119996 - Transcendence: Transfer
class spell_legion_monk_transcendence_transfer : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_transcendence_transfer);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_TRANSCENDENCE,
            SPELL_MONK_HEALING_WINDS_HEAL,
            SPELL_MONK_HEALING_WINDS
        });
    }

    SpellCastResult CheckCast()
    {
        for (Unit* summon : GetCaster()->m_Controlled)
            if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_MONK_TRANSCENDENCE)->GetEffect(EFFECT_0))
                if (summon->GetEntry() == uint32(effInfo->MiscValue))
                {
                    if (!GetCaster()->IsWithinDist(summon, GetSpellInfo()->GetMaxRange()))
                        return SPELL_FAILED_OUT_OF_RANGE;
                    else
                        return SPELL_CAST_OK;
                }

        SetCustomCastResultMessage(SPELL_CUSTOM_ERROR_YOU_HAVE_NO_SPIRIT_ACTIVE);
        return SPELL_FAILED_CUSTOM_ERROR;
    }

    void HandleDummy(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        for (Unit* summon : GetCaster()->m_Controlled)
            if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_MONK_TRANSCENDENCE)->GetEffect(EFFECT_0))
                if (summon->GetEntry() == uint32(effInfo->MiscValue))
                {
                    Position const casterPosition = GetCaster()->GetPosition();
                    GetCaster()->NearTeleportTo(summon->GetPositionX(), summon->GetPositionY(), summon->GetPositionZ(), summon->GetOrientation());
                    summon->NearTeleportTo(casterPosition.m_positionX, casterPosition.m_positionY, casterPosition.m_positionZ, casterPosition.GetOrientation());

                    if (Aura* healingWindsAura = GetCaster()->GetAura(SPELL_MONK_HEALING_WINDS))
                        if (AuraEffect* healingWindsEff = healingWindsAura->GetEffect(EFFECT_0))
                        {
                            int32 heal = int32(GetCaster()->CountPctFromMaxHealth(healingWindsEff->GetAmount()) / 3);
                            GetCaster()->CastCustomSpell(SPELL_MONK_HEALING_WINDS_HEAL, SPELLVALUE_BASE_POINT0, heal, GetCaster(), true);
                        }
                }
    }

    void Register() override
    {
        OnCheckCast += SpellCheckCastFn(spell_legion_monk_transcendence_transfer::CheckCast);
        OnEffectHit += SpellEffectFn(spell_legion_monk_transcendence_transfer::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 115151 - Renewing Mist
class spell_legion_monk_renewing_mist : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_renewing_mist);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_RENEWING_MIST_HOT
        });
    }

    void HandleDummy(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        if (Unit* target = GetExplTargetUnit())
            GetCaster()->CastSpell(target, SPELL_MONK_RENEWING_MIST_HOT, true);
    }

    void Register() override
    {
        OnEffectLaunch += SpellEffectFn(spell_legion_monk_renewing_mist::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 119611 - Renewing Mist (HoT)
class spell_legion_monk_renewing_mist_hot : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_renewing_mist_hot);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_RENEWING_MIST_JUMP,
            SPELL_MONK_UPLIFTING_TRANCE,
            SPELL_MONK_RENEWING_MIST
        });
    }

    void HandlePeriodicHeal(AuraEffect const* /*aurEff*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        if (GetTarget()->IsFullHealth())
            caster->CastSpell(GetTarget(), SPELL_MONK_RENEWING_MIST_JUMP, true);

        SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(SPELL_MONK_RENEWING_MIST);
        if (SpellEffectInfo const* effInfo = spellInfo->GetEffect(EFFECT_1))
            if (roll_chance_i(effInfo->CalcValue()))
                caster->CastSpell(caster, SPELL_MONK_UPLIFTING_TRANCE, true);
    }

    void CalcAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        Unit* caster = GetCaster();
        if (Aura* counteractAura = caster->GetAura(SPELL_MONK_COUNTERACT_MAGIC))
        {
            Unit::AuraApplicationMap& appliedAuras = GetUnitOwner()->GetAppliedAuras();
            for (Unit::AuraApplicationMap::iterator iter = appliedAuras.begin(); iter != appliedAuras.end(); ++iter)
            {
                Aura* baseAura = iter->second->GetBase();

                if (baseAura->GetSpellInfo()->IsPositive())
                    continue;

                if (!(baseAura->GetSpellInfo()->GetSchoolMask() & SPELL_SCHOOL_MASK_MAGIC))
                    continue;

                if (!(baseAura->GetSpellInfo()->GetDispelMask() & 1 << DISPEL_MAGIC))
                    continue;

                if (baseAura->HasEffectType(SPELL_AURA_PERIODIC_DAMAGE) ||
                    baseAura->HasEffectType(SPELL_AURA_PERIODIC_DAMAGE_PERCENT))
                {
                    if (AuraEffect const* effInfo = counteractAura->GetEffect(EFFECT_0))
                    {
                        // Idk why this all is not increasing the amount
                        AddPct(amount, effInfo->GetAmount());
                        return;
                    }
                }
            }
        }
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_renewing_mist_hot::HandlePeriodicHeal, EFFECT_0, SPELL_AURA_PERIODIC_HEAL);
        //DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_monk_renewing_mist_hot::CalcAmount, EFFECT_0, SPELL_AURA_PERIODIC_HEAL);
    }
};

// 119607 - Renewing Mist Jump
class spell_legion_monk_renewing_mist_jump : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_renewing_mist_jump);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_RENEWING_MIST_HOT,
            SPELL_MONK_DANCING_MISTS
        });
    }

    void HandleTargets(std::list<WorldObject*>& targets)
    {
        Unit* caster = GetCaster();
        Unit* previousTarget = GetExplTargetUnit();

        // Not remove full health targets now, dancing mists talent can jump on full health too
        targets.remove_if([caster, previousTarget, this](WorldObject* a)
        {
            Unit* ally = a->ToUnit();
            if (!ally || ally->HasAura(SPELL_MONK_RENEWING_MIST_HOT, caster->GetGUID()) || ally == previousTarget)
                return true;

            return false;
        });

        if (caster->HasAura(SPELL_MONK_DANCING_MISTS) && targets.size() > 1)
        {
            std::list<WorldObject*>::iterator it = targets.begin();
            std::advance(it, 1);
            _additionalTargetGuid = (*it)->GetGUID();
        }

        targets.remove_if([this](WorldObject* a)
        {
            Unit* ally = a->ToUnit();
            if (!ally || ally->IsFullHealth())
                return true;

            return false;
        });

        if (targets.size() > 1)
        {
            targets.sort(Trinity::HealthPctOrderPred());
            targets.resize(1);
        }

        _previousTargetGuid = previousTarget->GetGUID();
    }

    void HandleHit(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        Unit* caster = GetCaster();
        Unit* previousTarget = ObjectAccessor::GetUnit(*caster, _previousTargetGuid);
        Unit* additionalTarget = ObjectAccessor::GetUnit(*caster, _additionalTargetGuid);

        if (previousTarget)
        {
            if (Aura* oldAura = previousTarget->GetAura(SPELL_MONK_RENEWING_MIST_HOT, GetCaster()->GetGUID()))
            {
                if (Aura* newAura = caster->AddAura(SPELL_MONK_RENEWING_MIST_HOT, GetHitUnit()))
                {
                    newAura->SetDuration(oldAura->GetDuration());
                    previousTarget->SendPlaySpellVisual(GetHitUnit()->GetGUID(), SPELL_MONK_VISUAL_RENEWING_MIST, 0, 0, 50.f);
                    oldAura->Remove();
                }
                if (AuraEffect* aurEffDancingMists = caster->GetAuraEffect(SPELL_MONK_DANCING_MISTS, EFFECT_0))
                    if (additionalTarget && roll_chance_i(aurEffDancingMists->GetAmount()))
                        if (Aura* newAura = caster->AddAura(SPELL_MONK_RENEWING_MIST_HOT, additionalTarget))
                            newAura->SetDuration(oldAura->GetDuration());
            }
        }
    }

private:
    ObjectGuid _previousTargetGuid;
    ObjectGuid _additionalTargetGuid;

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_renewing_mist_jump::HandleTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_renewing_mist_jump::HandleHit, EFFECT_1, SPELL_EFFECT_DUMMY);
    }
};

// 210804 - Rising Thunder
class spell_legion_monk_rising_thunder : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_rising_thunder);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo
        ({
            SPELL_MONK_THUNDER_FOCUS_TEA
        });
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_MONK_THUNDER_FOCUS_TEA, true);
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_rising_thunder::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 115008 - Chi Torpedo
class spell_legion_monk_chi_torpedo : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_chi_torpedo);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_YU_LONS_GIFT_AURA,
            SPELL_MONK_YU_LONGS_GIFT
        });
    }

    void HandleEffect(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();

        if (caster->HasAura(SPELL_MONK_YU_LONS_GIFT_AURA))
            caster->CastSpell(caster, SPELL_MONK_YU_LONGS_GIFT, true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_torpedo::HandleEffect, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
    }
};

// 109132 - Roll
class spell_legion_monk_roll : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_roll);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_ROLL_TRIGGER,
            SPELL_MONK_ROLL_FEATHER_FALL
        });
    }

    SpellCastResult CheckCast()
    {
        if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
            return SPELL_FAILED_ROOTED;
        return SPELL_CAST_OK;
    }

    void HandleDummy(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        Unit* caster = GetCaster();

        caster->CastSpell(caster, SPELL_MONK_ROLL_FEATHER_FALL, true);
        caster->CastSpell(caster, SPELL_MONK_ROLL_TRIGGER, true);

        if (caster->HasAura(SPELL_MONK_YU_LONS_GIFT_AURA))
            caster->CastSpell(caster, SPELL_MONK_YU_LONGS_GIFT, true);
    }

    void Register() override
    {
        OnCheckCast += SpellCheckCastFn(spell_legion_monk_roll::CheckCast);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_roll::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 148187 - Rushing Jade Wind
class spell_legion_monk_rushing_jade_wind_damage : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_rushing_jade_wind_damage);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo
        ({
            SPELL_MONK_RUSHING_JADE_WIND_PERIODIC,
            SPELL_MONK_CYCLONE_STRIKES,
            SPELL_MONK_MARK_OF_THE_CRANE
        });
    }

    bool Load() override
    {
        return GetCaster()->HasAura(SPELL_MONK_CYCLONE_STRIKES);
    }

    void FilterTargets(std::list<WorldObject*>& targets)
    {
        savedTargets = targets;
        if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_MONK_RUSHING_JADE_WIND_PERIODIC)->GetEffect(EFFECT_1))
            Trinity::Containers::RandomResize(targets, effInfo->CalcValue(GetCaster()));
    }

    void HandleEffect(SpellEffIndex /*effIndex*/)
    {
        if (AuraEffect* windPeriodic = GetCaster()->GetAuraEffect(SPELL_MONK_RUSHING_JADE_WIND_PERIODIC, EFFECT_0))
        {
            if (windPeriodic->GetTickNumber() == 1)
            {
                std::list<WorldObject*>::iterator hitUnit = std::find(savedTargets.begin(), savedTargets.end(), GetHitUnit());
                if (hitUnit != savedTargets.end())
                    GetCaster()->CastSpell((*hitUnit)->ToUnit(), SPELL_MONK_MARK_OF_THE_CRANE, true);
            }
        }
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_rushing_jade_wind_damage::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_rushing_jade_wind_damage::HandleEffect, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
    }

private:
    std::list<WorldObject*> savedTargets;
};

// 199640 - Celestial Breath
class spell_legion_monk_celestial_breath : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_celestial_breath);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CELESTIAL_BREATH_DEBUFF
        });
    }

    bool CheckProc(ProcEventInfo& /*eventInfo*/)
    {
        if (GetTarget()->HasAura(SPELL_MONK_CELESTIAL_BREATH_DEBUFF))
            return false;
        return true;
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_celestial_breath::CheckProc);
    }
};

// 185099 - Rising Sun Kick
class spell_legion_monk_rising_sun_kick : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_rising_sun_kick);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_FISTS_OF_FURY,
            SPELL_MONK_WHIRLING_DRAGON_PUNCH,
            SPELL_MONK_WHIRLING_DRAGON_PUNCH_ENABLE,
            SPELL_MONK_MORTAL_WOUNDS
        });
    }

    void HandleAfterCast()
    {
        if (Unit* caster = GetCaster())
        {
            if (caster->HasSpell(SPELL_MONK_WHIRLING_DRAGON_PUNCH))
            {
                int32 risingSunKickCD = caster->GetSpellHistory()->GetChargeRecoveryTime(sSpellMgr->AssertSpellInfo(SPELL_MONK_RISING_SUN_KICK)->ChargeCategoryId);
                int32 fistsCD = caster->GetSpellHistory()->GetRemainingCooldown(sSpellMgr->AssertSpellInfo(SPELL_MONK_FISTS_OF_FURY));
                int32 duration = std::min(fistsCD, risingSunKickCD);
                if (Aura* enabler = caster->AddAura(SPELL_MONK_WHIRLING_DRAGON_PUNCH_ENABLE, caster))
                {
                    enabler->SetMaxDuration(duration);
                    enabler->SetDuration(duration);
                }
            }
        }
    }

    void HandleHit(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
            GetCaster()->CastSpell(target, SPELL_MONK_MORTAL_WOUNDS, true);

        if (GetSpell()->HasCustomCastFlag(CUSTOM_CAST_FLAG_SPECIAL_CALCULATION))
            if (AuraEffect* tornadoKicks = GetCaster()->GetAuraEffect(SPELL_MONK_TORNADO_KICKS, EFFECT_0))
                SetHitDamage(CalculatePct(GetHitDamage(), tornadoKicks->GetAmount()));
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_rising_sun_kick::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
        AfterCast += SpellCastFn(spell_legion_monk_rising_sun_kick::HandleAfterCast);
    }
};

// 100780 - Tiger Palm
class spell_legion_monk_tiger_palm : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_tiger_palm);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_POWER_STRIKES_BUFF,
            SPELL_MONK_POWER_STRIKES_ENERGIZE,
            SPELL_MONK_COMBO_BREAKER,
            SPELL_MONK_BLACKOUT_KICK_PROC,
            SPELL_MONK_POWER_STRIKES_TALENT,
            SPELL_MONK_PURIFYING_BREW,
            SPELL_MONK_IRONSKIN_BREW,
            SPELL_MONK_FORTIFYING_BREW,
            SPELL_MONK_IRONSKIN_BREW,
            SPELL_MONK_BLACK_OX_BREW,
            SPELL_MONK_FACE_PALM_EFFECT,
            SPELL_MONK_FACE_PALM
        });
    }

    void HandleDummy(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        Unit* caster = GetCaster();

        // Power Strikes Talents
        if (Aura* powerStrikeBuff = caster->GetAura(SPELL_MONK_POWER_STRIKES_BUFF))
        {
            caster->CastSpell(caster, SPELL_MONK_POWER_STRIKES_ENERGIZE, true);
            powerStrikeBuff->Remove();
            if (Aura* powerStrikePeriodicAura = caster->GetAura(SPELL_MONK_POWER_STRIKES_TALENT))
                if (AuraEffect* eff0 = powerStrikePeriodicAura->GetEffect(EFFECT_0))
                    eff0->ResetPeriodic(true);
        }

        if (Aura* comboBreaker = caster->GetAura(SPELL_MONK_COMBO_BREAKER))
            if (AuraEffect* aurEff = comboBreaker->GetEffect(EFFECT_0))
                if (roll_chance_i(aurEff->GetAmount()))
                    caster->CastSpell(caster, SPELL_MONK_BLACKOUT_KICK_PROC, true);
    }

    void HandleBrewCD(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();
        if (caster->HasAura(AURA_BREWMASTER_MONK))
        {
            int32 cdReduction = GetEffectValue() * IN_MILLISECONDS;

            if (AuraEffect* auraFacePalm = caster->GetAuraEffect(SPELL_MONK_FACE_PALM, EFFECT_0))
                if (roll_chance_i(auraFacePalm->GetAmount()))
                {
                    if (SpellEffectInfo const* spellEff = sSpellMgr->AssertSpellInfo(SPELL_MONK_FACE_PALM_EFFECT)->GetEffect(EFFECT_1))
                        cdReduction += spellEff->CalcValue() * IN_MILLISECONDS;
                    if (SpellEffectInfo const* spellEff = sSpellMgr->AssertSpellInfo(SPELL_MONK_FACE_PALM_EFFECT)->GetEffect(EFFECT_0))
                        SetHitDamage(CalculatePct(GetHitDamage(), spellEff->CalcValue()));
                }

            caster->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_MONK_PURIFYING_BREW)->ChargeCategoryId, cdReduction);
            caster->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_MONK_IRONSKIN_BREW)->ChargeCategoryId, cdReduction);
            caster->GetSpellHistory()->ModifyCooldown(SPELL_MONK_FORTIFYING_BREW, -cdReduction);
            caster->GetSpellHistory()->ModifyCooldown(SPELL_MONK_IRONSKIN_BREW, -cdReduction);
            caster->GetSpellHistory()->ModifyCooldown(SPELL_MONK_BLACK_OX_BREW, -cdReduction);
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_tiger_palm::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_tiger_palm::HandleBrewCD, EFFECT_2, SPELL_EFFECT_DUMMY);
    }
};

// 116670 - Vivify
class spell_legion_monk_vivify : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_vivify);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_SOOTHING_MIST_PASSIVE,
            SPELL_MONK_SOOTHING_MIST,
            SPELL_MONK_LIFECYCLES,
            SPELL_MONK_LIFECYCLES_ENVELOPING_MIST,
            SPELL_MONK_MASTERY_HEAL
        });
    }

    void HandleTargetSelect(std::list<WorldObject*>& targets)
    {
        if (GetCaster()->HasAura(SPELL_MONK_LUNAR_GLIDE) && GetCaster()->HasAura(SPELL_MONK_UPLIFTING_TRANCE))
            return;
        else if (targets.size() > 3)
            targets.resize(3);
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();
        Unit* explTarget = GetExplTargetUnit();

        if (caster->HasAura(SPELL_MONK_LIFECYCLES))
            caster->CastSpell(caster, SPELL_MONK_LIFECYCLES_ENVELOPING_MIST, true);

        if (caster->HasAura(SPELL_MONK_SOOTHING_MIST_PASSIVE) && !caster->HasAura(SPELL_MONK_ANCIENT_MW_ARTS))
            caster->CastSpell(explTarget, SPELL_MONK_SOOTHING_MIST, true);

        if (caster->HasAura(SPELL_MONK_REFRESHING_BREEZE))
            if (Unit* target = GetHitUnit())
                if (Aura* essenceFont = target->GetAura(SPELL_MONK_ESSENCE_FONT_HEAL, caster->GetGUID()))
                    essenceFont->RefreshDuration();
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_vivify::HandleTargetSelect, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_vivify::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 216113 - Way of the crane (Passive)
class spell_legion_monk_way_of_the_crane : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_way_of_the_crane);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_WAY_OF_THE_CRANE_HEAL
        });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage() > 0)
            return true;
        return false;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        PreventDefaultAction();
        int32 damage = eventInfo.GetDamageInfo()->GetDamage();
        AddPct(damage, aurEff->GetAmount());
        eventInfo.GetActor()->CastCustomSpell(SPELL_MONK_WAY_OF_THE_CRANE_HEAL, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetActor(), true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_way_of_the_crane::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_way_of_the_crane::HandleProc, EFFECT_3, SPELL_AURA_PROC_TRIGGER_SPELL);
    }
};

// 152175 - Whirling Dragon Punch
class spell_legion_monk_whirling_dragon_punch : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_whirling_dragon_punch);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_WHIRLING_DRAGON_PUNCH_DAMAGE
        });
    }

    void HandlePeriodic(AuraEffect const* aurEff)
    {
        // Should tick 3 times, duration is 1000 and amplitude is 250 with attribute start_at_apply tho...
        if (aurEff->GetTickNumber() <= 3)
            if (Unit* caster = GetCaster())
                caster->CastSpell(caster, SPELL_MONK_WHIRLING_DRAGON_PUNCH_DAMAGE, true);
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_whirling_dragon_punch::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
    }
};

// 126892 - Zen Pilgrimage (Level 110)
class spell_legion_monk_zen_pilgrimage : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_zen_pilgrimage);

    bool Load() override
    {
        return GetCaster()->GetTypeId() == TYPEID_PLAYER;
    }

    void SetDest(SpellDestination& /*dest*/)
    {
        // Position should be saved with ID - 126893 Zen Pilgrimage/Death Gate/Moonglade Storage Aura I (Level 110) instead of recallposition...
        GetCaster()->ToPlayer()->SaveRecallPosition();
    }

    void Register() override
    {
        OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_legion_monk_zen_pilgrimage::SetDest, EFFECT_0, TARGET_DEST_DB);
    }
};

// 126895 - Zen Pilgrimage: Return
class spell_legion_monk_zen_pilgrimage_return : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_zen_pilgrimage_return);

    bool Load() override
    {
        return GetCaster()->GetTypeId() == TYPEID_PLAYER;
    }

    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        GetCaster()->RemoveAurasDueToSpell(SPELL_MONK_ZEN_PILGRIMAGE_OVERRIDE_AURA);
        GetCaster()->ToPlayer()->TeleportTo(GetCaster()->ToPlayer()->GetRecallPosition());
    }

    void Register() override
    {
        OnEffectHit += SpellEffectFn(spell_legion_monk_zen_pilgrimage_return::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

// 159535 - Yu'lon's Gift (Snare Remove)
// 116841 - Tiger's Lust
// 232877 - Niuzao's Essence
class spell_legion_monk_remove_snare : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_remove_snare);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_YU_LONGS_GIFT,
            SPELL_MONK_TIGERS_LUST
        });
    }

    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        if (m_scriptSpellId == SPELL_MONK_YU_LONGS_GIFT || m_scriptSpellId == SPELL_MONK_NIUZAOS_ESSENCE_BUFF)
            GetCaster()->RemoveAurasWithMechanic(1 << MECHANIC_SNARE);
        else
            GetCaster()->RemoveMovementImpairingAuras();
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_MONK_YU_LONGS_GIFT || m_scriptSpellId == SPELL_MONK_NIUZAOS_ESSENCE_BUFF)
            OnEffectHit += SpellEffectFn(spell_legion_monk_remove_snare::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        else if (m_scriptSpellId == SPELL_MONK_TIGERS_LUST)
            OnEffectHit += SpellEffectFn(spell_legion_monk_remove_snare::HandleScript, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
    }
};

// 124081 - Zen Pulse
class spell_legion_monk_zen_pulse : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_zen_pulse);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_MONK_ZEN_PULSE_HEAL });
    }

    SpellCastResult CheckCast()
    {
        // TODO: Should not be castable when there is no target that could potencially be hit
        return SPELL_CAST_OK;
    }

    void CountTargets(std::list<WorldObject*>& targets)
    {
        hitTargets = targets.size();
    }

    void HandleHit(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        SpellInfo const* spellInfoHeal = sSpellMgr->AssertSpellInfo(SPELL_MONK_ZEN_PULSE_HEAL);
        if (SpellEffectInfo const* effect = spellInfoHeal->GetEffect(EFFECT_0))
            if (Unit* target = GetHitUnit())
            {
                // Workaround, recalculation doesnt work
                for (int32 i = 0; i < hitTargets; ++i)
                    GetCaster()->CastCustomSpell(SPELL_MONK_ZEN_PULSE_HEAL, SPELLVALUE_BASE_POINT0, effect->CalcValue(), target, true);
            }
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_zen_pulse::CountTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_zen_pulse::HandleHit, EFFECT_0, SPELL_EFFECT_DUMMY);
        OnCheckCast += SpellCheckCastFn(spell_legion_monk_zen_pulse::CheckCast);
    }

    int32 hitTargets = 0;
};

/*
 *      Monk Pets
 */

 // 61146 - Black Ox Statue (Spell: 115315)
struct npc_legion_monk_pet_black_ox_statue : ScriptedAI
{
    TaskScheduler scheduler;

    enum ChiJiSpells
    {
        SPELL_MONK_BLACK_OX_STATUE_AURA = 163177,
    };

    npc_legion_monk_pet_black_ox_statue(Creature* creature) : ScriptedAI(creature) { }

    void InitializeAI() override
    {
        me->SetReactState(REACT_PASSIVE);
        me->CastSpell(me, SPELL_MONK_BLACK_OX_STATUE_AURA, true);
    }

    void UpdateAI(uint32 /*diff*/) override
    {
        // Nothing to do here
    }
};

// 63508 - Xuen (Spell: 123904)
struct npc_legion_monk_pet_xuen : ScriptedAI
{
    TaskScheduler scheduler;

    enum ChiJiSpells
    {
        SPELL_MONK_XUEN_DAMAGE_AURA = 123999,
    };

    npc_legion_monk_pet_xuen(Creature* creature) : ScriptedAI(creature) { }

    void InitializeAI() override
    {
        me->SetReactState(REACT_AGGRESSIVE);

        if (Unit* owner = me->GetOwner())
            if (Unit* ownerTarget = owner->GetVictim())
                me->Attack(ownerTarget, true);

        me->CastSpell(me, SPELL_MONK_SPAWN_SMOKE, true);
        me->CastSpell(me, SPELL_MONK_XUEN_DAMAGE_AURA, true);
    }

    void UpdateAI(uint32 diff) override
    {
        scheduler.Update(diff);

        DoMeleeAttackIfReady();
    }
};

// 73967 - Niuzao (Spell: 132578)
struct npc_legion_monk_pet_niuzao : ScriptedAI
{
    enum ChiJiSpells
    {
        SPELL_GEN_AVOIDANCE = 65220,
        SPELL_GEN_PLAYER_DAMAGE_REDUCTION = 115043,
        SPELL_GEN_SPAWN_SMOKE_DRUID = 36747,
        // Xuen, the White Tiger
        SPELL_MONK_EMINENCE_XUEN = 130324,
        SPELL_MONK_INVOKE_XUEN_THE_WHITE_TIGER = 123995,
        SPELL_MONK_TIGER_LEAP = 124007,

        // Niuzao, the Black Ox
        SPELL_MONK_INVOKE_NIUZAO_THE_BLACK_OX = 196729,
        SPELL_MONK_OX_CHARGE = 196728
    };

    npc_legion_monk_pet_niuzao(Creature* creature) : ScriptedAI(creature) { }

    void InitializeAI() override
    {
        me->SetReactState(REACT_AGGRESSIVE);
        DoCast(me, SPELL_GEN_AVOIDANCE, true);
        DoCast(me, SPELL_GEN_PLAYER_DAMAGE_REDUCTION, true);
        DoCast(me, SPELL_GEN_SPAWN_SMOKE_DRUID, true);
        DoCast(me, SPELL_MONK_INVOKE_NIUZAO_THE_BLACK_OX, true); // threat buff
    }

    void AttackStart(Unit* victim) override
    {
        if (!victim)
            return;

        // try to reach the target with a charge
        if (me->GetDistance(victim) >= 5.0f)
        {
            SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(SPELL_MONK_OX_CHARGE);
            if (me->IsWithinDist(victim, spellInfo->GetMaxRange(true)))
                DoCast(victim, SPELL_MONK_OX_CHARGE, true);
        }

        UnitAI::AttackStart(victim);
    }

    void UpdateAI(uint32 /*diff*/) override
    {
        if (!UpdateVictim())
        {
            if (me->HasUnitState(UNIT_STATE_FOLLOW))
                return;

            Unit* owner = me->GetOwner();

            if (!owner || !owner->ToPlayer())
            {
                me->DespawnOrUnsummon();
                return;
            }

            if (Unit* target = owner->ToPlayer()->GetSelectedUnit())
                if (me->IsValidAttackTarget(target))
                    AttackStart(target);
        }

        DoMeleeAttackIfReady();
    }
};

// 100868 - Chi-Ji (Spell: 198664)
struct npc_legion_monk_pet_chi_ji : ScriptedAI
{
    TaskScheduler scheduler;

    enum ChiJiSpells
    {
        SPELL_MONK_CHI_JI_HEAL_SEARCHER = 198766
    };

    npc_legion_monk_pet_chi_ji(Creature* creature) : ScriptedAI(creature) { }

    void InitializeAI() override
    {
        me->SetReactState(REACT_PASSIVE);

        me->CastSpell(me, SPELL_MONK_SPAWN_SMOKE, true);

        scheduler.Schedule(Seconds(1), [this](TaskContext healEvent)
        {
            me->CastSpell(me, SPELL_MONK_CHI_JI_HEAL_SEARCHER, false);
            healEvent.Repeat(Seconds(2));
        });
    }

    void UpdateAI(uint32 diff) override
    {
        scheduler.Update(diff);
    }
};

// 198766 - Crane Heal (Aoe Searcher)
class spell_legion_monk_chi_ji_heal_searcher : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_chi_ji_heal_searcher);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CHI_JI_CRANE_HEAL_JUMP
        });
    }

    void FilterTargets(std::list<WorldObject*>& targets)
    {
        if (targets.empty())
            return;
        targets.sort(Trinity::HealthPctOrderPred());
        // No basepoints with resize to 3, only ToolTip: Chi'Ji dashes to three allies
        if (targets.size() > 3)
            targets.resize(3);
    }

    void HandleHit(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
            if (Unit* owner = GetCaster()->GetOwner())
            {
                // Fix Pet System so ChiJi gets owners int as ap
                int32 heal = int32(owner->GetStat(STAT_INTELLECT) * 1.35f);
                GetCaster()->CastCustomSpell(SPELL_MONK_CHI_JI_CRANE_HEAL_JUMP, SPELLVALUE_BASE_POINT1, heal, target, true);
            }
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_monk_chi_ji_heal_searcher::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_chi_ji_heal_searcher::HandleHit, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 196743 - Chi Orbit
class spell_legion_monk_chi_orbit : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_chi_orbit);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_MONK_CHI_ORBIT_1,
            SPELL_MONK_CHI_ORBIT_2,
            SPELL_MONK_CHI_ORBIT_3,
            SPELL_MONK_CHI_ORBIT_4
        });
    }

    void HandlePeriodic(AuraEffect const* /*aurEff*/)
    {
        for (uint8 i = 0; i < 4; i++)
        {
            if (!GetTarget()->HasAura(chiOrbitHelper[i]))
            {
                GetTarget()->CastSpell(GetTarget(), chiOrbitHelper[i], true);
                break;
            }
        }
    }

    void HandleEffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        for (uint8 i = 0; i < 4; i++)
            GetTarget()->RemoveAurasDueToSpell(chiOrbitHelper[i]);
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_chi_orbit::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
        AfterEffectRemove += AuraEffectRemoveFn(spell_legion_monk_chi_orbit::HandleEffectRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

struct areatrigger_legion_monk_chi_orbit : public AreaTriggerAI
{
    areatrigger_legion_monk_chi_orbit(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitEnter(Unit* target) override
    {
        if (Unit* caster = at->GetCaster())
        {
            if (!caster->IsValidAttackTarget(target) || triggered)
                return;

            triggered = true; // prevent xxxx procs if monk jump into a npc group
            caster->CastSpell(target, SPELL_MONK_CHI_ORBIT_DAMAGE, true);
            caster->RemoveAurasDueToSpell(at->GetSpellId());
        }
    }

private:
    bool triggered = false;
};

// 157411 - Windwalking
struct areatrigger_legion_monk_windwalking : public AreaTriggerAI
{
    areatrigger_legion_monk_windwalking(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitEnter(Unit* target) override
    {
        if (Unit* caster = at->GetCaster())
        {
            if (!caster->IsValidAssistTarget(target) || (target->GetTypeId() == TYPEID_UNIT && !target->GetOwnerGUID()))
                return;

            caster->CastSpell(target, SPELL_MONK_WINDWALKING_BUFF, true);
        }
    }

    void OnUnitExit(Unit* target) override
    {
        target->RemoveAurasDueToSpell(SPELL_MONK_WINDWALKING_BUFF, at->GetCasterGuid());
    }
};

// 116844 - Ring of Peace
struct areatrigger_legion_monk_ring_of_peace : public AreaTriggerAI
{
    areatrigger_legion_monk_ring_of_peace(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnCreate() override
    {
        _scheduler.Schedule(Milliseconds(750), [this](TaskContext task)
        {
            if (!at->GetInsideUnits().empty())
                if (Unit* caster = at->GetCaster())
                    caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_MONK_RING_OF_PEACE_KNOCKBACK, true);

            task.Repeat();
        });
    }

    void OnUpdate(uint32 diff) override
    {
        _scheduler.Update(diff);
    }

private:
    TaskScheduler _scheduler;
};

// 124503 - 124506 - 214418 - 214420 - Gift of the Ox
struct areatrigger_legion_monk_gift_of_the_ox : public AreaTriggerAI
{
    areatrigger_legion_monk_gift_of_the_ox(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitEnter(Unit* unit) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || unit != caster || removedByEnter)
            return;

        uint32 spellId = at->GetEntry() == 11692 ? SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEAL : SPELL_MONK_GIFT_OF_THE_OX_HEAL;

        caster->CastSpell(caster, spellId, true);
        removedByEnter = true;
        at->Remove();
    }

    void OnRemove() override
    {
        if (removedByEnter)
            return;

        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        uint32 spellId = at->GetEntry() == 11692 ? SPELL_MONK_GIFT_OF_THE_OX_GREATER_HEAL_EXPIRED : SPELL_MONK_GIFT_OF_THE_OX_HEAL_EXPIRED;

        caster->CastSpell(caster, spellId, true);
    }

private:
    bool removedByEnter = false;
};

// 199391 Spirit Tether
struct areatrigger_legion_monk_spirit_tether : public AreaTriggerAI
{
    areatrigger_legion_monk_spirit_tether(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitEnter(Unit* unit) override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        if (caster->_IsValidAttackTarget(unit, sSpellMgr->GetSpellInfo(SPELL_MONK_SPIRIT_TETHER_SLOW)))
            caster->CastSpell(unit, SPELL_MONK_SPIRIT_TETHER_SLOW, true);
    }

    void OnUnitExit(Unit* unit) override
    {
        unit->RemoveAura(SPELL_MONK_SPIRIT_TETHER_SLOW, at->GetCasterGuid());
    }

    void OnUpdate(uint32 /*diff*/) override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        GuidUnorderedSet const& guids = at->GetInsideUnits();
        for (ObjectGuid const& insideTargetGuid : guids)
        {
            Unit* insideTarget = ObjectAccessor::GetUnit(*at, insideTargetGuid);
            if (!insideTarget)
                continue;

            if (insideTarget->HasAura(SPELL_MONK_SPIRIT_TETHER_SLOW, at->GetCasterGuid()))
                continue;

            if (caster->_IsValidAttackTarget(insideTarget, sSpellMgr->AssertSpellInfo(SPELL_MONK_SPIRIT_TETHER_SLOW)))
                caster->CastSpell(insideTarget, SPELL_MONK_SPIRIT_TETHER_SLOW, true);
        }
    }
};

struct areatrigger_legion_monk_sheilungs_gift : public AreaTriggerAI
{
    areatrigger_legion_monk_sheilungs_gift(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnCreate() override
    {
        if (Unit* caster = at->GetCaster())
            caster->CastSpell(caster, SPELL_MONK_SHEILUNS_GIFT_COUNTER, true);
    }

    void OnRemove() override
    {
        if (Unit* caster = at->GetCaster())
            caster->RemoveAuraFromStack(SPELL_MONK_SHEILUNS_GIFT_COUNTER);
    }
};

// 205406 - Sheilun's Gift
class spell_legion_monk_sheiluns_gift : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_sheiluns_gift);

    void HandleHit(SpellEffIndex /*effIndex*/)
    {
        std::vector<AreaTrigger*> areaTriggers = GetCaster()->GetAreaTriggers(SPELL_MONK_SHEILUNS_GIFT_AREATRIGGER);
        for (AreaTrigger* at : areaTriggers)
        {
            if (guids.find(at->GetGUID()) == guids.end())
                continue;

            at->Remove();
        }

        SetHitHeal(GetHitHeal() * guids.size());
    }

    void HandleVisual()
    {
        std::vector<AreaTrigger*> areaTriggers = GetCaster()->GetAreaTriggers(SPELL_MONK_SHEILUNS_GIFT_AREATRIGGER);
        for (AreaTrigger* at : areaTriggers)
        {
            GetCaster()->SendPlayOrphanSpellVisual(at, GetCaster()->GetGUID(), 56861, 1.5f, true, true);
            guids.insert(at->GetGUID());
        }
    }

    void Register() override
    {
        OnPrepare += SpellPrepareFn(spell_legion_monk_sheiluns_gift::HandleVisual);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_sheiluns_gift::HandleHit, EFFECT_0, SPELL_EFFECT_HEAL);
    }

private:
    GuidUnorderedSet guids;
};

// 214483 - Sheilun's Gift
class spell_legion_monk_sheiluns_gift_periodic : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_sheiluns_gift_periodic);

    void HandlePeriodic(AuraEffect const* /*aurEff*/)
    {
        if (GetTarget()->IsInCombat())
            GetTarget()->CastSpell(GetTarget(), SPELL_MONK_SHEILUNS_GIFT_AREATRIGGER, true);
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_monk_sheiluns_gift_periodic::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
    }
};

// 115310 - Revival
class spell_legion_monk_revival : public SpellScript
{
    PrepareSpellScript(spell_legion_monk_revival);

    void HandleHit(SpellEffIndex /*effIndex*/)
    {
        if (AuraEffect* blessingOfYulon = GetCaster()->GetAuraEffect(SPELL_MONK_BLESSING_OF_YULON, EFFECT_0))
            GetCaster()->CastCustomSpell(SPELL_MONK_BLESSING_OF_YULON_PERIODIC_HEAL, SPELLVALUE_BASE_POINT0, CalculatePct(GetHitHeal(), blessingOfYulon->GetAmount()), GetHitUnit(), true);
    }

    void HandleYulon()
    {
        if (GetCaster()->HasAura(SPELL_MONK_BLESSING_OF_YULON))
            GetCaster()->CastSpell(GetCaster(), SPELL_MONK_BLESSING_OF_YULON_DRAGON, true);
    }

    void Register() override
    {
        OnCast += SpellCastFn(spell_legion_monk_revival::HandleYulon);
        OnEffectHitTarget += SpellEffectFn(spell_legion_monk_revival::HandleHit, EFFECT_0, SPELL_EFFECT_HEAL);
    }
};

// 196607 - Eye of the Tiger
class spell_legion_monk_eye_of_the_tiger : public AuraScript
{
    PrepareAuraScript(spell_legion_monk_eye_of_the_tiger);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetDamageInfo() != nullptr;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        PreventDefaultAction();

        if (GetCaster() == eventInfo.GetDamageInfo()->GetVictim())
            return;

        SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(196608);
        GetUnitOwner()->AddAura(spellInfo, (1 << EFFECT_0), GetUnitOwner());
        GetUnitOwner()->AddAura(spellInfo, (1 << EFFECT_1), eventInfo.GetDamageInfo()->GetVictim());
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_legion_monk_eye_of_the_tiger::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_legion_monk_eye_of_the_tiger::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
    }
};

void AddSC_legion_monk_spell_scripts()
{
    RegisterAuraScript(spell_legion_monk_afterlife);
    RegisterSpellScript(spell_legion_monk_black_ox_brew);
    RegisterAuraScript(spell_legion_monk_blackout_combo);
    RegisterSpellScript(spell_legion_monk_breath_of_fire);
    RegisterAuraScript(spell_legion_monk_celestial_breath);
    RegisterAuraScript(spell_legion_monk_celestial_fortune);
    RegisterSpellScript(spell_legion_monk_chi_ji_heal_searcher);
    RegisterAuraScript(spell_legion_monk_chi_orbit);
    RegisterSpellScript(spell_legion_monk_chi_torpedo);
    RegisterSpellScript(spell_legion_monk_chi_wave);
    RegisterSpellScript(spell_legion_monk_chi_wave_heal);
    RegisterSpellScript(spell_legion_monk_chi_wave_markers);
    RegisterSpellScript(spell_legion_monk_chi_wave_target_select);
    RegisterAuraScript(spell_legion_monk_crackling_jade_lightning_aura);
    RegisterAuraScript(spell_legion_monk_dampen_harm);
    RegisterAuraScript(spell_legion_monk_dark_side_of_the_moon);
    RegisterSpellScript(spell_legion_monk_diffuse_magic);
    RegisterAuraScript(spell_legion_monk_disable);
    RegisterSpellScript(spell_legion_monk_effuse_honor_talent);
    RegisterAuraScript(spell_legion_monk_elusive_brawler);
    RegisterSpellAndAuraScriptPair(spell_legion_monk_enveloping_mist, spell_legion_monk_enveloping_mist_AuraScript);
    RegisterAuraScript(spell_legion_monk_essence_font);
    RegisterSpellScript(spell_legion_monk_essence_font_heal);
    RegisterAuraScript(spell_legion_monk_fists_of_fury);
    RegisterAuraScript(spell_legion_monk_flying_serpent_kick_second_cast);
    RegisterAuraScript(spell_legion_monk_fortification);
    RegisterSpellScript(spell_legion_monk_fortifying_brew);
    RegisterAuraScript(spell_legion_monk_gale_burst);
    RegisterAuraScript(spell_legion_monk_gift_of_the_ox);
    RegisterSpellScript(spell_legion_monk_gift_of_the_ox_healing_sphere_expired);
    RegisterAuraScript(spell_legion_monk_guard);
    RegisterAuraScript(spell_legion_monk_gust_of_mists_mastery);
    RegisterAuraScript(spell_legion_monk_healing_elixir);
    RegisterSpellScript(spell_legion_monk_ironskin_brew);
    RegisterSpellScript(spell_legion_monk_keg_smash);
    RegisterAuraScript(spell_legion_monk_life_cocoon);
    RegisterAuraScript(spell_legion_monk_lifecycles);
    RegisterSpellScript(spell_legion_monk_provoke);
    RegisterSpellScript(spell_legion_monk_purifying_brew);
    RegisterSpellScript(spell_legion_monk_remove_snare);
    RegisterSpellScript(spell_legion_monk_renewing_mist);
    RegisterAuraScript(spell_legion_monk_renewing_mist_hot);
    RegisterSpellScript(spell_legion_monk_renewing_mist_jump);
    RegisterSpellScript(spell_legion_monk_rising_sun_kick);
    RegisterAuraScript(spell_legion_monk_rising_thunder);
    RegisterSpellScript(spell_legion_monk_roll);
    RegisterSpellScript(spell_legion_monk_rushing_jade_wind_damage);
    RegisterAuraScript(spell_legion_monk_soothing_mist);
    RegisterAuraScript(spell_legion_monk_soothing_mist_passive);
    RegisterSpellScript(spell_legion_monk_special_delivery);
    RegisterAuraScript(spell_legion_monk_special_delivery_aura);
    RegisterSpellScript(spell_legion_monk_spinning_crane_kick);
    RegisterAuraScript(spell_legion_monk_spirit_of_the_crane_passive);
    RegisterAuraScript(spell_legion_monk_stagger_base);
    RegisterAuraScript(spell_legion_monk_stagger_dot);
    RegisterAuraScript(spell_legion_monk_teachings_of_the_monastery_buff);
    RegisterAuraScript(spell_legion_monk_teachings_of_the_monastery_passive);
    RegisterSpellScript(spell_legion_monk_tiger_palm);
    RegisterAuraScript(spell_legion_monk_tornado_kicks);
    RegisterAuraScript(spell_legion_monk_touch_of_death);
    RegisterAuraScript(spell_legion_monk_touch_of_karma);
    RegisterAuraScript(spell_legion_monk_touch_of_karma_buff);
    RegisterSpellAndAuraScriptPair(spell_legion_monk_transcendence, spell_legion_monk_transcendence_AuraScript);
    RegisterSpellScript(spell_legion_monk_transcendence_transfer);
    RegisterSpellScript(spell_legion_monk_vivify);
    RegisterAuraScript(spell_legion_monk_way_of_the_crane);
    RegisterAuraScript(spell_legion_monk_whirling_dragon_punch);
    RegisterSpellScript(spell_legion_monk_zen_pilgrimage);
    RegisterSpellScript(spell_legion_monk_zen_pilgrimage_return);
    RegisterSpellScript(spell_legion_monk_zen_pulse);
    RegisterSpellScript(spell_legion_monk_sheiluns_gift);
    RegisterAuraScript(spell_legion_monk_sheiluns_gift_periodic);
    RegisterSpellScript(spell_legion_monk_revival);
    RegisterAuraScript(spell_legion_monk_eye_of_the_tiger);

    RegisterCreatureAI(npc_legion_monk_pet_black_ox_statue);
    RegisterCreatureAI(npc_legion_monk_pet_chi_ji);
    RegisterCreatureAI(npc_legion_monk_pet_niuzao);
    RegisterCreatureAI(npc_legion_monk_pet_xuen);

    RegisterAreaTriggerAI(areatrigger_legion_monk_chi_orbit);
    RegisterAreaTriggerAI(areatrigger_legion_monk_windwalking);
    RegisterAreaTriggerAI(areatrigger_legion_monk_ring_of_peace);
    RegisterAreaTriggerAI(areatrigger_legion_monk_gift_of_the_ox);
    RegisterAreaTriggerAI(areatrigger_legion_monk_spirit_tether);
    RegisterAreaTriggerAI(areatrigger_legion_monk_sheilungs_gift);
}
