/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

void AddSC_legion_deathknight_spell_scripts();
void AddSC_legion_demon_hunter_spell_scripts();
void AddSC_legion_druid_spell_scripts();
void AddSC_legion_generic_spell_scripts();
void AddSC_legion_honor_spell_scripts();
void AddSC_legion_hunter_spell_scripts();
void AddSC_legion_mage_spell_scripts();
void AddSC_legion_monk_spell_scripts();
void AddSC_legion_paladin_spell_scripts();
void AddSC_legion_priest_spell_scripts();
void AddSC_legion_rogue_spell_scripts();
void AddSC_legion_shaman_spell_scripts();
void AddSC_legion_warlock_spell_scripts();
void AddSC_legion_warrior_spell_scripts();
void AddSC_legion_quest_spell_scripts();
void AddSC_legion_item_spell_scripts();
void AddSC_legion_holiday_spell_scripts();

// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddSpellsLegionScripts()
{
    AddSC_legion_deathknight_spell_scripts();
    AddSC_legion_demon_hunter_spell_scripts();
    AddSC_legion_druid_spell_scripts();
    AddSC_legion_generic_spell_scripts();
    AddSC_legion_honor_spell_scripts();
    AddSC_legion_hunter_spell_scripts();
    AddSC_legion_mage_spell_scripts();
    AddSC_legion_monk_spell_scripts();
    AddSC_legion_paladin_spell_scripts();
    AddSC_legion_priest_spell_scripts();
    AddSC_legion_rogue_spell_scripts();
    AddSC_legion_shaman_spell_scripts();
    AddSC_legion_warlock_spell_scripts();
    AddSC_legion_warrior_spell_scripts();
    AddSC_legion_quest_spell_scripts();
    AddSC_legion_item_spell_scripts();
    AddSC_legion_holiday_spell_scripts();
}
