/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_PRIEST and SPELLFAMILY_GENERIC spells used by priest players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_pri_".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "GridNotifiers.h"
#include "Group.h"
#include "MoveSplineInitArgs.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"
#include <G3D/Vector3.h>

enum PriestSpells
{
    SPELL_GENERIC_ID                            = 205477, // Used on tooltip, dont know if its available for players
    SPELL_PRIEST_ANGELIC_FEATHER_SPEED_BUFF     = 121557,
    SPELL_PRIEST_ANGELIC_FEATHER_SUMMON         = 158624,
    SPELL_PRIEST_APOTHEOSIS                     = 200183,
    SPELL_PRIEST_ATONEMENT                      = 81749,
    SPELL_PRIEST_ATONEMENT_BUFF                 = 194384,
    SPELL_PRIEST_ATONEMENT_HEAL_CAN_CRIT        = 94472,
    SPELL_PRIEST_ATONEMENT_HEAL_CANT_CRIT       = 81751, // used on critical damage proc
    SPELL_PRIEST_BARRIER_FOR_THE_DEVOTED        = 197815,
    SPELL_PRIEST_BENEDICTION                    = 193157,
    SPELL_PRIEST_BINDING_HEAL                   = 32546,
    SPELL_PRIEST_BLESSED_LIGHT                  = 196813,
    SPELL_PRIEST_BODY_AND_SOUL                  = 64129,
    SPELL_PRIEST_BODY_AND_SOUL_SPEED_BUFF       = 65081,
    SPELL_PRIEST_BORROWED_TIME                  = 197762,
    SPELL_PRIEST_BORROWED_TIME_AURA             = 197763,
    SPELL_PRIEST_CALL_OF_THE_VOID_SUMMON        = 193470,
    SPELL_PRIEST_CENSURE                        = 200199,
    SPELL_PRIEST_DARK_ARCHANGEL_BUFF            = 197874,
    SPELL_PRIEST_DAZZING_LIGHTS                 = 196810,
    SPELL_PRIEST_DELIVERED_FROM_EVIL            = 196611,
    SPELL_PRIEST_DIVINE_HYMN                    = 64843,
    SPELL_PRIEST_DIVINE_STAR_DAMAGE             = 122128,
    SPELL_PRIEST_DIVINE_STAR_HEAL               = 110745,
    SPELL_PRIEST_ECHO_OF_LIGHT_HEAL             = 77489,
    SPELL_PRIEST_FLASH_HEAL                     = 2061,
    SPELL_PRIEST_FLEETING_EMBRACE               = 199388,
    SPELL_PRIEST_FOCUS_IN_THE_LIGHT_CASTER      = 210980,
    SPELL_PRIEST_FOCUS_IN_THE_LIGHT_ENEMY       = 210979,
    SPELL_PRIEST_FOCUSED_WILL                   = 45242,
    SPELL_PRIEST_GRACE                          = 200309,
    SPELL_PRIEST_GUARDIAN_ANGEL                 = 200209,
    SPELL_PRIEST_GUARDIAN_SPIRIT                = 47788,
    SPELL_PRIEST_GUARDIAN_SPIRIT_HEAL           = 48153,
    SPELL_PRIEST_GUARDIANS_OF_THE_LIGHT         = 196437,
    SPELL_PRIEST_HALO_DAMAGE                    = 120696,
    SPELL_PRIEST_HALO_HEAL                      = 120692,
    SPELL_PRIEST_HEAL                           = 2060,
    SPELL_PRIEST_HEALING_LIGHT                  = 196809,
    SPELL_PRIEST_HOLY_FIRE                      = 14914,
    SPELL_PRIEST_HOLY_FIRE_EVENT_MARKER         = 231403,
    SPELL_PRIEST_HOLY_MENDING                   = 196779,
    SPELL_PRIEST_HOLY_MENDING_HEAL              = 196781,
    SPELL_PRIEST_HOLY_NOVA                      = 132157,
    SPELL_PRIEST_HOLY_WORD_CHASTISE_DISORIENT   = 200196,
    SPELL_PRIEST_HOLY_WORD_CHASTISE_STUN        = 200200,
    SPELL_PRIEST_HOLY_WORD_CHATISE              = 88625,
    SPELL_PRIEST_HOLY_WORD_SANCTIFY             = 34861,
    SPELL_PRIEST_HOLY_WORD_SERENITY             = 2050,
    SPELL_PRIEST_INSANITY                       = 194251,
    SPELL_PRIEST_INSANITY_1                     = 185908,
    SPELL_PRIEST_INSANITY_2                     = 185909,
    SPELL_PRIEST_INSANITY_3                     = 185910,
    SPELL_PRIEST_INSANITY_4                     = 185911,
    SPELL_PRIEST_INVOKE_THE_NAARU_SUMMON        = 196685,
    SPELL_PRIEST_LEAP_OF_FAITH_EFFECT           = 92832,
    SPELL_PRIEST_LEAP_OF_FAITH_EFFECT_TRIGGER   = 92833,
    SPELL_PRIEST_LEVITATE_TRIGGERED             = 111759,
    SPELL_PRIEST_LIGHT_ERUPTION                 = 196812,
    SPELL_PRIEST_LIGHT_OF_THE_NAARU             = 196985,
    SPELL_PRIEST_LINGERING_INSANITY             = 199849,
    SPELL_PRIEST_LINGERING_INSANITY_PERIODIC    = 197937,
    SPELL_PRIEST_MANA_LEECH                     = 123051,
    SPELL_PRIEST_MANIA_SPEED_BUFF               = 195290,
    SPELL_PRIEST_MASOCHISM                      = 193063,
    SPELL_PRIEST_MASOCHISM_PERIODIC_HEAL        = 193065,
    SPELL_PRIEST_MASS_HYSTERIA                  = 194378,
    SPELL_PRIEST_MENTAL_FORTITUDE               = 194018,
    SPELL_PRIEST_MENTAL_FORTITUDE_SHIELD        = 194022,
    SPELL_PRIEST_MIND_BLAST                     = 8092,
    SPELL_PRIEST_MIND_BOMB_TRIGGERED            = 226943,
    SPELL_PRIEST_MIND_FLAY                      = 15407,
    SPELL_PRIEST_MIND_FLAY_AOE_DAMAGE           = 237388,
    SPELL_PRIEST_MIND_FLAY_AOE_SELECTOR         = 234696,
    SPELL_PRIEST_MIND_FLAY_ENERGIZE             = 208232,
    SPELL_PRIEST_MIND_SEAR_PASSIVE              = 234702,
    SPELL_PRIEST_MIND_TRAUMA                    = 199445,
    SPELL_PRIEST_MISERY                         = 238558,
    SPELL_PRIEST_PENANCE                        = 47540,
    SPELL_PRIEST_PENANCE_DAMAGE                 = 47666,
    SPELL_PRIEST_PENANCE_HEAL                   = 47750,
    SPELL_PRIEST_PENANCE_TARGET_ALLY            = 47757,
    SPELL_PRIEST_PENANCE_TARGET_ENEMY           = 47758,
    SPELL_PRIEST_PERSEVERANCE                   = 235189,
    SPELL_PRIEST_PIETY                          = 197034,
    SPELL_PRIEST_PLEA                           = 200829,
    SPELL_PRIEST_PLEA_MANA_COST_BUFF            = 212100,
    SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_AURA    = 198069,
    SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_MARKER  = 225795,
    SPELL_PRIEST_POWER_OF_THE_NAARU_BONUS       = 196490,
    SPELL_PRIEST_POWER_WORD_BARRIER_AURA        = 81782,
    SPELL_PRIEST_POWER_WORD_SHIELD              = 17,
    SPELL_PRIEST_POWER_WORD_SOLACE_ENERGIZE     = 129253,
    SPELL_PRIEST_PRAYER_OF_HEALING              = 596,
    SPELL_PRIEST_PRAYER_OF_MENDING              = 33076,
    SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE       = 225275,
    SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE_AREA  = 155793,
    SPELL_PRIEST_PRAYER_OF_MENDING_HEAL         = 33110,
    SPELL_PRIEST_PRAYER_OF_MENDING_PROC_AURA    = 41635,
    SPELL_PRIEST_PREMONITION                    = 209780,
    SPELL_PRIEST_PREMONITION_DAMAGE             = 209885,
    SPELL_PRIEST_PSYCHIC_LINK_DAMAGE            = 199486,
    SPELL_PRIEST_PSYCHIC_LINK_HONOR_TALENT      = 199484,
    SPELL_PRIEST_PURE_SHADOW_HONOR_TALENT       = 199131,
    SPELL_PRIEST_PURGE_THE_WICKED               = 204197,
    SPELL_PRIEST_PURGE_THE_WICKED_DOT           = 204213,
    SPELL_PRIEST_PURGE_THE_WICKED_SEARCHER      = 204215,
    SPELL_PRIEST_PURIFIED_RESOLVE_HONOR_TALENT  = 196439,
    SPELL_PRIEST_PURIFIED_RESOLVE_SHIELD        = 196440,
    SPELL_PRIEST_REAPER_OF_SOULS                = 199853,
    SPELL_PRIEST_RENEW                          = 139,
    SPELL_PRIEST_SAY_YOUR_PRAYERS               = 196358,
    SPELL_PRIEST_SEARING_LIGHT                  = 196811,
    SPELL_PRIEST_SHADOW_COVENANT_ABSORB         = 219521,
    SPELL_PRIEST_SHADOW_MANIA_PERIODIC          = 199579,
    SPELL_PRIEST_SHADOW_MEND_DAMAGE             = 186439,
    SPELL_PRIEST_SHADOW_MEND_PERIODIC_DUMMY     = 187464,
    SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE     = 190714,
    SPELL_PRIEST_SHADOW_WORD_DEATH_MARKER       = 226734,
    SPELL_PRIEST_SHADOW_WORD_PAIN               = 589,
    SPELL_PRIEST_SHADOWFORM                     = 232698,
    SPELL_PRIEST_SHADOWY_APPARITION_MISSILE     = 147193,
    SPELL_PRIEST_SHADOWY_INSIGHT_AURA           = 124430,
    SPELL_PRIEST_SHARE_IN_THE_LIGHT             = 197781,
    SPELL_PRIEST_SHARE_IN_THE_LIGHT_ABSORB      = 210027,
    SPELL_PRIEST_SHIELD_DISCIPLINE              = 197045,
    SPELL_PRIEST_SHIELD_DISCIPLINE_ENERGIZE     = 47755,
    SPELL_PRIEST_SHIELD_OF_FAITH                = 197729,
    SPELL_PRIEST_SILENCE                        = 215774,
    SPELL_PRIEST_SIN_AND_PUNISHMENT             = 87204,
    SPELL_PRIEST_SINS_OF_THE_MANY               = 198074,
    SPELL_PRIEST_SINS_OF_THE_MANY_AURA          = 198076,
    SPELL_PRIEST_SMITE                          = 585,
    SPELL_PRIEST_SMITE_AREA_ABSORB              = 208771,
    SPELL_PRIEST_SMITE_ENEMY_DEBUFF             = 208772,
    SPELL_PRIEST_SMITE_LEVEL_BONUS              = 231682,
    SPELL_PRIEST_SPEED_OF_THE_PIOUS             = 197766,
    SPELL_PRIEST_SPEED_OF_THE_PIOUS_AURA        = 197767,
    SPELL_PRIEST_SPHERE_OF_INSANITY             = 194179,
    SPELL_PRIEST_SPHERE_OF_INSANITY_DAMAGE      = 194238,
    SPELL_PRIEST_SPHERE_OF_INSANITY_PROC        = 194230,
    SPELL_PRIEST_SPHERE_OF_INSANITY_SUMMON      = 194182,
    SPELL_PRIEST_SPIRIT_OF_REDEMPTION           = 27827,
    SPELL_PRIEST_SPIRIT_OF_THE_REDEEMER         = 215982,
    SPELL_PRIEST_STRENGTH_OF_SOUL_HEAL          = 197470,
    SPELL_PRIEST_STRENGTH_OF_SOUL_HONOR_TALENT  = 197535,
    SPELL_PRIEST_STRENGTH_OF_SOUL_SHIELD        = 197548,
    SPELL_PRIEST_SURGE_OF_LIGHT_TRIGGERED       = 114255,
    SPELL_PRIEST_SURRENDER_TO_MADNESS           = 193223,
    SPELL_PRIEST_SURRENDER_TO_MADNESS_KILL      = 195455,
    SPELL_PRIEST_SUSTAINED_SANITY               = 219772,
    SPELL_PRIEST_TAMING_THE_SHADOWS             = 197779,
    SPELL_PRIEST_THE_PENITENT                   = 200347,
    SPELL_PRIEST_THRIVE_IN_THE_SHADOWS          = 194024,
    SPELL_PRIEST_THRIVE_IN_THE_SHADOWS_HEAL     = 194025,
    SPELL_PRIEST_TRAIL_OF_LIGHT_HEAL            = 234946,
    SPELL_PRIEST_TRANQUIL_LIGHT                 = 196816,
    SPELL_PRIEST_TRINITY_HONOR_TALENT           = 214205,
    SPELL_PRIEST_TRUST_IN_THE_LIGHT             = 196355,
    SPELL_PRIEST_TRUST_IN_THE_LIGHT_PERIODIC    = 196356,
    SPELL_PRIEST_VAMPIRIC_EMBRACE_HEAL          = 15290,
    SPELL_PRIEST_VAMPIRIC_TOUCH                 = 34914,
    SPELL_PRIEST_VESTMENTS_OF_DISCIPLINE        = 197711,
    SPELL_PRIEST_VESTMENTS_OF_DISCIPLINE_AURA   = 216135,
    SPELL_PRIEST_VIM_AND_VIGOR_BONUS            = 195488,
    SPELL_PRIEST_VOID_BOLT_BONUS                = 231688,
    SPELL_PRIEST_VOID_ERUPTION_DAMAGE           = 228360,
    SPELL_PRIEST_VOID_SHIELD                    = 199145,
    SPELL_PRIEST_VOID_SHIELD_HEAL               = 204778,
    SPELL_PRIEST_VOID_SHIELD_HONOR_TALENT       = 199144,
    SPELL_PRIEST_VOID_SHIFT                     = 118594,
    SPELL_PRIEST_VOID_TORRENT                   = 205065,
    SPELL_PRIEST_VOIDFORM                       = 194249,
    SPELL_PRIEST_VOIDFORM_CONTROLLER            = 210199,
    SPELL_PRIEST_RAY_OF_HOPE_HEAL               = 197336,
    SPELL_PRIEST_RAY_OF_HOPE_DAMAGE             = 197341,
    SPELL_PRIEST_RAY_OF_HOPE_HEAL_INFO          = 232707,
    SPELL_PRIEST_RAY_OF_HOPE_DAMAGE_INFO        = 232708,
    SPELL_PRIEST_PSYFIEND_SUMMON                = 211522,
};

enum PriestMiscData
{
    ACTION_RESHAPE                  = 0,
    SPELL_VISUAL_SHADOWY_APPARITION = 33584,
    SHADOWY_APPARITION_TRAVEL_SPEED = 6,
    NPC_SPHERE_OF_INSANITY          = 98680,
    NPC_IMAGE_OF_TUURE              = 99904
};

enum GenericSpells
{
    SPELL_GENERIC_POWER_LEECH       = 200010
};

// 121536 - Angelic Feather
class spell_pri_legion_angelic_feather : public SpellScriptLoader
{
    public:
        spell_pri_legion_angelic_feather() : SpellScriptLoader("spell_pri_legion_angelic_feather") { }

        class spell_pri_legion_angelic_feather_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_angelic_feather_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_ANGELIC_FEATHER_SUMMON
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                std::vector<AreaTrigger*> feathers = GetCaster()->GetAreaTriggers(SPELL_PRIEST_ANGELIC_FEATHER_SUMMON);
                if (feathers.size() == 3)
                    feathers.front()->Remove();

                if (WorldLocation* dest = GetHitDest())
                    GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_PRIEST_ANGELIC_FEATHER_SUMMON, true);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_pri_legion_angelic_feather_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_angelic_feather_SpellScript();
        }
};

// 197862 - Archangel
class spell_pri_legion_archangel : public SpellScriptLoader
{
    public:
        spell_pri_legion_archangel() : SpellScriptLoader("spell_pri_legion_archangel") { }

        class spell_pri_legion_archangel_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_archangel_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_ATONEMENT_BUFF
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_PRIEST_ATONEMENT_BUFF, GetCaster()->GetGUID()));
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                if (Aura* aura = GetHitUnit()->GetAura(SPELL_PRIEST_ATONEMENT_BUFF, GetCaster()->GetGUID()))
                    aura->RefreshDuration();
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_archangel_SpellScript::HandleScriptEffect, EFFECT_2, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_archangel_SpellScript();
        }
};

class DelayedPleaAuraCheck : public BasicEvent
{
    public:
        DelayedPleaAuraCheck(Unit* owner) : _owner(owner) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            if (Aura* aura = _owner->GetAura(SPELL_PRIEST_PLEA_MANA_COST_BUFF))
            {
                uint32 stackCount = _owner->GetTargetAuraApplications(SPELL_PRIEST_ATONEMENT_BUFF).size();
                if (stackCount > 0)
                    stackCount++; // one stack base costs + one stack per player
                aura->SetStackAmount(std::max(uint8(1), uint8(stackCount)));
            }
            return true;
        }

    private:
        Unit* _owner;
};

// 81749 - Atonement
// 195178 - Atonement (Shadowfiend aura)
class spell_pri_legion_atonement : public SpellScriptLoader
{
    public:
        spell_pri_legion_atonement() : SpellScriptLoader("spell_pri_legion_atonement") { }

        class spell_pri_legion_atonement_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_atonement_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_ATONEMENT_BUFF,
                    SPELL_PRIEST_ATONEMENT_HEAL_CAN_CRIT,
                    SPELL_PRIEST_ATONEMENT_HEAL_CANT_CRIT,
                    SPELL_PRIEST_BARRIER_FOR_THE_DEVOTED,
                    SPELL_PRIEST_POWER_WORD_BARRIER_AURA
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                Unit* caster = GetTarget()->IsSummon() ? GetTarget()->GetCharmerOrOwner() : GetTarget();
                if (!caster)
                    return;

                int32 heal = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
                int32 barrierBonus = 0;

                if (AuraEffect* barrierForTheDevoted = caster->GetAuraEffect(SPELL_PRIEST_BARRIER_FOR_THE_DEVOTED, EFFECT_0))
                    barrierBonus = barrierForTheDevoted->GetAmount();

                for (AuraApplication* aurApp : caster->GetTargetAuraApplications(SPELL_PRIEST_ATONEMENT_BUFF))
                {
                    if (Unit* target = aurApp->GetTarget())
                    {
                        int32 tempHeal = heal;
                        if (target->HasAura(SPELL_PRIEST_POWER_WORD_BARRIER_AURA, caster->GetGUID()))
                            AddPct(tempHeal, barrierBonus);

                        if (eventInfo.GetHitMask() & PROC_HIT_CRITICAL)
                            caster->CastCustomSpell(SPELL_PRIEST_ATONEMENT_HEAL_CANT_CRIT, SPELLVALUE_BASE_POINT0, tempHeal, target, TRIGGERED_FULL_MASK);
                        else
                            caster->CastCustomSpell(SPELL_PRIEST_ATONEMENT_HEAL_CAN_CRIT, SPELLVALUE_BASE_POINT0, tempHeal, target, TRIGGERED_FULL_MASK);
                    }
                }
            }

            void HandleApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                // Login case (delay is needed because not all auras are added yet)
                // we need to check plea mana cost buff if values are still valid
                GetTarget()->m_Events.AddEvent(new DelayedPleaAuraCheck(GetTarget()), 100);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_atonement_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_atonement_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_atonement_AuraScript::HandleApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_atonement_AuraScript();
        }
};

// 194384 - Atonement
class spell_pri_legion_atonement_buff : public SpellScriptLoader
{
    public:
        spell_pri_legion_atonement_buff() : SpellScriptLoader("spell_pri_legion_atonement_buff") { }

        class spell_pri_legion_atonement_buff_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_atonement_buff_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PLEA_MANA_COST_BUFF,
                    SPELL_PRIEST_BORROWED_TIME,
                    SPELL_PRIEST_SINS_OF_THE_MANY,
                    SPELL_PRIEST_SINS_OF_THE_MANY_AURA,
                    SPELL_PRIEST_TRINITY_HONOR_TALENT
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_PRIEST_PLEA_MANA_COST_BUFF))
                        caster->CastSpell(caster, SPELL_PRIEST_PLEA_MANA_COST_BUFF, true);
                    else
                        caster->CastCustomSpell(SPELL_PRIEST_PLEA_MANA_COST_BUFF, SPELLVALUE_AURA_STACK, 2, caster, TRIGGERED_FULL_MASK);

                    if (AuraEffect* borrowedTime = caster->GetAuraEffect(SPELL_PRIEST_BORROWED_TIME, EFFECT_0))
                    {
                        CustomSpellValues values;
                        values.AddSpellMod(SPELLVALUE_BASE_POINT0, borrowedTime->GetAmount());
                        values.AddSpellMod(SPELLVALUE_BASE_POINT1, borrowedTime->GetAmount());
                        values.AddSpellMod(SPELLVALUE_BASE_POINT2, borrowedTime->GetAmount());
                        values.AddSpellMod(SPELLVALUE_BASE_POINT3, borrowedTime->GetAmount());
                        caster->CastCustomSpell(SPELL_PRIEST_BORROWED_TIME_AURA, values, caster, TRIGGERED_FULL_MASK);
                    }

                    if (caster->HasAura(SPELL_PRIEST_SINS_OF_THE_MANY))
                        caster->CastSpell(caster, SPELL_PRIEST_SINS_OF_THE_MANY_AURA, true);
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->RemoveAuraFromStack(SPELL_PRIEST_PLEA_MANA_COST_BUFF);
                    caster->RemoveAuraFromStack(SPELL_PRIEST_SINS_OF_THE_MANY_AURA);
                }
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_pri_legion_atonement_buff_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_atonement_buff_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_atonement_buff_AuraScript();
        }
};

// 17 - Power Word: Shield
// 186263 - Shadow Mend
// 194509 - Power Word: Radiance
// 200829 - Plea
class spell_pri_legion_atonement_helper : public SpellScriptLoader
{
    public:
        spell_pri_legion_atonement_helper() : SpellScriptLoader("spell_pri_legion_atonement_helper") { }

        class spell_pri_legion_atonement_helper_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_atonement_helper_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_ATONEMENT_BUFF,
                    SPELL_PRIEST_ATONEMENT,
                    SPELL_PRIEST_PLEA
                });
            }

            bool Load() override
            {
                if (GetCaster()->HasAura(SPELL_PRIEST_TRINITY_HONOR_TALENT) && GetSpellInfo()->Id != SPELL_PRIEST_PLEA)
                    return false;
                return GetCaster()->HasAura(SPELL_PRIEST_ATONEMENT);
            }

            void HandleAfterCast()
            {
                if (!GetHitUnit())
                    return;

                if (AuraEffect* trinity = GetCaster()->GetAuraEffect(SPELL_PRIEST_TRINITY_HONOR_TALENT, EFFECT_0))
                    if (int32(GetCaster()->GetTargetAuraApplications(SPELL_PRIEST_ATONEMENT_BUFF).size()) >= trinity->GetAmount())
                        return;

                if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_PRIEST_ATONEMENT_BUFF)->GetEffect(EFFECT_2))
                {
                    int32 duration = effInfo->CalcValue(GetCaster()) * 1000;
                    if (GetCaster()->HasSpell(SPELL_PRIEST_TRINITY_HONOR_TALENT))
                        if (SpellEffectInfo const* honorEffInfo = sSpellMgr->AssertSpellInfo(SPELL_PRIEST_TRINITY_HONOR_TALENT)->GetEffect(EFFECT_2))
                            duration += honorEffInfo->CalcValue(GetCaster()) * 1000;

                    GetCaster()->CastCustomSpell(SPELL_PRIEST_ATONEMENT_BUFF, SPELLVALUE_AURA_DURATION, duration, GetHitUnit(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_pri_legion_atonement_helper_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_atonement_helper_SpellScript();
        }
};

// 32546 - Binding Heal
class spell_pri_legion_binding_heal : public SpellScriptLoader
{
    public:
        spell_pri_legion_binding_heal() : SpellScriptLoader("spell_pri_legion_binding_heal") { }

        class spell_pri_legion_binding_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_binding_heal_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetExplTargetUnit());
                targets.remove(GetCaster());
                targets.sort(Trinity::HealthPctOrderPred());
                if (targets.size() > 1)
                    targets.resize(1);
                targets.push_back(GetExplTargetUnit());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_binding_heal_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_binding_heal_SpellScript();
        }
};

// 193371 - Call to the Void
class spell_pri_legion_call_of_the_void : public SpellScriptLoader
{
    public:
        spell_pri_legion_call_of_the_void() : SpellScriptLoader("spell_pri_legion_call_of_the_void") { }

        class spell_pri_legion_call_of_the_void_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_call_of_the_void_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_CALL_OF_THE_VOID_SUMMON
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_CALL_OF_THE_VOID_SUMMON, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_call_of_the_void_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_call_of_the_void_AuraScript();
        }
};

// 204883 - Circle of Healing
class spell_pri_legion_circle_of_healing : public SpellScriptLoader
{
    public:
        spell_pri_legion_circle_of_healing() : SpellScriptLoader("spell_pri_legion_circle_of_healing") { }

        class spell_pri_legion_circle_of_healing_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_circle_of_healing_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;
                return true;
            }

            void SelectTarget(std::list<WorldObject*>& targets)
            {
                uint32 maxTargets = GetEffectInfo(EFFECT_1)->CalcValue(GetCaster());
                targets.sort(Trinity::HealthPctOrderPred());
                if (targets.size() > maxTargets)
                    targets.resize(maxTargets);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_circle_of_healing_SpellScript::SelectTarget, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_circle_of_healing_SpellScript();
        }
};

// 19236 - Desperate Prayer
class spell_pri_legion_desperate_prayer : public SpellScriptLoader
{
    public:
        spell_pri_legion_desperate_prayer() : SpellScriptLoader("spell_pri_legion_desperate_prayer") { }

        class spell_pri_legion_desperate_prayer_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_desperate_prayer_AuraScript);

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (AuraEffect* aurEff0 = GetEffect(EFFECT_0))
                    aurEff0->ChangeAmount(aurEff0->GetAmount() - aurEff->GetAmount());
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_desperate_prayer_AuraScript::HandleDummyTick, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_desperate_prayer_AuraScript();
        }
};

// 152118 - Clarity of Will
class spell_pri_legion_clarity_of_will : public SpellScriptLoader
{
    public:
        spell_pri_legion_clarity_of_will() : SpellScriptLoader("spell_pri_legion_clarity_of_will") { }

        class spell_pri_legion_clarity_of_will_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_clarity_of_will_AuraScript);

            bool Load() override
            {
                Unit* caster = GetCaster();
                return caster && caster->GetTypeId() == TYPEID_PLAYER;
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                if (Unit* caster = GetCaster())
                {
                    int32 baseamount = int32(9.0f * caster->SpellBaseHealingBonusDone(GetSpellInfo()->GetSchoolMask()));
                    AddPct(baseamount, GetCaster()->ToPlayer()->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE));

                    if (AuraEffect* grace = caster->GetAuraEffect(SPELL_PRIEST_GRACE, EFFECT_0))
                        AddPct(baseamount, grace->GetAmount());

                    Unit::AuraEffectList const& absorbBonus = caster->GetAuraEffectsByType(SPELL_AURA_MOD_ABSORB_EFFECTS_AMOUNT_PCT);
                    for (auto bonus : absorbBonus)
                        AddPct(baseamount, bonus->GetAmount());

                    amount = std::min((baseamount + aurEff->GetAmount()), (baseamount * 2));
                }
            }

            void HandleApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                // small exploit fix
                // if caster unlearns clarity of will during the cast the aura gets a amount overflow
                if (Unit* caster = GetCaster())
                    if (!GetCaster()->HasSpell(GetId()))
                        for (auto itr : caster->GetTargetAuraApplications(GetId()))
                            itr->GetBase()->Remove();
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_clarity_of_will_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                AfterEffectApply += AuraEffectApplyFn(spell_pri_legion_clarity_of_will_AuraScript::HandleApplyEffect, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_clarity_of_will_AuraScript();
        }
};

// 197871 - Dark Archangel
class spell_pri_legion_dark_archangel : public SpellScriptLoader
{
    public:
        spell_pri_legion_dark_archangel() : SpellScriptLoader("spell_pri_legion_dark_archangel") { }

        class spell_pri_legion_dark_archangel_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_dark_archangel_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_DARK_ARCHANGEL_BUFF
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetCaster());
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_PRIEST_ATONEMENT_BUFF, GetCaster()->GetGUID()));
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_DARK_ARCHANGEL_BUFF, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_dark_archangel_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_dark_archangel_SpellScript::HandleScriptEffect, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_dark_archangel_SpellScript();
        }
};

// 64844 - Divine Hymn
class spell_pri_legion_divine_hymn : public SpellScriptLoader
{
    public:
        spell_pri_legion_divine_hymn() : SpellScriptLoader("spell_pri_legion_divine_hymn") { }

        class spell_pri_legion_divine_hymn_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_divine_hymn_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_DIVINE_HYMN
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                Group* grp = GetCaster()->ToPlayer()->GetGroup();

                if (!grp || !grp->isRaidGroup())
                {
                    if (AuraEffect const* pctBonus = GetCaster()->GetAuraEffect(SPELL_PRIEST_DIVINE_HYMN, EFFECT_1))
                    {
                        int32 heal = GetHitHeal();
                        AddPct(heal, pctBonus->GetAmount());
                        SetHitHeal(heal);
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_divine_hymn_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_divine_hymn_SpellScript();
        }
};

// 47585 - Dispersion
class spell_pri_legion_dispersion : public SpellScriptLoader
{
    public:
        spell_pri_legion_dispersion() : SpellScriptLoader("spell_pri_legion_dispersion") { }

        class spell_pri_legion_dispersion_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_dispersion_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_THRIVE_IN_THE_SHADOWS,
                    SPELL_PRIEST_THRIVE_IN_THE_SHADOWS_HEAL
                });
            }

            void HandleApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetTarget())
                {
                    // Disable Energy drain if dispersion is active
                    if (caster->GetTypeId() == TYPEID_PLAYER)
                    {
                        caster->ToPlayer()->SetFloatValue(UNIT_FIELD_POWER_REGEN_FLAT_MODIFIER + caster->GetPowerIndex(POWER_INSANITY), 0);
                        caster->ToPlayer()->SetFloatValue(UNIT_FIELD_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + caster->GetPowerIndex(POWER_INSANITY), 0);
                    }

                    if (AuraEffect* thriveInTheShadows = caster->GetAuraEffect(SPELL_PRIEST_THRIVE_IN_THE_SHADOWS, EFFECT_0))
                    {
                        int32 heal = CalculatePct(caster->GetMaxHealth(), thriveInTheShadows->GetAmount()) / sSpellMgr->AssertSpellInfo(SPELL_PRIEST_THRIVE_IN_THE_SHADOWS_HEAL)->GetMaxTicks(DIFFICULTY_NONE);
                        caster->CastCustomSpell(SPELL_PRIEST_THRIVE_IN_THE_SHADOWS_HEAL, SPELLVALUE_BASE_POINT0, heal, caster, TRIGGERED_FULL_MASK);
                    }
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetTarget())
                {
                    if (caster->GetTypeId() == TYPEID_PLAYER)
                    {
                        if (caster->HasAura(SPELL_PRIEST_PURE_SHADOW_HONOR_TALENT))
                            caster->CastSpell(caster, SPELL_PRIEST_SUSTAINED_SANITY, true);
                        else
                        {
                            float totalValue = 0.f;
                            Unit::AuraEffectList const& auras = caster->GetAuraEffectsByType(SPELL_AURA_MOD_POWER_REGEN);
                            for (Unit::AuraEffectList::const_iterator i = auras.begin(); i != auras.end(); ++i)
                                if ((*i)->GetMiscValue() == POWER_INSANITY)
                                    totalValue += (*i)->GetAmount();

                            totalValue = (totalValue / 500) * 100;
                            caster->SetFloatValue(UNIT_FIELD_POWER_REGEN_FLAT_MODIFIER + caster->GetPowerIndex(POWER_INSANITY), totalValue);
                            caster->SetFloatValue(UNIT_FIELD_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + caster->GetPowerIndex(POWER_INSANITY), totalValue);
                        }
                    }
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_dispersion_AuraScript::HandleApplyEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_dispersion_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_dispersion_AuraScript();
        }
};

// 47788 - Guardian Spirit
class spell_pri_legion_guardian_spirit : public SpellScriptLoader
{
    public:
        spell_pri_legion_guardian_spirit() : SpellScriptLoader("spell_pri_legion_guardian_spirit") { }

        class spell_pri_legion_guardian_spirit_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_guardian_spirit_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_GUARDIAN_SPIRIT_HEAL,
                    SPELL_PRIEST_GUARDIAN_ANGEL
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32 & absorbAmount)
            {
                Unit* target = GetTarget();
                if (dmgInfo.GetDamage() < target->GetHealth() || dmgInfo.GetDamage() >= uint32(aurEff->GetBaseAmount()))
                    return;

                int32 healAmount = int32(target->CountPctFromMaxHealth(sSpellMgr->AssertSpellInfo(SPELL_PRIEST_GUARDIAN_SPIRIT_HEAL)->GetEffect(EFFECT_0)->CalcValue()));
                // remove the aura now, we don't want 40% healing bonus
                Remove(AURA_REMOVE_BY_ENEMY_SPELL);
                target->CastCustomSpell(SPELL_PRIEST_GUARDIAN_SPIRIT_HEAL, SPELLVALUE_BASE_POINT0, healAmount, target, TRIGGERED_FULL_MASK, nullptr, aurEff, GetCasterGUID());
                absorbAmount = dmgInfo.GetDamage();
            }

            void HandleApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->GetGUID() == GetCasterGUID())
                    return;

                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_PRIEST_GUARDIANS_OF_THE_LIGHT))
                        caster->AddAura(GetId(), caster);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_EXPIRE)
                    if (Unit* caster = GetCaster())
                        if (AuraEffect* guardianAngel = caster->GetAuraEffect(SPELL_PRIEST_GUARDIAN_ANGEL, EFFECT_0))
                            caster->GetSpellHistory()->ModifyCooldown(GetId(), ((guardianAngel->GetAmount() * 1000) - caster->GetSpellHistory()->GetRemainingCooldown(GetSpellInfo())));
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_guardian_spirit_AuraScript::CalculateAmount, EFFECT_2, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pri_legion_guardian_spirit_AuraScript::Absorb, EFFECT_2);
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_guardian_spirit_AuraScript::HandleApplyEffect, EFFECT_2, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_guardian_spirit_AuraScript::RemoveEffect, EFFECT_2, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_guardian_spirit_AuraScript();
        }
};

// 200153 - Enduring Renewal
class spell_pri_legion_enduring_renewal : public SpellScriptLoader
{
    public:
        spell_pri_legion_enduring_renewal() : SpellScriptLoader("spell_pri_legion_enduring_renewal") { }

        class spell_pri_legion_enduring_renewal_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_enduring_renewal_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_RENEW,
                    SPELL_PRIEST_HALO_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // Only direct heal can trigger this aura
                if (!eventInfo.GetActionTarget() || eventInfo.GetSpellInfo()->IsTargetingArea(DIFFICULTY_NONE) || (!eventInfo.GetSpellInfo()->HasEffect(SPELL_EFFECT_HEAL) && !eventInfo.GetSpellInfo()->HasEffect(SPELL_EFFECT_HEAL_PCT)))
                    return false;

                if (eventInfo.GetSpellInfo()->Id == SPELL_PRIEST_HALO_HEAL)
                    return false;

                return true;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                if (Aura* renew = eventInfo.GetActionTarget()->GetAura(SPELL_PRIEST_RENEW, GetCasterGUID()))
                    renew->RefreshDuration();
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_enduring_renewal_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_enduring_renewal_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_enduring_renewal_AuraScript();
        }
};

// 586 - Fade
class spell_pri_legion_fade : public SpellScriptLoader
{
    public:
        spell_pri_legion_fade() : SpellScriptLoader("spell_pri_legion_fade") { }

        class spell_pri_legion_fade_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_fade_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_VESTMENTS_OF_DISCIPLINE,
                    SPELL_PRIEST_VESTMENTS_OF_DISCIPLINE_AURA
                });
            }

            void HandleApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (AuraEffect* vestmentsOfDiscipline = caster->GetAuraEffect(SPELL_PRIEST_VESTMENTS_OF_DISCIPLINE, EFFECT_0))
                        caster->CastCustomSpell(SPELL_PRIEST_VESTMENTS_OF_DISCIPLINE_AURA, SPELLVALUE_BASE_POINT0, vestmentsOfDiscipline->GetAmount(), caster, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_fade_AuraScript::HandleApplyEffect, EFFECT_0, SPELL_AURA_MOD_TOTAL_THREAT, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_fade_AuraScript();
        }
};

// 2061 - Flash Heal
/// @TODO: new proc system
class spell_pri_legion_flash_heal : public SpellScriptLoader
{
    public:
        spell_pri_legion_flash_heal() : SpellScriptLoader("spell_pri_legion_flash_heal") { }

        class spell_pri_legion_flash_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_flash_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SURGE_OF_LIGHT_TRIGGERED
                });
            }

            void HandleEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->RemoveAuraFromStack(SPELL_PRIEST_SURGE_OF_LIGHT_TRIGGERED);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_flash_heal_SpellScript::HandleEffectHit, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_flash_heal_SpellScript();
        }
};

// 196419 - Focus in the Light
class spell_pri_legion_focus_in_the_light : public SpellScriptLoader
{
    public:
        spell_pri_legion_focus_in_the_light() : SpellScriptLoader("spell_pri_legion_focus_in_the_light") { }

        class spell_pri_legion_focus_in_the_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_focus_in_the_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_FOCUS_IN_THE_LIGHT_ENEMY,
                    SPELL_PRIEST_FOCUS_IN_THE_LIGHT_CASTER,
                    SPELL_PRIEST_FOCUSED_WILL
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return GetTarget()->HasAura(SPELL_PRIEST_FOCUSED_WILL, GetCasterGUID());
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_FOCUS_IN_THE_LIGHT_ENEMY, true);
                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_FOCUS_IN_THE_LIGHT_CASTER, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_focus_in_the_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_focus_in_the_light_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_focus_in_the_light_AuraScript();
        }
};

// 231687 - Holy Fire
class spell_pri_legion_holy_fire_passive : public SpellScriptLoader
{
    public:
        spell_pri_legion_holy_fire_passive() : SpellScriptLoader("spell_pri_legion_holy_fire_passive") { }

        class spell_pri_legion_holy_fire_passive_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_holy_fire_passive_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_HOLY_FIRE,
                    SPELL_PRIEST_HOLY_FIRE_EVENT_MARKER
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_PRIEST_HOLY_FIRE, true);
                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_HOLY_FIRE_EVENT_MARKER, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_holy_fire_passive_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_holy_fire_passive_AuraScript();
        }
};

// 88625 - Holy Word: Chastise
class spell_pri_legion_holy_word_chastise : public SpellScriptLoader
{
    public:
        spell_pri_legion_holy_word_chastise() : SpellScriptLoader("spell_pri_legion_holy_word_chastise") { }

        class spell_pri_legion_holy_word_chastise_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_holy_word_chastise_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_CENSURE,
                    SPELL_PRIEST_HOLY_WORD_CHASTISE_STUN,
                    SPELL_PRIEST_HOLY_WORD_CHASTISE_DISORIENT
                });
            }

            void HandleAfterHit()
            {
                if (GetCaster()->HasAura(SPELL_PRIEST_CENSURE))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_HOLY_WORD_CHASTISE_STUN, true);
                else
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_HOLY_WORD_CHASTISE_DISORIENT, true);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_pri_legion_holy_word_chastise_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_holy_word_chastise_SpellScript();
        }
};

// 34861 - Holy Word: Sanctify
class spell_pri_legion_holy_word_sanctify : public SpellScriptLoader
{
    public:
        spell_pri_legion_holy_word_sanctify() : SpellScriptLoader("spell_pri_legion_holy_word_sanctify") { }

        class spell_pri_legion_holy_word_sanctify_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_holy_word_sanctify_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;
                return true;
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.sort(Trinity::HealthPctOrderPred());
                uint32 maxTargets = uint32(GetEffectInfo(EFFECT_1)->CalcValue(GetCaster()));
                if (targets.size() > maxTargets)
                    targets.resize(maxTargets);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_holy_word_sanctify_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_holy_word_sanctify_SpellScript();
        }
};

// 196684 - Invoke the Naaru
class spell_pri_legion_invoke_the_naaru_summon_proc : public SpellScriptLoader
{
    public:
        spell_pri_legion_invoke_the_naaru_summon_proc() : SpellScriptLoader("spell_pri_legion_invoke_the_naaru_summon_proc") { }

        class spell_pri_legion_invoke_the_naaru_summon_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_invoke_the_naaru_summon_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_INVOKE_THE_NAARU_SUMMON
                });
            }

            ///@TODO: new proc system
            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return !GetTarget()->GetSpellHistory()->HasCooldown(GetId());
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_INVOKE_THE_NAARU_SUMMON, true);

                ///@TODO: new proc system
                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::seconds(15));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_invoke_the_naaru_summon_proc_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_invoke_the_naaru_summon_proc_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_invoke_the_naaru_summon_proc_AuraScript();
        }
};

// 196705 - Invoke the Naaru
class spell_pri_legion_invoke_the_naaru_copy_proc : public SpellScriptLoader
{
    public:
        spell_pri_legion_invoke_the_naaru_copy_proc() : SpellScriptLoader("spell_pri_legion_invoke_the_naaru_copy_proc") { }

        class spell_pri_legion_invoke_the_naaru_copy_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_invoke_the_naaru_copy_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_RENEW,
                    SPELL_PRIEST_HEAL,
                    SPELL_PRIEST_FLASH_HEAL,
                    SPELL_PRIEST_HOLY_WORD_SERENITY,
                    SPELL_PRIEST_PRAYER_OF_HEALING,
                    SPELL_PRIEST_HOLY_WORD_SANCTIFY,
                    SPELL_PRIEST_PRAYER_OF_MENDING,
                    SPELL_PRIEST_DIVINE_HYMN,
                    SPELL_PRIEST_SMITE,
                    SPELL_PRIEST_HOLY_FIRE,
                    SPELL_PRIEST_HOLY_WORD_CHATISE,
                    SPELL_PRIEST_HOLY_NOVA,
                    SPELL_PRIEST_TRANQUIL_LIGHT,
                    SPELL_PRIEST_HEALING_LIGHT,
                    SPELL_PRIEST_DAZZING_LIGHTS,
                    SPELL_PRIEST_BLESSED_LIGHT,
                    SPELL_PRIEST_SEARING_LIGHT,
                    SPELL_PRIEST_LIGHT_ERUPTION
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                Unit* caster = nullptr;
                for (Unit* minion : GetTarget()->m_Controlled)
                {
                    if (minion->GetEntry() == NPC_IMAGE_OF_TUURE)
                    {
                        caster = minion;
                        break;
                    }
                }

                if (!caster)
                    return;

                switch (eventInfo.GetSpellInfo()->Id)
                {
                    case SPELL_PRIEST_RENEW:
                        caster->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_TRANQUIL_LIGHT, true, nullptr, nullptr, GetCasterGUID());
                        break;
                    case SPELL_PRIEST_HEAL:
                    case SPELL_PRIEST_FLASH_HEAL:
                    case SPELL_PRIEST_HOLY_WORD_SERENITY:
                        caster->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_HEALING_LIGHT, true, nullptr, nullptr, GetCasterGUID());
                        break;
                    case SPELL_PRIEST_PRAYER_OF_HEALING:
                    case SPELL_PRIEST_HOLY_WORD_SANCTIFY:
                        caster->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_DAZZING_LIGHTS, true, nullptr, nullptr, GetCasterGUID());
                        break;
                    case SPELL_PRIEST_PRAYER_OF_MENDING:
                    case SPELL_PRIEST_DIVINE_HYMN:
                        caster->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_BLESSED_LIGHT, true, nullptr, nullptr, GetCasterGUID());
                        break;
                    case SPELL_PRIEST_SMITE:
                    case SPELL_PRIEST_HOLY_FIRE:
                    case SPELL_PRIEST_HOLY_WORD_CHATISE:
                        caster->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_SEARING_LIGHT, true, nullptr, nullptr, GetCasterGUID());
                        break;
                    case SPELL_PRIEST_HOLY_NOVA:
                        caster->CastSpell(GetTarget(), SPELL_PRIEST_LIGHT_ERUPTION, true, nullptr, nullptr, GetCasterGUID());
                        break;
                    default:
                        break;
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_invoke_the_naaru_copy_proc_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_invoke_the_naaru_copy_proc_AuraScript();
        }
};

// 215776 - Last Word
class spell_pri_legion_last_word : public SpellScriptLoader
{
    public:
        spell_pri_legion_last_word() : SpellScriptLoader("spell_pri_legion_last_word") { }

        class spell_pri_legion_last_word_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_last_word_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SILENCE
                });
            }

            void HandleProc(ProcEventInfo& procInfo)
            {
                GetTarget()->CastSpell(procInfo.GetActionTarget(), SPELL_PRIEST_SILENCE, true);
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_pri_legion_last_word_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_last_word_AuraScript();
        }
};

// 73325 - Leap of Faith
class spell_pri_legion_leap_of_faith : public SpellScriptLoader
{
    public:
        spell_pri_legion_leap_of_faith() : SpellScriptLoader("spell_pri_legion_leap_of_faith") { }

        class spell_pri_legion_leap_of_faith_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_leap_of_faith_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_LEAP_OF_FAITH_EFFECT_TRIGGER,
                    SPELL_PRIEST_TRUST_IN_THE_LIGHT,
                    SPELL_PRIEST_TRUST_IN_THE_LIGHT_PERIODIC,
                    SPELL_PRIEST_DELIVERED_FROM_EVIL
                });
            }

            void HandleEffectDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_LEAP_OF_FAITH_EFFECT_TRIGGER, true);

                if (AuraEffect* trustInTheLight = GetCaster()->GetAuraEffect(SPELL_PRIEST_TRUST_IN_THE_LIGHT, EFFECT_0))
                {
                    int32 heal = GetHitUnit()->CountPctFromMaxHealth(trustInTheLight->GetAmount()) / sSpellMgr->AssertSpellInfo(SPELL_PRIEST_TRUST_IN_THE_LIGHT_PERIODIC)->GetMaxTicks(DIFFICULTY_NONE);
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_TRUST_IN_THE_LIGHT_PERIODIC, SPELLVALUE_BASE_POINT0, heal, GetHitUnit(), TRIGGERED_FULL_MASK);
                    if (GetCaster()->HasAura(SPELL_PRIEST_DELIVERED_FROM_EVIL))
                        GetHitUnit()->RemoveMovementImpairingAuras();
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_leap_of_faith_SpellScript::HandleEffectDummy, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_leap_of_faith_SpellScript();
        }

        class spell_pri_legion_leap_of_faith_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_leap_of_faith_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_BODY_AND_SOUL,
                    SPELL_PRIEST_BODY_AND_SOUL_SPEED_BUFF
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_PRIEST_BODY_AND_SOUL))
                        caster->CastSpell(GetTarget(), SPELL_PRIEST_BODY_AND_SOUL_SPEED_BUFF, true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_leap_of_faith_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_leap_of_faith_AuraScript();
        }
};

// 92833 - Leap of Faith
class spell_pri_legion_leap_of_faith_effect_trigger : public SpellScriptLoader
{
    public:
        spell_pri_legion_leap_of_faith_effect_trigger() : SpellScriptLoader("spell_pri_legion_leap_of_faith_effect_trigger") { }

        class spell_pri_legion_leap_of_faith_effect_trigger_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_leap_of_faith_effect_trigger_SpellScript);

            void HandleEffectDummy(SpellEffIndex /*effIndex*/)
            {
                SpellCastTargets targets;
                targets.SetDst(GetCaster()->GetPosition());
                targets.SetUnitTarget(GetCaster());
                GetHitUnit()->CastSpell(targets, sSpellMgr->GetSpellInfo(GetEffectValue()), nullptr);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_leap_of_faith_effect_trigger_SpellScript::HandleEffectDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_leap_of_faith_effect_trigger_SpellScript();
        }
};

// 1706 - Levitate
class spell_pri_legion_levitate : public SpellScriptLoader
{
    public:
        spell_pri_legion_levitate() : SpellScriptLoader("spell_pri_legion_levitate") { }

        class spell_pri_legion_levitate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_levitate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_LEVITATE_TRIGGERED
                });
            }

            void HandleEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_LEVITATE_TRIGGERED, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_levitate_SpellScript::HandleEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_levitate_SpellScript();
        }
};

// 207946 - Light's Wrath
class spell_pri_legion_light_s_wrath : public SpellScriptLoader
{
    public:
        spell_pri_legion_light_s_wrath() : SpellScriptLoader("spell_pri_legion_light_s_wrath") { }

        class spell_pri_legion_light_s_wrath_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_light_s_wrath_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_ATONEMENT_BUFF
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                int32 atonementAurasActive = GetCaster()->GetTargetAuraApplications(SPELL_PRIEST_ATONEMENT_BUFF).size();
                if (atonementAurasActive == 0)
                    return;

                int32 damage = GetHitDamage();
                AddPct(damage, (GetEffectInfo(EFFECT_1)->CalcValue(GetCaster()) * atonementAurasActive));
                SetHitDamage(damage);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_light_s_wrath_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_light_s_wrath_SpellScript();
        }
};

// 197937 Lingering Insanity
class spell_pri_legion_lingering_insanity : public SpellScriptLoader
{
    public:
        spell_pri_legion_lingering_insanity() : SpellScriptLoader("spell_pri_legion_lingering_insanity") { }

        class spell_pri_legion_lingering_insanity_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_lingering_insanity_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_LINGERING_INSANITY
                });
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                if (SpellEffectInfo const* stackInfo = sSpellMgr->GetSpellInfo(SPELL_PRIEST_LINGERING_INSANITY)->GetEffect(EFFECT_0))
                    GetAura()->ModStackAmount(-stackInfo->CalcValue(GetTarget()));
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_lingering_insanity_AuraScript::HandleDummyTick, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_lingering_insanity_AuraScript();
        }
};

// 123050 - Mana Leech
class spell_pri_legion_mana_leech_proc : public SpellScriptLoader
{
    public:
        spell_pri_legion_mana_leech_proc() : SpellScriptLoader("spell_pri_legion_mana_leech_proc") { }

        class spell_pri_legion_mana_leech_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_mana_leech_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_MANA_LEECH,
                    SPELL_GENERIC_POWER_LEECH
                });
            }

            void HandleProc(ProcEventInfo& /*procInfo*/)
            {
                if (Unit* target = GetTarget()->GetCharmerOrOwner())
                {
                    if (target->ToPlayer()->GetRole() == TALENT_ROLE_HEALER)
                        GetTarget()->CastSpell(target, SPELL_PRIEST_MANA_LEECH, true);
                    else
                        GetTarget()->CastSpell(target, SPELL_GENERIC_POWER_LEECH, true);
                }
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_pri_legion_mana_leech_proc_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_mana_leech_proc_AuraScript();
        }
};

// 193173 - Mania
class spell_pri_legion_mania : public SpellScriptLoader
{
    public:
        spell_pri_legion_mania() : SpellScriptLoader("spell_pri_legion_mania") { }

        class spell_pri_legion_mania_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_mania_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_MANIA_SPEED_BUFF
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_MANIA_SPEED_BUFF);
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 500;
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                int32 speedValue = floor(GetTarget()->GetPower(POWER_INSANITY) / (aurEff->GetAmount() * 100));
                if (AuraEffect* mania = GetTarget()->GetAuraEffect(SPELL_PRIEST_MANIA_SPEED_BUFF, EFFECT_0))
                {
                    if (speedValue != mania->GetAmount())
                        GetTarget()->CastCustomSpell(SPELL_PRIEST_MANIA_SPEED_BUFF, SPELLVALUE_BASE_POINT0, speedValue, GetTarget(), TRIGGERED_FULL_MASK);
                }
                else
                    GetTarget()->CastCustomSpell(SPELL_PRIEST_MANIA_SPEED_BUFF, SPELLVALUE_BASE_POINT0, speedValue, GetTarget(), TRIGGERED_FULL_MASK);
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetUnitOwner());
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_mania_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_pri_legion_mania_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_mania_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_pri_legion_mania_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_mania_AuraScript();
        }
};

// 193065 - Masochism
class spell_pri_legion_masochism_heal : public SpellScriptLoader
{
    public:
        spell_pri_legion_masochism_heal() : SpellScriptLoader("spell_pri_legion_masochism_heal") { }

        class spell_pri_legion_masochism_heal_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_masochism_heal_AuraScript);

            void FixTooltip(AuraEffect const* aurEff, int32& amount, bool& /*canBeRecalculated*/)
            {
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_masochism_heal_AuraScript::FixTooltip, EFFECT_1, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_masochism_heal_AuraScript();
        }
};

// 77485 - Mastery: Echo of Light
class spell_pri_legion_mastery_echo_of_light : public SpellScriptLoader
{
    public:
        spell_pri_legion_mastery_echo_of_light() : SpellScriptLoader("spell_pri_legion_mastery_echo_of_light") { }

        class spell_pri_legion_mastery_echo_of_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_mastery_echo_of_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_ECHO_OF_LIGHT_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo() || !eventInfo.GetHealInfo() || eventInfo.GetHealInfo()->GetHeal() == 0 || !eventInfo.GetActionTarget())
                    return false;

                // Only direct heal can trigger this aura
                if (eventInfo.GetSpellInfo()->IsTargetingArea(DIFFICULTY_NONE) || (!eventInfo.GetSpellInfo()->HasEffect(SPELL_EFFECT_HEAL) && !eventInfo.GetSpellInfo()->HasEffect(SPELL_EFFECT_HEAL_PCT)))
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 heal = CalculatePct(eventInfo.GetHealInfo()->GetHeal(), aurEff->GetAmount());
                heal /= sSpellMgr->AssertSpellInfo(SPELL_PRIEST_ECHO_OF_LIGHT_HEAL)->GetMaxTicks(DIFFICULTY_NONE);
                heal += int32(eventInfo.GetActionTarget()->GetRemainingPeriodicAmount(GetCasterGUID(), SPELL_PRIEST_ECHO_OF_LIGHT_HEAL, SPELL_AURA_PERIODIC_HEAL, EFFECT_0));
                GetTarget()->CastCustomSpell(SPELL_PRIEST_ECHO_OF_LIGHT_HEAL, SPELLVALUE_BASE_POINT0, heal, eventInfo.GetActionTarget(), TRIGGERED_FULL_MASK, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_mastery_echo_of_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_mastery_echo_of_light_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_mastery_echo_of_light_AuraScript();
        }
};

// 194018 - Mental Fortitude
class spell_pri_legion_mental_fortitude : public SpellScriptLoader
{
    public:
        spell_pri_legion_mental_fortitude() : SpellScriptLoader("spell_pri_legion_mental_fortitude") { }

        class spell_pri_legion_mental_fortitude_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_mental_fortitude_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_MENTAL_FORTITUDE_SHIELD
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr && GetEffect(EFFECT_0)->GetAmount() == 1;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                int32 shieldAmount = eventInfo.GetDamageInfo()->GetDamage() / 2;
                if (AuraEffect* shield = GetTarget()->GetAuraEffect(SPELL_PRIEST_MENTAL_FORTITUDE_SHIELD, EFFECT_0))
                    shieldAmount += shield->GetAmount();
                shieldAmount = std::min(shieldAmount, int32(GetTarget()->GetMaxHealth() * 0.08f));
                GetTarget()->CastCustomSpell(SPELL_PRIEST_MENTAL_FORTITUDE_SHIELD, SPELLVALUE_BASE_POINT0, shieldAmount, GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_mental_fortitude_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_mental_fortitude_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_mental_fortitude_AuraScript();
        }
};

// 205369 - Mind Bomb
class spell_pri_legion_mind_bomb : public SpellScriptLoader
{
    public:
        spell_pri_legion_mind_bomb() : SpellScriptLoader("spell_pri_legion_mind_bomb") { }

        class spell_pri_legion_mind_bomb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_mind_bomb_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_MIND_BOMB_TRIGGERED
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH || GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_EXPIRE)
                    if (Unit* caster = GetCaster())
                        caster->CastSpell(GetTarget(), SPELL_PRIEST_MIND_BOMB_TRIGGERED, true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_mind_bomb_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_mind_bomb_AuraScript();
        }
};

class CheckShadowyInsightEvent : public BasicEvent
{
    public:
        CheckShadowyInsightEvent(Unit* owner) : _owner(owner) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            _owner->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_PRIEST_MIND_BLAST)->ChargeCategoryId);
            return true;
        }

    private:
        Unit* _owner;
};

// 8092 - Mind Blast
class spell_pri_legion_mind_blast : public SpellScriptLoader
{
    public:
        spell_pri_legion_mind_blast() : SpellScriptLoader("spell_pri_legion_mind_blast") { }

        class spell_pri_legion_mind_blast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_mind_blast_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_SHADOWY_INSIGHT_AURA
                });
            }

            void HandleAfterCast()
            {
                // There is a possibility that shadowy insight was proccing during the mind blast cast... the priest should be still able to cast mind blast in this case again (retail confirmed)
                // wait one world tick to make sure mind blast cd is set
                if (GetCaster()->HasAura(SPELL_PRIEST_SHADOWY_INSIGHT_AURA))
                    GetCaster()->m_Events.AddEvent(new CheckShadowyInsightEvent(GetCaster()), GetCaster()->m_Events.CalculateTime(100));
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pri_legion_mind_blast_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_mind_blast_SpellScript();
        }
};

// 205364 - Mind Control
class spell_pri_legion_mind_control : public SpellScriptLoader
{
    public:
        spell_pri_legion_mind_control() : SpellScriptLoader("spell_pri_legion_mind_control") { }

        class spell_pri_legion_mind_control_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_mind_control_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_GENERIC_ID
                });
            }

            SpellCastResult CheckCast()
            {
                if (!GetCaster()->HasAura(SPELL_GENERIC_ID))
                    if (GetExplTargetUnit() && GetExplTargetUnit()->GetTypeId() == TYPEID_PLAYER)
                        return SPELL_FAILED_BAD_TARGETS;
                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_pri_legion_mind_control_SpellScript::CheckCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_mind_control_SpellScript;
        }
};

// 15407 Mind Flay
class spell_pri_legion_mind_flay : public SpellScriptLoader
{
    public:
        spell_pri_legion_mind_flay() : SpellScriptLoader("spell_pri_legion_mind_flay") { }

        class spell_pri_legion_mind_flay_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_mind_flay_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_WORD_PAIN,
                    SPELL_PRIEST_MIND_SEAR_PASSIVE,
                    SPELL_PRIEST_MIND_FLAY_AOE_SELECTOR,
                    SPELL_PRIEST_MIND_TRAUMA
                });
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& /*canBeRecalculated*/)
            {
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_PRIEST_MIND_TRAUMA))
                        caster->CastSpell(GetTarget(), SPELL_PRIEST_SHADOW_WORD_PAIN, true);
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (GetTarget()->HasAura(SPELL_PRIEST_SHADOW_WORD_PAIN, GetCasterGUID()))
                    if (Unit* caster = GetCaster())
                        if (caster->HasAura(SPELL_PRIEST_MIND_SEAR_PASSIVE))
                            caster->CastSpell(GetTarget(), SPELL_PRIEST_MIND_FLAY_AOE_SELECTOR, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_mind_flay_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_MOD_DECREASE_SPEED);
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_mind_flay_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_mind_flay_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_mind_flay_AuraScript();
        }
};

// 234696 - Mind Flay
class spell_pri_legion_mind_flay_aoe_selector : public SpellScriptLoader
{
    public:
        spell_pri_legion_mind_flay_aoe_selector() : SpellScriptLoader("spell_pri_legion_mind_flay_aoe_selector") { }

        class spell_pri_legion_mind_flay_aoe_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_mind_flay_aoe_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_MIND_FLAY_AOE_DAMAGE,
                    SPELL_PRIEST_MIND_FLAY_ENERGIZE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetExplTargetUnit());
                targetCount = targets.size();
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_MIND_FLAY_AOE_DAMAGE, true);
            }

            void HandleEnergize()
            {
                int32 energizeAmount = sSpellMgr->AssertSpellInfo(SPELL_PRIEST_MIND_FLAY_ENERGIZE)->GetEffect(EFFECT_0)->CalcValue(GetCaster()) * targetCount;
                GetCaster()->CastCustomSpell(SPELL_PRIEST_MIND_FLAY_ENERGIZE, SPELLVALUE_BASE_POINT0, energizeAmount, GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_mind_flay_aoe_selector_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_mind_flay_aoe_selector_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_pri_legion_mind_flay_aoe_selector_SpellScript::HandleEnergize);
            }

        private:
            uint32 targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_mind_flay_aoe_selector_SpellScript();
        }
};

// 47540 - Penance
class spell_pri_legion_penance : public SpellScriptLoader
{
    public:
        spell_pri_legion_penance() : SpellScriptLoader("spell_pri_legion_penance") { }

        class spell_pri_legion_penance_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_penance_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PENANCE_TARGET_ALLY,
                    SPELL_PRIEST_PENANCE_TARGET_ENEMY,
                    SPELL_PRIEST_THE_PENITENT,
                    SPELL_PRIEST_PURGE_THE_WICKED_DOT,
                    SPELL_PRIEST_PURGE_THE_WICKED_SEARCHER
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                Unit* target = GetHitUnit();

                if (target->HasAura(SPELL_PRIEST_PURGE_THE_WICKED_DOT, GetCaster()->GetGUID()))
                    caster->CastSpell(target, SPELL_PRIEST_PURGE_THE_WICKED_SEARCHER, true);

                if (caster->HasAura(SPELL_PRIEST_THE_PENITENT) && caster->IsFriendlyTo(target))
                    caster->CastSpell(target, SPELL_PRIEST_PENANCE_TARGET_ALLY, TRIGGERED_FULL_MASK);
                else
                    caster->CastSpell(target, SPELL_PRIEST_PENANCE_TARGET_ENEMY, TRIGGERED_FULL_MASK);
            }

            SpellCastResult CheckCast()
            {
                Player* caster = GetCaster()->ToPlayer();

                if (Unit* target = GetExplTargetUnit())
                {
                    // spell can only hit unfriendly units
                    if (!caster->HasAura(SPELL_PRIEST_THE_PENITENT))
                    {
                        if (!caster->IsValidAttackTarget(target))
                            return SPELL_FAILED_BAD_TARGETS;
                    }  // spell can target both - friendly and unfriendly units
                    else if (!caster->IsValidAttackTarget(target) && !caster->IsValidAssistTarget(target))
                        return SPELL_FAILED_BAD_TARGETS;

                    if (!caster->isInFront(target))
                        return SPELL_FAILED_UNIT_NOT_INFRONT;
                }
                else
                    return SPELL_FAILED_BAD_TARGETS;

                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_penance_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnCheckCast += SpellCheckCastFn(spell_pri_legion_penance_SpellScript::CheckCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_penance_SpellScript;
        }
};

class DelayedAuraRemoveEvent : public BasicEvent
{
    public:
        DelayedAuraRemoveEvent(Unit* owner, uint32 spellId) : _owner(owner), _spellId(spellId) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            _owner->RemoveAurasDueToSpell(_spellId);
            return true;
        }

    private:
        Unit* _owner;
        uint32 _spellId;
};

// 47757 - Penance
// 47758 - Penance
class spell_pri_legion_penance_triggered : public SpellScriptLoader
{
    public:
        spell_pri_legion_penance_triggered() : SpellScriptLoader("spell_pri_legion_penance_triggered") { }

        class spell_pri_legion_penance_triggered_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_penance_triggered_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SPEED_OF_THE_PIOUS,
                    SPELL_PRIEST_SPEED_OF_THE_PIOUS_AURA,
                    SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_AURA,
                    SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_MARKER
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->ClearDynamicValue(UNIT_DYNAMIC_FIELD_CHANNEL_OBJECTS);
                    caster->AddDynamicStructuredValue(UNIT_DYNAMIC_FIELD_CHANNEL_OBJECTS, &GetTarget()->GetGUID());

                    if (caster->HasAura(SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_AURA))
                    {
                        caster->RemoveAurasDueToSpell(SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_AURA);
                        caster->CastSpell(caster, SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_MARKER, true);
                    }
                    if (AuraEffect* speedOfThePious = caster->GetAuraEffect(SPELL_PRIEST_SPEED_OF_THE_PIOUS, EFFECT_0))
                        caster->CastCustomSpell(SPELL_PRIEST_SPEED_OF_THE_PIOUS_AURA, SPELLVALUE_BASE_POINT0, speedOfThePious->GetAmount(), caster, TRIGGERED_FULL_MASK);
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster()) // Penance has travel time we need to delay the aura remove a little bit...
                    caster->m_Events.AddEvent(new DelayedAuraRemoveEvent(caster, SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_MARKER), caster->m_Events.CalculateTime(1000));
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_penance_triggered_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_penance_triggered_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_penance_triggered_AuraScript();
        }
};

// 47750 - Penance
// 47666 - Penance
class spell_pri_legion_penance_heal_damage : public SpellScriptLoader
{
    public:
        spell_pri_legion_penance_heal_damage() : SpellScriptLoader("spell_pri_legion_penance_heal_damage") { }

        class spell_pri_legion_penance_heal_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_penance_heal_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_MARKER,
                    SPELL_PRIEST_PENANCE_HEAL
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* powerOfTheDarkSide = GetCaster()->GetAuraEffect(SPELL_PRIEST_POWER_OF_THE_DARK_SIDE_MARKER, EFFECT_0))
                {
                    if (GetSpellInfo()->Id == SPELL_PRIEST_PENANCE_HEAL)
                    {
                        int32 heal = GetHitHeal();
                        AddPct(heal, powerOfTheDarkSide->GetAmount());
                        SetHitHeal(heal);
                    }
                    else
                    {
                        int32 damage = GetHitDamage();
                        AddPct(damage, powerOfTheDarkSide->GetAmount());
                        SetHitDamage(damage);
                    }
                }
            }

            void Register() override
            {
                if (m_scriptSpellId == SPELL_PRIEST_PENANCE_HEAL)
                    OnEffectHitTarget += SpellEffectFn(spell_pri_legion_penance_heal_damage_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_HEAL);
                if (m_scriptSpellId == SPELL_PRIEST_PENANCE_DAMAGE)
                    OnEffectHitTarget += SpellEffectFn(spell_pri_legion_penance_heal_damage_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_penance_heal_damage_SpellScript();
        }
};

// 194509 - Power Word: Radiance
class spell_pri_legion_power_word_radiance : public SpellScriptLoader
{
    public:
        spell_pri_legion_power_word_radiance() : SpellScriptLoader("spell_pri_legion_power_word_radiance") { }

        class spell_pri_legion_power_word_radiance_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_power_word_radiance_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targetList)
            {
                targetList.remove(GetExplTargetUnit());
                targetList.sort(Trinity::ObjectDistanceOrderPred(GetExplTargetUnit()));

                std::list<WorldObject*> tempTargets = targetList;
                // this spell should focus targets without atonement first
                tempTargets.remove_if(Trinity::UnitAuraCheck(true, SPELL_PRIEST_ATONEMENT_BUFF, GetCaster()->GetGUID()));

                if (tempTargets.size() >= 2)
                {
                    if (tempTargets.size() > 2)
                        tempTargets.resize(2);

                    tempTargets.push_back(GetExplTargetUnit());
                    targetList = tempTargets;
                    return;
                }

                // not enough targets without atonement found lets fill up tempTargets with targets which has atonement active
                for (WorldObject* target : targetList)
                {
                    tempTargets.push_back(target);
                    if (tempTargets.size() == 2)
                        break;
                }

                tempTargets.push_back(GetExplTargetUnit());
                targetList = tempTargets;

            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_power_word_radiance_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_power_word_radiance_SpellScript();
        }
};

// 17 - Power Word: Shield
class spell_pri_legion_power_word_shield : public SpellScriptLoader
{
    public:
        spell_pri_legion_power_word_shield() : SpellScriptLoader("spell_pri_legion_power_word_shield") { }

        class spell_pri_legion_power_word_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_power_word_shield_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHIELD_OF_FAITH,
                    SPELL_PRIEST_SHARE_IN_THE_LIGHT,
                    SPELL_PRIEST_SHARE_IN_THE_LIGHT_ABSORB,
                    SPELL_PRIEST_SHIELD_DISCIPLINE,
                    SPELL_PRIEST_SHIELD_DISCIPLINE_ENERGIZE,
                    SPELL_PRIEST_VOID_SHIELD_HONOR_TALENT,
                    SPELL_PRIEST_VOID_SHIELD
                });
            }

            bool Load() override
            {
                return GetCaster() && GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                if (Unit* caster = GetCaster())
                {
                    amount = int32(5.5f * caster->SpellBaseHealingBonusDone(GetSpellInfo()->GetSchoolMask()));
                    AddPct(amount, caster->ToPlayer()->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE));

                    if (AuraEffect* shieldOfFaith = caster->GetAuraEffect(SPELL_PRIEST_SHIELD_OF_FAITH, EFFECT_0))
                        AddPct(amount, shieldOfFaith->GetAmount());

                    if (AuraEffect* grace = caster->GetAuraEffect(SPELL_PRIEST_GRACE, EFFECT_0))
                        AddPct(amount, grace->GetAmount());

                    Unit::AuraEffectList const& absorbBonus = caster->GetAuraEffectsByType(SPELL_AURA_MOD_ABSORB_EFFECTS_AMOUNT_PCT);
                    for (auto bonus : absorbBonus)
                        AddPct(amount, bonus->GetAmount());
                }
            }

            void HandleApplyEffect(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_PRIEST_STRENGTH_OF_SOUL_HONOR_TALENT))
                    {
                        caster->CastSpell(GetTarget(), SPELL_PRIEST_STRENGTH_OF_SOUL_HEAL, true);
                        caster->CastSpell(GetTarget(), SPELL_PRIEST_STRENGTH_OF_SOUL_SHIELD, true);
                    }

                    if (GetCasterGUID() == GetTarget()->GetGUID())
                    {
                        if (caster->HasAura(SPELL_PRIEST_VOID_SHIELD_HONOR_TALENT))
                            caster->CastSpell(caster, SPELL_PRIEST_VOID_SHIELD, true);
                        return;
                    }

                    if (AuraEffect* shareInTheLight = caster->GetAuraEffect(SPELL_PRIEST_SHARE_IN_THE_LIGHT, EFFECT_0))
                        caster->CastCustomSpell(SPELL_PRIEST_SHARE_IN_THE_LIGHT_ABSORB, SPELLVALUE_BASE_POINT0, CalculatePct(aurEff->GetAmount(), shareInTheLight->GetAmount()), caster, TRIGGERED_FULL_MASK);
                }
            }

            void RemoveEffect(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_ENEMY_SPELL && aurEff->GetAmount() <= 0)
                    if (Unit* caster = GetCaster())
                        if (caster->HasAura(SPELL_PRIEST_SHIELD_DISCIPLINE))
                            caster->CastSpell(caster, SPELL_PRIEST_SHIELD_DISCIPLINE_ENERGIZE, true, nullptr, aurEff);

                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_STRENGTH_OF_SOUL_SHIELD);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_power_word_shield_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_power_word_shield_AuraScript::HandleApplyEffect, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_power_word_shield_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_power_word_shield_AuraScript();
        }

        class spell_pri_legion_power_word_shield_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_power_word_shield_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_BODY_AND_SOUL,
                    SPELL_PRIEST_BODY_AND_SOUL_SPEED_BUFF
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_PRIEST_BODY_AND_SOUL) && GetHitUnit())
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_BODY_AND_SOUL_SPEED_BUFF, true);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_pri_legion_power_word_shield_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_power_word_shield_SpellScript();
        }
};

// 33076 - Prayer of Mending
class spell_pri_legion_prayer_of_mending_initial : public SpellScriptLoader
{
    public:
        spell_pri_legion_prayer_of_mending_initial() : SpellScriptLoader("spell_pri_legion_prayer_of_mending_initial") { }

        class spell_pri_legion_prayer_of_mending_initial_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_prayer_of_mending_initial_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_prayer_of_mending_initial_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_prayer_of_mending_initial_SpellScript();
        }
};

// 225275 - Prayer of Mending
class spell_pri_legion_prayer_of_mending_bounce : public SpellScriptLoader
{
    public:
        spell_pri_legion_prayer_of_mending_bounce() : SpellScriptLoader("spell_pri_legion_prayer_of_mending_bounce") { }

        class spell_pri_legion_prayer_of_mending_bounce_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_prayer_of_mending_bounce_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PRAYER_OF_MENDING_PROC_AURA,
                    SPELL_PRIEST_HOLY_MENDING,
                    SPELL_PRIEST_HOLY_MENDING_HEAL
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetOriginalCaster())
                {
                    caster->CastCustomSpell(SPELL_PRIEST_PRAYER_OF_MENDING_PROC_AURA, SPELLVALUE_AURA_STACK, GetEffectValue(), GetHitUnit(), TRIGGERED_FULL_MASK);

                    if (caster->HasAura(SPELL_PRIEST_HOLY_MENDING) && GetHitUnit()->HasAura(SPELL_PRIEST_RENEW, caster->GetGUID()))
                        caster->CastSpell(GetHitUnit(), SPELL_PRIEST_HOLY_MENDING_HEAL, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_prayer_of_mending_bounce_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_prayer_of_mending_bounce_SpellScript();
        }
};

// 41635 - Prayer of Mending
class spell_pri_legion_prayer_of_mending_proc : public SpellScriptLoader
{
    public:
        spell_pri_legion_prayer_of_mending_proc() : SpellScriptLoader("spell_pri_legion_prayer_of_mending_proc") { }

        class spell_pri_legion_prayer_of_mending_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_prayer_of_mending_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE_AREA,
                    SPELL_PRIEST_PRAYER_OF_MENDING_HEAL,
                    SPELL_PRIEST_BENEDICTION,
                    SPELL_PRIEST_RENEW,
                    SPELL_PRIEST_SAY_YOUR_PRAYERS
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                if (Unit* caster = GetCaster())
                    amount = int32(caster->SpellBaseHealingBonusDone(GetSpellInfo()->GetSchoolMask()) * 1.75f) / GetAura()->GetStackAmount();
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                {
                    bool withoutChargeDrop = false;
                    if (AuraEffect* sayYourPrayers = caster->GetAuraEffect(SPELL_PRIEST_SAY_YOUR_PRAYERS, EFFECT_0))
                        withoutChargeDrop = roll_chance_i(sayYourPrayers->GetAmount());

                    if (GetAura()->GetStackAmount() > 1 || withoutChargeDrop)
                        GetTarget()->CastCustomSpell(SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE_AREA, SPELLVALUE_BASE_POINT0, withoutChargeDrop ? GetAura()->GetStackAmount() : (GetAura()->GetStackAmount() - 1), GetTarget(), TRIGGERED_FULL_MASK, nullptr, aurEff, GetCasterGUID());

                    if (AuraEffect* benediction = caster->GetAuraEffect(SPELL_PRIEST_BENEDICTION, EFFECT_0))
                        if (roll_chance_i(benediction->GetAmount()))
                            caster->CastSpell(GetTarget(), SPELL_PRIEST_RENEW, true);

                    caster->CastSpell(GetTarget(), SPELL_PRIEST_PRAYER_OF_MENDING_HEAL, true, nullptr, aurEff);
                }

                Remove();
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_prayer_of_mending_proc_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_prayer_of_mending_proc_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_prayer_of_mending_proc_AuraScript();
        }
};

// 155793 - Prayer of Mending
class spell_pri_legion_prayer_of_mending_bounce_selector : public SpellScriptLoader
{
    public:
        spell_pri_legion_prayer_of_mending_bounce_selector() : SpellScriptLoader("spell_pri_legion_prayer_of_mending_bounce_selector") { }

        class spell_pri_legion_prayer_of_mending_bounce_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_prayer_of_mending_bounce_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE,
                    SPELL_PRIEST_PRAYER_OF_MENDING_PROC_AURA
                });
            }

            void SelectTarget(std::list<WorldObject*>& targets)
            {
                Player* caster = GetCaster()->GetCharmerOrOwnerPlayerOrPlayerItself();
                targets.remove(caster);
                if (GetOriginalCaster())
                    targets.remove_if(Trinity::UnitAuraCheck(true, SPELL_PRIEST_PRAYER_OF_MENDING_PROC_AURA, GetOriginalCaster()->GetGUID()));

                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit()) // Possibly a pet or temp summon
                        if (Player* playerTarget = unitTarget->GetCharmerOrOwnerPlayerOrPlayerItself())
                            return !caster->IsInSameRaidWith(playerTarget);
                    return true;
                });

                targets.sort(Trinity::HealthPctOrderPred(true));
                if (targets.size() > 1)
                    targets.resize(1);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetOriginalCaster())
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE, SPELLVALUE_BASE_POINT0, GetEffectValue(), GetHitUnit(), TRIGGERED_FULL_MASK, nullptr, nullptr, caster->GetGUID());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_prayer_of_mending_bounce_selector_SpellScript::SelectTarget, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_prayer_of_mending_bounce_selector_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_prayer_of_mending_bounce_selector_SpellScript();
        }
};

// 209780 - Premonition
class spell_pri_legion_premonition : public SpellScriptLoader
{
    public:
        spell_pri_legion_premonition() : SpellScriptLoader("spell_pri_legion_premonition") { }

        class spell_pri_legion_premonition_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_premonition_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_PREMONITION_DAMAGE,
                    SPELL_PRIEST_ATONEMENT_BUFF
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                int32 damage = int32(GetCaster()->GetTargetAuraApplications(SPELL_PRIEST_ATONEMENT_BUFF).size()) * int32(GetCaster()->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_MAGIC) * GetEffectInfo()->BonusCoefficient);
                GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_ATONEMENT_BUFF, true);
                GetCaster()->CastCustomSpell(SPELL_PRIEST_PREMONITION_DAMAGE, SPELLVALUE_BASE_POINT1, damage, GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_premonition_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_premonition_SpellScript();
        }
};

// 209885 - Premonition
class spell_pri_legion_premonition_damage : public SpellScriptLoader
{
    public:
        spell_pri_legion_premonition_damage() : SpellScriptLoader("spell_pri_legion_premonition_damage") { }

        class spell_pri_legion_premonition_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_premonition_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_PREMONITION,
                    SPELL_PRIEST_ATONEMENT_BUFF
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Aura* atonement = GetHitUnit()->GetAura(SPELL_PRIEST_ATONEMENT_BUFF, GetCaster()->GetGUID()))
                {
                    int32 newDuration = atonement->GetDuration();
                    if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_PRIEST_PREMONITION)->GetEffect(EFFECT_0))
                        newDuration += effInfo->BasePoints;

                    if (newDuration > atonement->GetMaxDuration())
                        atonement->SetMaxDuration(newDuration);
                    atonement->SetDuration(newDuration);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_premonition_damage_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_premonition_damage_SpellScript();
        }
};

// 8092 - Mind Blast
// 205448 - Void Bolt
class spell_pri_legion_psychic_link : public SpellScriptLoader
{
    public:
        spell_pri_legion_psychic_link() : SpellScriptLoader("spell_pri_legion_psychic_link") { }

        class spell_pri_legion_psychic_link_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_psychic_link_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PSYCHIC_LINK_HONOR_TALENT,
                    SPELL_PRIEST_PSYCHIC_LINK_DAMAGE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_PRIEST_PSYCHIC_LINK_HONOR_TALENT, EFFECT_0))
                {
                    int32 damage = CalculatePct(GetHitDamage(), aurEff->GetAmount());
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_PSYCHIC_LINK_DAMAGE, SPELLVALUE_BASE_POINT0, damage, GetHitUnit(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_psychic_link_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_psychic_link_SpellScript();
        }
};

// 199486 - Psychic Link
class spell_pri_legion_psychic_link_damage : public SpellScriptLoader
{
    public:
        spell_pri_legion_psychic_link_damage() : SpellScriptLoader("spell_pri_legion_psychic_link_damage") { }

        class spell_pri_legion_psychic_link_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_psychic_link_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_WORD_PAIN
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_PRIEST_SHADOW_WORD_PAIN, GetCaster()->GetGUID()));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_psychic_link_damage_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_psychic_link_damage_SpellScript();
        }
};

// 199845 - Psyflay
class spell_pri_legion_psyflay : public SpellScriptLoader
{
    public:
        spell_pri_legion_psyflay() : SpellScriptLoader("spell_pri_legion_psyflay") { }

        class spell_pri_legion_psyflay_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_psyflay_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                std::list<WorldObject*> tempTargets = targets;
                tempTargets.remove_if(Trinity::UnitAuraCheck(false, SPELL_PRIEST_PSYFIEND_SUMMON, GetCaster()->GetOwnerGUID()));

                if (tempTargets.empty() && targets.size() > 1)
                    Trinity::Containers::RandomResize(targets, 1);
                else
                    targets = tempTargets;
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_psyflay_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_psyflay_SpellScript();
        }

        class spell_pri_legion_psyflay_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_psyflay_AuraScript);

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = true;
                amount = CalculatePct(GetUnitOwner()->GetMaxHealth(), aurEff->GetBaseAmount());
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_psyflay_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_psyflay_AuraScript();
        }
};

// 196489 - Power of the Naaru
class spell_pri_legion_power_of_the_naaru : public SpellScriptLoader
{
    public:
        spell_pri_legion_power_of_the_naaru() : SpellScriptLoader("spell_pri_legion_power_of_the_naaru") { }

        class spell_pri_legion_power_of_the_naaru_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_power_of_the_naaru_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_POWER_OF_THE_NAARU_BONUS,
                    SPELL_PRIEST_HOLY_WORD_SANCTIFY
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                return eventInfo.GetSpellInfo()->Id == SPELL_PRIEST_HOLY_WORD_SANCTIFY;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastCustomSpell(SPELL_PRIEST_POWER_OF_THE_NAARU_BONUS, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_power_of_the_naaru_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_power_of_the_naaru_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_power_of_the_naaru_AuraScript();
        }
};

// 129250 - Power Word: Solace
class spell_pri_legion_power_word_solace : public SpellScriptLoader
{
    public:
        spell_pri_legion_power_word_solace() : SpellScriptLoader("spell_pri_legion_power_word_solace") { }

        class spell_pri_legion_power_word_solace_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_power_word_solace_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_POWER_WORD_SOLACE_ENERGIZE
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_POWER_WORD_SOLACE_ENERGIZE, true);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_pri_legion_power_word_solace_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_power_word_solace_SpellScript();
        }
};

// 204215 - Purge the Wicked
class spell_pri_legion_purge_the_wicked_selector : public SpellScriptLoader
{
    public:
        spell_pri_legion_purge_the_wicked_selector() : SpellScriptLoader("spell_pri_legion_purge_the_wicked_selector") { }

        class spell_pri_legion_purge_the_wicked_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_purge_the_wicked_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PURGE_THE_WICKED_DOT,
                    SPELL_PRIEST_PURGE_THE_WICKED
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(true, SPELL_PRIEST_PURGE_THE_WICKED_DOT, GetCaster()->GetGUID()));
                targets.sort(Trinity::ObjectDistanceOrderPred(GetExplTargetUnit()));
                if (targets.size() > 1)
                    targets.resize(1);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_PURGE_THE_WICKED, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_purge_the_wicked_selector_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_purge_the_wicked_selector_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_purge_the_wicked_selector_SpellScript();
        }
};

// 527 Purify
class spell_pri_legion_purify : public SpellScriptLoader
{
    public:
        spell_pri_legion_purify() : SpellScriptLoader("spell_pri_legion_purify") { }

        class spell_pri_legion_purify_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_purify_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_PURIFIED_RESOLVE_HONOR_TALENT,
                    SPELL_PRIEST_PURIFIED_RESOLVE_SHIELD
                });
            }

            void OnSuccessfulDispel(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_PRIEST_PURIFIED_RESOLVE_HONOR_TALENT, EFFECT_0))
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_PURIFIED_RESOLVE_SHIELD, SPELLVALUE_BASE_POINT0, int32(CalculatePct(GetHitUnit()->GetMaxHealth(), aurEff->GetAmount())), GetHitUnit(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectSuccessfulDispel += SpellEffectFn(spell_pri_legion_purify_SpellScript::OnSuccessfulDispel, EFFECT_ALL, SPELL_EFFECT_DISPEL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_purify_SpellScript();
        }
};

// 47536 - Rapture
class spell_pri_legion_rapture : public SpellScriptLoader
{
    public:
        spell_pri_legion_rapture() : SpellScriptLoader("spell_pri_legion_rapture") { }

        class spell_pri_legion_rapture_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_rapture_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_POWER_WORD_SHIELD
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_PRIEST_POWER_WORD_SHIELD, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pri_legion_rapture_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_rapture_SpellScript();
        }
};

// 197268 - Ray of Hope
class spell_pri_legion_ray_of_hope : public SpellScriptLoader
{
    public:
        spell_pri_legion_ray_of_hope() : SpellScriptLoader("spell_pri_legion_ray_of_hope") { }

        class spell_pri_legion_ray_of_hope_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_ray_of_hope_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_RAY_OF_HOPE_HEAL,
                    SPELL_PRIEST_RAY_OF_HOPE_DAMAGE,
                    SPELL_PRIEST_RAY_OF_HOPE_HEAL_INFO,
                    SPELL_PRIEST_RAY_OF_HOPE_DAMAGE_INFO,
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32 & absorbAmount)
            {
                absorbAmount = dmgInfo.GetDamage();
                if (AuraEffect* damageHolder = GetEffect(EFFECT_2))
                {
                    damageHolder->SetAmount(damageHolder->GetAmount() + absorbAmount);

                    if (AuraEffect* healHolder = GetEffect(EFFECT_3))
                    {
                        if (damageHolder->GetAmount() > healHolder->GetAmount())
                        {
                            if (!GetTarget()->HasAura(SPELL_PRIEST_RAY_OF_HOPE_DAMAGE_INFO))
                            {
                                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_RAY_OF_HOPE_HEAL_INFO);
                                if (Aura* aura = GetTarget()->AddAura(SPELL_PRIEST_RAY_OF_HOPE_DAMAGE_INFO, GetTarget()))
                                {
                                    aura->SetMaxDuration(GetAura()->GetMaxDuration());
                                    aura->SetDuration(GetAura()->GetDuration());
                                }
                            }
                        }
                    }
                }
            }

            void AbsorbHeal(AuraEffect* /*aurEff*/, HealInfo& healInfo, uint32& absorbAmount)
            {
                absorbAmount = healInfo.GetHeal();
                int32 tempAbsorb = absorbAmount;
                if (AuraEffect* healHolder = GetEffect(EFFECT_3))
                {
                    if (AuraEffect* healPctBonus = GetEffect(EFFECT_4))
                        AddPct(tempAbsorb, healPctBonus->GetAmount());

                    healHolder->SetAmount(healHolder->GetAmount() + tempAbsorb);

                    if (AuraEffect* damageHolder = GetEffect(EFFECT_2))
                    {
                        if (healHolder->GetAmount() > damageHolder->GetAmount())
                        {
                            if (!GetTarget()->HasAura(SPELL_PRIEST_RAY_OF_HOPE_HEAL_INFO))
                            {
                                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_RAY_OF_HOPE_DAMAGE_INFO);
                                if (Aura* aura = GetTarget()->AddAura(SPELL_PRIEST_RAY_OF_HOPE_HEAL_INFO, GetTarget()))
                                    aura->SetDuration(GetAura()->GetDuration());
                            }
                        }
                    }
                }
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                AuraEffect* damageHolder = GetEffect(EFFECT_2);
                AuraEffect* healHolder = GetEffect(EFFECT_3);

                if (!damageHolder || !healHolder)
                    return;

                int32 damageOrHeal = healHolder->GetAmount() - damageHolder->GetAmount();

                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(damageOrHeal > 0 ? SPELL_PRIEST_RAY_OF_HOPE_HEAL : SPELL_PRIEST_RAY_OF_HOPE_DAMAGE, SPELLVALUE_BASE_POINT0, std::abs(damageOrHeal), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_ray_of_hope_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_ray_of_hope_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_SCHOOL_HEAL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pri_legion_ray_of_hope_AuraScript::Absorb, EFFECT_0);
                OnEffectAbsorbHeal += AuraEffectAbsorbHealFn(spell_pri_legion_ray_of_hope_AuraScript::AbsorbHeal, EFFECT_1);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_ray_of_hope_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_SCHOOL_HEAL_ABSORB, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_ray_of_hope_AuraScript();
        }
};

// 139 - Renew
class spell_pri_legion_renew : public SpellScriptLoader
{
    public:
        spell_pri_legion_renew() : SpellScriptLoader("spell_pri_legion_renew") { }

        class spell_pri_legion_renew_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_renew_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PERSEVERANCE
                });
            }

            void CalculateAmount(AuraEffect const* aurEff, int32 & amount, bool & /*canBeRecalculated*/)
            {
                if (GetUnitOwner()->HasAura(SPELL_PRIEST_PERSEVERANCE, GetCasterGUID()))
                    const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
                else
                    amount = 0;
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_renew_AuraScript::CalculateAmount, EFFECT_2, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_renew_AuraScript();
        }
};

// 196529 - Renew the Faith
class spell_pri_legion_renew_the_faith : public SpellScriptLoader
{
    public:
        spell_pri_legion_renew_the_faith() : SpellScriptLoader("spell_pri_legion_renew_the_faith") { }

        class spell_pri_legion_renew_the_faith_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_renew_the_faith_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PRAYER_OF_MENDING_PROC_AURA,
                    SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE_AREA,
                    SPELL_PRIEST_BENEDICTION,
                    SPELL_PRIEST_RENEW,
                    SPELL_PRIEST_PRAYER_OF_MENDING_HEAL
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Aura* prayerOfMending = GetHitUnit()->GetAura(SPELL_PRIEST_PRAYER_OF_MENDING_PROC_AURA, GetCaster()->GetGUID()))
                {
                    GetHitUnit()->CastCustomSpell(SPELL_PRIEST_PRAYER_OF_MENDING_BOUNCE_AREA, SPELLVALUE_BASE_POINT0, prayerOfMending->GetStackAmount(), GetHitUnit(), TRIGGERED_FULL_MASK, nullptr, prayerOfMending->GetEffect(EFFECT_0), prayerOfMending->GetCasterGUID());

                    if (AuraEffect* benediction = GetCaster()->GetAuraEffect(SPELL_PRIEST_BENEDICTION, EFFECT_0))
                        if (roll_chance_i(benediction->GetAmount()))
                            GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_RENEW, true);

                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_PRAYER_OF_MENDING_HEAL, true, nullptr, prayerOfMending->GetEffect(EFFECT_0));
                    prayerOfMending->Remove();
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_renew_the_faith_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_renew_the_faith_SpellScript();
        }
};

// 215768 - Searing Light
class spell_pri_legion_searing_light : public SpellScriptLoader
{
    public:
        spell_pri_legion_searing_light() : SpellScriptLoader("spell_pri_legion_searing_light") { }

        class spell_pri_legion_searing_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_searing_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_PENANCE
                });
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_PRIEST_PENANCE, aurEff->GetAmount());
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_searing_light_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_searing_light_AuraScript();
        }
};

// 63733 - Serendipity
class spell_pri_legion_serendipity : public SpellScriptLoader
{
    public:
        spell_pri_legion_serendipity() : SpellScriptLoader("spell_pri_legion_serendipity") { }

        class spell_pri_legion_serendipity_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_serendipity_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_LIGHT_OF_THE_NAARU,
                    SPELL_PRIEST_FLASH_HEAL,
                    SPELL_PRIEST_HEAL,
                    SPELL_PRIEST_APOTHEOSIS,
                    SPELL_PRIEST_HOLY_WORD_SERENITY,
                    SPELL_PRIEST_PRAYER_OF_MENDING,
                    SPELL_PRIEST_PIETY,
                    SPELL_PRIEST_PRAYER_OF_HEALING,
                    SPELL_PRIEST_HOLY_WORD_SANCTIFY,
                    SPELL_PRIEST_SMITE,
                    SPELL_PRIEST_HOLY_WORD_CHATISE,
                    SPELL_PRIEST_BINDING_HEAL
                });
            }

            void HandleProc(ProcEventInfo& procInfo)
            {
                PreventDefaultAction();
                int32 cooldownReduce = 0;
                if (AuraEffect* lightOfTheNaaru = GetTarget()->GetAuraEffect(SPELL_PRIEST_LIGHT_OF_THE_NAARU, EFFECT_0))
                    cooldownReduce += fabs(lightOfTheNaaru->GetAmount());

                switch (procInfo.GetSpellInfo()->Id)
                {
                    case SPELL_PRIEST_FLASH_HEAL:
                    case SPELL_PRIEST_HEAL:
                        if (AuraEffect* aurEff = GetEffect(EFFECT_0))
                            cooldownReduce += aurEff->GetAmount() * 1000;

                        if (AuraEffect* apotheosis = GetTarget()->GetAuraEffect(SPELL_PRIEST_APOTHEOSIS, EFFECT_0))
                            AddPct(cooldownReduce, apotheosis->GetAmount());

                        GetTarget()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_PRIEST_HOLY_WORD_SERENITY)->ChargeCategoryId, cooldownReduce);
                        break;
                    case SPELL_PRIEST_PRAYER_OF_MENDING:
                        if (!GetTarget()->HasAura(SPELL_PRIEST_PIETY))
                            break;
                    case SPELL_PRIEST_PRAYER_OF_HEALING:
                        if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                            break;
                        else
                            GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(50)); /// @TODO: new proc system

                        if (AuraEffect* aurEff = GetEffect(EFFECT_1))
                            cooldownReduce += aurEff->GetAmount() * 1000;

                        if (AuraEffect* apotheosis = GetTarget()->GetAuraEffect(SPELL_PRIEST_APOTHEOSIS, EFFECT_0))
                            AddPct(cooldownReduce, apotheosis->GetAmount());

                        GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_PRIEST_HOLY_WORD_SANCTIFY, -cooldownReduce);
                        break;
                    case SPELL_PRIEST_SMITE:
                        if (AuraEffect* aurEff = GetEffect(EFFECT_2))
                            cooldownReduce += aurEff->GetAmount() * 1000;

                        if (AuraEffect* apotheosis = GetTarget()->GetAuraEffect(SPELL_PRIEST_APOTHEOSIS, EFFECT_0))
                            AddPct(cooldownReduce, apotheosis->GetAmount());

                        GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_PRIEST_HOLY_WORD_CHATISE, -cooldownReduce);
                        break;
                    case SPELL_PRIEST_BINDING_HEAL:
                        if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                            break;
                        else
                            GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(50));

                        if (AuraEffect* aurEff = GetEffect(EFFECT_3))
                            cooldownReduce += aurEff->GetAmount() * 1000;

                        if (AuraEffect* apotheosis = GetTarget()->GetAuraEffect(SPELL_PRIEST_APOTHEOSIS, EFFECT_0))
                            AddPct(cooldownReduce, apotheosis->GetAmount());

                        GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_PRIEST_HOLY_WORD_SANCTIFY, -cooldownReduce);
                        GetTarget()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_PRIEST_HOLY_WORD_SERENITY)->ChargeCategoryId, cooldownReduce);
                        break;
                    default:
                        break;
                }
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_pri_legion_serendipity_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_serendipity_AuraScript();
        }
};

// 232698 - Shadowform
class spell_pri_legion_shadowform : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadowform() : SpellScriptLoader("spell_pri_legion_shadowform") { }

        class spell_pri_legion_shadowform_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_shadowform_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_INSANITY_1,
                    SPELL_PRIEST_INSANITY_2,
                    SPELL_PRIEST_INSANITY_3,
                    SPELL_PRIEST_INSANITY_4
                });
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 250;
            }

            void RemoveShadowForms()
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_INSANITY_1);
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_INSANITY_2);
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_INSANITY_3);
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_INSANITY_4);
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                int32 currentPower = GetTarget()->GetPower(POWER_INSANITY);
                if (currentPower < 2500 && !GetTarget()->HasAura(SPELL_PRIEST_INSANITY_1))
                {
                    RemoveShadowForms();
                    GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_INSANITY_1, true);
                }
                else if (currentPower < 5000 && currentPower >= 2500 && !GetTarget()->HasAura(SPELL_PRIEST_INSANITY_2))
                {
                    RemoveShadowForms();
                    GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_INSANITY_2, true);
                }
                else if (currentPower < 7500 && currentPower >= 5000 && !GetTarget()->HasAura(SPELL_PRIEST_INSANITY_3))
                {
                    RemoveShadowForms();
                    GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_INSANITY_3, true);
                }
                else if (currentPower >= 7500 && !GetTarget()->HasAura(SPELL_PRIEST_INSANITY_4))
                {
                    RemoveShadowForms();
                    GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_INSANITY_4, true);
                }
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void RemoveVisuals(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                RemoveShadowForms();
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_pri_legion_shadowform_AuraScript::CalcPeriodic, EFFECT_2, SPELL_AURA_MOD_SHAPESHIFT);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_shadowform_AuraScript::HandleDummyTick, EFFECT_2, SPELL_AURA_MOD_SHAPESHIFT);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_pri_legion_shadowform_AuraScript::HandleUpdatePeriodic, EFFECT_2, SPELL_AURA_MOD_SHAPESHIFT);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_shadowform_AuraScript::RemoveVisuals, EFFECT_2, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_shadowform_AuraScript();
        }
};

// 204065 - Shadow Covenant
/// @TODO: new proc system ?
class spell_pri_legion_shadow_covenant : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadow_covenant() : SpellScriptLoader("spell_pri_legion_shadow_covenant") { }

        class spell_pri_legion_shadow_covenant_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_shadow_covenant_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_COVENANT_ABSORB
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.sort(Trinity::HealthPctOrderPred());
                uint32 maxTargets = uint32(GetEffectInfo(EFFECT_1)->CalcValue(GetCaster()));
                if (targets.size() > maxTargets)
                    targets.resize(maxTargets);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                absorbAmount = GetHitHeal() / 2;
            }

            void HandleAbsorb()
            {
                // We handle this here because if the absorb is casted too early it's consumed by the heal itself
                if (GetHitUnit() && absorbAmount > 0)
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_SHADOW_COVENANT_ABSORB, SPELLVALUE_BASE_POINT0, absorbAmount, GetHitUnit(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_shadow_covenant_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_shadow_covenant_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_HEAL);
                AfterHit += SpellHitFn(spell_pri_legion_shadow_covenant_SpellScript::HandleAbsorb);
            }

        private:
            uint32 absorbAmount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_shadow_covenant_SpellScript();
        }
};

// 32379 - Shadow Word: Death
// 199911 - Shadow Word: Death (Reaper of Souls version)
class spell_pri_legion_shadow_word_death : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadow_word_death() : SpellScriptLoader("spell_pri_legion_shadow_word_death") { }

        class spell_pri_legion_shadow_word_death_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_shadow_word_death_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE,
                    SPELL_PRIEST_SHADOW_WORD_DEATH_MARKER,
                    SPELL_PRIEST_REAPER_OF_SOULS
                });
            }

            void HandleEnergize(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_PRIEST_REAPER_OF_SOULS))
                    GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE, true);
                else
                {
                    int32 energize = sSpellMgr->AssertSpellInfo(SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE)->GetEffect(EFFECT_0)->CalcValue(GetCaster()) / 2;
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE, SPELLVALUE_BASE_POINT0, energize, GetCaster(), TRIGGERED_FULL_MASK);
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_SHADOW_WORD_DEATH_MARKER, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_shadow_word_death_SpellScript::HandleEnergize, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_shadow_word_death_SpellScript();
        }
};

// 226734 - Shadow Word: Death
class spell_pri_legion_shadow_word_death_marker: public SpellScriptLoader
{
    public:
        spell_pri_legion_shadow_word_death_marker() : SpellScriptLoader("spell_pri_legion_shadow_word_death_marker") { }

        class spell_pri_legion_shadow_word_death_marker_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_shadow_word_death_marker_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH)
                {
                    if (Unit* caster = GetCaster())
                    {
                        int32 energize = sSpellMgr->AssertSpellInfo(SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE)->GetEffect(EFFECT_0)->CalcValue(caster) / 2;
                        caster->CastCustomSpell(SPELL_PRIEST_SHADOW_WORD_DEATH_ENERGIZE, SPELLVALUE_BASE_POINT0, energize, caster, TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_shadow_word_death_marker_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_shadow_word_death_marker_AuraScript();
        }
};

// 589 - Shadow Word: Pain
class spell_pri_legion_shadow_word_pain : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadow_word_pain() : SpellScriptLoader("spell_pri_legion_shadow_word_pain") { }

        class spell_pri_legion_shadow_word_pain_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_shadow_word_pain_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleEffectHit(SpellEffIndex effIndex)
            {
                if (GetCaster()->ToPlayer()->GetRole() != TALENT_ROLE_DAMAGE)
                    PreventHitDefaultEffect(effIndex);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_shadow_word_pain_SpellScript::HandleEffectHit, EFFECT_2, SPELL_EFFECT_ENERGIZE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_shadow_word_pain_SpellScript();
        }
};

// 199572 - Shadow Mania
class spell_pri_legion_shadow_mania : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadow_mania() : SpellScriptLoader("spell_pri_legion_shadow_mania") { }

        class spell_pri_legion_shadow_mania_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_shadow_mania_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_SHADOW_MANIA_PERIODIC
                });
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                PreventDefaultAction(); // prevent console spam
                if ((int32(GetTarget()->GetTargetAuraApplications(SPELL_PRIEST_VAMPIRIC_TOUCH).size()) >= aurEff->GetAmount()))
                {
                    if (!GetTarget()->HasAura(SPELL_PRIEST_SHADOW_MANIA_PERIODIC))
                        GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_SHADOW_MANIA_PERIODIC, true);
                }
                else
                    GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_SHADOW_MANIA_PERIODIC);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_SHADOW_MANIA_PERIODIC);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_shadow_mania_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_shadow_mania_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_shadow_mania_AuraScript();
        }
};

// 186263 - Shadow Mend
class spell_pri_legion_shadow_mend_heal : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadow_mend_heal() : SpellScriptLoader("spell_pri_legion_shadow_mend_heal") { }

        class spell_pri_legion_shadow_mend_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_shadow_mend_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_MEND_PERIODIC_DUMMY,
                    SPELL_PRIEST_MASOCHISM,
                    SPELL_PRIEST_MASOCHISM_PERIODIC_HEAL,
                    SPELL_PRIEST_TAMING_THE_SHADOWS
                });
            }

            void HandleEffectHit(SpellEffIndex /*effIndex*/)
            {
                int32 periodicAmount = GetHitHeal() / 20;
                int32 damageForAuraRemoveAmount = periodicAmount * 10;
                if (GetCaster()->HasAura(SPELL_PRIEST_MASOCHISM) && GetCaster()->GetGUID() == GetHitUnit()->GetGUID())
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_MASOCHISM_PERIODIC_HEAL, SPELLVALUE_BASE_POINT0, periodicAmount, GetCaster(), TRIGGERED_FULL_MASK);
                else if (GetHitUnit()->IsInCombat())
                {
                    if (AuraEffect* tamingTheShadows = GetCaster()->GetAuraEffect(SPELL_PRIEST_TAMING_THE_SHADOWS, EFFECT_0))
                        if (roll_chance_i(tamingTheShadows->GetAmount()))
                            return;
                    GetCaster()->CastCustomSpell(GetHitUnit(), SPELL_PRIEST_SHADOW_MEND_PERIODIC_DUMMY, &periodicAmount, &damageForAuraRemoveAmount, nullptr, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_shadow_mend_heal_SpellScript::HandleEffectHit, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_shadow_mend_heal_SpellScript();
        }
};

// 187464 - Shadow Mend
class spell_pri_legion_shadow_mend_periodic : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadow_mend_periodic() : SpellScriptLoader("spell_pri_legion_shadow_mend_periodic") { }

        class spell_pri_legion_shadow_mend_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_shadow_mend_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_MEND_DAMAGE
                });
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (GetTarget()->IsInCombat())
                    GetTarget()->CastCustomSpell(SPELL_PRIEST_SHADOW_MEND_DAMAGE, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK, nullptr, aurEff, GetCasterGUID());
                else
                    Remove();
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 newAmount = aurEff->GetAmount() - eventInfo.GetDamageInfo()->GetDamage();
                if (newAmount < 0)
                    Remove();
                else
                    const_cast<AuraEffect*>(aurEff)->ChangeAmount(newAmount);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_shadow_mend_periodic_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_shadow_mend_periodic_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_shadow_mend_periodic_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_shadow_mend_periodic_AuraScript();
        }
};

// 78203 - Shadowy Apparitions
class spell_pri_legion_shadowy_apparitions : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadowy_apparitions() : SpellScriptLoader("spell_pri_legion_shadowy_apparitions") { }

        class spell_pri_legion_shadowy_apparitions_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_shadowy_apparitions_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOWY_APPARITION_MISSILE
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_SHADOWY_APPARITION_MISSILE, true);
                GetTarget()->SendPlaySpellVisual(eventInfo.GetActionTarget()->GetGUID(), SPELL_VISUAL_SHADOWY_APPARITION, SPELL_MISS_NONE, SPELL_MISS_NONE, SHADOWY_APPARITION_TRAVEL_SPEED, false);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_shadowy_apparitions_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_shadowy_apparitions_AuraScript();
        }
};

// 162452 - Shadowy Insight
class spell_pri_legion_shadowy_insight : public SpellScriptLoader
{
    public:
        spell_pri_legion_shadowy_insight() : SpellScriptLoader("spell_pri_legion_shadowy_insight") { }

        class spell_pri_legion_shadowy_insight_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_shadowy_insight_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_MIND_BLAST
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_PRIEST_MIND_BLAST)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_shadowy_insight_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_shadowy_insight_AuraScript();
        }
};

// 585 - Smite
class spell_pri_legion_smite : public SpellScriptLoader
{
    public:
        spell_pri_legion_smite() : SpellScriptLoader("spell_pri_legion_smite") { }

        class spell_pri_legion_smite_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_smite_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SMITE_ENEMY_DEBUFF,
                    SPELL_PRIEST_SMITE_AREA_ABSORB,
                    SPELL_PRIEST_SMITE_LEVEL_BONUS
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_PRIEST_SMITE_LEVEL_BONUS))
                {
                    int32 baseAmount = int32(GetCaster()->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_MAGIC) * 2.25f);
                    AddPct(baseAmount, GetCaster()->ToPlayer()->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE));
                    int32 absorb = baseAmount;
                    if (AuraEffect* smiteDebuff = GetHitUnit()->GetAuraEffect(SPELL_PRIEST_SMITE_ENEMY_DEBUFF, EFFECT_0, GetCaster()->GetGUID()))
                        absorb += smiteDebuff->GetAmount();
                    absorb = std::min(absorb, int32(baseAmount * 3));
                    GetCaster()->CastCustomSpell(SPELL_PRIEST_SMITE_ENEMY_DEBUFF, SPELLVALUE_BASE_POINT0, absorb, GetHitUnit(), TRIGGERED_FULL_MASK);
                    GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_SMITE_AREA_ABSORB, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_smite_SpellScript::HandleEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_smite_SpellScript();
        }
};

// 208771 - Smite
class spell_pri_legion_smite_absorb : public SpellScriptLoader
{
    public:
        spell_pri_legion_smite_absorb() : SpellScriptLoader("spell_pri_legion_smite_absorb") { }

        class spell_pri_legion_smite_absorb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_smite_absorb_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SMITE_ENEMY_DEBUFF
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32 & absorbAmount)
            {
                if (AuraEffect* smiteDebuff = dmgInfo.GetAttacker()->GetAuraEffect(SPELL_PRIEST_SMITE_ENEMY_DEBUFF, EFFECT_0, GetCasterGUID()))
                {
                    absorbAmount = smiteDebuff->GetAmount();
                    smiteDebuff->GetBase()->Remove();
                }
                else
                    absorbAmount = 0;
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_smite_absorb_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pri_legion_smite_absorb_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_smite_absorb_AuraScript();
        }
};

// 194230 - Sphere of Insanity
class spell_pri_legion_sphere_of_insanity : public SpellScriptLoader
{
    public:
        spell_pri_legion_sphere_of_insanity() : SpellScriptLoader("spell_pri_legion_sphere_of_insanity") { }

        class spell_pri_legion_sphere_of_insanity_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_sphere_of_insanity_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SPHERE_OF_INSANITY_DAMAGE,
                    SPELL_PRIEST_SPHERE_OF_INSANITY_SUMMON
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                if (SpellEffectInfo const* pctInfo = sSpellMgr->AssertSpellInfo(SPELL_PRIEST_SPHERE_OF_INSANITY_SUMMON)->GetEffect(EFFECT_2))
                {
                    int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), pctInfo->CalcValue(GetTarget()));
                    for (AuraApplication* aurApp : GetTarget()->GetTargetAuraApplications(SPELL_PRIEST_SHADOW_WORD_PAIN))
                        GetTarget()->CastCustomSpell(SPELL_PRIEST_SPHERE_OF_INSANITY_DAMAGE, SPELLVALUE_BASE_POINT0, damage, aurApp->GetTarget(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_sphere_of_insanity_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_pri_legion_sphere_of_insanity_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_sphere_of_insanity_AuraScript();
        }
};

// 215769 - Spirit of Redemption
class spell_pri_legion_spirit_of_redemption_honor_talent : public SpellScriptLoader
{
    public:
        spell_pri_legion_spirit_of_redemption_honor_talent() : SpellScriptLoader("spell_pri_legion_spirit_of_redemption_honor_talent") { }

        class spell_pri_legion_spirit_of_redemption_honor_talent_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_spirit_of_redemption_honor_talent_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->HasSpell(SPELL_PRIEST_SPIRIT_OF_THE_REDEEMER);
            }

            void RestoreHealth(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->SetHealth(GetTarget()->CountPctFromMaxHealth(aurEff->GetAmount()));
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_pri_legion_spirit_of_redemption_honor_talent_AuraScript::RestoreHealth, EFFECT_8, SPELL_AURA_SPIRIT_OF_REDEMPTION, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_spirit_of_redemption_honor_talent_AuraScript();
        }

        class spell_pri_legion_spirit_of_redemption_honor_talent_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_spirit_of_redemption_honor_talent_SpellScript);

            bool Load() override
            {
                return GetCaster()->HasSpell(SPELL_PRIEST_SPIRIT_OF_THE_REDEEMER);
            }

            void SaveHealthPct()
            {
                healthPct = int32(GetCaster()->GetHealthPct());
            }

            void SetAuraAmount()
            {
                if (Aura* aura = GetHitAura())
                    if (AuraEffect* aurEff = aura->GetEffect(EFFECT_8))
                        aurEff->SetAmount(healthPct);
            }

            void Register() override
            {
                BeforeCast += SpellCastFn(spell_pri_legion_spirit_of_redemption_honor_talent_SpellScript::SaveHealthPct);
                AfterHit += SpellHitFn(spell_pri_legion_spirit_of_redemption_honor_talent_SpellScript::SetAuraAmount);
            }

            private:
                int32 healthPct = 100;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_spirit_of_redemption_honor_talent_SpellScript();
        }
};

// 20711 - Spirit of Redemption
class spell_pri_legion_spirit_of_redemption : public SpellScriptLoader
{
    public:
        spell_pri_legion_spirit_of_redemption() : SpellScriptLoader("spell_pri_legion_spirit_of_redemption") { }

        class spell_pri_legion_spirit_of_redemption_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_spirit_of_redemption_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SPIRIT_OF_REDEMPTION,
                    SPELL_PRIEST_GUARDIAN_SPIRIT
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32 & absorbAmount)
            {
                if ((dmgInfo.GetDamage() < GetTarget()->GetHealth()) || GetTarget()->HasAura(SPELL_PRIEST_GUARDIAN_SPIRIT) || GetTarget()->HasSpell(SPELL_PRIEST_SPIRIT_OF_THE_REDEEMER))
                    return;

                absorbAmount = dmgInfo.GetDamage();
                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_SPIRIT_OF_REDEMPTION, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_spirit_of_redemption_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pri_legion_spirit_of_redemption_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_spirit_of_redemption_AuraScript();
        }
};

// 109186 - Surge of Light
class spell_pri_legion_surge_of_light : public SpellScriptLoader
{
    public:
        spell_pri_legion_surge_of_light() : SpellScriptLoader("spell_pri_legion_surge_of_light") { }

        class spell_pri_legion_surge_of_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_surge_of_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SURGE_OF_LIGHT_TRIGGERED,
                    SPELL_PRIEST_SMITE,
                    SPELL_PRIEST_HALO_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // Only direct heal can trigger this aura
                if (!eventInfo.GetSpellInfo() || !eventInfo.GetHealInfo() || (eventInfo.GetHealInfo()->GetHeal() == 0 && eventInfo.GetSpellInfo()->Id != SPELL_PRIEST_SMITE))
                    return false;

                if (eventInfo.GetSpellInfo()->Id == SPELL_PRIEST_HALO_HEAL)
                    return false;

                /// TODO: new proc system
                if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;

                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(50));

                if (AuraEffect* procChance = GetEffect(EFFECT_0))
                    return roll_chance_i(procChance->GetAmount());
                return false;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_SURGE_OF_LIGHT_TRIGGERED, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_surge_of_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_surge_of_light_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_surge_of_light_AuraScript();
        }
};

// 193223 - Surrender to Madness
class spell_pri_legion_surrender_to_madness : public SpellScriptLoader
{
    public:
        spell_pri_legion_surrender_to_madness() : SpellScriptLoader("spell_pri_legion_surrender_to_madness") { }

        class spell_pri_legion_surrender_to_madness_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_surrender_to_madness_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SURRENDER_TO_MADNESS_KILL
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_SURRENDER_TO_MADNESS_KILL, true);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_pri_legion_surrender_to_madness_AuraScript::RemoveEffect, EFFECT_2, SPELL_AURA_CAST_WHILE_WALKING_2, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_surrender_to_madness_AuraScript();
        }
};

// 219772 - Sustained Sanity
class spell_pri_legion_sustained_sanity : public SpellScriptLoader
{
    public:
        spell_pri_legion_sustained_sanity() : SpellScriptLoader("spell_pri_legion_sustained_sanity") { }

        class spell_pri_legion_sustained_sanity_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_sustained_sanity_AuraScript);

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetTarget())
                {
                    if (caster->GetTypeId() == TYPEID_PLAYER)
                    {
                        float totalValue = 0.f;
                        Unit::AuraEffectList const& auras = caster->GetAuraEffectsByType(SPELL_AURA_MOD_POWER_REGEN);
                        for (auto aura : auras)
                            if (aura->GetMiscValue() == POWER_INSANITY)
                                totalValue += aura->GetAmount();

                        totalValue = (totalValue / 500) * 100;
                        caster->SetFloatValue(UNIT_FIELD_POWER_REGEN_FLAT_MODIFIER + caster->GetPowerIndex(POWER_INSANITY), totalValue);
                        caster->SetFloatValue(UNIT_FIELD_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + caster->GetPowerIndex(POWER_INSANITY), totalValue);
                    }
                }
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_sustained_sanity_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_sustained_sanity_AuraScript();
        }
};

// 64901 - Symbol of Hope
class spell_pri_legion_symbol_of_hope : public SpellScriptLoader
{
    public:
        spell_pri_legion_symbol_of_hope() : SpellScriptLoader("spell_pri_legion_symbol_of_hope") { }

        class spell_pri_legion_symbol_of_hope_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_symbol_of_hope_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void SelectTarget(std::list<WorldObject*>& targets)
            {
                Player* caster = GetCaster()->ToPlayer();
                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    if (Player* playerTarget = target->ToPlayer())
                        return !(caster->IsInSameRaidWith(playerTarget) && playerTarget->GetRole() == TALENT_ROLE_HEALER);
                    return true;
                });
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_symbol_of_hope_SpellScript::SelectTarget, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_symbol_of_hope_SpellScript();
        }
};

// 200128 - Trail of Light
class spell_pri_legion_trail_of_light : public SpellScriptLoader
{
    public:
        spell_pri_legion_trail_of_light() : SpellScriptLoader("spell_pri_legion_trail_of_light") { }

        class spell_pri_legion_trail_of_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_trail_of_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_TRAIL_OF_LIGHT_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetHealInfo() != nullptr;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetHealInfo()->GetHeal() == 0 || !eventInfo.GetActionTarget())
                    return;

                if (!_lastTargetGuid.IsEmpty() && _lastTargetGuid != eventInfo.GetActionTarget()->GetGUID())
                {
                    int32 heal = CalculatePct(eventInfo.GetHealInfo()->GetHeal(), aurEff->GetAmount());
                    if (Unit* target = ObjectAccessor::GetUnit(*GetTarget(), _lastTargetGuid))
                        GetTarget()->CastCustomSpell(SPELL_PRIEST_TRAIL_OF_LIGHT_HEAL, SPELLVALUE_BASE_POINT0, heal, target, TRIGGERED_FULL_MASK, nullptr, aurEff);
                }

                _lastTargetGuid = eventInfo.GetActionTarget()->GetGUID();
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_trail_of_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_trail_of_light_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }

        private:
            ObjectGuid _lastTargetGuid;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_trail_of_light_AuraScript();
        }
};

// 109142 - Twist of Fate
class spell_pri_legion_twist_of_fate : public SpellScriptLoader
{
    public:
        spell_pri_legion_twist_of_fate() : SpellScriptLoader("spell_pri_legion_twist_of_fate") { }

        class spell_pri_legion_twist_of_fate_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_twist_of_fate_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_MIND_FLAY
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget() || !eventInfo.GetHealInfo())
                    return false;

                if (!GetTarget()->HasSpell(SPELL_PRIEST_MIND_FLAY))
                    if (!(eventInfo.GetTypeMask() & (PROC_FLAG_DONE_SPELL_NONE_DMG_CLASS_POS | PROC_FLAG_DONE_SPELL_MAGIC_DMG_CLASS_POS)))
                        return false;

                if (GetTarget()->HasSpell(SPELL_PRIEST_MIND_FLAY))
                    if (eventInfo.GetTypeMask() & (PROC_FLAG_DONE_SPELL_NONE_DMG_CLASS_POS | PROC_FLAG_DONE_SPELL_MAGIC_DMG_CLASS_POS))
                        return false;

                uint64 healthCap = CalculatePct(eventInfo.GetActionTarget()->GetMaxHealth(), GetEffect(EFFECT_0)->GetAmount());
                return (uint64(eventInfo.GetActionTarget()->GetHealth() - eventInfo.GetHealInfo()->GetHeal()) < healthCap);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_twist_of_fate_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_twist_of_fate_AuraScript();
        }
};

// 194093 - Unleash the Shadows
class spell_pri_legion_unleash_the_shadows : public SpellScriptLoader
{
    public:
        spell_pri_legion_unleash_the_shadows() : SpellScriptLoader("spell_pri_legion_unleash_the_shadows") { }

        class spell_pri_legion_unleash_the_shadows_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_unleash_the_shadows_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOWY_APPARITION_MISSILE
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                if (AuraEffect* procChance = GetTarget()->GetAuraEffect(GetId(), EFFECT_0))
                    return roll_chance_i(procChance->GetAmount());
                return false;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_PRIEST_SHADOWY_APPARITION_MISSILE, true);
                GetTarget()->SendPlaySpellVisual(eventInfo.GetActionTarget()->GetGUID(), SPELL_VISUAL_SHADOWY_APPARITION, SPELL_MISS_NONE, SPELL_MISS_NONE, SHADOWY_APPARITION_TRAVEL_SPEED, false);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_unleash_the_shadows_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_unleash_the_shadows_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_unleash_the_shadows_AuraScript();
        }
};

// 15286 - Vampiric Embrace
class spell_pri_legion_vampiric_embrace : public SpellScriptLoader
{
    public:
        spell_pri_legion_vampiric_embrace() : SpellScriptLoader("spell_pri_legion_vampiric_embrace") { }

        class spell_pri_legion_vampiric_embrace_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_vampiric_embrace_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_VAMPIRIC_EMBRACE_HEAL
                });
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                amount = 0;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo())
                    return false;

                if (!eventInfo.GetSpellInfo() || eventInfo.GetSpellInfo()->IsTargetingArea(DIFFICULTY_NONE)
                    || eventInfo.GetDamageInfo()->GetDamage() == 0 || eventInfo.GetSpellInfo()->SpellFamilyName != SPELLFAMILY_PRIEST)
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                const_cast<AuraEffect*>(aurEff)->SetAmount(aurEff->GetAmount() + eventInfo.GetDamageInfo()->GetDamage());
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (aurEff->GetAmount() == 0)
                    return;

                int32 heal = CalculatePct(aurEff->GetAmount(), aurEff->GetBaseAmount());
                const_cast<AuraEffect*>(aurEff)->SetAmount(0);
                GetTarget()->CastCustomSpell(SPELL_PRIEST_VAMPIRIC_EMBRACE_HEAL, SPELLVALUE_BASE_POINT0, heal, GetTarget(), TRIGGERED_FULL_MASK, nullptr, aurEff);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_legion_vampiric_embrace_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_vampiric_embrace_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_vampiric_embrace_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_vampiric_embrace_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_vampiric_embrace_AuraScript();
        }
};

// 15290 Vampiric Embrace
class spell_pri_legion_vampiric_embrace_heal : public SpellScriptLoader
{
    public:
        spell_pri_legion_vampiric_embrace_heal() : SpellScriptLoader("spell_pri_legion_vampiric_embrace_heal") { }

        class spell_pri_legion_vampiric_embrace_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_vampiric_embrace_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_FLEETING_EMBRACE
                });
            }

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                std::list<WorldObject*> allTargets = targets;
                std::list<WorldObject*> injuredTargets;

                targets.remove_if([](WorldObject* target) -> bool
                {
                    return target->ToUnit()->IsFullHealth();
                });

                if (!targets.empty())
                    injuredTargets = targets;
                else
                    targets = allTargets;

                injuredTargets.remove_if([](WorldObject* target) -> bool
                {
                    return target->GetTypeId() == TYPEID_UNIT;
                });

                if (injuredTargets.empty() && targets.empty())
                    return;

                if (GetCaster()->HasAura(SPELL_PRIEST_FLEETING_EMBRACE)) // target count is hardcoded in tooltip we cant use auraeffect
                {
                    if (targets.size() > 3)
                        Trinity::Containers::RandomResize(targets, 3);
                }
                else
                {
                    WorldObject* target = Trinity::Containers::SelectRandomContainerElement(injuredTargets.empty() ? targets : injuredTargets);
                    targets.clear();
                    targets.push_back(target);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_vampiric_embrace_heal_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_vampiric_embrace_heal_SpellScript();
        }
};

// 34914 - Vampiric Touch
class spell_pri_legion_vampiric_touch : public SpellScriptLoader
{
    public:
        spell_pri_legion_vampiric_touch() : SpellScriptLoader("spell_pri_legion_vampiric_touch") { }

        class spell_pri_legion_vampiric_touch_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_vampiric_touch_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SIN_AND_PUNISHMENT,
                    SPELL_PRIEST_SHADOW_WORD_PAIN,
                    SPELL_PRIEST_SIN_AND_PUNISHMENT
                });
            }

            void HandleApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_PRIEST_MISERY))
                        caster->CastSpell(GetTarget(), SPELL_PRIEST_SHADOW_WORD_PAIN, true);
            }

            void HandleDispel(DispelInfo* dispelInfo)
            {
                if (Unit* caster = GetCaster())
                    dispelInfo->GetDispeller()->CastSpell(dispelInfo->GetDispeller(), SPELL_PRIEST_SIN_AND_PUNISHMENT, true, nullptr, nullptr, caster->GetGUID());
            }

            void HandlePeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    if (AuraEffect* mentalFortitude = caster->GetAuraEffect(SPELL_PRIEST_MENTAL_FORTITUDE, EFFECT_0))
                        mentalFortitude->SetAmount(caster->IsFullHealth() ? 1 : 0);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_legion_vampiric_touch_AuraScript::HandleApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                AfterDispel += AuraDispelFn(spell_pri_legion_vampiric_touch_AuraScript::HandleDispel);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_vampiric_touch_AuraScript::HandlePeriodicTick, EFFECT_1, SPELL_AURA_PERIODIC_LEECH);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_vampiric_touch_AuraScript();
        }
};

// 195483 - Vim and Vigor
class spell_pri_legion_vim_and_vigor : public SpellScriptLoader
{
    public:
        spell_pri_legion_vim_and_vigor() : SpellScriptLoader("spell_pri_legion_vim_and_vigor") { }

        class spell_pri_legion_vim_and_vigor_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_vim_and_vigor_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_VIM_AND_VIGOR_BONUS
                });
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                if (GetTarget()->HealthAbovePct(GetEffect(EFFECT_1)->GetAmount()))
                    GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_VIM_AND_VIGOR_BONUS, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_VIM_AND_VIGOR_BONUS);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_vim_and_vigor_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_vim_and_vigor_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_vim_and_vigor_AuraScript();
        }
};

// 228260 - Void Eruption
class spell_pri_legion_void_eruption : public SpellScriptLoader
{
    public:
        spell_pri_legion_void_eruption() : SpellScriptLoader("spell_pri_legion_void_eruption") { }

        class spell_pri_legion_void_eruption_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_void_eruption_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_PURGE_THE_WICKED_DOT,
                    SPELL_PRIEST_VOID_ERUPTION_DAMAGE,
                    SPELL_PRIEST_VOIDFORM_CONTROLLER,
                    SPELL_PRIEST_VOIDFORM
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                ObjectGuid const& casterGuid = GetCaster()->GetGUID();
                targets.remove_if([casterGuid](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit())
                        if (!unitTarget->HasAura(SPELL_PRIEST_SHADOW_WORD_PAIN, casterGuid) && !unitTarget->HasAura(SPELL_PRIEST_VAMPIRIC_TOUCH, casterGuid))
                            return true;
                    return false;
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_VOID_ERUPTION_DAMAGE, true);
            }

            void HandleVoidForm()
            {
                GetCaster()->SetPower(POWER_INSANITY, GetCaster()->GetMaxPower(POWER_INSANITY));
                GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_VOIDFORM_CONTROLLER, true);
                GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_VOIDFORM, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_void_eruption_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_void_eruption_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_pri_legion_void_eruption_SpellScript::HandleVoidForm);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_void_eruption_SpellScript();
        }
};

// 194249 - Voidform
class spell_pri_legion_voidform : public SpellScriptLoader
{
    public:
        spell_pri_legion_voidform() : SpellScriptLoader("spell_pri_legion_voidform") { }

        class spell_pri_legion_voidform_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_voidform_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_SHADOWFORM,
                    SPELL_PRIEST_SPHERE_OF_INSANITY,
                    SPELL_PRIEST_SPHERE_OF_INSANITY_SUMMON
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->RemoveAurasDueToSpell(SPELL_PRIEST_SHADOWFORM);
                if (GetCaster()->HasAura(SPELL_PRIEST_SPHERE_OF_INSANITY))
                    GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_SPHERE_OF_INSANITY_SUMMON, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pri_legion_voidform_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_voidform_SpellScript();
        }

        class spell_pri_legion_voidform_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_voidform_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_VOID_TORRENT,
                    SPELL_PRIEST_MASS_HYSTERIA,
                    SPELL_PRIEST_SHADOWFORM,
                    SPELL_PRIEST_VOID_TORRENT,
                    SPELL_PRIEST_LINGERING_INSANITY,
                    SPELL_PRIEST_LINGERING_INSANITY_PERIODIC,
                    SPELL_PRIEST_SURRENDER_TO_MADNESS,
                    SPELL_PRIEST_SPHERE_OF_INSANITY,
                    SPELL_PRIEST_SPHERE_OF_INSANITY_SUMMON,
                    SPELL_PRIEST_SPHERE_OF_INSANITY_PROC,
                    SPELL_PRIEST_VOID_TORRENT,
                    SPELL_PRIEST_VOIDFORM
                });
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 250;
            }

            void HandleCustomTick(AuraEffect const* aurEff)
            {
                if (GetTarget()->HasAura(SPELL_PRIEST_VOID_TORRENT))
                    if (aurEff->GetAmount() != 0)
                        const_cast<AuraEffect*>(aurEff)->ChangeAmount(0);
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                GetAura()->ModStackAmount(1);
                ++_tickCount;
                if (AuraEffect* drain = GetEffect(EFFECT_1))
                    if (!GetTarget()->HasAura(SPELL_PRIEST_VOID_TORRENT))
                        drain->ChangeAmount(-int32(_tickCount * 333) + drain->GetBaseAmount());

                if (AuraEffect* haste = GetEffect(EFFECT_2))
                    haste->ChangeAmount(haste->GetBaseAmount() * GetAura()->GetStackAmount());

                if (AuraEffect* massHisteria = GetTarget()->GetAuraEffect(SPELL_PRIEST_MASS_HYSTERIA, EFFECT_0))
                    if (AuraEffect* bonusEff = GetEffect(EFFECT_9))
                        bonusEff->ChangeAmount(massHisteria->GetAmount() * GetAura()->GetStackAmount());

            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_PRIEST_LINGERING_INSANITY))
                    GetTarget()->CastCustomSpell(SPELL_PRIEST_LINGERING_INSANITY_PERIODIC, SPELLVALUE_AURA_STACK, GetAura()->GetStackAmount(), GetTarget(), TRIGGERED_FULL_MASK);

                GetTarget()->CastSpell(GetTarget(), SPELL_PRIEST_SHADOWFORM, true);
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_VOIDFORM_CONTROLLER);
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_SURRENDER_TO_MADNESS);
                GetTarget()->RemoveAurasDueToSpell(SPELL_PRIEST_SPHERE_OF_INSANITY_PROC);

                // copy list to prevent iterator invalidation
                std::set<Unit*> controlled = GetTarget()->m_Controlled;
                for (Unit* minion : controlled)
                    if (minion->GetEntry() == NPC_SPHERE_OF_INSANITY)
                        minion->ToCreature()->DespawnOrUnsummon();
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_pri_legion_voidform_AuraScript::CalcPeriodic, EFFECT_1, SPELL_AURA_MOD_POWER_REGEN);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_voidform_AuraScript::HandleCustomTick, EFFECT_1, SPELL_AURA_MOD_POWER_REGEN);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_pri_legion_voidform_AuraScript::HandleUpdatePeriodic, EFFECT_1, SPELL_AURA_MOD_POWER_REGEN);

                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_legion_voidform_AuraScript::HandleDummyTick, EFFECT_4, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_legion_voidform_AuraScript::RemoveEffect, EFFECT_4, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }

        private:
            uint32 _tickCount = 0;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_voidform_AuraScript();
        }
};

// 234746 - Void Bolt
class spell_pri_legion_void_bolt : public SpellScriptLoader
{
    public:
        spell_pri_legion_void_bolt() : SpellScriptLoader("spell_pri_legion_void_bolt") { }

        class spell_pri_legion_void_bolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_void_bolt_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_PRIEST_SHADOW_WORD_PAIN,
                    SPELL_PRIEST_VAMPIRIC_TOUCH,
                    SPELL_PRIEST_VOID_BOLT_BONUS
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_PRIEST_VOID_BOLT_BONUS);
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                ObjectGuid const& casterGuid = GetCaster()->GetGUID();
                targets.remove_if([casterGuid](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit())
                        if (!unitTarget->HasAura(SPELL_PRIEST_SHADOW_WORD_PAIN, casterGuid) && !unitTarget->HasAura(SPELL_PRIEST_VAMPIRIC_TOUCH, casterGuid))
                            return true;
                    return false;
                });
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* timeBonus = GetCaster()->GetAuraEffect(SPELL_PRIEST_VOID_BOLT_BONUS, EFFECT_0))
                {
                    if (Aura* shadowWordPain = GetHitUnit()->GetAura(SPELL_PRIEST_SHADOW_WORD_PAIN, GetCaster()->GetGUID()))
                    {
                        int32 newDuration = shadowWordPain->GetDuration() + timeBonus->GetAmount();
                        if (newDuration > shadowWordPain->GetMaxDuration())
                            shadowWordPain->SetMaxDuration(newDuration);
                        shadowWordPain->SetDuration(newDuration);
                    }

                    if (Aura* vampiricTouch = GetHitUnit()->GetAura(SPELL_PRIEST_VAMPIRIC_TOUCH, GetCaster()->GetGUID()))
                    {
                        int32 newDuration = vampiricTouch->GetDuration() + timeBonus->GetAmount();
                        if (newDuration > vampiricTouch->GetMaxDuration())
                            vampiricTouch->SetMaxDuration(newDuration);
                        vampiricTouch->SetDuration(newDuration);
                    }
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_legion_void_bolt_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_void_bolt_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_void_bolt_SpellScript();
        }
};

// 199145 - Void Shield
class spell_pri_legion_void_shield : public SpellScriptLoader
{
    public:
        spell_pri_legion_void_shield() : SpellScriptLoader("spell_pri_legion_void_shield") { }

        class spell_pri_legion_void_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_legion_void_shield_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_VOID_SHIELD_HEAL,
                    SPELL_PRIEST_VOID_SHIELD_HONOR_TALENT
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                int32 heal = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), sSpellMgr->AssertSpellInfo(SPELL_PRIEST_VOID_SHIELD_HONOR_TALENT)->GetEffect(EFFECT_0)->CalcValue(GetTarget()));

                if (heal <= 0)
                    return;

                GetTarget()->CastCustomSpell(SPELL_PRIEST_VOID_SHIELD_HEAL, SPELLVALUE_BASE_POINT0, heal, GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_legion_void_shield_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_legion_void_shield_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_legion_void_shield_AuraScript();
        }
};

// 108968 - Void Shift
class spell_pri_legion_void_shift : public SpellScriptLoader
{
    public:
        spell_pri_legion_void_shift() : SpellScriptLoader("spell_pri_legion_void_shift") { }

        class spell_pri_legion_void_shift_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_legion_void_shift_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_PRIEST_VOID_SHIFT
                });
            }

            ///@ TODO: for some reason one of the players dies if both < 5% hp ?!
            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                float casterHealthPct = GetCaster()->GetHealthPct();
                float targetHealthPct = GetHitUnit()->GetHealthPct();

                if (casterHealthPct < float(GetEffectValue()) || targetHealthPct < float(GetEffectValue()))
                {
                    if (casterHealthPct < targetHealthPct)
                        casterHealthPct = float(GetEffectValue());
                    else
                        targetHealthPct = float(GetEffectValue());
                }
                bool casterDamage = targetHealthPct < casterHealthPct;

                uint64 casterHealOrDamage = int32(CalculatePct(GetCaster()->GetMaxHealth(), targetHealthPct));
                uint64 targetHealOrDamage = int32(CalculatePct(GetHitUnit()->GetMaxHealth(), casterHealthPct));

                if (casterHealOrDamage <= GetCaster()->GetHealth())
                    casterHealOrDamage -= GetCaster()->GetHealth();
                else
                    casterHealOrDamage = GetCaster()->GetHealth() - casterHealOrDamage;

                if (targetHealOrDamage <= GetHitUnit()->GetHealth())
                    targetHealOrDamage -= GetHitUnit()->GetHealth();
                else
                    targetHealOrDamage = GetHitUnit()->GetHealth() - targetHealOrDamage;

                GetCaster()->CastCustomSpell(SPELL_PRIEST_VOID_SHIFT, casterDamage ? SPELLVALUE_BASE_POINT0 : SPELLVALUE_BASE_POINT1, std::abs(int32(casterHealOrDamage)), GetCaster(), TRIGGERED_FULL_MASK);
                GetCaster()->CastCustomSpell(SPELL_PRIEST_VOID_SHIFT, !casterDamage ? SPELLVALUE_BASE_POINT0 : SPELLVALUE_BASE_POINT1, std::abs(int32(targetHealOrDamage)), GetHitUnit(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_legion_void_shift_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_legion_void_shift_SpellScript();
        }
};

// 158624 - Angelic Feather
class areatrigger_pri_legion_angelic_feather : public AreaTriggerEntityScript
{
    public:
        areatrigger_pri_legion_angelic_feather() : AreaTriggerEntityScript("areatrigger_pri_legion_angelic_feather") { }

        struct areatrigger_pri_legion_angelic_featherAI : public AreaTriggerAI
        {
            areatrigger_pri_legion_angelic_featherAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnBirth()
            {
                for (ObjectGuid const& guid : at->GetInsideUnits())
                    if (guid == at->GetCasterGuid())
                    {
                        casterInsidePrio = true;
                        break;
                    }
            }

            void OnUnitEnter(Unit* unit) override
            {
                if (casterInsidePrio && unit->GetGUID() != at->GetCasterGuid())
                    return;

                // Angelic Feather should only trigger one time
                if (isTriggered)
                    return;

                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsFriendlyTo(unit))
                    {
                        isTriggered = true;
                        caster->CastSpell(unit, SPELL_PRIEST_ANGELIC_FEATHER_SPEED_BUFF, true);
                        at->Remove();
                    }
                }
            }

            private:
                bool isTriggered = false;
                bool casterInsidePrio = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pri_legion_angelic_featherAI(areaTrigger);
        }
};

// 110744 - Divine Star
class areatrigger_pri_legion_divine_star : public AreaTriggerEntityScript
{
    public:
        areatrigger_pri_legion_divine_star() : AreaTriggerEntityScript("areatrigger_pri_legion_divine_star") { }

        struct areatrigger_pri_legion_divine_starAI : public AreaTriggerAI
        {
            areatrigger_pri_legion_divine_starAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnDestinationReached() override
            {
                at->AI()->DoAction(ACTION_RESHAPE);
            }

            void DoAction(int32 action) override
            {
                if (!_reshapeActive && action == ACTION_RESHAPE)
                {
                    _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                    {
                        if (Unit* caster = at->GetCaster())
                        {
                            if (!_reshapeActive || caster->GetDistance(_casterPosition) > 2.00f)
                            {
                                _reshapeActive = true;
                                _casterPosition = caster->GetPosition();

                                Movement::PointsArray reshapePoints;
                                for (uint8 i = 0; i < 4; i++)
                                {
                                    G3D::Vector3 point;
                                    point.x = (i < 2) ? at->GetPositionX() : caster->GetPositionX();
                                    point.y = (i < 2) ? at->GetPositionY() : caster->GetPositionY();
                                    point.z = (i < 2) ? at->GetPositionZ() : caster->GetPositionZ();
                                    reshapePoints.push_back(point);
                                }
                                at->InitSplines(reshapePoints, (at->GetDistance(caster) / 24) * 1000);
                            }
                        }
                        task.Repeat(Milliseconds(250));
                    });
                }
                else
                    at->Remove();
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsFriendlyTo(target) && caster->IsValidAssistTarget(target))
                        caster->CastSpell(target, SPELL_PRIEST_DIVINE_STAR_HEAL, true);
                    else if (caster->IsValidAttackTarget(target))
                        caster->CastSpell(target, SPELL_PRIEST_DIVINE_STAR_DAMAGE, true);
                }
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
                Position _casterPosition;
                bool _reshapeActive = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pri_legion_divine_starAI(areaTrigger);
        }
};

// 120517 - Halo
class areatrigger_pri_legion_halo : public AreaTriggerEntityScript
{
    public:
        areatrigger_pri_legion_halo() : AreaTriggerEntityScript("areatrigger_pri_legion_halo") { }

        struct areatrigger_pri_legion_haloAI : public AreaTriggerAI
        {
            areatrigger_pri_legion_haloAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsFriendlyTo(target) && caster->IsValidAssistTarget(target))
                    {
                        if (at->GetExactDist(target) <= sSpellMgr->AssertSpellInfo(at->GetSpellId())->GetMaxRange(true, caster))
                            caster->CastSpell(target, SPELL_PRIEST_HALO_HEAL, true);
                    }
                    else if (caster->IsValidAttackTarget(target) && at->GetExactDist(target) <= sSpellMgr->AssertSpellInfo(at->GetSpellId())->GetMaxRange(false, caster))
                        caster->CastSpell(target, SPELL_PRIEST_HALO_DAMAGE, true);
                }
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_pri_legion_haloAI(areaTrigger);
        }
};

void AddSC_legion_priest_spell_scripts()
{
    // Spells
    new spell_pri_legion_angelic_feather();
    new spell_pri_legion_archangel();
    new spell_pri_legion_atonement();
    new spell_pri_legion_atonement_buff();
    new spell_pri_legion_atonement_helper();
    new spell_pri_legion_binding_heal();
    new spell_pri_legion_call_of_the_void();
    new spell_pri_legion_circle_of_healing();
    new spell_pri_legion_clarity_of_will();
    new spell_pri_legion_dark_archangel();
    new spell_pri_legion_desperate_prayer();
    new spell_pri_legion_divine_hymn();
    new spell_pri_legion_dispersion();
    new spell_pri_legion_enduring_renewal();
    new spell_pri_legion_fade();
    new spell_pri_legion_flash_heal();
    new spell_pri_legion_focus_in_the_light();
    new spell_pri_legion_guardian_spirit();
    new spell_pri_legion_holy_fire_passive();
    new spell_pri_legion_holy_word_chastise();
    new spell_pri_legion_holy_word_sanctify();
    new spell_pri_legion_invoke_the_naaru_summon_proc();
    new spell_pri_legion_invoke_the_naaru_copy_proc();
    new spell_pri_legion_last_word();
    new spell_pri_legion_leap_of_faith();
    new spell_pri_legion_leap_of_faith_effect_trigger();
    new spell_pri_legion_levitate();
    new spell_pri_legion_light_s_wrath();
    new spell_pri_legion_lingering_insanity();
    new spell_pri_legion_mana_leech_proc();
    new spell_pri_legion_mania();
    new spell_pri_legion_masochism_heal();
    new spell_pri_legion_mastery_echo_of_light();
    new spell_pri_legion_mental_fortitude();
    new spell_pri_legion_mind_bomb();
    new spell_pri_legion_mind_blast();
    new spell_pri_legion_mind_control();
    new spell_pri_legion_mind_flay();
    new spell_pri_legion_mind_flay_aoe_selector();
    new spell_pri_legion_penance();
    new spell_pri_legion_penance_triggered();
    new spell_pri_legion_penance_heal_damage();
    new spell_pri_legion_power_word_radiance();
    new spell_pri_legion_prayer_of_mending_initial();
    new spell_pri_legion_prayer_of_mending_bounce();
    new spell_pri_legion_prayer_of_mending_proc();
    new spell_pri_legion_prayer_of_mending_bounce_selector();
    new spell_pri_legion_premonition();
    new spell_pri_legion_premonition_damage();
    new spell_pri_legion_psychic_link();
    new spell_pri_legion_psychic_link_damage();
    new spell_pri_legion_psyflay();
    new spell_pri_legion_power_of_the_naaru();
    new spell_pri_legion_power_word_solace();
    new spell_pri_legion_power_word_shield();
    new spell_pri_legion_purge_the_wicked_selector();
    new spell_pri_legion_purify();
    new spell_pri_legion_rapture();
    new spell_pri_legion_ray_of_hope();
    new spell_pri_legion_renew();
    new spell_pri_legion_renew_the_faith();
    new spell_pri_legion_searing_light();
    new spell_pri_legion_serendipity();
    new spell_pri_legion_shadowform();
    new spell_pri_legion_shadow_covenant();
    new spell_pri_legion_shadow_word_death();
    new spell_pri_legion_shadow_word_death_marker();
    new spell_pri_legion_shadow_word_pain();
    new spell_pri_legion_shadow_mania();
    new spell_pri_legion_shadow_mend_heal();
    new spell_pri_legion_shadow_mend_periodic();
    new spell_pri_legion_shadowy_apparitions();
    new spell_pri_legion_shadowy_insight();
    new spell_pri_legion_smite();
    new spell_pri_legion_smite_absorb();
    new spell_pri_legion_sphere_of_insanity();
    new spell_pri_legion_spirit_of_redemption_honor_talent();
    new spell_pri_legion_spirit_of_redemption();
    new spell_pri_legion_surge_of_light();
    new spell_pri_legion_surrender_to_madness();
    new spell_pri_legion_sustained_sanity();
    new spell_pri_legion_symbol_of_hope();
    new spell_pri_legion_trail_of_light();
    new spell_pri_legion_twist_of_fate();
    new spell_pri_legion_unleash_the_shadows();
    new spell_pri_legion_vampiric_embrace();
    new spell_pri_legion_vampiric_embrace_heal();
    new spell_pri_legion_vampiric_touch();
    new spell_pri_legion_vim_and_vigor();
    new spell_pri_legion_void_eruption();
    new spell_pri_legion_voidform();
    new spell_pri_legion_void_bolt();
    new spell_pri_legion_void_shield();
    new spell_pri_legion_void_shift();

    // AreaTriggers
    new areatrigger_pri_legion_angelic_feather();
    new areatrigger_pri_legion_divine_star();
    new areatrigger_pri_legion_halo();
}
