/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_DEATHKNIGHT and SPELLFAMILY_GENERIC spells used by deathknight players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_dk_".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "Containers.h"
#include "Object.h"
#include "ScriptMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "Unit.h"

enum DeathknightSpells
{
    SPELL_DK_ARMY_FLESH_BEAST_TRANSFORM           = 127533,
    SPELL_DK_ARMY_GEIST_TRANSFORM                 = 127534,
    SPELL_DK_ARMY_NORTHREND_SKELETON_TRANSFORM    = 127528,
    SPELL_DK_ARMY_SKELETON_TRANSFORM              = 127527,
    SPELL_DK_ARMY_SPIKED_GHOUL_TRANSFORM          = 127525,
    SPELL_DK_ARMY_SUPER_ZOMBIE_TRANSFORM          = 127526,
    SPELL_DK_DEATH_AND_DECAY                      = 43265,
    SPELL_DK_DEATH_AND_DECAY_AOE_BONUS            = 188290,
    SPELL_DK_DEATH_AND_DECAY_DAMAGE               = 52212,
    SPELL_DK_DEATH_AND_DECAY_DUMMY                = 227591,
    SPELL_DK_DEFILE_DAMAGE                        = 156000,
    SPELL_DK_DEFILE_DUMMY_AURA                    = 156004,
    SPELL_DK_GLYPH_OF_FOUL_MENAGERIE              = 58642,
    SPELL_DK_PILLAR_OF_FROST                      = 51271,
    SPELL_DK_TIGHTENING_GRASP                     = 206970,
    SPELL_DK_TIGHTENING_GRASP_SNARE               = 143375,
};

static const uint32 ArmyTransforms[6]
{
    SPELL_DK_ARMY_FLESH_BEAST_TRANSFORM,
    SPELL_DK_ARMY_GEIST_TRANSFORM,
    SPELL_DK_ARMY_NORTHREND_SKELETON_TRANSFORM,
    SPELL_DK_ARMY_SKELETON_TRANSFORM,
    SPELL_DK_ARMY_SPIKED_GHOUL_TRANSFORM,
    SPELL_DK_ARMY_SUPER_ZOMBIE_TRANSFORM
};

// 127517 - Army Transform
class spell_dk_legion_army_transform : public SpellScriptLoader
{
public:
    spell_dk_legion_army_transform() : SpellScriptLoader("spell_dk_legion_army_transform") { }

    class spell_dk_legion_army_transform_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_dk_legion_army_transform_SpellScript);

        bool Load() override
        {
            return GetCaster()->IsGuardian();
        }

        SpellCastResult CheckCast()
        {
            if (Unit* owner = GetCaster()->GetOwner())
                if (owner->HasAura(SPELL_DK_GLYPH_OF_FOUL_MENAGERIE))
                    return SPELL_CAST_OK;

            return SPELL_FAILED_SPELL_UNAVAILABLE;
        }

        void HandleDummy(SpellEffIndex /*effIndex*/)
        {
            GetCaster()->CastSpell(GetCaster(), ArmyTransforms[urand(0, 5)], true);
        }

        void Register() override
        {
            OnCheckCast += SpellCheckCastFn(spell_dk_legion_army_transform_SpellScript::CheckCast);
            OnEffectHitTarget += SpellEffectFn(spell_dk_legion_army_transform_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_dk_legion_army_transform_SpellScript();
    }
};

// 43265 - Death and Decay
class spell_dk_legion_death_and_decay : public SpellScriptLoader
{
    public:
        spell_dk_legion_death_and_decay() : SpellScriptLoader("spell_dk_legion_death_and_decay") { }

        class spell_dk_legion_death_and_decay_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_dk_legion_death_and_decay_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DK_DEATH_AND_DECAY_DUMMY
                });
            }

            void HandleDummyEffect(SpellEffIndex /*eff*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_DK_DEATH_AND_DECAY_DUMMY, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_dk_legion_death_and_decay_SpellScript::HandleDummyEffect, EFFECT_2, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_dk_legion_death_and_decay_SpellScript();
        }

        class spell_dk_legion_death_and_decay_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dk_legion_death_and_decay_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_DK_DEATH_AND_DECAY_DAMAGE
                });
            }

            void HandlePeriodic(AuraEffect const* /*eff*/)
            {
                Unit* owner = GetUnitOwner();
                if (!owner)
                    return;

                if (AreaTrigger const* trigger = owner->GetAreaTrigger(GetId()))
                    owner->CastSpell(trigger->GetPositionX(), trigger->GetPositionY(), trigger->GetPositionZ(), SPELL_DK_DEATH_AND_DECAY_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_dk_legion_death_and_decay_AuraScript::HandlePeriodic, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dk_legion_death_and_decay_AuraScript();
        }
};

// 207142 Avalanche
class spell_dk_legion_frost_avalanche : public AuraScript
{
    PrepareAuraScript(spell_dk_legion_frost_avalanche);

    bool CheckProc(ProcEventInfo& /*eventInfo*/)
    {
        return GetTarget()->HasAura(SPELL_DK_PILLAR_OF_FROST);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dk_legion_frost_avalanche::CheckProc);
    }
};

// 1713 - Defile
class areatrigger_dk_legion_defile : public AreaTriggerEntityScript
{
    public:
        areatrigger_dk_legion_defile() : AreaTriggerEntityScript("areatrigger_dk_legion_defile") { }

        struct areatrigger_dk_legion_defileAI : public AreaTriggerAI
        {
            areatrigger_dk_legion_defileAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            //void OnPeriodic() override
            //{
            //    Unit* caster = at->GetCaster();
            //    if (!caster)
            //        return;
            //
            //    GuidUnorderedSet triggers = at->GetHostileInsideUnits();
            //
            //    // "Defile the targeted ground, dealing ${($156000s1*($d+1)/$t3)} Shadowfrost damage to all enemies over $d."
            //    for (auto itr : triggers)
            //    {
            //        if (Unit* target = ObjectAccessor::GetUnit(*caster, itr))
            //        {
            //            caster->CastSpell(target, SPELL_DK_DEFILE_DAMAGE, true);
            //            caster->CastSpell(target, SPELL_DK_DEFILE_DUMMY_AURA, true);
            //        }
            //    }
            //}

            void OnUnitEnter(Unit* unit) override
            {
                if (unit->GetGUID() == at->GetCasterGuid())
                    // "While you remain within your Defile, your [...] will hit all enemies near the target."
                    unit->CastCustomSpell(SPELL_DK_DEATH_AND_DECAY_AOE_BONUS, SPELLVALUE_AURA_DURATION, at->GetDuration(), unit, true);
            }

            void OnUnitExit(Unit* unit) override
            {
                if (unit->GetGUID() == at->GetCasterGuid())
                    unit->RemoveAurasDueToSpell(SPELL_DK_DEATH_AND_DECAY_AOE_BONUS);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_dk_legion_defileAI(areaTrigger);
        }
};

// 4485 - Death and Decay
class areatrigger_dk_legion_death_and_decay : public AreaTriggerEntityScript
{
    public:
        areatrigger_dk_legion_death_and_decay() : AreaTriggerEntityScript("areatrigger_dk_legion_death_and_decay") { }

        struct areatrigger_dk_legion_death_and_decayAI : public AreaTriggerAI
        {
            areatrigger_dk_legion_death_and_decayAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                if (Unit* caster = at->GetCaster())
                    // Tightening Grasp (talent) - "your Death and Decay also reduces the movement speed of enemies within its radius by 70%."
                    if (caster->HasAura(SPELL_DK_TIGHTENING_GRASP))
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DK_TIGHTENING_GRASP_SNARE, true);
            }

            void OnUnitEnter(Unit* unit) override
            {
                if (unit->GetGUID() == at->GetCasterGuid())
                    // "While you remain within the area, your [...] will hit all enemies near the target."
                    unit->CastCustomSpell(SPELL_DK_DEATH_AND_DECAY_AOE_BONUS, SPELLVALUE_AURA_DURATION, at->GetDuration(), unit, TRIGGERED_FULL_MASK);
            }

            void OnUnitExit(Unit* unit) override
            {
                if (unit->GetGUID() == at->GetCasterGuid())
                    unit->RemoveAurasDueToSpell(SPELL_DK_DEATH_AND_DECAY_AOE_BONUS);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_dk_legion_death_and_decayAI(areaTrigger);
        }
};

void AddSC_legion_deathknight_spell_scripts()
{
    new spell_dk_legion_army_transform();
    new spell_dk_legion_death_and_decay();
    RegisterAuraScript(spell_dk_legion_frost_avalanche);

    // Areatriggers
    new areatrigger_dk_legion_defile();
    new areatrigger_dk_legion_death_and_decay();
}
