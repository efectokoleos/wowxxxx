/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_WARRIOR and SPELLFAMILY_GENERIC spells used by warrior players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_warr_".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "Cell.h"
#include "CellImpl.h"
#include "DB2Stores.h"
#include "GridNotifiers.h"
#include "Map.h"
#include "Object.h"
#include "ObjectAccessor.h"
#include "PassiveAI.h"
#include "PathGenerator.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Spell.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"

enum WarriorSpells
{
    SPELL_GENERIC_COPY_WEAPON                   = 63416,
    SPELL_WARRIOR_BATTLE_CRY                    = 1719,
    SPELL_WARRIOR_BATTLE_SCARS_AURA             = 200954,
    SPELL_WARRIOR_BATTLE_SCARS_TRAIT            = 200857,
    SPELL_WARRIOR_BATTLE_TRANCE                 = 213858,
    SPELL_WARRIOR_BEST_SERVED_COLD              = 202560,
    SPELL_WARRIOR_BEST_SERVED_COLD_ENERGIZE     = 202559, // unused since 7.1.5
    SPELL_WARRIOR_BLOODBATH_PERIODIC            = 113344,
    SPELL_WARRIOR_BLOODCRAZE                    = 200859,
    SPELL_WARRIOR_BLOODTHIRST_HEAL              = 117313,
    SPELL_WARRIOR_BOUNDING_STRIDE               = 202163,
    SPELL_WARRIOR_BOUNDING_STRIDE_SPEED         = 202164,
    SPELL_WARRIOR_CHARGE                        = 100,
    SPELL_WARRIOR_CHARGE_ROOT                   = 105771,
    SPELL_WARRIOR_CHARGE_SLOW                   = 236027,
    SPELL_WARRIOR_CHARGE_TRIGGERED              = 198337, /// @TODO: Implement triggered spells (glyphed effect), 109128 ??? extra energize + interrupt maybe talent/trait related?
    SPELL_WARRIOR_CLEAVE_LEVEL_BONUS            = 231833,
    SPELL_WARRIOR_CLEAVE_LEVEL_BONUS_AURA       = 188923,
    SPELL_WARRIOR_COLOSSUS_SMASH                = 167105,
    SPELL_WARRIOR_COLOSSUS_SMASH_DEBUFF         = 208086,
    SPELL_WARRIOR_COMMANDING_SHOUT_TRIGGERED    = 97463,
    SPELL_WARRIOR_CORRUPTED_BLOOD_OF_ZAKAJZ_PERIODIC = 209569,
    SPELL_WARRIOR_DEATH_SENTENCE_HONOR_TALENT   = 198500,
    SPELL_WARRIOR_DEFENSIVE_STANCE_ALLIANCE     = 147925,
    SPELL_WARRIOR_DEFENSIVE_STANCE_HORDE        = 146127,
    SPELL_WARRIOR_DEMORALIZING_SHOUT            = 1160,
    SPELL_WARRIOR_DUEL_CASTER_AREATRIGGER_EFFECT= 237261,
    SPELL_WARRIOR_DUEL_ENEMY_AREATRIGGER_EFFECT = 236276,
    SPELL_WARRIOR_ENRAGE_TRIGGERED              = 184362,
    SPELL_WARRIOR_EXECUTE_ARMS                  = 163201,
    SPELL_WARRIOR_EXECUTE_FURY                  = 5308,
    SPELL_WARRIOR_EXECUTE_OFF_HAND_FURY         = 163558,
    SPELL_WARRIOR_FERVOR_OF_BATTLE              = 202316,
    SPELL_WARRIOR_FOCUS_IN_CHAOS_BONUS          = 200876,
    SPELL_WARRIOR_FOCUS_IN_CHAOS_TRAIT          = 200871,
    SPELL_WARRIOR_FOCUSED_RAGE                  = 207982,
    SPELL_WARRIOR_FOCUSED_RAGE_PROTECTION       = 204488,
    SPELL_WARRIOR_FURIOUS_CHARGE                = 202224,
    SPELL_WARRIOR_FURIOUS_CHARGE_TRIGGERED      = 202225,
    SPELL_WARRIOR_FURIOUS_SLASH_BONUS           = 231824,
    SPELL_WARRIOR_HEAVY_REPERCUSSIONS           = 203177,
    SPELL_WARRIOR_HEROIC_LEAP_JUMP              = 162052,
    SPELL_WARRIOR_IGNORE_PAIN                   = 190456,
    SPELL_WARRIOR_IMPENDING_VICTORY             = 202168,
    SPELL_WARRIOR_IMPENDING_VICTORY_HEAL        = 202166,
    SPELL_WARRIOR_INSPIRING_PRESENCE_HEAL       = 222944,
    SPELL_WARRIOR_INTERCEPT_CHARGE_ALLY         = 198760,
    SPELL_WARRIOR_INTO_THE_FRAY                 = 202602,
    SPELL_WARRIOR_LAST_STAND                    = 12975,
    SPELL_WARRIOR_LEAVE_NO_MAN_BEHIND_AURA      = 199038,
    SPELL_WARRIOR_LEAVE_NO_MAN_BEHIND_TALENT    = 199037,
    SPELL_WARRIOR_MASSACRE_BONUS                = 206316,
    SPELL_WARRIOR_MIGHT_OF_THE_VRYKUL_BONUS     = 188783,
    SPELL_WARRIOR_MIGHT_OF_THE_VRYKUL_TALENT    = 188778,
    SPELL_WARRIOR_MORALE_KILLER_HONOR_TALENT    = 199023,
    SPELL_WARRIOR_MORTAL_STRIKE                 = 12294,
    SPELL_WARRIOR_MORTAL_STRIKE_HEAL_REDUCE     = 198819,
    SPELL_WARRIOR_MORTAL_WOUNDS                 = 115804,
    SPELL_WARRIOR_NEVER_SURRENDER               = 202561,
    SPELL_WARRIOR_ONE_AGAINST_MANY_TRAIT        = 209462,
    SPELL_WARRIOR_OUTBURST                      = 206320,
    SPELL_WARRIOR_PAIN_TRAIN                    = 198765,
    SPELL_WARRIOR_PRECISE_STRIKES_BONUS         = 209493,
    SPELL_WARRIOR_RAVAGER_DAMAGE                = 156287,
    SPELL_WARRIOR_RAVAGER_PARRY_BONUS           = 227744,
    SPELL_WARRIOR_RAVAGER_PROTECTION            = 228920,
    SPELL_WARRIOR_RAVAGER_SUMMON                = 227876,
    SPELL_WARRIOR_REFLECTIVE_PLATING            = 188672,
    SPELL_WARRIOR_REVENGE                       = 6572,
    SPELL_WARRIOR_SAFEGUARD                     = 223657,
    SPELL_WARRIOR_SAFEGUARD_SPLIT_DAMAGE_AURA   = 223658,
    SPELL_WARRIOR_SECOND_WIND_BLOCKER           = 202149,
    SPELL_WARRIOR_SHADOW_OF_THE_COLOSSUS        = 198807,
    SPELL_WARRIOR_SHARPEN_BLADE                 = 198817,
    SPELL_WARRIOR_SHIELD_BLOCK_TRIGGERED        = 132404,
    SPELL_WARRIOR_SHIELD_SLAM                   = 23922,
    SPELL_WARRIOR_SHIELD_SLAM_EVENT_MARKER      = 224324,
    SPELL_WARRIOR_SHIELD_SLAM_LEVEL_BONUS       = 231834,
    SPELL_WARRIOR_SHIELD_WALL                   = 871,
    SPELL_WARRIOR_SHOCKWAVE_STUN                = 132168,
    SPELL_WARRIOR_SLAM                          = 1464,
    SPELL_WARRIOR_SLAUGHTERHOUSE                = 199204,
    SPELL_WARRIOR_STORM_BOLT_STUN               = 132169,
    SPELL_WARRIOR_SWEEPING_STRIKES              = 202161,
    SPELL_WARRIOR_TACTICAL_ADVANCE_BONUS        = 209484,
    SPELL_WARRIOR_TACTICAL_ADVANCE_TRAIT        = 209483,
    SPELL_WARRIOR_TASTE_FOR_BLOOD               = 206333,
    SPELL_WARRIOR_TAUNT                         = 355,
    SPELL_WARRIOR_THUNDERSTRUCK_HONOR_TALENT    = 199045,
    SPELL_WARRIOR_THUNDERSTRUCK_ROOT            = 199042,
    SPELL_WARRIOR_TOUCH_OF_ZAKAJZ_HEAL          = 209933,
    SPELL_WARRIOR_TRAUMA_PERIODIC               = 215537,
    SPELL_WARRIOR_UNRIVALED_STRENGTH_BONUS      = 200977,
    SPELL_WARRIOR_VENGEANCE_IGNORE_PAIN         = 202574,
    SPELL_WARRIOR_VENGEANCE_REVENGE             = 202573,
    SPELL_WARRIOR_VICTORIOUS                    = 32216,
    SPELL_WARRIOR_VICTORY_RUSH_HEAL             = 118779,
    SPELL_WARRIOR_VOID_CLEAVE                   = 209700,
    SPELL_WARRIOR_VOID_CLEAVE_TRAIT             = 209573,
    SPELL_WARRIOR_WAR_BANNER_BUFF               = 236321,
    SPELL_WARRIOR_WARBRINGER                    = 103828,
    SPELL_WARRIOR_WARBRINGER_DAMAGE             = 213427,
    SPELL_WARRIOR_WARPATH_HONOR_TALENT          = 199086,
    SPELL_WARRIOR_WARPATH_STUN                  = 199085,
    SPELL_WARRIOR_WHIRLWIND_TRIGGERED           = 199658,
    SPELL_WARRIOR_WHIRLWIND_TRIGGERED_2         = 199850,
    SPELL_WARRIOR_WRECKING_BALL                 = 215570,
};

enum MiscData
{
    SPELL_VISUAL_WARRIOR_RAVAGER    = 36990,
};

// 152278 - Anger Management
class spell_warr_legion_anger_management : public SpellScriptLoader
{
    public:
        spell_warr_legion_anger_management() : SpellScriptLoader("spell_warr_legion_anger_management") { }

        class spell_warr_legion_anger_management_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_anger_management_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetPowerCost(POWER_RAGE) > 0;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                if (Player* target = GetTarget()->ToPlayer())
                {
                    int32 spentRage = eventInfo.GetProcSpell()->GetPowerCost(POWER_RAGE);

                    int32 bonus = spentRage / (10 * aurEff->GetAmount());
                    target->GetSpellHistory()->ModifyCooldown(SPELL_WARRIOR_BATTLE_CRY, -(1000 * bonus));

                    if (target->GetRole() == TALENT_ROLE_TANK)
                    {
                        target->GetSpellHistory()->ModifyCooldown(SPELL_WARRIOR_LAST_STAND, -(1000 * bonus));
                        target->GetSpellHistory()->ModifyCooldown(SPELL_WARRIOR_SHIELD_WALL, -(1000 * bonus));
                        target->GetSpellHistory()->ModifyCooldown(SPELL_WARRIOR_DEMORALIZING_SHOUT, -(1000 * bonus));
                    }
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_anger_management_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_anger_management_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_anger_management_AuraScript();
        }
};

// 107574 - Avatar
class spell_warr_legion_avatar : public SpellScriptLoader
{
    public:
        spell_warr_legion_avatar() : SpellScriptLoader("spell_warr_legion_avatar") { }

        class spell_warr_legion_avatar_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_avatar_SpellScript);

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->RemoveMovementImpairingAuras();
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_avatar_SpellScript::HandleScriptEffect, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_avatar_SpellScript();
        }
};

// 1719 - Battle Cry
class spell_warr_legion_battle_cry : public SpellScriptLoader
{
    public:
        spell_warr_legion_battle_cry() : SpellScriptLoader("spell_warr_legion_battle_cry") { }

        class spell_warr_legion_battle_cry_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_battle_cry_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_WARRIOR_PAIN_TRAIN });
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_WARRIOR_PAIN_TRAIN))
                    if (!GetHitUnit()->HasAura(GetSpellInfo()->Id))
                        GetCaster()->AddAura(GetSpellInfo()->Id, GetHitUnit());
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_battle_cry_SpellScript::HandleScriptEffect, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_battle_cry_SpellScript();
        }
};

// 213857 - Battle Trance
class spell_warr_legion_battle_trance : public SpellScriptLoader
{
    public:
        spell_warr_legion_battle_trance() : SpellScriptLoader("spell_warr_legion_battle_trance") { }

        class spell_warr_legion_battle_trance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_battle_trance_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_BATTLE_TRANCE
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetActor() == eventInfo.GetActionTarget())
                    return;

                if (lastTargetGuid == eventInfo.GetActionTarget()->GetGUID())
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_BATTLE_TRANCE, true);
                else
                {
                    GetTarget()->RemoveAurasDueToSpell(SPELL_WARRIOR_BATTLE_TRANCE);
                    lastTargetGuid = eventInfo.GetActionTarget()->GetGUID();
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_battle_trance_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
            private:
                ObjectGuid lastTargetGuid;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_battle_trance_AuraScript();
        }
};

// 18499 - Berserker Rage
class spell_warr_legion_berserker_rage : public SpellScriptLoader
{
    public:
        spell_warr_legion_berserker_rage() : SpellScriptLoader("spell_warr_legion_berserker_rage") { }

        class spell_warr_legion_berserker_rage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_berserker_rage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_OUTBURST,
                    SPELL_WARRIOR_ENRAGE_TRIGGERED
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_WARRIOR_OUTBURST))
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_ENRAGE_TRIGGERED, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_berserker_rage_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_berserker_rage_SpellScript();
        }
};

// 12292 - Bloodbath
class spell_warr_legion_bloodbath : public SpellScriptLoader
{
    public:
        spell_warr_legion_bloodbath() : SpellScriptLoader("spell_warr_legion_bloodbath") { }

        class spell_warr_legion_bloodbath_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_bloodbath_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_BLOODBATH_PERIODIC
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo())
                    return false;

                if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_WARRIOR_BLOODBATH_PERIODIC)
                    return false;
                return true;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()) / sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_BLOODBATH_PERIODIC)->GetMaxTicks(DIFFICULTY_NONE);
                damage += std::min(int32(eventInfo.GetActionTarget()->GetRemainingPeriodicAmount(GetTarget()->GetGUID(), SPELL_WARRIOR_BLOODBATH_PERIODIC, SPELL_AURA_PERIODIC_DAMAGE)), INT32_MAX);
                GetTarget()->CastCustomSpell(eventInfo.GetActionTarget(), SPELL_WARRIOR_BLOODBATH_PERIODIC, &damage, nullptr, nullptr, true, nullptr, aurEff); /// @TODO: figure out what amount0 is overwriting AFTER custom cast... (damage/amount is always 0)
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_bloodbath_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_bloodbath_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_bloodbath_AuraScript();
        }
};

// 23881 - Bloodthirst
class spell_warr_legion_bloodthirst : public SpellScriptLoader
{
    public:
        spell_warr_legion_bloodthirst() : SpellScriptLoader("spell_warr_legion_bloodthirst") { }

        class spell_warr_legion_bloodthirst_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_bloodthirst_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_BLOODTHIRST_HEAL,
                    SPELL_WARRIOR_SLAUGHTERHOUSE
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_BLOODTHIRST_HEAL, true);
            }

            void CalculateBonusDamage(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* slaughterhouse = GetCaster()->GetAuraEffect(SPELL_WARRIOR_SLAUGHTERHOUSE, EFFECT_0))
                {
                    if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_SLAUGHTERHOUSE)->GetEffect(EFFECT_2))
                    {
                        float pctBonusCount = floor((100.00f - GetHitUnit()->GetHealthPct()) / float(effInfo->CalcValue(GetCaster())));
                        if (pctBonusCount > 0.00f)
                        {
                            SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), (slaughterhouse->GetAmount() * pctBonusCount)));
                            GetCaster()->GetSpellHistory()->ModifyCooldown(GetSpellInfo()->Id, std::chrono::milliseconds(-int32(pctBonusCount * 1000)));
                        }
                    }
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_bloodthirst_SpellScript::HandleAfterCast);
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_bloodthirst_SpellScript::CalculateBonusDamage, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_bloodthirst_SpellScript();
        }
};

// 117313 - Bloodthirst Heal
class spell_warr_legion_bloodthirst_heal : public SpellScriptLoader
{
    public:
        spell_warr_legion_bloodthirst_heal() : SpellScriptLoader("spell_warr_legion_bloodthirst_heal") { }

        class spell_warr_legion_bloodthirst_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_bloodthirst_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_BLOODCRAZE
                });
            }

            void CalculateHealPct(SpellEffIndex /*effIndex*/)
            {
                int32 effectValue = GetEffectValue();
                if (AuraEffect* bloodcraze = GetCaster()->GetAuraEffect(SPELL_WARRIOR_BLOODCRAZE, EFFECT_0))
                    if (GetCaster()->HealthBelowPct(20))
                        effectValue += bloodcraze->GetAmount();
                SetEffectValue(effectValue);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_bloodthirst_heal_SpellScript::CalculateHealPct, EFFECT_0, SPELL_EFFECT_HEAL_PCT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_bloodthirst_heal_SpellScript();
        }
};

// 213871 - Bodyguard
class spell_warr_legion_bodyguard : public SpellScriptLoader
{
    public:
        spell_warr_legion_bodyguard() : SpellScriptLoader("spell_warr_legion_bodyguard") { }

        class spell_warr_legion_bodyguard_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_bodyguard_AuraScript);

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_SHIELD_SLAM, true);
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (Unit* caster = GetCaster())
                    if (GetTarget()->GetDistance(caster) > float(aurEff->GetAmount()))
                        Remove();
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_bodyguard_AuraScript::OnProc, EFFECT_0, SPELL_AURA_SPLIT_DAMAGE_PCT);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warr_legion_bodyguard_AuraScript::HandleDummyTick, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_bodyguard_AuraScript();
        }
};

// 100 - Charge
class spell_warr_legion_charge : public SpellScriptLoader
{
    public:
        spell_warr_legion_charge() : SpellScriptLoader("spell_warr_legion_charge") { }

        class spell_warr_legion_charge_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_charge_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_CHARGE_TRIGGERED,
                    SPELL_WARRIOR_CHARGE_ROOT,
                });
            }

            SpellCastResult CheckCast()
            {
                if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
                    return SPELL_FAILED_ROOTED;
                return SPELL_CAST_OK;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_CHARGE_TRIGGERED, true);
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_CHARGE_ROOT, true);
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warr_legion_charge_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_charge_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_charge_SpellScript();
        }
};

// 126664 - Charge
class spell_warr_legion_charge_triggered : public SpellScriptLoader
{
    public:
        spell_warr_legion_charge_triggered() : SpellScriptLoader("spell_warr_legion_charge_triggered") { }

        class spell_warr_legion_charge_triggered_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_charge_triggered_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_FURIOUS_CHARGE,
                    SPELL_WARRIOR_FURIOUS_CHARGE_TRIGGERED,
                    SPELL_WARRIOR_SHADOW_OF_THE_COLOSSUS,
                    SPELL_WARRIOR_CHARGE_SLOW
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_CHARGE_SLOW, true);
                if (GetCaster()->HasAura(SPELL_WARRIOR_FURIOUS_CHARGE))
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_FURIOUS_CHARGE_TRIGGERED, true);
                if (GetCaster()->HasAura(SPELL_WARRIOR_SHADOW_OF_THE_COLOSSUS))
                    GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_COLOSSUS_SMASH, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_charge_triggered_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_charge_triggered_SpellScript();
        }
};

// 845 - Cleave
class spell_warr_legion_cleave : public SpellScriptLoader
{
    public:
        spell_warr_legion_cleave() : SpellScriptLoader("spell_warr_legion_cleave") { }

        class spell_warr_legion_cleave_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_cleave_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_CLEAVE_LEVEL_BONUS,
                    SPELL_WARRIOR_CLEAVE_LEVEL_BONUS_AURA,
                    SPELL_WARRIOR_VOID_CLEAVE_TRAIT,
                    SPELL_WARRIOR_VOID_CLEAVE
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_WARRIOR_CLEAVE_LEVEL_BONUS);
            }

            void CountTargets(std::list<WorldObject*>& targets)
            {
                targetCount = targets.size();
            }

            void HandleAfterCast()
            {
                int32 addValue = 0;
                if (SpellEffectInfo const* levelBonus = sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_CLEAVE_LEVEL_BONUS_AURA)->GetEffect(EFFECT_0))
                    addValue += levelBonus->CalcValue(GetCaster());

                if (AuraEffect* oneAgainstMany = GetCaster()->GetAuraEffect(SPELL_WARRIOR_ONE_AGAINST_MANY_TRAIT, EFFECT_0))
                    addValue += oneAgainstMany->GetAmount();

                CustomSpellValues values;
                values.AddSpellMod(SPELLVALUE_BASE_POINT0, addValue);
                values.AddSpellMod(SPELLVALUE_AURA_STACK, std::min(5, targetCount));
                GetCaster()->CastCustomSpell(SPELL_WARRIOR_CLEAVE_LEVEL_BONUS_AURA, values, GetCaster(), TRIGGERED_FULL_MASK);

                ///@TODO: new proc system (?) current proc system cant count targets thats why its directly implemented here
                if (AuraEffect* voidCleaveTrait = GetCaster()->GetAuraEffect(SPELL_WARRIOR_VOID_CLEAVE_TRAIT, EFFECT_0))
                    if (targetCount >= voidCleaveTrait->GetAmount())
                        GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_VOID_CLEAVE, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warr_legion_cleave_SpellScript::CountTargets, EFFECT_1, TARGET_UNIT_CONE_ENEMY_104);
                AfterCast += SpellCastFn(spell_warr_legion_cleave_SpellScript::HandleAfterCast);
            }

            private:
                int32 targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_cleave_SpellScript();
        }
};

// 167105 - Colossus Smash
// 209577 - Warbreaker
class spell_warr_legion_colossus_smash : public SpellScriptLoader
{
    public:
        spell_warr_legion_colossus_smash() : SpellScriptLoader("spell_warr_legion_colossus_smash") { }

        class spell_warr_legion_colossus_smash_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_colossus_smash_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_COLOSSUS_SMASH_DEBUFF
                });
            }

            void HandleDebuff(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_COLOSSUS_SMASH_DEBUFF, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_colossus_smash_SpellScript::HandleDebuff, EFFECT_FIRST_FOUND, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_colossus_smash_SpellScript();
        }
};

// 97462 - Commanding Shout
class spell_warr_legion_commanding_shout : public SpellScriptLoader
{
    public:
        spell_warr_legion_commanding_shout() : SpellScriptLoader("spell_warr_legion_commanding_shout") { }

        class spell_warr_legion_commanding_shout_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_commanding_shout_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_COMMANDING_SHOUT_TRIGGERED
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                int32 bonusHealth = CalculatePct(GetHitUnit()->GetMaxHealth(), GetEffectValue());
                GetCaster()->CastCustomSpell(GetHitUnit(), SPELL_WARRIOR_COMMANDING_SHOUT_TRIGGERED, &bonusHealth, nullptr, nullptr, true); // currently unknown for what effect 1 is used.
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_commanding_shout_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_commanding_shout_SpellScript();
        }
};

// 209567 - Corrupted Blood of Zakajz
class spell_warr_legion_corrupted_blood_of_zakajz : public SpellScriptLoader
{
    public:
        spell_warr_legion_corrupted_blood_of_zakajz() : SpellScriptLoader("spell_warr_legion_corrupted_blood_of_zakajz") { }

        class spell_warr_legion_corrupted_blood_of_zakajz_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_corrupted_blood_of_zakajz_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_CORRUPTED_BLOOD_OF_ZAKAJZ_PERIODIC
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo())
                    return false;

                if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_WARRIOR_CORRUPTED_BLOOD_OF_ZAKAJZ_PERIODIC)
                    return false;
                return true;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()) / sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_CORRUPTED_BLOOD_OF_ZAKAJZ_PERIODIC)->GetMaxTicks(DIFFICULTY_NONE);
                damage += std::min(int32(eventInfo.GetActionTarget()->GetRemainingPeriodicAmount(GetTarget()->GetGUID(), SPELL_WARRIOR_CORRUPTED_BLOOD_OF_ZAKAJZ_PERIODIC, SPELL_AURA_PERIODIC_DAMAGE)), INT32_MAX);
                GetTarget()->CastCustomSpell(eventInfo.GetActionTarget(), SPELL_WARRIOR_CORRUPTED_BLOOD_OF_ZAKAJZ_PERIODIC, &damage, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_corrupted_blood_of_zakajz_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_corrupted_blood_of_zakajz_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_corrupted_blood_of_zakajz_AuraScript();
        }
};

// 115768 - Deep Wounds
class spell_warr_legion_deep_wounds : public SpellScriptLoader
{
    public:
        spell_warr_legion_deep_wounds() : SpellScriptLoader("spell_warr_legion_deep_wounds") { }

        class spell_warr_legion_deep_wounds_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_deep_wounds_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // prevent proc on friendly targets
                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_deep_wounds_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_deep_wounds_AuraScript();
        }
};

// 197690 - Defensive Stance
class spell_warr_legion_defensive_stance : public SpellScriptLoader
{
    public:
        spell_warr_legion_defensive_stance() : SpellScriptLoader("spell_warr_legion_defensive_stance") { }

        class spell_warr_legion_defensive_stance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_defensive_stance_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_DEFENSIVE_STANCE_ALLIANCE,
                    SPELL_WARRIOR_DEFENSIVE_STANCE_HORDE
                });
            }

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->ToPlayer()->GetTeam() == HORDE)
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_DEFENSIVE_STANCE_HORDE, true);
                else
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_DEFENSIVE_STANCE_ALLIANCE, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_WARRIOR_DEFENSIVE_STANCE_HORDE);
                GetTarget()->RemoveAurasDueToSpell(SPELL_WARRIOR_DEFENSIVE_STANCE_ALLIANCE);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_warr_legion_defensive_stance_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_warr_legion_defensive_stance_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_defensive_stance_AuraScript();
        }
};

// 1160 - Demoralizing Shout
class spell_warr_legion_demoralizing_shout : public SpellScriptLoader
{
    public:
        spell_warr_legion_demoralizing_shout() : SpellScriptLoader("spell_warr_legion_demoralizing_shout") { }

        class spell_warr_legion_demoralizing_shout_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_demoralizing_shout_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_MORALE_KILLER_HONOR_TALENT
                });
            }

            void CheckHonorTalent(SpellEffIndex effIndex)
            {
                if ((effIndex == EFFECT_0 && GetCaster()->HasAura(SPELL_WARRIOR_MORALE_KILLER_HONOR_TALENT))
                    || (effIndex == EFFECT_1 && !GetCaster()->HasAura(SPELL_WARRIOR_MORALE_KILLER_HONOR_TALENT)))
                    PreventHitEffect(effIndex);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_demoralizing_shout_SpellScript::CheckHonorTalent, EFFECT_0, SPELL_EFFECT_TRIGGER_SPELL);
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_demoralizing_shout_SpellScript::CheckHonorTalent, EFFECT_1, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_demoralizing_shout_SpellScript();
        }
};

// 125565 - Demoralizing Shout
class spell_warr_legion_demoralizing_shout_absorb : public SpellScriptLoader
{
    public:
        spell_warr_legion_demoralizing_shout_absorb() : SpellScriptLoader("spell_warr_legion_demoralizing_shout_absorb") { }

        class spell_warr_legion_demoralizing_shout_absorb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_demoralizing_shout_absorb_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_DEMORALIZING_SHOUT
                });
            }

            void CalculateAmount(AuraEffect const* /*auraEffect*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                amount = -1;
            }

            void OnAbsorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_DEMORALIZING_SHOUT)->GetEffect(EFFECT_0))
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), std::abs(effInfo->CalcValue(GetTarget())));
            }

            void AfterApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_WARRIOR_MIGHT_OF_THE_VRYKUL_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_MIGHT_OF_THE_VRYKUL_BONUS, true);
            }

            void AfterRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_WARRIOR_MIGHT_OF_THE_VRYKUL_BONUS);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_warr_legion_demoralizing_shout_absorb_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_warr_legion_demoralizing_shout_absorb_AuraScript::OnAbsorb, EFFECT_0);
                AfterEffectApply += AuraEffectApplyFn(spell_warr_legion_demoralizing_shout_absorb_AuraScript::AfterApply, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_warr_legion_demoralizing_shout_absorb_AuraScript::AfterRemove, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_demoralizing_shout_absorb_AuraScript();
        }
};

// 236279 - Devastator
class spell_warr_legion_devastator : public SpellScriptLoader
{
    public:
        spell_warr_legion_devastator() : SpellScriptLoader("spell_warr_legion_devastator") { }

        class spell_warr_legion_devastator_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_devastator_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_SHIELD_SLAM,
                    SPELL_WARRIOR_SHIELD_SLAM_EVENT_MARKER
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (GetTarget()->GetSpellHistory()->HasCooldown(SPELL_WARRIOR_SHIELD_SLAM)) // and have a $s2% chance to reset the remaining cooldown on Shield Slam.
                {
                    if (SpellEffectInfo const* eff1Info = GetSpellInfo()->GetEffect(EFFECT_1))
                    {
                        if (roll_chance_i(eff1Info->CalcValue(GetTarget())))
                        {
                            GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_SHIELD_SLAM, true);
                            GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_SHIELD_SLAM_EVENT_MARKER, true);
                        }
                    }
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_devastator_AuraScript::OnProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_devastator_AuraScript();
        }
};

// 198877 - Endless Rage
class spell_warr_legion_endless_rage : public SpellScriptLoader
{
    public:
        spell_warr_legion_endless_rage() : SpellScriptLoader("spell_warr_legion_endless_rage") { }

        class spell_warr_legion_endless_rage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_endless_rage_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_ENRAGE_TRIGGERED
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Aura* enrage = GetTarget()->GetAura(SPELL_WARRIOR_ENRAGE_TRIGGERED, GetTarget()->GetGUID()))
                    enrage->RefreshDuration(true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_endless_rage_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_endless_rage_AuraScript();
        }
};

// 184362 - Enrage
class spell_warr_legion_enrage : public SpellScriptLoader
{
    public:
        spell_warr_legion_enrage() : SpellScriptLoader("spell_warr_legion_enrage") { }

        class spell_warr_legion_enrage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_enrage_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_FOCUS_IN_CHAOS_TRAIT,
                    SPELL_WARRIOR_FOCUS_IN_CHAOS_BONUS,
                    SPELL_WARRIOR_BATTLE_SCARS_TRAIT,   /// @TODO: new proc system
                    SPELL_WARRIOR_BATTLE_SCARS_AURA
                });
            }

            void HandleEffectApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_WARRIOR_FOCUS_IN_CHAOS_TRAIT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_FOCUS_IN_CHAOS_BONUS, true);

                if (AuraEffect* battleScars = GetTarget()->GetAuraEffect(SPELL_WARRIOR_BATTLE_SCARS_TRAIT, EFFECT_0))
                    GetTarget()->CastCustomSpell(SPELL_WARRIOR_BATTLE_SCARS_AURA, SPELLVALUE_BASE_POINT0, battleScars->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void HandleEffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_WARRIOR_FOCUS_IN_CHAOS_BONUS);
                GetTarget()->RemoveAurasDueToSpell(SPELL_WARRIOR_BATTLE_SCARS_AURA);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_warr_legion_enrage_AuraScript::HandleEffectApply, EFFECT_1, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                AfterEffectRemove += AuraEffectRemoveFn(spell_warr_legion_enrage_AuraScript::HandleEffectRemove, EFFECT_1, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_enrage_AuraScript();
        }
};

// 5308 - Execute
// 163201 - Execute
class spell_warr_legion_execute : public SpellScriptLoader
{
    public:
        spell_warr_legion_execute() : SpellScriptLoader("spell_warr_legion_execute") { }

        class spell_warr_legion_execute_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_execute_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_DEATH_SENTENCE_HONOR_TALENT,
                    SPELL_WARRIOR_CHARGE_ROOT,
                    SPELL_WARRIOR_CHARGE_TRIGGERED,
                    SPELL_WARRIOR_EXECUTE_FURY,
                    SPELL_WARRIOR_EXECUTE_OFF_HAND_FURY,
                    SPELL_WARRIOR_SWEEPING_STRIKES
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (GetCaster()->HasAura(SPELL_WARRIOR_SWEEPING_STRIKES))
                {
                    if (Player* caster = GetCaster()->ToPlayer())
                    {
                        targets.clear();

                        uint32 maxTargets = GetSpellInfo()->GetEffect(EFFECT_1)->ChainTargets;
                        caster->ApplySpellMod(GetSpellInfo()->Id, SPELLMOD_JUMP_TARGETS, maxTargets);

                        std::list<Unit*> unitTargets;
                        Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(caster, GetExplTargetUnit(), 5.0f);
                        Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(caster, unitTargets, u_check);
                        Cell::VisitAllObjects(GetExplTargetUnit(), searcher, 5.0f);

                        unitTargets.remove(GetExplTargetUnit());
                        unitTargets.remove_if([caster](Unit* target)
                        {
                            return !caster->IsValidAttackTarget(target);
                        });

                        if (uint32(unitTargets.size()) > maxTargets)
                            Trinity::Containers::RandomResize(unitTargets, maxTargets);

                        for (Unit* target : unitTargets)
                            targets.push_back(target);
                    }
                }
            }

            void CalculatePower(SpellEffIndex /*effIndex*/)
            {
                if (GetSpellInfo()->Id == SPELL_WARRIOR_EXECUTE_ARMS)
                {
                    if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_3))
                    {
                        if (effInfo->CalcValue() <= 0)
                            return;

                        int32 maxPctBonus = effInfo->CalcValue(GetCaster()) * 10;
                        int32 pctBonus = GetCaster()->GetPower(POWER_RAGE) - maxPctBonus;
                        if (pctBonus < 0)
                            energizeAmount = maxPctBonus + pctBonus;
                        else
                            energizeAmount = maxPctBonus;
                    }
                }
            }

            void HandleEffect(SpellEffIndex /*effIndex*/)
            {
                if (energizeAmount != 0)
                {
                    int32 damage = GetHitDamage();
                    AddPct(damage, energizeAmount);
                    SetHitDamage(damage);
                }

                if (GetSpellInfo()->Id == SPELL_WARRIOR_EXECUTE_FURY && GetCaster()->haveOffhandWeapon())
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_EXECUTE_OFF_HAND_FURY, true);

                ///@TODO: verify charge spell we don't have a warrior with honor level 37 available...
                if (GetCaster()->HasAura(SPELL_WARRIOR_DEATH_SENTENCE_HONOR_TALENT))
                {
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_CHARGE_ROOT, true);
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_CHARGE_TRIGGERED, true);
                }
            }

            void CalculateEnergize(SpellEffIndex /*effIndex*/)
            {
                SetEffectValue(-energizeAmount);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warr_legion_execute_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_TARGET_ENEMY);
                OnEffectLaunchTarget += SpellEffectFn(spell_warr_legion_execute_SpellScript::CalculatePower, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_execute_SpellScript::HandleEffect, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
                if (m_scriptSpellId == SPELL_WARRIOR_EXECUTE_ARMS)
                    OnEffectLaunchTarget += SpellEffectFn(spell_warr_legion_execute_SpellScript::CalculateEnergize, EFFECT_2, SPELL_EFFECT_ENERGIZE);
            }

            private:
                int32 energizeAmount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_execute_SpellScript();
        }
};

// 215571 - Frothing Berserker
class spell_warr_legion_frothing_berserker : public SpellScriptLoader
{
    public:
        spell_warr_legion_frothing_berserker() : SpellScriptLoader("spell_warr_legion_frothing_berserker") { }

        class spell_warr_legion_frothing_berserker_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_frothing_berserker_AuraScript);


            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return GetTarget()->GetPower(POWER_RAGE) == GetTarget()->GetMaxPower(POWER_RAGE);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_frothing_berserker_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_frothing_berserker_AuraScript();
        }
};

// 100130 - Furious Slash
class spell_warr_legion_furious_slash : public SpellScriptLoader
{
    public:
        spell_warr_legion_furious_slash() : SpellScriptLoader("spell_warr_legion_furious_slash") { }

        class spell_warr_legion_furious_slash_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_furious_slash_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_FURIOUS_SLASH_BONUS,
                    SPELL_WARRIOR_TASTE_FOR_BLOOD
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_WARRIOR_FURIOUS_SLASH_BONUS))
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_TASTE_FOR_BLOOD, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_furious_slash_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_furious_slash_SpellScript();
        }
};

// Heroic leap - 6544
class spell_warr_legion_heroic_leap : public SpellScriptLoader
{
    public:
        spell_warr_legion_heroic_leap() : SpellScriptLoader("spell_warr_legion_heroic_leap") { }

        class spell_warr_legion_heroic_leap_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_heroic_leap_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_HEROIC_LEAP_JUMP,
                    SPELL_WARRIOR_BOUNDING_STRIDE,
                    SPELL_WARRIOR_BOUNDING_STRIDE_SPEED,
                    SPELL_WARRIOR_TAUNT,
                    SPELL_WARRIOR_TACTICAL_ADVANCE_TRAIT,
                    SPELL_WARRIOR_TACTICAL_ADVANCE_BONUS
                });
            }

            SpellCastResult CheckElevation()
            {
                if (WorldLocation const* dest = GetExplTargetDest())
                {
                    if (GetCaster()->HasUnitMovementFlag(MOVEMENTFLAG_ROOT))
                        return SPELL_FAILED_ROOTED;

                    if (GetCaster()->GetMap()->Instanceable())
                    {
                        float range = GetSpellInfo()->GetMaxRange(true, GetCaster()) * 1.5f;

                        PathGenerator generatedPath(GetCaster());
                        generatedPath.SetPathLengthLimit(range);

                        bool result = generatedPath.CalculatePath(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), false, true);
                        if (generatedPath.GetPathType() & PATHFIND_SHORT)
                            return SPELL_FAILED_OUT_OF_RANGE;
                        else if (!result || generatedPath.GetPathType() & PATHFIND_NOPATH)
                        {
                            result = generatedPath.CalculatePath(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), false, false);
                            if (generatedPath.GetPathType() & PATHFIND_SHORT)
                                return SPELL_FAILED_OUT_OF_RANGE;
                            else if (!result || generatedPath.GetPathType() & PATHFIND_NOPATH)
                                return SPELL_FAILED_NOPATH;
                        }
                    }
                    else if (dest->GetPositionZ() > GetCaster()->GetPositionZ() + 4.0f)
                        return SPELL_FAILED_NOPATH;

                    return SPELL_CAST_OK;
                }

                return SPELL_FAILED_NO_VALID_TARGETS;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                ///@TODO: replace this with the correct spell after effect 254 is implemented
                if (WorldLocation* dest = GetHitDest())
                    GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_WARRIOR_HEROIC_LEAP_JUMP, true);

                if (GetCaster()->HasAura(SPELL_WARRIOR_BOUNDING_STRIDE))
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_BOUNDING_STRIDE_SPEED, true);

                if (GetCaster()->GetTypeId() == TYPEID_PLAYER && GetCaster()->ToPlayer()->GetRole() == TALENT_ROLE_TANK)
                    GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_TAUNT, true);

                ///@TODO: add this to the correct jump spell after effect 254 is fixed
                if (AuraEffect* tacticalAdvance = GetCaster()->GetAuraEffect(SPELL_WARRIOR_TACTICAL_ADVANCE_TRAIT, EFFECT_0))
                {
                    int32 bonus = tacticalAdvance->GetAmount();
                    GetCaster()->CastCustomSpell(GetCaster(), SPELL_WARRIOR_TACTICAL_ADVANCE_BONUS, &bonus, &bonus, nullptr, true);
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warr_legion_heroic_leap_SpellScript::CheckElevation);
                OnEffectHit += SpellEffectFn(spell_warr_legion_heroic_leap_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_heroic_leap_SpellScript();
        }
};


// 52174 - Heroic Leap
class spell_warr_legion_heroic_leap_damage : public SpellScriptLoader
{
    public:
        spell_warr_legion_heroic_leap_damage() : SpellScriptLoader("spell_warr_legion_heroic_leap_damage") { }

        class spell_warr_legion_heroic_leap_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_heroic_leap_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_WARPATH_HONOR_TALENT,
                    SPELL_WARRIOR_WARPATH_STUN
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_WARRIOR_WARPATH_HONOR_TALENT);
            }

            void HandleHonorTalent(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_WARPATH_STUN, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_heroic_leap_damage_SpellScript::HandleHonorTalent, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_heroic_leap_damage_SpellScript();
        }
};

// 190456 - Ignore Pain
class spell_warr_legion_ignore_pain : public SpellScriptLoader
{
    public:
        spell_warr_legion_ignore_pain() : SpellScriptLoader("spell_warr_legion_ignore_pain") { }

        class spell_warr_legion_ignore_pain_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_ignore_pain_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_NEVER_SURRENDER,
                    SPELL_WARRIOR_VENGEANCE_IGNORE_PAIN,
                    SPELL_WARRIOR_VENGEANCE_REVENGE
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void SaveOldAmount()
            {
                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(GetSpellInfo()->Id, EFFECT_0))
                    _oldAuraAmount = aurEff->GetAmount();
            }

            void CalculateAbsorb()
            {
                int32 powerCost = GetSpell()->GetPowerCost(POWER_RAGE);
                float baseAmount = GetCaster()->GetTotalAttackPowerValue(BASE_ATTACK) * (float(GetSpellInfo()->GetEffect(EFFECT_0)->CalcValue(GetCaster())) / 10);
                AddPct(baseAmount, GetCaster()->ToPlayer()->GetRatingBonusValue(CR_VERSATILITY_DAMAGE_DONE));
                if (SpellEffectInfo const* pctInfo = GetSpellInfo()->GetEffect(EFFECT_1))
                    baseAmount = CalculatePct(baseAmount, float(pctInfo->CalcValue(GetCaster())));

                if (AuraEffect const* neverSurrender = GetCaster()->GetAuraEffect(SPELL_WARRIOR_NEVER_SURRENDER, EFFECT_0))
                    AddPct(baseAmount, (float(neverSurrender->GetAmount()) / 100) * (100.00f - GetCaster()->GetHealthPct()));

                uint32 minPowerCost = 0;
                uint32 maxPowerCost = 0;
                std::vector<SpellPowerEntry const*> powers = sDB2Manager.GetSpellPowers(GetSpellInfo()->Id, DIFFICULTY_NONE);
                if (!powers.empty())
                {
                    minPowerCost = powers.front()->ManaCost;
                    maxPowerCost = powers.front()->ManaCostAdditional + minPowerCost;
                    GetCaster()->ToPlayer()->ApplySpellMod(GetSpellInfo()->Id, SPELLMOD_COST, minPowerCost);
                    if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_WARRIOR_VENGEANCE_IGNORE_PAIN, EFFECT_1))
                        maxPowerCost += aurEff->GetAmount();
                }

                // all done, lets save the cap value before we calculate the amount based on power spend
                int32 cap = int32(baseAmount * 3);
                // Power based amount calculation
                int32 auraAmount = int32(baseAmount) * powerCost / int32(maxPowerCost);

                if (_oldAuraAmount > 0)
                    auraAmount = std::min(cap, (int32(auraAmount) + _oldAuraAmount));

                if (Aura* painAura = GetHitAura())
                    if (AuraEffect* aurEff = painAura->GetEffect(EFFECT_0))
                        aurEff->ChangeAmount(auraAmount);
            }

            void Register() override
            {
                BeforeCast += SpellCastFn(spell_warr_legion_ignore_pain_SpellScript::SaveOldAmount);
                AfterHit += SpellHitFn(spell_warr_legion_ignore_pain_SpellScript::CalculateAbsorb);
            }

        private:
            int32 _oldAuraAmount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_ignore_pain_SpellScript();
        }

        class spell_warr_legion_ignore_pain_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_ignore_pain_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;
                return true;
            }

            void OnAbsorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                int32 damage = CalculatePct(int32(dmgInfo.GetDamage()), GetAura()->GetSpellEffectInfo(EFFECT_1)->CalcValue(GetTarget()));
                absorbAmount = std::min(aurEff->GetAmount(), damage);
            }

            void Register() override
            {
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_warr_legion_ignore_pain_AuraScript::OnAbsorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_ignore_pain_AuraScript();
        }
};

// 202168 - Impending Victory
class spell_warr_legion_impending_victory : public SpellScriptLoader
{
    public:
        spell_warr_legion_impending_victory() : SpellScriptLoader("spell_warr_legion_impending_victory") { }

        class spell_warr_legion_impending_victory_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_impending_victory_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_IMPENDING_VICTORY_HEAL
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_IMPENDING_VICTORY_HEAL, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_impending_victory_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_impending_victory_SpellScript();
        }
};

// 215550 - In For The Kill
class spell_warr_legion_in_for_the_kill : public SpellScriptLoader
{
    public:
        spell_warr_legion_in_for_the_kill() : SpellScriptLoader("spell_warr_legion_in_for_the_kill") { }

        class spell_warr_legion_in_for_the_kill_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_in_for_the_kill_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_MORTAL_STRIKE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo() || eventInfo.GetSpellInfo()->Id != SPELL_WARRIOR_MORTAL_STRIKE
                    || !eventInfo.GetActionTarget() || eventInfo.GetActionTarget()->GetHealthPct() > 20.00f)
                    return false;
                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_in_for_the_kill_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_in_for_the_kill_AuraScript();
        }
};

// 205484 - Inspiring Presence
class spell_warr_legion_inspiring_presence : public SpellScriptLoader
{
    public:
        spell_warr_legion_inspiring_presence() : SpellScriptLoader("spell_warr_legion_inspiring_presence") { }

        class spell_warr_legion_inspiring_presence_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_inspiring_presence_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_INSPIRING_PRESENCE_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                // The heal part should only affect group members but not the caster itself
                if (GetTarget()->GetGUID() == GetCasterGUID())
                    return;

                int32 healAmount = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
                GetTarget()->CastCustomSpell(GetTarget(), SPELL_WARRIOR_INSPIRING_PRESENCE_HEAL, &healAmount, nullptr, nullptr, true, nullptr, nullptr, GetCasterGUID());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_inspiring_presence_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_inspiring_presence_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_inspiring_presence_AuraScript();
        }
};

// 222944 - Inspiring Presence
class spell_warr_legion_inspiring_presence_heal : public SpellScriptLoader
{
    public:
        spell_warr_legion_inspiring_presence_heal() : SpellScriptLoader("spell_warr_legion_inspiring_presence_heal") { }

        class spell_warr_legion_inspiring_presence_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_inspiring_presence_heal_SpellScript);

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.clear();
                targets.push_back(GetCaster());
            }

            void Register()
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warr_legion_inspiring_presence_heal_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_inspiring_presence_heal_SpellScript();
        }
};

// 198304 - Intercept
class spell_warr_legion_intercept : public SpellScriptLoader
{
    public:
        spell_warr_legion_intercept() : SpellScriptLoader("spell_warr_legion_intercept") { }

        class spell_warr_legion_intercept_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_intercept_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_CHARGE_TRIGGERED,
                    SPELL_WARRIOR_INTERCEPT_CHARGE_ALLY,
                    SPELL_WARRIOR_SAFEGUARD,
                    SPELL_WARRIOR_SAFEGUARD_SPLIT_DAMAGE_AURA,
                    SPELL_WARRIOR_CHARGE_SLOW,
                    SPELL_WARRIOR_WARBRINGER,
                    SPELL_WARRIOR_WARBRINGER_DAMAGE,
                    SPELL_WARRIOR_LEAVE_NO_MAN_BEHIND_TALENT,
                    SPELL_WARRIOR_LEAVE_NO_MAN_BEHIND_AURA
                });
            }

            SpellCastResult CheckTarget()
            {
                if (GetExplTargetUnit())
                {
                    if (!GetCaster()->IsFriendlyTo(GetExplTargetUnit()))
                        if (GetCaster()->GetDistance(GetExplTargetUnit()) < sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_CHARGE_TRIGGERED)->GetMinRange())
                            return SPELL_FAILED_TOO_CLOSE;
                }

                if (GetExplTargetUnit() == GetCaster())
                {
                    SetCustomCastResultMessage(SPELL_CUSTOM_ERROR_NO_TARGET);
                    return SPELL_FAILED_CUSTOM_ERROR;
                }

                if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
                    return SPELL_FAILED_ROOTED;

                return SPELL_CAST_OK;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (!GetCaster()->isInFront(GetHitUnit()))
                    GetCaster()->SetOrientation(GetCaster()->GetAngle(GetHitUnit()));

                if (GetCaster()->IsFriendlyTo(GetHitUnit()))
                {
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_INTERCEPT_CHARGE_ALLY, true);

                    if (GetCaster()->HasAura(SPELL_WARRIOR_SAFEGUARD))
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_SAFEGUARD_SPLIT_DAMAGE_AURA, true);

                    if (GetCaster()->HasAura(SPELL_WARRIOR_LEAVE_NO_MAN_BEHIND_TALENT))
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_LEAVE_NO_MAN_BEHIND_AURA, true);
                }
                else
                {
                    GetCaster()->CastSpell(GetHitUnit(), GetCaster()->HasAura(SPELL_WARRIOR_WARBRINGER) ? SPELL_WARRIOR_WARBRINGER_DAMAGE : SPELL_WARRIOR_CHARGE_ROOT, true);
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_CHARGE_TRIGGERED, true);
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warr_legion_intercept_SpellScript::CheckTarget);
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_intercept_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_intercept_SpellScript();
        }
};

// 5246 - Intimidating Shout
class spell_warr_legion_intimidating_shout : public SpellScriptLoader
{
    public:
        spell_warr_legion_intimidating_shout() : SpellScriptLoader("spell_warr_legion_intimidating_shout") { }

        class spell_warr_legion_intimidating_shout_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_intimidating_shout_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                // WoD leftover - effect is no longer used... WHY preventhiteffect doesn't work ?!
                targets.clear();
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warr_legion_intimidating_shout_SpellScript::FilterTargets, EFFECT_3, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_intimidating_shout_SpellScript();
        }
};

// 12294 - Mortal Strike
class spell_warr_legion_mortal_strike : public SpellScriptLoader
{
    public:
        spell_warr_legion_mortal_strike() : SpellScriptLoader("spell_warr_legion_mortal_strike") { }

        class spell_warr_legion_mortal_strike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_mortal_strike_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_MORTAL_WOUNDS,
                    SPELL_WARRIOR_MORTAL_STRIKE_HEAL_REDUCE,
                    SPELL_WARRIOR_SHARPEN_BLADE
                });
            }

            void HandleDebuff(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_WARRIOR_SHARPEN_BLADE))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_MORTAL_STRIKE_HEAL_REDUCE, true);
                else
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_MORTAL_WOUNDS, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_mortal_strike_SpellScript::HandleDebuff, EFFECT_2, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_mortal_strike_SpellScript();
        }
};

// 200872 - Odyn's Champion
class spell_warr_legion_odyns_champion_initial_proc : public SpellScriptLoader
{
    public:
        spell_warr_legion_odyns_champion_initial_proc() : SpellScriptLoader("spell_warr_legion_odyns_champion_initial_proc") { }

        class spell_warr_legion_odyns_champion_initial_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_odyns_champion_initial_proc_AuraScript);

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                /// @TODO: new proc system - replace this with positive spell cast check, currently we check the proc on triggered damage one time and set a cooldown on proc
                if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;

                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(1500));
                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_odyns_champion_initial_proc_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_odyns_champion_initial_proc_AuraScript();
        }
};

// 200986 - Odyn's Champion
class spell_warr_legion_odyns_champion : public SpellScriptLoader
{
    public:
        spell_warr_legion_odyns_champion() : SpellScriptLoader("spell_warr_legion_odyns_champion") { }

        class spell_warr_legion_odyns_champion_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_odyns_champion_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()) || !eventInfo.GetSpellInfo()
                    || !eventInfo.GetDamageInfo() || eventInfo.GetDamageInfo()->GetDamage() == 0)
                    return false;

                switch (eventInfo.GetSpellInfo()->Id)
                {
                    /// @TODO: new proc system
                    case 184707: // Rampage delayed trigger- only initial (positive) should proc this talent
                    case 184709: // Rampage delayed trigger- only initial (positive) should proc this talent
                    case 201364: // Rampage delayed trigger- only initial (positive) should proc this talent
                    case 201363: // Rampage delayed trigger- only initial (positive) should proc this talent
                    case 50622:  // Bladestorm - only initial (positive) should proc this talent
                    case 95738:  // Bladestorm - only initial (positive) should proc this talent
                        return false;
                    default:
                        GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(500));
                        return true;
                }
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                if (Player* target = GetTarget()->ToPlayer())
                {
                    PlayerSpellMap const& plrSpellMap = target->GetSpellMap();
                    for (PlayerSpellMap::const_iterator itr = plrSpellMap.begin(); itr != plrSpellMap.end(); ++itr)
                    {
                        if (itr->second->state == PLAYERSPELL_REMOVED || itr->second->disabled)
                            continue;
                        SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(itr->first);
                        if (spellInfo->IsPassive() || !spellInfo->HasAttribute(SPELL_ATTR0_ABILITY))
                            continue;

                        if (spellInfo->ChargeCategoryId != 0)
                            GetTarget()->GetSpellHistory()->ReduceChargeTime(spellInfo->ChargeCategoryId, aurEff->GetAmount() * 10);
                        else
                            GetTarget()->GetSpellHistory()->ModifyCooldown(spellInfo->Id, std::chrono::milliseconds(-(aurEff->GetAmount() * 10)));
                    }
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_odyns_champion_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_odyns_champion_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_odyns_champion_AuraScript();
        }
};

// 203179 - Opportunity Strikes
class spell_warr_legion_opportunity_strikes : public SpellScriptLoader
{
    public:
        spell_warr_legion_opportunity_strikes() : SpellScriptLoader("spell_warr_legion_opportunity_strikes") { }

        class spell_warr_legion_opportunity_strikes_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_opportunity_strikes_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                return roll_chance_f((100.00f - eventInfo.GetActionTarget()->GetHealthPct()) * 0.60f);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_opportunity_strikes_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_opportunity_strikes_AuraScript();
        }
};

// 209492 - Precise Strikes
class spell_warr_legion_precise_strikes : public SpellScriptLoader
{
    public:
        spell_warr_legion_precise_strikes() : SpellScriptLoader("spell_warr_legion_precise_strikes") { }

        class spell_warr_legion_precise_strikes_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_precise_strikes_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_PRECISE_STRIKES_BONUS
                });
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                int32 bonus = aurEff->GetAmount();
                GetTarget()->CastCustomSpell(GetTarget(), SPELL_WARRIOR_PRECISE_STRIKES_BONUS, &bonus, &bonus, nullptr, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_precise_strikes_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_precise_strikes_AuraScript();
        }
};

// 184367 - Rampage
class spell_warr_legion_rampage : public SpellScriptLoader
{
    public:
        spell_warr_legion_rampage() : SpellScriptLoader("spell_warr_legion_rampage") { }

        class spell_warr_legion_rampage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_rampage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_MASSACRE_BONUS,
                    SPELL_WARRIOR_ENRAGE_TRIGGERED
                });
            }

            void HandleOnPrepare()
            {
                // Rampage should always casted directly otherwise it blocks other spell casts (retail verified)
                GetSpell()->SetTriggerCastFlags(TriggerCastFlags(GetSpell()->GetTriggerCastFlags() | TRIGGERED_IGNORE_CAST_IN_PROGRESS | TRIGGERED_CAST_DIRECTLY));
            }

            void HandleAfterCast()
            {
                GetCaster()->RemoveAurasDueToSpell(SPELL_WARRIOR_MASSACRE_BONUS);
                GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_ENRAGE_TRIGGERED, true);
            }

            void Register() override
            {
                OnPrepare += SpellPrepareFn(spell_warr_legion_rampage_SpellScript::HandleOnPrepare);
                AfterCast += SpellCastFn(spell_warr_legion_rampage_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_rampage_SpellScript();
        }
};

// 152277 - Ravager
// 228920 - Ravager
class spell_warr_legion_ravager : public SpellScriptLoader
{
    public:
        spell_warr_legion_ravager() : SpellScriptLoader("spell_warr_legion_ravager") { }

        class spell_warr_legion_ravager_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_ravager_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_RAVAGER_SUMMON,
                    SPELL_WARRIOR_RAVAGER_PROTECTION,
                    SPELL_WARRIOR_RAVAGER_PARRY_BONUS
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (WorldLocation const* destPos = GetExplTargetDest())
                    GetCaster()->CastSpell(destPos->GetPositionX(), destPos->GetPositionY(), destPos->GetPositionZ(), SPELL_WARRIOR_RAVAGER_SUMMON, true);
            }

            void HandleAfterCast()
            {
                if (GetSpellInfo()->Id == SPELL_WARRIOR_RAVAGER_PROTECTION)
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_RAVAGER_PARRY_BONUS, true);
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_warr_legion_ravager_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_warr_legion_ravager_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_ravager_SpellScript();
        }
};

// 6572 - Revenge
class spell_warr_legion_revenge : public SpellScriptLoader
{
    public:
        spell_warr_legion_revenge() : SpellScriptLoader("spell_warr_legion_revenge") { }

        class spell_warr_legion_revenge_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_revenge_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_BEST_SERVED_COLD
                });
            }

            void CountTargets(std::list<WorldObject*>& targets)
            {
                targetCount = targets.size() - 1;
            }

            void HandleBonusDamage(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* bestServedCold = GetCaster()->GetAuraEffect(SPELL_WARRIOR_BEST_SERVED_COLD, EFFECT_0))
                {
                    if (SpellEffectInfo const* eff1Info = bestServedCold->GetBase()->GetSpellInfo()->GetEffect(EFFECT_1))
                    {
                        int32 bonusDamage = bestServedCold->GetAmount() * std::min(targetCount, eff1Info->CalcValue(GetCaster()));
                        SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), bonusDamage));
                    }
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warr_legion_revenge_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_CONE_ENEMY_104);
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_revenge_SpellScript::HandleBonusDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

            private:
                int32 targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_revenge_SpellScript();
        }
};

// 5302 - Revenge!
class spell_warr_legion_revenge_triggered : public SpellScriptLoader
{
    public:
        spell_warr_legion_revenge_triggered() : SpellScriptLoader("spell_warr_legion_revenge_triggered") { }

        class spell_warr_legion_revenge_triggered_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_revenge_triggered_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_REVENGE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_REVENGE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_revenge_triggered_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_revenge_triggered_SpellScript();
        }
};

// 29838 - Second Wind
class spell_warr_legion_second_wind : public SpellScriptLoader
{
    public:
        spell_warr_legion_second_wind() : SpellScriptLoader("spell_warr_legion_second_wind") { }

        class spell_warr_legion_second_wind_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_second_wind_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_SECOND_WIND_BLOCKER
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_SECOND_WIND_BLOCKER, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_second_wind_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_second_wind_AuraScript();
        }
};

// 209706 - Shattered Defenses
/// @TODO: new proc system
class spell_warr_legion_shattered_defenses : public SpellScriptLoader
{
    public:
        spell_warr_legion_shattered_defenses() : SpellScriptLoader("spell_warr_legion_shattered_defenses") { }

        class spell_warr_legion_shattered_defenses_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_shattered_defenses_AuraScript);

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetAura()->Remove();
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_shattered_defenses_AuraScript::OnProc, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_shattered_defenses_AuraScript();
        }
};

// 198912 - Shield Bash
class spell_warr_legion_shield_bash : public SpellScriptLoader
{
    public:
        spell_warr_legion_shield_bash() : SpellScriptLoader("spell_warr_legion_shield_bash") { }

        class spell_warr_legion_shield_bash_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_shield_bash_SpellScript);

            void HandleExtraEffect(SpellEffIndex /*effIndex*/)
            {
                // If the target is casting, the cooldown is instantly reset.
                if (GetHitUnit()->IsNonMeleeSpellCast(false))
                    GetCaster()->GetSpellHistory()->ResetCooldown(GetSpellInfo()->Id, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_shield_bash_SpellScript::HandleExtraEffect, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_shield_bash_SpellScript();
        }
};

// 2565 - Shield Block
class spell_warr_legion_shield_block : public SpellScriptLoader
{
    public:
        spell_warr_legion_shield_block() : SpellScriptLoader("spell_warr_legion_shield_block") { }

        class spell_warr_legion_shield_block_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_shield_block_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_SHIELD_BLOCK_TRIGGERED
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_SHIELD_BLOCK_TRIGGERED, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_shield_block_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_shield_block_SpellScript();
        }
};

// 23922 - Shield Slam
class spell_warr_legion_shield_slam : public SpellScriptLoader
{
    public:
        spell_warr_legion_shield_slam() : SpellScriptLoader("spell_warr_legion_shield_slam") { }

        class spell_warr_legion_shield_slam_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_shield_slam_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_HEAVY_REPERCUSSIONS,
                    SPELL_WARRIOR_SHIELD_BLOCK_TRIGGERED
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_WARRIOR_HEAVY_REPERCUSSIONS))
                {
                    if (Aura* shieldBlock = GetCaster()->GetAura(SPELL_WARRIOR_SHIELD_BLOCK_TRIGGERED))
                    {
                        if (SpellEffectInfo const* effInfo = sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_HEAVY_REPERCUSSIONS)->GetEffect(EFFECT_0))
                        {
                            int32 newDuration = shieldBlock->GetDuration() + (effInfo->CalcValue(GetCaster()) * 10);
                            if (newDuration > shieldBlock->GetMaxDuration())
                                shieldBlock->SetMaxDuration(newDuration);
                            shieldBlock->SetDuration(newDuration);
                        }
                    }
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_shield_slam_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_shield_slam_SpellScript();
        }
};

// 6343 - Thunder Clap
// 6572 Revenge
// 20243 - Devastate
class spell_warr_legion_shield_slam_level_bonus : public SpellScriptLoader
{
    public:
        spell_warr_legion_shield_slam_level_bonus() : SpellScriptLoader("spell_warr_legion_shield_slam_level_bonus") { }

        class spell_warr_legion_shield_slam_level_bonus_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_shield_slam_level_bonus_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_SHIELD_SLAM_LEVEL_BONUS,
                    SPELL_WARRIOR_SHIELD_SLAM,
                    SPELL_WARRIOR_SHIELD_SLAM_EVENT_MARKER
                });
            }

            void HandleAfterCast()
            {
                if (AuraEffect* shieldSlamLevelBonus = GetCaster()->GetAuraEffect(SPELL_WARRIOR_SHIELD_SLAM_LEVEL_BONUS, EFFECT_0))
                {
                    if (GetCaster()->GetSpellHistory()->HasCooldown(SPELL_WARRIOR_SHIELD_SLAM))
                    {
                        if (roll_chance_i(shieldSlamLevelBonus->GetAmount()))
                        {
                            GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_SHIELD_SLAM, true);
                            GetCaster()->CastSpell(GetCaster(), SPELL_WARRIOR_SHIELD_SLAM_EVENT_MARKER, true);
                        }
                    }
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_shield_slam_level_bonus_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_shield_slam_level_bonus_SpellScript();
        }
};

// 46968 - Shockwave
class spell_warr_legion_shockwave : public SpellScriptLoader
{
    public:
        spell_warr_legion_shockwave() : SpellScriptLoader("spell_warr_legion_shockwave") { }

        class spell_warr_legion_shockwave_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_shockwave_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_3) || spellInfo->GetEffect(EFFECT_3)->Effect != SPELL_EFFECT_SCRIPT_EFFECT)
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_SHOCKWAVE_STUN
                });
            }

            void CountTargets(std::list<WorldObject*>& targets)
            {
                targetCount = targets.size();
            }

            void HandleDebuff(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_SHOCKWAVE_STUN, true);
            }

            void HandleAfterCast()
            {
                if (targetCount >= GetSpellInfo()->GetEffect(EFFECT_0)->CalcValue())
                    GetCaster()->GetSpellHistory()->ModifyCooldown(GetSpellInfo()->Id, -(GetSpellInfo()->GetEffect(EFFECT_3)->CalcValue() * 1000));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warr_legion_shockwave_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_CONE_ENEMY_104);
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_shockwave_SpellScript::HandleDebuff, EFFECT_0, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_warr_legion_shockwave_SpellScript::HandleAfterCast);
            }

            private:
                int32 targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_shockwave_SpellScript();
        }
};

// 23920 - Spell Reflection
class spell_warr_legion_spell_reflection : public SpellScriptLoader
{
    public:
        spell_warr_legion_spell_reflection() : SpellScriptLoader("spell_warr_legion_spell_reflection") { }

        class spell_warr_legion_spell_reflection_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_spell_reflection_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_REFLECTIVE_PLATING
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return !GetTarget()->HasAura(SPELL_WARRIOR_REFLECTIVE_PLATING);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_spell_reflection_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_spell_reflection_AuraScript();
        }
};

// 107570 - Storm Bolt
class spell_warr_legion_storm_bolt : public SpellScriptLoader
{
    public:
        spell_warr_legion_storm_bolt() : SpellScriptLoader("spell_warr_legion_storm_bolt") { }

        class spell_warr_legion_storm_bolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_storm_bolt_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_STORM_BOLT_STUN
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetExplTargetUnit(), SPELL_WARRIOR_STORM_BOLT_STUN, true);
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_warr_legion_storm_bolt_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_storm_bolt_SpellScript();
        }
};

// 202161 - Sweeping Strikes
class spell_warr_legion_sweeping_strikes : public SpellScriptLoader
{
    public:
        spell_warr_legion_sweeping_strikes() : SpellScriptLoader("spell_warr_legion_sweeping_strikes") { }

        class spell_warr_legion_sweeping_strikes_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_sweeping_strikes_AuraScript);

            void HandleEffectCalcSpellMod(AuraEffect const* /*aurEff*/, SpellModifier*& spellMod)
            {
                spellMod->mask = flag128(0x22000000);
            }

            void Register() override
            {
                DoEffectCalcSpellMod += AuraEffectCalcSpellModFn(spell_warr_legion_sweeping_strikes_AuraScript::HandleEffectCalcSpellMod, EFFECT_0, SPELL_AURA_ADD_FLAT_MODIFIER);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_sweeping_strikes_AuraScript();
        }
};

// 184783 - Tactician
class spell_warr_legion_tactician : public SpellScriptLoader
{
    public:
        spell_warr_legion_tactician() : SpellScriptLoader("spell_warr_legion_tactician") { }

        class spell_warr_legion_tactician_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_tactician_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetProcSpell())
                    return false;

                int32 rageSpent = eventInfo.GetProcSpell()->GetPowerCost(POWER_RAGE);
                float chance = float(GetAura()->GetEffect(EFFECT_1)->GetAmount()) / 100;
                chance *= float(rageSpent) / 10;
                return roll_chance_f(chance);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_tactician_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_tactician_AuraScript();
        }
};

// 199854 - Tactician (triggered)
class spell_warr_legion_tactician_triggered : public SpellScriptLoader
{
    public:
        spell_warr_legion_tactician_triggered() : SpellScriptLoader("spell_warr_legion_tactician_triggered") { }

        class spell_warr_legion_tactician_triggered_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_tactician_triggered_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_COLOSSUS_SMASH,
                    SPELL_WARRIOR_MORTAL_STRIKE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_COLOSSUS_SMASH, true);
                GetCaster()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_MORTAL_STRIKE)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_tactician_triggered_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_tactician_triggered_SpellScript();
        }
};

// 206333 - Taste for Blood
class spell_warr_legion_taste_for_blood : public SpellScriptLoader
{
    public:
        spell_warr_legion_taste_for_blood() : SpellScriptLoader("spell_warr_legion_taste_for_blood") { }

        class spell_warr_legion_taste_for_blood_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_taste_for_blood_AuraScript);

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                Remove();
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_taste_for_blood_AuraScript::OnProc, EFFECT_0, SPELL_AURA_ADD_FLAT_MODIFIER);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_taste_for_blood_AuraScript();
        }
};

// 6343 - Thunder Clap
class spell_warr_legion_thunder_clap : public SpellScriptLoader
{
    public:
        spell_warr_legion_thunder_clap() : SpellScriptLoader("spell_warr_legion_thunder_clap") { }

        class spell_warr_legion_thunder_clap_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_thunder_clap_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_THUNDERSTRUCK_HONOR_TALENT,
                    SPELL_WARRIOR_THUNDERSTRUCK_ROOT
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_WARRIOR_THUNDERSTRUCK_HONOR_TALENT);
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARRIOR_THUNDERSTRUCK_ROOT, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_thunder_clap_SpellScript::HandleScriptEffect, EFFECT_1, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_thunder_clap_SpellScript();
        }
};

// 209541 - Touch of Zakajz
class spell_warr_legion_touch_of_zakajz : public SpellScriptLoader
{
    public:
        spell_warr_legion_touch_of_zakajz() : SpellScriptLoader("spell_warr_legion_touch_of_zakajz") { }

        class spell_warr_legion_touch_of_zakajz_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_touch_of_zakajz_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_TOUCH_OF_ZAKAJZ_HEAL
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastCustomSpell(SPELL_WARRIOR_TOUCH_OF_ZAKAJZ_HEAL, SPELLVALUE_BASE_POINT0, int32(CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount())), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_touch_of_zakajz_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_touch_of_zakajz_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_touch_of_zakajz_AuraScript();
        }
};

// 215538 - Trauma
class spell_warr_legion_trauma : public SpellScriptLoader
{
    public:
        spell_warr_legion_trauma() : SpellScriptLoader("spell_warr_legion_trauma") { }

        class spell_warr_legion_trauma_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_trauma_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_SLAM,
                    SPELL_WARRIOR_WHIRLWIND_TRIGGERED,
                    SPELL_WARRIOR_WHIRLWIND_TRIGGERED_2,
                    SPELL_WARRIOR_TRAUMA_PERIODIC

                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo() || !eventInfo.GetSpellInfo() || !eventInfo.GetActionTarget())
                    return false;

                uint32 spellId = eventInfo.GetSpellInfo()->Id;
                if (spellId != SPELL_WARRIOR_SLAM && spellId != SPELL_WARRIOR_WHIRLWIND_TRIGGERED && spellId != SPELL_WARRIOR_WHIRLWIND_TRIGGERED_2)
                    return false;

                return true;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                uint32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()) / sSpellMgr->AssertSpellInfo(SPELL_WARRIOR_TRAUMA_PERIODIC)->GetMaxTicks(DIFFICULTY_NONE);
                damage += eventInfo.GetActionTarget()->GetRemainingPeriodicAmount(GetTarget()->GetGUID(), SPELL_WARRIOR_TRAUMA_PERIODIC, SPELL_AURA_PERIODIC_DAMAGE);

                GetTarget()->CastCustomSpell(SPELL_WARRIOR_TRAUMA_PERIODIC, SPELLVALUE_BASE_POINT0, int32(damage), eventInfo.GetActionTarget(), true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_trauma_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_trauma_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_trauma_AuraScript();
        }
};

// 200860 - Unrivaled Strength
class spell_warr_legion_unrivaled_strength : public SpellScriptLoader
{
    public:
        spell_warr_legion_unrivaled_strength() : SpellScriptLoader("spell_warr_legion_unrivaled_strength") { }

        class spell_warr_legion_unrivaled_strength_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_unrivaled_strength_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_UNRIVALED_STRENGTH_BONUS
                });
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastCustomSpell(SPELL_WARRIOR_UNRIVALED_STRENGTH_BONUS, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_unrivaled_strength_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_unrivaled_strength_AuraScript();
        }
};

// 202572 - Vengeance
class spell_warr_legion_vengeance : public SpellScriptLoader
{
    public:
        spell_warr_legion_vengeance() : SpellScriptLoader("spell_warr_legion_vengeance") { }

        class spell_warr_legion_vengeance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_vengeance_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_IGNORE_PAIN,
                    SPELL_WARRIOR_VENGEANCE_REVENGE,
                    SPELL_WARRIOR_VENGEANCE_IGNORE_PAIN,
                    SPELL_WARRIOR_REVENGE
                });
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo()->Id == SPELL_WARRIOR_IGNORE_PAIN)
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_VENGEANCE_REVENGE, true, nullptr, aurEff);
                else if (eventInfo.GetSpellInfo()->Id == SPELL_WARRIOR_REVENGE)
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARRIOR_VENGEANCE_IGNORE_PAIN, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_vengeance_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_vengeance_AuraScript();
        }
};

// 32215 - Victorious State
class spell_warr_legion_victorious_state : public SpellScriptLoader
{
    public:
        spell_warr_legion_victorious_state() : SpellScriptLoader("spell_warr_legion_victorious_state") { }

        class spell_warr_legion_victorious_state_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_victorious_state_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_IMPENDING_VICTORY
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // Only available on arms and protection spec
                if (Player* actor = eventInfo.GetActor()->ToPlayer())
                    return actor->GetUInt32Value(PLAYER_FIELD_CURRENT_SPEC_ID) != TALENT_SPEC_WARRIOR_FURY;
                return false;
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (GetTarget()->HasSpell(SPELL_WARRIOR_IMPENDING_VICTORY))
                {
                    PreventDefaultAction();
                    GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_WARRIOR_IMPENDING_VICTORY, true);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warr_legion_victorious_state_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warr_legion_victorious_state_AuraScript::OnProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_victorious_state_AuraScript();
        }
};

// 34428 - Victory Rush
class spell_warr_legion_victory_rush : public SpellScriptLoader
{
    public:
        spell_warr_legion_victory_rush() : SpellScriptLoader("spell_warr_legion_victory_rush") { }

        class spell_warr_legion_victory_rush_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_victory_rush_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_VICTORIOUS,
                    SPELL_WARRIOR_VICTORY_RUSH_HEAL
                });
            }

            void HandleHeal()
            {
                Unit* caster = GetCaster();
                caster->CastSpell(caster, SPELL_WARRIOR_VICTORY_RUSH_HEAL, true);
                caster->RemoveAurasDueToSpell(SPELL_WARRIOR_VICTORIOUS);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_victory_rush_SpellScript::HandleHeal);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_victory_rush_SpellScript();
        }
};

// 236321 - War Banner
class spell_warr_legion_war_banner : public SpellScriptLoader
{
    public:
        spell_warr_legion_war_banner() : SpellScriptLoader("spell_warr_legion_war_banner") { }

        class spell_warr_legion_war_banner_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warr_legion_war_banner_AuraScript);

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 250;
            }

            void HandleCustomTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (GetTarget()->GetDistance(caster) > 30.00f)
                        Remove();
                }
                else
                    Remove();
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_warr_legion_war_banner_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_MOD_SPEED_NOT_STACK);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warr_legion_war_banner_AuraScript::HandleCustomTick, EFFECT_0, SPELL_AURA_MOD_SPEED_NOT_STACK);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_warr_legion_war_banner_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_MOD_SPEED_NOT_STACK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warr_legion_war_banner_AuraScript();
        }
};

class DelayedWhirlwindBonusRemoveEvent : public BasicEvent
{
    public:
        explicit DelayedWhirlwindBonusRemoveEvent(Unit* caster) : _caster(caster) { }

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            _caster->RemoveAurasDueToSpell(SPELL_WARRIOR_CLEAVE_LEVEL_BONUS_AURA);
            return true;
        }

    private:
        Unit* _caster;
};

// 1680 - Whirlwind
class spell_warr_legion_whirlwind_arms : public SpellScriptLoader
{
    public:
        spell_warr_legion_whirlwind_arms() : SpellScriptLoader("spell_warr_legion_whirlwind_arms") { }

        class spell_warr_legion_whirlwind_arms_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_whirlwind_arms_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_CLEAVE_LEVEL_BONUS_AURA
                });
            }

            void HandleAfterCast()
            {
                // Last whirlwind trigger is casted 400ms after the initial cast
                GetCaster()->m_Events.AddEvent(new DelayedWhirlwindBonusRemoveEvent(GetCaster()), GetCaster()->m_Events.CalculateTime(450));
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warr_legion_whirlwind_arms_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_whirlwind_arms_SpellScript();
        }
};

// 190411 - Whirlwind
class spell_warr_legion_whirlwind_fury : public SpellScriptLoader
{
    public:
        spell_warr_legion_whirlwind_fury() : SpellScriptLoader("spell_warr_legion_whirlwind_fury") { }

        class spell_warr_legion_whirlwind_fury_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_whirlwind_fury_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_WRECKING_BALL
                });
            }

            void HandleAfterCast()
            {
                // We remove this aura here because this aura should be removed after all trigger spells are casted (all trigger spells use the same spellfam flags)
                GetCaster()->RemoveAurasDueToSpell(SPELL_WARRIOR_WRECKING_BALL);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_warr_legion_whirlwind_fury_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_whirlwind_fury_SpellScript();
        }
};

// 199658 - Whirlwind
// 199850 - Whirlwind
class spell_warr_legion_whirlwind_triggered : public SpellScriptLoader
{
    public:
        spell_warr_legion_whirlwind_triggered() : SpellScriptLoader("spell_warr_legion_whirlwind_triggered") { }

        class spell_warr_legion_whirlwind_triggered_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warr_legion_whirlwind_triggered_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARRIOR_FERVOR_OF_BATTLE
                });
            }

            void CalculateBonusDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->GetGUID() != GetCaster()->GetTarget())
                    return;

                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_WARRIOR_FERVOR_OF_BATTLE, EFFECT_0))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), aurEff->GetAmount()));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warr_legion_whirlwind_triggered_SpellScript::CalculateBonusDamage, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warr_legion_whirlwind_triggered_SpellScript();
        }
};

class npc_warr_legion_ravager : public CreatureScript
{
    public:
        npc_warr_legion_ravager() : CreatureScript("npc_warr_legion_ravager") { }

        struct npc_warr_legion_ravagerAI : public PassiveAI
        {
            npc_warr_legion_ravagerAI(Creature* creature) : PassiveAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                DoCast(summoner, SPELL_GENERIC_COPY_WEAPON, true);
                me->SetControlled(true, UNIT_STATE_ROOT);
                _scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    me->SendPlaySpellVisual(Position(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ()), 0.00f, SPELL_VISUAL_WARRIOR_RAVAGER, 0, 0, 0, true);
                    me->CastSpell(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), SPELL_WARRIOR_RAVAGER_DAMAGE, true, nullptr, nullptr, me->GetOwnerGUID());
                    task.Repeat();
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warr_legion_ravagerAI(creature);
        }
};

class npc_warr_legion_war_banner : public CreatureScript
{
    public:
        npc_warr_legion_war_banner() : CreatureScript("npc_warr_legion_war_banner") { }

        struct npc_warr_legion_war_bannerAI : public PassiveAI
        {
            npc_warr_legion_war_bannerAI(Creature* creature) : PassiveAI(creature) {}

            void IsSummonedBy(Unit* /*summoner*/) override
            {
                me->SetControlled(true, UNIT_STATE_ROOT);
                _scheduler.Schedule(Milliseconds(500), [this](TaskContext task)
                {
                    if (Unit* owner = me->GetCharmerOrOwner())
                    {
                        std::list<Unit*> targets;
                        Trinity::AnyFriendlyUnitInObjectRangeCheck checker(me, owner, 30.0f);
                        Trinity::UnitListSearcher<Trinity::AnyFriendlyUnitInObjectRangeCheck> searcher(me, targets, checker);
                        Cell::VisitAllObjects(me, searcher, 30.0f);

                        for (Unit* unitTarget : targets)
                            if (!unitTarget->HasAura(SPELL_WARRIOR_WAR_BANNER_BUFF))
                                me->CastSpell(unitTarget, SPELL_WARRIOR_WAR_BANNER_BUFF, true);
                    }
                    task.Repeat();
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warr_legion_war_bannerAI(creature);
        }
};

class areatrigger_warr_legion_into_the_fray : public AreaTriggerEntityScript
{
    public:
        areatrigger_warr_legion_into_the_fray() : AreaTriggerEntityScript("areatrigger_warr_legion_into_the_fray") { }

        struct areatrigger_warr_legion_into_the_frayAI : public AreaTriggerAI
        {
            areatrigger_warr_legion_into_the_frayAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                    if (caster->IsValidAttackTarget(unit) && !unit->IsCritter())
                        caster->CastSpell(caster, SPELL_WARRIOR_INTO_THE_FRAY, true);
            }

            void OnUnitExit(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!caster->IsValidAttackTarget(unit) || unit->IsCritter())
                        return;

                    uint32 unfriendlyInsideUnits = 0;
                    for (ObjectGuid const& guid : at->GetInsideUnits())
                    {
                        if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                            if (caster->IsValidAttackTarget(target) && !unit->IsCritter())
                                unfriendlyInsideUnits++;
                    }

                    if (unfriendlyInsideUnits == 0)
                        caster->RemoveAurasDueToSpell(SPELL_WARRIOR_INTO_THE_FRAY);
                    else
                        if (Aura* intoTheFray = caster->GetAura(SPELL_WARRIOR_INTO_THE_FRAY))
                            intoTheFray->SetStackAmount(std::min(unfriendlyInsideUnits, intoTheFray->CalcMaxStackAmount(caster)));
                }
            }

            void OnRemove() override
            {
                if (Unit* caster = at->GetCaster())
                    caster->RemoveAurasDueToSpell(SPELL_WARRIOR_INTO_THE_FRAY);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_warr_legion_into_the_frayAI(areaTrigger);
        }
};

// 236273 - Duel
class areatrigger_warr_legion_duel_target_trigger : public AreaTriggerEntityScript
{
    public:
        areatrigger_warr_legion_duel_target_trigger() : AreaTriggerEntityScript("areatrigger_warr_legion_duel_target_trigger") { }

        struct areatrigger_warr_legion_duel_target_triggerAI : public AreaTriggerAI
        {
            areatrigger_warr_legion_duel_target_triggerAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* target = at->GetTarget())
                    if (target->IsValidAttackTarget(unit))
                        if (unit->GetGUID() != at->GetCasterGuid())
                            target->CastSpell(unit, SPELL_WARRIOR_DUEL_ENEMY_AREATRIGGER_EFFECT, true);
            }

            void OnUnitExit(Unit* unit) override
            {
                unit->RemoveAurasDueToSpell(SPELL_WARRIOR_DUEL_ENEMY_AREATRIGGER_EFFECT, at->GetTarget()->GetGUID());
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_warr_legion_duel_target_triggerAI(areaTrigger);
        }
};

// 236273 - Duel
class areatrigger_warr_legion_duel_caster_trigger : public AreaTriggerEntityScript
{
    public:
        areatrigger_warr_legion_duel_caster_trigger() : AreaTriggerEntityScript("areatrigger_warr_legion_duel_caster_trigger") { }

        struct areatrigger_warr_legion_duel_caster_triggerAI : public AreaTriggerAI
        {
            areatrigger_warr_legion_duel_caster_triggerAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnUnitEnter(Unit* unit) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsValidAttackTarget(unit))
                    {
                        ObjectGuid enemyAreatriggerTargetGuid;
                        // we need to check that on this way because caster and enemy areatrigger spell id is the same
                        for (auto itr : caster->GetTargetAuraApplications(at->GetSpellId()))
                        {
                            if (itr->GetTarget() != caster)
                            {
                                enemyAreatriggerTargetGuid = itr->GetTarget()->GetGUID();
                                break;
                            }
                        }

                        if (unit->GetGUID() != enemyAreatriggerTargetGuid)
                            caster->CastSpell(unit, SPELL_WARRIOR_DUEL_CASTER_AREATRIGGER_EFFECT, true);
                    }
                }
            }

            void OnUnitExit(Unit* unit) override
            {
                unit->RemoveAurasDueToSpell(SPELL_WARRIOR_DUEL_CASTER_AREATRIGGER_EFFECT, at->GetCasterGuid());
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_warr_legion_duel_caster_triggerAI(areaTrigger);
        }
};

void AddSC_legion_warrior_spell_scripts()
{
    new spell_warr_legion_anger_management();
    new spell_warr_legion_avatar();
    new spell_warr_legion_battle_cry();
    new spell_warr_legion_battle_trance();
    new spell_warr_legion_berserker_rage();
    new spell_warr_legion_bloodbath();
    new spell_warr_legion_bloodthirst();
    new spell_warr_legion_bloodthirst_heal();
    new spell_warr_legion_bodyguard();
    new spell_warr_legion_charge();
    new spell_warr_legion_charge_triggered();
    new spell_warr_legion_cleave();
    new spell_warr_legion_colossus_smash();
    new spell_warr_legion_commanding_shout();
    new spell_warr_legion_corrupted_blood_of_zakajz();
    new spell_warr_legion_deep_wounds();
    new spell_warr_legion_defensive_stance();
    new spell_warr_legion_demoralizing_shout();
    new spell_warr_legion_demoralizing_shout_absorb();
    new spell_warr_legion_devastator();
    new spell_warr_legion_endless_rage();
    new spell_warr_legion_enrage();
    new spell_warr_legion_execute();
    new spell_warr_legion_frothing_berserker();
    new spell_warr_legion_furious_slash();
    new spell_warr_legion_heroic_leap();
    new spell_warr_legion_heroic_leap_damage();
    new spell_warr_legion_ignore_pain();
    new spell_warr_legion_impending_victory();
    new spell_warr_legion_in_for_the_kill();
    new spell_warr_legion_inspiring_presence();
    new spell_warr_legion_inspiring_presence_heal();
    new spell_warr_legion_intercept();
    new spell_warr_legion_intimidating_shout();
    new spell_warr_legion_mortal_strike();
    new spell_warr_legion_odyns_champion_initial_proc();
    new spell_warr_legion_odyns_champion();
    new spell_warr_legion_opportunity_strikes();
    new spell_warr_legion_precise_strikes();
    new spell_warr_legion_rampage();
    new spell_warr_legion_ravager();
    new spell_warr_legion_revenge();
    new spell_warr_legion_revenge_triggered();
    new spell_warr_legion_second_wind();
    new spell_warr_legion_shattered_defenses();
    new spell_warr_legion_shield_bash();
    new spell_warr_legion_shield_block();
    new spell_warr_legion_shield_slam();
    new spell_warr_legion_shield_slam_level_bonus();
    new spell_warr_legion_shockwave();
    new spell_warr_legion_spell_reflection();
    new spell_warr_legion_storm_bolt();
    new spell_warr_legion_sweeping_strikes();
    new spell_warr_legion_tactician();
    new spell_warr_legion_tactician_triggered();
    new spell_warr_legion_taste_for_blood();
    new spell_warr_legion_thunder_clap();
    new spell_warr_legion_touch_of_zakajz();
    new spell_warr_legion_trauma();
    new spell_warr_legion_unrivaled_strength();
    new spell_warr_legion_vengeance();
    new spell_warr_legion_victorious_state();
    new spell_warr_legion_victory_rush();
    new spell_warr_legion_war_banner();
    new spell_warr_legion_whirlwind_arms();
    new spell_warr_legion_whirlwind_fury();
    new spell_warr_legion_whirlwind_triggered();

    new npc_warr_legion_ravager();
    new npc_warr_legion_war_banner();
    new areatrigger_warr_legion_into_the_fray();
    new areatrigger_warr_legion_duel_target_trigger();
    new areatrigger_warr_legion_duel_caster_trigger();
}
