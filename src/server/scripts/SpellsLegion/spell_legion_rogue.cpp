/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_ROGUE and SPELLFAMILY_GENERIC spells used by rogue players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_rog_".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "Containers.h"
#include "Creature.h"
#include "DynamicObject.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "Spell.h"
#include "SpellAuras.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"
#include "MoveSplineInitArgs.h"
#include "MotionMaster.h"

enum RogueSpecAuras
{
    AURA_ASSASINATION_ROGUE = 137037,
    AURA_OUTLAW_ROGUE       = 137036,
    AURA_SUBTLETY_ROGUE     = 137035,
};

enum SpellsLegionRogue
{
    SPELL_ROGUE_ADRENALINE_RUSH                 = 13750,
    SPELL_ROGUE_AGONIZING_POISON                = 200802,
    SPELL_ROGUE_AGONIZING_POISON_AURA           = 200803,
    SPELL_ROGUE_ASSASINATION_STEALTH            = 115191,
    SPELL_ROGUE_BAG_OF_TRICKS_AREATRIGGER       = 192661,
    SPELL_ROGUE_BETWEEN_THE_EYES                = 199804,
    SPELL_ROGUE_BLADE_FLURRY                    = 13877,
    SPELL_ROGUE_BLADE_FLURRY_DAMAGE             = 22482,
    SPELL_ROGUE_BLADEMASTER_INITIAL_PROC_AURA   = 202628,
    SPELL_ROGUE_BLADEMASTER_PROC_OF_PROC_AURA   = 202631,
    SPELL_ROGUE_BLUNDERBUSS                     = 202897,
    SPELL_ROGUE_BLUNDERBUSS_OVERRIDE            = 202848,
    SPELL_ROGUE_BROADSIDES                      = 193356,   // Your combo-generating abilities generate 1 additional combo point
    SPELL_ROGUE_BURIED_TREASURE                 = 199600,   // Your base Energy regeneration is increased by 25%
    SPELL_ROGUE_CANNONBALL_BARRAGE              = 185767,
    SPELL_ROGUE_CANNONBALL_BARRAGE_DAMAGE       = 185779,
    SPELL_ROGUE_CHEAP_TRICKS_HONOR_TALENT       = 212035,
    SPELL_ROGUE_CHEAP_TRICKS_DEBUFF             = 212150,
    SPELL_ROGUE_CHEAT_DEATH_COOLDOWN            = 45181,
    SPELL_ROGUE_CHEAT_DEATH_TRIGGER             = 31231,
    SPELL_ROGUE_CLOAK_OF_SHADOWS                = 31224,
    SPELL_ROGUE_COLD_BLOOD_AURA                 = 213981,
    SPELL_ROGUE_COLD_BLOOD_DAMAGE               = 213983,
    SPELL_ROGUE_CREEPING_VENOM                  = 198092,   // Honor Talent poison
    SPELL_ROGUE_CREEPING_VENOM_AURA             = 198097,
    SPELL_ROGUE_CRIPPLING_POISON                = 3408,
    SPELL_ROGUE_CRIPPLING_POISON_AURA           = 3409,
    SPELL_ROGUE_CURSE_OF_THE_DREADBLADES        = 202665,
    SPELL_ROGUE_CURSE_OF_THE_DREADBLADES_DAMAGE = 202669,
    SPELL_ROGUE_DAGGER_IN_THE_DARK_TRIGGERED    = 198688,
    SPELL_ROGUE_DEADLY_BREW_TALENT              = 197044,
    SPELL_ROGUE_DEADLY_POISON                   = 2823,
    SPELL_ROGUE_DEADLY_POISON_AURA              = 2818,
    SPELL_ROGUE_DEADLY_POISON_INSTANT_DAMAGE    = 113780,
    SPELL_ROGUE_DEATH_FROM_ABOVE                = 152150,
    SPELL_ROGUE_DECEPTION_PROC_AURA             = 202755,
    SPELL_ROGUE_EMBRACE_OF_DARKNESS_PROC_AURA   = 197604,
    SPELL_ROGUE_EMBRACE_OF_DARKNESS_SHIELD      = 197603,
    SPELL_ROGUE_ENVENOM                         = 32645,
    SPELL_ROGUE_EVASION                         = 5277,
    SPELL_ROGUE_EVISCERATE                      = 196819,
    SPELL_ROGUE_FAN_OF_KNIVES                   = 51723,
    SPELL_ROGUE_FASTER_THAN_THE_LIGHT_TRIGGER   = 197270,
    SPELL_ROGUE_FEINT                           = 1966,
    SPELL_ROGUE_FINALITY_EVISCERATE             = 197496,
    SPELL_ROGUE_FINALITY_NIGHTBLADE             = 197498,
    SPELL_ROGUE_FLICKERING_SHADOWS_PROC_AURA    = 197256,
    SPELL_ROGUE_FORTUNE_S_BITE                  = 197369,
    SPELL_ROGUE_FROM_THE_SHADOWS_BUFF           = 192432,
    SPELL_ROGUE_FROM_THE_SHADOWS_DAMAGE         = 192434,
    SPELL_ROGUE_FROM_THE_SHADOWS_TALENT         = 192428,
    SPELL_ROGUE_GARROTE                         = 703,
    SPELL_ROGUE_GARROTE_LEVEL_BONUS             = 231719,
    SPELL_ROGUE_GARROTE_SILENCE                 = 1330,
    SPELL_ROGUE_GHOSTLY_SHELL                   = 202533,
    SPELL_ROGUE_GHOSTLY_SHELL_DUMMY_AURA        = 202534,
    SPELL_ROGUE_GHOSTLY_SHELL_HEAL              = 202536,
    SPELL_ROGUE_GRAND_MELEE                     = 193358,   // Increases your attack speed by 50% and your Leech by 25%
    SPELL_ROGUE_GRAPPLING_HOOK                  = 195457,
    SPELL_ROGUE_GRAPPLING_HOOK_JUMP             = 227180,
    SPELL_ROGUE_GREED_HEAL                      = 202824,
    SPELL_ROGUE_GREED_HEAL_VISUAL               = 206356,
    SPELL_ROGUE_GREED_PROC_AOE                  = 202822,
    SPELL_ROGUE_HIDDEN_BLADE                    = 202754,
    SPELL_ROGUE_HONOR_AMONG_THIEVES_ENERGIZE    = 51699,
    SPELL_ROGUE_INTENT_TO_KILL                  = 197007,
    SPELL_ROGUE_INTERNAL_BLEEDING               = 154904,
    SPELL_ROGUE_INTERNAL_BLEEDING_PERIODIC      = 154953,
    SPELL_ROGUE_JOLLY_JOKER                     = 199603,   // Saber Slash has an additional $s1% chance of striking an additional time
    SPELL_ROGUE_KIDNEY_SHOT                     = 408,
    SPELL_ROGUE_KILLING_SPREE                   = 51690,
    SPELL_ROGUE_KILLING_SPREE_DMG_BUFF          = 61851,
    SPELL_ROGUE_KILLING_SPREE_TELEPORT          = 57840,
    SPELL_ROGUE_KILLING_SPREE_WEAPON_DMG        = 57841,
    SPELL_ROGUE_KINGSBANE                       = 192759,
    SPELL_ROGUE_MAIN_GAUCHE                     = 86392,
    SPELL_ROGUE_MANEUVERABILITY                 = 197000,
    SPELL_ROGUE_MANEUVERABILITY_BUFF            = 197003,
    SPELL_ROGUE_MARKED_FOR_DEATH                = 137619,
    SPELL_ROGUE_MARKED_FOR_DEATH_TRIGGER        = 140149,
    SPELL_ROGUE_MASTER_OF_SHADOWS_EFFECT        = 196980,
    SPELL_ROGUE_MASTER_OF_SHADOWS_TALENT        = 196976,
    SPELL_ROGUE_MASTER_OF_SUBTLETY_EFFECT       = 31665,
    SPELL_ROGUE_MASTER_OF_SUBTLETY_TALENT       = 31223,
    SPELL_ROGUE_MIND_NUMBING_POISON             = 197050,
    SPELL_ROGUE_MIND_NUMBING_POISON_AURA        = 197051,
    SPELL_ROGUE_MIND_NUMBING_POISON_DAMAGE      = 197062,
    SPELL_ROGUE_MUTILATE                        = 5374,
    SPELL_ROGUE_MUTILATE_OFFHAND                = 27576,
    SPELL_ROGUE_NIGHT_TERRORS                   = 206760,
    SPELL_ROGUE_NIGHTBLADE                      = 195452,
    SPELL_ROGUE_NIGHTSTALKER_EFFECT             = 130493,
    SPELL_ROGUE_NIGHTSTALKER_TALENT             = 14062,
    SPELL_ROGUE_OPPORTUNITY                     = 195627,
    SPELL_ROGUE_PHANTOM_ASSASSIN                = 216883,
    SPELL_ROGUE_POISON_BOMB                     = 192660,   // Ticks 6 times in 3 seconds, areatrigger damage
    SPELL_ROGUE_POISON_KNIVES                   = 192376,
    SPELL_ROGUE_POISONED_KNIFE                  = 185565,
    SPELL_ROGUE_POISONED_KNIFE_DAMAGE           = 192380,
    SPELL_ROGUE_RELENTLESS_STRIKES_ENERGIZE     = 98440,
    SPELL_ROGUE_ROLL_THE_BONES                  = 193316,
    SPELL_ROGUE_RUN_THROUGH                     = 2098,
    SPELL_ROGUE_RUPTURE                         = 1943,
    SPELL_ROGUE_SABER_SLASH                     = 193315,
    SPELL_ROGUE_SABER_SLASH_MULTI               = 197834,
    SPELL_ROGUE_SHADOW_BLADE                    = 121473,
    SPELL_ROGUE_SHADOW_BLADE_OFF_HAND           = 121474,
    SPELL_ROGUE_SHADOW_DANCE                    = 185313,
    SPELL_ROGUE_SHADOW_DANCE_EFFECT             = 185422,
    SPELL_ROGUE_SHADOW_FOCUS_EFFECT             = 112942,
    SPELL_ROGUE_SHADOW_FOCUS_TALENT             = 108209,
    SPELL_ROGUE_SHADOW_S_CAREERS                = 198665,
    SPELL_ROGUE_SHADOW_S_CAREERS_BUFF           = 209427,
    SPELL_ROGUE_SHADOW_SWIFTNESS                = 192422,
    SPELL_ROGUE_SHADOW_TECHNIQUES_ENERGIZE      = 196911,
    SPELL_ROGUE_SHADOWSTEP                      = 36554,
    SPELL_ROGUE_SHADOWSTRIKE_RANGE_BUFF         = 231718,
    SPELL_ROGUE_SHADOWY_DUEL                    = 207736,
    SPELL_ROGUE_SHADOWY_DUEL_INVIS              = 207756,
    SPELL_ROGUE_SHADOWY_DUEL_STALK              = 210558,
    SPELL_ROGUE_SHARK_INFESTED_WATERS           = 193357,   // +25% crit
    SPELL_ROGUE_SHELLSHOCKED                    = 185778,
    SPELL_ROGUE_SHURIKEN_STORM                  = 197835,
    SPELL_ROGUE_SHURIKEN_STORM_ENERGIZE         = 212743,
    SPELL_ROGUE_SILHOUETTE                      = 197899,
    SPELL_ROGUE_SPRINT                          = 2983,
    SPELL_ROGUE_STEALTH                         = 1784,
    SPELL_ROGUE_STEALTH_EFFECT                  = 158185,
    SPELL_ROGUE_STEALTH_EFFECT_2                = 158188,
    SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_SNARE   = 222775,
    SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_STUN    = 196958,
    SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_TALENT  = 196951,
    SPELL_ROGUE_SUBTERFUGE                      = 115192,
    SPELL_ROGUE_SUBTERFUGE_EFFECT               = 115192,
    SPELL_ROGUE_SUBTERFUGE_TALENT               = 108208,
    SPELL_ROGUE_SURGE_OF_TOXIN                  = 192424,
    SPELL_ROGUE_SURGE_OF_TOXIN_DEBUFF           = 192425,
    SPELL_ROGUE_SYMBOLS_OF_DEATH                = 212283,
    SPELL_ROGUE_SYSTEM_SHOCK                    = 198145,
    SPELL_ROGUE_SYSTEM_SHOCK_DAMAGE             = 198222,
    SPELL_ROGUE_TAKE_YOUR_CUT_BONUS             = 198368,
    SPELL_ROGUE_TAKE_YOUR_CUT_PROC_AURA         = 198265,
    SPELL_ROGUE_TALENT_DFA                      = 152150,   // Base talent
    SPELL_ROGUE_TALENT_DFA_DAMAGE_MOD           = 163786,   // Applied with Base talent
    SPELL_ROGUE_TALENT_DFA_DUMMY                = 178070,   // Casted after Jump
    SPELL_ROGUE_TALENT_DFA_JUMP                 = 156327,   // Jump - Triggered by Base Talent (jump to target)
    SPELL_ROGUE_TALENT_DFA_JUMP_DUMMY           = 156527,   // Triggered by Jump
    SPELL_ROGUE_TALENT_DFA_PREJUMP              = 178236,   // Executed with Base talent
    SPELL_ROGUE_THICK_AS_THIEVES                = 221622,
    SPELL_ROGUE_THIEF_S_BARGAIN                 = 212081,
    SPELL_ROGUE_THIEF_S_BARGAIN_BUFF            = 213985,
    SPELL_ROGUE_THUGGEE                         = 196861,
    SPELL_ROGUE_TRICKS_OF_THE_TRADE_DPS_BOOST   = 59628,
    SPELL_ROGUE_TRICKS_OF_THE_TRADE_DPS_BOOST_2 = 221630,
    SPELL_ROGUE_TRUE_BEARING                    = 193359,   // Cooldown reduction on alot of rogue spells
    SPELL_ROGUE_TURN_THE_TABLES_BONUS           = 198027,
    SPELL_ROGUE_UNFAIR_ADVANTAGE_BUFF           = 209417,
    SPELL_ROGUE_URGE_TO_KILL                    = 192384,
    SPELL_ROGUE_VANISH                          = 1856,
    SPELL_ROGUE_VANISH_AURA                     = 11327,
    SPELL_ROGUE_VEIL_OF_MIDNIGHT                = 198952,
    SPELL_ROGUE_VEIL_OF_MIDNIGHT_BUFF           = 199027,
    SPELL_ROGUE_VENDETTA                        = 79140,
    SPELL_ROGUE_VENOM_RUSH                      = 152152,
    SPELL_ROGUE_VENOMOUS_VIM_ENERGIZE           = 51637,
    SPELL_ROGUE_WEAPON_MASTER_DOT_DAMAGE        = 193536,
    SPELL_ROGUE_WOUND_POISON                    = 8679,
    SPELL_ROGUE_WOUND_POISON_AURA               = 8680,
};

enum RogueMiscData
{
    SPELL_VISUAL_CANNONBALL_BARRAGE   = 50647,
    SPELL_GEN_COMBO_POINT             = 211665
};

static std::unordered_map<uint32 /*poison aura*/, uint32 /*triggerspell*/> const poisonAuras =
{
    { SPELL_ROGUE_WOUND_POISON,         SPELL_ROGUE_WOUND_POISON_AURA        },
    { SPELL_ROGUE_DEADLY_POISON,        SPELL_ROGUE_DEADLY_POISON_AURA       },
    { SPELL_ROGUE_CRIPPLING_POISON,     SPELL_ROGUE_CRIPPLING_POISON_AURA    },
    { SPELL_ROGUE_AGONIZING_POISON,     SPELL_ROGUE_AGONIZING_POISON_AURA    },
    { SPELL_ROGUE_MIND_NUMBING_POISON,  SPELL_ROGUE_MIND_NUMBING_POISON_AURA }
};

// 193539 - Alacrity
class spell_legion_rogue_alacrity : public SpellScriptLoader
{
    public:
        spell_legion_rogue_alacrity() : SpellScriptLoader("spell_legion_rogue_alacrity") { }

        class spell_legion_rogue_alacrity_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_alacrity_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetProcSpell() || eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) == 0 || GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;

                // Small cooldown to prevent double procs from Envenom
                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(100));

                if (AuraEffect* baseProcChance = GetEffect(EFFECT_1))
                    return roll_chance_i(eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) * baseProcChance->GetAmount());

                return false;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_alacrity_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_alacrity_AuraScript();
        }
};

// 53 - Backstab
class spell_legion_rogue_backstab : public SpellScriptLoader
{
    public:
        spell_legion_rogue_backstab() : SpellScriptLoader("spell_legion_rogue_backstab") { }

        class spell_legion_rogue_backstab_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_backstab_SpellScript);

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->isInBack(GetCaster(), static_cast<float>(M_PI)))
                {
                    int32 damage = GetHitDamage();
                    if (SpellEffectInfo const* pctInfo = GetEffectInfo(EFFECT_3))
                        AddPct(damage, pctInfo->CalcValue(GetCaster()));

                    SetHitDamage(damage);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_backstab_SpellScript::HandleDamage, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_backstab_SpellScript();
        }
};

// 192657 - Bag of Tricks
class spell_legion_rogue_bag_of_tricks : public SpellScriptLoader
{
    public:
        spell_legion_rogue_bag_of_tricks() : SpellScriptLoader("spell_legion_rogue_bag_of_tricks") { }

        class spell_legion_rogue_bag_of_tricks_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_bag_of_tricks_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_BAG_OF_TRICKS_AREATRIGGER
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetProcSpell() || eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) == 0)
                    return false;

                if (AuraEffect* aurEff = GetEffect(EFFECT_0))
                    return roll_chance_i((aurEff->GetAmount() / 10) * eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS));
                return false;
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_ROGUE_BAG_OF_TRICKS_AREATRIGGER, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_bag_of_tricks_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_bag_of_tricks_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_bag_of_tricks_AuraScript();
        }
};

// 199804 - Between the Eyes
class spell_legion_rogue_between_the_eyes : public SpellScriptLoader
{
    public:
        spell_legion_rogue_between_the_eyes() : SpellScriptLoader("spell_legion_rogue_between_the_eyes") { }

        class spell_legion_rogue_between_the_eyes_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_between_the_eyes_SpellScript);

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(GetHitDamage() * GetSpell()->GetPowerCost(POWER_COMBO_POINTS));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_between_the_eyes_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_between_the_eyes_SpellScript();
        }
};

// 13877 - Blade Flurry
class spell_legion_rogue_blade_flurry : public SpellScriptLoader
{
    public:
        spell_legion_rogue_blade_flurry() : SpellScriptLoader("spell_legion_rogue_blade_flurry") { }

        class spell_legion_rogue_blade_flurry_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_blade_flurry_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_BLADE_FLURRY_DAMAGE,
                    SPELL_ROGUE_MAIN_GAUCHE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo())
                    return false;

                if (eventInfo.GetSpellInfo() &&
                    ((!eventInfo.GetSpellInfo()->HasAttribute(SPELL_ATTR3_MAIN_HAND) && !eventInfo.GetSpellInfo()->HasAttribute(SPELL_ATTR3_REQ_OFFHAND)) ||
                    (eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_MAIN_GAUCHE || eventInfo.GetSpellInfo()->IsTargetingArea(DIFFICULTY_NONE))))
                    return false;
                return true;
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                if (SpellEffectInfo const* pctInfo = GetAura()->GetSpellEffectInfo(EFFECT_2))
                {
                    int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), pctInfo->CalcValue(GetTarget()));
                    GetTarget()->CastCustomSpell(SPELL_ROGUE_BLADE_FLURRY_DAMAGE, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetActionTarget(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_blade_flurry_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_blade_flurry_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_blade_flurry_AuraScript();
        }
};

// 22482 - Blade Flurry
class spell_legion_rogue_blade_flurry_chain : public SpellScriptLoader
{
    public:
        spell_legion_rogue_blade_flurry_chain() : SpellScriptLoader("spell_legion_rogue_blade_flurry_chain") { }

        class spell_legion_rogue_blade_flurry_chain_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_blade_flurry_chain_SpellScript);

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit() == GetExplTargetUnit())
                    SetHitDamage(0);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_blade_flurry_chain_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_blade_flurry_chain_SpellScript();
        }
};

// 2094 - Blind
class spell_legion_rogue_blind : public SpellScriptLoader
{
    public:
        spell_legion_rogue_blind() : SpellScriptLoader("spell_legion_rogue_blind") { }

        class spell_legion_rogue_blind_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_blind_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_CHEAP_TRICKS_HONOR_TALENT,
                    SPELL_ROGUE_CHEAP_TRICKS_DEBUFF
                });
            }

            void EffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_ROGUE_CHEAP_TRICKS_HONOR_TALENT))
                        caster->CastSpell(GetTarget(), SPELL_ROGUE_CHEAP_TRICKS_DEBUFF, true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_legion_rogue_blind_AuraScript::EffectRemove, EFFECT_0, SPELL_AURA_MOD_DECREASE_SPEED, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_blind_AuraScript();
        }
};

// 202895 - Blunderbuss (Level 110)
class spell_legion_rogue_blunderbuss : public SpellScriptLoader
{
    public:
        spell_legion_rogue_blunderbuss() : SpellScriptLoader("spell_legion_rogue_blunderbuss") { }

        class spell_legion_rogue_blunderbuss_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_blunderbuss_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_BLUNDERBUSS_OVERRIDE
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->RemoveAurasDueToSpell(SPELL_ROGUE_BLUNDERBUSS_OVERRIDE);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_rogue_blunderbuss_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_blunderbuss_SpellScript();
        }
};

// 209754 - Boarding Party
class spell_legion_rogue_boarding_party : public SpellScriptLoader
{
    public:
        spell_legion_rogue_boarding_party() : SpellScriptLoader("spell_legion_rogue_boarding_party") { }

        class spell_legion_rogue_boarding_party_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_boarding_party_SpellScript);

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetCaster());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_rogue_boarding_party_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_boarding_party_SpellScript();
        }
};

// 199740 - Bribe
class spell_legion_rogue_bribe : public SpellScriptLoader
{
    public:
        spell_legion_rogue_bribe() : SpellScriptLoader("spell_legion_rogue_bribe") { }

        class spell_legion_rogue_bribe_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_bribe_SpellScript);

            SpellCastResult DoCastCheck()
            {
                Unit* target = GetExplTargetUnit();

                // can't be used against players or player controlled units
                if (!target || target->GetTypeId() == TYPEID_PLAYER)
                    return SPELL_FAILED_BAD_TARGETS;

                // can't be used against bosses
                if (Creature* creature = target->ToCreature())
                    if (CreatureTemplate const* creatureTemplate = creature->GetCreatureTemplate())
                        if (creatureTemplate->flags_extra & (CREATURE_FLAG_EXTRA_INSTANCE_BIND | CREATURE_FLAG_EXTRA_DUNGEON_BOSS))
                            return SPELL_FAILED_BAD_TARGETS;

                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_legion_rogue_bribe_SpellScript::DoCastCheck);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_bribe_SpellScript();
        }
};

// 185767 - Cannonball Barrage
class spell_legion_rogue_cannonball_barrage : public SpellScriptLoader
{
    public:
        spell_legion_rogue_cannonball_barrage() : SpellScriptLoader("spell_legion_rogue_cannonball_barrage") { }

        class spell_legion_rogue_cannonball_barrage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_cannonball_barrage_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_VISUAL_CANNONBALL_BARRAGE,
                    SPELL_ROGUE_CANNONBALL_BARRAGE_DAMAGE
                });
            }

            void HandlePeriodic(AuraEffect const* /*aurEff*/)
            {
                if (DynamicObject* dynObject = GetTarget()->GetDynObject(GetId()))
                {
                    Position targetRndPos = dynObject->GetRandomNearPosition(3.00f);
                    GetTarget()->SendPlayOrphanSpellVisual(targetRndPos, SPELL_VISUAL_CANNONBALL_BARRAGE, 0.6f, true, true);
                    GetTarget()->CastSpell(dynObject->GetPositionX(), dynObject->GetPositionY(), dynObject->GetPositionZ(), SPELL_ROGUE_CANNONBALL_BARRAGE_DAMAGE, true);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_rogue_cannonball_barrage_AuraScript::HandlePeriodic, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_cannonball_barrage_AuraScript();
        }
};

// 185779 - Cannonball Barrage
class spell_legion_rogue_cannonball_barrage_damage : public SpellScriptLoader
{
    public:
        spell_legion_rogue_cannonball_barrage_damage() : SpellScriptLoader("spell_legion_rogue_cannonball_barrage_damage") { }

        class spell_legion_rogue_cannonball_barrage_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_cannonball_barrage_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SHELLSHOCKED
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_ROGUE_SHELLSHOCKED, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_cannonball_barrage_damage_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_cannonball_barrage_damage_SpellScript();
        }
};

// 31230 - Cheat Death
class spell_legion_rogue_cheat_death : public SpellScriptLoader
{
    public:
        spell_legion_rogue_cheat_death() : SpellScriptLoader("spell_legion_rogue_cheat_death") { }

        class spell_legion_rogue_cheat_death_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_cheat_death_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_CHEAT_DEATH_COOLDOWN,
                    SPELL_ROGUE_CHEAT_DEATH_TRIGGER
                });
            }

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                Unit* target = GetTarget();
                if (dmgInfo.GetDamage() < target->GetHealth() || target->HasAura(SPELL_ROGUE_CHEAT_DEATH_COOLDOWN))
                    return;

                target->CastSpell(target, SPELL_ROGUE_CHEAT_DEATH_COOLDOWN, true);
                target->CastSpell(target, SPELL_ROGUE_CHEAT_DEATH_TRIGGER, true);
                target->SetHealth(target->CountPctFromMaxHealth(7));
                absorbAmount = dmgInfo.GetDamage();
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_legion_rogue_cheat_death_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_legion_rogue_cheat_death_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_cheat_death_AuraScript();
        }
};

// 31224 - Cloak of Shadows
class spell_legion_rogue_cloak_of_shadows : public SpellScriptLoader
{
    public:
        spell_legion_rogue_cloak_of_shadows() : SpellScriptLoader("spell_legion_rogue_cloak_of_shadows") { }

        class spell_legion_rogue_cloak_of_shadows_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_cloak_of_shadows_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_GHOSTLY_SHELL_HEAL
                });
            }

            void HandleEffectLaunchTarget(SpellEffIndex effIndex)
            {
                Unit* target = GetHitUnit();
                PreventHitDefaultEffect(effIndex);

                uint32 dispelMask = SpellInfo::GetDispelMask(DISPEL_ALL);
                Unit::AuraApplicationMap& Auras = target->GetAppliedAuras();
                for (Unit::AuraApplicationMap::iterator itr = Auras.begin(); itr != Auras.end();)
                {
                    // remove all harmful spells on you...
                    SpellInfo const* spellInfo = itr->second->GetBase()->GetSpellInfo();
                    if (((spellInfo->DmgClass == SPELL_DAMAGE_CLASS_MAGIC && spellInfo->GetSchoolMask() != SPELL_SCHOOL_MASK_NORMAL) // only affect magic spells
                        || (spellInfo->GetDispelMask() & dispelMask)) &&
                        // ignore positive and passive auras
                        !itr->second->IsPositive() && !itr->second->GetBase()->IsPassive())
                    {
                        target->RemoveAura(itr);

                        // Ghostly Shell (Artifact trait) - "The Dreadblades heal you for 2% of your maximum health for each effect cleared by Cloak of Shadows"
                        if (AuraEffect* ghostlyShell = target->GetAuraEffect(SPELL_ROGUE_GHOSTLY_SHELL, EFFECT_0))
                            target->CastCustomSpell(SPELL_ROGUE_GHOSTLY_SHELL_HEAL, SPELLVALUE_BASE_POINT0, ghostlyShell->GetAmount(), target, TRIGGERED_FULL_MASK);
                    }
                    else
                        ++itr;
                }

                if (target->HasAura(SPELL_ROGUE_GHOSTLY_SHELL))
                    target->CastSpell(target, SPELL_ROGUE_GHOSTLY_SHELL_DUMMY_AURA, true);
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_legion_rogue_cloak_of_shadows_SpellScript::HandleEffectLaunchTarget, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_cloak_of_shadows_SpellScript();
        }
};

// 213981 - Cold Blood
class spell_legion_rogue_cold_blood : public SpellScriptLoader
{
    public:
        spell_legion_rogue_cold_blood() : SpellScriptLoader("spell_legion_rogue_cold_blood") { }

        class spell_legion_rogue_cold_blood_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_cold_blood_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_COLD_BLOOD_DAMAGE
                });
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                int32 healthPct = int32(eventInfo.GetProcTarget()->CountPctFromMaxHealth(GetEffect(EFFECT_0)->GetAmount()));
                GetTarget()->CastCustomSpell(SPELL_ROGUE_COLD_BLOOD_DAMAGE, SPELLVALUE_BASE_POINT0, healthPct, eventInfo.GetProcTarget(), true);
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_legion_rogue_cold_blood_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_cold_blood_AuraScript();
        }
};

// 35551 - Combat Potency
class spell_legion_rogue_combat_potency : public SpellScriptLoader
{
    public:
        spell_legion_rogue_combat_potency() : SpellScriptLoader("spell_legion_rogue_combat_potency") { }

        class spell_legion_rogue_combat_potency_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_combat_potency_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_0))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!(eventInfo.GetTypeMask() & PROC_FLAG_DONE_OFFHAND_ATTACK))
                    return false;

               return roll_chance_f(float(GetEffect(EFFECT_0)->GetAmount() * (GetTarget()->GetBaseAttackTime(OFF_ATTACK) * 1.4f / 1000.0f)));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_combat_potency_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_combat_potency_AuraScript();
        }
};

// 212219 - Control is King
class spell_legion_rogue_control_is_king : public SpellScriptLoader
{
    public:
        spell_legion_rogue_control_is_king() : SpellScriptLoader("spell_legion_rogue_control_is_king") { }

        class spell_legion_rogue_control_is_king_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_control_is_king_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_ADRENALINE_RUSH
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo() || GetTarget()->GetGUID() == GetCasterGUID())
                    return false;

                if (eventInfo.GetSpellInfo()->HasAura(DIFFICULTY_NONE, SPELL_AURA_MOD_SILENCE)
                    || eventInfo.GetSpellInfo()->HasAura(DIFFICULTY_NONE, SPELL_AURA_MOD_STUN)
                    || eventInfo.GetSpellInfo()->HasAura(DIFFICULTY_NONE, SPELL_AURA_TRANSFORM))
                    return true;
                return false;
            }

            void HandleProc(ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(SPELL_ROGUE_ADRENALINE_RUSH, SPELLVALUE_AURA_DURATION, 3000, caster, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_control_is_king_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_control_is_king_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_control_is_king_AuraScript();
        }
};

// 198097 - Creeping Venom (Honor Talent)
class spell_legion_rogue_creeping_venom : public SpellScriptLoader
{
    public:
        spell_legion_rogue_creeping_venom() : SpellScriptLoader("spell_legion_rogue_creeping_venom") { }

        class spell_legion_rogue_creeping_venom_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_creeping_venom_AuraScript);

            void HandlePeriodic(AuraEffect const* /*aurEff*/)
            {
                if (GetTarget()->isMoving())
                    GetAura()->RefreshDuration();
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_rogue_creeping_venom_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_creeping_venom_AuraScript();
        }
};

// 2098 - Run Through
// 193316 - Roll the Bones
// 199804 - Between the Eyes
class spell_legion_rogue_curse_of_the_dreadblades : public SpellScriptLoader
{
    public:
        spell_legion_rogue_curse_of_the_dreadblades() : SpellScriptLoader("spell_legion_rogue_curse_of_the_dreadblades") { }

        class spell_legion_rogue_curse_of_the_dreadblades_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_curse_of_the_dreadblades_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_CURSE_OF_THE_DREADBLADES,
                    SPELL_ROGUE_CURSE_OF_THE_DREADBLADES_DAMAGE
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_ROGUE_CURSE_OF_THE_DREADBLADES);
            }

            void HandleAfterCast()
            {
                int32 damage = int32(GetCaster()->CountPctFromMaxHealth(sSpellMgr->AssertSpellInfo(SPELL_ROGUE_CURSE_OF_THE_DREADBLADES_DAMAGE)->GetEffect(EFFECT_0)->CalcValue(GetCaster())));
                GetCaster()->CastCustomSpell(SPELL_ROGUE_CURSE_OF_THE_DREADBLADES_DAMAGE, SPELLVALUE_BASE_POINT0, damage, GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_rogue_curse_of_the_dreadblades_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_curse_of_the_dreadblades_SpellScript();
        }
};

// 198675 - Dagger in the Dark
class spell_legion_rogue_dagger_in_the_dark : public SpellScriptLoader
{
    public:
        spell_legion_rogue_dagger_in_the_dark() : SpellScriptLoader("spell_legion_rogue_dagger_in_the_dark") { }

        class spell_legion_rogue_dagger_in_the_dark_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_dagger_in_the_dark_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_DAGGER_IN_THE_DARK_TRIGGERED,
                    SPELL_ROGUE_SHADOW_DANCE
                });
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                PreventDefaultAction();
                if (GetTarget()->HasAuraType(SPELL_AURA_MOD_STEALTH) || GetTarget()->HasAura(SPELL_ROGUE_SHADOW_DANCE))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_DAGGER_IN_THE_DARK_TRIGGERED, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_rogue_dagger_in_the_dark_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_dagger_in_the_dark_AuraScript();
        }
};

// 2818 - Deadly Poison (Level 110)
class spell_legion_rogue_deadly_poison : public SpellScriptLoader
{
    public:
        spell_legion_rogue_deadly_poison() : SpellScriptLoader("spell_legion_rogue_deadly_poison") { }

        class spell_legion_rogue_deadly_poison_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_deadly_poison_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_DEADLY_POISON_INSTANT_DAMAGE
                });
            }

            void HandleEffectLaunchTarget(SpellEffIndex /*effIndex*/)
            {
                // Subsequent poison applications deal instant Nature damage.
                if (GetHitUnit()->HasAura(GetSpellInfo()->Id, GetCaster()->GetGUID()))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_ROGUE_DEADLY_POISON_INSTANT_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_legion_rogue_deadly_poison_SpellScript::HandleEffectLaunchTarget, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_deadly_poison_SpellScript();
        }
};

// 152150 - Death from above
class spell_legion_rogue_death_from_above : public SpellScriptLoader
{
    public:
        spell_legion_rogue_death_from_above() : SpellScriptLoader("spell_legion_rogue_death_from_above") { }

        class spell_legion_rogue_death_from_above_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_death_from_above_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_TALENT_DFA_DAMAGE_MOD,
                    SPELL_ROGUE_TALENT_DFA_PREJUMP
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (Aura* aura = GetCaster()->AddAura(SPELL_ROGUE_TALENT_DFA_DAMAGE_MOD, GetCaster()))
                {
                    aura->SetCastExtraParam("ComboPoints", GetSpell()->GetPowerCost(POWER_COMBO_POINTS));
                    aura->SetCastExtraParam("TargetGUID", GetHitUnit()->GetGUID());
                    GetCaster()->CastSpell(GetCaster(), SPELL_ROGUE_TALENT_DFA_PREJUMP, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_death_from_above_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_death_from_above_SpellScript();
        }

        class spell_legion_rogue_death_from_above_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_death_from_above_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_TALENT_DFA_DAMAGE_MOD,
                    SPELL_ROGUE_TALENT_DFA_PREJUMP
                });
            }

            void EffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (Aura* dmgAura = caster->GetAura(SPELL_ROGUE_TALENT_DFA_DAMAGE_MOD))
                    if (ObjectGuid const* targetGUID = dmgAura->GetCastExtraParam<ObjectGuid>("TargetGUID"))
                        if (Unit* target = ObjectAccessor::GetUnit(*GetTarget(), *targetGUID))
                            if (target->IsAlive() && !target->HasStealthAura() && !target->HasInvisibilityAura())
                                GetTarget()->CastSpell(target, SPELL_ROGUE_TALENT_DFA_JUMP, true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_legion_rogue_death_from_above_AuraScript::EffectRemove, EFFECT_3, SPELL_AURA_IGNORE_HIT_DIRECTION, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_death_from_above_AuraScript();
        }
};

// 178070 - Death from above After jump to target dummy (damage)
class spell_legion_rogue_death_from_above_damage : public SpellScriptLoader
{
    public:
        spell_legion_rogue_death_from_above_damage() : SpellScriptLoader("spell_legion_rogue_death_from_above_damage") { }

        class spell_legion_rogue_death_from_above_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_death_from_above_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_TALENT_DFA_JUMP_DUMMY,
                    AURA_SUBTLETY_ROGUE,
                    SPELL_ROGUE_EVISCERATE,
                    AURA_ASSASINATION_ROGUE,
                    SPELL_ROGUE_ENVENOM,
                    AURA_OUTLAW_ROGUE,
                    SPELL_ROGUE_RUN_THROUGH
                });
            }

            void HandleCast()
            {
                if (Unit* target = GetExplTargetUnit())
                {
                    Unit* caster = GetCaster();
                    caster->CastSpell(caster, SPELL_ROGUE_TALENT_DFA_JUMP_DUMMY, true);

                    if (Aura* aura = GetCaster()->GetAura(SPELL_ROGUE_TALENT_DFA_DAMAGE_MOD))
                        if (int32 const* cpUsed = aura->GetCastExtraParam<int32>("ComboPoints"))
                        {
                            // FIX ME HACK: Need solution to CastSpell with preset Combo Points
                            caster->SetPower(POWER_COMBO_POINTS, *cpUsed);
                            if (caster->HasAura(AURA_SUBTLETY_ROGUE))
                                caster->CastSpell(target, SPELL_ROGUE_EVISCERATE, TRIGGERED_IGNORE_GCD);
                            else if (caster->HasAura(AURA_ASSASINATION_ROGUE))
                                caster->CastSpell(target, SPELL_ROGUE_ENVENOM, TRIGGERED_IGNORE_GCD);
                            else if (caster->HasAura(AURA_OUTLAW_ROGUE))
                                caster->CastSpell(target, SPELL_ROGUE_RUN_THROUGH, TRIGGERED_IGNORE_GCD);
                        }
                }
            }

            void Register() override
            {
                OnCast += SpellCastFn(spell_legion_rogue_death_from_above_damage_SpellScript::HandleCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_death_from_above_damage_SpellScript();
        }
};

// 156327 - Death from above Jump to target
class spell_legion_rogue_death_from_above_jump_target : public SpellScriptLoader
{
    public:
        spell_legion_rogue_death_from_above_jump_target() : SpellScriptLoader("spell_legion_rogue_death_from_above_jump_target") { }

        class spell_legion_rogue_death_from_above_jump_target_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_death_from_above_jump_target_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_TALENT_DFA_DUMMY
                });
            }

            void HandleLaunch(SpellEffIndex /*effIndex*/)
            {
                PreventHitEffect(EFFECT_1);
                if (Unit* target = GetHitUnit())
                {
                    JumpArrivalCastArgs arrivalCast;
                    arrivalCast.SpellId = SPELL_ROGUE_TALENT_DFA_DUMMY; // GetEffectInfo()->TriggerSpell is a different one
                    arrivalCast.Target = target->GetGUID();
                    GetCaster()->GetMotionMaster()->MoveJump(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), GetCaster()->GetOrientation(), GetEffectInfo()->Amplitude, 1.0f, EVENT_CHARGE, true, { arrivalCast });
                }
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_legion_rogue_death_from_above_jump_target_SpellScript::HandleLaunch, EFFECT_1, SPELL_EFFECT_JUMP);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_death_from_above_jump_target_SpellScript();
        }
};

// 178236 - Death from above Pre Jump
class spell_legion_rogue_death_from_above_pre_jump : public SpellScriptLoader
{
    public:
        spell_legion_rogue_death_from_above_pre_jump() : SpellScriptLoader("spell_legion_rogue_death_from_above_pre_jump") { }

        class spell_legion_rogue_death_from_above_pre_jump_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_death_from_above_pre_jump_SpellScript);

            void HandleEffect(SpellEffIndex effIndex)
            {
                PreventHitEffect(effIndex);
                GetCaster()->GetMotionMaster()->MoveJump(GetCaster()->GetPositionX(), GetCaster()->GetPositionY(), GetCaster()->GetPositionZ() + GetEffectInfo()->Amplitude, GetCaster()->GetOrientation(), 9.0f, 9.0f);
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_legion_rogue_death_from_above_pre_jump_SpellScript::HandleEffect, EFFECT_0, SPELL_EFFECT_JUMP_DEST);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_death_from_above_pre_jump_SpellScript();
        }
};

// 185314 - Deepening Shadows
class spell_legion_rogue_deepening_shadows : public SpellScriptLoader
{
    public:
        spell_legion_rogue_deepening_shadows() : SpellScriptLoader("spell_legion_rogue_deepening_shadows") { }

        class spell_legion_rogue_deepening_shadows_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_deepening_shadows_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_NIGHTBLADE,
                    SPELL_ROGUE_KIDNEY_SHOT,
                    SPELL_ROGUE_EVISCERATE,
                    SPELL_ROGUE_SHADOW_DANCE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetProcSpell() || eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) == 0)
                    return false;

                return true;
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                if (SpellEffectInfo const* spellEffInfo = GetSpellInfo()->GetEffect(EFFECT_1))
                    GetTarget()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_ROGUE_SHADOW_DANCE)->ChargeCategoryId, (spellEffInfo->CalcValue() * eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) * IN_MILLISECONDS));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_deepening_shadows_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_deepening_shadows_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_deepening_shadows_AuraScript();
        }
};

// 32645 - Envenom
class spell_legion_rogue_envenom : public SpellScriptLoader
{
    public:
        spell_legion_rogue_envenom() : SpellScriptLoader("spell_legion_rogue_envenom") { }

        class spell_legion_rogue_envenom_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_envenom_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_CREEPING_VENOM,
                    SPELL_ROGUE_CREEPING_VENOM_AURA,
                    SPELL_ROGUE_SYSTEM_SHOCK,
                    SPELL_ROGUE_SYSTEM_SHOCK_DAMAGE
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(GetHitDamage() * GetSpell()->GetPowerCost(POWER_COMBO_POINTS));

                if (Unit* target = GetHitUnit())
                {
                    if (GetCaster()->HasAura(SPELL_ROGUE_CREEPING_VENOM))
                        GetCaster()->CastSpell(target, SPELL_ROGUE_CREEPING_VENOM_AURA, true);

                    if (GetCaster()->HasAura(SPELL_ROGUE_SYSTEM_SHOCK) && GetSpell()->GetPowerCost(POWER_COMBO_POINTS) >= 5)
                        if (target->HasAura(SPELL_ROGUE_GARROTE, GetCaster()->GetGUID())
                            && target->HasAura(SPELL_ROGUE_RUPTURE, GetCaster()->GetGUID())
                            && (target->HasAura(SPELL_ROGUE_DEADLY_POISON_AURA, GetCaster()->GetGUID())
                            || target->HasAura(SPELL_ROGUE_WOUND_POISON_AURA, GetCaster()->GetGUID())
                            || target->HasAura(SPELL_ROGUE_AGONIZING_POISON_AURA, GetCaster()->GetGUID())))
                            GetCaster()->CastSpell(target, SPELL_ROGUE_SYSTEM_SHOCK_DAMAGE, true);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_envenom_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_envenom_SpellScript();
        }
};

// 196819 - Eviscerate
class spell_legion_rogue_eviscerate : public SpellScriptLoader
{
    public:
        spell_legion_rogue_eviscerate() : SpellScriptLoader("spell_legion_rogue_eviscerate") { }

        class spell_legion_rogue_eviscerate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_eviscerate_SpellScript);

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(GetHitDamage() * GetSpell()->GetPowerCost(POWER_COMBO_POINTS));
                GetCaster()->RemoveAurasDueToSpell(SPELL_ROGUE_FINALITY_EVISCERATE);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_eviscerate_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_eviscerate_SpellScript();
        }
};

// 200806 - Exsanguinate
class spell_legion_rogue_exsanguinate : public SpellScriptLoader
{
    public:
        spell_legion_rogue_exsanguinate() : SpellScriptLoader("spell_legion_rogue_exsanguinate") { }

        class spell_legion_rogue_exsanguinate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_exsanguinate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_GARROTE,
                    SPELL_ROGUE_RUPTURE,
                    SPELL_ROGUE_INTERNAL_BLEEDING_PERIODIC
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                for (auto const& itr : GetHitUnit()->GetAuraEffectsByType(SPELL_AURA_PERIODIC_DAMAGE))
                    if (itr->GetBase()->GetCasterGUID() == GetCaster()->GetGUID())
                        if (itr->GetId() == SPELL_ROGUE_GARROTE || itr->GetId() == SPELL_ROGUE_RUPTURE || itr->GetId() == SPELL_ROGUE_INTERNAL_BLEEDING_PERIODIC)
                        {
                            itr->GetBase()->SetDuration(itr->GetBase()->GetDuration() / 2);
                            itr->SetPeriodicTimer(itr->GetPeriodicTimer() / 2);
                            itr->SetPeriod(int32(itr->GetPeriod() / 2));
                        }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_exsanguinate_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_exsanguinate_SpellScript();
        }
};

// 51723 - Fan of Knives
class spell_legion_rogue_fan_of_knives : public SpellScriptLoader
{
    public:
        spell_legion_rogue_fan_of_knives() : SpellScriptLoader("spell_legion_rogue_fan_of_knives") { }

        class spell_legion_rogue_fan_of_knives_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_fan_of_knives_SpellScript);

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* poinsonKnives = GetCaster()->GetAuraEffect(SPELL_ROGUE_POISON_KNIVES, EFFECT_0))
                {
                    if (AuraEffect* deadlyPoison = GetHitUnit()->GetAuraEffect(SPELL_ROGUE_DEADLY_POISON_AURA, EFFECT_0, GetCaster()->GetGUID()))
                    {
                        int32 damage = CalculatePct(deadlyPoison->GetDamage() * std::max<int32>(deadlyPoison->GetTotalTicks() - int32(deadlyPoison->GetTickNumber()), 1), poinsonKnives->GetAmount());
                        GetCaster()->CastCustomSpell(SPELL_ROGUE_POISONED_KNIFE_DAMAGE, SPELLVALUE_BASE_POINT0, damage, GetHitUnit(), TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_fan_of_knives_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_fan_of_knives_SpellScript();
        }
};

// 197270 - Faster Than Light Trigger
class spell_legion_rogue_faster_than_the_light : public SpellScriptLoader
{
    public:
        spell_legion_rogue_faster_than_the_light() : SpellScriptLoader("spell_legion_rogue_faster_than_the_light") { }

        class spell_legion_rogue_faster_than_the_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_faster_than_the_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_STEALTH,
                    SPELL_ROGUE_ASSASINATION_STEALTH,
                    SPELL_ROGUE_STEALTH_EFFECT_2
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_EXPIRE)
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_VANISH, true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_faster_than_the_light_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_faster_than_the_light_AuraScript();
        }
};

// 197406 - Finality
class spell_legion_rogue_finality : public SpellScriptLoader
{
    public:
        spell_legion_rogue_finality() : SpellScriptLoader("spell_legion_rogue_finality") { }

        class spell_legion_rogue_finality_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_finality_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_FINALITY_EVISCERATE,
                    SPELL_ROGUE_FINALITY_NIGHTBLADE,
                    SPELL_ROGUE_EVISCERATE,
                    SPELL_ROGUE_NIGHTBLADE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo() || !eventInfo.GetProcSpell() || eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) == 0)
                    return false;

                if (eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_EVISCERATE && GetTarget()->HasAura(SPELL_ROGUE_FINALITY_EVISCERATE))
                    return false;

                if (eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_NIGHTBLADE && GetTarget()->HasAura(SPELL_ROGUE_FINALITY_NIGHTBLADE))
                    return false;

                return true;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                int32 triggerSpellId = eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_EVISCERATE ? SPELL_ROGUE_FINALITY_EVISCERATE : SPELL_ROGUE_FINALITY_NIGHTBLADE;
                GetTarget()->CastCustomSpell(triggerSpellId, SPELLVALUE_BASE_POINT0, eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) * 4, GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_finality_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_finality_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_finality_AuraScript();
        }
};

// Vendetta Procs
// 192428 - From the shadows Talent (Unleash dagger barrage on target on Vendetta)
class spell_legion_rogue_from_the_shadows : public SpellScriptLoader
{
    public:
        spell_legion_rogue_from_the_shadows() : SpellScriptLoader("spell_legion_rogue_from_the_shadows") { }

        class spell_legion_rogue_from_the_shadows_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_from_the_shadows_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_FROM_THE_SHADOWS_BUFF
                });
            }

            void HandleProcShadows(ProcEventInfo& eventInfo)
            {
                if (Aura* shadowsAura = GetTarget()->AddAura(SPELL_ROGUE_FROM_THE_SHADOWS_BUFF, GetTarget()))
                    shadowsAura->SetCastExtraParam("targetGUID", eventInfo.GetActionTarget()->GetGUID());
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_legion_rogue_from_the_shadows_AuraScript::HandleProcShadows);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_from_the_shadows_AuraScript();
        }
};

// 192432 - From the shadows buff
class spell_legion_rogue_from_the_shadows_buff : public SpellScriptLoader
{
    public:
        spell_legion_rogue_from_the_shadows_buff() : SpellScriptLoader("spell_legion_rogue_from_the_shadows_buff") { }

        class spell_legion_rogue_from_the_shadows_buff_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_from_the_shadows_buff_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_FROM_THE_SHADOWS_DAMAGE,
                    SPELL_ROGUE_VENDETTA
                });
            }

            void HandlePeriodic(AuraEffect const* /*aurEff*/)
            {
                if (ObjectGuid const* targetGuid = GetAura()->GetCastExtraParam<ObjectGuid>("targetGUID"))
                    if (Unit* target = ObjectAccessor::GetUnit(*GetTarget(), *targetGuid))
                        if (target->IsAlive() && target->HasAura(SPELL_ROGUE_VENDETTA))
                        {
                            Position startPosLeft = target->GetRandomNearPosition(8.0f);
                            Position startPosRight = target->GetRandomNearPosition(8.0f);
                            GetTarget()->SendPlayOrphanSpellVisual(target->GetGUID(), startPosLeft, 56804, 20.0f);
                            GetTarget()->SendPlayOrphanSpellVisual(target->GetGUID(), startPosRight, 56804, 20.0f);
                            GetTarget()->CastSpell(target, SPELL_ROGUE_FROM_THE_SHADOWS_DAMAGE, true);
                            GetTarget()->CastSpell(target, SPELL_ROGUE_FROM_THE_SHADOWS_DAMAGE, true);
                        }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_rogue_from_the_shadows_buff_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_from_the_shadows_buff_AuraScript();
        }
};

// 703 - Garrote
class spell_legion_rogue_garrote : public SpellScriptLoader
{
    public:
        spell_legion_rogue_garrote() : SpellScriptLoader("spell_legion_rogue_garrote") { }

        class spell_legion_rogue_garrote_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_garrote_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SUBTERFUGE_EFFECT,
                    SPELL_ROGUE_GARROTE_LEVEL_BONUS,
                    SPELL_ROGUE_GARROTE_SILENCE,
                    SPELL_ROGUE_SUBTERFUGE
                });
            }

            bool Load() override
            {
                _stealthActive = GetCaster()->HasStealthAura() || GetCaster()->HasAura(SPELL_ROGUE_SUBTERFUGE_EFFECT);
                return true;
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (_stealthActive && GetCaster()->HasAura(SPELL_ROGUE_GARROTE_LEVEL_BONUS))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_ROGUE_GARROTE_SILENCE, true);
            }

            void HandleAfterHit()
            {
                if (Aura* aura = GetHitAura())
                {
                    if (AuraEffect* subterfuge = GetCaster()->GetAuraEffect(SPELL_ROGUE_SUBTERFUGE, EFFECT_1))
                    {
                        if (AuraEffect* aurEff = aura->GetEffect(EFFECT_0))
                        {
                            int32 amount = aurEff->GetDamage();
                            AddPct(amount, subterfuge->GetAmount());
                            aurEff->SetDamage(amount);
                            aura->SetNeedClientUpdateForTargets();
                        }
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_garrote_SpellScript::HandleHit, EFFECT_1, SPELL_EFFECT_DUMMY);
                AfterHit += SpellHitFn(spell_legion_rogue_garrote_SpellScript::HandleAfterHit);
            }

        private:
            bool _stealthActive = false;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_garrote_SpellScript();
        }

        class spell_legion_rogue_garrote_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_garrote_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_THUGGEE
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_ROGUE_THUGGEE))
                        caster->GetSpellHistory()->ResetCooldown(GetId(), true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_garrote_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_garrote_AuraScript();
        }
};

// 202534 - Ghostly Shell (dummy aura)
class spell_legion_rogue_ghostly_shell_dummy_aura : public SpellScriptLoader
{
    public:
        spell_legion_rogue_ghostly_shell_dummy_aura() : SpellScriptLoader("spell_legion_rogue_ghostly_shell_dummy_aura") { }

        class spell_legion_rogue_ghostly_shell_dummy_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_ghostly_shell_dummy_aura_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_GHOSTLY_SHELL_HEAL
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (AuraEffect* ghostlyShell = GetTarget()->GetAuraEffect(SPELL_ROGUE_GHOSTLY_SHELL, EFFECT_0))
                    GetTarget()->CastCustomSpell(SPELL_ROGUE_GHOSTLY_SHELL_HEAL, SPELLVALUE_BASE_POINT0, ghostlyShell->GetAmount(), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_ghostly_shell_dummy_aura_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_ghostly_shell_dummy_aura_AuraScript();
        }
};

// 195457 - Grappling Hook
class spell_legion_rogue_grappling_hook: public SpellScriptLoader
{
    public:
        spell_legion_rogue_grappling_hook() : SpellScriptLoader("spell_legion_rogue_grappling_hook") { }

        class spell_legion_rogue_grappling_hook_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_grappling_hook_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_GRAPPLING_HOOK_JUMP
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                // This Spell doesn't occure in any sniff
                // GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_ROGUE_GRAPPLING_HOOK_JUMP, true);

                WorldLocation const* dest = GetHitDest();
                if (!dest)
                    return;

                // HACK: Calculations aren't blizzlike.. we don't have any data or information about the correct calculation
                float dist = dest->GetExactDist(*GetCaster());
                float speedXY = dist + dist * (14.f / GetSpellInfo()->GetMaxRange());
                float speedZ = 0.19642354884476423184854952469683f;

                std::vector<JumpArrivalCastArgs> arrivalCasts;
                Optional<Movement::SpellEffectExtraData> extra;
                extra = boost::in_place();
                extra->SpellVisualId = 47819;

                GetCaster()->GetMotionMaster()->MoveJump(*dest, speedXY, speedZ, EVENT_JUMP, false, arrivalCasts, extra.get_ptr());
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_legion_rogue_grappling_hook_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_grappling_hook_SpellScript();
        }
};

// 202822 - Greed
class spell_legion_rogue_greed_damage : public SpellScriptLoader
{
    public:
        spell_legion_rogue_greed_damage() : SpellScriptLoader("spell_legion_rogue_greed_damage") { }

        class spell_legion_rogue_greed_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_greed_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_GREED_HEAL_VISUAL,
                    SPELL_ROGUE_GREED_HEAL
                });
            }

            void CountTargets(std::list<WorldObject*>& targets)
            {
                hitCount = int32(targets.size());
            }

            void HandleVisual(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_ROGUE_GREED_HEAL_VISUAL, true);
            }

            void Heal()
            {
                GetCaster()->CastCustomSpell(SPELL_ROGUE_GREED_HEAL, SPELLVALUE_BASE_POINT0, int32(GetCaster()->GetMaxHealth() * 0.05f * hitCount), GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_rogue_greed_damage_SpellScript::CountTargets, EFFECT_1, TARGET_UNIT_SRC_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_greed_damage_SpellScript::HandleVisual, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
                AfterCast += SpellCastFn(spell_legion_rogue_greed_damage_SpellScript::Heal);
            }

            private:
                int32 hitCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_greed_damage_SpellScript();
        }
};

// 202820 - Greed
class spell_legion_rogue_greed_proc : public SpellScriptLoader
{
    public:
        spell_legion_rogue_greed_proc() : SpellScriptLoader("spell_legion_rogue_greed_proc") { }

        class spell_legion_rogue_greed_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_greed_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_GREED_PROC_AOE
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_GREED_PROC_AOE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_greed_proc_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_greed_proc_AuraScript();
        }
};

// 198031 - Honor Among Thieves
class spell_legion_rogue_honor_among_thieves : public SpellScriptLoader
{
    public:
        spell_legion_rogue_honor_among_thieves() : SpellScriptLoader("spell_legion_rogue_honor_among_thieves") { }

        class spell_legion_rogue_honor_among_thieves_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_honor_among_thieves_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_HONOR_AMONG_THIEVES_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return GetTarget()->GetGUID() != GetCasterGUID();
            }

            void HandleProc(ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_ROGUE_HONOR_AMONG_THIEVES_ENERGIZE, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_honor_among_thieves_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_honor_among_thieves_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_honor_among_thieves_AuraScript();
        }
};

// 154953 - Internal Bleeding
class spell_legion_rogue_internal_bleeding : public SpellScriptLoader
{
    public:
        spell_legion_rogue_internal_bleeding() : SpellScriptLoader("spell_legion_rogue_internal_bleeding") { }

        class spell_legion_rogue_internal_bleeding_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_internal_bleeding_AuraScript);

            void HandleEffectApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                const_cast<AuraEffect*>(aurEff)->SetDamage(aurEff->GetDamage() * aurEff->GetAmount());
                GetAura()->SetNeedClientUpdateForTargets();
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_legion_rogue_internal_bleeding_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_internal_bleeding_AuraScript();
        }
};

// 408 - Kidney Shot
class spell_legion_rogue_kidney_shot : public SpellScriptLoader
{
    public:
        spell_legion_rogue_kidney_shot() : SpellScriptLoader("spell_legion_rogue_kidney_shot") { }

        class spell_legion_rogue_kidney_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_kidney_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_INTERNAL_BLEEDING,
                    SPELL_ROGUE_INTERNAL_BLEEDING_PERIODIC
                });
            }

            void HandleHit()
            {
                if (GetCaster()->HasAura(SPELL_ROGUE_INTERNAL_BLEEDING))
                    GetCaster()->CastCustomSpell(SPELL_ROGUE_INTERNAL_BLEEDING_PERIODIC, SPELLVALUE_BASE_POINT0, GetSpell()->GetPowerCost(POWER_COMBO_POINTS), GetHitUnit(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_legion_rogue_kidney_shot_SpellScript::HandleHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_kidney_shot_SpellScript();
        }
};

// 51690 - Killing Spree
class spell_legion_rogue_killing_spree : public SpellScriptLoader
{
    public:
        static char constexpr const ScriptName[] = "spell_rog_killing_spree";
        spell_legion_rogue_killing_spree() : SpellScriptLoader(ScriptName) { }

        class spell_legion_rogue_killing_spree_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_killing_spree_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_BLADE_FLURRY,
                    SPELL_ROGUE_KILLING_SPREE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (GetCaster()->HasAura(SPELL_ROGUE_BLADE_FLURRY))
                {
                    if (targets.empty() || GetCaster()->GetVehicleBase())
                        FinishCast(SPELL_FAILED_OUT_OF_RANGE);
                }
                else
                    targets.clear();
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Aura* aura = GetCaster()->GetAura(SPELL_ROGUE_KILLING_SPREE))
                {
                    if (spell_legion_rogue_killing_spree_AuraScript* script = aura->GetScript<spell_legion_rogue_killing_spree_AuraScript>(ScriptName))
                        script->AddTarget(GetHitUnit());
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_rogue_killing_spree_SpellScript::FilterTargets, EFFECT_2, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_killing_spree_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_killing_spree_SpellScript::HandleDummy, EFFECT_2, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_killing_spree_SpellScript();
        }

        class spell_legion_rogue_killing_spree_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_killing_spree_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_KILLING_SPREE_DMG_BUFF,
                    SPELL_ROGUE_KILLING_SPREE_TELEPORT,
                    SPELL_ROGUE_KILLING_SPREE_WEAPON_DMG
                });
            }

            void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_KILLING_SPREE_DMG_BUFF, true);
                startPosition = GetTarget()->GetPosition();
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                while (!_targets.empty())
                {
                    ObjectGuid guid = Trinity::Containers::SelectRandomContainerElement(_targets);
                    if (Unit* target = ObjectAccessor::GetUnit(*GetTarget(), guid))
                    {
                        GetTarget()->CastSpell(target, SPELL_ROGUE_KILLING_SPREE_TELEPORT, true);
                        GetTarget()->CastSpell(target, SPELL_ROGUE_KILLING_SPREE_WEAPON_DMG, true);
                        break;
                    }
                    else
                        _targets.remove(guid);
                }
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_KILLING_SPREE_DMG_BUFF);
                GetTarget()->NearTeleportTo(startPosition);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_legion_rogue_killing_spree_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_rogue_killing_spree_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                AfterEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_killing_spree_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }

        public:
            void AddTarget(Unit* target)
            {
                _targets.push_back(target->GetGUID());
            }

        private:
            GuidList _targets;
            Position startPosition;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_killing_spree_AuraScript();
        }
};

char constexpr const spell_legion_rogue_killing_spree::ScriptName[];

// 192853 - Kingsbane
class spell_legion_rogue_kingsbane_pct_mod : public SpellScriptLoader
{
    public:
        spell_legion_rogue_kingsbane_pct_mod() : SpellScriptLoader("spell_legion_rogue_kingsbane_pct_mod") { }

        class spell_legion_rogue_kingsbane_pct_mod_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_kingsbane_pct_mod_AuraScript);

            void HandleEffectApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                for (auto itr : GetTarget()->GetTargetAuraApplications(SPELL_ROGUE_KINGSBANE))
                {
                    if (AuraEffect* kingsbaneDot = itr->GetBase()->GetEffect(EFFECT_3))
                    {
                        kingsbaneDot->SetDonePct(GetTarget()->SpellDamagePctDone(itr->GetTarget(), kingsbaneDot->GetBase()->GetSpellInfo(), DOT, kingsbaneDot->GetSpellEffectInfo()));
                        kingsbaneDot->SetDamage(GetTarget()->SpellDamageBonusDone(itr->GetTarget(), kingsbaneDot->GetBase()->GetSpellInfo(), kingsbaneDot->GetBaseAmount(), DOT, kingsbaneDot->GetSpellEffectInfo(), kingsbaneDot->GetBase()->GetStackAmount()) * kingsbaneDot->GetDonePct());
                        itr->SetNeedClientUpdate();
                    }
                }
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_legion_rogue_kingsbane_pct_mod_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_kingsbane_pct_mod_AuraScript();
        }
};

// 2818, 200803- Deadly poison, Agonizing Poison
class spell_legion_rogue_lethal_poisons : public SpellScriptLoader
{
    public:
        spell_legion_rogue_lethal_poisons() : SpellScriptLoader("spell_legion_rogue_lethal_poisons") { }

        class spell_legion_rogue_lethal_poisons_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_lethal_poisons_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_WOUND_POISON_AURA,
                    SPELL_ROGUE_DEADLY_BREW_TALENT,
                    SPELL_ROGUE_MIND_NUMBING_POISON,
                    SPELL_ROGUE_MIND_NUMBING_POISON_AURA
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* target = GetHitUnit())
                {
                    if (GetCaster()->HasAura(SPELL_ROGUE_DEADLY_BREW_TALENT))
                        GetCaster()->AddAura(SPELL_ROGUE_WOUND_POISON_AURA, target);    // No damage, just apply effect
                    if (GetCaster()->HasAura(SPELL_ROGUE_MIND_NUMBING_POISON))
                        GetCaster()->AddAura(SPELL_ROGUE_MIND_NUMBING_POISON_AURA, target); // No damage, just apply effect
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_lethal_poisons_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_lethal_poisons_SpellScript();
        }
};

// 140149 - Marked for Death Trigger Aura
class spell_legion_rogue_marked_for_death_trig : public SpellScriptLoader
{
    public:
        spell_legion_rogue_marked_for_death_trig() : SpellScriptLoader("spell_legion_rogue_marked_for_death_trig") { }

        class spell_legion_rogue_marked_for_death_trig_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_marked_for_death_trig_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_MARKED_FOR_DEATH,
                });
            }

            void HandlePeriodic(AuraEffect const* aurEff)
            {
                for (auto itr : GetTarget()->GetTargetAuraApplications(SPELL_ROGUE_MARKED_FOR_DEATH))
                {
                    if (!GetTarget()->GetSpellHistory()->HasCooldown(SPELL_ROGUE_MARKED_FOR_DEATH))
                    {
                        itr->GetBase()->Remove();
                        aurEff->GetBase()->Remove();
                    }
                    else if (itr->GetTarget()->isDead())
                    {
                        GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_ROGUE_MARKED_FOR_DEATH, true);
                        itr->GetBase()->Remove();
                        aurEff->GetBase()->Remove();
                    }
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_rogue_marked_for_death_trig_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_marked_for_death_trig_AuraScript();
        }
};

// 76806 - Mastery: Main Gauche
class spell_legion_rogue_mastery_main_gauche : public SpellScriptLoader
{
    public:
        spell_legion_rogue_mastery_main_gauche() : SpellScriptLoader("spell_legion_rogue_mastery_main_gauche") { }

        class spell_legion_rogue_mastery_main_gauche_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_mastery_main_gauche_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_0))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_MAIN_GAUCHE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo() && !eventInfo.GetSpellInfo()->HasAttribute(SPELL_ATTR3_MAIN_HAND))
                    return false;

               return roll_chance_i(GetEffect(EFFECT_0)->GetAmount());
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_ROGUE_MAIN_GAUCHE, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_mastery_main_gauche_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_mastery_main_gauche_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_mastery_main_gauche_AuraScript();
        }
};

// 197051 - Mind numbing poison proc
class spell_legion_rogue_mind_numbing_poison : public SpellScriptLoader
{
    public:
        spell_legion_rogue_mind_numbing_poison() : SpellScriptLoader("spell_legion_rogue_mind_numbing_poison") { }

        class spell_legion_rogue_mind_numbing_poison_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_mind_numbing_poison_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_MIND_NUMBING_POISON_DAMAGE
                });
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_ROGUE_MIND_NUMBING_POISON_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_mind_numbing_poison_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_mind_numbing_poison_AuraScript();
        }
};

// 195452 - Nightblade
class spell_legion_rogue_nightblade : public SpellScriptLoader
{
    public:
        spell_legion_rogue_nightblade() : SpellScriptLoader("spell_legion_rogue_nightblade") { }

        class spell_legion_rogue_nightblade_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_nightblade_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_NIGHT_TERRORS,
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetActor()->GetGUID() != GetCasterGUID())
                    return false;

                if (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->IsPositive())
                    return false;

                return true;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                eventInfo.GetActor()->CastSpell(eventInfo.GetActionTarget(), SPELL_ROGUE_NIGHT_TERRORS, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_nightblade_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_nightblade_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_nightblade_AuraScript();
        }
};

// 199743 - Parley
class spell_legion_rogue_parley : public SpellScriptLoader
{
    public:
        spell_legion_rogue_parley() : SpellScriptLoader("spell_legion_rogue_parley") { }

        class spell_legion_rogue_parley_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_parley_SpellScript);

            void HandleHit()
            {
                if (GetHitUnit()->GetTypeId() == TYPEID_PLAYER)
                {
                    if (Aura* aura = GetHitAura())
                    {
                        aura->SetMaxDuration(4 * IN_MILLISECONDS);
                        aura->SetDuration(4 * IN_MILLISECONDS);
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_legion_rogue_parley_SpellScript::HandleHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_parley_SpellScript();
        }

        class spell_legion_rogue_parley_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_parley_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_CHEAP_TRICKS_HONOR_TALENT,
                    SPELL_ROGUE_CHEAP_TRICKS_DEBUFF
                });
            }

            void EffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_ROGUE_CHEAP_TRICKS_HONOR_TALENT))
                        caster->CastSpell(GetTarget(), SPELL_ROGUE_CHEAP_TRICKS_DEBUFF, true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_legion_rogue_parley_AuraScript::EffectRemove, EFFECT_0, SPELL_AURA_MOD_PACIFY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_parley_AuraScript();
        }
};

// 185763 - Pistol Shot
class spell_legion_rogue_pistol_shot : public SpellScriptLoader
{
    public:
        spell_legion_rogue_pistol_shot() : SpellScriptLoader("spell_legion_rogue_pistol_shot") { }

        class spell_legion_rogue_pistol_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_pistol_shot_SpellScript);

            void HandleQuickDraw(SpellEffIndex /*effIndex*/)
            {
                int8 extraComboPoint = 2;
                if (GetSpell()->GetPowerCost(POWER_ENERGY) == 0)
                    SetEffectValue(extraComboPoint);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_pistol_shot_SpellScript::HandleQuickDraw, EFFECT_1, SPELL_EFFECT_ENERGIZE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_pistol_shot_SpellScript();
        }
};

// 185565 - Poisoned Knife
class spell_legion_rogue_poisoned_knife : public SpellScriptLoader
{
    public:
        spell_legion_rogue_poisoned_knife() : SpellScriptLoader("spell_legion_rogue_poisoned_knife") { }

        class spell_legion_rogue_poisoned_knife_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_poisoned_knife_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_WOUND_POISON,
                    SPELL_ROGUE_CRIPPLING_POISON,
                    SPELL_ROGUE_DEADLY_POISON,
                    SPELL_ROGUE_DEADLY_POISON_AURA,
                    SPELL_ROGUE_CRIPPLING_POISON_AURA,
                    SPELL_ROGUE_WOUND_POISON_AURA,
                    SPELL_ROGUE_AGONIZING_POISON,
                    SPELL_ROGUE_AGONIZING_POISON_AURA
                });
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                    for (std::pair<uint32, uint32> poisonSpellEntry : poisonAuras)
                        if (caster->HasAura(poisonSpellEntry.first))
                            caster->CastSpell(GetHitUnit(), poisonSpellEntry.second, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_poisoned_knife_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_poisoned_knife_SpellScript();
        }
};

// 58423 - Relentless Strikes
class spell_legion_rogue_relentless_strikes : public SpellScriptLoader
{
    public:
        spell_legion_rogue_relentless_strikes() : SpellScriptLoader("spell_legion_rogue_relentless_strikes") { }

        class spell_legion_rogue_relentless_strikes_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_relentless_strikes_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_RELENTLESS_STRIKES_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetProcSpell())
                    return false;

                return roll_chance_i(eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) * 20);
            }

            void HandleProc(ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_RELENTLESS_STRIKES_ENERGIZE, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_relentless_strikes_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_relentless_strikes_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_relentless_strikes_AuraScript();
        }
};

// 199754 - Riposte
class spell_legion_rogue_riposte : public SpellScriptLoader
{
    public:
        spell_legion_rogue_riposte() : SpellScriptLoader("spell_legion_rogue_riposte") { }

        class spell_legion_rogue_riposte_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_riposte_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_BLADEMASTER_INITIAL_PROC_AURA
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_ROGUE_BLADEMASTER_INITIAL_PROC_AURA))
                    GetCaster()->CastSpell(GetCaster(), SPELL_ROGUE_BLADEMASTER_PROC_OF_PROC_AURA, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_rogue_riposte_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_riposte_SpellScript();
        }
};

// 193316 - Roll the bones
class spell_legion_rogue_roll_the_bones : public SpellScriptLoader
{
    public:
        spell_legion_rogue_roll_the_bones() : SpellScriptLoader("spell_legion_rogue_roll_the_bones") { }

        class spell_legion_rogue_roll_the_bones_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_roll_the_bones_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_JOLLY_JOKER,
                    SPELL_ROGUE_GRAND_MELEE,
                    SPELL_ROGUE_SHARK_INFESTED_WATERS,
                    SPELL_ROGUE_TRUE_BEARING,
                    SPELL_ROGUE_BURIED_TREASURE,
                    SPELL_ROGUE_BROADSIDES
                });
            }

            void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                std::list<uint32> bonesBuffs =
                {
                    SPELL_ROGUE_JOLLY_JOKER,
                    SPELL_ROGUE_GRAND_MELEE,
                    SPELL_ROGUE_SHARK_INFESTED_WATERS,
                    SPELL_ROGUE_TRUE_BEARING,
                    SPELL_ROGUE_BURIED_TREASURE,
                    SPELL_ROGUE_BROADSIDES
                };

                // Remove old buffs
                for (uint32 spellId : bonesBuffs)
                    GetTarget()->RemoveAura(spellId);

                int8 count[6] = { 0, 0, 0, 0, 0, 0 };
                for (int8 i = 0; i < 6; ++i)
                    count[irand(0, 5)]++;

                int8 max = 0;
                int8 result = 0;
                for (int8 i = 0; i < 6; ++i)
                    if (count[i] > max)
                        max = count[i];
                for (int8 i = 0; i < 6; ++i)
                    if (count[i] == max)
                        ++result;

                Trinity::Containers::RandomResize(bonesBuffs, result);

                for (uint32 spellId : bonesBuffs)
                    GetTarget()->CastCustomSpell(spellId, SPELLVALUE_AURA_DURATION, (GetSpellInfo()->GetDuration() + (GetSpellInfo()->GetDuration() * GetAura()->GetPowerCost(POWER_COMBO_POINTS))), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_legion_rogue_roll_the_bones_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_roll_the_bones_AuraScript();
        }
};

// 2098 - Run Through
class spell_legion_rogue_run_through : public SpellScriptLoader
{
    public:
        spell_legion_rogue_run_through() : SpellScriptLoader("spell_legion_rogue_run_through") { }

        class spell_legion_rogue_run_through_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_run_through_SpellScript);

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(GetHitDamage() * GetSpell()->GetPowerCost(POWER_COMBO_POINTS));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_run_through_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_run_through_SpellScript();
        }
};

// 1943 - Rupture
class spell_legion_rogue_rupture : public SpellScriptLoader
{
    public:
        spell_legion_rogue_rupture() : SpellScriptLoader("spell_legion_rogue_rupture") { }

        class spell_legion_rogue_rupture_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_rupture_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_VENOMOUS_VIM_ENERGIZE
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(SPELL_ROGUE_VENOMOUS_VIM_ENERGIZE, SPELLVALUE_BASE_POINT0, ceil(GetAura()->GetDuration() / 1000), caster, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_rupture_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_rupture_AuraScript();
        }
};

// 14161 - Ruthlessness
class spell_legion_rogue_ruthlessness : public SpellScriptLoader
{
    public:
        spell_legion_rogue_ruthlessness() : SpellScriptLoader("spell_legion_rogue_ruthlessness") { }

        class spell_legion_rogue_ruthlessness_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_ruthlessness_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_GEN_COMBO_POINT
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) > 0;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                int32 chance = 20 * eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS);
                if (roll_chance_i(chance))
                    GetUnitOwner()->CastSpell(GetUnitOwner(), SPELL_GEN_COMBO_POINT, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_ruthlessness_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_ruthlessness_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_ruthlessness_AuraScript();
        }
};

// 193315 - Saber Slash
class spell_legion_rogue_saber_slash : public SpellScriptLoader
{
    public:
        spell_legion_rogue_saber_slash() : SpellScriptLoader("spell_legion_rogue_saber_slash") { }

        class spell_legion_rogue_saber_slash_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_saber_slash_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SABER_SLASH
                });
            }

            void HandleAfterHit()
            {
                if (!_targetGUID.IsEmpty())
                    if (Aura* saberAura = GetCaster()->GetAura(SPELL_ROGUE_SABER_SLASH))
                        saberAura->SetCastExtraParam("targetGUID", _targetGUID);
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                _targetGUID = GetHitUnit()->GetGUID();
            }

        private:
            ObjectGuid _targetGUID;

            void Register() override
            {
                AfterHit += SpellHitFn(spell_legion_rogue_saber_slash_SpellScript::HandleAfterHit);
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_saber_slash_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        class spell_legion_rogue_saber_slash_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_saber_slash_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_JOLLY_JOKER,
                    SPELL_ROGUE_SABER_SLASH_MULTI,
                    SPELL_ROGUE_OPPORTUNITY,
                    SPELL_ROGUE_BLUNDERBUSS,
                    SPELL_ROGUE_BLUNDERBUSS_OVERRIDE
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (SpellEffectInfo const* effInfo = GetSpellInfo()->GetEffect(EFFECT_4))
                    {
                        int32 chance = effInfo->CalcValue(GetTarget());

                        if (Aura* jollyJokerAura = caster->GetAura(SPELL_ROGUE_JOLLY_JOKER))
                            if (AuraEffect* jollyEffInfo = jollyJokerAura->GetEffect(EFFECT_0))
                                chance += jollyEffInfo->GetAmount();

                        if (roll_chance_i(chance) || caster->HasAura(SPELL_ROGUE_HIDDEN_BLADE))
                        {
                            caster->RemoveAurasDueToSpell(SPELL_ROGUE_HIDDEN_BLADE);
                            if (ObjectGuid const* targetGUID = GetAura()->GetCastExtraParam<ObjectGuid>("targetGUID"))
                                if (Unit* procTarget = ObjectAccessor::GetUnit(*GetCaster(), *targetGUID))
                                    if (procTarget->IsAlive())
                                    {
                                        caster->CastSpell(procTarget, SPELL_ROGUE_SABER_SLASH_MULTI, true);
                                        caster->CastSpell(caster, SPELL_ROGUE_OPPORTUNITY, true);
                                    }


                            if (AuraEffect* blunderbussChance = caster->GetAuraEffect(SPELL_ROGUE_BLUNDERBUSS, EFFECT_1))
                                if (roll_chance_i(blunderbussChance->GetAmount()))
                                    caster->CastSpell(caster, SPELL_ROGUE_BLUNDERBUSS_OVERRIDE, true);
                        }
                    }
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_saber_slash_AuraScript::HandleRemove, EFFECT_3, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_saber_slash_AuraScript();
        }

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_saber_slash_SpellScript();
        }
};

// 14190 - Seal Fate
class spell_legion_rogue_seal_fate : public SpellScriptLoader
{
    public:
        spell_legion_rogue_seal_fate() : SpellScriptLoader("spell_legion_rogue_seal_fate") { }

        class spell_legion_rogue_seal_fate_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_seal_fate_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_MUTILATE,
                    SPELL_ROGUE_MUTILATE_OFFHAND
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                if (eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_MUTILATE || eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_MUTILATE_OFFHAND || eventInfo.GetSpellInfo()->HasEffect(SPELL_EFFECT_ENERGIZE))
                    return true;
                return false;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_seal_fate_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_seal_fate_AuraScript();
        }
};

// 185422 - Shadow Dance
class spell_legion_rogue_shadow_dance : public SpellScriptLoader
{
    public:
        spell_legion_rogue_shadow_dance() : SpellScriptLoader("spell_legion_rogue_shadow_dance") { }

        class spell_legion_rogue_shadow_dance_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_shadow_dance_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_STEALTH_EFFECT,
                    SPELL_ROGUE_STEALTH_EFFECT_2,
                    SPELL_ROGUE_NIGHTSTALKER_TALENT,
                    SPELL_ROGUE_NIGHTSTALKER_EFFECT,
                    SPELL_ROGUE_SHADOW_FOCUS_TALENT,
                    SPELL_ROGUE_SHADOW_FOCUS_EFFECT,
                    SPELL_ROGUE_MASTER_OF_SHADOWS_TALENT,
                    SPELL_ROGUE_MASTER_OF_SHADOWS_EFFECT,
                    SPELL_ROGUE_MASTER_OF_SUBTLETY_TALENT,
                    SPELL_ROGUE_MASTER_OF_SUBTLETY_EFFECT,
                    SPELL_ROGUE_SUBTERFUGE_TALENT,
                    SPELL_ROGUE_SUBTERFUGE_EFFECT,
                    SPELL_ROGUE_SHADOW_S_CAREERS,
                    SPELL_ROGUE_SHADOW_S_CAREERS_BUFF,
                    SPELL_ROGUE_EMBRACE_OF_DARKNESS_PROC_AURA,
                    SPELL_ROGUE_EMBRACE_OF_DARKNESS_SHIELD
                });
            }

            void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_STEALTH_EFFECT_2, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_NIGHTSTALKER_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_NIGHTSTALKER_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_SHADOW_FOCUS_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SHADOW_FOCUS_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_MASTER_OF_SHADOWS_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_MASTER_OF_SHADOWS_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_MASTER_OF_SUBTLETY_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_MASTER_OF_SUBTLETY_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_SHADOW_S_CAREERS))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SHADOW_S_CAREERS_BUFF, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_PHANTOM_ASSASSIN))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SYMBOLS_OF_DEATH, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_THIEF_S_BARGAIN))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_THIEF_S_BARGAIN_BUFF, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_EMBRACE_OF_DARKNESS_PROC_AURA))
                    GetTarget()->CastCustomSpell(SPELL_ROGUE_EMBRACE_OF_DARKNESS_SHIELD, SPELLVALUE_BASE_POINT0, int32(GetCaster()->GetTotalAttackPowerValue(BASE_ATTACK)), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_NIGHTSTALKER_EFFECT);
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_SHADOW_FOCUS_EFFECT);
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_SHADOW_S_CAREERS_BUFF);
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_THIEF_S_BARGAIN_BUFF);
                if (Aura* masterSubAura = GetTarget()->GetAura(SPELL_ROGUE_MASTER_OF_SUBTLETY_EFFECT))
                {
                    masterSubAura->SetMaxDuration(6 * IN_MILLISECONDS);
                    masterSubAura->SetDuration(6 * IN_MILLISECONDS);
                }
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_STEALTH_EFFECT_2);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectRemoveFn(spell_legion_rogue_shadow_dance_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_shadow_dance_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_shadow_dance_AuraScript();
        }
};

// 209781 - Shadow Nova
class spell_legion_rogue_shadow_nova : public SpellScriptLoader
{
    public:
        spell_legion_rogue_shadow_nova() : SpellScriptLoader("spell_legion_rogue_shadow_nova") { }

        class spell_legion_rogue_shadow_nova_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_shadow_nova_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SHADOW_DANCE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                if (eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_SHURIKEN_STORM && !GetTarget()->HasAura(SPELL_ROGUE_SHADOW_DANCE))
                    return false;

                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_shadow_nova_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_shadow_nova_AuraScript();
        }
};

// 196912 - Shadow Techniques
class spell_legion_rogue_shadow_techniques : public SpellScriptLoader
{
    public:
        spell_legion_rogue_shadow_techniques() : SpellScriptLoader("spell_legion_rogue_shadow_techniques") { }

        class spell_legion_rogue_shadow_techniques_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_shadow_techniques_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_FORTUNE_S_BITE,
                    SPELL_ROGUE_SHADOW_TECHNIQUES_ENERGIZE,
                    SPELL_ROGUE_SHADOW_BLADE,
                    SPELL_ROGUE_SHADOW_BLADE_OFF_HAND
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!(eventInfo.GetTypeMask() & (PROC_FLAG_DONE_MAINHAND_ATTACK | PROC_FLAG_DONE_OFFHAND_ATTACK)) ||
                    (eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id != SPELL_ROGUE_SHADOW_BLADE && eventInfo.GetSpellInfo()->Id != SPELL_ROGUE_SHADOW_BLADE_OFF_HAND))
                    return false;

                if (AuraEffect* procStore = GetEffect(EFFECT_0))
                {
                    if (procStore->GetAmount() == 0)
                    {
                        procStore->SetAmount(urand(4, 6));
                        return true;
                    }
                    else
                        procStore->SetAmount(procStore->GetAmount() - 1);
                }
                return false;
            }

            void HandleProc(ProcEventInfo& /*eventInfo*/)
            {
                if (AuraEffect* fortunesBite = GetTarget()->GetAuraEffect(SPELL_ROGUE_FORTUNE_S_BITE, EFFECT_0))
                {
                    if (roll_chance_i(fortunesBite->GetAmount()))
                        GetTarget()->CastCustomSpell(SPELL_ROGUE_SHADOW_TECHNIQUES_ENERGIZE, SPELLVALUE_BASE_POINT0, 2, GetTarget(), TRIGGERED_FULL_MASK);
                    else
                        GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SHADOW_TECHNIQUES_ENERGIZE, true);
                }
                else
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SHADOW_TECHNIQUES_ENERGIZE, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_shadow_techniques_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_shadow_techniques_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_shadow_techniques_AuraScript();
        }
};

class DelayedCdReduceEvent : public BasicEvent
{
    public:
        DelayedCdReduceEvent(Unit* caster, int32 cdReduce) : _caster(caster), _cdReduce(cdReduce) { }

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            _caster->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_ROGUE_SHADOWSTEP)->ChargeCategoryId, _cdReduce);
            return true;
        }

    private:
        Unit* _caster;
        int32 _cdReduce;
};

// 36554 - Shadowstep
class spell_legion_rogue_shadowstep : public SpellScriptLoader
{
    public:
        spell_legion_rogue_shadowstep() : SpellScriptLoader("spell_legion_rogue_shadowstep") { }

        class spell_legion_rogue_shadowstep_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_shadowstep_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SHADOW_SWIFTNESS,
                    SPELL_ROGUE_EVASION,
                    SPELL_ROGUE_INTENT_TO_KILL,
                    SPELL_ROGUE_VENDETTA,
                    SPELL_ROGUE_SILHOUETTE
                });
            }

            SpellCastResult DoCastCheck()
            {
                // Shadowstep cannot target self
                if (Unit* target = GetExplTargetUnit())
                    if (target->GetGUID() == GetCaster()->GetGUID())
                        return SPELL_FAILED_BAD_TARGETS;

                if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
                    return SPELL_FAILED_ROOTED;

                return SPELL_CAST_OK;
            }

            void DoEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_ROGUE_SHADOW_SWIFTNESS, EFFECT_0))
                {
                    if (Aura* evasionOld = GetCaster()->GetAura(SPELL_ROGUE_EVASION))
                    {
                        if (evasionOld->GetDuration() < aurEff->GetAmount() * IN_MILLISECONDS)
                            evasionOld->SetDuration(aurEff->GetAmount() * IN_MILLISECONDS);
                    }
                    else
                    {
                        if (Aura* evasion = GetCaster()->AddAura(SPELL_ROGUE_EVASION, GetCaster()))
                            evasion->SetDuration(aurEff->GetAmount() * IN_MILLISECONDS);
                    }
                }
                if (Unit* target = GetHitUnit())
                {
                    if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_ROGUE_INTENT_TO_KILL, EFFECT_0))
                    {
                        if (target->HasAura(SPELL_ROGUE_VENDETTA, GetCaster()->GetGUID()))
                        {
                            int32 cdReduce = CalculatePct(GetCaster()->GetSpellHistory()->GetChargeRecoveryTime(GetSpellInfo()->ChargeCategoryId), aurEff->GetAmount());
                            GetCaster()->m_Events.AddEvent(new DelayedCdReduceEvent(GetCaster(), cdReduce), GetCaster()->m_Events.CalculateTime(100));
                        }
                    }
                    if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_ROGUE_SILHOUETTE, EFFECT_0))
                    {
                        if (GetCaster()->IsFriendlyTo(target))
                        {
                            int32 cdReduce = CalculatePct(GetCaster()->GetSpellHistory()->GetChargeRecoveryTime(GetSpellInfo()->ChargeCategoryId), aurEff->GetAmount());
                            GetCaster()->m_Events.AddEvent(new DelayedCdReduceEvent(GetCaster(), cdReduce), GetCaster()->m_Events.CalculateTime(100));
                        }
                    }
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_legion_rogue_shadowstep_SpellScript::DoCastCheck);
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_shadowstep_SpellScript::DoEffectHit, EFFECT_0, SPELL_EFFECT_TRIGGER_SPELL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_shadowstep_SpellScript();
        }
};

// 185438 - Shadowstrike
class spell_legion_rogue_shadowstrike : public SpellScriptLoader
{
    public:
        spell_legion_rogue_shadowstrike() : SpellScriptLoader("spell_legion_rogue_shadowstrike") { }

        class spell_legion_rogue_shadowstrike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_shadowstrike_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SHADOWSTRIKE_RANGE_BUFF,
                    SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_TALENT,
                    SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_SNARE,
                    SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_STUN
                });
            }

            SpellCastResult CheckCast()
            {
                if (Unit* explTarget = GetExplTargetUnit())
                    if (GetCaster()->HasUnitState(UNIT_STATE_ROOT)
                        && !GetCaster()->IsWithinBoundaryRadius(explTarget)
                        && GetCaster()->HasAura(SPELL_ROGUE_SHADOWSTRIKE_RANGE_BUFF))
                        return SPELL_FAILED_ROOTED;
                return SPELL_CAST_OK;
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_ROGUE_SHADOWSTRIKE_RANGE_BUFF))
                {
                    if (!GetCaster()->IsWithinBoundaryRadius(GetHitUnit()))
                    {
                        // TODO: This should teleport you behind the target, not on top of it, couldnt find spell for it in sniff yet
                        Position teleport(*GetHitUnit());
                        GetCaster()->NearTeleportTo(teleport.GetPositionX(), teleport.GetPositionY(), teleport.GetPositionZ(), teleport.GetOrientation());
                    }
                }

                if (GetCaster()->HasAura(SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_TALENT))
                {
                    if (GetHitUnit()->GetTypeId() == TYPEID_PLAYER)
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_SNARE, true);
                    else
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_ROGUE_STRIKE_FROM_THE_SHADOWS_STUN, true);
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_legion_rogue_shadowstrike_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_shadowstrike_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_shadowstrike_SpellScript();
        }
};

// 207736 - Shadowy Duel
class spell_legion_rogue_shadowy_duel : public SpellScriptLoader
{
    public:
        spell_legion_rogue_shadowy_duel() : SpellScriptLoader("spell_legion_rogue_shadowy_duel") { }

        class spell_legion_rogue_shadowy_duel_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_shadowy_duel_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SHADOWY_DUEL_INVIS,
                    SPELL_ROGUE_SHADOWY_DUEL_STALK
                });
            }

            void OnTriggerTarget(SpellEffIndex effIndex)
            {
                PreventHitEffect(effIndex);
            }

            void OnHitTarget(SpellEffIndex /*effIndex*/)
            {
                // Weird Spell Blizz, you trigger on urself with ENEMY target
                GetCaster()->CastSpell(GetHitUnit(), SPELL_ROGUE_SHADOWY_DUEL_STALK, true);
                GetHitUnit()->CastSpell(GetCaster(), SPELL_ROGUE_SHADOWY_DUEL_STALK, true);
                GetCaster()->CastSpell(GetCaster(), SPELL_ROGUE_SHADOWY_DUEL_INVIS, true);
                GetCaster()->AddAura(SPELL_ROGUE_SHADOWY_DUEL_INVIS, GetHitUnit());
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_shadowy_duel_SpellScript::OnTriggerTarget, EFFECT_2, SPELL_EFFECT_TRIGGER_SPELL);
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_shadowy_duel_SpellScript::OnHitTarget, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_shadowy_duel_SpellScript();
        }
};

// 197835 - Shuriken Storm
class spell_legion_rogue_shuriken_storm : public SpellScriptLoader
{
    public:
        spell_legion_rogue_shuriken_storm() : SpellScriptLoader("spell_legion_rogue_shuriken_storm") { }

        class spell_legion_rogue_shuriken_storm_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_shuriken_storm_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SHADOW_DANCE,
                    SPELL_ROGUE_SHURIKEN_STORM_ENERGIZE
                });
            }

            bool Load() override
            {
                _stealthActive = GetCaster()->HasStealthAura();
                return true;
            }

            void CountTargets(std::list<WorldObject*>& targets)
            {
                _energizeAmount = int32(targets.size());
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (_stealthActive || GetCaster()->HasAura(SPELL_ROGUE_SHADOW_DANCE))
                {
                    int32 damage = GetHitDamage();
                    if (SpellEffectInfo const* pctInfo = GetEffectInfo(EFFECT_2))
                        AddPct(damage, pctInfo->CalcValue(GetCaster()));
                    SetHitDamage(damage);
                }
            }

            void Energize()
            {
                _energizeAmount = std::min(_energizeAmount, GetCaster()->GetMaxPower(POWER_COMBO_POINTS));
                GetCaster()->CastCustomSpell(SPELL_ROGUE_SHURIKEN_STORM_ENERGIZE, SPELLVALUE_BASE_POINT0, _energizeAmount, GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_legion_rogue_shuriken_storm_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_rogue_shuriken_storm_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                AfterCast += SpellCastFn(spell_legion_rogue_shuriken_storm_SpellScript::Energize);
            }

        private:
            bool _stealthActive = false;
            int32 _energizeAmount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_shuriken_storm_SpellScript();
        }
};

// 2983 - Sprint
class spell_legion_rogue_sprint : public SpellScriptLoader
{
    public:
        spell_legion_rogue_sprint() : SpellScriptLoader("spell_legion_rogue_sprint") { }

        class spell_legion_rogue_sprint_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_sprint_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_MANEUVERABILITY,
                    SPELL_ROGUE_MANEUVERABILITY_BUFF,
                    SPELL_ROGUE_FEINT,
                    SPELL_ROGUE_FLICKERING_SHADOWS_PROC_AURA,
                    SPELL_ROGUE_FASTER_THAN_THE_LIGHT_TRIGGER
                });
            }

            void DoAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_ROGUE_MANEUVERABILITY))
                    GetCaster()->CastSpell(GetCaster(), SPELL_ROGUE_MANEUVERABILITY_BUFF, true);

                if (GetCaster()->HasAura(SPELL_ROGUE_DECEPTION_PROC_AURA))
                    GetCaster()->CastSpell(GetCaster(), SPELL_ROGUE_FEINT, true);

                if (GetCaster()->HasAura(SPELL_ROGUE_FLICKERING_SHADOWS_PROC_AURA))
                    GetCaster()->CastSpell(GetCaster(), SPELL_ROGUE_FASTER_THAN_THE_LIGHT_TRIGGER, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_rogue_sprint_SpellScript::DoAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_sprint_SpellScript();
        }
};

class DelayedStealthRemoveEvent : public BasicEvent
{
public:
    DelayedStealthRemoveEvent(Unit* caster) : _caster(caster) { }

    bool Execute(uint64 /*time*/, uint32 /*diff*/) override
    {
        _caster->RemoveAurasDueToSpell(SPELL_ROGUE_NIGHTSTALKER_EFFECT);
        _caster->RemoveAurasDueToSpell(SPELL_ROGUE_THIEF_S_BARGAIN_BUFF);
        return true;
    }

private:
    Unit* _caster;
};

// 1784 - Stealth Base
class spell_legion_rogue_stealth : public SpellScriptLoader
{
    public:
        spell_legion_rogue_stealth() : SpellScriptLoader("spell_legion_rogue_stealth") { }

        class spell_legion_rogue_stealth_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_stealth_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({
                    SPELL_ROGUE_STEALTH_EFFECT,
                    SPELL_ROGUE_STEALTH_EFFECT_2,
                    SPELL_ROGUE_NIGHTSTALKER_TALENT,
                    SPELL_ROGUE_NIGHTSTALKER_EFFECT,
                    SPELL_ROGUE_SHADOW_FOCUS_TALENT,
                    SPELL_ROGUE_SHADOW_FOCUS_EFFECT,
                    SPELL_ROGUE_MASTER_OF_SHADOWS_TALENT,
                    SPELL_ROGUE_MASTER_OF_SHADOWS_EFFECT,
                    SPELL_ROGUE_MASTER_OF_SUBTLETY_TALENT,
                    SPELL_ROGUE_MASTER_OF_SUBTLETY_EFFECT,
                    SPELL_ROGUE_SUBTERFUGE_TALENT,
                    SPELL_ROGUE_SUBTERFUGE_EFFECT,
                    SPELL_ROGUE_SHADOW_S_CAREERS,
                    SPELL_ROGUE_SHADOW_S_CAREERS_BUFF,
                    SPELL_ROGUE_THIEF_S_BARGAIN,
                    SPELL_ROGUE_THIEF_S_BARGAIN_BUFF,
                    SPELL_ROGUE_EMBRACE_OF_DARKNESS_PROC_AURA,
                    SPELL_ROGUE_EMBRACE_OF_DARKNESS_PROC_AURA
                });
            }

            void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                // TODO: Changing Talents should remove Stealth (and probably other auras)

                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_STEALTH_EFFECT, true);
                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_STEALTH_EFFECT_2, true);

                if (GetTarget()->HasAura(SPELL_ROGUE_NIGHTSTALKER_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_NIGHTSTALKER_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_SHADOW_FOCUS_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SHADOW_FOCUS_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_MASTER_OF_SHADOWS_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_MASTER_OF_SHADOWS_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_MASTER_OF_SUBTLETY_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_MASTER_OF_SUBTLETY_EFFECT, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_SHADOW_S_CAREERS))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SHADOW_S_CAREERS_BUFF, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_PHANTOM_ASSASSIN))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SYMBOLS_OF_DEATH, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_THIEF_S_BARGAIN))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_THIEF_S_BARGAIN_BUFF, true);
                if (GetTarget()->HasAura(SPELL_ROGUE_EMBRACE_OF_DARKNESS_PROC_AURA))
                    GetTarget()->CastCustomSpell(SPELL_ROGUE_EMBRACE_OF_DARKNESS_SHIELD, SPELLVALUE_BASE_POINT0, int32(GetCaster()->GetTotalAttackPowerValue(BASE_ATTACK)), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_STEALTH_EFFECT);
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_SHADOW_FOCUS_EFFECT);
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_SHADOW_S_CAREERS_BUFF);
                if (GetTarget()->HasAura(SPELL_ROGUE_VEIL_OF_MIDNIGHT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_VEIL_OF_MIDNIGHT_BUFF, true);
                if (Aura* masterSubAura = GetTarget()->GetAura(SPELL_ROGUE_MASTER_OF_SUBTLETY_EFFECT))
                {
                    masterSubAura->SetMaxDuration(6 * IN_MILLISECONDS);
                    masterSubAura->SetDuration(6 * IN_MILLISECONDS);
                }
                if (GetTarget()->HasAura(SPELL_ROGUE_SUBTERFUGE_TALENT))
                {
                    GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_SUBTERFUGE_EFFECT, true);
                    return; // Stealth Effect 2 is removed when Subterfuge Effect ends
                }
                GetTarget()->m_Events.AddEvent(new DelayedStealthRemoveEvent(GetTarget()), GetTarget()->m_Events.CalculateTime(100));
                GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_STEALTH_EFFECT_2);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_legion_rogue_stealth_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_stealth_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_stealth_AuraScript();
        }
};

// 115192 - Subterfuge
class spell_legion_rogue_subterfuge : public SpellScriptLoader
{
    public:
        spell_legion_rogue_subterfuge() : SpellScriptLoader("spell_legion_rogue_subterfuge") { }

        class spell_legion_rogue_subterfuge_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_subterfuge_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_STEALTH,
                    SPELL_ROGUE_ASSASINATION_STEALTH,
                    SPELL_ROGUE_STEALTH_EFFECT_2
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (!GetTarget()->HasAura(SPELL_ROGUE_STEALTH) && !GetTarget()->HasAura(SPELL_ROGUE_ASSASINATION_STEALTH))
                    GetTarget()->RemoveAurasDueToSpell(SPELL_ROGUE_STEALTH_EFFECT_2);

                GetTarget()->m_Events.AddEvent(new DelayedStealthRemoveEvent(GetTarget()), GetTarget()->m_Events.CalculateTime(100));
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_subterfuge_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_subterfuge_AuraScript();
        }
};

// 192424 - Surge of Toxin Talent
class spell_legion_rogue_surge_of_toxin : public SpellScriptLoader
{
    public:
        spell_legion_rogue_surge_of_toxin() : SpellScriptLoader("spell_legion_rogue_surge_of_toxin") { }

        class spell_legion_rogue_surge_of_toxin_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_surge_of_toxin_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SURGE_OF_TOXIN,
                    SPELL_ROGUE_SURGE_OF_TOXIN_DEBUFF
                });
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_ROGUE_SURGE_OF_TOXIN_DEBUFF, true);
            }

            void Register() override
            {
                OnProc += AuraProcFn(spell_legion_rogue_surge_of_toxin_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_surge_of_toxin_AuraScript();
        }
};

// 5171 - Slice and Dice
// 193316 - Roll the Bones
class spell_legion_rogue_take_your_cut : public SpellScriptLoader
{
    public:
        spell_legion_rogue_take_your_cut() : SpellScriptLoader("spell_legion_rogue_take_your_cut") { }

        class spell_legion_rogue_take_your_cut_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_take_your_cut_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_TAKE_YOUR_CUT_PROC_AURA,
                    SPELL_ROGUE_TAKE_YOUR_CUT_BONUS
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_ROGUE_TAKE_YOUR_CUT_PROC_AURA))
                    GetCaster()->CastSpell(GetCaster(), SPELL_ROGUE_TAKE_YOUR_CUT_BONUS, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_legion_rogue_take_your_cut_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_take_your_cut_SpellScript();
        }
};

// 198368 - Take Your Cut
class spell_legion_rogue_take_your_cut_bonus : public SpellScriptLoader
{
    public:
        spell_legion_rogue_take_your_cut_bonus() : SpellScriptLoader("spell_legion_rogue_take_your_cut_bonus") { }

        class spell_legion_rogue_take_your_cut_bonus_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_take_your_cut_bonus_SpellScript);

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetCaster());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_legion_rogue_take_your_cut_bonus_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_take_your_cut_bonus_SpellScript();
        }
};

// 193359 - True Bearing
class spell_legion_rogue_true_bearing : public SpellScriptLoader
{
    public:
        spell_legion_rogue_true_bearing() : SpellScriptLoader("spell_legion_rogue_true_bearing") { }

        class spell_legion_rogue_true_bearing_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_true_bearing_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_ADRENALINE_RUSH,
                    SPELL_ROGUE_BETWEEN_THE_EYES,
                    SPELL_ROGUE_SPRINT,
                    SPELL_ROGUE_GRAPPLING_HOOK,
                    SPELL_ROGUE_CANNONBALL_BARRAGE,
                    SPELL_ROGUE_KILLING_SPREE,
                    SPELL_ROGUE_MARKED_FOR_DEATH,
                    SPELL_ROGUE_DEATH_FROM_ABOVE,
                    SPELL_ROGUE_VANISH
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetProcSpell())
                    return false;

                return eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) > 0;
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                int32 cdReduce = -(eventInfo.GetProcSpell()->GetPowerCost(POWER_COMBO_POINTS) * 1000);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_ADRENALINE_RUSH, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_BETWEEN_THE_EYES, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_SPRINT, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_GRAPPLING_HOOK, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_CANNONBALL_BARRAGE, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_KILLING_SPREE, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_MARKED_FOR_DEATH, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_DEATH_FROM_ABOVE, cdReduce);
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_ROGUE_VANISH, cdReduce);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_true_bearing_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_true_bearing_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_true_bearing_AuraScript();
        }
};

// 57934 - Tricks of the Trade
class spell_legion_rogue_tricks_of_the_trade : public SpellScriptLoader
{
    public:
        spell_legion_rogue_tricks_of_the_trade() : SpellScriptLoader("spell_legion_rogue_tricks_of_the_trade") { }

        class spell_legion_rogue_tricks_of_the_trade_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_tricks_of_the_trade_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_TRICKS_OF_THE_TRADE_DPS_BOOST,
                    SPELL_ROGUE_TRICKS_OF_THE_TRADE_DPS_BOOST_2,
                    SPELL_ROGUE_THICK_AS_THIEVES
                });
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->ResetRedirectThreat();
                for (auto itr : GetTarget()->GetTargetAuraApplications(GetId()))
                    itr->GetBase()->Remove();
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                if (triggerActive)
                    return;

                triggerActive = true;

                for (auto itr : GetTarget()->GetTargetAuraApplications(GetId()))
                {
                    if (itr->GetBase()->GetDuration() >= 6000)
                        itr->GetBase()->SetDuration(6000);

                    if (AuraEffect* thickAsThieves = GetTarget()->GetAuraEffect(SPELL_ROGUE_THICK_AS_THIEVES, EFFECT_0))
                        GetTarget()->CastCustomSpell(itr->GetTarget()->GetGUID() == GetCasterGUID() ? SPELL_ROGUE_TRICKS_OF_THE_TRADE_DPS_BOOST : SPELL_ROGUE_TRICKS_OF_THE_TRADE_DPS_BOOST_2, SPELLVALUE_BASE_POINT1, thickAsThieves->GetAmount(), itr->GetTarget(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_tricks_of_the_trade_AuraScript::OnRemove, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_tricks_of_the_trade_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_DUMMY);
            }

            private:
                bool triggerActive = false;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_tricks_of_the_trade_AuraScript();
        }
};


// 198020 - Turn the Tables
class spell_legion_rogue_turn_the_tables : public SpellScriptLoader
{
    public:
        spell_legion_rogue_turn_the_tables() : SpellScriptLoader("spell_legion_rogue_turn_the_tables") { }

        class spell_legion_rogue_turn_the_tables_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_turn_the_tables_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                return eventInfo.GetSpellInfo()->HasAura(DIFFICULTY_NONE, SPELL_AURA_MOD_STUN);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_turn_the_tables_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_turn_the_tables_AuraScript();
        }
};

// 198023 - Turn the Tables
class spell_legion_rogue_turn_the_tables_periodic : public SpellScriptLoader
{
    public:
        spell_legion_rogue_turn_the_tables_periodic() : SpellScriptLoader("spell_legion_rogue_turn_the_tables_periodic") { }

        class spell_legion_rogue_turn_the_tables_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_turn_the_tables_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_TURN_THE_TABLES_BONUS
                });
            }

            void HandlePeriodic(AuraEffect const* /*aurEff*/)
            {
                if (!GetTarget()->HasAuraType(SPELL_AURA_MOD_STUN))
                    Remove();
            }

            void EffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    if (!GetTarget()->HasAuraType(SPELL_AURA_MOD_STUN))
                        GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_TURN_THE_TABLES_BONUS, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_legion_rogue_turn_the_tables_periodic_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectRemove += AuraEffectApplyFn(spell_legion_rogue_turn_the_tables_periodic_AuraScript::EffectRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_turn_the_tables_periodic_AuraScript();
        }
};

// 206317 - Unfair Advantage
class spell_legion_rogue_unfair_advantage : public SpellScriptLoader
{
    public:
        spell_legion_rogue_unfair_advantage() : SpellScriptLoader("spell_legion_rogue_unfair_advantage") { }

        class spell_legion_rogue_unfair_advantage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_unfair_advantage_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_UNFAIR_ADVANTAGE_BUFF
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())  // Procs only from Abilites
                    return false;

                return GetTarget()->GetHealthPct() > eventInfo.GetActionTarget()->GetHealthPct();
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_UNFAIR_ADVANTAGE_BUFF, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_unfair_advantage_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_unfair_advantage_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_unfair_advantage_AuraScript();
        }
};

class DelayedVanishEvent : public BasicEvent
{
public:
    DelayedVanishEvent(Unit* caster) : _caster(caster) { }

    bool Execute(uint64 /*time*/, uint32 /*diff*/) override
    {
        _caster->CastSpell(_caster, SPELL_ROGUE_VANISH_AURA, TRIGGERED_FULL_MASK);
        _caster->CastSpell(_caster, SPELL_ROGUE_STEALTH_EFFECT_2, TRIGGERED_FULL_MASK);
        return true;
    }

    private:
        Unit* _caster;
};

// 1856 - Vanish
class spell_legion_rogue_vanish : public SpellScriptLoader
{
    public:
        spell_legion_rogue_vanish() : SpellScriptLoader("spell_legion_rogue_vanish") { }

        class spell_legion_rogue_vanish_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_legion_rogue_vanish_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_VANISH_AURA,
                    SPELL_ROGUE_STEALTH_EFFECT_2
                });
            }

            void OnLaunchTarget(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);

                Unit* target = GetHitUnit();

                target->RemoveMovementImpairingAuras();
                target->RemoveAurasByType(SPELL_AURA_MOD_STALKED);
                if (target->GetTypeId() != TYPEID_PLAYER)
                    return;

                if (target->HasAura(SPELL_ROGUE_VANISH_AURA))
                    return;

                target->m_Events.AddEvent(new DelayedVanishEvent(target), target->m_Events.CalculateTime(200));
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_legion_rogue_vanish_SpellScript::OnLaunchTarget, EFFECT_1, SPELL_EFFECT_TRIGGER_SPELL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_legion_rogue_vanish_SpellScript();
        }
};

// 11327 - Vanish
class spell_legion_rogue_vanish_aura : public SpellScriptLoader
{
    public:
        spell_legion_rogue_vanish_aura() : SpellScriptLoader("spell_legion_rogue_vanish_aura") { }

        class spell_legion_rogue_vanish_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_vanish_aura_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_STEALTH
                });
            }

            void HandleEffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_ROGUE_STEALTH, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_legion_rogue_vanish_aura_AuraScript::HandleEffectRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_vanish_aura_AuraScript();
        }
};

// 79134 - Venomous Wounds
class spell_legion_rogue_venomous_wounds : public SpellScriptLoader
{
    public:
        spell_legion_rogue_venomous_wounds() : SpellScriptLoader("spell_legion_rogue_venomous_wounds") { }

        class spell_legion_rogue_venomous_wounds_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_venomous_wounds_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_VENOM_RUSH,
                    SPELL_ROGUE_VENOMOUS_VIM_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                if (!(eventInfo.GetSpellInfo()->GetAllEffectsMechanicMask() & 1 << MECHANIC_BLEED))
                    return false;

                for (std::pair<uint32, uint32> const& poisonSpellEntry : poisonAuras)
                    if (eventInfo.GetActionTarget()->HasAura(poisonSpellEntry.second, GetCasterGUID()))
                        return true;

                return false;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                int32 energizeAmount = aurEff->GetAmount();
                if (AuraEffect* venomRush = GetTarget()->GetAuraEffect(SPELL_ROGUE_VENOM_RUSH, EFFECT_0))
                    energizeAmount += venomRush->GetAmount();
                GetTarget()->CastCustomSpell(SPELL_ROGUE_VENOMOUS_VIM_ENERGIZE, SPELLVALUE_BASE_POINT0, energizeAmount, GetTarget(), TRIGGERED_FULL_MASK, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_venomous_wounds_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_legion_rogue_venomous_wounds_AuraScript::HandleProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_venomous_wounds_AuraScript();
        }
};

// 193537 - Weaponmaster
class spell_legion_rogue_weaponmaster : public SpellScriptLoader
{
    public:
        spell_legion_rogue_weaponmaster() : SpellScriptLoader("spell_legion_rogue_weaponmaster") { }

        class spell_legion_rogue_weaponmaster_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_legion_rogue_weaponmaster_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_ROGUE_SHURIKEN_STORM,
                    SPELL_ROGUE_WEAPON_MASTER_DOT_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo() || !eventInfo.GetSpellInfo() || GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return false;
                if (eventInfo.GetSpellInfo()->Id == SPELL_ROGUE_SHURIKEN_STORM) // shuriken has a very high spell speed this prevent endless procs
                    GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::seconds(1));
                return true;
            }

            void HandleProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetTypeMask() & PROC_FLAG_DONE_PERIODIC)
                    GetTarget()->CastCustomSpell(SPELL_ROGUE_WEAPON_MASTER_DOT_DAMAGE, SPELLVALUE_BASE_POINT0, eventInfo.GetDamageInfo()->GetDamage(), eventInfo.GetActionTarget(), TRIGGERED_FULL_MASK);
                else
                    GetTarget()->CastSpell(eventInfo.GetActionTarget(), eventInfo.GetSpellInfo()->Id, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_legion_rogue_weaponmaster_AuraScript::CheckProc);
                OnProc += AuraProcFn(spell_legion_rogue_weaponmaster_AuraScript::HandleProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_legion_rogue_weaponmaster_AuraScript();
        }
};

// Poison Bomb Created by
// 192661 - Bag of Tricks
class areatrigger_legion_rogue_poison_bomb : public AreaTriggerEntityScript
{
    public:
        areatrigger_legion_rogue_poison_bomb() : AreaTriggerEntityScript("areatrigger_legion_rogue_poison_bomb") { }

        struct areatrigger_legion_rogue_poison_bombAI : public AreaTriggerAI
        {
            areatrigger_legion_rogue_poison_bombAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                scheduler.Schedule(Milliseconds(500), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_ROGUE_POISON_BOMB, true);
                    task.Repeat();
                });
                AreaTriggerAI::OnCreate();
            }

            void OnUpdate(uint32 diff) override
            {
                scheduler.Update(diff);
            }

            private:
                TaskScheduler scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_legion_rogue_poison_bombAI(areaTrigger);
        }
};

void AddSC_legion_rogue_spell_scripts()
{
    new spell_legion_rogue_alacrity();
    new spell_legion_rogue_backstab();
    new spell_legion_rogue_bag_of_tricks();
    new spell_legion_rogue_between_the_eyes();
    new spell_legion_rogue_blade_flurry();
    new spell_legion_rogue_blade_flurry_chain();
    new spell_legion_rogue_blind();
    new spell_legion_rogue_blunderbuss();
    new spell_legion_rogue_boarding_party();
    new spell_legion_rogue_bribe();
    new spell_legion_rogue_cannonball_barrage();
    new spell_legion_rogue_cannonball_barrage_damage();
    new spell_legion_rogue_cheat_death();
    new spell_legion_rogue_cloak_of_shadows();
    new spell_legion_rogue_cold_blood();
    new spell_legion_rogue_combat_potency();
    new spell_legion_rogue_control_is_king();
    new spell_legion_rogue_creeping_venom();
    new spell_legion_rogue_curse_of_the_dreadblades();
    new spell_legion_rogue_dagger_in_the_dark();
    new spell_legion_rogue_deadly_poison();
    new spell_legion_rogue_death_from_above();
    new spell_legion_rogue_death_from_above_damage();
    new spell_legion_rogue_death_from_above_jump_target();
    new spell_legion_rogue_death_from_above_pre_jump();
    new spell_legion_rogue_deepening_shadows();
    new spell_legion_rogue_envenom();
    new spell_legion_rogue_eviscerate();
    new spell_legion_rogue_exsanguinate();
    new spell_legion_rogue_fan_of_knives();
    new spell_legion_rogue_faster_than_the_light();
    new spell_legion_rogue_finality();
    new spell_legion_rogue_from_the_shadows();
    new spell_legion_rogue_from_the_shadows_buff();
    new spell_legion_rogue_garrote();
    new spell_legion_rogue_ghostly_shell_dummy_aura();
    new spell_legion_rogue_grappling_hook();
    new spell_legion_rogue_greed_damage();
    new spell_legion_rogue_greed_proc();
    new spell_legion_rogue_honor_among_thieves();
    new spell_legion_rogue_internal_bleeding();
    new spell_legion_rogue_kidney_shot();
    new spell_legion_rogue_killing_spree();
    new spell_legion_rogue_kingsbane_pct_mod();
    new spell_legion_rogue_lethal_poisons();
    new spell_legion_rogue_marked_for_death_trig();
    new spell_legion_rogue_mastery_main_gauche();
    new spell_legion_rogue_mind_numbing_poison();
    new spell_legion_rogue_nightblade();
    new spell_legion_rogue_parley();
    new spell_legion_rogue_pistol_shot();
    new spell_legion_rogue_poisoned_knife();
    new spell_legion_rogue_relentless_strikes();
    new spell_legion_rogue_riposte();
    new spell_legion_rogue_roll_the_bones();
    new spell_legion_rogue_run_through();
    new spell_legion_rogue_rupture();
    new spell_legion_rogue_ruthlessness();
    new spell_legion_rogue_saber_slash();
    new spell_legion_rogue_seal_fate();
    new spell_legion_rogue_shadow_dance();
    new spell_legion_rogue_shadow_nova();
    new spell_legion_rogue_shadow_techniques();
    new spell_legion_rogue_shadowstep();
    new spell_legion_rogue_shadowstrike();
    new spell_legion_rogue_shadowy_duel();
    new spell_legion_rogue_shuriken_storm();
    new spell_legion_rogue_sprint();
    new spell_legion_rogue_stealth();
    new spell_legion_rogue_subterfuge();
    new spell_legion_rogue_surge_of_toxin();
    new spell_legion_rogue_take_your_cut();
    new spell_legion_rogue_take_your_cut_bonus();
    new spell_legion_rogue_true_bearing();
    new spell_legion_rogue_tricks_of_the_trade();
    new spell_legion_rogue_turn_the_tables();
    new spell_legion_rogue_turn_the_tables_periodic();
    new spell_legion_rogue_unfair_advantage();
    new spell_legion_rogue_vanish();
    new spell_legion_rogue_vanish_aura();
    new spell_legion_rogue_venomous_wounds();
    new spell_legion_rogue_weaponmaster();

    new areatrigger_legion_rogue_poison_bomb();
}
