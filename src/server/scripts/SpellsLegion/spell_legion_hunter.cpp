/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_HUNTER, SPELLFAMILY_PET and SPELLFAMILY_GENERIC spells used by hunter players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_hun_".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "CreatureAIImpl.h"
#include "ObjectAccessor.h"
#include "Pet.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"
#include "TemporarySummon.h"
#include "Unit.h"
#include "GridNotifiers.h"
#include <iostream>

enum HunterSpells
{
    SPELL_HUNTER_A_MURDER_OF_CROWS_DAMAGE           = 131900,
    SPELL_HUNTER_AIMED_SHOT                         = 19434,
    SPELL_HUNTER_AIMED_SHOT_BONUS_BLOCKER           = 236641,
    SPELL_HUNTER_AIMED_SHOT_GOLDEN_TRAIT            = 191043,
    SPELL_HUNTER_ANIMAL_INSTINCTS_PERIODIC          = 232646,
    SPELL_HUNTER_ARCANE_SHOT                        = 185358,
    SPELL_HUNTER_ARCANE_SHOT_ENERGIZE               = 187675,
    SPELL_HUNTER_ASPECT_CHEETAH_SLOW                = 186258,
    SPELL_HUNTER_ASPECT_OF_THE_BEAST                = 191384,
    SPELL_HUNTER_ASPECT_OF_THE_CHEETAH_AOE          = 203233,
    SPELL_HUNTER_ASPECT_OF_THE_EAGLE                = 186289,
    SPELL_HUNTER_ASPECT_OF_THE_TURTLE_PACIFY        = 205769,
    SPELL_HUNTER_BEAST_CLEAVE_DAMAGE                = 118459,
    SPELL_HUNTER_BEAST_CLEAVE_PASSIVE               = 115939,
    SPELL_HUNTER_BEAST_CLEAVE_PET                   = 118455,
    SPELL_HUNTER_BESTIAL_CUNNING                    = 191397,
    SPELL_HUNTER_BESTIAL_FEROCITY                   = 191413,
    SPELL_HUNTER_BESTIAL_TENACITY                   = 191414,
    SPELL_HUNTER_BESTIAL_WRATH                      = 19574,
    SPELL_HUNTER_BESTIAL_WRATH_HATI                 = 207033,
    SPELL_HUNTER_BESTIAL_WRATH_LEVEL_BONUS          = 231548,
    SPELL_HUNTER_BINDING_SHOT_AURA                  = 117405,
    SPELL_HUNTER_BINDING_SHOT_STUN                  = 117526,
    SPELL_HUNTER_BINDING_SHOT_VISUAL                = 117614,
    SPELL_HUNTER_BIRD_OF_PREY_HEAL                  = 224765,
    SPELL_HUNTER_BLACK_ARROW                        = 194599,
    SPELL_HUNTER_BLINK_STRIKES                      = 130392,
    SPELL_HUNTER_BLINK_STRIKES_TELEPORT             = 199810,
    SPELL_HUNTER_BURSTING_SHOT_DISORIENT            = 224729,
    SPELL_HUNTER_CALTROPS_DEBUFF                    = 194279,
    SPELL_HUNTER_CAREFUL_AIM_PERIODIC               = 63468,
    SPELL_HUNTER_CHIMAERA_SHOT_FROST_DAMAGE         = 171454,
    SPELL_HUNTER_CHIMAERA_SHOT_NATURE_DAMAGE        = 171457,
    SPELL_HUNTER_CRITICAL_FOCUS_ENERGIZE            = 215107,
    SPELL_HUNTER_DIAMOND_ICE                        = 203340,
    SPELL_HUNTER_DIRE_BEAST                         = 120679,
    SPELL_HUNTER_DIRE_FRENZY                        = 217200,
    SPELL_HUNTER_DIRE_FRENZY_DAMAGE                 = 217207,
    SPELL_HUNTER_DISENGAGE                          = 781,
    SPELL_HUNTER_DRAGONSFIRE_CONFLAGRATION          = 194859,
    SPELL_HUNTER_EXHILARATION                       = 109304,
    SPELL_HUNTER_EXIHILARATION_LEVEL_BONUS          = 231546,
    SPELL_HUNTER_EXIHILARATION_PET_HEAL             = 128594,
    SPELL_HUNTER_EXPERT_TRAPPER                     = 199543,
    SPELL_HUNTER_EXPLOSIVE_SHOT_DAMAGE              = 212680,
    SPELL_HUNTER_EXPLOSIVE_SHOT_OVERRIDE            = 212431,
    SPELL_HUNTER_EXPLOSIVE_TRAP_DAMAGE              = 13812,
    SPELL_HUNTER_EXPLOSIVE_TRAP_TIME_BONUS          = 237338,
    SPELL_HUNTER_FLANKING_STRIKE                    = 202800,
    SPELL_HUNTER_FLANKING_STRIKE_PET                = 204740,
    SPELL_HUNTER_FLARE_DISPEL                       = 132951,
    SPELL_HUNTER_FLUFFY_GO_BONUS                    = 218955,
    SPELL_HUNTER_FREEZING_TRAP_AREATRIGGER          = 187651,
    SPELL_HUNTER_FREEZING_TRAP_FREEZE               = 3355,
    SPELL_HUNTER_FREEZING_TRAP_HONOR_VERSION        = 203337,
    SPELL_HUNTER_FROZEN_WAKE                        = 201142,
    SPELL_HUNTER_FURIOUS_SWIPE                      = 197047,
    SPELL_HUNTER_GO_FOR_THE_THROAT                  = 212670,
    SPELL_HUNTER_HARPOON                            = 190925,
    SPELL_HUNTER_HARPOON_ROOT                       = 190927,
    SPELL_HUNTER_HEART_OF_THE_PHOENIX_RESURRECT     = 54114,
    SPELL_HUNTER_HELLCARVER                         = 203673,
    SPELL_HUNTER_HI_EXPLOSIVE_TRAP_DAMAGE           = 236777,
    SPELL_HUNTER_HUNTER_S_ADVANTAGE_BONUS           = 211138,
    SPELL_HUNTER_HUNTER_S_MARK                      = 185365,
    SPELL_HUNTER_HUNTER_S_MARK_CASTER               = 185743,
    SPELL_HUNTER_HUNTING_PACK                       = 203235,
    SPELL_HUNTER_INTIMIDATION_STUN                  = 24394,
    SPELL_HUNTER_JAWS_OF_THUNDER_DAMAGE             = 197163,
    SPELL_HUNTER_JAWS_OF_THUNDER_TRAIT              = 197162,
    SPELL_HUNTER_KILL_COMMAND                       = 34026,
    SPELL_HUNTER_KILL_COMMAND_CHARGE                = 118171,
    SPELL_HUNTER_LONE_WOLF                          = 155228,
    SPELL_HUNTER_MARKED_FOR_DEATH                   = 190529,
    SPELL_HUNTER_MARKED_SHOT_DAMAGE                 = 212621,
    SPELL_HUNTER_MARKING_TARGETS                    = 223138,
    SPELL_HUNTER_MASTER_OF_BEASTS                   = 197248,
    SPELL_HUNTER_MASTER_S_CALL_PET                  = 62305,
    SPELL_HUNTER_MASTERY_HUNTING_COMPANION          = 191334,
    SPELL_HUNTER_MIMIRON_S_SHELL_PERIODIC           = 197161,
    SPELL_HUNTER_MISDIRECTION                       = 34477,
    SPELL_HUNTER_MISDIRECTION_PROC                  = 35079,
    SPELL_HUNTER_MONGOOSE_BITE                      = 190928, 
    SPELL_HUNTER_MONGOOSE_FURY                      = 190931,
    SPELL_HUNTER_MULTI_SHOT                         = 2643,
    SPELL_HUNTER_MULTI_SHOT_ENERGIZE                = 213363,
    SPELL_HUNTER_PIERCING_SHOT_DAMAGE               = 213678,
    SPELL_HUNTER_POSTHASTE_BONUS                    = 118922,
    SPELL_HUNTER_POSTHASTE_TALENT                   = 109215,
    SPELL_HUNTER_ROAR_OF_SACRIFICE_DAMAGE           = 67481,
    SPELL_HUNTER_SEPARATION_ANXIETY_BONUS           = 213882,
    SPELL_HUNTER_SERPENT_STING_DEBUFF               = 118253,
    SPELL_HUNTER_SERPENT_STING_TALENT               = 87935,
    SPELL_HUNTER_SIDEWINDERS                        = 214579,
    SPELL_HUNTER_SIDEWINDERS_DAMAGE                 = 214581,
    SPELL_HUNTER_SPIDER_STING_SILENCE               = 202933,
    SPELL_HUNTER_SPIRIT_BOUND_HEAL                  = 197205,
    SPELL_HUNTER_SPIRIT_BOUND_TRAIT                 = 197199,
    SPELL_HUNTER_STAMPEDE_PERIODIC                  = 201631,
    SPELL_HUNTER_STEADY_FOCUS_BONUS                 = 193534,
    SPELL_HUNTER_STEEL_TRAP_EXPERT_PERIODIC         = 201199,
    SPELL_HUNTER_STEEL_TRAP_INSTANT_DAMAGE          = 201199,
    SPELL_HUNTER_STEEL_TRAP_PERIODIC                = 162487,
    SPELL_HUNTER_STEEL_TRAP_ROOT                    = 162480,
    SPELL_HUNTER_STICKY_BOMB_KNOCK_BACK             = 191244,
    SPELL_HUNTER_STICKY_TAR                         = 203264,
    SPELL_HUNTER_STICKY_TAR_PERIODIC                = 203266,
    SPELL_HUNTER_SUMMON_HATI_NORMAL                 = 197388, ///@TODO: implement hidden skin versions
    SPELL_HUNTER_SUPER_STICKY_TAR                   = 236699,
    SPELL_HUNTER_SURGE_OF_THE_STORMGOD_DAMAGE       = 197465,
    SPELL_HUNTER_SURGE_OF_THE_STORMGOD_VISUAL       = 197466,
    SPELL_HUNTER_SURVIVAL_OF_THE_FITTEST_BONUS      = 190515,
    SPELL_HUNTER_SURVIVAL_OF_THE_FITTEST_TRAIT      = 190514,
    SPELL_HUNTER_TALON_SLASH                        = 242735,
    SPELL_HUNTER_TAR_TRAP_SLOW                      = 135299,
    SPELL_HUNTER_TAR_TRAP_SLOW_AREATRIGGER          = 187700,
    SPELL_HUNTER_THE_BEAST_WITHIN_BONUS             = 212704,
    SPELL_HUNTER_THE_BEAST_WITHIN_HONOR_TALENT      = 212668,
    SPELL_HUNTER_THROWING_AXES_DAMAGE               = 200167,
    SPELL_HUNTER_TITAN_S_THUNDER_DIRE_FRENZY        = 218638,
    SPELL_HUNTER_TITAN_S_THUNDER_DIRE_FRENZY_DAMAGE = 218635,
    SPELL_HUNTER_TITAN_S_THUNDER_PERIODIC           = 207094,
    SPELL_HUNTER_TITAN_S_THUNDER_PERIODIC_DAMAGE    = 207097,
    SPELL_HUNTER_TRAILBLAZER_BUFF                   = 231390,
    SPELL_HUNTER_TRICK_SHOT_BONUS                   = 227272,
    SPELL_HUNTER_TRICK_SHOT_SELECTOR                = 199885,
    SPELL_HUNTER_TRICK_SHOT_TALENT                  = 199522,
    SPELL_HUNTER_TRUESHOT                           = 193526,
    SPELL_HUNTER_VULNERABLE                         = 187131,
    SPELL_HUNTER_WAY_OF_THE_COBRA                   = 194397,
    SPELL_HUNTER_WAYLAY                             = 234955,
    SPELL_HUNTER_WEAKENED_HEART                     = 55711,
    SPELL_HUNTER_WILD_PROTECTOR_AREATRIGGER         = 204358,
    SPELL_HUNTER_WILD_PROTECTOR_BONUS               = 204205,
    SPELL_HUNTER_WIND_ARROW_BARRAGE                 = 191061,
    SPELL_HUNTER_WINDBURST_BONUS                    = 204477,
};

enum HunterMisc
{
    SPELL_VISUAL_BARRAGE        = 39597,
    SPELL_VISUAL_SIDEWINDERS    = 56931,
    TRAVEL_SPEED_SIDEWINDERS    = 18,
    TRAVEL_SPEED_BARRAGE        = 29,
    ACTION_WAYLAY_BONUS         = 1,
};

// ...
#define direBeastSummonSpell RAND(121118, 122802, 122804, 122806, 122807, 122809, 122811, 126213, 126214, 126215, 126216, 132764, 138574, 138580, 139226, 139227, 149365, 170357, 170358, 170359, 170360, 170361, 170362, 170363, 170364, 204397, 204409, 204410, 204411, 204412, 204413, 204415, 204416, 204417, 204418, 204419, 204420, 204422, 204423, 204424, 204425, 204426, 204427, 204428, 204429, 204430, 204431, 204432, 204433, 204434, 204480, 204494, 204507, 204512, 204514, 204516, 204518, 204520, 204521, 204522, 204523, 204524, 204525, 204526, 204527, 204528, 204530, 204532, 204533, 204534, 204546, 204548, 204550, 204552, 204554, 204555, 204556, 204557, 204558, 204560, 204561, 204565, 204568, 204569, 204571, 204572, 204583, 204584, 204585, 204614, 204619, 204621, 204622, 204623, 204627, 204629, 204630, 204632, 204633, 204633, 212381, 212382, 212386, 212388, 212389, 212396, 212397, 219199, 224573, 224574, 224575, 224576, 224577, 224578)

// 131894 - A Murder of Crows
// 206505 - A Murder of Crows
class spell_hun_legion_a_murder_of_crows : public SpellScriptLoader
{
    public:
        spell_hun_legion_a_murder_of_crows() : SpellScriptLoader("spell_hun_legion_a_murder_of_crows") { }

        class spell_hun_legion_a_murder_of_crows_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_a_murder_of_crows_AuraScript);

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_A_MURDER_OF_CROWS_DAMAGE, true, nullptr, aurEff, GetCasterGUID());
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH)
                    if (Unit* caster = GetCaster())
                        caster->GetSpellHistory()->ResetCooldown(GetId(), true);
            }                  

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_a_murder_of_crows_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_a_murder_of_crows_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_a_murder_of_crows_AuraScript();
        }
};

// 19434 - Aimed Shot
class spell_hun_legion_aimed_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_aimed_shot() : SpellScriptLoader("spell_hun_legion_aimed_shot") { }

        class spell_hun_legion_aimed_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_aimed_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_AIMED_SHOT_BONUS_BLOCKER,
                    SPELL_HUNTER_TRICK_SHOT_TALENT,
                    SPELL_HUNTER_TRICK_SHOT_SELECTOR,
                });
            }

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                if (!GetHitUnit()->HasAura(SPELL_HUNTER_AIMED_SHOT_BONUS_BLOCKER, GetCaster()->GetGUID()))
                    if (SpellEffectInfo const* eff2 = GetEffectInfo(EFFECT_2))
                        SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), eff2->CalcValue(GetCaster())));

                if (GetSpell()->HasCustomCastFlag(CUSTOM_CAST_FLAG_SPECIAL_CALCULATION))
                {
                    if (AuraEffect* trickShot = GetCaster()->GetAuraEffect(SPELL_HUNTER_TRICK_SHOT_TALENT, EFFECT_0))
                        SetHitDamage(CalculatePct(GetHitDamage(), trickShot->GetAmount()));
                }
                else if (GetCaster()->HasAura(SPELL_HUNTER_TRICK_SHOT_TALENT))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_TRICK_SHOT_SELECTOR, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_aimed_shot_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_aimed_shot_SpellScript();
        }
};

// 236641 - Aimed Shot
class spell_hun_legion_aimed_shot_blocker : public SpellScriptLoader
{
    public:
        spell_hun_legion_aimed_shot_blocker() : SpellScriptLoader("spell_hun_legion_aimed_shot_blocker") { }

        class spell_hun_legion_aimed_shot_blocker_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_aimed_shot_blocker_AuraScript);

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 1000;
            }               

            void HandleCustomTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    if (GetTarget()->IsInCombatWith(caster))
                        return;
                Remove();
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_hun_legion_aimed_shot_blocker_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_aimed_shot_blocker_AuraScript::HandleCustomTick, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_hun_legion_aimed_shot_blocker_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_aimed_shot_blocker_AuraScript();
        }
};

// 204315 - Animal Instincts
class spell_hun_legion_animal_instincts : public SpellScriptLoader
{
    public:
        spell_hun_legion_animal_instincts() : SpellScriptLoader("spell_hun_legion_animal_instincts") { }

        class spell_hun_legion_animal_instincts_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_animal_instincts_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_FLANKING_STRIKE,  // cd
                    SPELL_HUNTER_MONGOOSE_BITE,  // charge
                    SPELL_HUNTER_ASPECT_OF_THE_EAGLE, // cd
                    SPELL_HUNTER_HARPOON    // charge
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                int32 spellId = RAND(SPELL_HUNTER_FLANKING_STRIKE, SPELL_HUNTER_MONGOOSE_BITE, SPELL_HUNTER_ASPECT_OF_THE_EAGLE, SPELL_HUNTER_HARPOON);
                switch (spellId)
                {
                    case SPELL_HUNTER_FLANKING_STRIKE:
                    case SPELL_HUNTER_ASPECT_OF_THE_EAGLE:
                        GetTarget()->GetSpellHistory()->ModifyCooldown(spellId, -(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_ANIMAL_INSTINCTS_PERIODIC)->GetEffect(EFFECT_0)->CalcValue(GetTarget()) * IN_MILLISECONDS));
                        break;
                    case SPELL_HUNTER_MONGOOSE_BITE:
                    case SPELL_HUNTER_HARPOON:
                        GetTarget()->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(spellId)->ChargeCategoryId, sSpellMgr->AssertSpellInfo(SPELL_HUNTER_ANIMAL_INSTINCTS_PERIODIC)->GetEffect(EFFECT_0)->CalcValue(GetTarget()) * IN_MILLISECONDS);
                        break;
                    default:
                        break;
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_animal_instincts_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_animal_instincts_AuraScript();
        }
};

// 185358 - Arcane Shot
class spell_hun_legion_arcane_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_arcane_shot() : SpellScriptLoader("spell_hun_legion_arcane_shot") { }

        class spell_hun_legion_arcane_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_arcane_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_ARCANE_SHOT_ENERGIZE,
                    SPELL_HUNTER_TRUESHOT,
                    SPELL_HUNTER_HUNTER_S_MARK
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_ARCANE_SHOT_ENERGIZE, true);
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_TRUESHOT) || GetCaster()->HasAura(SPELL_HUNTER_MARKING_TARGETS))
                {
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_HUNTER_S_MARK_CASTER, true);
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_HUNTER_S_MARK, true);
                }
            }                      

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_arcane_shot_SpellScript::HandleAfterCast);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_arcane_shot_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_arcane_shot_SpellScript();
        }
};

// 186257 - Aspect of the Cheetah
class spell_hun_legion_aspect_cheetah : public SpellScriptLoader
{
    public:
        spell_hun_legion_aspect_cheetah() : SpellScriptLoader("spell_hun_legion_aspect_cheetah") { }

        class spell_hun_legion_aspect_cheetah_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_aspect_cheetah_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_ASPECT_CHEETAH_SLOW
                });
            }

            void HandleOnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_EXPIRE)
                    GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_ASPECT_CHEETAH_SLOW, true);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_hun_legion_aspect_cheetah_AuraScript::HandleOnRemove, EFFECT_0, SPELL_AURA_MOD_INCREASE_SPEED, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
             return new spell_hun_legion_aspect_cheetah_AuraScript();
        }

        class spell_hun_legion_aspect_cheetah_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_aspect_cheetah_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_HUNTING_PACK,
                    SPELL_HUNTER_ASPECT_OF_THE_CHEETAH_AOE
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_HUNTING_PACK))
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_ASPECT_OF_THE_CHEETAH_AOE, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_aspect_cheetah_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_aspect_cheetah_SpellScript();
        }
};

// 186265 - Aspect of the Turtle
class spell_hun_legion_aspect_of_the_turtle : public SpellScriptLoader
{
    public:
        spell_hun_legion_aspect_of_the_turtle() : SpellScriptLoader("spell_hun_legion_aspect_of_the_turtle") { }

        class spell_hun_legion_aspect_of_the_turtle_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_aspect_of_the_turtle_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({ 
                    SPELL_HUNTER_ASPECT_OF_THE_TURTLE_PACIFY 
                });
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_ASPECT_OF_THE_TURTLE_PACIFY, true);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_HUNTER_ASPECT_OF_THE_TURTLE_PACIFY);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_hun_legion_aspect_of_the_turtle_AuraScript::OnApply, EFFECT_0, SPELL_AURA_MOD_ATTACKER_MELEE_HIT_CHANCE, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_hun_legion_aspect_of_the_turtle_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_MOD_ATTACKER_MELEE_HIT_CHANCE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_aspect_of_the_turtle_AuraScript();
        }
};

// 193530 - Aspect of the Wild
class spell_hun_legion_aspect_of_the_wild : public SpellScriptLoader
{
    public:
        spell_hun_legion_aspect_of_the_wild() : SpellScriptLoader("spell_hun_legion_aspect_of_the_wild") { }

        class spell_hun_legion_aspect_of_the_wild_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_aspect_of_the_wild_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    pet->EnergizeBySpell(pet, GetId(), aurEff->GetAmount(), Powers(aurEff->GetMiscValue()));
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_aspect_of_the_wild_AuraScript::HandleDummyTick, EFFECT_1, SPELL_AURA_PERIODIC_ENERGIZE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_aspect_of_the_wild_AuraScript();
        }
};

class BarrageVisualEvent : public BasicEvent
{
    public:
        BarrageVisualEvent(Unit* owner) : _owner(owner) {}

        bool Execute(uint64 time, uint32 /*diff*/) override
        {
            _owner->SendPlaySpellVisual(_owner->GetRandomPoint(_owner->GetNearPosition(50.00f, 0.00f), 30.00f), 0.00f, SPELL_VISUAL_BARRAGE, 0, 0, TRAVEL_SPEED_BARRAGE, false);

            if (!_owner->GetCurrentSpell(CURRENT_CHANNELED_SPELL))
                return true;

            _owner->m_Events.AddEvent(this, time + 100);
            return false;
        }

    private:
        Unit* _owner;
};

// 120360 - Barrage
class spell_hun_legion_barrage : public SpellScriptLoader
{
    public:
        spell_hun_legion_barrage() : SpellScriptLoader("spell_hun_legion_barrage") { }

        class spell_hun_legion_barrage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_barrage_AuraScript);

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->m_Events.AddEvent(new BarrageVisualEvent(GetTarget()), GetTarget()->m_Events.CalculateTime(100));
            }                       

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_hun_legion_barrage_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_barrage_AuraScript();
        }
};

// 16827 - Claw
// 17253 - Bite
// 49966 - Smack
// 242735 - Talon Slash
class spell_hun_legion_basic_pet_attacks : public SpellScriptLoader
{
    public:
        spell_hun_legion_basic_pet_attacks() : SpellScriptLoader("spell_hun_legion_basic_pet_attacks") { }

        class spell_hun_legion_basic_pet_attacks_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_basic_pet_attacks_SpellScript);

            bool Load() override
            {
                return GetCaster()->IsPet();
            }

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                SetEffectValue(GetEffectValue() + int32(GetCaster()->ToPet()->GetBonusDamage() * 0.333f));
            }

            void CalculateBonusDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetSpellInfo()->Id == SPELL_HUNTER_TALON_SLASH)
                    return;

                // Deals 100% more damage and costs 100% more Focus when your pet has 50 or more Focus.
                // check if we have min 25 focus left (power cost is already removed)
                if (GetCaster()->GetPower(POWER_FOCUS) >= 25)
                {
                    GetCaster()->ModifyPower(POWER_FOCUS, -25);
                    SetHitDamage(GetHitDamage() * 2);
                }
            }                  

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_hun_legion_basic_pet_attacks_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_basic_pet_attacks_SpellScript::CalculateBonusDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_basic_pet_attacks_SpellScript();
        }
};

// 118455 - Beast Cleave
class spell_hun_legion_beast_cleave : public SpellScriptLoader
{
    public:
        spell_hun_legion_beast_cleave() : SpellScriptLoader("spell_hun_legion_beast_cleave") { }

        class spell_hun_legion_beast_cleave_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_beast_cleave_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BEAST_CLEAVE_DAMAGE,
                    SPELL_HUNTER_FURIOUS_SWIPE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage() > 0;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                int32 damage = eventInfo.GetDamageInfo()->GetDamage();
                if (Unit* owner = GetCaster())
                    if (AuraEffect* furiousSwipe = owner->GetAuraEffect(SPELL_HUNTER_FURIOUS_SWIPE, EFFECT_0))
                        AddPct(damage, furiousSwipe->GetAmount());

                GetTarget()->CastCustomSpell(SPELL_HUNTER_BEAST_CLEAVE_DAMAGE, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetProcTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_beast_cleave_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_beast_cleave_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_beast_cleave_AuraScript();
        }
};

// 118459 - Beast Cleave
class spell_hun_legion_beast_cleave_damage : public SpellScriptLoader
{
    public:
        spell_hun_legion_beast_cleave_damage() : SpellScriptLoader("spell_hun_legion_beast_cleave_damage") { }

        class spell_hun_legion_beast_cleave_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_beast_cleave_damage_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                // Shouldn't hit the initiall target - getexpltargetunit doesnt work here
                if (GetCaster()->GetVictim())
                    targets.remove(GetCaster()->GetVictim());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_beast_cleave_damage_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_beast_cleave_damage_SpellScript();
        }
};

// 19574 - Bestial Wrath
class spell_hun_legion_bestial_wrath : public SpellScriptLoader
{
    public:
        spell_hun_legion_bestial_wrath() : SpellScriptLoader("spell_hun_legion_bestial_wrath") { }

        class spell_hun_legion_bestial_wrath_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_bestial_wrath_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MASTER_OF_BEASTS,
                    SPELL_HUNTER_BESTIAL_WRATH_HATI
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_MASTER_OF_BEASTS))
                    for (Unit* summon : GetCaster()->m_Controlled)
                        if (summon->IsHatiPet())
                            GetCaster()->CastSpell(summon, SPELL_HUNTER_BESTIAL_WRATH_HATI, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_bestial_wrath_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_bestial_wrath_SpellScript();
        }

        class spell_hun_legion_bestial_wrath_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_bestial_wrath_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_THE_BEAST_WITHIN_HONOR_TALENT,
                    SPELL_HUNTER_THE_BEAST_WITHIN_BONUS
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasAura(SPELL_HUNTER_THE_BEAST_WITHIN_HONOR_TALENT))
                    GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_THE_BEAST_WITHIN_BONUS, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_HUNTER_THE_BEAST_WITHIN_BONUS);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_hun_legion_bestial_wrath_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_bestial_wrath_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_bestial_wrath_AuraScript();
        }
};

// 224764 - Bird of Prey
class spell_hun_legion_bird_of_prey : public SpellScriptLoader
{
    public:
        spell_hun_legion_bird_of_prey() : SpellScriptLoader("spell_hun_legion_bird_of_prey") { }

        class spell_hun_legion_bird_of_prey_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_bird_of_prey_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BIRD_OF_PREY_HEAL
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetTarget()->CastCustomSpell(SPELL_HUNTER_BIRD_OF_PREY_HEAL, SPELLVALUE_BASE_POINT0, CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_bird_of_prey_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_bird_of_prey_AuraScript();
        }
};

// 214351 - Black Arrow
class spell_hun_legion_black_arrow_proc : public SpellScriptLoader
{
    public:
        spell_hun_legion_black_arrow_proc() : SpellScriptLoader("spell_hun_legion_black_arrow_proc") { }

        class spell_hun_legion_black_arrow_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_black_arrow_proc_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BLACK_ARROW
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_HUNTER_BLACK_ARROW, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_black_arrow_proc_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_black_arrow_proc_AuraScript();
        }
};

// 16827 - Claw
// 17253 - Bite
// 49966 - Smack
// 145625 - Bite
// 242735 - Talon Slash
class spell_hun_legion_blink_strikes : public SpellScriptLoader
{
    public:
        spell_hun_legion_blink_strikes() : SpellScriptLoader("spell_hun_legion_blink_strikes") { }

        class spell_hun_legion_blink_strikes_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_blink_strikes_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BLINK_STRIKES,
                    SPELL_HUNTER_BLINK_STRIKES_TELEPORT
                });
            }

            void HandleHit()
            {
                if (Unit* owner = GetCaster()->GetOwner(false))
                    if (owner->HasAura(SPELL_HUNTER_BLINK_STRIKES))
                        if (!GetCaster()->GetSpellHistory()->HasCooldown(SPELL_HUNTER_BLINK_STRIKES_TELEPORT))
                        {
                            GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_BLINK_STRIKES_TELEPORT, true);
                            GetCaster()->GetSpellHistory()->AddCooldown(SPELL_HUNTER_BLINK_STRIKES_TELEPORT, 0, std::chrono::milliseconds(20000));
                        }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_hun_legion_blink_strikes_SpellScript::HandleHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_blink_strikes_SpellScript();
        }
};

// 204089 - Bullseye
class spell_hun_legion_bullseye : public SpellScriptLoader
{
    public:
        spell_hun_legion_bullseye() : SpellScriptLoader("spell_hun_legion_bullseye") { }

        class spell_hun_legion_bullseye_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_bullseye_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcTarget()->HealthBelowPct(GetEffect(EFFECT_0)->GetAmount());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_bullseye_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_bullseye_AuraScript();
        }
};

// 212436 - Butchery
class spell_hun_legion_butchery : public SpellScriptLoader
{
    public:
        spell_hun_legion_butchery() : SpellScriptLoader("spell_hun_legion_butchery") { }

        class spell_hun_legion_butchery_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_butchery_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_HELLCARVER
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = uint32(targets.size());
            }                   

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* hellcarver = GetCaster()->GetAuraEffect(SPELL_HUNTER_HELLCARVER, EFFECT_0))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), hellcarver->GetAmount() * _targetCount));
            }                  

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_butchery_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_SRC_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_butchery_SpellScript::HandleOnEffectHit, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
            private:
                uint32 _targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_butchery_SpellScript();
        }
};

// 186387 - Bursting Shot
class spell_hun_legion_bursting_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_bursting_shot() : SpellScriptLoader("spell_hun_legion_bursting_shot") { }

        class spell_hun_legion_bursting_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_bursting_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BURSTING_SHOT_DISORIENT
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_BURSTING_SHOT_DISORIENT, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_bursting_shot_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_bursting_shot_SpellScript();
        }
};

// 191048 - Call of the Hunter
class spell_hun_legion_call_of_the_hunter : public SpellScriptLoader
{
    public:
        spell_hun_legion_call_of_the_hunter() : SpellScriptLoader("spell_hun_legion_call_of_the_hunter") { }

        class spell_hun_legion_call_of_the_hunter_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_call_of_the_hunter_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_WIND_ARROW_BARRAGE
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                for (AuraApplication* aurApp : GetTarget()->GetTargetAuraApplications(SPELL_HUNTER_VULNERABLE))
                    GetTarget()->CastSpell(aurApp->GetTarget(), SPELL_HUNTER_WIND_ARROW_BARRAGE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_call_of_the_hunter_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_call_of_the_hunter_AuraScript();
        }
};

// Call Pet - 883, 83242, 83243, 83244, 83245
class spell_hun_legion_call_pet : public SpellScriptLoader
{
    public:
        spell_hun_legion_call_pet() : SpellScriptLoader("spell_hun_legion_call_pet") { }

        class spell_hun_legion_call_pet_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_call_pet_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_LONE_WOLF
                });
            }

            SpellCastResult CheckTalent()
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_LONE_WOLF))
                    return SPELL_FAILED_NO_PET;

                return SPELL_CAST_OK;
            }         

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_hun_legion_call_pet_SpellScript::CheckTalent);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_call_pet_SpellScript();
        }
};

// 199483 - Camouflage
class spell_hun_legion_camouflage : public SpellScriptLoader
{
    public:
        spell_hun_legion_camouflage() : SpellScriptLoader("spell_hun_legion_camouflage") { }

        class spell_hun_legion_camouflage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_camouflage_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                // should affect the caster too
                targets.push_back(GetCaster());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_camouflage_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_MINIONS);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_camouflage_SpellScript();
        }
};

// 53238 - Careful Aim
class spell_hun_legion_careful_aim : public SpellScriptLoader
{
    public:
        spell_hun_legion_careful_aim() : SpellScriptLoader("spell_hun_legion_careful_aim") { }

        class spell_hun_legion_careful_aim_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_careful_aim_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_CAREFUL_AIM_PERIODIC
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                // is this possible?
                if (!eventInfo.GetProcTarget())
                    return false;

                return eventInfo.GetProcTarget()->HealthAbovePct(GetAura()->GetEffect(EFFECT_1)->GetAmount());
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                CustomSpellValues values;
                values.AddSpellMod(SPELLVALUE_CUSTOM_FLAGS, CUSTOM_CAST_FLAG_NO_DONE_BONUS);
                values.AddSpellMod(SPELLVALUE_BASE_POINT0, CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), GetAura()->GetEffect(EFFECT_2)->GetAmount()) / sSpellMgr->AssertSpellInfo(SPELL_HUNTER_CAREFUL_AIM_PERIODIC)->GetMaxTicks(DIFFICULTY_NONE));
                GetCaster()->CastCustomSpell(SPELL_HUNTER_CAREFUL_AIM_PERIODIC, values, eventInfo.GetProcTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_careful_aim_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_careful_aim_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_careful_aim_AuraScript();
        }
};

// 187708 - Carve
class spell_hun_legion_carve : public SpellScriptLoader
{
    public:
        spell_hun_legion_carve() : SpellScriptLoader("spell_hun_legion_carve") { }

        class spell_hun_legion_carve_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_carve_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_HELLCARVER
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = uint32(targets.size());
            }                   

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* hellcarver = GetCaster()->GetAuraEffect(SPELL_HUNTER_HELLCARVER, EFFECT_0))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), hellcarver->GetAmount() * _targetCount));
            }                  

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_carve_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_CONE_ENEMY_104);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_carve_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
            private:
                uint32 _targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_carve_SpellScript();
        }
};

// 53209 - Chimaera Shot
class spell_hun_legion_chimaera_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_chimaera_shot() : SpellScriptLoader("spell_hun_legion_chimaera_shot") { }

        class spell_hun_legion_chimaera_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_chimaera_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_CHIMAERA_SHOT_NATURE_DAMAGE,
                    SPELL_HUNTER_CHIMAERA_SHOT_FROST_DAMAGE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit() == GetExplTargetUnit())
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_CHIMAERA_SHOT_NATURE_DAMAGE, true);
                else
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_CHIMAERA_SHOT_FROST_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_chimaera_shot_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_chimaera_shot_SpellScript();
        }
};

// 193455 - Cobra Shot
class spell_hun_legion_cobra_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_cobra_shot() : SpellScriptLoader("spell_hun_legion_cobra_shot") { }

        class spell_hun_legion_cobra_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_cobra_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_WAY_OF_THE_COBRA
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* wayOfTheCobra = GetCaster()->GetAuraEffect(SPELL_HUNTER_WAY_OF_THE_COBRA, EFFECT_0))
                {
                    uint16 guardianCnt = 0;
                    for (Unit* summon : GetCaster()->m_Controlled)
                        if (summon->HasUnitTypeMask(UNIT_MASK_GUARDIAN) || summon->HasUnitTypeMask(UNIT_MASK_HUNTER_PET))
                            guardianCnt++;

                    if (guardianCnt > 0)
                        SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), guardianCnt * wayOfTheCobra->GetAmount()));
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_cobra_shot_SpellScript::HandleOnEffectHit, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_cobra_shot_SpellScript();
        }
};

// 191328 - Critical Focus
class spell_hun_legion_critical_focus : public SpellScriptLoader
{
    public:
        spell_hun_legion_critical_focus() : SpellScriptLoader("spell_hun_legion_critical_focus") { }

        class spell_hun_legion_critical_focus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_critical_focus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_CRITICAL_FOCUS_ENERGIZE
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_CRITICAL_FOCUS_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_critical_focus_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_critical_focus_AuraScript();
        }
};

// 204683 - Dark Whisper
class spell_hun_legion_dark_whisper : public SpellScriptLoader
{
    public:
        spell_hun_legion_dark_whisper() : SpellScriptLoader("spell_hun_legion_dark_whisper") { }

        class spell_hun_legion_dark_whisper_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_dark_whisper_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BLACK_ARROW
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_HUNTER_BLACK_ARROW, GetCaster()->GetOwnerGUID()));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_dark_whisper_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_dark_whisper_SpellScript();
        }
};

// 120679 - Dire Beast
class spell_hun_legion_dire_beast : public SpellScriptLoader
{
    public:
        spell_hun_legion_dire_beast() : SpellScriptLoader("spell_hun_legion_dire_beast") { }

        class spell_hun_legion_dire_beast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_dire_beast_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BESTIAL_WRATH_LEVEL_BONUS,
                    SPELL_HUNTER_BESTIAL_WRATH
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), direBeastSummonSpell, true);

                if (GetCaster()->HasAura(SPELL_HUNTER_BESTIAL_WRATH_LEVEL_BONUS))
                    if (SpellEffectInfo const* bestialWrath = sSpellMgr->AssertSpellInfo(SPELL_HUNTER_BESTIAL_WRATH)->GetEffect(EFFECT_2))
                        GetCaster()->GetSpellHistory()->ModifyCooldown(SPELL_HUNTER_BESTIAL_WRATH, -(bestialWrath->CalcValue(GetCaster()) * IN_MILLISECONDS));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_dire_beast_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_dire_beast_SpellScript();
        }
};

class DireFrenzyDamageEvent : public BasicEvent
{
    public:
        DireFrenzyDamageEvent(Unit* owner, ObjectGuid targetGuid) : _owner(owner), _targetGuid(targetGuid) {}

        bool Execute(uint64 time, uint32 /*diff*/) override
        {
            if (Unit* target = ObjectAccessor::GetUnit(*_owner, _targetGuid))
            {
                _owner->CastSpell(target, SPELL_HUNTER_DIRE_FRENZY_DAMAGE, true);

                if (_owner->HasAura(SPELL_HUNTER_TITAN_S_THUNDER_DIRE_FRENZY))
                    _owner->CastSpell(target, SPELL_HUNTER_TITAN_S_THUNDER_DIRE_FRENZY_DAMAGE, true);
            }

            if (--_repeatCounter == 0)
            {
                _owner->RemoveAurasDueToSpell(SPELL_HUNTER_TITAN_S_THUNDER_DIRE_FRENZY);
                return true;
            }

            _owner->m_Events.AddEvent(this, time + 100);
            return false;
        }

    private:
        Unit* _owner;
        ObjectGuid _targetGuid;
        int8 _repeatCounter = 5;
};

// 217200 - Dire Frenzy
class spell_hun_legion_dire_frenzy : public SpellScriptLoader
{
    public:
        spell_hun_legion_dire_frenzy() : SpellScriptLoader("spell_hun_legion_dire_frenzy") { }

        class spell_hun_legion_dire_frenzy_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_dire_frenzy_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({ 
                    SPELL_HUNTER_DIRE_FRENZY_DAMAGE,
                    SPELL_HUNTER_BESTIAL_WRATH_LEVEL_BONUS,
                    SPELL_HUNTER_BESTIAL_WRATH
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (Pet* pet = GetCaster()->ToPlayer()->GetPet())
                    pet->m_Events.AddEvent(new DireFrenzyDamageEvent(pet, GetHitUnit()->GetGUID()), pet->m_Events.CalculateTime(100));

                if (GetCaster()->HasAura(SPELL_HUNTER_BESTIAL_WRATH_LEVEL_BONUS))
                    if (SpellEffectInfo const* bestialWrath = sSpellMgr->AssertSpellInfo(SPELL_HUNTER_BESTIAL_WRATH)->GetEffect(EFFECT_2))
                        GetCaster()->GetSpellHistory()->ModifyCooldown(SPELL_HUNTER_BESTIAL_WRATH, -(bestialWrath->CalcValue(GetCaster()) * IN_MILLISECONDS));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_dire_frenzy_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_dire_frenzy_SpellScript();
        }
};

// 781 - Disengage
class spell_hun_legion_disengage : public SpellScriptLoader
{
    public:
        spell_hun_legion_disengage() : SpellScriptLoader("spell_hun_legion_disengage") { }

        class spell_hun_legion_disengage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_disengage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SURVIVAL_OF_THE_FITTEST_TRAIT,
                    SPELL_HUNTER_SURVIVAL_OF_THE_FITTEST_BONUS
                });
            }

            void HandleAfterCast()
            {
                if (AuraEffect* survivalOfTheFittest = GetCaster()->GetAuraEffect(SPELL_HUNTER_SURVIVAL_OF_THE_FITTEST_TRAIT, EFFECT_0))
                    GetCaster()->CastCustomSpell(SPELL_HUNTER_SURVIVAL_OF_THE_FITTEST_BONUS, SPELLVALUE_BASE_POINT0, survivalOfTheFittest->GetAmount(), GetCaster(), TRIGGERED_FULL_MASK);
            }                

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_disengage_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_disengage_SpellScript();
        }
};

// 194859 - Dragonsfire Conflagration
class spell_hun_legion_dragonsfire_conflagration : public SpellScriptLoader
{
    public:
        spell_hun_legion_dragonsfire_conflagration() : SpellScriptLoader("spell_hun_legion_dragonsfire_conflagration") { }

        class spell_hun_legion_dragonsfire_conflagration_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_dragonsfire_conflagration_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetExplTargetUnit());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_dragonsfire_conflagration_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
            }
            private:
                bool _targetsEmpty = false;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_dragonsfire_conflagration_SpellScript();
        }
};

// 194858 - Dragonsfire Grenade
class spell_hun_legion_dragonsfire_grenade : public SpellScriptLoader
{
    public:
        spell_hun_legion_dragonsfire_grenade() : SpellScriptLoader("spell_hun_legion_dragonsfire_grenade") { }

        class spell_hun_legion_dragonsfire_grenade_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_dragonsfire_grenade_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_TITAN_S_THUNDER_PERIODIC_DAMAGE
                });
            }

            void HandlePeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_HUNTER_DRAGONSFIRE_CONFLAGRATION, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_dragonsfire_grenade_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_dragonsfire_grenade_AuraScript();
        }
};

// 202589 - Dragonscale Armor
class spell_hun_legion_dragonscale_armor : public SpellScriptLoader
{
    public:
        spell_hun_legion_dragonscale_armor() : SpellScriptLoader("spell_hun_legion_dragonscale_armor") { }

        class spell_hun_legion_dragonscale_armor_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_dragonscale_armor_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_1)->IsAura())
                    return false;
                return true;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }
            void Absorb(AuraEffect* aurEff, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                absorbAmount = 0;
                if (dmgInfo.GetSchoolMask() & aurEff->GetSpellEffectInfo()->MiscValue)
                    if (dmgInfo.GetDamageType() == DOT)
                        absorbAmount = CalculatePct(dmgInfo.GetDamage(), GetEffect(EFFECT_1)->GetAmount());
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_hun_legion_dragonscale_armor_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_hun_legion_dragonscale_armor_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_dragonscale_armor_AuraScript();
        }
};

// 109304 - Exhilaration
class spell_hun_legion_exhilaration : public SpellScriptLoader
{
    public:
        spell_hun_legion_exhilaration() : SpellScriptLoader("spell_hun_legion_exhilaration") { }

        class spell_hun_legion_exhilaration_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_exhilaration_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_EXIHILARATION_LEVEL_BONUS,
                    SPELL_HUNTER_EXIHILARATION_PET_HEAL
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_EXIHILARATION_LEVEL_BONUS))
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_EXIHILARATION_PET_HEAL, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_exhilaration_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_exhilaration_SpellScript();
        }
};

// 212680 - Explosive Shot
class spell_hun_legion_explosive_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_explosive_shot() : SpellScriptLoader("spell_hun_legion_explosive_shot") { }

        class spell_hun_legion_explosive_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_explosive_shot_SpellScript);

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(GetHitDamage() - (GetHitUnit()->GetDistance(GetHitDest()->GetPosition()) * GetHitDamage() / GetEffectInfo()->CalcRadius(GetCaster())));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_explosive_shot_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_explosive_shot_SpellScript();
        }
};

// 212679 - Explosive Shot: Detonate!
class spell_hun_legion_explosive_shot_detonate : public SpellScriptLoader
{
    public:
        spell_hun_legion_explosive_shot_detonate() : SpellScriptLoader("spell_hun_legion_explosive_shot_detonate") { }

        class spell_hun_legion_explosive_shot_detonate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_explosive_shot_detonate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_EXPLOSIVE_SHOT_DAMAGE,
                    SPELL_HUNTER_EXPLOSIVE_SHOT_OVERRIDE
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (AreaTrigger* at = GetCaster()->GetAreaTrigger(SPELL_HUNTER_EXPLOSIVE_SHOT_OVERRIDE))
                {
                    GetCaster()->RemoveAurasDueToSpell(SPELL_HUNTER_EXPLOSIVE_SHOT_OVERRIDE);
                    at->Remove();
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_explosive_shot_detonate_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_explosive_shot_detonate_SpellScript();
        }
};

// 13812 - Explosive Trap
class spell_hun_legion_explosive_trap_damage : public SpellScriptLoader
{
    public:
        spell_hun_legion_explosive_trap_damage() : SpellScriptLoader("spell_hun_legion_explosive_trap_damage") { }

        class spell_hun_legion_explosive_trap_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_explosive_trap_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_EXPERT_TRAPPER
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                float dist = 100.00f;
                // for expert trapper we need to know who is the initial target this spell doesn't have a explicit target...
                // we cast this spell directly at the position of the initial target lets take the closest target as initial target...
                if (WorldLocation const* dest = GetExplTargetDest())
                {
                    for (WorldObject* target : targets)
                    {
                        if (target->GetExactDist(dest) < dist)
                        {
                            dist = target->GetExactDist(dest);
                            _initialTargetGuid = target->GetGUID();
                        }
                    }
                }
            }                   

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->GetGUID() == _initialTargetGuid)
                    if (AuraEffect* expertTrapper = GetCaster()->GetAuraEffect(SPELL_HUNTER_EXPERT_TRAPPER, EFFECT_0))
                        SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), expertTrapper->GetAmount()));
            }                  

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_explosive_trap_damage_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_explosive_trap_damage_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
            private:
                ObjectGuid _initialTargetGuid;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_explosive_trap_damage_SpellScript();
        }
};

// 199564 - Farstrider
class spell_hun_legion_farstrider : public SpellScriptLoader
{
    public:
        spell_hun_legion_farstrider() : SpellScriptLoader("spell_hun_legion_farstrider") { }

        class spell_hun_legion_farstrider_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_farstrider_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_DISENGAGE,
                    SPELL_HUNTER_HARPOON
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasSpell(SPELL_HUNTER_DISENGAGE))
                    GetCaster()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_DISENGAGE)->ChargeCategoryId);
                else
                    GetCaster()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_HARPOON)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_farstrider_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_farstrider_SpellScript();
        }
};

// 202800 - Flanking Strike
class spell_hun_legion_flanking_strike : public SpellScriptLoader
{
    public:
        spell_hun_legion_flanking_strike() : SpellScriptLoader("spell_hun_legion_flanking_strike") { }

        class spell_hun_legion_flanking_strike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_flanking_strike_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_2))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_FLANKING_STRIKE_PET,
                    SPELL_HUNTER_ASPECT_OF_THE_BEAST,
                    SPELL_HUNTER_BESTIAL_CUNNING,
                    SPELL_HUNTER_BESTIAL_FEROCITY,
                    SPELL_HUNTER_BESTIAL_TENACITY
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (Pet* pet = GetCaster()->ToPlayer()->GetPet())
                {
                    int32 damage = int32((1.5f * (pet->GetBonusDamage() * 2.152f)) * (std::min(pet->getLevel(), uint8(20)) * 0.05f));
                    if (GetHitUnit()->GetVictim() == GetCaster())
                        damage = damage + CalculatePct(damage, GetEffectInfo(EFFECT_2)->CalcValue(GetCaster()));
                    pet->CastCustomSpell(SPELL_HUNTER_FLANKING_STRIKE_PET, SPELLVALUE_BASE_POINT0, damage, GetHitUnit(), TRIGGERED_FULL_MASK);

                    if (GetCaster()->HasAura(SPELL_HUNTER_ASPECT_OF_THE_BEAST))
                    {
                        switch (pet->GetSpecialization())
                        {
                            case TALENT_SPEC_HUNTER_PET_CUNNING:
                                pet->CastSpell(GetHitUnit(), SPELL_HUNTER_BESTIAL_CUNNING, true);
                                break;
                            case TALENT_SPEC_HUNTER_PET_FEROCITY:
                                pet->CastSpell(GetHitUnit(), SPELL_HUNTER_BESTIAL_FEROCITY, true);
                                break;
                            case TALENT_SPEC_HUNTER_PET_TENACITY:
                                pet->CastSpell(pet, SPELL_HUNTER_BESTIAL_TENACITY, true);
                                break;
                            default:
                                break;
                        }
                    }
                }

                if (GetHitUnit()->GetVictim() != GetCaster())
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), GetEffectInfo(EFFECT_2)->CalcValue(GetCaster())));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_flanking_strike_SpellScript::HandleOnEffectHit, EFFECT_1, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_flanking_strike_SpellScript();
        }
};

// 203669 - Fluffy, Go
class spell_hun_legion_fluffy_go : public SpellScriptLoader
{
    public:
        spell_hun_legion_fluffy_go() : SpellScriptLoader("spell_hun_legion_fluffy_go") { }

        class spell_hun_legion_fluffy_go_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_fluffy_go_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void ApplyEffect(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    pet->CastCustomSpell(SPELL_HUNTER_FLUFFY_GO_BONUS, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), pet, TRIGGERED_FULL_MASK);
            }       

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    pet->RemoveAurasDueToSpell(SPELL_HUNTER_FLUFFY_GO_BONUS);
            }                     

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_hun_legion_fluffy_go_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_CHANGE_AMOUNT_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_fluffy_go_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_fluffy_go_AuraScript();
        }
};

// 3355 - Freezing Trap
class spell_hun_legion_freezing_trap : public SpellScriptLoader
{
    public:
        spell_hun_legion_freezing_trap() : SpellScriptLoader("spell_hun_legion_freezing_trap") { }

        class spell_hun_legion_freezing_trap_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_freezing_trap_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_WAYLAY,
                    SPELL_HUNTER_FROZEN_WAKE,
                    SPELL_HUNTER_EXPERT_TRAPPER
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                if (!_breakDelay)
                    return true;

                if (Unit* caster = GetCaster())
                    if (AuraEffect* waylay = caster->GetAuraEffect(SPELL_HUNTER_WAYLAY, EFFECT_1))
                        if (GetDuration() <= (GetMaxDuration() - waylay->GetAmount()))
                            return true;
                return false;
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (AreaTrigger* at = caster->GetAreaTrigger(SPELL_HUNTER_FREEZING_TRAP_AREATRIGGER))
                    {
                        if (AuraEffect* waylay = caster->GetAuraEffect(SPELL_HUNTER_WAYLAY, EFFECT_0))
                            if (at->GetTimeSinceCreated() >= uint32(waylay->GetAmount() * IN_MILLISECONDS))
                                _breakDelay = true;
                        at->Remove();
                    }
                }
            }       

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_HUNTER_EXPERT_TRAPPER))
                        caster->CastSpell(GetTarget(), SPELL_HUNTER_FROZEN_WAKE, true);
            }    

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_freezing_trap_AuraScript::CheckProc);
                OnEffectApply += AuraEffectApplyFn(spell_hun_legion_freezing_trap_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_freezing_trap_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }

            private:
                bool _breakDelay = false;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_freezing_trap_AuraScript();
        }
};

// 203337 - Freezing Trap
class spell_hun_legion_freezing_trap_honor : public SpellScriptLoader
{
    public:
        spell_hun_legion_freezing_trap_honor() : SpellScriptLoader("spell_hun_legion_freezing_trap_honor") { }

        class spell_hun_legion_freezing_trap_honor_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_freezing_trap_honor_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_FROZEN_WAKE,
                    SPELL_HUNTER_EXPERT_TRAPPER
                });
            }   

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_HUNTER_EXPERT_TRAPPER))
                        caster->CastSpell(GetTarget(), SPELL_HUNTER_FROZEN_WAKE, true);
            }    

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_freezing_trap_honor_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_freezing_trap_honor_AuraScript();
        }
};

// 203413 - Fury of the Eagle
class spell_hun_legion_fury_of_the_eagle : public SpellScriptLoader
{
    public:
        spell_hun_legion_fury_of_the_eagle() : SpellScriptLoader("spell_hun_legion_fury_of_the_eagle") { }

        class spell_hun_legion_fury_of_the_eagle_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_fury_of_the_eagle_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MONGOOSE_FURY
                });
            }

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                if (AuraEffect* mongooseFury = GetCaster()->GetAuraEffect(SPELL_HUNTER_MONGOOSE_FURY, EFFECT_0))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), mongooseFury->GetAmount()));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_fury_of_the_eagle_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_fury_of_the_eagle_SpellScript();
        }
};

// 232047 - Hardiness
class spell_hun_legion_hardiness : public SpellScriptLoader
{
    public:
        spell_hun_legion_hardiness() : SpellScriptLoader("spell_hun_legion_hardiness") { }

        class spell_hun_legion_hardiness_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_hardiness_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_1)->IsAura())
                    return false;
                return true;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }
            void Absorb(AuraEffect* aurEff, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                if (GetTarget()->HealthAbovePct(GetEffect(EFFECT_1)->GetAmount()))
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), aurEff->GetBaseAmount());
                else
                    absorbAmount = 0;
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_hun_legion_hardiness_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_hun_legion_hardiness_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_hardiness_AuraScript();
        }
};

// 197344 - Hati's Bond
class spell_hun_legion_hati_s_bond : public SpellScriptLoader
{
    public:
        spell_hun_legion_hati_s_bond() : SpellScriptLoader("spell_hun_legion_hati_s_bond") { }

        class spell_hun_legion_hati_s_bond_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_hati_s_bond_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 1000;
            }

            void HandleCustomTick(AuraEffect const* /*aurEff*/)
            {
                if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()))
                    return;

                if (GetTarget()->ToPlayer()->GetPet())
                {
                    if (Creature* hati = ObjectAccessor::GetCreature(*GetTarget(), _lastKnownHatiGuid))
                    {
                        if (hati->IsAlive())
                            return;

                        hati->DespawnOrUnsummon();
                        // hati respawns 30sec after death
                        GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(30000));
                        return;
                    }

                    bool hatiPetActive = false;
                    for (Unit* summon : GetTarget()->m_Controlled)
                    {
                        if (summon->IsHatiPet())
                        {
                            hatiPetActive = true;
                            _lastKnownHatiGuid = summon->GetGUID();
                            break;
                        }
                    }

                    if (!hatiPetActive)
                        GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_SUMMON_HATI_NORMAL, true);
                }
                else if (Creature* hati = ObjectAccessor::GetCreature(*GetTarget(), _lastKnownHatiGuid))
                    hati->DespawnOrUnsummon();
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Creature* hati = ObjectAccessor::GetCreature(*GetTarget(), _lastKnownHatiGuid))
                    hati->DespawnOrUnsummon();
                else // can only happen during the first two periodic ticks
                {
                    std::set<Unit*> controlled = GetTarget()->m_Controlled;
                    for (Unit* summon : controlled)
                        if (summon->IsHatiPet())
                            summon->ToCreature()->DespawnOrUnsummon();
                }
            }                     

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_hun_legion_hati_s_bond_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_hati_s_bond_AuraScript::HandleCustomTick, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_hun_legion_hati_s_bond_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_hati_s_bond_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
            private:
                ObjectGuid _lastKnownHatiGuid;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_hati_s_bond_AuraScript();
        }
};

// 55709 - Heart of the Phoenix
class spell_hun_legion_heart_of_the_phoenix : public SpellScriptLoader
{
    public:
        spell_hun_legion_heart_of_the_phoenix() : SpellScriptLoader("spell_hun_legion_heart_of_the_phoenix") { }

        class spell_hun_legion_heart_of_the_phoenix_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_heart_of_the_phoenix_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_HEART_OF_THE_PHOENIX_RESURRECT,
                    SPELL_HUNTER_WEAKENED_HEART
                });
            }

            SpellCastResult CheckDebuff()
            {
                if (Unit* owner = GetCaster()->GetOwner(false))
                    if (!owner->HasAura(SPELL_HUNTER_WEAKENED_HEART))
                        return SPELL_CAST_OK;
                return SPELL_FAILED_DONT_REPORT;
            }         

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* owner = GetCaster()->GetOwner(false))
                {
                    owner->CastSpell(owner, SPELL_HUNTER_WEAKENED_HEART, true);
                    owner->CastSpell(GetCaster(), SPELL_HUNTER_HEART_OF_THE_PHOENIX_RESURRECT, true);
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_hun_legion_heart_of_the_phoenix_SpellScript::CheckDebuff);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_heart_of_the_phoenix_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_heart_of_the_phoenix_SpellScript();
        }
};

// 197178 - Hunter's Advantage
class spell_hun_legion_hunter_s_advantage : public SpellScriptLoader
{
    public:
        spell_hun_legion_hunter_s_advantage() : SpellScriptLoader("spell_hun_legion_hunter_s_advantage") { }

        class spell_hun_legion_hunter_s_advantage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_hunter_s_advantage_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_HUNTER_S_ADVANTAGE_BONUS
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcTarget()->IsPet() && eventInfo.GetProcTarget()->GetOwnerGUID() == GetCasterGUID();
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(eventInfo.GetProcTarget(), SPELL_HUNTER_HUNTER_S_ADVANTAGE_BONUS, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_hunter_s_advantage_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_hunter_s_advantage_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_hunter_s_advantage_AuraScript();
        }
};

// 203749 - Hunter's Bounty
class spell_hun_legion_hunter_s_bounty : public SpellScriptLoader
{
    public:
        spell_hun_legion_hunter_s_bounty() : SpellScriptLoader("spell_hun_legion_hunter_s_bounty") { }

        class spell_hun_legion_hunter_s_bounty_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_hunter_s_bounty_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_EXHILARATION
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_HUNTER_EXHILARATION, -(aurEff->GetAmount() * IN_MILLISECONDS));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_hunter_s_bounty_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_hunter_s_bounty_AuraScript();
        }
};

// 191336 - Hunting Companion
class spell_hun_legion_hunting_companion : public SpellScriptLoader
{
    public:
        spell_hun_legion_hunting_companion() : SpellScriptLoader("spell_hun_legion_hunting_companion") { }

        class spell_hun_legion_hunting_companion_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_hunting_companion_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MASTERY_HUNTING_COMPANION,
                    SPELL_HUNTER_ASPECT_OF_THE_EAGLE,
                    SPELL_HUNTER_FLANKING_STRIKE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (Unit* owner = GetTarget()->GetOwner(false))
                {
                    int32 procChance = 0;
                    if (AuraEffect* mastery = owner->GetAuraEffect(SPELL_HUNTER_MASTERY_HUNTING_COMPANION, EFFECT_0))
                        procChance += mastery->GetAmount();
                    
                    if (AuraEffect* aspectOfTheEagle = owner->GetAuraEffect(SPELL_HUNTER_ASPECT_OF_THE_EAGLE, EFFECT_1))
                        procChance += aspectOfTheEagle->GetAmount();

                    if (eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetSpellInfo()->Id == SPELL_HUNTER_FLANKING_STRIKE_PET)
                        procChance *= 2;

                    return roll_chance_i(procChance);
                }
                return false;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_hunting_companion_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_hunting_companion_AuraScript();
        }
};

// 191335 - Hunting Companion
class spell_hun_legion_hunting_companion_proc : public SpellScriptLoader
{
    public:
        spell_hun_legion_hunting_companion_proc() : SpellScriptLoader("spell_hun_legion_hunting_companion_proc") { }

        class spell_hun_legion_hunting_companion_proc_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_hunting_companion_proc_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MONGOOSE_BITE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* owner = GetCaster()->GetOwner(false))
                    owner->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_MONGOOSE_BITE)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_hunting_companion_proc_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_hunting_companion_proc_SpellScript();
        }
};

// 212658 - Hunting Party
class spell_hun_legion_hunting_party : public SpellScriptLoader
{
    public:
        spell_hun_legion_hunting_party() : SpellScriptLoader("spell_hun_legion_hunting_party") { }

        class spell_hun_legion_hunting_party_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_hunting_party_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_EXHILARATION
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_HUNTER_EXHILARATION, -(aurEff->GetAmount() * IN_MILLISECONDS));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_hunting_party_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_hunting_party_AuraScript();
        }
};

// 19577 - Intimidation
class spell_hun_legion_intimidation : public SpellScriptLoader
{
    public:
        spell_hun_legion_intimidation() : SpellScriptLoader("spell_hun_legion_intimidation") { }

        class spell_hun_legion_intimidation_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_intimidation_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_INTIMIDATION_STUN
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->GetVictim())
                    GetHitUnit()->CastSpell(GetHitUnit()->GetVictim(), SPELL_HUNTER_INTIMIDATION_STUN, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_intimidation_SpellScript::HandleOnEffectHit, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_intimidation_SpellScript();
        }
};

class KillCommandCdResetEvent : public BasicEvent
{
    public:
        KillCommandCdResetEvent(Unit* owner) : _owner(owner) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            SpellInfo const* killCommand = sSpellMgr->AssertSpellInfo(SPELL_HUNTER_KILL_COMMAND);
            // don't reset this spell instant - sync it with the recovery time of all spells
            _owner->GetSpellHistory()->ModifyCooldown(SPELL_HUNTER_KILL_COMMAND, (killCommand->StartRecoveryTime - 100/*event execute time*/) - _owner->GetSpellHistory()->GetRemainingCooldown(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_KILL_COMMAND)));
            return true;
        }

    private:
        Unit* _owner;
};

// 34026 - Kill Command 
class spell_hun_legion_kill_command : public SpellScriptLoader
{
    public:
        spell_hun_legion_kill_command() : SpellScriptLoader("spell_hun_legion_kill_command") { }

        class spell_hun_legion_kill_command_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_kill_command_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_KILL_COMMAND_CHARGE,
                    SPELL_HUNTER_ASPECT_OF_THE_BEAST,
                    SPELL_HUNTER_BESTIAL_CUNNING,
                    SPELL_HUNTER_BESTIAL_FEROCITY,
                    SPELL_HUNTER_BESTIAL_TENACITY,
                    SPELL_HUNTER_GO_FOR_THE_THROAT
                });
            }

            void SaveTargetGuid(SpellEffIndex /*effIndex*/)
            {
                _targetGuid = GetHitUnit()->GetGUID();

                if (AuraEffect* goForTheThroat = GetCaster()->GetAuraEffect(SPELL_HUNTER_GO_FOR_THE_THROAT, EFFECT_0))
                    if (GetHitUnit()->HealthBelowPct(goForTheThroat->GetAmount()))
                        GetCaster()->m_Events.AddEvent(new KillCommandCdResetEvent(GetCaster()), GetCaster()->m_Events.CalculateTime(100)); // cooldown isn't set yet we need to delay the reset to the next world tick
            }

            void HandleKillCommand(SpellEffIndex /*effIndex*/)
            {
                int32 baseDamage = 0;
                if (SpellEffectInfo const* killCommandDamageInfo = sSpellMgr->AssertSpellInfo(GetEffectValue())->GetEffect(EFFECT_0))
                    baseDamage = killCommandDamageInfo->CalcValue(GetHitUnit());

                float nerfMod = 0.5f + float(std::min(GetCaster()->getLevel(), uint8(20))) * 0.025f;
                float damage = 1.5f * (baseDamage + (GetCaster()->GetTotalAttackPowerValue(RANGED_ATTACK) * 3)) * nerfMod;
                if (Unit* target = ObjectAccessor::GetUnit(*GetCaster(), _targetGuid))
                {
                    GetHitUnit()->CastSpell(target, SPELL_HUNTER_KILL_COMMAND_CHARGE, true);
                    GetHitUnit()->CastCustomSpell(GetEffectValue(), SPELLVALUE_BASE_POINT0, int32(damage), target, TRIGGERED_FULL_MASK);

                    if (GetCaster()->HasAura(SPELL_HUNTER_MASTER_OF_BEASTS))
                    {
                        for (Unit* summon : GetCaster()->m_Controlled)
                        {
                            if (summon->IsHatiPet())
                            {
                                summon->CastSpell(target, SPELL_HUNTER_KILL_COMMAND_CHARGE, true);
                                summon->CastCustomSpell(GetEffectValue(), SPELLVALUE_BASE_POINT0, int32(damage), target, TRIGGERED_FULL_MASK);
                            }
                        }
                    }

                    if (GetCaster()->HasAura(SPELL_HUNTER_ASPECT_OF_THE_BEAST) && GetHitUnit()->IsPet())
                    {
                        switch (GetHitUnit()->ToPet()->GetSpecialization())
                        {
                            case TALENT_SPEC_HUNTER_PET_CUNNING:
                                GetHitUnit()->CastSpell(target, SPELL_HUNTER_BESTIAL_CUNNING, true);
                                break;
                            case TALENT_SPEC_HUNTER_PET_FEROCITY:
                                GetHitUnit()->CastSpell(target, SPELL_HUNTER_BESTIAL_FEROCITY, true);
                                break;
                            case TALENT_SPEC_HUNTER_PET_TENACITY:
                                GetHitUnit()->CastSpell(GetHitUnit(), SPELL_HUNTER_BESTIAL_TENACITY, true);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_kill_command_SpellScript::SaveTargetGuid, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_kill_command_SpellScript::HandleKillCommand, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }

            private:
                ObjectGuid _targetGuid;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_kill_command_SpellScript();
        }
};

// 83381 - Kill Command
class spell_hun_legion_kill_command_damage : public SpellScriptLoader
{
    public:
        spell_hun_legion_kill_command_damage() : SpellScriptLoader("spell_hun_legion_kill_command_damage") { }

        class spell_hun_legion_kill_command_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_kill_command_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SPIRIT_BOUND_TRAIT,
                    SPELL_HUNTER_SPIRIT_BOUND_HEAL,
                    SPELL_HUNTER_JAWS_OF_THUNDER_TRAIT,
                    SPELL_HUNTER_JAWS_OF_THUNDER_DAMAGE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* owner = GetCaster()->GetOwner(false))
                {
                    if (AuraEffect* spiritBound = owner->GetAuraEffect(SPELL_HUNTER_SPIRIT_BOUND_TRAIT, EFFECT_0))
                        owner->CastCustomSpell(SPELL_HUNTER_SPIRIT_BOUND_HEAL, SPELLVALUE_BASE_POINT0, CalculatePct(GetHitDamage(), spiritBound->GetAmount()), owner, TRIGGERED_FULL_MASK);

                    if (AuraEffect* jawsOfThunder = owner->GetAuraEffect(SPELL_HUNTER_JAWS_OF_THUNDER_TRAIT, EFFECT_0))
                        if (AuraEffect* jawsOfThunderEff1 = owner->GetAuraEffect(SPELL_HUNTER_JAWS_OF_THUNDER_TRAIT, EFFECT_1))
                        if (roll_chance_i(jawsOfThunder->GetAmount()))
                            GetCaster()->CastCustomSpell(SPELL_HUNTER_JAWS_OF_THUNDER_DAMAGE, SPELLVALUE_BASE_POINT0, CalculatePct(GetHitDamage(), jawsOfThunderEff1->GetAmount()), GetHitUnit(), TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_kill_command_damage_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_kill_command_damage_SpellScript();
        }
};

// 199532 - Killer Cobra
class spell_hun_legion_killer_cobra : public SpellScriptLoader
{
    public:
        spell_hun_legion_killer_cobra() : SpellScriptLoader("spell_hun_legion_killer_cobra") { }

        class spell_hun_legion_killer_cobra_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_killer_cobra_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BEAST_CLEAVE_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return GetTarget()->HasAura(SPELL_HUNTER_BESTIAL_WRATH);
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_HUNTER_KILL_COMMAND, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_killer_cobra_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_killer_cobra_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_killer_cobra_AuraScript();
        }
};

// 53478 - Last Stand
class spell_hun_legion_last_stand : public SpellScriptLoader
{
    public:
        spell_hun_legion_last_stand() : SpellScriptLoader("spell_hun_legion_last_stand") { }

        class spell_hun_legion_last_stand_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_last_stand_AuraScript);

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                amount = CalculatePct(int32(GetUnitOwner()->GetMaxHealth()), amount);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_hun_legion_last_stand_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_MOD_INCREASE_HEALTH_2);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_last_stand_AuraScript();
        }
};

class AimedShotEvent : public BasicEvent
{
    public:
        AimedShotEvent(Unit* owner, ObjectGuid targetGuid, int32 repeatCounter) : _owner(owner), _targetGuid(targetGuid), _repeatCounter(repeatCounter) {}

        bool Execute(uint64 time, uint32 /*diff*/) override
        {
            if (Unit* target = ObjectAccessor::GetUnit(*_owner, _targetGuid))
                _owner->CastSpell(target, SPELL_HUNTER_AIMED_SHOT_GOLDEN_TRAIT, true);

            if (--_repeatCounter == 0)
                return true;

            _owner->m_Events.AddEvent(this, time + 100);
            return false;
        }

    private:
        Unit* _owner;
        ObjectGuid _targetGuid;
        int32 _repeatCounter;
};

// 190852 - Legacy of the Windrunners
class spell_hun_legion_legacy_of_the_windrunners : public SpellScriptLoader
{
    public:
        spell_hun_legion_legacy_of_the_windrunners() : SpellScriptLoader("spell_hun_legion_legacy_of_the_windrunners") { }

        class spell_hun_legion_legacy_of_the_windrunners_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_legacy_of_the_windrunners_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_AIMED_SHOT_GOLDEN_TRAIT
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                // unconfirmed
                return !GetTarget()->HasAura(GetAura()->GetSpellEffectInfo(EFFECT_0)->TriggerSpell);
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                GetTarget()->m_Events.AddEvent(new AimedShotEvent(GetTarget(), eventInfo.GetProcTarget()->GetGUID(), aurEff->GetAmount()), GetTarget()->m_Events.CalculateTime(100));
            }

            void Register() override
            {
              //  DoCheckProc += AuraCheckProcFn(spell_hun_legion_legacy_of_the_windrunners_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_legacy_of_the_windrunners_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_legacy_of_the_windrunners_AuraScript();
        }
};

// 155228 - Lone Wolf
class spell_hun_legion_lone_wolf : public SpellScriptLoader
{
    public:
        spell_hun_legion_lone_wolf() : SpellScriptLoader("spell_hun_legion_lone_wolf") { }

        class spell_hun_legion_lone_wolf_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_lone_wolf_AuraScript);

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    pet->Remove(PET_SAVE_AS_CURRENT);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_hun_legion_lone_wolf_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_lone_wolf_AuraScript();
        }
};

// 204219 - Mark of the Windrunner
class spell_hun_legion_mark_of_the_windrunner : public SpellScriptLoader
{
    public:
        spell_hun_legion_mark_of_the_windrunner() : SpellScriptLoader("spell_hun_legion_mark_of_the_windrunner") { }

        class spell_hun_legion_mark_of_the_windrunner_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_mark_of_the_windrunner_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_VULNERABLE
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetCaster()->CastSpell(eventInfo.GetProcTarget(), SPELL_HUNTER_VULNERABLE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_mark_of_the_windrunner_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_mark_of_the_windrunner_AuraScript();
        }
};

// 185901 - Marked Shot
class spell_hun_legion_marked_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_marked_shot() : SpellScriptLoader("spell_hun_legion_marked_shot") { }

        class spell_hun_legion_marked_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_marked_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MARKED_SHOT_DAMAGE,
                    SPELL_HUNTER_HUNTER_S_MARK,
                    SPELL_HUNTER_VULNERABLE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_HUNTER_HUNTER_S_MARK, GetCaster()->GetGUID()));
            }                   

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_MARKED_SHOT_DAMAGE, true);
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_VULNERABLE, true);
            }                  

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_marked_shot_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_marked_shot_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_marked_shot_SpellScript();
        }
};

// 137016 - Marksmanship Hunter
class spell_hun_legion_marksmanship_hunter : public SpellScriptLoader
{
    public:
        spell_hun_legion_marksmanship_hunter() : SpellScriptLoader("spell_hun_legion_marksmanship_hunter") { }

        class spell_hun_legion_marksmanship_hunter_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_marksmanship_hunter_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_AIMED_SHOT_BONUS_BLOCKER
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(eventInfo.GetProcTarget(), SPELL_HUNTER_AIMED_SHOT_BONUS_BLOCKER, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_marksmanship_hunter_AuraScript::HandleEffectProc, EFFECT_FIRST_FOUND, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_marksmanship_hunter_AuraScript();
        }
};

// 53271 - Masters Call
class spell_hun_legion_masters_call : public SpellScriptLoader
{
    public:
        spell_hun_legion_masters_call() : SpellScriptLoader("spell_hun_legion_masters_call") { }

        class spell_hun_legion_masters_call_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_masters_call_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MASTER_S_CALL_PET
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Player* caster = GetCaster()->ToPlayer())
                    if (Pet* pet = caster->GetPet())
                        if (GetHitUnit() != pet)
                        {
                            pet->CastSpell(GetHitUnit(), GetEffectValue(), TriggerCastFlags(TRIGGERED_FULL_MASK & ~TRIGGERED_IGNORE_CASTER_AURASTATE));
                            GetHitUnit()->RemoveMovementImpairingAuras();
                        }
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                GetHitUnit()->CastSpell(GetHitUnit(), SPELL_HUNTER_MASTER_S_CALL_PET, TriggerCastFlags(TRIGGERED_FULL_MASK & ~TRIGGERED_IGNORE_CASTER_AURASTATE));
                GetHitUnit()->RemoveMovementImpairingAuras();
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_masters_call_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_masters_call_SpellScript::HandleScriptEffect, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_masters_call_SpellScript();
        }
};

// 197160 - Mimiron's Shell
class spell_hun_legion_mimiron_s_shell : public SpellScriptLoader
{
    public:
        spell_hun_legion_mimiron_s_shell() : SpellScriptLoader("spell_hun_legion_mimiron_s_shell") { }

        class spell_hun_legion_mimiron_s_shell_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_mimiron_s_shell_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MIMIRON_S_SHELL_PERIODIC
                });
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                int32 heal = CalculatePct(GetTarget()->GetMaxHealth(), aurEff->GetAmount()) / sSpellMgr->AssertSpellInfo(SPELL_HUNTER_MIMIRON_S_SHELL_PERIODIC)->GetMaxTicks(DIFFICULTY_NONE);
                GetTarget()->CastCustomSpell(SPELL_HUNTER_MIMIRON_S_SHELL_PERIODIC, SPELLVALUE_BASE_POINT0, heal, GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_mimiron_s_shell_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_mimiron_s_shell_AuraScript();
        }
};

// 34477 - Misdirection
class spell_hun_legion_misdirection : public SpellScriptLoader
{
    public:
        spell_hun_legion_misdirection() : SpellScriptLoader("spell_hun_legion_misdirection") { }

        class spell_hun_legion_misdirection_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_misdirection_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({ 
                    SPELL_HUNTER_MISDIRECTION_PROC 
                });
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_HUNTER_MISDIRECTION_PROC, true);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->ResetRedirectThreat();

                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE && GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEFAULT)
                    if (Unit* caster = GetCaster())
                        caster->RemoveAurasDueToSpell(SPELL_HUNTER_MISDIRECTION_PROC);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_hun_legion_misdirection_AuraScript::OnApply, EFFECT_1, SPELL_AURA_MOD_SCALE, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_hun_legion_misdirection_AuraScript::OnRemove, EFFECT_1, SPELL_AURA_MOD_SCALE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_misdirection_AuraScript();
        }
};

// 35079 - Misdirection (Proc)
class spell_hun_legion_misdirection_proc : public SpellScriptLoader
{
    public:
        spell_hun_legion_misdirection_proc() : SpellScriptLoader("spell_hun_legion_misdirection_proc") { }

        class spell_hun_legion_misdirection_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_misdirection_proc_AuraScript);

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->ResetRedirectThreat();

                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE && GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEFAULT)
                {
                    std::vector<AuraApplication*> aurApps = GetTarget()->GetTargetAuraApplications(SPELL_HUNTER_MISDIRECTION);
                    for (AuraApplication* aurApp : aurApps)
                        aurApp->GetBase()->Remove();
                }
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_hun_legion_misdirection_proc_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_misdirection_proc_AuraScript();
        }
};

// 201091 - Mortal Wounds
class spell_hun_legion_mortal_wounds : public SpellScriptLoader
{
    public:
        spell_hun_legion_mortal_wounds() : SpellScriptLoader("spell_hun_legion_mortal_wounds") { }

        class spell_hun_legion_mortal_wounds_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_mortal_wounds_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MONGOOSE_BITE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_MONGOOSE_BITE)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_mortal_wounds_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_mortal_wounds_SpellScript();
        }
};

// 2643 - Multi-Shot
class spell_hun_legion_multi_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_multi_shot() : SpellScriptLoader("spell_hun_legion_multi_shot") { }

        class spell_hun_legion_multi_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_multi_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BEAST_CLEAVE_PASSIVE,
                    SPELL_HUNTER_BEAST_CLEAVE_PET,
                    SPELL_HUNTER_AIMED_SHOT,
                    SPELL_HUNTER_MULTI_SHOT_ENERGIZE,
                    SPELL_HUNTER_HUNTER_S_MARK,
                    SPELL_HUNTER_TRUESHOT
                });
            }

            void CountTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = uint32(targets.size());
            }                   

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_BEAST_CLEAVE_PASSIVE))
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_BEAST_CLEAVE_PET, true);

                if (GetCaster()->HasAura(SPELL_HUNTER_MASTER_OF_BEASTS))
                    for (Unit* summon : GetCaster()->m_Controlled)
                        if (summon->IsHatiPet())
                            GetCaster()->AddAura(SPELL_HUNTER_BEAST_CLEAVE_PET, summon);

                if (GetCaster()->HasSpell(SPELL_HUNTER_AIMED_SHOT))
                    if (SpellEffectInfo const* energizeEff0Info = sSpellMgr->AssertSpellInfo(SPELL_HUNTER_MULTI_SHOT_ENERGIZE)->GetEffect(EFFECT_0))
                        GetCaster()->CastCustomSpell(SPELL_HUNTER_MULTI_SHOT_ENERGIZE, SPELLVALUE_BASE_POINT0, energizeEff0Info->CalcValue(GetCaster()) * _targetCount, GetCaster(), TRIGGERED_FULL_MASK);
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_TRUESHOT) || GetCaster()->HasAura(SPELL_HUNTER_MARKING_TARGETS))
                {
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_HUNTER_S_MARK_CASTER, true);
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_HUNTER_S_MARK, true);
                }
            }                  

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_multi_shot_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                AfterCast += SpellCastFn(spell_hun_legion_multi_shot_SpellScript::HandleAfterCast);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_multi_shot_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }

            private:
                uint32 _targetCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_multi_shot_SpellScript();
        }
};

// 190928 - Mongoose Bite
class spell_hun_legion_mongoose_bite : public SpellScriptLoader
{
    public:
        spell_hun_legion_mongoose_bite() : SpellScriptLoader("spell_hun_legion_mongoose_bite") { }

        class spell_hun_legion_mongoose_bite_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_mongoose_bite_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MONGOOSE_FURY
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (Aura* mongooseFury = GetCaster()->GetAura(SPELL_HUNTER_MONGOOSE_FURY))
                {
                    if (mongooseFury->GetStackAmount() < mongooseFury->CalcMaxStackAmount(GetCaster()))
                        mongooseFury->SetStackAmount(mongooseFury->GetStackAmount() + 1); // don't use modstackamount we DONT refresh the duration!
                }
                else
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_MONGOOSE_FURY, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_mongoose_bite_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_mongoose_bite_SpellScript();
        }
};

// 197138 - Natural Reflexes
class spell_hun_legion_natural_reflexes : public SpellScriptLoader
{
    public:
        spell_hun_legion_natural_reflexes() : SpellScriptLoader("spell_hun_legion_natural_reflexes") { }

        class spell_hun_legion_natural_reflexes_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_natural_reflexes_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                // should affect the caster too
                targets.push_back(GetCaster());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_natural_reflexes_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_MINIONS);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_natural_reflexes_SpellScript();
        }
};

// 204081 - On the Trail
class spell_hun_legion_on_the_trail : public SpellScriptLoader
{
    public:
        spell_hun_legion_on_the_trail() : SpellScriptLoader("spell_hun_legion_on_the_trail") { }

        class spell_hun_legion_on_the_trail_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_on_the_trail_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetActor() && eventInfo.GetActor()->GetGUID() == GetCasterGUID();
            }   

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetAura()->SetDuration(std::min(GetAura()->GetDuration() + 6000, GetAura()->GetMaxDuration()));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_on_the_trail_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_on_the_trail_AuraScript::HandleEffectProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_on_the_trail_AuraScript();
        }
};

// 198670 - Piercing Shot
class spell_hun_legion_piercing_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_piercing_shot() : SpellScriptLoader("spell_hun_legion_piercing_shot") { }

        class spell_hun_legion_piercing_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_piercing_shot_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.clear();
                targets.push_back(GetExplTargetUnit());
            }

            void SetDest(SpellDestination& dest)
            {
                dest.Relocate(GetCaster()->GetPosition());
            }               

            void FilterTargetsInLine(std::list<WorldObject*>& targets)
            {
                targets.remove(GetExplTargetUnit());
            }

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit() == GetExplTargetUnit())
                    GetCaster()->CastCustomSpell(SPELL_HUNTER_PIERCING_SHOT_DAMAGE, SPELLVALUE_BASE_POINT1, sSpellMgr->AssertSpellInfo(SPELL_HUNTER_PIERCING_SHOT_DAMAGE)->GetEffect(EFFECT_1)->CalcValue(GetCaster()) * 2, GetHitUnit(), TRIGGERED_FULL_MASK);
                else
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_PIERCING_SHOT_DAMAGE, true);
            }

            void DisableDamage(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(0);
            }

            void Register() override
            {
                // nope, no joke...
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_piercing_shot_SpellScript::FilterTargets, EFFECT_3, TARGET_UNIT_DEST_AREA_ENEMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_piercing_shot_SpellScript::FilterTargets, EFFECT_4, TARGET_UNIT_DEST_AREA_ENEMY);
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_hun_legion_piercing_shot_SpellScript::SetDest, EFFECT_1, TARGET_DEST_TARGET_ENEMY);
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_hun_legion_piercing_shot_SpellScript::SetDest, EFFECT_2, TARGET_DEST_TARGET_ENEMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_piercing_shot_SpellScript::FilterTargetsInLine, EFFECT_1, TARGET_UNIT_ENEMY_IN_LINE);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_piercing_shot_SpellScript::FilterTargetsInLine, EFFECT_2, TARGET_UNIT_ENEMY_IN_LINE);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_piercing_shot_SpellScript::HandleDamage, EFFECT_2, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_piercing_shot_SpellScript::HandleDamage, EFFECT_4, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_piercing_shot_SpellScript::DisableDamage, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_piercing_shot_SpellScript::DisableDamage, EFFECT_2, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_piercing_shot_SpellScript::DisableDamage, EFFECT_3, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_piercing_shot_SpellScript::DisableDamage, EFFECT_4, SPELL_EFFECT_NORMALIZED_WEAPON_DMG);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_piercing_shot_SpellScript();
        }
};

// 781 - Disengage
// 190925 - Harpoon
class spell_hun_legion_posthaste : public SpellScriptLoader
{
    public:
        spell_hun_legion_posthaste() : SpellScriptLoader("spell_hun_legion_posthaste") { }

        class spell_hun_legion_posthaste_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_posthaste_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_POSTHASTE_TALENT,
                    SPELL_HUNTER_POSTHASTE_BONUS
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_HUNTER_POSTHASTE_TALENT))
                {
                    GetCaster()->RemoveMovementImpairingAuras();
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_POSTHASTE_BONUS, true);
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_posthaste_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_posthaste_SpellScript();
        }
};

// 53480 - Roar of Sacrifice
class spell_hun_legion_roar_of_sacrifice : public SpellScriptLoader
{
    public:
        spell_hun_legion_roar_of_sacrifice() : SpellScriptLoader("spell_hun_legion_roar_of_sacrifice") { }

        class spell_hun_legion_roar_of_sacrifice_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_roar_of_sacrifice_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_BEAST_CLEAVE_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage() > 0;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
                if (Unit* caster = GetCaster())
                    GetTarget()->CastCustomSpell(SPELL_HUNTER_ROAR_OF_SACRIFICE_DAMAGE, SPELLVALUE_BASE_POINT0, damage, caster, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_roar_of_sacrifice_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_roar_of_sacrifice_AuraScript::HandleEffectProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_roar_of_sacrifice_AuraScript();
        }
};

// 213691 - Scatter Shot
class spell_hun_legion_scatter_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_scatter_shot() : SpellScriptLoader("spell_hun_legion_scatter_shot") { }

        class spell_hun_legion_scatter_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_scatter_shot_SpellScript);

            void HandleAfterCast()
            {
                // A short-range shot that deals $s1% weapon damage, removes all harmful damage over time effects, and incapacitating the target for $d.  Any damage caused will remove the effect.  Turns off your attack when used.
                std::list<Aura*> aurasToRemove;
                for (AuraEffect* aurEff : GetCaster()->GetAuraEffectsByType(SPELL_AURA_PERIODIC_DAMAGE))
                    if (aurEff->GetSpellInfo()->Dispel == DISPEL_MAGIC)
                        if (std::find(aurasToRemove.begin(), aurasToRemove.end(), aurEff->GetBase()) == aurasToRemove.end())
                            aurasToRemove.push_back(aurEff->GetBase());

                for (AuraEffect* aurEff : GetCaster()->GetAuraEffectsByType(SPELL_AURA_PERIODIC_LEECH))
                    if (aurEff->GetSpellInfo()->Dispel == DISPEL_MAGIC)
                        if (std::find(aurasToRemove.begin(), aurasToRemove.end(), aurEff->GetBase()) == aurasToRemove.end())
                            aurasToRemove.push_back(aurEff->GetBase());

                for (AuraEffect* aurEff : GetCaster()->GetAuraEffectsByType(SPELL_AURA_PERIODIC_DAMAGE_PERCENT))
                    if (aurEff->GetSpellInfo()->Dispel == DISPEL_MAGIC)
                        if (std::find(aurasToRemove.begin(), aurasToRemove.end(), aurEff->GetBase()) == aurasToRemove.end())
                            aurasToRemove.push_back(aurEff->GetBase());

                for (Aura* aura : aurasToRemove)
                    aura->Remove(AURA_REMOVE_BY_ENEMY_SPELL);
            }                     

            void Register() override
            {
                AfterCast += SpellCastFn(spell_hun_legion_scatter_shot_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_scatter_shot_SpellScript();
        }
};

// 206817 - Sentinel
class spell_hun_legion_sentinel : public SpellScriptLoader
{
    public:
        spell_hun_legion_sentinel() : SpellScriptLoader("spell_hun_legion_sentinel") { }

        class spell_hun_legion_sentinel_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_sentinel_SpellScript);

            void HandleEffect(SpellEffIndex /*effIndex*/)
            {
                int32 duration = GetSpellInfo()->CalcDuration(GetCaster());
                AreaTrigger* areaTrigger = new AreaTrigger();
                if (!areaTrigger->CreateAreaTrigger(GetEffectInfo()->MiscValue, GetCaster(), nullptr, GetSpellInfo(), GetHitDest()->GetPosition(), duration, GetSpell()->m_SpellVisual, GetSpell()->m_castId))
                    delete areaTrigger;
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_hun_legion_sentinel_SpellScript::HandleEffect, EFFECT_1, SPELL_EFFECT_CREATE_AREATRIGGER);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_sentinel_SpellScript();
        }
};

// 213875 - Separation Anxiety
class spell_hun_legion_separation_anxiety : public SpellScriptLoader
{
    public:
        spell_hun_legion_separation_anxiety() : SpellScriptLoader("spell_hun_legion_separation_anxiety") { }

        class spell_hun_legion_separation_anxiety_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_separation_anxiety_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SEPARATION_ANXIETY_BONUS
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* owner = caster->GetOwner(false))
                        return eventInfo.GetActor() == owner;
                return false;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                if (Unit* caster = GetCaster())
                    caster->RemoveAurasDueToSpell(SPELL_HUNTER_SEPARATION_ANXIETY_BONUS);
                Remove();
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_HUNTER_SEPARATION_ANXIETY_BONUS, true);
            }                  

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_separation_anxiety_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_separation_anxiety_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_separation_anxiety_AuraScript::HandleDummyTick, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_separation_anxiety_AuraScript();
        }
};

// 186270 - Raptor Strike
// 187708 - Carve
class spell_hun_legion_serpent_sting : public SpellScriptLoader
{
    public:
        spell_hun_legion_serpent_sting() : SpellScriptLoader("spell_hun_legion_serpent_sting") { }

        class spell_hun_legion_serpent_sting_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_serpent_sting_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SERPENT_STING_TALENT,
                    SPELL_HUNTER_SERPENT_STING_DEBUFF
                });
            }

            bool Load() override
            {
                return GetCaster()->HasSpell(SPELL_HUNTER_SERPENT_STING_TALENT);
            }

            void HandleHit()
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_SERPENT_STING_DEBUFF, true);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_hun_legion_serpent_sting_SpellScript::HandleHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_serpent_sting_SpellScript();
        }
};

// 214579 - Sidewinders
class spell_hun_legion_sidewinders : public SpellScriptLoader
{
    public:
        spell_hun_legion_sidewinders() : SpellScriptLoader("spell_hun_legion_sidewinders") { }

        class spell_hun_legion_sidewinders_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_sidewinders_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SIDEWINDERS_DAMAGE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->SendPlaySpellVisual(GetHitUnit()->GetPosition(), GetCaster()->GetOrientation(), SPELL_VISUAL_SIDEWINDERS, 0, 0, TRAVEL_SPEED_SIDEWINDERS, false);
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                Unit* mainTarget = GetExplTargetUnit();
                if (!mainTarget)
                    return;

                // range where Sidewinders can hit side targets increases with distance
                // 6 yds wide at 0 yd distance, 10 yds wide at 45 yds distance
                float dist = std::min(caster->GetDistance(mainTarget->GetPosition()), 45.f);
                float width = 10.f * (0.6f + ((dist / 45.f) * 0.4f));

                targets.remove_if([caster, mainTarget, width, dist](WorldObject const* obj)
                {
                    Unit const* target = obj->ToUnit();
                    if (!target)
                        return true;

                    return (!caster->HasInLine(target, width) || caster->GetDistance(target) > (dist + 4.f)) && target != mainTarget;
                });
            }                      

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_SIDEWINDERS_DAMAGE, true);
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_VULNERABLE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_sidewinders_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_sidewinders_SpellScript::FilterTargets, EFFECT_2, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_sidewinders_SpellScript::HandleDamage, EFFECT_2, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_sidewinders_SpellScript();
        }
};

// 201078 - Snake Hunter
class spell_hun_legion_snake_hunter : public SpellScriptLoader
{
    public:
        spell_hun_legion_snake_hunter() : SpellScriptLoader("spell_hun_legion_snake_hunter") { }

        class spell_hun_legion_snake_hunter_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_snake_hunter_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_MONGOOSE_BITE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->GetSpellHistory()->ResetCharges(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_MONGOOSE_BITE)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_snake_hunter_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_snake_hunter_SpellScript();
        }
};

// 232045 - Sparring
class spell_hun_legion_sparring : public SpellScriptLoader
{
    public:
        spell_hun_legion_sparring() : SpellScriptLoader("spell_hun_legion_sparring") { }

        class spell_hun_legion_sparring_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_sparring_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_1)->IsAura())
                    return false;
                return true;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }
            void Absorb(AuraEffect* aurEff, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                absorbAmount = 0;
                // Physical melee attacks have a $s1% chance to be blunted, dealing $s2% reduced damage.
                if (dmgInfo.GetSchoolMask() & aurEff->GetSpellEffectInfo()->MiscValue)
                    if (dmgInfo.GetDamageType() == DIRECT_DAMAGE && roll_chance_i(aurEff->GetAmount()))
                        absorbAmount = CalculatePct(dmgInfo.GetDamage(), GetEffect(EFFECT_1)->GetAmount());
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_hun_legion_sparring_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_hun_legion_sparring_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_sparring_AuraScript();
        }
};

// 202914 - Spider Sting
class spell_hun_legion_spider_sting : public SpellScriptLoader
{
    public:
        spell_hun_legion_spider_sting() : SpellScriptLoader("spell_hun_legion_spider_sting") { }

        class spell_hun_legion_spider_sting_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_spider_sting_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SPIDER_STING_SILENCE
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_HUNTER_SPIDER_STING_SILENCE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_spider_sting_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
            private:
                uint32 _lastKnownProcSpellId = 0;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_spider_sting_AuraScript();
        }
};

// 201631 - Stampede
class spell_hun_legion_stampede_periodic : public SpellScriptLoader
{
    public:
        spell_hun_legion_stampede_periodic() : SpellScriptLoader("spell_hun_legion_stampede_periodic") { }

        class spell_hun_legion_stampede_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_stampede_periodic_AuraScript);

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->CastSpell(caster, aurEff->GetSpellEffectInfo()->TriggerSpell, true);
                    if (AuraEffect* eff1 = GetEffect(EFFECT_1))
                        caster->CastSpell(caster, eff1->GetSpellEffectInfo()->TriggerSpell, true);
                }
            }                  

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_stampede_periodic_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_stampede_periodic_AuraScript();
        }
};

// 193533 - Steady Focus
class spell_hun_legion_steady_focus : public SpellScriptLoader
{
    public:
        spell_hun_legion_steady_focus() : SpellScriptLoader("spell_hun_legion_steady_focus") { }

        class spell_hun_legion_steady_focus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_steady_focus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SIDEWINDERS,
                    SPELL_HUNTER_ARCANE_SHOT,
                    SPELL_HUNTER_MULTI_SHOT
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo()->Id == SPELL_HUNTER_SIDEWINDERS || eventInfo.GetSpellInfo()->Id == SPELL_HUNTER_ARCANE_SHOT || eventInfo.GetSpellInfo()->Id == SPELL_HUNTER_MULTI_SHOT)
                    if (_lastKnownProcSpellId == eventInfo.GetSpellInfo()->Id)
                        return true;

                _lastKnownProcSpellId = eventInfo.GetSpellInfo()->Id;
                return false;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_STEADY_FOCUS_BONUS, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_steady_focus_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_steady_focus_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
            private:
                uint32 _lastKnownProcSpellId = 0;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_steady_focus_AuraScript();
        }
};

// 162480 - Steel Trap
class spell_hun_legion_steel_trap_root : public SpellScriptLoader
{
    public:
        spell_hun_legion_steel_trap_root() : SpellScriptLoader("spell_hun_legion_steel_trap_root") { }

        class spell_hun_legion_steel_trap_root_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_steel_trap_root_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_STEEL_TRAP_PERIODIC,
                    SPELL_HUNTER_STEEL_TRAP_EXPERT_PERIODIC
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (eventInfo.GetSpellInfo())
                    if (eventInfo.GetSpellInfo()->Id == SPELL_HUNTER_STEEL_TRAP_PERIODIC || eventInfo.GetSpellInfo()->Id == SPELL_HUNTER_STEEL_TRAP_EXPERT_PERIODIC)
                        return false;
                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_steel_trap_root_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_steel_trap_root_AuraScript();
        }
};

// 191241 - Sticky Bomb
class spell_hun_legion_sticky_bomb : public SpellScriptLoader
{
    public:
        spell_hun_legion_sticky_bomb() : SpellScriptLoader("spell_hun_legion_sticky_bomb") { }

        class spell_hun_legion_sticky_bomb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_sticky_bomb_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_STICKY_BOMB_KNOCK_BACK
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget()->GetPositionX(), GetTarget()->GetPositionY(), GetTarget()->GetPositionZ(), SPELL_HUNTER_STICKY_BOMB_KNOCK_BACK, true);
            }                  

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_sticky_bomb_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_sticky_bomb_AuraScript();
        }
};

// 201754 - Stomp
class spell_hun_legion_stomp : public SpellScriptLoader
{
    public:
        spell_hun_legion_stomp() : SpellScriptLoader("spell_hun_legion_stomp") { }

        class spell_hun_legion_stomp_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_stomp_SpellScript);

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                // When your Dire Beasts charge in, they will stomp the ground, dealing ${($76657m1/100+1)*($201754s1)*1.4} Physical damage to all nearby enemies.
                SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), 40));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_stomp_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_stomp_SpellScript();
        }
};

// 197354 - Surge of the Stormgod
class spell_hun_legion_surge_of_the_stormgod : public SpellScriptLoader
{
    public:
        spell_hun_legion_surge_of_the_stormgod() : SpellScriptLoader("spell_hun_legion_surge_of_the_stormgod") { }

        class spell_hun_legion_surge_of_the_stormgod_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_surge_of_the_stormgod_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_SURGE_OF_THE_STORMGOD_DAMAGE,
                    SPELL_HUNTER_SURGE_OF_THE_STORMGOD_VISUAL
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                int32 damage = int32(GetTarget()->GetTotalAttackPowerValue(RANGED_ATTACK) * sSpellMgr->AssertSpellInfo(SPELL_HUNTER_SURGE_OF_THE_STORMGOD_VISUAL)->GetEffect(EFFECT_0)->BonusCoefficientFromAP);
                for (Unit* summon : GetTarget()->m_Controlled)
                {
                    // Only target main pet and hati
                    if (summon->HasUnitTypeMask(UNIT_MASK_PET) || summon->IsHatiPet())
                    {
                        summon->CastSpell(summon, SPELL_HUNTER_SURGE_OF_THE_STORMGOD_VISUAL, true);
                        GetTarget()->CastCustomSpell(SPELL_HUNTER_SURGE_OF_THE_STORMGOD_DAMAGE, SPELLVALUE_BASE_POINT0, damage, summon, TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_surge_of_the_stormgod_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_surge_of_the_stormgod_AuraScript();
        }
};

// 164857 - Survivalist
class spell_hun_legion_survivalist : public SpellScriptLoader
{
    public:
        spell_hun_legion_survivalist() : SpellScriptLoader("spell_hun_legion_survivalist") { }

        class spell_hun_legion_survivalist_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_survivalist_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                // should affect the caster too
                targets.push_back(GetCaster());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_survivalist_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_MINIONS);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_survivalist_SpellScript();
        }
};

// 203563 - Talon Strike
class spell_hun_legion_talon_strike : public SpellScriptLoader
{
    public:
        spell_hun_legion_talon_strike() : SpellScriptLoader("spell_hun_legion_talon_strike") { }

        class spell_hun_legion_talon_strike_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_talon_strike_AuraScript);

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(eventInfo.GetProcTarget(), aurEff->GetSpellEffectInfo()->TriggerSpell, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_talon_strike_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_talon_strike_AuraScript();
        }
};

// 203754 - Terms of Engagement
class spell_hun_legion_terms_of_engagement : public SpellScriptLoader
{
    public:
        spell_hun_legion_terms_of_engagement() : SpellScriptLoader("spell_hun_legion_terms_of_engagement") { }

        class spell_hun_legion_terms_of_engagement_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_terms_of_engagement_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_HARPOON
                });
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_HARPOON)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_terms_of_engagement_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_terms_of_engagement_AuraScript();
        }
};

class ThrowingAxesEvent : public BasicEvent
{
    public:
        ThrowingAxesEvent(Unit* owner, ObjectGuid targetGuid, int32 repeatCounter) : _owner(owner), _targetGuid(targetGuid), _repeatCounter(repeatCounter) {}

        bool Execute(uint64 time, uint32 /*diff*/) override
        {
            if (Unit* target = ObjectAccessor::GetUnit(*_owner, _targetGuid))
                _owner->CastSpell(target, SPELL_HUNTER_THROWING_AXES_DAMAGE, true);

            if (--_repeatCounter == 0)
                return true;

            _owner->m_Events.AddEvent(this, time + 300);
            return false;
        }

    private:
        Unit* _owner;
        ObjectGuid _targetGuid;
        int32 _repeatCounter;
};

// 200163 - Throwing Axes
class spell_hun_legion_throwing_axes : public SpellScriptLoader
{
    public:
        spell_hun_legion_throwing_axes() : SpellScriptLoader("spell_hun_legion_throwing_axes") { }

        class spell_hun_legion_throwing_axes_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_throwing_axes_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({ 
                    SPELL_HUNTER_THROWING_AXES_DAMAGE
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->m_Events.AddEvent(new ThrowingAxesEvent(GetCaster(), GetHitUnit()->GetGUID(), GetEffectValue()), GetCaster()->m_Events.CalculateTime(0));
            }

            void Register() override
            {
                // dont change that to OnEffectHitTarget - spell has speed 20 which is wrong first axe should be casted instant after cast
                OnEffectLaunchTarget += SpellEffectFn(spell_hun_legion_throwing_axes_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_throwing_axes_SpellScript();
        }
};

// 207081 Titan's Thunder
class spell_hun_legion_titan_s_thunder : public SpellScriptLoader
{
    public:
        spell_hun_legion_titan_s_thunder() : SpellScriptLoader("spell_hun_legion_titan_s_thunder") { }

        class spell_hun_legion_titan_s_thunder_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_titan_s_thunder_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_TITAN_S_THUNDER_PERIODIC,
                    SPELL_HUNTER_TITAN_S_THUNDER_DIRE_FRENZY,
                    SPELL_HUNTER_MASTER_OF_BEASTS
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            SpellCastResult CheckPets()
            {
                if (!GetCaster()->ToPlayer()->GetPet())
                    return SPELL_FAILED_NO_PET;

                return SPELL_CAST_OK;
            }                      

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    if (!target->ToUnit()->IsPet() && !target->ToUnit()->IsDireBeastSummon() && !target->ToUnit()->IsHatiPet())
                        return true;

                    return target->ToUnit()->GetOwnerGUID() != caster->GetGUID();
                });
            }

            void HandleHonorTalent(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_TITAN_S_THUNDER_PERIODIC, true);
                
                if (GetCaster()->HasSpell(SPELL_HUNTER_DIRE_FRENZY))
                    if (GetHitUnit()->IsPet() || (GetCaster()->HasAura(SPELL_HUNTER_MASTER_OF_BEASTS) && GetHitUnit()->IsHatiPet()))
                        GetCaster()->CastSpell(GetHitUnit(), SPELL_HUNTER_TITAN_S_THUNDER_DIRE_FRENZY, true);
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_hun_legion_titan_s_thunder_SpellScript::CheckPets);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_titan_s_thunder_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_titan_s_thunder_SpellScript::HandleHonorTalent, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_titan_s_thunder_SpellScript();
        }
};

// 207094 - Titan's Thunder
class spell_hun_legion_titan_s_thunder_periodic : public SpellScriptLoader
{
    public:
        spell_hun_legion_titan_s_thunder_periodic() : SpellScriptLoader("spell_hun_legion_titan_s_thunder_periodic") { }

        class spell_hun_legion_titan_s_thunder_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_titan_s_thunder_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_TITAN_S_THUNDER_PERIODIC_DAMAGE
                });
            }

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (GetTarget()->GetVictim())
                    GetTarget()->CastSpell(GetTarget()->GetVictim(), SPELL_HUNTER_TITAN_S_THUNDER_PERIODIC_DAMAGE, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_titan_s_thunder_periodic_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_titan_s_thunder_periodic_AuraScript();
        }
};

// 199921 - Trailblazer
class spell_hun_legion_trailblazer : public SpellScriptLoader
{
    public:
        spell_hun_legion_trailblazer() : SpellScriptLoader("spell_hun_legion_trailblazer") { }

        class spell_hun_legion_trailblazer_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_trailblazer_AuraScript);

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 1000;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->RemoveAurasDueToSpell(SPELL_HUNTER_TRAILBLAZER_BUFF);
                GetTarget()->GetSpellHistory()->AddCooldown(GetId(), 0, std::chrono::milliseconds(aurEff->GetAmount()));
            }                  

            void HandleCustomTick(AuraEffect const* /*aurEff*/)
            {
                if (GetTarget()->GetSpellHistory()->HasCooldown(GetId()) || GetTarget()->HasAura(SPELL_HUNTER_TRAILBLAZER_BUFF))
                    return;
                
                GetTarget()->CastSpell(GetTarget(), SPELL_HUNTER_TRAILBLAZER_BUFF, true);
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_HUNTER_TRAILBLAZER_BUFF);
            }                     

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_trailblazer_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_hun_legion_trailblazer_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_trailblazer_AuraScript::HandleCustomTick, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_hun_legion_trailblazer_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_trailblazer_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_trailblazer_AuraScript();
        }
};

// 199885 - Trick Shot
class spell_hun_legion_trick_shot : public SpellScriptLoader
{
    public:
        spell_hun_legion_trick_shot() : SpellScriptLoader("spell_hun_legion_trick_shot") { }

        class spell_hun_legion_trick_shot_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_trick_shot_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_VULNERABLE,
                    SPELL_HUNTER_AIMED_SHOT,
                    SPELL_HUNTER_TRICK_SHOT_BONUS
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetExplTargetUnit());
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_HUNTER_VULNERABLE, GetCaster()->GetGUID()));
                _targetsEmpty = targets.empty();
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastCustomSpell(SPELL_HUNTER_AIMED_SHOT, SPELLVALUE_CUSTOM_FLAGS, CUSTOM_CAST_FLAG_SPECIAL_CALCULATION, GetHitUnit(), TRIGGERED_FULL_MASK);
            }                    

            void HandleAfterCast()
            {
                if (_targetsEmpty)
                    GetCaster()->CastSpell(GetCaster(), SPELL_HUNTER_TRICK_SHOT_BONUS, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_hun_legion_trick_shot_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_hun_legion_trick_shot_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_hun_legion_trick_shot_SpellScript::HandleAfterCast);
            }
            private:
                bool _targetsEmpty = false;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_trick_shot_SpellScript();
        }
};

// 63900 - Thunderstomp
class spell_hun_legion_thunderstomp : public SpellScriptLoader
{
    public:
        spell_hun_legion_thunderstomp() : SpellScriptLoader("spell_hun_legion_thunderstomp") { }

        class spell_hun_legion_thunderstomp_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_thunderstomp_SpellScript);

            bool Load() override
            {
                return GetCaster()->IsPet();
            }

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                SetEffectValue(GetEffectValue() + int32(GetCaster()->ToPet()->GetBonusDamage() * 0.250f));
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_hun_legion_thunderstomp_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_thunderstomp_SpellScript();
        }
};

// 202797 - Viper Sting
class spell_hun_legion_viper_sting : public SpellScriptLoader
{
    public:
        spell_hun_legion_viper_sting() : SpellScriptLoader("spell_hun_legion_viper_sting") { }

        class spell_hun_legion_viper_sting_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_viper_sting_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetCastTime() > 0;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_hun_legion_viper_sting_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_viper_sting_AuraScript();
        }
};

// 194386 - Volley
class spell_hun_legion_volley : public SpellScriptLoader
{
    public:
        spell_hun_legion_volley() : SpellScriptLoader("spell_hun_legion_volley") { }

        class spell_hun_legion_volley_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_volley_AuraScript);

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(eventInfo.GetProcTarget(), aurEff->GetSpellEffectInfo()->TriggerSpell, TriggerCastFlags(TRIGGERED_FULL_MASK & ~TRIGGERED_IGNORE_POWER_AND_REAGENT_COST));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_hun_legion_volley_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_volley_AuraScript();
        }
};

// 187131 - Vulnerable
class spell_hun_legion_vulnerable : public SpellScriptLoader
{
    public:
        spell_hun_legion_vulnerable() : SpellScriptLoader("spell_hun_legion_vulnerable") { }

        class spell_hun_legion_vulnerable_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_vulnerable_AuraScript);

            void HandleDummyTick(AuraEffect const* aurEff)
            {
                if (AuraEffect* dmgBonus = GetEffect(EFFECT_1))
                    dmgBonus->ChangeAmount(dmgBonus->GetAmount() + aurEff->GetAmount());
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                if (Unit* caster = GetCaster())
                    if (AuraEffect* markedForDeath = caster->GetAuraEffect(SPELL_HUNTER_MARKED_FOR_DEATH, EFFECT_0))
                        amount += markedForDeath->GetAmount();
                const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
            }                                    

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_hun_legion_vulnerable_AuraScript::HandleDummyTick, EFFECT_3, SPELL_AURA_PERIODIC_DUMMY);
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_hun_legion_vulnerable_AuraScript::CalculateAmount, EFFECT_2, SPELL_AURA_MOD_CRIT_CHANCE_FOR_CASTER);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_vulnerable_AuraScript();
        }
};

// 185791 - Wild Call
class spell_hun_legion_wild_call : public SpellScriptLoader
{
    public:
        spell_hun_legion_wild_call() : SpellScriptLoader("spell_hun_legion_wild_call") { }

        class spell_hun_legion_wild_call_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_wild_call_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_DIRE_BEAST,
                    SPELL_HUNTER_DIRE_FRENZY
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget()->HasSpell(SPELL_HUNTER_DIRE_BEAST))
                    GetTarget()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_DIRE_BEAST)->ChargeCategoryId);
                else
                    GetTarget()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_HUNTER_DIRE_FRENZY)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_hun_legion_wild_call_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_wild_call_AuraScript();
        }
};

// 204190 - Wild Protector
class spell_hun_legion_wild_protector : public SpellScriptLoader
{
    public:
        spell_hun_legion_wild_protector() : SpellScriptLoader("spell_hun_legion_wild_protector") { }

        class spell_hun_legion_wild_protector_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hun_legion_wild_protector_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HUNTER_WILD_PROTECTOR_AREATRIGGER
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    pet->CastSpell(pet, SPELL_HUNTER_WILD_PROTECTOR_AREATRIGGER, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    pet->RemoveAurasDueToSpell(SPELL_HUNTER_WILD_PROTECTOR_AREATRIGGER);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_hun_legion_wild_protector_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_hun_legion_wild_protector_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_hun_legion_wild_protector_AuraScript();
        }
};

// 204147 - Windburst
class spell_hun_legion_windburst : public SpellScriptLoader
{
    public:
        spell_hun_legion_windburst() : SpellScriptLoader("spell_hun_legion_windburst") { }

        class spell_hun_legion_windburst_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_hun_legion_windburst_SpellScript);

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            {
                uint32 atSpawns = int32(GetCaster()->GetExactDist(GetHitUnit()) / 4.5) + 1;
                for (uint32 i = 0; i < atSpawns; i++)
                {
                    float angle = GetCaster()->GetRelativeAngle(GetHitUnit());
                    Position pos = GetCaster()->GetFirstCollisionPosition(float(i) * 4.5, angle);
                    SpellCastTargets targets;
                    pos.SetOrientation(angle + pos.GetOrientation());
                    targets.SetDst(pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation());
                    GetCaster()->CastSpell(targets, sSpellMgr->AssertSpellInfo(i & 1 ? 223114 : 226872), nullptr, TRIGGERED_FULL_MASK);
                }
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_hun_legion_windburst_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_hun_legion_windburst_SpellScript();
        }
};

// 109248 - Binding Shot
class areatrigger_hun_legion_binding_shot : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_binding_shot() : AreaTriggerEntityScript("areatrigger_hun_legion_binding_shot") { }

        struct areatrigger_hun_legion_binding_shotAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_binding_shotAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                _scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        for (ObjectGuid guid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                                if (caster->IsValidAttackTarget(target))
                                    target->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_HUNTER_BINDING_SHOT_VISUAL, true);
                    task.Repeat();
                });
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                    if (caster->IsValidAttackTarget(target))
                        caster->CastSpell(target, SPELL_HUNTER_BINDING_SHOT_AURA, true);
            }

            void OnUnitExit(Unit* target) override
            {
                target->RemoveAurasDueToSpell(SPELL_HUNTER_BINDING_SHOT_AURA, at->GetCasterGuid());

                if (at->IsRemoved())
                    return;

                if (Unit* caster = at->GetCaster())
                    if (caster->IsValidAttackTarget(target))
                        caster->CastSpell(target, SPELL_HUNTER_BINDING_SHOT_STUN, true);
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_binding_shotAI(areaTrigger);
        }
};

// 194278 - Caltrops
class areatrigger_hun_legion_caltrops : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_caltrops() : AreaTriggerEntityScript("areatrigger_hun_legion_caltrops") { }

        struct areatrigger_hun_legion_caltropsAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_caltropsAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                // small delay for the first task to make sure that insideunits are already set
                _scheduler.Schedule(Milliseconds(250), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        for (ObjectGuid guid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                                if (Aura* caltrops = target->GetAura(SPELL_HUNTER_CALTROPS_DEBUFF, caster->GetGUID()))
                                    caltrops->RefreshDuration(true);
                    task.Repeat(Seconds(1));
                });
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsValidAttackTarget(target))
                    {
                        caster->CastSpell(target, SPELL_HUNTER_CALTROPS_DEBUFF, true);
                        if (caster->HasAura(SPELL_HUNTER_STICKY_TAR))
                            caster->CastSpell(target, SPELL_HUNTER_STICKY_TAR_PERIODIC, true);
                    }
                }
                AreaTriggerAI::OnUnitEnter(target);
            }

            void OnUnitExit(Unit* target) override
            {
                target->RemoveAurasDueToSpell(SPELL_HUNTER_STICKY_TAR_PERIODIC, at->GetCasterGuid());
                AreaTriggerAI::OnUnitExit(target);
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_caltropsAI(areaTrigger);
        }
};

// 212431 - Explosive Shot
class areatrigger_hun_legion_explosive_shot : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_explosive_shot() : AreaTriggerEntityScript("areatrigger_hun_legion_explosive_shot") { }

        struct areatrigger_hun_legion_explosive_shotAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_explosive_shotAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnRemove() override
            {
                if (Unit* caster = at->GetCaster())
                    caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_HUNTER_EXPLOSIVE_SHOT_DAMAGE, true);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_explosive_shotAI(areaTrigger);
        }
};

// 13813 - Explosive Trap
class areatrigger_hun_legion_explosive_trap : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_explosive_trap() : AreaTriggerEntityScript("areatrigger_hun_legion_explosive_trap") { }

        struct areatrigger_hun_legion_explosive_trapAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_explosive_trapAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                if (Unit* caster = at->GetCaster())
                {
                    std::vector<AreaTrigger*> triggers = caster->GetAreaTriggers(at->GetSpellId());
                    for (AreaTrigger* trigger : triggers)
                        if (trigger->GetGUID() != at->GetGUID())
                            trigger->Remove();
                }
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!_triggered && caster->IsValidAttackTarget(target))
                    {
                        _triggered = true;
                        caster->CastSpell(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), SPELL_HUNTER_EXPLOSIVE_TRAP_DAMAGE, true);

                        if (AuraEffect* waylay = caster->GetAuraEffect(SPELL_HUNTER_WAYLAY, EFFECT_0))
                            if (at->GetTimeSinceCreated() >= uint32(waylay->GetAmount() * IN_MILLISECONDS))
                                caster->CastSpell(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), SPELL_HUNTER_EXPLOSIVE_TRAP_TIME_BONUS, true);

                        at->Remove();
                    }
                }
            }

            private:
                bool _triggered = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_explosive_trapAI(areaTrigger);
        }
};

// 132950 - Flare
class areatrigger_hun_legion_flare : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_flare() : AreaTriggerEntityScript("areatrigger_hun_legion_flare") { }

        struct areatrigger_hun_legion_flareAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_flareAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                    if (!caster->IsFriendlyTo(target))
                        target->CastSpell(target, SPELL_HUNTER_FLARE_DISPEL, true);
            }

            void OnUnitExit(Unit* target) override
            {
                target->RemoveAurasDueToSpell(SPELL_HUNTER_FLARE_DISPEL);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_flareAI(areaTrigger);
        }
};

// 187651 - Freezing Trap
class areatrigger_hun_legion_freezing_trap : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_freezing_trap() : AreaTriggerEntityScript("areatrigger_hun_legion_freezing_trap") { }

        struct areatrigger_hun_legion_freezing_trapAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_freezing_trapAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                if (Unit* caster = at->GetCaster())
                {
                    std::vector<AreaTrigger*> triggers = caster->GetAreaTriggers(at->GetSpellId());
                    for (AreaTrigger* trigger : triggers)
                        if (trigger->GetGUID() != at->GetGUID())
                            trigger->Remove();
                }
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                    if (!_triggered && caster->IsValidAttackTarget(target))
                    {
                        _triggered = true;
                        if (caster->HasAura(SPELL_HUNTER_DIAMOND_ICE))
                        {
                            caster->CastSpell(target, SPELL_HUNTER_FREEZING_TRAP_HONOR_VERSION, true);
                            at->Remove();
                        }
                        else
                            caster->CastSpell(target, SPELL_HUNTER_FREEZING_TRAP_FREEZE, true);
                    }
            }

            private:
                bool _triggered = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_freezing_trapAI(areaTrigger);
        }
};

// 236775 - Hi-Explosive Trap
class areatrigger_hun_legion_hi_explosive_trap : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_hi_explosive_trap() : AreaTriggerEntityScript("areatrigger_hun_legion_hi_explosive_trap") { }

        struct areatrigger_hun_legion_hi_explosive_trapAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_hi_explosive_trapAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                if (Unit* caster = at->GetCaster())
                {
                    std::vector<AreaTrigger*> triggers = caster->GetAreaTriggers(at->GetSpellId());
                    for (AreaTrigger* trigger : triggers)
                        if (trigger->GetGUID() != at->GetGUID())
                            trigger->Remove();
                }
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!_triggered && caster->IsValidAttackTarget(target))
                    {
                        _triggered = true;
                        caster->CastSpell(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), SPELL_HUNTER_HI_EXPLOSIVE_TRAP_DAMAGE, true);
                        at->Remove();
                    }
                }
            }

            private:
                bool _triggered = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_hi_explosive_trapAI(areaTrigger);
        }
};

// 206817 - Sentinel
class areatrigger_hun_legion_sentinel : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_sentinel() : AreaTriggerEntityScript("areatrigger_hun_legion_sentinel") { }

        struct areatrigger_hun_legion_sentinelAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_sentinelAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                // small delay for the first task to make sure that insideunits are already set
                _scheduler.Schedule(Milliseconds(250), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                    {
                        bool markApplied = false;
                        for (ObjectGuid guid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                                if (caster->IsValidAttackTarget(target))
                                {
                                    caster->CastSpell(target, SPELL_HUNTER_HUNTER_S_MARK, true);
                                    markApplied = true;
                                }

                        if (markApplied)
                            caster->CastSpell(caster, SPELL_HUNTER_HUNTER_S_MARK_CASTER, true);
                    }
                    task.Repeat(Milliseconds(_firstRepeat ? 5750 : 6000));
                    _firstRepeat = false;
                });
            }

            void OnUnitExit(Unit* target) override
            {
                // small areatrigger "hack" the last tick isn't executed it seems that the insideunits are removed before the last task is executed 
                if (at->IsRemoved())
                {
                    if (Unit* caster = at->GetCaster())
                    {
                        if (caster->IsValidAttackTarget(target))
                        {
                            caster->CastSpell(target, SPELL_HUNTER_HUNTER_S_MARK, true);
                            caster->CastSpell(caster, SPELL_HUNTER_HUNTER_S_MARK_CASTER, true);
                        }
                    }
                }
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
                bool _firstRepeat = true;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_sentinelAI(areaTrigger);
        }
};

// 162480 - Steel Trap
class areatrigger_hun_legion_steel_trap : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_steel_trap() : AreaTriggerEntityScript("areatrigger_hun_legion_steel_trap") { }

        struct areatrigger_hun_legion_steel_trapAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_steel_trapAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                if (Unit* caster = at->GetCaster())
                {
                    std::vector<AreaTrigger*> triggers = caster->GetAreaTriggers(at->GetSpellId());
                    for (AreaTrigger* trigger : triggers)
                        if (trigger->GetGUID() != at->GetGUID())
                            trigger->Remove();
                }
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!_triggered && caster->IsValidAttackTarget(target))
                    {
                        _triggered = true;
                        if (caster->HasAura(SPELL_HUNTER_EXPERT_TRAPPER))
                            caster->CastSpell(target, SPELL_HUNTER_STEEL_TRAP_INSTANT_DAMAGE, true);

                        if (AuraEffect* waylay = caster->GetAuraEffect(SPELL_HUNTER_WAYLAY, EFFECT_0))
                        {
                            if (at->GetTimeSinceCreated() >= uint32(waylay->GetAmount() * IN_MILLISECONDS))
                                caster->CastSpell(target, target->IsInCombat() ? SPELL_HUNTER_STEEL_TRAP_PERIODIC : SPELL_HUNTER_STEEL_TRAP_EXPERT_PERIODIC, true);
                            else
                                caster->CastSpell(target, SPELL_HUNTER_STEEL_TRAP_PERIODIC, true);
                        }
                        else
                            caster->CastSpell(target, SPELL_HUNTER_STEEL_TRAP_PERIODIC, true);

                        caster->CastSpell(target, SPELL_HUNTER_STEEL_TRAP_ROOT, true);
                        at->Remove();
                    }
                }
            }

            private:
                bool _triggered = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_steel_trapAI(areaTrigger);
        }
};

// 187699 - Tar Trap
class areatrigger_hun_legion_tar_trap : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_tar_trap() : AreaTriggerEntityScript("areatrigger_hun_legion_tar_trap") { }

        struct areatrigger_hun_legion_tar_trapAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_tar_trapAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                if (Unit* caster = at->GetCaster())
                {
                    std::vector<AreaTrigger*> triggers = caster->GetAreaTriggers(at->GetSpellId());
                    for (AreaTrigger* trigger : triggers)
                        if (trigger->GetGUID() != at->GetGUID())
                            trigger->Remove();
                }
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (!_triggered && caster->IsValidAttackTarget(target))
                    {
                        _triggered = true;
                        caster->CastSpell(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), SPELL_HUNTER_TAR_TRAP_SLOW_AREATRIGGER, true);

                        if (AuraEffect* waylay = caster->GetAuraEffect(SPELL_HUNTER_WAYLAY, EFFECT_0))
                            if (at->GetTimeSinceCreated() >= uint32(waylay->GetAmount() * IN_MILLISECONDS))
                                caster->CastSpell(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), SPELL_HUNTER_SUPER_STICKY_TAR, true);

                        at->Remove();
                    }
                }
            }

            private:
                bool _triggered = false;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_tar_trapAI(areaTrigger);
        }
};

// 187700 - Tar Trap
class areatrigger_hun_legion_tar_trap_slow : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_tar_trap_slow() : AreaTriggerEntityScript("areatrigger_hun_legion_tar_trap_slow") { }

        struct areatrigger_hun_legion_tar_trap_slowAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_tar_trap_slowAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                {
                    if (caster->IsValidAttackTarget(target))
                    {
                        if (caster->HasAura(SPELL_HUNTER_STICKY_TAR))
                            caster->CastSpell(target, SPELL_HUNTER_STICKY_TAR_PERIODIC, true);

                        if (caster->HasAura(SPELL_HUNTER_EXPERT_TRAPPER))
                            if (roll_chance_i(25)) // there is nothing public about the proc chance but on retail it feels like 25% chance
                                caster->AddAura(SPELL_HUNTER_SUPER_STICKY_TAR, target);
                    }
                }
                AreaTriggerAI::OnUnitEnter(target);
            }

            void OnUnitExit(Unit* target) override
            {
                target->RemoveAurasDueToSpell(SPELL_HUNTER_STICKY_TAR_PERIODIC, at->GetCasterGuid());
                AreaTriggerAI::OnUnitExit(target);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_tar_trap_slowAI(areaTrigger);
        }
};

// 204358 - Wild Protector
class areatrigger_hun_legion_wild_protector : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_wild_protector() : AreaTriggerEntityScript("areatrigger_hun_legion_wild_protector") { }

        struct areatrigger_hun_legion_wild_protectorAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_wild_protectorAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnCreate() override
            {
                // refresh the auras
                _scheduler.Schedule(Seconds(5), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        for (ObjectGuid guid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, guid))
                                caster->CastSpell(target, SPELL_HUNTER_WILD_PROTECTOR_BONUS, true);
                    task.Repeat();
                });
            }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                    if (target != caster && caster->IsValidAssistTarget(target))
                        caster->CastSpell(target, SPELL_HUNTER_WILD_PROTECTOR_BONUS, true);
            }

            void OnUnitExit(Unit* target) override
            {
                target->RemoveAurasDueToSpell(SPELL_HUNTER_WILD_PROTECTOR_BONUS, at->GetCasterGuid());
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_wild_protectorAI(areaTrigger);
        }
};

// 223114 - Windburst
// 226872 - Windburst
class areatrigger_hun_legion_windburst : public AreaTriggerEntityScript
{
    public:
        areatrigger_hun_legion_windburst() : AreaTriggerEntityScript("areatrigger_hun_legion_windburst") { }

        struct areatrigger_hun_legion_windburstAI : public AreaTriggerAI
        {
            areatrigger_hun_legion_windburstAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

            void OnUnitEnter(Unit* target) override
            {
                if (Unit* caster = at->GetCaster())
                    if (caster->IsValidAssistTarget(target))
                        caster->CastSpell(target, SPELL_HUNTER_WINDBURST_BONUS, true);
            }

            void OnUnitExit(Unit* target) override
            {
                // Don't remove the speed aura if the target is in another windburst areatrigger
                if (Unit* caster = at->GetCaster())
                    for (AreaTrigger* ats : caster->GetAreaTriggersByEntry({ at->GetEntry() }))
                        if (ats->GetGUID() != at->GetGUID())
                            if (std::find(ats->GetInsideUnits().begin(), ats->GetInsideUnits().end(), target->GetGUID()) != ats->GetInsideUnits().end())
                                return;

                target->RemoveAurasDueToSpell(SPELL_HUNTER_WINDBURST_BONUS);
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_hun_legion_windburstAI(areaTrigger);
        }
};

void AddSC_legion_hunter_spell_scripts()
{
    new spell_hun_legion_a_murder_of_crows();
    new spell_hun_legion_aimed_shot();
    new spell_hun_legion_aimed_shot_blocker();
    new spell_hun_legion_animal_instincts();
    new spell_hun_legion_arcane_shot();
    new spell_hun_legion_aspect_cheetah();
    new spell_hun_legion_aspect_of_the_turtle();
    new spell_hun_legion_aspect_of_the_wild();
    new spell_hun_legion_barrage();
    new spell_hun_legion_basic_pet_attacks();
    new spell_hun_legion_beast_cleave();
    new spell_hun_legion_beast_cleave_damage();
    new spell_hun_legion_bestial_wrath();
    new spell_hun_legion_bird_of_prey();
    new spell_hun_legion_black_arrow_proc();
    new spell_hun_legion_blink_strikes();
    new spell_hun_legion_bullseye();
    new spell_hun_legion_butchery();
    new spell_hun_legion_bursting_shot();
    new spell_hun_legion_call_of_the_hunter();
    new spell_hun_legion_call_pet();
    new spell_hun_legion_camouflage();
    new spell_hun_legion_careful_aim();
    new spell_hun_legion_carve();
    new spell_hun_legion_chimaera_shot();
    new spell_hun_legion_cobra_shot();
    new spell_hun_legion_critical_focus();
    new spell_hun_legion_dark_whisper();
    new spell_hun_legion_dire_beast();
    new spell_hun_legion_dire_frenzy();
    new spell_hun_legion_disengage();
    new spell_hun_legion_dragonsfire_conflagration();
    new spell_hun_legion_dragonsfire_grenade();
    new spell_hun_legion_dragonscale_armor();
    new spell_hun_legion_exhilaration();
    new spell_hun_legion_explosive_shot();
    new spell_hun_legion_explosive_shot_detonate();
    new spell_hun_legion_explosive_trap_damage();
    new spell_hun_legion_farstrider();
    new spell_hun_legion_flanking_strike();
    new spell_hun_legion_fluffy_go();
    new spell_hun_legion_freezing_trap();
    new spell_hun_legion_fury_of_the_eagle();
    new spell_hun_legion_hardiness();
    new spell_hun_legion_hati_s_bond();
    new spell_hun_legion_heart_of_the_phoenix();
    new spell_hun_legion_hunter_s_advantage();
    new spell_hun_legion_hunter_s_bounty();
    new spell_hun_legion_hunting_companion();
    new spell_hun_legion_hunting_companion_proc();
    new spell_hun_legion_hunting_party();
    new spell_hun_legion_intimidation();
    new spell_hun_legion_kill_command();
    new spell_hun_legion_kill_command_damage();
    new spell_hun_legion_killer_cobra();
    new spell_hun_legion_last_stand();
    new spell_hun_legion_legacy_of_the_windrunners();
    new spell_hun_legion_lone_wolf();
    new spell_hun_legion_mark_of_the_windrunner();
    new spell_hun_legion_marked_shot();
    new spell_hun_legion_marksmanship_hunter();
    new spell_hun_legion_masters_call();
    new spell_hun_legion_mimiron_s_shell();
    new spell_hun_legion_misdirection();
    new spell_hun_legion_misdirection_proc();
    new spell_hun_legion_mortal_wounds();
    new spell_hun_legion_multi_shot();
    new spell_hun_legion_mongoose_bite();
    new spell_hun_legion_natural_reflexes();
    new spell_hun_legion_on_the_trail();
    new spell_hun_legion_piercing_shot();
    new spell_hun_legion_posthaste();
    new spell_hun_legion_roar_of_sacrifice();
    new spell_hun_legion_scatter_shot();
    new spell_hun_legion_sentinel();
    new spell_hun_legion_separation_anxiety();
    new spell_hun_legion_serpent_sting();
    new spell_hun_legion_sidewinders();
    new spell_hun_legion_snake_hunter();
    new spell_hun_legion_sparring();
    new spell_hun_legion_spider_sting();
    new spell_hun_legion_stampede_periodic();
    new spell_hun_legion_steady_focus();
    new spell_hun_legion_steel_trap_root();
    new spell_hun_legion_sticky_bomb();
    new spell_hun_legion_stomp();
    new spell_hun_legion_surge_of_the_stormgod();
    new spell_hun_legion_survivalist();
    new spell_hun_legion_talon_strike();
    new spell_hun_legion_terms_of_engagement();
    new spell_hun_legion_throwing_axes();
    new spell_hun_legion_titan_s_thunder();
    new spell_hun_legion_titan_s_thunder_periodic();
    new spell_hun_legion_trailblazer();
    new spell_hun_legion_trick_shot();
    new spell_hun_legion_thunderstomp();
    new spell_hun_legion_viper_sting();
    new spell_hun_legion_volley();
    new spell_hun_legion_vulnerable();
    new spell_hun_legion_wild_call();
    new spell_hun_legion_wild_protector();
    new spell_hun_legion_windburst();

    new areatrigger_hun_legion_binding_shot();
    new areatrigger_hun_legion_caltrops();
    new areatrigger_hun_legion_explosive_shot();
    new areatrigger_hun_legion_explosive_trap();
    new areatrigger_hun_legion_flare();
    new areatrigger_hun_legion_freezing_trap();
    new areatrigger_hun_legion_hi_explosive_trap();
    new areatrigger_hun_legion_sentinel();
    new areatrigger_hun_legion_steel_trap();
    new areatrigger_hun_legion_tar_trap();
    new areatrigger_hun_legion_tar_trap_slow();
    new areatrigger_hun_legion_wild_protector();
    new areatrigger_hun_legion_windburst();
}
