/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_WARLOCK and SPELLFAMILY_GENERIC spells used by warlock players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_warl_legion".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "Containers.h"
#include "Creature.h"
#include "Creature.h"
#include "CreatureAI.h"
#include "CreatureAIImpl.h"
#include "DB2Stores.h"
#include "G3D/Vector3.h"
#include "GameObject.h"
#include "GameObject.h"
#include "GridNotifiers.h"
#include "Item.h"
#include "Map.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "Pet.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "Spell.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellPackets.h"
#include "SpellScript.h"
#include "TaskScheduler.h"
#include "TemporarySummon.h"
#include "UnitAI.h"
#include "UnitAI.h"
#include <random>

enum Spells
{
    SPELL_HOTFIX_WILD_IMP_TARGET_MARKER             = 300003,
    SPELL_PET_SPECIAL_ABYSSAL                       = 171154,
    SPELL_PET_SPECIAL_CAUTERIZE_MASTER              = 119899,
    SPELL_PET_SPECIAL_FELGUARD                      = 119914,
    SPELL_PET_SPECIAL_FELHUNTER                     = 119910,
    SPELL_PET_SPECIAL_IMP                           = 119905,
    SPELL_PET_SPECIAL_INFERNAL                      = 171152,
    SPELL_PET_SPECIAL_OBSERVER                      = 119911,
    SPELL_PET_SPECIAL_SHIVARRA                      = 119913,
    SPELL_PET_SPECIAL_SUCCUBUS                      = 119909,
    SPELL_PET_SPECIAL_SUFFERING                     = 176799,
    SPELL_PET_SPECIAL_TERRORGUARD                   = 171140,
    SPELL_PET_SPECIAL_VOIDLORD                      = 119907,
    SPELL_PET_SPECIAL_WRATHGUARD                    = 119915,
    SPELL_UNSTABLE_TEAR_CHAOS_BARRAGE_DAMAGE        = 187394,
    SPELL_WARLOCK_ABSOLUTE_CORRUPTION               = 196103,
    SPELL_WARLOCK_AGONY                             = 980,
    SPELL_WARLOCK_AGONY_ENERGIZE                    = 210067,
    SPELL_WARLOCK_BACKDRAFT                         = 196406,
    SPELL_WARLOCK_BACKDRAFT_BONUS                   = 117828,
    SPELL_WARLOCK_BANE_OF_HAVOC                     = 200546,
    SPELL_WARLOCK_BANE_OF_HAVOC_AURA                = 200548,
    SPELL_WARLOCK_CALL_DREADSTALKERS_LEFT           = 193331,
    SPELL_WARLOCK_CALL_DREADSTALKERS_RIGHT          = 193332,
    SPELL_WARLOCK_CHANNEL_DEMONFIRE_DAMAGE          = 196448,
    SPELL_WARLOCK_CHANNEL_DEMONFIRE_SELECTOR        = 196449,
    SPELL_WARLOCK_CHAOS_BOLT                        = 116858,
    SPELL_WARLOCK_CHAOS_TEAR                        = 215276,
    SPELL_WARLOCK_COMPOUNDING_DAMAGE                = 231489,
    SPELL_WARLOCK_COMPOUNDING_HORROR_AURA           = 199281,
    SPELL_WARLOCK_COMPOUNDING_HORROR_BUFF           = 199281,
    SPELL_WARLOCK_CONFLAGRATION_OF_CHAOS_BONUS      = 196546,
    SPELL_WARLOCK_CONTAGION                         = 196105,
    SPELL_WARLOCK_CONTAGION_DEBUFF                  = 233494,
    SPELL_WARLOCK_CORRUPTION                        = 172,
    SPELL_WARLOCK_CORRUPTION_AURA                   = 146739,
    SPELL_WARLOCK_CREATE_HEALTHSTONE                = 23517,
    SPELL_WARLOCK_CREATE_HEALTHSTONE_RECHARGE       = 120038,
    SPELL_WARLOCK_CREMATION                         = 212282,
    SPELL_WARLOCK_CURSE_OF_SHADOWS_DAMAGE           = 236615,
    SPELL_WARLOCK_DEADWIND_HARVESTER                = 216708,
    SPELL_WARLOCK_DEMONIC_CALLING_BUFF              = 205146,
    SPELL_WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST         = 62388,
    SPELL_WARLOCK_DEMONIC_CIRCLE_SUMMON             = 48018,
    SPELL_WARLOCK_DEMONIC_CIRCLE_TELEPORT           = 48020,
    SPELL_WARLOCK_DEMONIC_PORTAL_SUMMON_PLAYER_POS  = 113886,
    SPELL_WARLOCK_DEMONIC_SKIN                      = 219272,
    SPELL_WARLOCK_DEMONWRATH_CHANNEL                = 193440,
    SPELL_WARLOCK_DEMONWRATH_DAMAGE                 = 193439,
    SPELL_WARLOCK_DEMONWRATH_ENERGIZE               = 194379,
    SPELL_WARLOCK_DEVOUR_MAGIC_HEAL                 = 19658,
    SPELL_WARLOCK_DEVOURER_OF_LIFE_BONUS            = 215165,
    SPELL_WARLOCK_DEVOURER_OF_LIFE_PROC_AURA        = 196301,
    SPELL_WARLOCK_DIMENSIONAL_RIFT                  = 196586,
    SPELL_WARLOCK_DOOM                              = 603,
    SPELL_WARLOCK_DOOM_ENERGIZE                     = 193318,
    SPELL_WARLOCK_DRAIN_SOUL_ENERGIZE               = 205292,
    SPELL_WARLOCK_ESSENCE_DRAIN_DEBUFF              = 221715,
    SPELL_WARLOCK_ETERNAL_STRUGGLE_BONUS            = 196304,
    SPELL_WARLOCK_ETERNAL_STRUGGLE_PROC_AURA        = 196305,
    SPELL_WARLOCK_FATAL_ECHOES                      = 199257,
    SPELL_WARLOCK_FEAR_TRIGGERED                    = 118699,
    SPELL_WARLOCK_FIRE_AND_BRIMSTONE                = 196408,
    SPELL_WARLOCK_FOCUSED_CHAOS                     = 233577,
    SPELL_WARLOCK_GRIMOIRE_OF_SUPREMACY             = 152107,
    SPELL_WARLOCK_HAND_OF_DOOM                      = 196283,
    SPELL_WARLOCK_HAND_OF_GULDAN_CASTER_AURA        = 225481,
    SPELL_WARLOCK_HAND_OF_GULDAN_DAMAGE             = 86040,
    SPELL_WARLOCK_HAND_OF_GULDAN_DEST_CASTER        = 196282,
    SPELL_WARLOCK_HAND_OF_GULDAN_DURATION           = 225481,
    SPELL_WARLOCK_HAND_OF_GULDAN_SUMMON_IMP         = 104317,
    SPELL_WARLOCK_HAVOC                             = 80240,
    SPELL_WARLOCK_HEALTH_FUNNEL_TRIGGERED           = 217979,
    SPELL_WARLOCK_IMMOLATE                          = 20153,
    SPELL_WARLOCK_IMMOLATE_ACTIVATOR                = 228312,
    SPELL_WARLOCK_IMMOLATE_PERIODIC                 = 157736,
    SPELL_WARLOCK_IMPROVED_DREADSTALKERS            = 196272,
    SPELL_WARLOCK_INCINERATE                        = 29722,
    SPELL_WARLOCK_LORD_OF_FLAMES_DEBUFF             = 226802,
    SPELL_WARLOCK_LORD_OF_FLAMES_SUMMON_INFERNAL    = 224105,
    SPELL_WARLOCK_LORD_OF_FLAMES_TRAIT              = 224103,
    SPELL_WARLOCK_PET_FEL_FIREBOLT                  = 104318,
    SPELL_WARLOCK_PHANTOM_SINGULARITY_DAMAGE        = 205246,
    SPELL_WARLOCK_PLEASURE_THROUGH_PAIN             = 212618,
    SPELL_WARLOCK_PLEASURE_THROUGH_PAIN_PET_AURA    = 212999,
    SPELL_WARLOCK_POWER_TRIP                        = 196605,
    SPELL_WARLOCK_POWER_TRIP_ENERGIZE               = 216125,
    SPELL_WARLOCK_RAIN_OF_FIRE_DAMAGE               = 42223,
    SPELL_WARLOCK_ROARING_BLAZE_PCT_BONUS           = 205690,
    SPELL_WARLOCK_ROT_AND_DECAY                     = 212371,
    SPELL_WARLOCK_SEED_OF_CORRUPTION_AOE            = 27285,
    SPELL_WARLOCK_SHADOW_BOLT                       = 686,
    SPELL_WARLOCK_SHADOW_BOLT_ENERGIZE              = 194192,
    SPELL_WARLOCK_SHADOWBURN_ENERGIZE               = 29341,
    SPELL_WARLOCK_SHADOWY_INSPIRATION               = 196269,
    SPELL_WARLOCK_SHADOWY_INSPIRATION_BONUS         = 196606,
    SPELL_WARLOCK_SHADOWY_TEAR                      = 196639,
    SPELL_WARLOCK_SINGE_MAGIC_DISPEL                = 212620,
    SPELL_WARLOCK_SIPHON_LIFE                       = 63106,
    SPELL_WARLOCK_SOUL_CONDUIT                      = 215942,
    SPELL_WARLOCK_SOUL_EFFIGY                       = 205178,
    SPELL_WARLOCK_SOUL_EFFIGY_DAMAGE                = 205260,
    SPELL_WARLOCK_SOUL_EFFIGY_VISUAL                = 205277,
    SPELL_WARLOCK_SOUL_FLAME_DAMAGE                 = 199581,
    SPELL_WARLOCK_SOUL_LEECH                        = 108370,
    SPELL_WARLOCK_SOUL_LEECH_SHIELD                 = 108366,
    SPELL_WARLOCK_SOUL_LINK                         = 108446,
    SPELL_WARLOCK_SOUL_LINK_HEAL                    = 108447,
    SPELL_WARLOCK_SOULSHATTER_ENERGIZE              = 212921,
    SPELL_WARLOCK_SOULSHATTER_HASTE_BONUS           = 236471,
    SPELL_WARLOCK_SOULSNATCHER_ENERGIZE             = 196234,
    SPELL_WARLOCK_SOW_THE_SEEDS                     = 196226,
    SPELL_WARLOCK_STOLEN_POWER_BONUS                = 211583,
    SPELL_WARLOCK_SWEET_SOULS                       = 199220,
    SPELL_WARLOCK_SWEET_SOULS_HEAL                  = 199221,
    SPELL_WARLOCK_THALKIELS_CONSUMPTION_DAMAGE      = 211715,
    SPELL_WARLOCK_THALKIELS_DISCORD                 = 211727,
    SPELL_WARLOCK_TORMENTED_SOULS                   = 216695,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_DUMMY         = 30108,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_ENERGIZE      = 31117,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_LEVEL_BONUS   = 231791,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_SILENCE       = 43523,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED     = 233490,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_2   = 233496,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_3   = 233497,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_4   = 233498,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_5   = 233499,
    SPELL_WARLOCK_UNSTABLE_TEAR                     = 187370,
    SPELL_WARLOCK_USE_SOULSTONE                     = 3026,
    SPELL_WARLOCK_VISUAL_SOUL_EFFIGY                = 53810,
    SPELL_WARLOCK_WARLOCK                           = 157902,
    SPELL_WARLOCK_WRATH_OF_CONSUMPTION_BONUS        = 199646,
    SPELL_WARLOCk_IMPENDING_DOOM                    = 196270,
    SPELL_WARLOCK_SOUL_OF_THE_DAMNED_AREATRIGGER    = 216463,
    SPELL_SOUL_EFFIGY_PERIODIC                      = 205247,
};

static const uint32 UnstableAfflictionHelper[5] =
{
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_2,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_3,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_4,
    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_5
};

enum Misc
{
    GO_SOULWELL                                 = 181621,
    ITEM_HEALTHSTONE                            = 5512,
    NPC_ABYSSAL                                 = 58997,
    NPC_DEMONIC_GATEWAY_CASTER                  = 59262,
    NPC_DEMONIC_GATEWAY_DEST                    = 59271,
    NPC_DEMONIC_GATEWAY_TARGET                  = 47319,
    NPC_DOOMGUARD                               = 78158,
    NPC_FEL_IMP                                 = 58959,
    NPC_FELGUARD                                = 17252,
    NPC_FELHUNTER                               = 417,
    NPC_IMP                                     = 416,
    NPC_INFERNAL                                = 78217,
    NPC_OBSERVER                                = 58964,
    NPC_SHIVARRA                                = 58963,
    NPC_SUCCUBUS                                = 1863,
    NPC_TERRORGUARD                             = 78215,
    NPC_VOID_LORD                               = 58960,
    NPC_VOID_WALKER                             = 1860,
    NPC_WILD_IMP                                = 55659,
    NPC_WRATHGUARD                              = 58965,
    SPELL_VISUAL_DEMONIC_GATEWAY_DEST_SUMMON    = 63644,
};

// 980 - Agony
class spell_warl_legion_agony : public SpellScriptLoader
{
    public:
        spell_warl_legion_agony() : SpellScriptLoader("spell_warl_legion_agony") { }

        class spell_warl_legion_agony_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_agony_AuraScript);

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                if (GetStackAmount() < GetAura()->CalcMaxStackAmount())
                    SetStackAmount(GetStackAmount() + 1);

                if (GetCaster() && GetCaster()->GetTypeId() == TYPEID_PLAYER)
                {
                    float procChance = GetCaster()->ToPlayer()->GetSharedProcChance(GetSpellInfo()->Id);
                    if (roll_chance_f(procChance))
                    {
                        GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_AGONY_ENERGIZE, true, nullptr, aurEff);
                        GetCaster()->ToPlayer()->UpdateSharedProcChance(GetSpellInfo()->Id, 2.5f);
                    }
                    else
                        GetCaster()->ToPlayer()->UpdateSharedProcChance(GetSpellInfo()->Id, procChance + 2.5f);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_agony_AuraScript::HandleEffectPeriodic, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_agony_AuraScript();
        }
};

// 710 - Banish
class spell_warl_legion_banish : public SpellScriptLoader
{
    public:
        spell_warl_legion_banish() : SpellScriptLoader("spell_warl_legion_banish") { }

        class spell_warl_legion_banish_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_banish_SpellScript);

            void HandleBanish(SpellMissInfo missInfo)
            {
                if (missInfo != SPELL_MISS_IMMUNE || !GetHitUnit())
                    return;

                if (Aura* banish = GetHitUnit()->GetAura(GetSpellInfo()->Id, GetCaster()->GetGUID()))
                    banish->Remove();
            }

            void Register() override
            {
                BeforeHit += BeforeSpellHitFn(spell_warl_legion_banish_SpellScript::HandleBanish);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_banish_SpellScript();
        }
};

// 111400 - Burning Rush
class spell_warl_legion_burning_rush : public SpellScriptLoader
{
    public:
        spell_warl_legion_burning_rush() : SpellScriptLoader("spell_warl_legion_burning_rush") { }

        class spell_warl_legion_burning_rush_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_burning_rush_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->HasAura(DIFFICULTY_NONE, SPELL_AURA_PERIODIC_DAMAGE_PERCENT))
                    return false;
                return true;
            }

            SpellCastResult CheckElevation()
            {
                if (GetCaster()->HasAura(GetSpellInfo()->Id))
                {
                    GetCaster()->RemoveAura(GetSpellInfo()->Id);
                    return SPELL_FAILED_DONT_REPORT;
                }

                if (GetCaster()->GetHealthPct() <= float(GetEffectInfo(EFFECT_1)->CalcValue(GetCaster())))
                    return SPELL_FAILED_FIZZLE;

                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warl_legion_burning_rush_SpellScript::CheckElevation);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_burning_rush_SpellScript();
        }

        class spell_warl_legion_burning_rush_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_burning_rush_AuraScript);

            void PeriodicTick(AuraEffect const* aurEff)
            {
                if (GetTarget()->GetHealthPct() <= float(aurEff->GetAmount()))
                {
                    PreventDefaultAction();
                    Remove();
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_burning_rush_AuraScript::PeriodicTick, EFFECT_1, SPELL_AURA_PERIODIC_DAMAGE_PERCENT);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_burning_rush_AuraScript();
        }
};

// 104316 - Call Dreadstalkers
class spell_warl_legion_call_dreadstalkers : public SpellScriptLoader
{
    public:
        spell_warl_legion_call_dreadstalkers() : SpellScriptLoader("spell_warl_legion_call_dreadstalkers") { }

        class spell_warl_legion_call_dreadstalkers_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_call_dreadstalkers_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_CALL_DREADSTALKERS_LEFT,
                    SPELL_WARLOCK_CALL_DREADSTALKERS_RIGHT,
                    SPELL_WARLOCK_IMPROVED_DREADSTALKERS,
                    SPELL_WARLOCK_DEMONIC_CALLING_BUFF
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_CALL_DREADSTALKERS_LEFT, true);
                GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_CALL_DREADSTALKERS_RIGHT, true);
                GetCaster()->RemoveAurasDueToSpell(SPELL_WARLOCK_DEMONIC_CALLING_BUFF); ///@TODO: new proc system - Buff is currently not removed because initial spell is not a damage spell
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAuraEffect(SPELL_WARLOCK_IMPROVED_DREADSTALKERS, EFFECT_0))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_HOTFIX_WILD_IMP_TARGET_MARKER, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warl_legion_call_dreadstalkers_SpellScript::HandleAfterCast);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_call_dreadstalkers_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_call_dreadstalkers_SpellScript();
        }
};

// 77220 - Mastery: Chaotic Energies
class spell_warl_legion_chaotic_energies : public SpellScriptLoader
{
    public:
        spell_warl_legion_chaotic_energies() : SpellScriptLoader("spell_warl_legion_chaotic_energies") { }

        class spell_warl_legion_chaotic_energies_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_chaotic_energies_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            float getRandomPct(float max)
            {
                return (0.1f + 1.0f) + (((float)rand()) / (float)RAND_MAX) * (max - (0.1f + 1.0f));
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                if (AuraEffect* maxAbsorbPct = GetEffect(EFFECT_1))
                {
                    float absorbPct = getRandomPct(float(maxAbsorbPct->GetAmount()));
                    absorbAmount = CalculatePct(dmgInfo.GetDamage(), absorbPct);
                }
                else
                    absorbAmount = 0;
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_warl_legion_chaotic_energies_AuraScript::CalculateAmount, EFFECT_2, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_warl_legion_chaotic_energies_AuraScript::Absorb, EFFECT_2);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_chaotic_energies_AuraScript();
        }
};

// 152108 - Cataclysm
class spell_warl_legion_cataclysm : public SpellScriptLoader
{
    public:
        spell_warl_legion_cataclysm() : SpellScriptLoader("spell_warl_legion_cataclysm") { }

        class spell_warl_legion_cataclysm_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_cataclysm_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_IMMOLATE_PERIODIC
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_IMMOLATE_PERIODIC, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_cataclysm_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_cataclysm_SpellScript();
        }
};

// 212619 - Call Felhunter
class spell_warl_legion_call_felhunter : public SpellScriptLoader
{
    public:
        spell_warl_legion_call_felhunter() : SpellScriptLoader("spell_warl_legion_call_felhunter") { }

        class spell_warl_legion_call_felhunter_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_call_felhunter_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            SpellCastResult CheckPet()
            {
                if (Pet* pet = GetCaster()->ToPlayer()->GetPet())
                    if (pet->GetEntry() == ENTRY_FELHUNTER)
                        return SPELL_FAILED_ALREADY_HAVE_PET;
                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warl_legion_call_felhunter_SpellScript::CheckPet);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_call_felhunter_SpellScript();
        }
};

// 196447 - Channel Demonfire
class spell_warl_legion_channel_demonfire : public SpellScriptLoader
{
    public:
        spell_warl_legion_channel_demonfire() : SpellScriptLoader("spell_warl_legion_channel_demonfire") { }

        class spell_warl_legion_channel_demonfire_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_channel_demonfire_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_CHANNEL_DEMONFIRE_SELECTOR
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_WARLOCK_CHANNEL_DEMONFIRE_SELECTOR, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_channel_demonfire_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_channel_demonfire_AuraScript();
        }
};

// 196449 - Channel Demonfire
class spell_warl_legion_channel_demonfire_selector : public SpellScriptLoader
{
    public:
        spell_warl_legion_channel_demonfire_selector() : SpellScriptLoader("spell_warl_legion_channel_demonfire_selector") { }

        class spell_warl_legion_channel_demonfire_selector_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_channel_demonfire_selector_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_IMMOLATE_PERIODIC,
                    SPELL_WARLOCK_CHANNEL_DEMONFIRE_DAMAGE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_WARLOCK_IMMOLATE_PERIODIC, GetCaster()->GetGUID()));

                if (targets.empty())
                    return;

                if (WorldObject* target = Trinity::Containers::SelectRandomContainerElement(targets))
                {
                    targets.clear();
                    targets.push_back(target);
                }
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_CHANNEL_DEMONFIRE_DAMAGE, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_channel_demonfire_selector_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_channel_demonfire_selector_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_channel_demonfire_selector_SpellScript();
        }
};

// 187385 Chaos Barrage
class spell_warl_legion_chaos_barrage : public SpellScriptLoader
{
    public:
        spell_warl_legion_chaos_barrage() : SpellScriptLoader("spell_warl_legion_chaos_barrage") { }

        class spell_warl_legion_chaos_barrage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_chaos_barrage_AuraScript);

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                Creature* caster = GetTarget()->ToCreature();
                Unit* owner = caster->GetOwner(false);

                if (!owner || !caster || !caster->IsAIEnabled)
                {
                    caster->DespawnOrUnsummon();
                    return;
                }

                if (Unit* target = ObjectAccessor::GetUnit(*caster, caster->AI()->GetGUID(0)))
                    if (target->IsAlive() && owner->IsValidAttackTarget(target))
                        caster->CastSpell(target, SPELL_UNSTABLE_TEAR_CHAOS_BARRAGE_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_chaos_barrage_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_chaos_barrage_AuraScript();
        }
};

// 116858 - Chaos Bolt
class spell_warl_legion_chaos_bolt : public SpellScriptLoader
{
    public:
        spell_warl_legion_chaos_bolt() : SpellScriptLoader("spell_warl_legion_chaos_bolt") { }

        class spell_warl_legion_chaos_bolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_chaos_bolt_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), GetCaster()->ToPlayer()->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1)));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_chaos_bolt_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_chaos_bolt_SpellScript();
        }
};

class DelayedFelstormCooldownEvent : public BasicEvent
{
    public:
        DelayedFelstormCooldownEvent(Player* owner, uint32 triggerSpellId) : _owner(owner), _triggerSpellId(triggerSpellId) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            if (Pet* pet = _owner->GetPet())
            {
                SpellInfo const* triggerSpellInfo = sSpellMgr->AssertSpellInfo(_triggerSpellId);
                pet->GetSpellHistory()->StartCooldown(triggerSpellInfo, 0);
                _owner->SendForcedCooldown(SPELL_PET_SPECIAL_FELGUARD, pet->GetSpellHistory()->GetRemainingCooldown(triggerSpellInfo));
            }
            return true;
        }

    private:
        Player* _owner;
        uint32 _triggerSpellId;
};

// 119905 - Cauterize Master
// 119907 - Suffering
// 119909 - Whiplash
// 119910 - Spell Lock
// 119911 - Optical Blast
// 119913 - Fellash
// 171140 - Shadow Lock
// 171152 - Meteor Strike
// 171154 - Meteor Strike
// 119914 - Felstorm
class spell_warl_legion_command_demon_dummys : public SpellScriptLoader
{
    public:
        spell_warl_legion_command_demon_dummys() : SpellScriptLoader("spell_warl_legion_command_demon_dummys") { }

        class spell_warl_legion_command_demon_dummys_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_command_demon_dummys_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            bool canCastSpell(Pet* pet, SpellInfo const* triggerSpellInfo)
            {
                for (auto const& cost : triggerSpellInfo->PowerCosts)
                    if (pet->GetPower(Powers(cost->PowerType)) < cost->ManaCost)
                        return false;
                return true;
            }

            void HandleDummy(SpellEffIndex effIndex)
            {
                if (GetSpellInfo()->Id == SPELL_PET_SPECIAL_SUCCUBUS || GetSpellInfo()->Id == SPELL_PET_SPECIAL_SHIVARRA)
                    return;

                uint32 triggerSpell = GetEffectInfo(effIndex)->BasePoints;

                if (GetSpellInfo()->Id == SPELL_PET_SPECIAL_IMP) // wrong basepoints
                    triggerSpell = SPELL_PET_SPECIAL_CAUTERIZE_MASTER;
                else if (GetSpellInfo()->Id == SPELL_PET_SPECIAL_VOIDLORD)
                    triggerSpell = SPELL_PET_SPECIAL_SUFFERING;

                SpellInfo const* triggerSpellInfo = sSpellMgr->GetSpellInfo(triggerSpell);

                if (!triggerSpellInfo || triggerSpell == 0)
                    return;

                if (Pet* pet = GetCaster()->ToPlayer()->GetPet())
                {
                    if (canCastSpell(pet, triggerSpellInfo))
                    {
                        pet->CastSpell(GetHitUnit(), triggerSpell, false);
                        GetCaster()->ToPlayer()->SendForcedCooldown(GetSpellInfo()->Id, pet->GetSpellHistory()->GetRemainingCooldown(triggerSpellInfo));
                    }
                }
            }

            void HandleDummyDest(SpellEffIndex effIndex)
            {
                if (GetSpellInfo()->Id != SPELL_PET_SPECIAL_SUCCUBUS && GetSpellInfo()->Id != SPELL_PET_SPECIAL_SHIVARRA)
                    return;

                uint32 triggerSpell = GetEffectInfo(effIndex)->BasePoints;
                SpellInfo const* triggerSpellInfo = sSpellMgr->GetSpellInfo(triggerSpell);

                if (!triggerSpellInfo || triggerSpell == 0)
                    return;

                if (WorldLocation* dest = GetHitDest())
                {
                    if (Pet* pet = GetCaster()->ToPlayer()->GetPet())
                    {
                        if (canCastSpell(pet, triggerSpellInfo))
                        {
                            pet->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), triggerSpell, false);
                            GetCaster()->ToPlayer()->SendForcedCooldown(GetSpellInfo()->Id, pet->GetSpellHistory()->GetRemainingCooldown(triggerSpellInfo));
                        }
                    }
                }
            }

            void HandleFelstormSpecial(SpellEffIndex /*effIndex*/)
            {
                SpellInfo const* triggerSpellInfo = sSpellMgr->AssertSpellInfo(GetEffectInfo()->TriggerSpell);
                    if (Pet* pet = GetCaster()->ToPlayer()->GetPet())
                        if (canCastSpell(pet, triggerSpellInfo))
                            GetCaster()->m_Events.AddEvent(new DelayedFelstormCooldownEvent(GetCaster()->ToPlayer(), GetEffectInfo()->TriggerSpell), GetCaster()->m_Events.CalculateTime(150));
            }

            void HandleAfterHit()
            {
                // needed to unstuck the dummy button
                GetSpell()->SendInterrupted(0);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_command_demon_dummys_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnEffectHit += SpellEffectFn(spell_warl_legion_command_demon_dummys_SpellScript::HandleDummyDest, EFFECT_0, SPELL_EFFECT_DUMMY);
                if (m_scriptSpellId == SPELL_PET_SPECIAL_FELGUARD)
                    OnEffectHitTarget += SpellEffectFn(spell_warl_legion_command_demon_dummys_SpellScript::HandleFelstormSpecial, EFFECT_0, SPELL_EFFECT_FORCE_CAST);
                AfterHit += SpellHitFn(spell_warl_legion_command_demon_dummys_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_command_demon_dummys_SpellScript();
        }
};

// 119904 - Override Command Demon
class spell_warl_legion_command_demon_override : public SpellScriptLoader
{
    public:
        spell_warl_legion_command_demon_override() : SpellScriptLoader("spell_warl_legion_command_demon_override") { }

        class spell_warl_legion_command_demon_override_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_command_demon_override_AuraScript);

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                if (!GetCaster() || !GetCaster()->IsPet() || GetCaster()->GetTypeId() != TYPEID_UNIT)
                    return;

                switch (GetCaster()->GetEntry())
                {
                    case NPC_IMP:
                    case NPC_FEL_IMP:
                        amount = SPELL_PET_SPECIAL_IMP;
                        break;
                    case NPC_VOID_LORD:
                    case NPC_VOID_WALKER:
                        amount = SPELL_PET_SPECIAL_VOIDLORD;
                        break;
                    case NPC_SUCCUBUS:
                        amount = SPELL_PET_SPECIAL_SUCCUBUS;
                        break;
                    case NPC_FELHUNTER:
                        amount = SPELL_PET_SPECIAL_FELHUNTER;
                        break;
                    case NPC_TERRORGUARD:
                    case NPC_DOOMGUARD:
                        amount = SPELL_PET_SPECIAL_TERRORGUARD;
                        break;
                    case NPC_SHIVARRA:
                        amount = SPELL_PET_SPECIAL_SHIVARRA;
                        break;
                    case NPC_OBSERVER:
                        amount = SPELL_PET_SPECIAL_OBSERVER;
                        break;
                    case NPC_WRATHGUARD:
                        amount = SPELL_PET_SPECIAL_WRATHGUARD;
                        break;
                    case NPC_ABYSSAL:
                        amount = SPELL_PET_SPECIAL_ABYSSAL;
                        break;
                    case NPC_INFERNAL:
                        amount = SPELL_PET_SPECIAL_INFERNAL;
                        break;
                    case NPC_FELGUARD:
                        amount = SPELL_PET_SPECIAL_FELGUARD;
                        break;
                    default:
                        amount = 119898;
                        break;
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_warl_legion_command_demon_override_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_OVERRIDE_ACTIONBAR_SPELLS);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_command_demon_override_AuraScript();
        }
};

// 231489 - Compounding Horror
class spell_warl_legion_compounding_horror_damage : public SpellScriptLoader
{
    public:
        spell_warl_legion_compounding_horror_damage() : SpellScriptLoader("spell_warl_legion_compounding_horror_damage") { }

        class spell_warl_legion_compounding_horror_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_compounding_horror_damage_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_COMPOUNDING_HORROR_AURA
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Aura* aura = GetCaster()->GetAura(SPELL_WARLOCK_COMPOUNDING_HORROR_AURA))
                {
                    SetHitDamage(GetHitDamage() * aura->GetStackAmount());
                    aura->Remove();
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_compounding_horror_damage_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_compounding_horror_damage_SpellScript();
        }
};


// 17962 - Conflagrate
class spell_warl_legion_conflagrate : public SpellScriptLoader
{
    public:
        spell_warl_legion_conflagrate() : SpellScriptLoader("spell_warl_legion_conflagrate") { }

        class spell_warl_legion_conflagrate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_conflagrate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_BACKDRAFT,
                    SPELL_WARLOCK_BACKDRAFT_BONUS,
                    SPELL_WARLOCK_CONFLAGRATION_OF_CHAOS_BONUS
                });
            }

            void HandleAfterCast()
            {
                if (GetCaster()->HasAura(SPELL_WARLOCK_BACKDRAFT))
                {
                    if (Aura* aura = GetCaster()->GetAura(SPELL_WARLOCK_BACKDRAFT_BONUS))
                        aura->SetCharges(std::min(uint8(aura->GetStackAmount() + 2), uint8(aura->GetSpellInfo()->StackAmount)));
                    else if (Aura* auraAdd = GetCaster()->AddAura(SPELL_WARLOCK_BACKDRAFT_BONUS, GetCaster()))
                        auraAdd->SetCharges(2);
                }
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_WARLOCK_CONFLAGRATION_OF_CHAOS_BONUS))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), GetCaster()->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1)));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_conflagrate_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                AfterCast += SpellCastFn(spell_warl_legion_conflagrate_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_conflagrate_SpellScript();
        }
};

// 172 - Corruption
class spell_warl_legion_corruption : public SpellScriptLoader
{
    public:
        spell_warl_legion_corruption() : SpellScriptLoader("spell_warl_legion_corruption") { }

        class spell_warl_legion_corruption_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_corruption_SpellScript);

            void HandleAfterHit()
            {
                if (Aura* aura = GetHitUnit()->GetAura(SPELL_WARLOCK_CORRUPTION_AURA, GetCaster()->GetGUID()))
                {
                    if (GetCaster()->HasAura(SPELL_WARLOCK_ABSOLUTE_CORRUPTION))
                    {
                        if (aura->GetOwner()->GetTypeId() == TYPEID_PLAYER)
                        {
                            if (AuraEffect const* aurEff = GetCaster()->GetAuraEffect(SPELL_WARLOCK_ABSOLUTE_CORRUPTION, EFFECT_0))
                            {
                                aura->SetMaxDuration(aurEff->GetAmount() * 1000);
                                aura->SetDuration(aurEff->GetAmount() * 1000);
                            }
                        }
                        else
                        {
                            aura->SetMaxDuration(-1);
                            aura->SetDuration(-1);
                        }
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_warl_legion_corruption_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_corruption_SpellScript();
        }
};

// 146739 Corruption
class spell_warl_legion_corruption_periodic : public SpellScriptLoader
{
    public:
        spell_warl_legion_corruption_periodic() : SpellScriptLoader("spell_warl_legion_corruption_periodic") { }

        class spell_warl_legion_corruption_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_corruption_periodic_AuraScript);

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                // Absolute Corruption is acitve we cant that that better for offline case
                if (GetMaxDuration() == -1 || GetMaxDuration() == 40000)
                {
                    // Remove permanent dot if: caster is not alive, caster is not in world, not in same map or distance > 100m
                    if (Unit* caster = GetCaster())
                        if (caster->IsAlive())
                            if (caster->IsSelfOrInSameMap(GetTarget()))
                                if (caster->GetExactDist(GetTarget()) <= 100.00f)
                                    return;
                    Remove();
                };
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_corruption_periodic_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_corruption_periodic_AuraScript();
        }
};

// 6201 - Create Healthstone
// 34130 - Create Healthstone (Soulwell)
class spell_warl_legion_create_healthstone : public SpellScriptLoader
{
    public:
        spell_warl_legion_create_healthstone() : SpellScriptLoader("spell_warl_legion_create_healthstone") { }

        class spell_warl_legion_create_healthstone_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_create_healthstone_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_WARLOCK_CREATE_HEALTHSTONE });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                ObjectGuid ownerGuid;
                if (GetSpellInfo()->Id == 34130)
                    if (GameObject* soulwell = GetCaster()->FindNearestGameObject(GO_SOULWELL, 10.00f))
                        ownerGuid = soulwell->GetOwnerGUID();

                if (GetCaster()->ToPlayer()->HasItemCount(ITEM_HEALTHSTONE, 1))
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_CREATE_HEALTHSTONE_RECHARGE, true);
                else
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_CREATE_HEALTHSTONE, true, nullptr, nullptr, ownerGuid.IsEmpty() ? GetCaster()->GetGUID() : ownerGuid);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_create_healthstone_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_create_healthstone_SpellScript();
        }
};

// 212327 - Cremation
class spell_warl_legion_cremation_damage : public SpellScriptLoader
{
    public:
        spell_warl_legion_cremation_damage() : SpellScriptLoader("spell_warl_legion_cremation_damage") { }

        class spell_warl_legion_cremation_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_cremation_damage_SpellScript);

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(CalculatePct(GetHitUnit()->GetMaxHealth(), GetEffectInfo()->CalcValue(GetCaster())));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_cremation_damage_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_cremation_damage_SpellScript();
        }
};

// 234877 - Curse of Shadows
class spell_warl_legion_curse_of_shadows : public SpellScriptLoader
{
    public:
        spell_warl_legion_curse_of_shadows() : SpellScriptLoader("spell_warl_legion_curse_of_shadows") { }

        class spell_warl_legion_curse_of_shadows_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_curse_of_shadows_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_CURSE_OF_SHADOWS_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(SPELL_WARLOCK_CURSE_OF_SHADOWS_DAMAGE, SPELLVALUE_BASE_POINT0, CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()), GetTarget(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_curse_of_shadows_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_curse_of_shadows_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_curse_of_shadows_AuraScript();
        }
};

// 108416 - Dark Pact
class spell_warl_legion_dark_pact : public SpellScriptLoader
{
    public:
        spell_warl_legion_dark_pact() : SpellScriptLoader("spell_warl_legion_dark_pact") { }

        class spell_warl_legion_dark_pact_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_dark_pact_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                if (Player* caster = GetUnitOwner()->ToPlayer())
                {
                    if (Pet* pet = caster->GetPet())
                    {
                        int32 absorb = pet->CountPctFromCurHealth(GetAura()->GetSpellEffectInfo(EFFECT_1)->CalcValue(caster));
                        pet->SetHealth(pet->GetHealth() - absorb);
                        amount = CalculatePct(absorb, aurEff->GetBaseAmount());
                    }
                    else
                    {
                        int32 absorb = caster->CountPctFromCurHealth(GetAura()->GetSpellEffectInfo(EFFECT_1)->CalcValue(caster));
                        caster->SetHealth(caster->GetHealth() - absorb);
                        amount = CalculatePct(absorb, aurEff->GetBaseAmount());
                    }
                }
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_warl_legion_dark_pact_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_dark_pact_AuraScript();
        }
};

// 157695 - Demonbolt
class spell_warl_legion_demonbolt : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonbolt() : SpellScriptLoader("spell_warl_legion_demonbolt") { }

        class spell_warl_legion_demonbolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_demonbolt_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DEMONWRATH_CHANNEL,
                    SPELL_WARLOCK_DEMONWRATH_ENERGIZE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _minionCount = targets.size();
                targets.clear(); // prevent damage visual on pets
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), GetEffectInfo(EFFECT_2)->CalcValue(GetCaster()) * _minionCount));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_demonbolt_SpellScript::FilterTargets, EFFECT_2, TARGET_UNIT_MINIONS);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_demonbolt_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

        private:
            uint32 _minionCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_demonbolt_SpellScript();
        }
};

// 193439 - Demonwrath
class spell_warl_legion_demonwrath : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonwrath() : SpellScriptLoader("spell_warl_legion_demonwrath") { }

        class spell_warl_legion_demonwrath_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_demonwrath_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DEMONWRATH_CHANNEL,
                    SPELL_WARLOCK_DEMONWRATH_ENERGIZE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    for (auto itr : caster->GetTargetAuraApplications(SPELL_WARLOCK_DEMONWRATH_CHANNEL))
                        if (itr->GetTarget()->GetGUID() != caster->GetGUID())
                            if (itr->GetTarget()->GetDistance(target) <= float(itr->GetBase()->GetEffect(EFFECT_0)->GetAmount()))
                                return false;
                    return true;
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (roll_chance_i(sSpellMgr->AssertSpellInfo(SPELL_WARLOCK_DEMONWRATH_CHANNEL)->GetEffect(EFFECT_2)->CalcValue(GetCaster())))
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_DEMONWRATH_ENERGIZE, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_demonwrath_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_demonwrath_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_demonwrath_SpellScript();
        }
};

// 19505 - Devour Magic
 class spell_warl_legion_devour_magic : public SpellScriptLoader
{
    public:
        spell_warl_legion_devour_magic() : SpellScriptLoader("spell_warl_legion_devour_magic") { }

        class spell_warl_legion_devour_magic_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_devour_magic_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DEVOUR_MAGIC_HEAL
                });
            }

            void OnSuccessfulDispel(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_DEVOUR_MAGIC_HEAL, true);
            }

            void Register() override
            {
                OnEffectSuccessfulDispel += SpellEffectFn(spell_warl_legion_devour_magic_SpellScript::OnSuccessfulDispel, EFFECT_0, SPELL_EFFECT_DISPEL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_devour_magic_SpellScript();
        }
};

 // 196586 - Dimensional Rift
class spell_warl_legion_dimensional_rift : public SpellScriptLoader
{
    public:
        spell_warl_legion_dimensional_rift() : SpellScriptLoader("spell_warl_legion_dimensional_rift") { }

        class spell_warl_legion_dimensional_rift_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_dimensional_rift_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_CHAOS_TEAR,
                    SPELL_WARLOCK_UNSTABLE_TEAR,
                    SPELL_WARLOCK_SHADOWY_TEAR,
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                uint32 portalId = RAND(SPELL_WARLOCK_CHAOS_TEAR, SPELL_WARLOCK_UNSTABLE_TEAR, SPELL_WARLOCK_SHADOWY_TEAR);
                SpellInfo const* portalSpellInfo = sSpellMgr->AssertSpellInfo(portalId);
                Position summonPos = GetCaster()->GetRandomNearPosition(10.00f);
                SummonPropertiesEntry const* properties = sSummonPropertiesStore.LookupEntry(portalSpellInfo->GetEffect(EFFECT_0)->MiscValueB);
                if (TempSummon* summon = GetCaster()->GetMap()->SummonCreature(portalSpellInfo->GetEffect(EFFECT_0)->MiscValue, summonPos, properties, portalSpellInfo->GetMaxDuration(), GetCaster(), portalId))
                    if (summon->IsAIEnabled)
                        summon->AI()->SetGUID(GetHitUnit()->GetGUID());

                // Update focus target for all spawned portals
                for (auto itr : GetCaster()->tempSummonList)
                    if (Creature* summon = ObjectAccessor::GetCreature(*GetCaster(), itr.first))
                        if (summon->GetEntry() == ENTRY_CHAOS_TEAR || summon->GetEntry() == ENTRY_SHADOWY_TEAR || summon->GetEntry() == ENTRY_UNSTABLE_TEAR)
                            if (summon->IsAIEnabled)
                                summon->AI()->SetGUID(GetHitUnit()->GetGUID());
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_dimensional_rift_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_dimensional_rift_SpellScript();
        }
};

// 205145 - Demonic Calling
class spell_warl_legion_demonic_calling : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonic_calling() : SpellScriptLoader("spell_warl_legion_demonic_calling") { }

        class spell_warl_legion_demonic_calling_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_demonic_calling_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SHADOW_BOLT,
                    SPELL_WARLOCK_DEMONWRATH_DAMAGE
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetSpellInfo())
                    return false;

                if (eventInfo.GetSpellInfo()->Id == SPELL_WARLOCK_SHADOW_BOLT)
                    return roll_chance_i(GetSpellInfo()->ProcChance);

                if (eventInfo.GetSpellInfo()->Id == SPELL_WARLOCK_DEMONWRATH_DAMAGE)
                    return roll_chance_i(GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(GetUnitOwner()));

                return false;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_demonic_calling_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_demonic_calling_AuraScript();
        }
};

// 193396 - Demonic Empowerment
class spell_warl_legion_demonic_empowerment : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonic_empowerment() : SpellScriptLoader("spell_warl_legion_demonic_empowerment") { }

        class spell_warl_legion_demonic_empowerment_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_demonic_empowerment_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_POWER_TRIP,
                    SPELL_WARLOCK_POWER_TRIP_ENERGIZE,
                    SPELL_WARLOCK_SHADOWY_INSPIRATION,
                    SPELL_WARLOCK_SHADOWY_INSPIRATION_BONUS
                });
            }

            void HandleAfterCast()
            {
                if (AuraEffect* powerTrip = GetCaster()->GetAuraEffect(SPELL_WARLOCK_POWER_TRIP, EFFECT_0))
                    if (roll_chance_i(powerTrip->GetAmount()))
                        GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_POWER_TRIP_ENERGIZE, true);

                if (GetCaster()->HasAura(SPELL_WARLOCK_SHADOWY_INSPIRATION))
                    GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_SHADOWY_INSPIRATION_BONUS, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warl_legion_demonic_empowerment_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_demonic_empowerment_SpellScript();
        }
};

// 196099 - Demonic Power
class spell_warl_legion_demonic_power : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonic_power() : SpellScriptLoader("spell_warl_legion_demonic_power") { }

        class spell_warl_legion_demonic_power_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_demonic_power_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalcPeriodic(AuraEffect const* /*aurEff*/, bool& isPeriodic, int32& amplitude)
            {
                isPeriodic = true;
                amplitude = 1000;
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                if (GetTarget()->ToPlayer()->GetPet())
                    Remove();
            }

            void HandleUpdatePeriodic(AuraEffect* aurEff)
            {
                aurEff->CalculatePeriodic(GetCaster());
            }

            void Register() override
            {
                DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_warl_legion_demonic_power_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_demonic_power_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
                OnEffectUpdatePeriodic += AuraEffectUpdatePeriodicFn(spell_warl_legion_demonic_power_AuraScript::HandleUpdatePeriodic, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_demonic_power_AuraScript();
        }
};

// 48018 - Demonic Circle: Summon
class spell_warl_legion_demonic_circle_summon : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonic_circle_summon() : SpellScriptLoader("spell_warl_legion_demonic_circle_summon") { }

        class spell_warl_legion_demonic_circle_summon_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_demonic_circle_summon_AuraScript);

            void HandleTeleport(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST, true);
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes mode)
            {
                // If effect is removed by expire remove the summoned demonic circle too.
                if (!(mode & AURA_EFFECT_HANDLE_REAPPLY))
                    GetTarget()->RemoveGameObject(GetId(), true);

                GetTarget()->RemoveAura(SPELL_WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST);
            }

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                if (GameObject* circle = GetTarget()->GetGameObject(GetId()))
                {
                    // Here we check if player is in demonic circle teleport range, if so add
                    // WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST; allowing him to cast the WARLOCK_DEMONIC_CIRCLE_TELEPORT.
                    // If not in range remove the WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST.

                    SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(SPELL_WARLOCK_DEMONIC_CIRCLE_TELEPORT);

                    if (GetTarget()->IsWithinDist(circle, spellInfo->GetMaxRange(true)))
                    {
                        if (!GetTarget()->HasAura(SPELL_WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST))
                            GetTarget()->CastSpell(GetTarget(), SPELL_WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST, true);
                    }
                    else
                        GetTarget()->RemoveAura(SPELL_WARLOCK_DEMONIC_CIRCLE_ALLOW_CAST);
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_warl_legion_demonic_circle_summon_AuraScript::HandleTeleport, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectApplyFn(spell_warl_legion_demonic_circle_summon_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_demonic_circle_summon_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_demonic_circle_summon_AuraScript();
        }
};

// 48020 - Demonic Circle: Teleport
class spell_warl_legion_demonic_circle_teleport : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonic_circle_teleport() : SpellScriptLoader("spell_warl_legion_demonic_circle_teleport") { }

        class spell_warl_legion_demonic_circle_teleport_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_demonic_circle_teleport_AuraScript);

            void HandleTeleport(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Player* player = GetTarget()->ToPlayer())
                {
                    if (GameObject* circle = player->GetGameObject(SPELL_WARLOCK_DEMONIC_CIRCLE_SUMMON))
                    {
                        player->NearTeleportTo(circle->GetPositionX(), circle->GetPositionY(), circle->GetPositionZ(), circle->GetOrientation());
                        player->RemoveMovementImpairingAuras();
                    }
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_warl_legion_demonic_circle_teleport_AuraScript::HandleTeleport, EFFECT_0, SPELL_AURA_MECHANIC_IMMUNITY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_demonic_circle_teleport_AuraScript();
        }
};

// 111771 - Demonic Gateway
class spell_warl_legion_demonic_gateway : public SpellScriptLoader
{
    public:
        spell_warl_legion_demonic_gateway() : SpellScriptLoader("spell_warl_legion_demonic_gateway") { }

        class spell_warl_legion_demonic_gateway_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_demonic_gateway_SpellScript);

            void DespawnPortal(SpellEffIndex /*effIndex*/)
            {
                for (auto& itr : GetCaster()->tempSummonList)
                    if (Creature* creature = ObjectAccessor::GetCreature(*GetCaster(), itr.first))
                        if (creature->GetEntry() == NPC_DEMONIC_GATEWAY_DEST || creature->GetEntry() == NPC_DEMONIC_GATEWAY_CASTER || creature->GetEntry() == NPC_DEMONIC_GATEWAY_TARGET)
                            creature->DespawnOrUnsummon();

                if (WorldLocation* loc = GetHitDest())
                {
                    Position targetPos = { loc->GetPositionX(), loc->GetPositionY(), loc->GetPositionZ() };
                    GetCaster()->SendPlaySpellVisual(targetPos, 0.0f, SPELL_VISUAL_DEMONIC_GATEWAY_DEST_SUMMON, 0, 0, 20.00f, false);
                }
            }

            void HandleSummon(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);

                if (WorldLocation* loc = GetHitDest())
                    GetCaster()->CastSpell(loc->GetPositionX(), loc->GetPositionY(), loc->GetPositionZ(), SPELL_WARLOCK_DEMONIC_PORTAL_SUMMON_PLAYER_POS, true);

                GetCaster()->CastSpell(GetCaster(), GetEffectInfo()->TriggerSpell, true);
            }

            void Register() override
            {
                OnEffectLaunch += SpellEffectFn(spell_warl_legion_demonic_gateway_SpellScript::DespawnPortal, EFFECT_0, SPELL_EFFECT_SUMMON);
                OnEffectLaunch += SpellEffectFn(spell_warl_legion_demonic_gateway_SpellScript::HandleSummon, EFFECT_1, SPELL_EFFECT_TRIGGER_SPELL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_demonic_gateway_SpellScript();
        }
};

// 219272 - Demon Skin
class spell_warl_legion_demon_skin : public SpellScriptLoader
{
    public:
        spell_warl_legion_demon_skin() : SpellScriptLoader("spell_warl_legion_demon_skin") { }

        class spell_warl_legion_demon_skin_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_demon_skin_AuraScript);

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                for (auto itr : GetTarget()->GetTargetAuraApplications(SPELL_WARLOCK_SOUL_LEECH))
                {
                    int32 absorbAmount = itr->GetTarget()->CountPctFromMaxHealth(aurEff->GetAmount());
                    if (AuraEffect* absorb = itr->GetTarget()->GetAuraEffect(SPELL_WARLOCK_SOUL_LEECH_SHIELD, EFFECT_0))
                        absorbAmount += absorb->GetAmount();

                    absorbAmount = std::min(absorbAmount, int32(itr->GetTarget()->CountPctFromMaxHealth(GetEffect(EFFECT_1)->GetAmount())));
                    if (AuraEffect* shieldEffect = itr->GetTarget()->GetAuraEffect(SPELL_WARLOCK_SOUL_LEECH_SHIELD, EFFECT_0))
                    {
                        if (shieldEffect->GetBase()->GetDuration() != -1)
                        {
                            shieldEffect->GetBase()->SetMaxDuration(-1);
                            shieldEffect->GetBase()->SetDuration(-1);
                        }
                        shieldEffect->ChangeAmount(absorbAmount);
                    }
                    else if (Aura* shield = GetTarget()->AddAura(SPELL_WARLOCK_SOUL_LEECH_SHIELD, itr->GetTarget()))
                    {
                        shield->SetDuration(-1);
                        shield->GetEffect(EFFECT_0)->ChangeAmount(absorbAmount);
                    }
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_demon_skin_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_demon_skin_AuraScript();
        }
};

// 219415 - Dimension Ripper
class spell_warl_legion_dimension_ripper : public SpellScriptLoader
{
    public:
        spell_warl_legion_dimension_ripper() : SpellScriptLoader("spell_warl_legion_dimension_ripper") { }

        class spell_warl_legion_dimension_ripper_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_dimension_ripper_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DIMENSIONAL_RIFT
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_WARLOCK_DIMENSIONAL_RIFT)->ChargeCategoryId);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_dimension_ripper_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_dimension_ripper_AuraScript();
        }
};

// 603 - Doom
class spell_warl_legion_doom : public SpellScriptLoader
{
    public:
        spell_warl_legion_doom() : SpellScriptLoader("spell_warl_legion_doom") { }

        class spell_warl_legion_doom_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_doom_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DOOM_ENERGIZE,
                    SPELL_WARLOCk_IMPENDING_DOOM,
                    SPELL_HOTFIX_WILD_IMP_TARGET_MARKER,
                    SPELL_WARLOCK_HAND_OF_GULDAN_SUMMON_IMP
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->CastSpell(caster, SPELL_WARLOCK_DOOM_ENERGIZE, true);

                    if (caster->HasAura(SPELL_WARLOCk_IMPENDING_DOOM))
                    {
                        caster->CastSpell(GetTarget(), SPELL_HOTFIX_WILD_IMP_TARGET_MARKER, true);
                        caster->CastSpell(GetTarget(), SPELL_WARLOCK_HAND_OF_GULDAN_SUMMON_IMP, true);
                    }
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_doom_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_doom_AuraScript();
        }
};

// 85692 - Doom Bolt
class spell_warl_legion_doom_bolt : public SpellScriptLoader
{
    public:
        spell_warl_legion_doom_bolt() : SpellScriptLoader("spell_warl_legion_doom_bolt") { }

        class spell_warl_legion_doom_bolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_doom_bolt_SpellScript);

            void HandleBonusDamage(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit()->HealthBelowPct(GetEffectInfo(EFFECT_1)->CalcValue(GetCaster())))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), GetEffectInfo(EFFECT_1)->CalcValue(GetCaster())));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_doom_bolt_SpellScript::HandleBonusDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_doom_bolt_SpellScript();
        }
};

// 234153 - Drain Life
class spell_warl_legion_drain_life : public SpellScriptLoader
{
    public:
        spell_warl_legion_drain_life() : SpellScriptLoader("spell_warl_legion_drain_life") { }

        class spell_warl_legion_drain_life_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_drain_life_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DEVOURER_OF_LIFE_PROC_AURA,
                    SPELL_WARLOCK_DEVOURER_OF_LIFE_BONUS
                });
            }

            void HandleAfterCast()
            {
                if (AuraEffect* devourerOfLife = GetCaster()->GetAuraEffect(SPELL_WARLOCK_DEVOURER_OF_LIFE_PROC_AURA, EFFECT_0))
                    if (roll_chance_i(devourerOfLife->GetAmount()))
                        GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_DEVOURER_OF_LIFE_BONUS, true); // this effect needs to be applied BEFORE the periodic begins thats why we hackfix it here... should be fixed on the right way after aura system is rewritten
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warl_legion_drain_life_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_drain_life_SpellScript();
        }
};

// 198590 - Drain Soul
class spell_warl_legion_drain_soul : public SpellScriptLoader
{
    public:
        spell_warl_legion_drain_soul() : SpellScriptLoader("spell_warl_legion_drain_soul") { }

        class spell_warl_legion_drain_soul_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_drain_soul_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DRAIN_SOUL_ENERGIZE,
                    SPELL_WARLOCK_ROT_AND_DECAY
                });
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                {
                    if (AuraEffect* rotAndDecay = caster->GetAuraEffect(SPELL_WARLOCK_ROT_AND_DECAY, EFFECT_0))
                    {
                        for (uint32 unstableAfflictionId : UnstableAfflictionHelper)
                            if (Aura* aura = GetTarget()->GetAura(unstableAfflictionId, GetCasterGUID()))
                                aura->SetDuration(std::min(aura->GetDuration() + rotAndDecay->GetAmount(), aura->GetMaxDuration()));

                        if (Aura* aura = GetTarget()->GetAura(SPELL_WARLOCK_CORRUPTION_AURA, GetCasterGUID()))
                            aura->SetDuration(std::min(aura->GetDuration() + rotAndDecay->GetAmount(), aura->GetMaxDuration()));

                        if (Aura* aura = GetTarget()->GetAura(SPELL_WARLOCK_AGONY, GetCasterGUID()))
                            aura->SetDuration(std::min(aura->GetDuration() + rotAndDecay->GetAmount(), aura->GetMaxDuration()));
                    }
                }
            }

            void HandleRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_WARLOCK_DRAIN_SOUL_ENERGIZE, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_drain_soul_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_LEECH);
                OnEffectRemove += AuraEffectApplyFn(spell_warl_legion_drain_soul_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_LEECH, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_drain_soul_AuraScript();
        }
};

// 233581 - Entrenched in Flame
class spell_warl_legion_entrenched_in_flame : public SpellScriptLoader
{
    public:
        spell_warl_legion_entrenched_in_flame() : SpellScriptLoader("spell_warl_legion_entrenched_in_flame") { }

        class spell_warl_legion_entrenched_in_flame_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_entrenched_in_flame_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_entrenched_in_flame_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_entrenched_in_flame_AuraScript();
        }
};

// 196412 - Eradication
class spell_warl_legion_eradication : public SpellScriptLoader
{
    public:
        spell_warl_legion_eradication() : SpellScriptLoader("spell_warl_legion_eradication") { }

        class spell_warl_legion_eradication_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_eradication_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetActionTarget())
                    return false;

                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_eradication_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_eradication_AuraScript();
        }
};

// 221711 - Essence Drain
class spell_warl_legion_essence_drain : public SpellScriptLoader
{
    public:
        spell_warl_legion_essence_drain() : SpellScriptLoader("spell_warl_legion_essence_drain") { }

        class spell_warl_legion_essence_drain_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_essence_drain_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_ESSENCE_DRAIN_DEBUFF
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->CastSpell(eventInfo.GetActionTarget(), SPELL_WARLOCK_ESSENCE_DRAIN_DEBUFF, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_essence_drain_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_essence_drain_AuraScript();
        }
};

// 205231 - Eye Laser
class spell_warl_legion_eye_laser : public SpellScriptLoader
{
    public:
        spell_warl_legion_eye_laser() : SpellScriptLoader("spell_warl_legion_eye_laser") { }

        class spell_warl_legion_eye_laser_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_eye_laser_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_DOOM
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_WARLOCK_DOOM, GetCaster()->GetOwnerGUID()));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_eye_laser_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_TARGET_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_eye_laser_SpellScript();
        }
};

// 5782 - Fear
class spell_warl_legion_fear : public SpellScriptLoader
{
    public:
        spell_warl_legion_fear() : SpellScriptLoader("spell_warl_legion_fear") { }

        class spell_warl_legion_fear_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_fear_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo({ SPELL_WARLOCK_FEAR_TRIGGERED });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_FEAR_TRIGGERED, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_fear_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_fear_SpellScript();
        }
};

// 213688 - Fel Cleave
class spell_warl_legion_fel_cleave : public SpellScriptLoader
{
    public:
        spell_warl_legion_fel_cleave() : SpellScriptLoader("spell_warl_legion_fel_cleave") { }

        class spell_warl_legion_fel_cleave_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_fel_cleave_SpellScript);

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(CalculatePct(GetHitUnit()->GetMaxHealth(), GetEffectValue()));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_fel_cleave_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_fel_cleave_SpellScript();
        }
};

// 200586 - Fel Fissure
class spell_warl_legion_fel_fissure : public SpellScriptLoader
{
    public:
        spell_warl_legion_fel_fissure() : SpellScriptLoader("spell_warl_legion_fel_fissure") { }

        class spell_warl_legion_fel_fissure_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_fel_fissure_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return GetTarget()->IsValidAttackTarget(eventInfo.GetActionTarget());
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_fel_fissure_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_fel_fissure_AuraScript();
        }
};

// 226044 - Fixate
class spell_warl_legion_fixate : public SpellScriptLoader
{
    public:
        spell_warl_legion_fixate() : SpellScriptLoader("spell_warl_legion_fixate") { }

        class spell_warl_legion_fixate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_fixate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_HOTFIX_WILD_IMP_TARGET_MARKER
                });
            }

            bool Load() override
            {
                return GetCaster()->GetEntry() == NPC_WILD_IMP;
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_HOTFIX_WILD_IMP_TARGET_MARKER, GetCaster()->GetOwnerGUID()));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_fixate_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_fixate_SpellScript();
        }
};

// 152107 - Grimoire of Supremacy
class spell_warl_legion_grimoire_of_supremacy : public SpellScriptLoader
{
    public:
        spell_warl_legion_grimoire_of_supremacy() : SpellScriptLoader("spell_warl_legion_grimoire_of_supremacy") { }

        class spell_warl_legion_grimoire_of_supremacy_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_grimoire_of_supremacy_AuraScript);

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                int32 spellOverrideEff0 = 0;
                int32 spellOverrideEff1 = 0;

                switch (GetTarget()->ToPlayer()->GetUInt32Value(PLAYER_FIELD_CURRENT_SPEC_ID))
                {
                    case TALENT_SPEC_WARLOCK_AFFLICTION:
                    case TALENT_SPEC_WARLOCK_DEMONOLOGY:   /// @TODO: needs sniff
                    case TALENT_SPEC_WARLOCK_DESTRUCTION:  /// @TODO: needs sniff
                        spellOverrideEff0 = GetEffect(EFFECT_2)->GetAmount();
                        spellOverrideEff1 = aurEff->GetAmount();
                        break;
                    default:
                        break;
                }

                GetTarget()->CastCustomSpell(GetTarget(), SPELL_WARLOCK_WARLOCK, &spellOverrideEff0, &spellOverrideEff1, nullptr, true);
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAura(SPELL_WARLOCK_WARLOCK);

                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    if (pet->GetEntry() == NPC_INFERNAL || pet->GetEntry() == NPC_DOOMGUARD)
                        pet->DespawnOrUnsummon();
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_warl_legion_grimoire_of_supremacy_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectApplyFn(spell_warl_legion_grimoire_of_supremacy_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_grimoire_of_supremacy_AuraScript();
        }
};

//  348 - Immolate
//  6789 - Mortal Coil
//  17877 - Shadowburn
//  17962 - Conflagrate
//  29722 - Incinerate
//  116858 - Chaos Bolt
class spell_warl_legion_havoc_trigger : public SpellScriptLoader
{
    public:
        spell_warl_legion_havoc_trigger() : SpellScriptLoader("spell_warl_legion_havoc_trigger") { }

        class spell_warl_legion_havoc_trigger_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_havoc_trigger_SpellScript);

            void HandleAfterCast()
            {
                if (!GetExplTargetUnit())
                    return;

                if (GetSpellInfo()->Id == SPELL_WARLOCK_CHAOS_BOLT && GetCaster()->HasAura(SPELL_WARLOCK_FOCUSED_CHAOS))
                    return;

                if (GetExplTargetUnit()->HasAura(SPELL_WARLOCK_HAVOC, GetCaster()->GetGUID()) || GetExplTargetUnit()->HasAura(SPELL_WARLOCK_BANE_OF_HAVOC_AURA, GetCaster()->GetGUID()))
                    return;

                if (GetSpellInfo()->Id == SPELL_WARLOCK_INCINERATE && GetCaster()->HasAura(SPELL_WARLOCK_FIRE_AND_BRIMSTONE))
                    return;

                // Havoc - "causing your single target spells to also strike the Havoc victim. Limit 1."
                Unit::AuraList const& auras = GetCaster()->GetSingleCastAuras();
                auto itr = std::find_if(auras.begin(), auras.end(), [](Aura const* aura) { return aura->GetId() == SPELL_WARLOCK_HAVOC; });
                if (itr != auras.end())
                    GetCaster()->CastSpell((*itr)->GetUnitOwner(), GetSpellInfo()->Id, true);
                // Bane of Havoc (Honor talent) - "Curses the ground with a demonic bane, causing all of your single target spells to also strike targets marked with the bane. Lasts 10 sec."
                else if (AreaTrigger* trigger = GetCaster()->GetAreaTrigger(SPELL_WARLOCK_BANE_OF_HAVOC))
                {
                    for (ObjectGuid const& guid : trigger->GetInsideUnits())
                        if (Unit* target = ObjectAccessor::GetUnit(*GetCaster(), guid))
                            if (GetCaster()->IsValidAttackTarget(target))
                                GetCaster()->CastSpell(target, GetSpellInfo()->Id, true);
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warl_legion_havoc_trigger_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_havoc_trigger_SpellScript();
        }
};

// 105174 - Hand of Gul'dan
class spell_warl_legion_hand_of_guldan : public SpellScriptLoader
{
    public:
        spell_warl_legion_hand_of_guldan() : SpellScriptLoader("spell_warl_legion_hand_of_guldan") { }

        class spell_warl_legion_hand_of_guldan_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_hand_of_guldan_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_HAND_OF_GULDAN_DAMAGE,
                    SPELL_WARLOCK_HAND_OF_GULDAN_SUMMON_IMP,
                    SPELL_HOTFIX_WILD_IMP_TARGET_MARKER,
                    SPELL_WARLOCK_HAND_OF_GULDAN_CASTER_AURA
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastCustomSpell(SPELL_WARLOCK_HAND_OF_GULDAN_CASTER_AURA, SPELLVALUE_BASE_POINT0, GetSpell()->GetPowerCost(POWER_SOUL_SHARDS), GetCaster(), TRIGGERED_FULL_MASK);
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_HAND_OF_GULDAN_DAMAGE, true);
                GetCaster()->CastSpell(GetHitUnit(), SPELL_HOTFIX_WILD_IMP_TARGET_MARKER, true);
                int32 impCount = GetSpell()->GetPowerCost(POWER_SOUL_SHARDS);
                for (int32 i = 0; i < impCount; i++)
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_HAND_OF_GULDAN_SUMMON_IMP, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_hand_of_guldan_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_hand_of_guldan_SpellScript();
        }
};

// 86040 - Hand of Gul'dan
class spell_warl_legion_hand_of_guldan_damage : public SpellScriptLoader
{
    public:
        spell_warl_legion_hand_of_guldan_damage() : SpellScriptLoader("spell_warl_legion_hand_of_guldan_damage") { }

        class spell_warl_legion_hand_of_guldan_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_hand_of_guldan_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_HAND_OF_DOOM
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                hitCount = targets.size();
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_WARLOCK_HAND_OF_DOOM))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_DOOM, true);

                if (AuraEffect* handOfGuldanAura = GetCaster()->GetAuraEffect(SPELL_WARLOCK_HAND_OF_GULDAN_CASTER_AURA, EFFECT_0))
                    SetHitDamage(GetHitDamage() * handOfGuldanAura->GetAmount() / hitCount);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_hand_of_guldan_damage_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_hand_of_guldan_damage_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

            private:
                int32 hitCount = 0;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_hand_of_guldan_damage_SpellScript();
        }
};

// 48181 - Haunt
class spell_warl_legion_haunt : public SpellScriptLoader
{
    public:
        spell_warl_legion_haunt() : SpellScriptLoader("spell_warl_legion_haunt") { }

        class spell_warl_legion_haunt_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_haunt_AuraScript);

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_DEATH)
                    if (Unit* caster = GetCaster())
                        caster->GetSpellHistory()->ResetCooldown(GetId(), true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_warl_legion_haunt_AuraScript::HandleRemove, EFFECT_1, SPELL_AURA_MOD_DAMAGE_PCT_TAKEN_FROM_CASTER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_haunt_AuraScript();
        }
};

// 6262 - Healthstone
class spell_warl_legion_healthstone : public SpellScriptLoader
{
    public:
        spell_warl_legion_healthstone() : SpellScriptLoader("spell_warl_legion_healthstone") { }

        class spell_warl_legion_healthstone_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_healthstone_SpellScript);

            void HandleOnPrepare()
            {
                if (Item* healthstone = GetCaster()->ToPlayer()->GetItemByEntry(ITEM_HEALTHSTONE))
                    itemOwnerGuid = healthstone->GetGuidValue(ITEM_FIELD_CREATOR);
            }

            void HandleOnHit()
            {
                if (Unit* owner = ObjectAccessor::GetUnit(*GetCaster(), itemOwnerGuid))
                    if (AuraEffect* sweetSouls = owner->GetAuraEffect(SPELL_WARLOCK_SWEET_SOULS, EFFECT_0))
                        if (itemOwnerGuid == GetCaster()->GetGUID())
                            SetHitHeal(GetHitHeal() + CalculatePct(GetCaster()->GetMaxHealth(), sweetSouls->GetAmount()));
            }

            void Register() override
            {
                OnPrepare += SpellPrepareFn(spell_warl_legion_healthstone_SpellScript::HandleOnPrepare);
                OnHit += SpellHitFn(spell_warl_legion_healthstone_SpellScript::HandleOnHit);
            }
            private:
                ObjectGuid itemOwnerGuid;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_healthstone_SpellScript();
        }
};

// 755 - Health Funnel
class spell_warl_legion_health_funnel : public SpellScriptLoader
{
    public:
        spell_warl_legion_health_funnel() : SpellScriptLoader("spell_warl_legion_health_funnel") { }

        class spell_warl_legion_health_funnel_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_health_funnel_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_WARLOCK_HEALTH_FUNNEL_TRIGGERED });
            }

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                if (Unit* caster = GetCaster())
                {
                    SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(SPELL_WARLOCK_HEALTH_FUNNEL_TRIGGERED);

                    if (!spellInfo->GetEffect(EFFECT_0) || !spellInfo->GetEffect(EFFECT_1))
                        return;

                    int32 damage = CalculatePct(caster->GetMaxHealth(), spellInfo->GetEffect(EFFECT_0)->CalcValue());
                    int32 heal = CalculatePct(caster->GetMaxHealth(), spellInfo->GetEffect(EFFECT_1)->CalcValue());

                    GetCaster()->CastCustomSpell(GetTarget(), SPELL_WARLOCK_HEALTH_FUNNEL_TRIGGERED, &damage, &heal, nullptr, true, nullptr, aurEff);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_health_funnel_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_health_funnel_AuraScript();
        }
};

// 348 - Immolate
class spell_warl_legion_immolate : public SpellScriptLoader
{
    public:
        spell_warl_legion_immolate() : SpellScriptLoader("spell_warl_legion_immolate") { }

        class spell_warl_legion_immolate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_immolate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_IMMOLATE_PERIODIC
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_IMMOLATE_PERIODIC, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_immolate_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_immolate_SpellScript();
        }
};

// 157736 - Immolate
class spell_warl_legion_immolate_periodic : public SpellScriptLoader
{
    public:
        spell_warl_legion_immolate_periodic() : SpellScriptLoader("spell_warl_legion_immolate_periodic") { }

        class spell_warl_legion_immolate_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_immolate_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_IMMOLATE_ACTIVATOR
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_WARLOCK_IMMOLATE_ACTIVATOR, true);

                GetTarget()->RemoveAurasDueToSpell(SPELL_WARLOCK_ROARING_BLAZE_PCT_BONUS, GetCasterGUID());
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->GetTargetAuraApplications(GetId()).empty())
                        caster->RemoveAurasDueToSpell(SPELL_WARLOCK_IMMOLATE_ACTIVATOR);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_warl_legion_immolate_periodic_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_warl_legion_immolate_periodic_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_immolate_periodic_AuraScript();
        }
};

// 193541 - Immolate
class spell_warl_legion_immolate_proc : public SpellScriptLoader
{
    public:
        spell_warl_legion_immolate_proc() : SpellScriptLoader("spell_warl_legion_immolate_proc") { }

        class spell_warl_legion_immolate_proc_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_immolate_proc_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                float procChange = GetEffect(EFFECT_0)->GetAmount();
                if (eventInfo.GetHitMask() & PROC_HIT_CRITICAL)
                    procChange *= 2;
                return roll_chance_i(procChange);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_immolate_proc_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_immolate_proc_AuraScript();
        }
};

// 196277 - Implosion
class spell_warl_legion_implosion : public SpellScriptLoader
{
    public:
        spell_warl_legion_implosion() : SpellScriptLoader("spell_warl_legion_implosion") { }

        class spell_warl_legion_implosion_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_implosion_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_IMMOLATE_PERIODIC
                });
            }

            SpellCastResult CheckElevation()
            {
                for (Unit* minion : GetCaster()->m_Controlled)
                    if (minion->GetEntry() == NPC_WILD_IMP)
                        if (GetCaster()->IsWithinDistInMap(minion, GetEffectInfo(EFFECT_1)->CalcRadius(GetCaster())))
                            return SPELL_CAST_OK;

                return SPELL_FAILED_DONT_REPORT;
            }

            void HandleLaunch(SpellEffIndex /*effIndex*/)
            {
                targetGuid = GetHitUnit()->GetGUID();
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetHitUnit() == GetCaster())
                    return;

                if (Unit* jumpTarget = ObjectAccessor::GetUnit(*GetCaster(), targetGuid))
                {
                    GetHitUnit()->InterruptNonMeleeSpells(false);
                    if (CharmInfo* charmInfo = GetHitUnit()->GetCharmInfo())
                        if (SpellInfo const* felFirebolt = sSpellMgr->AssertSpellInfo(SPELL_WARLOCK_PET_FEL_FIREBOLT))
                            charmInfo->ToggleCreatureAutocast(felFirebolt, false);

                    GetHitUnit()->GetMotionMaster()->MoveJump(jumpTarget->GetPosition(), 50.0f, 20.0f, EVENT_JUMP);
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warl_legion_implosion_SpellScript::CheckElevation);
                OnEffectLaunchTarget += SpellEffectFn(spell_warl_legion_implosion_SpellScript::HandleLaunch, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_implosion_SpellScript::HandleOnEffectHit, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
            private:
                ObjectGuid targetGuid;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_implosion_SpellScript();
        }
};

// 29722 - Incinerate
class spell_warl_legion_incinerate : public SpellScriptLoader
{
    public:
        spell_warl_legion_incinerate() : SpellScriptLoader("spell_warl_legion_incinerate") { }

        class spell_warl_legion_incinerate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_incinerate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_FIRE_AND_BRIMSTONE,
                    SPELL_WARLOCK_CREMATION,
                    SPELL_WARLOCK_IMMOLATE_PERIODIC
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (!GetCaster()->HasAura(SPELL_WARLOCK_FIRE_AND_BRIMSTONE))
                {
                    targets.clear();
                    targets.push_back(GetExplTargetUnit());
                }
            }


            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_WARLOCK_CREMATION))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_IMMOLATE_PERIODIC, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_incinerate_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_incinerate_SpellScript::HandleOnEffectHit, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_incinerate_SpellScript();
        }
};

// 1454 - Life Tap
class spell_warl_legion_life_tap : public SpellScriptLoader
{
    public:
        spell_warl_legion_life_tap() : SpellScriptLoader("spell_warl_legion_life_tap") { }

        class spell_warl_legion_life_tap_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_life_tap_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_ETERNAL_STRUGGLE_PROC_AURA,
                    SPELL_WARLOCK_ETERNAL_STRUGGLE_BONUS
                });
            }

            SpellCastResult CheckElevation()
            {
                if (GetCaster()->GetHealthPct() <= float(GetEffectInfo(EFFECT_1)->CalcValue(GetCaster())))
                    return SPELL_FAILED_FIZZLE;

                return SPELL_CAST_OK;
            }

            void HandleAfterCast()
            {
                if (AuraEffect* eternalStruggle = GetCaster()->GetAuraEffect(SPELL_WARLOCK_ETERNAL_STRUGGLE_PROC_AURA, EFFECT_0))
                    GetCaster()->CastCustomSpell(SPELL_WARLOCK_ETERNAL_STRUGGLE_BONUS, SPELLVALUE_BASE_POINT0, eternalStruggle->GetAmount(), GetCaster(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warl_legion_life_tap_SpellScript::CheckElevation);
                AfterCast += SpellCastFn(spell_warl_legion_life_tap_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_life_tap_SpellScript();
        }
};

// 171017 - Meteor Strike (Level 110)
class spell_warl_legion_meteor_strike : public SpellScriptLoader
{
    public:
        spell_warl_legion_meteor_strike() : SpellScriptLoader("spell_warl_legion_meteor_strike") { }

        class spell_warl_legion_meteor_strike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_meteor_strike_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_LORD_OF_FLAMES_DEBUFF,
                    SPELL_WARLOCK_LORD_OF_FLAMES_TRAIT,
                    SPELL_WARLOCK_LORD_OF_FLAMES_SUMMON_INFERNAL
                });
            }

            void HandleAfterCast()
            {
                if (Unit* owner = GetCaster()->GetOwner())
                {
                    if (!owner->HasAura(SPELL_WARLOCK_LORD_OF_FLAMES_TRAIT) || owner->HasAura(SPELL_WARLOCK_LORD_OF_FLAMES_DEBUFF))
                        return;

                    owner->CastSpell(owner, SPELL_WARLOCK_LORD_OF_FLAMES_DEBUFF, true);
                    for (int8 i = 0; i < 3; i++)
                        owner->CastSpell(GetCaster()->GetPositionX(), GetCaster()->GetPositionY(), GetCaster()->GetPositionZ(), SPELL_WARLOCK_LORD_OF_FLAMES_SUMMON_INFERNAL, true);
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warl_legion_meteor_strike_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_meteor_strike_SpellScript();
        }
};

// 205179 - Phantom Singularity
class spell_warl_legion_phantom_singularity : public SpellScriptLoader
{
    public:
        spell_warl_legion_phantom_singularity() : SpellScriptLoader("spell_warl_legion_phantom_singularity") { }

        class spell_warl_legion_phantom_singularity_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_phantom_singularity_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_WARLOCK_PHANTOM_SINGULARITY_DAMAGE });
            }

            void HandleEffectPeriodic(AuraEffect const* aurEff)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_WARLOCK_PHANTOM_SINGULARITY_DAMAGE, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_phantom_singularity_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_phantom_singularity_AuraScript();
        }
};

// 212618 - Pleasure through Pain
class spell_warl_legion_pleasure_through_pain : public SpellScriptLoader
{
    public:
        spell_warl_legion_pleasure_through_pain() : SpellScriptLoader("spell_warl_legion_pleasure_through_pain") { }

        class spell_warl_legion_pleasure_through_pain_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_pleasure_through_pain_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_PLEASURE_THROUGH_PAIN_PET_AURA
                });
            }

            bool Load() override
            {
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    if (pet->GetEntry() == ENTRY_SUCCUBUS)
                        pet->CastSpell(pet, SPELL_WARLOCK_PLEASURE_THROUGH_PAIN_PET_AURA, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Pet* pet = GetTarget()->ToPlayer()->GetPet())
                    if (pet->GetEntry() == ENTRY_SUCCUBUS)
                        pet->RemoveAurasDueToSpell(SPELL_WARLOCK_PLEASURE_THROUGH_PAIN_PET_AURA);
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_warl_legion_pleasure_through_pain_AuraScript::ApplyEffect, EFFECT_1, SPELL_AURA_ADD_FLAT_MODIFIER, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_warl_legion_pleasure_through_pain_AuraScript::RemoveEffect, EFFECT_1, SPELL_AURA_ADD_FLAT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_pleasure_through_pain_AuraScript();
        }
};

// 212999 - Pleasure through Pain
class spell_warl_legion_pleasure_through_pain_pet_aura : public SpellScriptLoader
{
    public:
        spell_warl_legion_pleasure_through_pain_pet_aura() : SpellScriptLoader("spell_warl_legion_pleasure_through_pain_pet_aura") { }

        class spell_warl_legion_pleasure_through_pain_pet_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_pleasure_through_pain_pet_aura_AuraScript);

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                if (Unit* owner = GetUnitOwner()->GetOwner())
                    if (AuraEffect* painAura = owner->GetAuraEffect(SPELL_WARLOCK_PLEASURE_THROUGH_PAIN, EFFECT_1))
                        amount = painAura->GetAmount();
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_warl_legion_pleasure_through_pain_pet_aura_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_ADD_FLAT_MODIFIER);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_pleasure_through_pain_pet_aura_AuraScript();
        }
};

// 216698 - Reap Souls
class spell_warl_legion_reap_souls : public SpellScriptLoader
{
    public:
        spell_warl_legion_reap_souls() : SpellScriptLoader("spell_warl_legion_reap_souls") { }

        class spell_warl_legion_reap_souls_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_reap_souls_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_TORMENTED_SOULS,
                    SPELL_WARLOCK_DEADWIND_HARVESTER
                });
            }

            SpellCastResult CheckElevation()
            {
                if (!GetCaster()->HasAura(SPELL_WARLOCK_TORMENTED_SOULS))
                    return SPELL_FAILED_DONT_REPORT;
                return SPELL_CAST_OK;
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (Aura* souls = GetCaster()->GetAura(SPELL_WARLOCK_TORMENTED_SOULS))
                {
                    std::vector<AreaTrigger*> triggers = GetCaster()->GetAreaTriggers(SPELL_WARLOCK_SOUL_OF_THE_DAMNED_AREATRIGGER);
                    for (AreaTrigger* areatrigger : triggers)
                        areatrigger->Remove();

                    GetCaster()->CastCustomSpell(SPELL_WARLOCK_DEADWIND_HARVESTER, SPELLVALUE_AURA_DURATION, souls->GetStackAmount() * 5000, GetCaster(), TRIGGERED_FULL_MASK);
                    souls->Remove();
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warl_legion_reap_souls_SpellScript::CheckElevation);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_reap_souls_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_reap_souls_SpellScript();
        }
};

// 205184 - Roaring Blaze
class spell_warl_legion_roaring_blaze : public SpellScriptLoader
{
    public:
        spell_warl_legion_roaring_blaze() : SpellScriptLoader("spell_warl_legion_roaring_blaze") { }

        class spell_warl_legion_roaring_blaze_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_roaring_blaze_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_ROARING_BLAZE_PCT_BONUS,
                    SPELL_WARLOCK_IMMOLATE_PERIODIC
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetActionTarget()->HasAura(SPELL_WARLOCK_IMMOLATE_PERIODIC, GetCasterGUID());
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                eventInfo.GetActor()->CastSpell(eventInfo.GetActionTarget(), SPELL_WARLOCK_ROARING_BLAZE_PCT_BONUS, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_roaring_blaze_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_roaring_blaze_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_roaring_blaze_AuraScript();
        }
};

// 27243 - Seed of Corruption
class spell_warl_legion_seed_of_corruption : public SpellScriptLoader
{
    public:
        spell_warl_legion_seed_of_corruption() : SpellScriptLoader("spell_warl_legion_seed_of_corruption") { }

        class spell_warl_legion_seed_of_corruption_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_seed_of_corruption_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                if (AuraEffect* sowTheSeeds = GetCaster()->GetAuraEffect(SPELL_WARLOCK_SOW_THE_SEEDS, EFFECT_0))
                {
                    if (targets.size() > uint32(sowTheSeeds->GetAmount()))
                    {
                        targets.remove(GetExplTargetUnit()); // Temp remove for random resize
                        Trinity::Containers::RandomResize(targets, sowTheSeeds->GetAmount());
                        targets.push_back(GetExplTargetUnit());
                        return;
                    }
                }
                targets.clear();
                targets.push_back(GetExplTargetUnit());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_seed_of_corruption_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_seed_of_corruption_SpellScript();
        }

        class spell_warl_legion_seed_of_corruption_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_seed_of_corruption_AuraScript);

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;
                if (Unit* caster = GetCaster())
                {
                    float bonus = caster->SpellBaseHealingBonusDone(GetSpellInfo()->GetSchoolMask()) * 3;
                    amount = int32(bonus);
                    const_cast<AuraEffect*>(aurEff)->SetDamage(amount);
                }
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (!eventInfo.GetDamageInfo())
                    return false;

                // check if it is a spell
                if (!eventInfo.GetSpellInfo())
                    return false;

                // check if the spell is from aura caster
                if (eventInfo.GetDamageInfo()->GetAttacker()->GetGUID() != GetCasterGUID())
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 damage = eventInfo.GetDamageInfo()->GetDamage();
                if (damage > aurEff->GetDamage() || eventInfo.GetSpellInfo()->Id == SPELL_WARLOCK_SEED_OF_CORRUPTION_AOE)
                    Remove();
                else
                {
                    const_cast<AuraEffect*>(aurEff)->SetAmount(aurEff->GetAmount() - damage);
                    const_cast<AuraEffect*>(aurEff)->SetDamage(aurEff->GetDamage() - damage);
                    GetAura()->SetNeedClientUpdateForTargets();
                }
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_WARLOCK_SEED_OF_CORRUPTION_AOE, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_warl_legion_seed_of_corruption_AuraScript::CalculateAmount, EFFECT_2, SPELL_AURA_DUMMY);
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_seed_of_corruption_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_seed_of_corruption_AuraScript::HandleEffectProc, EFFECT_2, SPELL_AURA_DUMMY);
                AfterEffectRemove += AuraEffectApplyFn(spell_warl_legion_seed_of_corruption_AuraScript::HandleRemove, EFFECT_2, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_seed_of_corruption_AuraScript();
        }
};

// 27285 - Seed of Corruption (Level 110)
class spell_warl_legion_seed_of_corruption_damage : public SpellScriptLoader
{
    public:
        spell_warl_legion_seed_of_corruption_damage() : SpellScriptLoader("spell_warl_legion_seed_of_corruption_damage") { }

        class spell_warl_legion_seed_of_corruption_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_seed_of_corruption_damage_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_CORRUPTION
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_CORRUPTION, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_seed_of_corruption_damage_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_seed_of_corruption_damage_SpellScript();
        }
};

// 17877 - Shadowburn
class spell_warl_legion_shadowburn : public SpellScriptLoader
{
    public:
        spell_warl_legion_shadowburn() : SpellScriptLoader("spell_warl_legion_shadowburn") { }

        class spell_warl_legion_shadowburn_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_shadowburn_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SHADOWBURN_ENERGIZE
                });
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_WARLOCK_SHADOWBURN_ENERGIZE, true);
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectApplyFn(spell_warl_legion_shadowburn_AuraScript::HandleRemove, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_shadowburn_AuraScript();
        }

        class spell_warl_legion_shadowburn_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_shadowburn_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_CONFLAGRATION_OF_CHAOS_BONUS
                });
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_WARLOCK_CONFLAGRATION_OF_CHAOS_BONUS))
                    SetHitDamage(GetHitDamage() + CalculatePct(GetHitDamage(), GetCaster()->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1)));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_shadowburn_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_shadowburn_SpellScript();
        }
};

// 196659 - Shadow Barrage
class spell_warl_legion_shadow_barrage : public SpellScriptLoader
{
    public:
        spell_warl_legion_shadow_barrage() : SpellScriptLoader("spell_warl_legion_shadow_barrage") { }

        class spell_warl_legion_shadow_barrage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_shadow_barrage_AuraScript);

            void HandleDummyTick(AuraEffect const* /*aurEff*/)
            {
                PreventDefaultAction();

                Creature* caster = GetTarget()->ToCreature();
                Unit* owner = caster->GetOwner(false);

                if (!owner || !caster || !caster->IsAIEnabled)
                {
                    caster->DespawnOrUnsummon();
                    return;
                }

                if (Unit* target = ObjectAccessor::GetUnit(*caster, caster->AI()->GetGUID(0)))
                    if (target->IsAlive() && owner->IsValidAttackTarget(target))
                        caster->CastSpell(target, GetSpellInfo()->GetEffect(EFFECT_0)->TriggerSpell, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_shadow_barrage_AuraScript::HandleDummyTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_shadow_barrage_AuraScript();
        }
};

// 115232 - Shadow Shield
class spell_warl_legion_shadow_shield : public SpellScriptLoader
{
    public:
        spell_warl_legion_shadow_shield() : SpellScriptLoader("spell_warl_legion_shadow_shield") { }

        class spell_warl_legion_shadow_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_shadow_shield_AuraScript);

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->RemoveAuraFromStack(GetId());
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_shadow_shield_AuraScript::OnProc, EFFECT_1, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_shadow_shield_AuraScript();
        }

        class spell_warl_legion_shadow_shield_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_shadow_shield_SpellScript);

            void HandleAfterHit()
            {
                if (Aura* aura = GetHitAura())
                    aura->SetStackAmount(GetSpellInfo()->StackAmount);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_warl_legion_shadow_shield_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_shadow_shield_SpellScript();
        }
};

// 212623 - Singe Magic
class spell_warl_legion_singe_magic : public SpellScriptLoader
{
    public:
        spell_warl_legion_singe_magic() : SpellScriptLoader("spell_warl_legion_singe_magic") { }

        class spell_warl_legion_singe_magic_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_singe_magic_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SINGE_MAGIC_DISPEL
                });
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            SpellCastResult CheckPet()
            {
                if (Pet* pet = GetCaster()->ToPlayer()->GetPet())
                    if (pet->GetEntry() != ENTRY_IMP)
                        return SPELL_FAILED_DONT_REPORT;
                return SPELL_CAST_OK;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_SINGE_MAGIC_DISPEL, true);
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_warl_legion_singe_magic_SpellScript::CheckPet);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_singe_magic_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_singe_magic_SpellScript();
        }
};

// 212620 - Singe Magic
class spell_warl_legion_singe_magic_damage : public SpellScriptLoader
{
    public:
        spell_warl_legion_singe_magic_damage() : SpellScriptLoader("spell_warl_legion_singe_magic_damage") { }

        class spell_warl_legion_singe_magic_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_singe_magic_damage_SpellScript);

            void HandleDamage(SpellEffIndex /*effIndex*/)
            {
                SetHitDamage(CalculatePct(GetHitUnit()->GetHealth(), GetHitDamage()));
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_singe_magic_damage_SpellScript::HandleDamage, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_singe_magic_damage_SpellScript();
        }
};

// 212356 - Soulshatter
class spell_warl_legion_soulshatter : public SpellScriptLoader
{
    public:
        spell_warl_legion_soulshatter() : SpellScriptLoader("spell_warl_legion_soulshatter") { }

        class spell_warl_legion_soulshatter_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_soulshatter_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SOULSHATTER_HASTE_BONUS,
                    SPELL_WARLOCK_SOULSHATTER_ENERGIZE
                });
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                targets.remove_if([caster](WorldObject* target) -> bool
                {
                    if (Unit* unitTarget = target->ToUnit())
                    {
                        for (uint32 afflictionId : UnstableAfflictionHelper)
                            if (unitTarget->HasAura(afflictionId))
                                return false;

                        if (unitTarget->HasAura(SPELL_WARLOCK_AGONY, caster->GetGUID()) || unitTarget->HasAura(SPELL_WARLOCK_CORRUPTION_AURA, caster->GetGUID()) || unitTarget->HasAura(SPELL_WARLOCK_SIPHON_LIFE, caster->GetGUID()))
                            return false;
                    }
                    return true;
                });

                if (targets.size() > 5)
                {
                    targets.sort(Trinity::ObjectDistanceOrderPred(GetCaster()));
                    targets.resize(5);
                }
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_SOULSHATTER_HASTE_BONUS, true);
                GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_SOULSHATTER_ENERGIZE, true);
                GetHitUnit()->RemoveAurasDueToSpell(SPELL_WARLOCK_AGONY, GetCaster()->GetGUID());
                GetHitUnit()->RemoveAurasDueToSpell(SPELL_WARLOCK_CORRUPTION_AURA, GetCaster()->GetGUID());
                GetHitUnit()->RemoveAurasDueToSpell(SPELL_WARLOCK_SIPHON_LIFE, GetCaster()->GetGUID());

                for (uint32 afflictionId : UnstableAfflictionHelper)
                    GetHitUnit()->RemoveAurasDueToSpell(afflictionId, GetCaster()->GetGUID());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_warl_legion_soulshatter_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_SRC_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_soulshatter_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_soulshatter_SpellScript();
        }
};

// 686 - Shadow Bolt
class spell_warl_legion_shadow_bolt : public SpellScriptLoader
{
    public:
        spell_warl_legion_shadow_bolt() : SpellScriptLoader("spell_warl_legion_shadow_bolt") { }

        class spell_warl_legion_shadow_bolt_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_shadow_bolt_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SHADOW_BOLT_ENERGIZE
                });
            }

            void HandleAfterCast()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_SHADOW_BOLT_ENERGIZE, true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_warl_legion_shadow_bolt_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_shadow_bolt_SpellScript();
        }
};

// 196236 - Soulsnatcher
class spell_warl_legion_soulsnatcher : public SpellScriptLoader
{
    public:
        spell_warl_legion_soulsnatcher() : SpellScriptLoader("spell_warl_legion_soulsnatcher") { }

        class spell_warl_legion_soulsnatcher_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_soulsnatcher_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SOULSNATCHER_ENERGIZE
                });
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return roll_chance_i(GetEffect(EFFECT_0)->GetAmount());
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_WARLOCK_SOULSNATCHER_ENERGIZE, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_soulsnatcher_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_soulsnatcher_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_soulsnatcher_AuraScript();
        }
};

// 215941 - Soul Conduit
class spell_warl_legion_soul_conduit : public SpellScriptLoader
{
    public:
        spell_warl_legion_soul_conduit() : SpellScriptLoader("spell_warl_legion_soul_conduit") { }

        class spell_warl_legion_soul_conduit_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_soul_conduit_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SOUL_CONDUIT
                });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcSpell() && eventInfo.GetProcSpell()->GetPowerCost(POWER_SOUL_SHARDS);
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                int32 usedShards = eventInfo.GetProcSpell()->GetPowerCost(POWER_SOUL_SHARDS);
                while (usedShards-- > 0)
                    if (roll_chance_i(aurEff->GetAmount()))
                        GetTarget()->CastSpell(GetTarget(), SPELL_WARLOCK_SOUL_CONDUIT, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_soul_conduit_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_soul_conduit_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_soul_conduit_AuraScript();
        }
};

// 205178 - Soul Effigy
class spell_warl_legion_soul_effigy : public SpellScriptLoader
{
    public:
        spell_warl_legion_soul_effigy() : SpellScriptLoader("spell_warl_legion_soul_effigy") { }

        class spell_warl_legion_soul_effigy_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_soul_effigy_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SOUL_EFFIGY_VISUAL
                });
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                // despawn soul effigy on dispel
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_ENEMY_SPELL)
                    return;

                std::map<ObjectGuid /*Guid*/, uint32 /*npc entry*/> summonList = GetCaster()->tempSummonList;
                for (auto itr : summonList)
                    if (Creature* creature = ObjectAccessor::GetCreature(*GetCaster(), itr.first))
                        if (creature->GetEntry() == uint32(GetAura()->GetSpellEffectInfo(EFFECT_1)->MiscValue))
                            creature->DespawnOrUnsummon();
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_warl_legion_soul_effigy_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_soul_effigy_AuraScript();
        }
};

// 205247 - Soul Effigy (periodic dummy)
class spell_warl_legion_soul_effigy_periodic : public SpellScriptLoader
{
    public:
        spell_warl_legion_soul_effigy_periodic() : SpellScriptLoader("spell_warl_legion_soul_effigy_periodic") { }

        class spell_warl_legion_soul_effigy_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_soul_effigy_periodic_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SOUL_EFFIGY_VISUAL
                });
            }

            bool Load() override
            {
                return GetUnitOwner()->IsSummon();
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage();
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                Unit* owner = GetUnitOwner();
                int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());

                // "35% of all damage taken by the Effigy is duplicated on the original target"
                if (Unit* creator = ObjectAccessor::GetUnit(*owner, owner->GetDemonCreatorGUID()))
                {
                    Unit::AuraList const& auras = creator->GetSingleCastAuras();
                    auto itr = std::find_if(auras.begin(), auras.end(), [](Aura const* aura) { return aura->GetId() == SPELL_WARLOCK_SOUL_EFFIGY; });
                    if (itr != auras.end())
                        owner->CastCustomSpell(SPELL_WARLOCK_SOUL_EFFIGY_VISUAL, SPELLVALUE_BASE_POINT0, damage, (*itr)->GetUnitOwner(), true);
                }
            }

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                Unit* owner = GetUnitOwner();
                bool checkPassed = false;
                Unit* target = nullptr;

                if (Unit* creator = owner->GetOwner(false))
                {
                    Unit::AuraList const& auras = creator->GetSingleCastAuras();
                    auto itr = std::find_if(auras.begin(), auras.end(), [](Aura const* aura) { return aura->GetId() == SPELL_WARLOCK_SOUL_EFFIGY; });
                    if (itr != auras.end())
                    {
                        target = (*itr)->GetUnitOwner();
                        float range = sSpellMgr->AssertSpellInfo(SPELL_WARLOCK_SOUL_EFFIGY_VISUAL)->GetMaxRange(true);
                        checkPassed = creator->IsAlive() && target->IsAlive() && owner->IsWithinDist(target, range) && owner->IsWithinDist(creator, range);
                    }
                }

                if (!checkPassed && target)
                    target->RemoveAurasDueToSpell(SPELL_WARLOCK_SOUL_EFFIGY, owner->GetDemonCreatorGUID(), 0, AURA_REMOVE_BY_ENEMY_SPELL);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_soul_effigy_periodic_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_soul_effigy_periodic_AuraScript::OnProc, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_warl_legion_soul_effigy_periodic_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_soul_effigy_periodic_AuraScript();
        }
};

// 205277 - Soul Effigy
class spell_warl_legion_soul_effigy_visual : public SpellScriptLoader
{
    public:
        spell_warl_legion_soul_effigy_visual() : SpellScriptLoader("spell_warl_legion_soul_effigy_visual") { }

        class spell_warl_legion_soul_effigy_visual_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_soul_effigy_visual_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_WARLOCK_SOUL_EFFIGY_DAMAGE });
            }

            void HandleOnEffectHitTarget(SpellEffIndex /*effIndex*/)
            {
                if (Unit* creator = GetCaster()->GetOwner(false))
                    creator->CastCustomSpell(SPELL_WARLOCK_SOUL_EFFIGY_DAMAGE, SPELLVALUE_BASE_POINT0, GetEffectValue(), GetHitUnit(), true);

                GetCaster()->SendPlaySpellVisual(GetHitUnit()->GetGUID(), SPELL_WARLOCK_VISUAL_SOUL_EFFIGY, 0, 0, 0.0f);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_soul_effigy_visual_SpellScript::HandleOnEffectHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_soul_effigy_visual_SpellScript();
        }
};

class DelayedSoulFlameEvent : public BasicEvent
{
    public:
        DelayedSoulFlameEvent(Unit* owner, float x, float y, float z) : _owner(owner), _x(x), _y(y), _z(z) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/) override
        {
            _owner->CastSpell(_x, _y, _z, SPELL_WARLOCK_SOUL_FLAME_DAMAGE, true);
            return true;
        }

    private:
        Unit* _owner;
        float _x, _y, _z;
};

// 199471 - Soul Flame
class spell_warl_legion_soul_flame : public SpellScriptLoader
{
    public:
        spell_warl_legion_soul_flame() : SpellScriptLoader("spell_warl_legion_soul_flame") { }

        class spell_warl_legion_soul_flame_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_soul_flame_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_SOUL_FLAME_DAMAGE
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                GetTarget()->m_Events.AddEvent
                    (new DelayedSoulFlameEvent(GetTarget(), eventInfo.GetActionTarget()->GetPositionX(), eventInfo.GetActionTarget()->GetPositionY(), eventInfo.GetActionTarget()->GetPositionZ()),
                    GetTarget()->m_Events.CalculateTime(200));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_soul_flame_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_soul_flame_AuraScript();
        }
};

// 196098 - Soul Harvest
class spell_warl_legion_soul_harvest : public SpellScriptLoader
{
    public:
        spell_warl_legion_soul_harvest() : SpellScriptLoader("spell_warl_legion_soul_harvest") { }

        class spell_warl_legion_soul_harvest_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_soul_harvest_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!spellInfo->GetEffect(EFFECT_1) || !spellInfo->GetEffect(EFFECT_2))
                    return false;

                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_AGONY,
                    SPELL_WARLOCK_DOOM,
                    SPELL_WARLOCK_IMMOLATE_PERIODIC
                });
            }

            void HandleAfterHit()
            {
                if (Aura* aura = GetHitAura())
                {
                    int32 bonusDuration = 0;
                    int32 bonusPerDot = GetEffectInfo(EFFECT_1)->CalcValue() * 1000;
                    bonusDuration += int32(GetCaster()->GetTargetAuraApplications(SPELL_WARLOCK_AGONY).size()) * bonusPerDot;
                    bonusDuration += int32(GetCaster()->GetTargetAuraApplications(SPELL_WARLOCK_DOOM).size()) * bonusPerDot;
                    bonusDuration += int32(GetCaster()->GetTargetAuraApplications(SPELL_WARLOCK_IMMOLATE_PERIODIC).size()) * bonusPerDot;

                    if (bonusDuration > 0)
                    {
                        int32 duration = aura->GetMaxDuration() + int32(std::min(bonusDuration, (GetEffectInfo(EFFECT_2)->CalcValue() * 1000)));
                        aura->SetMaxDuration(duration);
                        aura->SetDuration(duration);
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_warl_legion_soul_harvest_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_soul_harvest_SpellScript();
        }
};

// 108370 - Soul Leech
class spell_warl_legion_soul_leech : public SpellScriptLoader
{
    public:
        spell_warl_legion_soul_leech() : SpellScriptLoader("spell_warl_legion_soul_leech") { }

        class spell_warl_legion_soul_leech_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_soul_leech_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo({ SPELL_WARLOCK_SOUL_LEECH_SHIELD });
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetDamageInfo() != nullptr;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                // Exclude self damage
                if (eventInfo.GetActor() == eventInfo.GetActionTarget())
                    return;

                if (Unit* caster = GetCaster())
                {
                    int32 baseAbsorb = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
                    for (auto itr : caster->GetTargetAuraApplications(GetId()))
                    {
                        int32 absorbAmount = baseAbsorb;
                        if (AuraEffect* absorb = itr->GetTarget()->GetAuraEffect(SPELL_WARLOCK_SOUL_LEECH_SHIELD, EFFECT_0))
                            absorbAmount += absorb->GetAmount();

                        int32 auraCapPct = GetEffect(EFFECT_1)->GetAmount();
                        if (AuraEffect* demonSkin = caster->GetAuraEffect(SPELL_WARLOCK_DEMONIC_SKIN, EFFECT_1))
                            auraCapPct = demonSkin->GetAmount();

                        absorbAmount = std::min(absorbAmount, int32(itr->GetTarget()->CountPctFromMaxHealth(auraCapPct)));
                        if (AuraEffect* shield = itr->GetTarget()->GetAuraEffect(SPELL_WARLOCK_SOUL_LEECH_SHIELD, EFFECT_0))
                        {
                            if (!caster->HasAura(SPELL_WARLOCK_DEMONIC_SKIN) && shield->GetBase()->GetDuration() == -1)
                            {
                                shield->GetBase()->SetMaxDuration(shield->GetBase()->GetSpellInfo()->GetMaxDuration());
                                shield->GetBase()->SetDuration(shield->GetBase()->GetSpellInfo()->GetMaxDuration());
                            }

                            shield->ChangeAmount(absorbAmount);
                            shield->GetBase()->RefreshDuration();
                        }
                        else
                            caster->CastCustomSpell(SPELL_WARLOCK_SOUL_LEECH_SHIELD, SPELLVALUE_BASE_POINT0, absorbAmount, itr->GetTarget(), TRIGGERED_FULL_MASK);
                    }

                    if (Aura* soulLink = caster->GetAura(SPELL_WARLOCK_SOUL_LINK))
                    {
                        int32 warlockHeal = CalculatePct(baseAbsorb, soulLink->GetSpellEffectInfo(EFFECT_1)->CalcValue(caster));
                        int32 petHeal = CalculatePct(baseAbsorb, soulLink->GetSpellEffectInfo(EFFECT_2)->CalcValue(caster));

                        CustomSpellValues values;
                        values.AddSpellMod(SPELLVALUE_BASE_POINT0, warlockHeal);
                        values.AddSpellMod(SPELLVALUE_BASE_POINT1, petHeal);
                        caster->CastCustomSpell(SPELL_WARLOCK_SOUL_LINK_HEAL, values, caster, TRIGGERED_FULL_MASK);
                    }
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_warl_legion_soul_leech_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_soul_leech_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_soul_leech_AuraScript();
        }
};

// 20707 - Soulstone
class spell_warl_legion_soulstone : public SpellScriptLoader
{
    public:
        spell_warl_legion_soulstone() : SpellScriptLoader("spell_warl_legion_soulstone") { }

        class spell_warl_legion_soulstone_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_soulstone_AuraScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->ExcludeTargetAuraSpell != 0;
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    return;

                GetTarget()->CastSpell(GetTarget(), GetSpellInfo()->ExcludeTargetAuraSpell, true);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectApplyFn(spell_warl_legion_soulstone_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_soulstone_AuraScript();
        }

        class spell_warl_legion_soulstone_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_soulstone_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                return spellInfo->ExcludeTargetAuraSpell != 0;
            }

            void HandleAfterHit()
            {
                if (GetHitUnit()->GetTypeId() != TYPEID_PLAYER)
                    return;

                if (!GetHitUnit()->IsAlive())
                {
                    PreventHitAura();
                    GetHitUnit()->CastSpell(GetHitUnit(), GetSpellInfo()->ExcludeTargetAuraSpell, true);
                    GetHitUnit()->SetUInt32Value(PLAYER_SELF_RES_SPELL, SPELL_WARLOCK_USE_SOULSTONE);
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_warl_legion_soulstone_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_soulstone_SpellScript();
        }
};

// 211529 - Stolen Power
class spell_warl_legion_stolen_power : public SpellScriptLoader
{
    public:
        spell_warl_legion_stolen_power() : SpellScriptLoader("spell_warl_legion_stolen_power") { }

        class spell_warl_legion_stolen_power_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_stolen_power_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_ROARING_BLAZE_PCT_BONUS,
                    SPELL_WARLOCK_IMMOLATE_PERIODIC
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetAura()->GetStackAmount() == GetAura()->GetSpellInfo()->StackAmount)
                {
                    GetTarget()->CastSpell(GetTarget(), SPELL_WARLOCK_STOLEN_POWER_BONUS, true);
                    Remove();
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_warl_legion_stolen_power_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_stolen_power_AuraScript();
        }
};

// 1122 - Summon Infernal
class spell_warl_legion_summon_infernal : public SpellScriptLoader
{
    public:
        spell_warl_legion_summon_infernal() : SpellScriptLoader("spell_warl_legion_summon_infernal") { }

        class spell_warl_legion_summon_infernal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_summon_infernal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_LORD_OF_FLAMES_DEBUFF,
                    SPELL_WARLOCK_LORD_OF_FLAMES_TRAIT,
                    SPELL_WARLOCK_LORD_OF_FLAMES_SUMMON_INFERNAL
                });
            }

            bool Load() override
            {
                return GetCaster()->HasAura(SPELL_WARLOCK_LORD_OF_FLAMES_TRAIT);
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (GetCaster()->HasAura(SPELL_WARLOCK_LORD_OF_FLAMES_DEBUFF))
                    return;

                GetCaster()->CastSpell(GetCaster(), SPELL_WARLOCK_LORD_OF_FLAMES_DEBUFF, true);

                if (WorldLocation* dest = GetHitDest())
                    for (int8 i = 0; i < 3; i++)
                        GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_WARLOCK_LORD_OF_FLAMES_SUMMON_INFERNAL, true);

            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_warl_legion_summon_infernal_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SUMMON);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_summon_infernal_SpellScript();
        }
};

// 211714 - Thal'kiel's Consumption
class spell_warl_legion_thalkiels_consumption : public SpellScriptLoader
{
    public:
        spell_warl_legion_thalkiels_consumption() : SpellScriptLoader("spell_warl_legion_thalkiels_consumption") { }

        class spell_warl_legion_thalkiels_consumption_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_thalkiels_consumption_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_THALKIELS_CONSUMPTION_DAMAGE
                });
            }

            void HandleAfterHit()
            {
                if (GetExplTargetUnit())
                    GetCaster()->CastCustomSpell(SPELL_WARLOCK_THALKIELS_CONSUMPTION_DAMAGE, SPELLVALUE_BASE_POINT0, GetHitDamage(), GetExplTargetUnit(), TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_warl_legion_thalkiels_consumption_SpellScript::HandleAfterHit);
            }

            private:
                ObjectGuid explTargetUnitGuid;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_thalkiels_consumption_SpellScript();
        }
};

// 30108 - Unstable Affliction
class spell_warl_legion_unstable_affliction : public SpellScriptLoader
{
    public:
        spell_warl_legion_unstable_affliction() : SpellScriptLoader("spell_warl_legion_unstable_affliction") { }

        class spell_warl_legion_unstable_affliction_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_warl_legion_unstable_affliction_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED,
                    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_2,
                    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_3,
                    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_4,
                    SPELL_WARLOCK_UNSTABLE_AFFLICTION_TRIGGERED_5
                });
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                bool auraApplied = false;
                for (uint32 unstableAfflictionId : UnstableAfflictionHelper)
                {
                    if (!GetHitUnit()->HasAura(unstableAfflictionId, GetCaster()->GetGUID()))
                    {
                        GetCaster()->CastSpell(GetHitUnit(), unstableAfflictionId, true);
                        auraApplied = true;
                        break;
                    }
                }

                if (!auraApplied)
                    GetCaster()->CastSpell(GetHitUnit(), UnstableAfflictionHelper[0], true);

                if (GetCaster()->HasAura(SPELL_WARLOCK_COMPOUNDING_HORROR_AURA))
                    GetCaster()->CastSpell(GetHitUnit(), SPELL_WARLOCK_COMPOUNDING_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_warl_legion_unstable_affliction_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_warl_legion_unstable_affliction_SpellScript();
        }
};

// 233490 - Unstable Affliction
// 233496 - Unstable Affliction
// 233497 - Unstable Affliction
// 233498 - Unstable Affliction
// 233499 - Unstable Affliction
class spell_warl_legion_unstable_affliction_triggered : public SpellScriptLoader
{
    public:
        spell_warl_legion_unstable_affliction_triggered() : SpellScriptLoader("spell_warl_legion_unstable_affliction_triggered") { }

        class spell_warl_legion_unstable_affliction_triggered_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_unstable_affliction_triggered_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_UNSTABLE_AFFLICTION_DUMMY
                });
            }

            void ApplyEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (caster->HasAura(SPELL_WARLOCK_CONTAGION))
                        caster->CastSpell(GetTarget(), SPELL_WARLOCK_CONTAGION_DEBUFF, true);
            }

            void RemoveEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                bool afflictionStillActive = false;
                for (uint32 unstableAfflictionId : UnstableAfflictionHelper)
                {
                    if (unstableAfflictionId == GetId())
                        continue;

                    if (GetTarget()->HasAura(unstableAfflictionId, GetCasterGUID()))
                        afflictionStillActive = true;
                }

                if (Unit* caster = GetCaster())
                {
                    if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    {
                        if (AuraEffect* fatalEchoes = caster->GetAuraEffect(SPELL_WARLOCK_FATAL_ECHOES, EFFECT_0))
                        {
                            if (roll_chance_i(fatalEchoes->GetAmount()))
                            {
                                afflictionStillActive = true;
                                caster->CastSpell(GetTarget(), GetId(), true);
                            }
                        }
                    }
                    else
                    {
                        if (caster->HasAura(SPELL_WARLOCK_UNSTABLE_AFFLICTION_LEVEL_BONUS))
                            caster->CastSpell(caster, SPELL_WARLOCK_UNSTABLE_AFFLICTION_ENERGIZE, true);
                    }
                }

                if (!afflictionStillActive)
                    GetTarget()->RemoveAurasDueToSpell(SPELL_WARLOCK_CONTAGION_DEBUFF, GetCasterGUID());
            }

            void HandleAfterDispel(DispelInfo* dispelData)
            {
                if (Unit* dispeller = dispelData->GetDispeller())
                {
                    AuraEffect const* aurEff = GetEffect(EFFECT_0);
                    SpellEffectInfo const* dummyInfo = sSpellMgr->AssertSpellInfo(SPELL_WARLOCK_UNSTABLE_AFFLICTION_DUMMY)->GetEffect(EFFECT_0);

                    if (!aurEff || !dummyInfo)
                        return;

                    int32 damage = aurEff->GetDamage() * (dummyInfo->CalcValue() / 100);
                    dispeller->CastCustomSpell(dispeller, SPELL_WARLOCK_UNSTABLE_AFFLICTION_SILENCE, &damage, nullptr, nullptr, true, nullptr, GetEffect(EFFECT_0), GetCasterGUID());
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_warl_legion_unstable_affliction_triggered_AuraScript::ApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectRemove += AuraEffectRemoveFn(spell_warl_legion_unstable_affliction_triggered_AuraScript::RemoveEffect, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
                AfterDispel += AuraDispelFn(spell_warl_legion_unstable_affliction_triggered_AuraScript::HandleAfterDispel);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_unstable_affliction_triggered_AuraScript();
        }
};

// 199472 - Wrath of Consumption
class spell_warl_legion_wrath_of_consumption : public SpellScriptLoader
{
    public:
        spell_warl_legion_wrath_of_consumption() : SpellScriptLoader("spell_warl_legion_wrath_of_consumption") { }

        class spell_warl_legion_wrath_of_consumption_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_warl_legion_wrath_of_consumption_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo
                ({
                    SPELL_WARLOCK_WRATH_OF_CONSUMPTION_BONUS
                });
            }

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_WARLOCK_WRATH_OF_CONSUMPTION_BONUS, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_warl_legion_wrath_of_consumption_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_warl_legion_wrath_of_consumption_AuraScript();
        }
};

class areatrigger_warl_legion_rain_of_fire : public AreaTriggerEntityScript
{
    public:
        areatrigger_warl_legion_rain_of_fire() : AreaTriggerEntityScript("areatrigger_warl_legion_rain_of_fire") { }

        struct areatrigger_warl_legion_rain_of_fireAI : public AreaTriggerAI
        {
            areatrigger_warl_legion_rain_of_fireAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                _scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        for (ObjectGuid const& insideGuid : at->GetInsideUnits())
                            if (Unit* target = ObjectAccessor::GetUnit(*caster, insideGuid))
                                if (caster->IsValidAttackTarget(target))
                                    caster->CastSpell(target, SPELL_WARLOCK_RAIN_OF_FIRE_DAMAGE, true);
                    task.Repeat();
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_warl_legion_rain_of_fireAI(areaTrigger);
        }
};

// 211729 - Thal'kiel's Discord
class areatrigger_warl_legion_thalkiels_discord : public AreaTriggerEntityScript
{
    public:
        areatrigger_warl_legion_thalkiels_discord() : AreaTriggerEntityScript("areatrigger_warl_legion_thalkiels_discord") { }

        struct areatrigger_warl_legion_thalkiels_discordAI : public AreaTriggerAI
        {
            areatrigger_warl_legion_thalkiels_discordAI(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) {}

            void OnCreate() override
            {
                _scheduler.Schedule(Milliseconds(1400), [this](TaskContext task)
                {
                    if (Unit* caster = at->GetCaster())
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_WARLOCK_THALKIELS_DISCORD, true);
                    task.Repeat(Milliseconds(1500));
                });
            }

            void OnUpdate(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        AreaTriggerAI* GetAI(AreaTrigger* areaTrigger) const override
        {
            return new areatrigger_warl_legion_thalkiels_discordAI(areaTrigger);
        }
};

void AddSC_legion_warlock_spell_scripts()
{
    new spell_warl_legion_agony();
    new spell_warl_legion_banish();
    new spell_warl_legion_burning_rush();
    new spell_warl_legion_call_dreadstalkers();
    new spell_warl_legion_cataclysm();
    new spell_warl_legion_call_felhunter();
    new spell_warl_legion_channel_demonfire();
    new spell_warl_legion_channel_demonfire_selector();
    new spell_warl_legion_chaos_barrage();
    new spell_warl_legion_chaos_bolt();
    new spell_warl_legion_chaotic_energies();
    new spell_warl_legion_command_demon_dummys();
    new spell_warl_legion_command_demon_override();
    new spell_warl_legion_compounding_horror_damage();
    new spell_warl_legion_conflagrate();
    new spell_warl_legion_corruption();
    new spell_warl_legion_corruption_periodic();
    new spell_warl_legion_create_healthstone();
    new spell_warl_legion_cremation_damage();
    new spell_warl_legion_curse_of_shadows();
    new spell_warl_legion_dark_pact();
    new spell_warl_legion_demon_skin();
    new spell_warl_legion_demonbolt();
    new spell_warl_legion_demonic_calling();
    new spell_warl_legion_demonic_circle_summon();
    new spell_warl_legion_demonic_circle_teleport();
    new spell_warl_legion_demonic_empowerment();
    new spell_warl_legion_demonic_gateway();
    new spell_warl_legion_demonic_power();
    new spell_warl_legion_demonwrath();
    new spell_warl_legion_devour_magic();
    new spell_warl_legion_dimensional_rift();
    new spell_warl_legion_dimension_ripper();
    new spell_warl_legion_doom();
    new spell_warl_legion_doom_bolt();
    new spell_warl_legion_drain_life();
    new spell_warl_legion_drain_soul();
    new spell_warl_legion_entrenched_in_flame();
    new spell_warl_legion_eradication();
    new spell_warl_legion_essence_drain();
    new spell_warl_legion_eye_laser();
    new spell_warl_legion_fear();
    new spell_warl_legion_fel_cleave();
    new spell_warl_legion_fel_fissure();
    new spell_warl_legion_fixate();
    new spell_warl_legion_grimoire_of_supremacy();
    new spell_warl_legion_havoc_trigger();
    new spell_warl_legion_hand_of_guldan();
    new spell_warl_legion_hand_of_guldan_damage();
    new spell_warl_legion_haunt();
    new spell_warl_legion_healthstone();
    new spell_warl_legion_health_funnel();
    new spell_warl_legion_immolate();
    new spell_warl_legion_immolate_periodic();
    new spell_warl_legion_immolate_proc();
    new spell_warl_legion_implosion();
    new spell_warl_legion_incinerate();
    new spell_warl_legion_life_tap();
    new spell_warl_legion_meteor_strike();
    new spell_warl_legion_phantom_singularity();
    new spell_warl_legion_pleasure_through_pain();
    new spell_warl_legion_pleasure_through_pain_pet_aura();
    new spell_warl_legion_reap_souls();
    new spell_warl_legion_roaring_blaze();
    new spell_warl_legion_seed_of_corruption();
    new spell_warl_legion_seed_of_corruption_damage();
    new spell_warl_legion_shadow_bolt();
    new spell_warl_legion_shadowburn();
    new spell_warl_legion_shadow_barrage();
    new spell_warl_legion_shadow_shield();
    new spell_warl_legion_singe_magic();
    new spell_warl_legion_singe_magic_damage();
    new spell_warl_legion_soulshatter();
    new spell_warl_legion_soul_conduit();
    new spell_warl_legion_soul_effigy();
    new spell_warl_legion_soul_effigy_periodic();
    new spell_warl_legion_soul_effigy_visual();
    new spell_warl_legion_soul_flame();
    new spell_warl_legion_soul_harvest();
    new spell_warl_legion_soul_leech();
    new spell_warl_legion_soulsnatcher();
    new spell_warl_legion_soulstone();
    new spell_warl_legion_stolen_power();
    new spell_warl_legion_summon_infernal();
    new spell_warl_legion_thalkiels_consumption();
    new spell_warl_legion_unstable_affliction();
    new spell_warl_legion_unstable_affliction_triggered();
    new spell_warl_legion_wrath_of_consumption();

    new areatrigger_warl_legion_rain_of_fire();
    new areatrigger_warl_legion_thalkiels_discord();
}
