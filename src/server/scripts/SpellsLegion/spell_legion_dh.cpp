/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_DEMONHUNTER and SPELLFAMILY_GENERIC spells used by demon hunter players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_dh_legion".
 */

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "MotionMaster.h"
#include "MoveSplineInitArgs.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "SpellHistory.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TaskScheduler.h"

#include <G3D/Vector3.h>

enum DemonHunterSpells
{
    AREATRIGGER_DH_LESSER_SOUL_FRAGMENT            = 12504,
    AREATRIGGER_DH_SHATTERED_SOULS_HAVOC           = 8352,
    AREATRIGGER_DH_SHATTERED_SOULS_HAVOC_DEMON     = 11231,
    AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE       = 11266,
    AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE_DEMON = 10693,
    AREATRIGGER_DH_SOUL_FRAGMENT_HAVOC             = 12929,
    AREATRIGGER_DH_SOUL_FRAGMENT_VENGEANCE         = 10665,

    SPELL_DH_INFERNAL_STRIKE_PROGRESS_CURVE_ID     = 1636,
    SPELL_DH_METAMORPHOSIS_PROGRESS_CURVE_ID       = 1717,

    SPELL_DH_ABYSSAL_STRIKE                        = 207550,
    SPELL_DH_ANGUISH                               = 202446,
    SPELL_DH_ANGUISH_DEBUFF                        = 202443,
    SPELL_DH_ANGUISH_OF_THE_DECEIVER               = 201473,
    SPELL_DH_ANNIHILATION                          = 201427,
    SPELL_DH_ANNIHILATION_MH                       = 227518,
    SPELL_DH_ANNIHILATION_OH                       = 201428,
    SPELL_DH_AWAKEN_THE_DEMON_WITHIN_CD            = 207128,
    SPELL_DH_BALANCED_BLADES                       = 201470,
    SPELL_DH_BLOODLET                              = 206473,
    SPELL_DH_BLOODLET_PERIODIC                     = 207690,
    SPELL_DH_BLUR                                  = 212800,
    SPELL_DH_BLUR_TRIGGER                          = 198589,
    SPELL_DH_BURNING_ALIVE                         = 207739,
    SPELL_DH_BURNING_ALIVE_TARGET_SELECTOR         = 207760,
    SPELL_DH_CHAOS_NOVA                            = 179057,
    SPELL_DH_CHAOS_STRIKE                          = 162794,
    SPELL_DH_CHAOS_STRIKE_ENERGIZE                 = 193840,
    SPELL_DH_CHAOS_STRIKE_MH                       = 222031,
    SPELL_DH_CHAOS_STRIKE_OH                       = 199547,
    SPELL_DH_CHAOS_STRIKE_PROC                     = 197125,
    SPELL_DH_CONSUME_SOUL_HAVOC                    = 228542,
    SPELL_DH_CONSUME_SOUL_HAVOC_DEMON              = 228556,
    SPELL_DH_CONSUME_SOUL_HAVOC_SHATTERED          = 228540,
    SPELL_DH_CONSUME_SOUL_HEAL                     = 203794,
    SPELL_DH_CONSUME_SOUL_VENGEANCE                = 208014,
    SPELL_DH_CONSUME_SOUL_VENGEANCE_DEMON          = 210050,
    SPELL_DH_CONSUME_SOUL_VENGEANCE_SHATTERED      = 210047,
    SPELL_DH_DARKNESS_ABSORB                       = 209426,
    SPELL_DH_DECEIVERS_FURY_ENERGIZE               = 202120,
    SPELL_DH_DEMON_BLADES_DMG                      = 203796,
    SPELL_DH_DEMON_REBORN                          = 193897,
    SPELL_DH_DEMON_SPIKES                          = 203819,
    SPELL_DH_DEMON_SPIKES_TRIGGER                  = 203720,
    SPELL_DH_DEMONIC                               = 213410,
    SPELL_DH_DEMONIC_TRAMPLE_DMG                   = 208645,
    SPELL_DH_DEMONIC_TRAMPLE_STUN                  = 213491,
    SPELL_DH_DEMONS_BITE                           = 162243,
    SPELL_DH_EYE_BEAM                              = 198013,
    SPELL_DH_EYE_BEAM_DMG                          = 198030,
    SPELL_DH_EYE_OF_LEOTHERAS_DMG                  = 206650,
    SPELL_DH_FEAST_OF_SOULS                        = 207697,
    SPELL_DH_FEAST_OF_SOULS_PERIODIC_HEAL          = 207693,
    SPELL_DH_FEED_THE_DEMON                        = 218612,
    SPELL_DH_FEL_BARRAGE                           = 211053,
    SPELL_DH_FEL_BARRAGE_DMG                       = 211052,
    SPELL_DH_FEL_BARRAGE_PROC                      = 222703,
    SPELL_DH_FEL_DEVASTATION                       = 212084,
    SPELL_DH_FEL_DEVASTATION_DMG                   = 212105,
    SPELL_DH_FEL_DEVASTATION_HEAL                  = 212106,
    SPELL_DH_FEL_RUSH                              = 195072,
    SPELL_DH_FEL_RUSH_DMG                          = 192611,
    SPELL_DH_FEL_RUSH_GROUND                       = 197922,
    SPELL_DH_FEL_RUSH_WATER_AIR                    = 197923,
    SPELL_DH_FELBLADE                              = 232893,
    SPELL_DH_FELBLADE_CHARGE                       = 213241,
    SPELL_DH_FELBLADE_DMG                          = 213243,
    SPELL_DH_FELBLADE_PROC                         = 203557,
    SPELL_DH_FELBLADE_PROC_VISUAL                  = 204497,
    SPELL_DH_FELBLADE_PROC1                        = 236167,
    SPELL_DH_FIERY_BRAND                           = 204021,
    SPELL_DH_FIERY_BRAND_DMG_REDUCTION_DEBUFF      = 207744,
    SPELL_DH_FIERY_BRAND_DOT                       = 207771,
    SPELL_DH_FIRST_BLOOD                           = 206416,
    SPELL_DH_FLAME_CRASH                           = 227322,
    SPELL_DH_FRAILTY                               = 224509,
    SPELL_DH_FURY_OF_THE_ILLIDARI                  = 201628,
    SPELL_DH_FURY_OF_THE_ILLIDARI_1                = 201789,
    SPELL_DH_GLIDE                                 = 131347,
    SPELL_DH_GLIDE_DURATION                        = 197154,
    SPELL_DH_GLIDE_KNOCKBACK                       = 196353,
    SPELL_DH_HAVOC_MASTERY                         = 185164,
    SPELL_DH_INFERNAL_STRIKE_CAST                  = 189110,
    SPELL_DH_INFERNAL_STRIKE_IMPACT_DAMAGE         = 189112,
    SPELL_DH_INFERNAL_STRIKE_JUMP                  = 189111,
    SPELL_DH_INNER_DEMONS                          = 201471,
    SPELL_DH_INNER_DEMONS_DMG                      = 202388,
    SPELL_DH_JAGGED_SPIKES                         = 205627,
    SPELL_DH_JAGGED_SPIKES_DMG                     = 208790,
    SPELL_DH_JAGGED_SPIKES_PROC                    = 208796,
    SPELL_DH_MANA_RIFT_DMG_POWER_BURN              = 235904,
    SPELL_DH_METAMORPHOSIS                         = 191428,
    SPELL_DH_METAMORPHOSIS_DUMMY                   = 191427,
    SPELL_DH_METAMORPHOSIS_IMPACT_DAMAGE           = 200166,
    SPELL_DH_METAMORPHOSIS_TRANSFORM               = 162264,
    SPELL_DH_METAMORPHOSIS_VENGEANCE_TRANSFORM     = 187827,
    SPELL_DH_MOMENTUM                              = 208628,
    SPELL_DH_MOVE_MARKER                           = 221461,
    SPELL_DH_NEMESIS_ABERRATIONS                   = 208607,
    SPELL_DH_NEMESIS_BEASTS                        = 208608,
    SPELL_DH_NEMESIS_CRITTERS                      = 208609,
    SPELL_DH_NEMESIS_DEMONS                        = 208608,
    SPELL_DH_NEMESIS_DRAGONKIN                     = 208610,
    SPELL_DH_NEMESIS_ELEMENTALS                    = 208611,
    SPELL_DH_NEMESIS_GIANTS                        = 208612,
    SPELL_DH_NEMESIS_HUMANOIDS                     = 208605,
    SPELL_DH_NEMESIS_MECHANICALS                   = 208613,
    SPELL_DH_NEMESIS_UNDEAD                        = 208614,
    SPELL_DH_NETHER_BOND                           = 207810,
    SPELL_DH_NETHER_BOND_DMG_HEAL                  = 207812,
    SPELL_DH_NETHER_BOND_PERIODIC                  = 207811,
    SPELL_DH_PAINBRINGER                           = 207387,
    SPELL_DH_PAINBRINGER_REDUCTION                 = 212988,
    SPELL_DH_RAGE_OF_THE_ILLIDARI                  = 201472,
    SPELL_DH_RAGE_OF_THE_ILLIDARI_DMG_HOLDER       = 217060,
    SPELL_DH_RAGE_OF_THE_ILLIDARI_EXPLOSION        = 217070,
    SPELL_DH_RAGE_OF_THE_ILLIDARI_EXPLOSION_VISUAL = 226948,
    SPELL_DH_RAIN_OF_CHAOS                         = 205628,
    SPELL_DH_RAIN_OF_CHAOS_IMPACT                  = 232538,
    SPELL_DH_RAZOR_SPIKES                          = 210003,
    SPELL_DH_SHATTER_SOUL                          = 209980,
    SPELL_DH_SHATTER_SOUL_1                        = 209981,
    SPELL_DH_SHATTER_SOUL_2                        = 210038,
    SPELL_DH_SHATTERED_SOUL                        = 226258,
    SPELL_DH_SHEAR                                 = 203782,
    SPELL_DH_SEVER                                 = 235964,
    SPELL_DH_SIGIL_OF_FLAME_DAMAGE                 = 204598,
    SPELL_DH_SIGIL_OF_FLAME_FLAME_CRASH            = 228973,
    SPELL_DH_SIGIL_OF_MISERY                       = 207685,
    SPELL_DH_SIGIL_OF_SILENCE                      = 204490,
    SPELL_DH_SIPHON_POWER                          = 218910,
    SPELL_DH_SIPHONED_POWER                        = 218561,
    SPELL_DH_SOLITUDE                              = 211510,
    SPELL_DH_SOUL_BARRIER                          = 227225,
    SPELL_DH_SOUL_CLEAVE                           = 228477,
    SPELL_DH_SOUL_CLEAVE_DMG                       = 228478,
    SPELL_DH_SOUL_FRAGMENT_COUNTER                 = 203981,
    SPELL_DH_SOUL_RENDING                          = 204909,
    SPELL_DH_SPIRIT_BOMB_DAMAGE                    = 218677,
    SPELL_DH_SPIRIT_BOMB_HEAL                      = 227255,
    SPELL_DH_SPIRIT_BOMB_VISUAL                    = 218678,
    SPELL_DH_THROW_GLAIVE                          = 185123,
    SPELL_DH_UNCONTAINED_FEL                       = 209261,
    SPELL_DH_VENGEFUL_RETREAT                      = 198813,
    SPELL_DH_VENGEFUL_RETREAT_TRIGGER              = 198793,
};

// 206416 - First Blood
class spell_dh_legion_first_blood : public SpellScriptLoader
{
    public:
        static char constexpr const ScriptName[] = "spell_dh_legion_first_blood";

        spell_dh_legion_first_blood() : SpellScriptLoader(ScriptName) { }

        class spell_dh_legion_first_blood_AuraScript : public AuraScript
        {
            PrepareSpellScript(spell_dh_legion_first_blood_AuraScript);

        public:
            void SetFirstTarget(ObjectGuid const& targetGuid)
            {
                _firstTargetGUID = targetGuid;
            }

            ObjectGuid const& GetFirstTarget() const { return _firstTargetGUID; }

        private:
            void Register() override { }

        private:
            ObjectGuid _firstTargetGUID;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dh_legion_first_blood_AuraScript();
        }
};
char constexpr const spell_dh_legion_first_blood::ScriptName[];

// 188499 - Blade Dance
// 210152 - Death Sweep
class spell_dh_legion_blade_dance : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_blade_dance);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_FIRST_BLOOD });
    }

    void HandleSelectFirstTarget(SpellEffIndex /*effIndex*/)
    {
        if (!_firstTargetGUID.IsEmpty())
            return;

        _firstTargetGUID = GetHitUnit()->GetGUID();

        if (Aura* aura = GetCaster()->GetAura(SPELL_DH_FIRST_BLOOD))
        {
            using first_blood_script_t = spell_dh_legion_first_blood::spell_dh_legion_first_blood_AuraScript;

            if (first_blood_script_t* script = aura->GetScript<first_blood_script_t>(spell_dh_legion_first_blood::ScriptName))
                script->SetFirstTarget(_firstTargetGUID);
        }
    }

    void Register() override
    {
        OnEffectLaunchTarget += SpellEffectFn(spell_dh_legion_blade_dance::HandleSelectFirstTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
    }

private:
    ObjectGuid _firstTargetGUID;
};

// 199552 - Blade Dance
// 200685 - Blade Dance
// 210153 - Death Sweep
// 210155 - Death Sweep
class spell_dh_legion_blade_dance_damage : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_blade_dance_damage);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_BALANCED_BLADES, SPELL_DH_FIRST_BLOOD });
    }

    void CountTargets(std::list<WorldObject*>& targetList)
    {
        _targetCount = uint32(targetList.size());
    }

    void HandleHitTarget()
    {
        int32 damage = GetHitDamage();

        if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_DH_BALANCED_BLADES, EFFECT_0))
            AddPct(damage, _targetCount * aurEff->GetAmount());

        if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_DH_FIRST_BLOOD, EFFECT_0))
        {
            using first_blood_script_t = spell_dh_legion_first_blood::spell_dh_legion_first_blood_AuraScript;

            if (first_blood_script_t* script = aurEff->GetBase()->GetScript<first_blood_script_t>(spell_dh_legion_first_blood::ScriptName))
                if (GetHitUnit()->GetGUID() == script->GetFirstTarget())
                    AddPct(damage, aurEff->GetAmount());
        }

        SetHitDamage(damage);
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dh_legion_blade_dance_damage::CountTargets, EFFECT_1, TARGET_UNIT_SRC_AREA_ENEMY);
        OnHit += SpellHitFn(spell_dh_legion_blade_dance_damage::HandleHitTarget);
    }

private:
    uint32 _targetCount = 0;
};

enum FelBarrageValues
{
    SPELL_DH_VALUE_FEL_BARRAGE_CHARGES,
};

void AbsorbSoul(AreaTrigger* at, Unit* caster)
{
    uint32 spellId = 0;

    switch (at->GetEntry())
    {
        case AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE_DEMON:
            spellId = SPELL_DH_CONSUME_SOUL_VENGEANCE_DEMON;
            break;
        case AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE:
            spellId = SPELL_DH_CONSUME_SOUL_VENGEANCE_SHATTERED;
            break;
        case AREATRIGGER_DH_SOUL_FRAGMENT_VENGEANCE:
            spellId = SPELL_DH_CONSUME_SOUL_VENGEANCE;
            break;
        case AREATRIGGER_DH_SHATTERED_SOULS_HAVOC_DEMON:
            spellId = SPELL_DH_CONSUME_SOUL_HAVOC_DEMON;
            break;
        case AREATRIGGER_DH_SHATTERED_SOULS_HAVOC:
            spellId = SPELL_DH_CONSUME_SOUL_HAVOC_SHATTERED;
            break;
        case AREATRIGGER_DH_SOUL_FRAGMENT_HAVOC:
            spellId = SPELL_DH_CONSUME_SOUL_HAVOC;
            break;
    }

    if (!spellId)
        return;

    caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), spellId, true);
    at->Remove();
}

// 211053 - Fel Barrage
// 222703 - Fel Barrage Proc
class spell_dh_legion_havoc_fel_barrage_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_fel_barrage_AuraScript);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_FEL_BARRAGE, SPELL_DH_FEL_BARRAGE_DMG, SPELL_DH_FEL_BARRAGE_PROC });
    }

    void HandleProc(ProcEventInfo& eventInfo)
    {
        eventInfo.GetActor()->GetSpellHistory()->RestoreCharge(sSpellMgr->AssertSpellInfo(SPELL_DH_FEL_BARRAGE)->ChargeCategoryId);
    }

    void HandleEffectApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        uint32 categoryId = aurEff->GetSpellInfo()->ChargeCategoryId;

        // + 1 is because the spell cast uses 1 charge
        charges = GetCaster()->GetSpellHistory()->ConsumeAllCharges(categoryId) + 1;
    }

    void HandlePeriodicEffect(AuraEffect const* aurEff)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        SpellCastTargets targets;
        targets.SetUnitTarget(GetTarget());

        Spell* spell = new Spell(caster, sSpellMgr->AssertSpellInfo(SPELL_DH_FEL_BARRAGE_DMG), TRIGGERED_FULL_MASK, caster->GetGUID());
        spell->SetCastExtraParam(SPELL_DH_VALUE_FEL_BARRAGE_CHARGES, charges);
        spell->m_CastItem = nullptr;
        spell->prepare(&targets, aurEff);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_FEL_BARRAGE_PROC)
            OnProc += AuraProcFn(spell_dh_legion_havoc_fel_barrage_AuraScript::HandleProc);
        else if (m_scriptSpellId == SPELL_DH_FEL_BARRAGE)
        {
            OnEffectApply += AuraEffectApplyFn(spell_dh_legion_havoc_fel_barrage_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_havoc_fel_barrage_AuraScript::HandlePeriodicEffect, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
        }
    }

    private:
        int32 charges = 0;
};

// 211052 - Fel Barrage
class spell_dh_legion_havoc_fel_barrage : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_fel_barrage);

    enum Values
    {
        MAX_CHARGES = 5,
    };

    void HandleHitTarget(SpellEffIndex /*effIndex*/)
    {
        int32 const* charges = GetCastExtraParam<int32>(SPELL_DH_VALUE_FEL_BARRAGE_CHARGES);

        SetHitDamage(GetHitDamage() / MAX_CHARGES * (charges ? *charges : 1));
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_FEL_BARRAGE_DMG)
            OnEffectHitTarget += SpellEffectFn(spell_dh_legion_havoc_fel_barrage::HandleHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
    }
};

// 206491 - Nemesis
class spell_dh_legion_havoc_nemesis : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_nemesis);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo(
        {   SPELL_DH_NEMESIS_BEASTS,
            SPELL_DH_NEMESIS_DRAGONKIN,
            SPELL_DH_NEMESIS_DEMONS,
            SPELL_DH_NEMESIS_ELEMENTALS,
            SPELL_DH_NEMESIS_GIANTS,
            SPELL_DH_NEMESIS_UNDEAD,
            SPELL_DH_NEMESIS_HUMANOIDS,
            SPELL_DH_NEMESIS_CRITTERS,
            SPELL_DH_NEMESIS_MECHANICALS,
            SPELL_DH_NEMESIS_ABERRATIONS });
    }

    void HandleProc(ProcEventInfo& eventInfo)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        uint32 spellId = 0;

        switch (eventInfo.GetActor()->GetCreatureType())
        {
            case CREATURE_TYPE_BEAST:
                spellId = SPELL_DH_NEMESIS_BEASTS;
                break;
            case CREATURE_TYPE_DRAGONKIN:
                spellId = SPELL_DH_NEMESIS_DRAGONKIN;
                break;
            case CREATURE_TYPE_DEMON:
                spellId = SPELL_DH_NEMESIS_DEMONS;
                break;
            case CREATURE_TYPE_ELEMENTAL:
                spellId = SPELL_DH_NEMESIS_ELEMENTALS;
                break;
            case CREATURE_TYPE_GIANT:
                spellId = SPELL_DH_NEMESIS_GIANTS;
                break;
            case CREATURE_TYPE_UNDEAD:
                spellId = SPELL_DH_NEMESIS_UNDEAD;
                break;
            case CREATURE_TYPE_HUMANOID:
                spellId = SPELL_DH_NEMESIS_HUMANOIDS;
                break;
            case CREATURE_TYPE_CRITTER:
                spellId = SPELL_DH_NEMESIS_CRITTERS;
                break;
            case CREATURE_TYPE_MECHANICAL:
                spellId = SPELL_DH_NEMESIS_MECHANICALS;
                break;
            case CREATURE_TYPE_ABERRATION:
                spellId = SPELL_DH_NEMESIS_ABERRATIONS;
                break;
        }

        if (spellId)
            caster->CastSpell(caster, spellId, true);
    }

    void Register() override
    {
        OnProc += AuraProcFn(spell_dh_legion_havoc_nemesis::HandleProc);
    }
};

// 206476 - Momentum
class spell_dh_legion_havoc_momentum : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_momentum);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_MOMENTUM });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetSpellInfo()->Id == SPELL_DH_VENGEFUL_RETREAT_TRIGGER || eventInfo.GetSpellInfo()->Id == SPELL_DH_FEL_RUSH;
    }

    void HandleProc(ProcEventInfo& eventInfo)
    {
        eventInfo.GetActor()->CastSpell(eventInfo.GetActor(), SPELL_DH_MOMENTUM, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_momentum::CheckProc);
        OnProc += AuraProcFn(spell_dh_legion_havoc_momentum::HandleProc);
    }
};

// 206473 - Bloodlet
// 207690 - Bloodlet Periodic
class spell_dh_legion_havoc_bloodlet : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_bloodlet);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_BLOODLET_PERIODIC, SPELL_DH_THROW_GLAIVE });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetSpellInfo()->Id == SPELL_DH_THROW_GLAIVE;
    }

    void HandleProc(ProcEventInfo& eventInfo)
    {
        Unit* caster = GetCaster();
        if (!caster || !eventInfo.GetDamageInfo())
            return;

        int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), GetEffect(EFFECT_0)->GetAmount());
        damage += eventInfo.GetProcTarget()->GetRemainingPeriodicAmount(caster->GetGUID(), SPELL_DH_BLOODLET_PERIODIC, SPELL_AURA_PERIODIC_DAMAGE);
        caster->CastCustomSpell(SPELL_DH_BLOODLET_PERIODIC, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetProcTarget(), true);
    }

    void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& /*canBeRecalculated*/)
    {
        amount /= aurEff->GetTotalTicks();
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_BLOODLET)
        {
            DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_bloodlet::CheckProc);
            OnProc += AuraProcFn(spell_dh_legion_havoc_bloodlet::HandleProc);
        }
        else if (m_scriptSpellId == SPELL_DH_BLOODLET_PERIODIC)
        {
            DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dh_legion_havoc_bloodlet::CalculateAmount, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
        }
    }
};

// 131347 - Glide
class spell_dh_legion_glide : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_glide);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_GLIDE_KNOCKBACK, SPELL_DH_GLIDE_DURATION });
    }

    SpellCastResult CheckCast()
    {
        Unit* caster = GetCaster();
        if (!caster->IsFalling())
            return SPELL_FAILED_NOT_ON_GROUND;

        if (caster->IsMounted() || caster->GetVehicleBase())
            return SPELL_FAILED_DONT_REPORT;

        return SPELL_CAST_OK;
    }

    void HandleCast()
    {
        Unit* caster = GetCaster();
        caster->CastSpell(caster, SPELL_DH_GLIDE_KNOCKBACK, true);
        caster->CastSpell(caster, SPELL_DH_GLIDE_DURATION, true);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_GLIDE)
        {
            OnCheckCast += SpellCheckCastFn(spell_dh_legion_glide::CheckCast);
            BeforeCast += SpellCastFn(spell_dh_legion_glide::HandleCast);
        }
    }
};

// 197154 - Glide
class spell_dh_legion_glide_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_glide_AuraScript);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_GLIDE, SPELL_DH_GLIDE_DURATION });
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        GetTarget()->RemoveAura(SPELL_DH_GLIDE);
    }

    void OnRemoveGlide(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        GetTarget()->RemoveAura(SPELL_DH_GLIDE_DURATION);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_GLIDE_DURATION)
            OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_glide_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        else if (m_scriptSpellId == SPELL_DH_GLIDE)
            OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_glide_AuraScript::OnRemoveGlide, EFFECT_0, SPELL_AURA_FEATHER_FALL, AURA_EFFECT_HANDLE_REAL);
    }
};

// 198013 - Eye Beam
class spell_dh_legion_havoc_eye_beam : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_eye_beam);

    enum Values
    {
        DEMONIC_METAMORPHOSIS_DURATION = 8000,
    };

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_EYE_BEAM_DMG });
    }

    void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
    {
        if (Unit* caster = GetCaster())
            caster->CastSpell(nullptr, SPELL_DH_EYE_BEAM_DMG, TRIGGERED_FULL_MASK);
    }

    void HandleEffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE)
            return;

        if (!GetTarget()->HasAura(SPELL_DH_DEMONIC))
            return;

        if (Aura* aura = GetTarget()->GetAura(SPELL_DH_METAMORPHOSIS_TRANSFORM))
        {
            aura->SetDuration(aura->GetDuration() + DEMONIC_METAMORPHOSIS_DURATION);
            return;
        }

        if (Aura* aura = GetTarget()->AddAura(SPELL_DH_METAMORPHOSIS_TRANSFORM, GetTarget()))
            aura->SetDuration(DEMONIC_METAMORPHOSIS_DURATION);
    }

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(203550, EFFECT_2))
            amount += aurEff->GetAmount();
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_havoc_eye_beam::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
        OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_eye_beam::HandleEffectRemove, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL, AURA_EFFECT_HANDLE_REAL);
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dh_legion_havoc_eye_beam::CalculateAmount, EFFECT_3, SPELL_AURA_MOD_POWER_REGEN);
    }
};

// 201473 - Anguish of the Deceiver
// 202443 - Anguish
class spell_dh_legion_havoc_artifact_anguish_of_the_deceiver : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_artifact_anguish_of_the_deceiver);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_ANGUISH });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_DH_EYE_BEAM_DMG;
    }

    void AfterRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();

        if (!caster || GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE)
            return;

        int32 damage = int32(caster->GetTotalAttackPowerValue(BASE_ATTACK) * 0.4f * aurEff->GetAmount() / 10);

        caster->CastCustomSpell(SPELL_DH_ANGUISH, SPELLVALUE_BASE_POINT0, damage, GetTarget(), true);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_ANGUISH_OF_THE_DECEIVER)
            DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_artifact_anguish_of_the_deceiver::CheckProc);
        else if (m_scriptSpellId == SPELL_DH_ANGUISH_DEBUFF)
            AfterEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_artifact_anguish_of_the_deceiver::AfterRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 201463 - Deceiver's Fury
class spell_dh_legion_havoc_artifact_deceivers_fury : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_artifact_deceivers_fury);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_DECEIVERS_FURY_ENERGIZE });
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
    {
        if (Unit* caster = GetCaster())
            caster->CastCustomSpell(SPELL_DH_DECEIVERS_FURY_ENERGIZE, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), caster, TRIGGERED_FULL_MASK);
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_artifact_deceivers_fury::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 209400 - Razor Spikes
class spell_dh_legion_vengeance_razor_spikes : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_razor_spikes);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_RAZOR_SPIKES });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return GetCaster() && GetCaster()->HasAura(SPELL_DH_DEMON_SPIKES) && eventInfo.GetTypeMask() & (PROC_FLAG_DONE_SPELL_MELEE_DMG_CLASS | PROC_FLAG_DONE_MELEE_AUTO_ATTACK);
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        if (Unit* caster = GetCaster())
            caster->CastSpell(eventInfo.GetProcTarget(), SPELL_DH_RAZOR_SPIKES, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_vengeance_razor_spikes::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_razor_spikes::HandleProc, EFFECT_0, SPELL_AURA_ADD_FLAT_MODIFIER);
    }
};

// 212084 - Fel Devastation
class spell_dh_legion_vengeance_fel_devastation : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_fel_devastation);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_FEL_DEVASTATION, SPELL_DH_FEL_DEVASTATION_DMG, SPELL_DH_FEL_DEVASTATION_HEAL });
    }

    void HandlePeriodicEffect(AuraEffect const* /*aurEff*/)
    {
        Unit* caster = GetCaster();
        caster->CastSpell(nullptr, SPELL_DH_FEL_DEVASTATION_DMG, TRIGGERED_FULL_MASK);
        caster->CastSpell(caster, SPELL_DH_FEL_DEVASTATION_HEAL, true);
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_vengeance_fel_devastation::HandlePeriodicEffect, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
    }
};

// 203556 - Master of the glaives
class spell_dh_legion_havoc_master_of_the_glaives : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_master_of_the_glaives);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_THROW_GLAIVE });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetSpellInfo() && eventInfo.GetSpellInfo()->Id == SPELL_DH_THROW_GLAIVE;
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_master_of_the_glaives::CheckProc);
    }
};

// 197125 - Chaos Strike
class spell_dh_legion_havoc_chaos_strike_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_chaos_strike_AuraScript);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_CHAOS_STRIKE_ENERGIZE });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();

        if (spell && (spell->Id == SPELL_DH_CHAOS_STRIKE_MH || spell->Id == SPELL_DH_ANNIHILATION_MH) && (eventInfo.GetHitMask() & PROC_HIT_CRITICAL))
            return true;

        return false;
    }

    void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
    {
        int32 energizeAmount = aurEff->GetAmount();
        GetTarget()->CastCustomSpell(GetTarget(), SPELL_DH_CHAOS_STRIKE_ENERGIZE, &energizeAmount, nullptr, nullptr, true, nullptr, aurEff);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_CHAOS_STRIKE_PROC)
        {
            DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_chaos_strike_AuraScript::CheckProc);
            OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_chaos_strike_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
        }
    }
};

// 162794 - Chaos Strike
// 201427 - Annihilation
class spell_dh_legion_havoc_chaos_strike : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_chaos_strike);

    void HandleCritChance(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
            IsCrit = roll_chance_f(GetCaster()->GetUnitCriticalChance(BASE_ATTACK, target));
    }

    void HandleTriggerSpell(SpellEffIndex /*effIndex*/, Spell* spell)
    {
        spell->SetSpellCritValue(IsCrit ? SPELL_CRIT_VALUE_CRIT : SPELL_CRIT_VALUE_NO_CRIT);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_CHAOS_STRIKE || m_scriptSpellId == SPELL_DH_ANNIHILATION)
        {
            OnEffectLaunchTarget += SpellEffectFn(spell_dh_legion_havoc_chaos_strike::HandleCritChance, EFFECT_0, SPELL_EFFECT_DUMMY);
            OnEffectTriggerSpell += SpellTriggerSpellFn(spell_dh_legion_havoc_chaos_strike::HandleTriggerSpell, EFFECT_1, SPELL_EFFECT_TRIGGER_SPELL);
            OnEffectTriggerSpell += SpellTriggerSpellFn(spell_dh_legion_havoc_chaos_strike::HandleTriggerSpell, EFFECT_2, SPELL_EFFECT_TRIGGER_SPELL);
        }
    }

    private:
        bool IsCrit = false;
};

class at_dh_havoc_artifact_fury_of_the_illidari : public AreaTriggerAI
{
public:
    at_dh_havoc_artifact_fury_of_the_illidari(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnCreate() override
    {
        _maxTicks = uint32(at->GetDuration() / _periodic);

        ScheduleTask();
    }

    void ScheduleTask()
    {
        _scheduler.Schedule(Milliseconds(0), [this] (TaskContext task)
        {
            if (Unit* caster = at->GetCaster())
            {
                SpellCastTargets targets;
                targets.SetDst(*at);
                {
                    SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(SPELL_DH_FURY_OF_THE_ILLIDARI);
                    caster->CastSpell(targets, spellInfo, nullptr, TRIGGERED_FULL_MASK);
                }

                {
                    SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(SPELL_DH_FURY_OF_THE_ILLIDARI_1);
                    caster->CastSpell(targets, spellInfo, nullptr, TRIGGERED_FULL_MASK);
                }

                if (task.GetRepeatCounter() == _maxTicks)
                {
                    if (caster->HasAura(SPELL_DH_RAGE_OF_THE_ILLIDARI))
                    {
                        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DH_RAGE_OF_THE_ILLIDARI_EXPLOSION_VISUAL, true);
                        _scheduler.Schedule(Milliseconds(500), [this] (TaskContext /*task*/)
                        {
                            Unit* caster = at->GetCaster();
                            caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DH_RAGE_OF_THE_ILLIDARI_EXPLOSION, true);
                        });
                    }

                    return;
                }
            }

            task.Repeat(Milliseconds(_periodic));
        });
    }

    void OnUpdate(uint32 diff) override
    {
        _scheduler.Update(diff);
    }

    private:
        TaskScheduler _scheduler;
        static uint32 const _periodic = 400;
        uint32 _maxTicks = 0;
};

// 201472 - Rage of the Illidari
class spell_dh_legion_havoc_artifact_rage_of_the_illidari : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_artifact_rage_of_the_illidari);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_RAGE_OF_THE_ILLIDARI_DMG_HOLDER });
    }

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spellInfo = eventInfo.GetSpellInfo();
        return spellInfo && (spellInfo->Id == SPELL_DH_FURY_OF_THE_ILLIDARI || spellInfo->Id == SPELL_DH_FURY_OF_THE_ILLIDARI_1);
    }

    void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        int32 amount = eventInfo.GetDamageInfo() ? eventInfo.GetDamageInfo()->GetDamage() : 0;

        ApplyPct(amount, aurEff->GetAmount());

        if (AuraEffect* dmgHolder = GetCaster()->GetAuraEffect(SPELL_DH_RAGE_OF_THE_ILLIDARI_DMG_HOLDER, EFFECT_0))
            amount += dmgHolder->GetAmount();

        GetTarget()->CastCustomSpell(GetTarget(), SPELL_DH_RAGE_OF_THE_ILLIDARI_DMG_HOLDER, &amount, nullptr, nullptr, true, nullptr, aurEff);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_artifact_rage_of_the_illidari::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_artifact_rage_of_the_illidari::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 217070 - Rage of the Illidari
class spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage);

    void CountTargets(std::list<WorldObject*>& targetList)
    {
        _targetCount = uint32(targetList.size());
    }

    void HandleHitTarget(SpellEffIndex /*effIndex*/)
    {
        if (AuraEffect* dmgHolder = GetCaster()->GetAuraEffect(SPELL_DH_RAGE_OF_THE_ILLIDARI_DMG_HOLDER, EFFECT_0))
            SetHitDamage(dmgHolder->GetAmount() / _targetCount);
    }

    void HandleAfterCast()
    {
        GetCaster()->RemoveAura(SPELL_DH_RAGE_OF_THE_ILLIDARI_DMG_HOLDER);
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage::CountTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage::HandleHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
        AfterCast += SpellCastFn(spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage::HandleAfterCast);
    }

private:
    uint32 _targetCount = 0;
};

// 201471 - Inner Demons
class spell_dh_legion_havoc_artifact_inner_demons : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_artifact_inner_demons);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();

        if (spell &&  (spell->Id == SPELL_DH_ANNIHILATION_MH || spell->Id == SPELL_DH_CHAOS_STRIKE_MH))
            return true;

        return false;
    }

    void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        AuraEffect* aurEff1 = const_cast<AuraEffect*>(aurEff);
        aurEff1->GetBase()->SetCastExtraParam("TargetGUID", eventInfo.GetProcTarget()->GetGUID());
        aurEff1->SetPeriodic(true);
        aurEff1->SetPeriodicTimer(4000);
    }

    void HandlePeriodic(AuraEffect const* aurEff)
    {
        AuraEffect* aurEff1 = const_cast<AuraEffect*>(aurEff);
        aurEff1->GetBase()->SetCastExtraParam("TargetGUID", 0);
        aurEff1->SetPeriodic(false);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_artifact_inner_demons::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_artifact_inner_demons::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_havoc_artifact_inner_demons::HandlePeriodic, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
    }
};

class at_dh_havoc_artifact_inner_demons : public AreaTriggerAI
{
public:
    at_dh_havoc_artifact_inner_demons(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnInitialize() override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        Aura* aura = caster->GetAura(SPELL_DH_INNER_DEMONS);
        if (!aura)
            return;

        ObjectGuid const* TargetGUID = aura->GetCastExtraParam<ObjectGuid>("TargetGUID");
        if (!TargetGUID)
            return;

        if (Unit* target = ObjectAccessor::GetUnit(*at, *TargetGUID))
        {
            std::vector<G3D::Vector3> path;
            FillPath(at->GetPosition(), path);
            FillPath(target->GetPosition(), path);
            at->InitSplines(path, at->GetTotalDuration());
        }
    }

    void OnDestinationReached() override
    {
        if (Unit* caster = at->GetCaster())
            caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DH_INNER_DEMONS_DMG, true);
    }

    void FillPath(Position const& pos, std::vector<G3D::Vector3>& path) const
    {
        G3D::Vector3 point(pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ());

        path.push_back(point);
        path.push_back(point);
    }
};

struct at_dh_havoc_darkness : public AreaTriggerAI
{
public:
    at_dh_havoc_darkness(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitEnter(Unit* unit) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || !caster->IsFriendlyTo(unit))
            return;

        caster->CastSpell(unit, SPELL_DH_DARKNESS_ABSORB, true);
    }

    void OnUnitExit(Unit* unit) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || !caster->IsFriendlyTo(unit))
            return;

        unit->RemoveAura(SPELL_DH_DARKNESS_ABSORB, caster->GetGUID());
    }
};

// 209426 - Darkness
class spell_dh_legion_havoc_darkness : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_darkness);

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
    {
        // Set absorbtion amount to unlimited
        amount = -1;
    }

    void Absorb(AuraEffect* aurEff, DamageInfo & dmgInfo, uint32 & absorbAmount)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        int32 chance = aurEff->GetBase()->GetSpellEffectInfo(EFFECT_0)->CalcValue(caster);
        if (roll_chance_i(chance))
            absorbAmount = dmgInfo.GetDamage();
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dh_legion_havoc_darkness::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_dh_legion_havoc_darkness::Absorb, EFFECT_0);
    }
};

class spell_dh_legion_havoc_fel_rush : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_fel_rush);

    SpellCastResult CheckCast()
    {
        if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
            return SPELL_FAILED_ROOTED;
        return SPELL_CAST_OK;
    }

    void HandleGroundRush(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();

        if (!caster || caster->IsInWater() || caster->IsFalling())
            return;

        caster->CastSpell(caster, SPELL_DH_FEL_RUSH_GROUND, true);
        caster->CastSpell(caster, SPELL_DH_FEL_RUSH_DMG, true);
    }

    void HandleAirWaterRush(SpellEffIndex /*effIndex*/)
    {
        // Air/Water Rush is always buggy..
        Unit* caster = GetCaster();

        if (!caster || (!caster->IsInWater() && !caster->IsFalling()))
            return;

        caster->CastSpell(caster, SPELL_DH_FEL_RUSH_WATER_AIR, true);
        caster->CastSpell(caster, SPELL_DH_FEL_RUSH_DMG, true);

    }

    void Register() override
    {
        if (m_scriptSpellId == 195072)
        {
            OnCheckCast += SpellCheckCastFn(spell_dh_legion_havoc_fel_rush::CheckCast);
            OnEffectHitTarget += SpellEffectFn(spell_dh_legion_havoc_fel_rush::HandleGroundRush, EFFECT_0, SPELL_EFFECT_DUMMY);
            OnEffectHitTarget += SpellEffectFn(spell_dh_legion_havoc_fel_rush::HandleAirWaterRush, EFFECT_1, SPELL_EFFECT_DUMMY);
        }
    }
};

class spell_dh_legion_havoc_fel_rush_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_fel_rush_AuraScript);

    void PreventEffect(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        PreventDefaultAction();
    }

    void PreventEffectAndHandleSpeedApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        PreventDefaultAction();

        UpdateSpeedAndGravity(true, GetSpellInfo()->Id == 197923);
    }

    void PreventEffectAndHandleSpeedRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        PreventDefaultAction();

        UpdateSpeedAndGravity(false, GetSpellInfo()->Id == 197923);
    }

    void UpdateSpeedAndGravity(bool enable, bool air)
    {
        Unit* target = GetTarget();
        target->UpdateSpeed(MOVE_RUN);
        target->UpdateSpeed(MOVE_RUN_BACK);
        target->UpdateSpeed(MOVE_SWIM);
        target->UpdateSpeed(MOVE_FLIGHT);
        target->UpdateSpeed(MOVE_WALK, true);

        if (air)
            target->SetDisableGravity(enable);
    }

    void RemoveAura(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        GetTarget()->UpdateSpeed(MOVE_RUN);
    }

    void Register() override
    {
        if (m_scriptSpellId == 197923 || m_scriptSpellId == 197922)
        {
            OnEffectApply += AuraEffectApplyFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffect, EFFECT_3, SPELL_AURA_MOD_MINIMUM_SPEED, AURA_EFFECT_HANDLE_REAL);
            OnEffectApply += AuraEffectApplyFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffect, EFFECT_4, SPELL_AURA_USE_NORMAL_MOVEMENT_SPEED, AURA_EFFECT_HANDLE_REAL);
            OnEffectApply += AuraEffectApplyFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffectAndHandleSpeedApply, m_scriptSpellId == 197923 ? EFFECT_9 : EFFECT_6, SPELL_AURA_MOD_MINIMUM_SPEED_RATE, AURA_EFFECT_HANDLE_REAL);

            OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffect, EFFECT_3, SPELL_AURA_MOD_MINIMUM_SPEED, AURA_EFFECT_HANDLE_REAL);
            OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffect, EFFECT_4, SPELL_AURA_USE_NORMAL_MOVEMENT_SPEED, AURA_EFFECT_HANDLE_REAL);
            OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffectAndHandleSpeedRemove, m_scriptSpellId == 197923 ? EFFECT_9 : EFFECT_6, SPELL_AURA_MOD_MINIMUM_SPEED_RATE, AURA_EFFECT_HANDLE_REAL);
        }

        if (m_scriptSpellId == 197923)
        {
            OnEffectApply += AuraEffectApplyFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffect, EFFECT_5, SPELL_AURA_488, AURA_EFFECT_HANDLE_REAL);
            OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_fel_rush_AuraScript::PreventEffect, EFFECT_5, SPELL_AURA_488, AURA_EFFECT_HANDLE_REAL);
            AfterEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_fel_rush_AuraScript::RemoveAura, EFFECT_9, SPELL_AURA_MOD_MINIMUM_SPEED_RATE, AURA_EFFECT_HANDLE_REAL);
        }
    }
};

// 232893 - Fel Blade Dummy
class spell_dh_legion_felblade : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_felblade);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_FELBLADE_CHARGE });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
            GetCaster()->CastSpell(target, SPELL_DH_FELBLADE_CHARGE, true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_felblade::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 213241 - Fel Blade Charge
class spell_dh_legion_felblade_charge : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_felblade_charge);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_FELBLADE_DMG });
    }

    void HandleCharge(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
            GetCaster()->CastSpell(target, SPELL_DH_FELBLADE_DMG, TRIGGERED_FULL_MASK);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_felblade_charge::HandleCharge, EFFECT_0, SPELL_EFFECT_CHARGE);
    }
};

// 203557 - Fel Blade Proc Aura
// 236167 - Fel Blade Proc Aura (Havoc)
// 204497 - Fel Blade Proc Visual
class spell_dh_legion_felblade_aura : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_felblade_aura);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();

        if (spell && GetTarget()->GetSpellHistory()->HasCooldown(SPELL_DH_FELBLADE)
            && (spell->Id == SPELL_DH_DEMONS_BITE
            || spell->Id == SPELL_DH_SHEAR
            || spell->Id == SPELL_DH_SEVER
            || spell->Id == SPELL_DH_DEMON_BLADES_DMG))
            return true;

        return false;
    }

    bool CheckVisualProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();
        return spell && spell->Id == SPELL_DH_FELBLADE_DMG;
    }

    void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        GetTarget()->GetSpellHistory()->ResetCooldown(SPELL_DH_FELBLADE, true);
    }

    void HandleVisualEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
    {
        aurEff->GetBase()->Remove();
    }

    void Register() override
    {
        switch (m_scriptSpellId)
        {
            case SPELL_DH_FELBLADE_PROC:
            case SPELL_DH_FELBLADE_PROC1:
                DoCheckProc += AuraCheckProcFn(spell_dh_legion_felblade_aura::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_dh_legion_felblade_aura::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
                break;
            case SPELL_DH_FELBLADE_PROC_VISUAL:
                DoCheckProc += AuraCheckProcFn(spell_dh_legion_felblade_aura::CheckVisualProc);
                OnEffectProc += AuraEffectProcFn(spell_dh_legion_felblade_aura::HandleVisualEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
                break;
        }
    }
};

// 203720 - Demon Spikes
class spell_dh_legion_vengeance_demon_spikes : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_demon_spikes);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_DEMON_SPIKES });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
        {
            if (Aura* aura = target->GetAura(SPELL_DH_DEMON_SPIKES))
                aura->SetDuration(aura->GetDuration() + aura->GetSpellInfo()->CalcDuration(target));
            else
                GetCaster()->CastSpell(target, SPELL_DH_DEMON_SPIKES, true);
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_demon_spikes::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 203719 - Demon Spikes
class spell_dh_legion_vengeance_demon_spikes_aura : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_demon_spikes_aura);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_DH_JAGGED_SPIKES_PROC });
    }

    void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* target = GetTarget();
        if (target->HasAura(SPELL_DH_JAGGED_SPIKES))
            target->CastSpell(target, SPELL_DH_JAGGED_SPIKES_PROC, true);
    }

    void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* target = GetTarget();
        if (target->HasAura(SPELL_DH_JAGGED_SPIKES))
            target->RemoveAura(SPELL_DH_JAGGED_SPIKES_PROC);
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_dh_legion_vengeance_demon_spikes_aura::HandleApply, EFFECT_0, SPELL_AURA_MOD_PARRY_PERCENT, AURA_EFFECT_HANDLE_REAL);
        OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_vengeance_demon_spikes_aura::HandleRemove, EFFECT_0, SPELL_AURA_MOD_PARRY_PERCENT, AURA_EFFECT_HANDLE_REAL);
    }
};

// 228477 - Soul Cleave
// 228478 - Soul Cleave Damage
class spell_dh_legion_vengeance_soul_cleave : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_soul_cleave);

    enum Values
    {
        POWER_COST,
    };

    void ConsumeAllSouls(SpellEffIndex effIndex)
    {
        int32 distance = GetSpell()->GetEffect(effIndex)->BasePoints;
        Unit* caster = GetCaster();

        for (AreaTrigger* at : caster->GetAreaTriggersByEntry({ AREATRIGGER_DH_SOUL_FRAGMENT_VENGEANCE, AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE, AREATRIGGER_DH_LESSER_SOUL_FRAGMENT }))
        {
            if (at->GetExactDist(caster) > distance)
                continue;

            caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DH_CONSUME_SOUL_VENGEANCE, true);
            at->Remove();
        }
    }

    void HandleHeal(SpellEffIndex effIndex)
    {
        Unit* target = GetHitUnit();

        int32 powerCost = GetSpell()->GetPowerCost(POWER_PAIN) / 10;

        int32 healAmount = int32(2.0f * 5.5f * GetCaster()->GetTotalAttackPowerValue(BASE_ATTACK) * powerCost / 60);

        healAmount = target->SpellHealingBonusDone(target, GetSpellInfo(), healAmount, HEAL, GetSpellInfo()->GetEffect(effIndex));
        healAmount = target->SpellHealingBonusTaken(target, GetSpellInfo(), healAmount, HEAL, GetSpellInfo()->GetEffect(effIndex));

        healAmount = GetSpellInfo()->GetEffect(effIndex)->CalcValue(target, &healAmount);

        SetHitHeal(healAmount);
    }

    void HandleFeastOfSouls()
    {
        Unit* caster = GetCaster();
        if (caster->HasAura(SPELL_DH_FEAST_OF_SOULS))
        {
            int32 healPerTick = int32(caster->GetTotalAttackPowerValue(BASE_ATTACK) * 1.3f * 3.f / sSpellMgr->AssertSpellInfo(SPELL_DH_FEAST_OF_SOULS_PERIODIC_HEAL)->GetMaxTicks(DIFFICULTY_NONE));
            caster->CastCustomSpell(SPELL_DH_FEAST_OF_SOULS_PERIODIC_HEAL, SPELLVALUE_BASE_POINT0, healPerTick, caster, true);
        }
    }

    void HandleTriggerSpell(SpellEffIndex /*effIndex*/, Spell* spell)
    {
        spell->SetCastExtraParam(POWER_COST, GetSpell()->GetPowerCost(POWER_PAIN) / 10);
    }

    void HandleDamage(SpellEffIndex /*effIndex*/)
    {
        int32 const* powerCost = GetCastExtraParam<int32>(POWER_COST);

        if (powerCost)
            SetHitDamage(2 * GetHitDamage() * *powerCost / 60);
    }

    void Register() override
    {
        switch (m_scriptSpellId)
        {
            case SPELL_DH_SOUL_CLEAVE:
                AfterCast += SpellCastFn(spell_dh_legion_vengeance_soul_cleave::HandleFeastOfSouls);
                OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_soul_cleave::ConsumeAllSouls, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_soul_cleave::HandleHeal, EFFECT_3, SPELL_EFFECT_HEAL);
                OnEffectTriggerSpell += SpellTriggerSpellFn(spell_dh_legion_vengeance_soul_cleave::HandleTriggerSpell, EFFECT_1, SPELL_EFFECT_TRIGGER_SPELL);
                break;
            case SPELL_DH_SOUL_CLEAVE_DMG:
                OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_soul_cleave::HandleDamage, EFFECT_1, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
                break;
        }
    }
};

class spell_dh_legion_havoc_blur : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_blur);

    void HandleCooldown()
    {
        // This is only needed because spells cast from spell_linked_spell are triggered by default
        // Spell::SendSpellCooldown() skips all spells with TRIGGERED_IGNORE_SPELL_AND_CATEGORY_CD
        GetCaster()->GetSpellHistory()->StartCooldown(GetSpellInfo(), 0, GetSpell());
    }

    void Register() override
    {
        AfterCast += SpellCastFn(spell_dh_legion_havoc_blur::HandleCooldown);
    }
};

// 191427 - Metamorphosis
class spell_dh_legion_havoc_metamorphosis : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_metamorphosis);

    SpellCastResult CheckCast()
    {
        if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
            return SPELL_FAILED_ROOTED;

        if (WorldLocation const* dest = GetExplTargetDest())
        {
            if (!GetCaster()->IsWithinLOS(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ()))
                return SPELL_FAILED_LINE_OF_SIGHT;

            GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_DH_METAMORPHOSIS, false);
        }

        return SPELL_FAILED_DONT_REPORT;
    }

    void HandleJump(SpellEffIndex /*effIndex*/)
    {
        WorldLocation const* dest = GetExplTargetDest();
        if (!dest)
            return;

        float dist = dest->GetExactDist(*GetCaster());
        float speedXY = dist + dist * (5.3f / GetSpellInfo()->GetMaxRange());
        float speedZ = 0.19642354884476423184854952469683f;

        JumpArrivalCastArgs arrivalCast;
        arrivalCast.SpellId = SPELL_DH_METAMORPHOSIS_IMPACT_DAMAGE;
        std::vector<JumpArrivalCastArgs> arrivalCasts({ arrivalCast });

        Optional<Movement::SpellEffectExtraData> extra;
        extra = boost::in_place();
        extra->ProgressCurveId = SPELL_DH_METAMORPHOSIS_PROGRESS_CURVE_ID;

        GetCaster()->GetMotionMaster()->MoveJump(*dest, speedXY, speedZ, EVENT_JUMP, false, arrivalCasts, extra.get_ptr());

        std::vector<uint32> spellIds = { SPELL_DH_EYE_BEAM, SPELL_DH_CHAOS_NOVA, SPELL_DH_BLUR, SPELL_DH_BLUR_TRIGGER };
        if (GetCaster()->HasAura(SPELL_DH_DEMON_REBORN))
            for (uint32 spellId : spellIds)
                GetCaster()->GetSpellHistory()->ResetCooldown(spellId, true);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_METAMORPHOSIS_DUMMY)
            OnCheckCast += SpellCheckCastFn(spell_dh_legion_havoc_metamorphosis::CheckCast);
        else if (m_scriptSpellId == SPELL_DH_METAMORPHOSIS)
            OnEffectLaunch += SpellEffectFn(spell_dh_legion_havoc_metamorphosis::HandleJump, EFFECT_1, SPELL_EFFECT_254);
    }
};

// 206478 - Demonic Appetite
// 221461 - Shattered Souls (ICD)
class spell_dh_legion_havoc_demonic_appetite : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_demonic_appetite);

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        Unit* caster = eventInfo.GetActor();
        Unit* target = eventInfo.GetProcTarget();
        SpellInfo const* procSpell = eventInfo.GetSpellInfo();

        if (!procSpell || (procSpell->Id != SPELL_DH_CHAOS_STRIKE_MH && procSpell->Id != SPELL_DH_ANNIHILATION_MH))
            return;

        if (!caster || !target)
            return;

        if (caster->GetSpellHistory()->HasCooldown(GetSpellInfo()->Id))
            return;

        target->CastSpell(caster, roll_chance_i(50) ? 228533 : 237867, true);
        caster->GetSpellHistory()->AddCooldown(GetSpellInfo()->Id, 0, std::chrono::milliseconds(500));
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE)
            return;

        Unit* target = GetTarget();

        std::vector<AreaTrigger*> areaTriggers = target->GetAreaTriggers(228537);
        for (auto at : areaTriggers)
        {
            target->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), 228542, true);
            at->Remove();
        }
    }

    void Register() override
    {
        if (m_scriptSpellId == 206478)
            OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_demonic_appetite::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);

        if (m_scriptSpellId == 221461)
            OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_havoc_demonic_appetite::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 228533 - Shattered Souls (Demonic Appetite)
// 237867 - Shattered Souls (Demonic Appetite)
// 209651 - Shattered Souls (Havoc Kill Proc)
class spell_dh_legion_havoc_shattered_souls_missile : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_shattered_souls_missile);

    void HandleSoulFragment(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);

        WorldLocation const* loc = GetHitDest();
        if (!loc)
            return;

        uint32 spellId = 228536;

        if (GetSpellInfo()->Id == 209651)
        {
            spellId = 209687;

            if (GetCaster()->GetCreatureType() == CREATURE_TYPE_DEMON)
                spellId = 226370;
        }

        if (Unit* target = GetExplTargetUnit())
            target->CastSpell(loc->GetPositionX(), loc->GetPositionY(), loc->GetPositionZ(), spellId, true);
    }

    void Register() override
    {
        OnEffectLaunch += SpellEffectFn(spell_dh_legion_havoc_shattered_souls_missile::HandleSoulFragment, EFFECT_1, SPELL_EFFECT_TRIGGER_MISSILE);
    }
};

struct at_dh_havoc_shattered_souls : public AreaTriggerAI
{
public:
    at_dh_havoc_shattered_souls(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitEnter(Unit* unit) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || caster != unit)
            return;

        if (caster->HasAura(SPELL_DH_MOVE_MARKER))
            return;

        AbsorbSoul(at, caster);
    }

    void OnUpdate(uint32 /*diff*/) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || caster->HasAura(SPELL_DH_MOVE_MARKER))
            return;

        auto itr = at->GetInsideUnits().find(at->GetCasterGuid());
        if (itr != at->GetInsideUnits().end())
            AbsorbSoul(at, caster);
    }
};

// 189110 - Infernal Strike
class spell_dh_legion_vengeance_infernal_strike : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_infernal_strike);

    SpellCastResult CheckCast()
    {
        if (GetCaster()->HasUnitState(UNIT_STATE_ROOT))
            return SPELL_FAILED_ROOTED;

        if (WorldLocation const* dest = GetExplTargetDest())
        {
            if (!GetCaster()->IsWithinLOS(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ()))
                return SPELL_FAILED_LINE_OF_SIGHT;

            GetCaster()->CastSpell(dest->GetPositionX(), dest->GetPositionY(), dest->GetPositionZ(), SPELL_DH_INFERNAL_STRIKE_JUMP, false);
        }

        return SPELL_FAILED_DONT_REPORT;
    }

    void HandleJump(SpellEffIndex /*effIndex*/)
    {
        WorldLocation const* dest = GetExplTargetDest();
        if (!dest)
            return;

        float dist = dest->GetExactDist(*GetCaster());
        float speedMult = GetCaster()->HasAura(SPELL_DH_ABYSSAL_STRIKE) ? 7.9f : 16.0f;
        float speedXY = dist + dist * (speedMult / GetSpellInfo()->GetMaxRange());
        float speedZ = 0.19642354884476423184854952469683f;

        std::vector<JumpArrivalCastArgs> arrivalCasts;
        std::vector<int32> triggeredSpellIds = { SPELL_DH_INFERNAL_STRIKE_IMPACT_DAMAGE };

        if (GetCaster()->HasAura(SPELL_DH_FLAME_CRASH))
            triggeredSpellIds.push_back(SPELL_DH_SIGIL_OF_FLAME_FLAME_CRASH);

        if (GetCaster()->HasAura(SPELL_DH_RAIN_OF_CHAOS))
            triggeredSpellIds.push_back(SPELL_DH_RAIN_OF_CHAOS_IMPACT);

        for (int32 spellId : triggeredSpellIds)
        {
            JumpArrivalCastArgs arrivalCast;
            arrivalCast.SpellId = spellId;
            arrivalCasts.push_back(arrivalCast);
        }

        Optional<Movement::SpellEffectExtraData> extra;
        extra = boost::in_place();
        extra->ProgressCurveId = SPELL_DH_INFERNAL_STRIKE_PROGRESS_CURVE_ID;

        GetCaster()->GetMotionMaster()->MoveJump(*dest, speedXY, speedZ, EVENT_JUMP, false, arrivalCasts, extra.get_ptr());
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_INFERNAL_STRIKE_CAST)
            OnCheckCast += SpellCheckCastFn(spell_dh_legion_vengeance_infernal_strike::CheckCast);
        else if (m_scriptSpellId == SPELL_DH_INFERNAL_STRIKE_JUMP)
            OnEffectLaunch += SpellEffectFn(spell_dh_legion_vengeance_infernal_strike::HandleJump, EFFECT_0, SPELL_EFFECT_254);
    }
};

// 203783 - Shear (Shattered Soul Proc)
class spell_dh_legion_vengeance_shear : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_shear);

    static int32 constexpr const shatterChance[] = { 4, 12, 25, 40, 60, 80, 90, 100 };

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        Unit* caster = eventInfo.GetActor();
        Unit* target = eventInfo.GetProcTarget();
        SpellInfo const* procSpell = eventInfo.GetSpellInfo();

        if (!procSpell || (procSpell->Id != SPELL_DH_SHEAR && procSpell->Id != SPELL_DH_SEVER))
            return false;

        if (!caster || !target)
            return false;

        if (procSpell->Id == SPELL_DH_SEVER)
            return true;

        ASSERT(shearCounter < std::extent<decltype(shatterChance)>::value);
        int32 chance = shatterChance[shearCounter];

        if (caster->HealthBelowPct(50))
        {
            if (AuraEffect* shatterTheSouls = caster->GetAuraEffect(212827, EFFECT_0))
                chance += shatterTheSouls->GetAmount();
        }

        if (!roll_chance_i(chance))
        {
            shearCounter++;
            return false;
        }

        shearCounter = 0;

        return true;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        SpellCastTargets targets;
        targets.SetUnitTarget(eventInfo.GetActor());

        eventInfo.GetProcTarget()->CastSpell(targets, roll_chance_i(50) ? SPELL_DH_SHATTER_SOUL : SPELL_DH_SHATTER_SOUL_1, true, nullptr, aurEff->GetSpellInfo(), aurEff->GetCasterGUID());
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_vengeance_shear::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_shear::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
    }

private:
    uint8 shearCounter = 0;
};
int32 constexpr const spell_dh_legion_vengeance_shear::shatterChance[];

// 209980 - Shatter Soul
// 209981 - Shatter Soul
// 210038 - Shatter Soul (Vengeance Demon)
class spell_dh_legion_vengeance_shatter_soul : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_shatter_soul);

    #define MAX_SOUL_FRAGMENT_COUNT 4

    void HandleSoulFragment(SpellEffIndex /*effIndex*/)
    {
        WorldLocation const* loc(GetExplTargetDest());
        if (!loc)
            return;

        if (GetSpellInfo()->Id == 210038)
        {
            GetHitUnit()->CastSpell(loc->GetPositionX(), loc->GetPositionY(), loc->GetPositionZ(), GetCaster()->GetCreatureType() == CREATURE_TYPE_DEMON ? 226264 : 226263, true);
        }
        else
        {
            SpellInfo const* triggeringSpell = GetTriggeringSpell();

            if (triggeringSpell && triggeringSpell->Id != 187727)
            {
                std::vector<AreaTrigger*> areaTriggers = GetHitUnit()->GetAreaTriggersByEntry({ AREATRIGGER_DH_SOUL_FRAGMENT_VENGEANCE });
                if (areaTriggers.size() > MAX_SOUL_FRAGMENT_COUNT)
                {
                    AreaTrigger* at = Trinity::Containers::SelectRandomContainerElement(areaTriggers);
                    at->Remove();
                }
            }

            SpellCastTargets targets;
            targets.SetDst(loc->GetPositionX(), loc->GetPositionY(), loc->GetPositionZ(), GetHitUnit()->GetOrientation());

            GetHitUnit()->CastSpell(targets, SPELL_DH_SHATTERED_SOUL, true, nullptr, GetTriggeringSpell(), GetHitUnit()->GetGUID());
        }
    }

    void Register() override
    {
        OnEffectLaunchTarget += SpellEffectFn(spell_dh_legion_vengeance_shatter_soul::HandleSoulFragment, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 236189 - Demonic Infusion
class spell_dh_legion_vengeance_demonic_infusion : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_demonic_infusion);

    void HandleCharges(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();

        if (Aura* aura = caster->GetAura(SPELL_DH_DEMON_SPIKES))
            aura->SetDuration(aura->GetDuration() + aura->GetSpellInfo()->CalcDuration(caster));
        else
            caster->CastSpell(caster, SPELL_DH_DEMON_SPIKES, true);

        caster->GetSpellHistory()->ResetCharges(sSpellMgr->AssertSpellInfo(SPELL_DH_DEMON_SPIKES_TRIGGER)->ChargeCategoryId);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_demonic_infusion::HandleCharges, EFFECT_0, SPELL_EFFECT_ENERGIZE);
    }
};

// 209258 - Last Resort
class spell_dh_legion_vengeance_last_resort : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_last_resort);

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
    {
        // Set absorbtion amount to unlimited
        amount = -1;
    }

    void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
    {
        Unit* caster = GetCaster();
        if (!caster || caster->HasAura(SPELL_DH_UNCONTAINED_FEL))
            return;

        if (caster->GetHealth() <= dmgInfo.GetDamage())
        {
            absorbAmount = dmgInfo.GetDamage();
            caster->CastSpell(caster, SPELL_DH_UNCONTAINED_FEL, true);
            caster->CastSpell(caster, SPELL_DH_METAMORPHOSIS_VENGEANCE_TRANSFORM, true);
            caster->SetHealth(caster->CountPctFromMaxHealth(GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(caster)));
        }
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dh_legion_vengeance_last_resort::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_dh_legion_vengeance_last_resort::Absorb, EFFECT_0);
    }
};

class spell_dh_legion_vengeance_fiery_brand : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_fiery_brand);

    void HandleHitTarget(SpellEffIndex /*effIndex*/)
    {
        GetCaster()->CastSpell(GetCaster(), 204022, true);
    }

    void HandleVictimDebuff(SpellEffIndex /*effIndex*/)
    {
        GetCaster()->CastSpell(GetHitUnit(), GetCaster()->HasAura(SPELL_DH_BURNING_ALIVE) ? SPELL_DH_FIERY_BRAND_DOT : SPELL_DH_FIERY_BRAND_DMG_REDUCTION_DEBUFF, true);
    }

    void Register() override
    {
        if (m_scriptSpellId == 204021)
        {
            OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_fiery_brand::HandleHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
            OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_fiery_brand::HandleVictimDebuff, EFFECT_1, SPELL_EFFECT_SCHOOL_DAMAGE);
        }
    }
};

// 204022 - Fiery Brand
class spell_dh_legion_vengeance_fiery_brand_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_fiery_brand_AuraScript);

    void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        HandleBurningAlive();
    }

    void OnPeriodic(AuraEffect const* /*aurEff*/)
    {
        HandleBurningAlive();
    }

    void HandleBurningAlive()
    {
        if (GetCaster()->HasAura(SPELL_DH_BURNING_ALIVE))
            GetCaster()->CastSpell(GetCaster(), SPELL_DH_BURNING_ALIVE_TARGET_SELECTOR, true);
    }

    void Register() override
    {
        if (m_scriptSpellId == 204022)
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_vengeance_fiery_brand_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            OnEffectApply += AuraEffectApplyFn(spell_dh_legion_vengeance_fiery_brand_AuraScript::HandleApply, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    }
};

class spell_dh_legion_vengeance_burning_alive : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_burning_alive);

    void SelectTarget(std::list<WorldObject*>& targets)
    {
        targets.remove_if(Trinity::UnitAuraCheck(true, SPELL_DH_FIERY_BRAND_DOT, GetCaster()->GetGUID()));

        if (targets.size() > 1)
            Trinity::Containers::RandomResize(targets, 1);
    }

    void HandleHitTarget(SpellEffIndex /*effIndex*/)
    {
        GetCaster()->CastSpell(GetHitUnit(), SPELL_DH_FIERY_BRAND_DOT, true);
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_dh_legion_vengeance_burning_alive::SelectTarget, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_burning_alive::HandleHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 211048 - Chaos Blades
// 7.1.5 Change to prevent snapshotting mastery
class spell_dh_legion_havoc_chaos_blades : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_chaos_blades);

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_DH_HAVOC_MASTERY, EFFECT_0))
            amount = aurEff->GetAmount();
    }

    void RecalculateAmount(AuraEffect const* /*aurEff*/)
    {
        if (AuraEffect* aurEff = GetCaster()->GetAuraEffect(SPELL_DH_HAVOC_MASTERY, EFFECT_0))
            GetEffect(EFFECT_1)->SetAmount(aurEff->GetAmount());
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dh_legion_havoc_chaos_blades::CalculateAmount, EFFECT_1, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE);
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_havoc_chaos_blades::RecalculateAmount, EFFECT_2, SPELL_AURA_PERIODIC_DUMMY);
    }
};

class spell_dh_legion_havoc_artifact_feast_on_the_souls : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_artifact_feast_on_the_souls);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();
        return spell && spell->Id == 228532;
    }

    void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
    {
        GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_DH_EYE_BEAM, -aurEff->GetAmount());
        GetTarget()->GetSpellHistory()->ModifyCooldown(SPELL_DH_CHAOS_NOVA, -aurEff->GetAmount());
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_artifact_feast_on_the_souls::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_artifact_feast_on_the_souls::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

class spell_dh_legion_havoc_prepared : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_prepared);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();
        return spell && spell->Id == SPELL_DH_VENGEFUL_RETREAT;
    }

    void HandleProc(ProcEventInfo& /*eventInfo*/)
    {
        GetTarget()->CastSpell(GetTarget(), GetAura()->GetSpellEffectInfo(EFFECT_0)->TriggerSpell, true);
        GetAura()->AddProcCooldown(std::chrono::steady_clock::now() + Milliseconds(500));
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_prepared::CheckProc);
        OnProc += AuraProcFn(spell_dh_legion_havoc_prepared::HandleProc);
    }
};

class spell_dh_legion_havoc_chaos_cleave : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_chaos_cleave);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();

        if (!eventInfo.GetDamageInfo())
            return false;

        if (spell && (spell->Id == SPELL_DH_CHAOS_STRIKE_MH || spell->Id == SPELL_DH_CHAOS_STRIKE_OH || spell->Id == SPELL_DH_ANNIHILATION_MH || spell->Id == SPELL_DH_ANNIHILATION_OH))
            return true;

        return false;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), GetAura()->GetSpellEffectInfo(EFFECT_1)->CalcValue());

        // magic number
        eventInfo.GetActor()->CastCustomSpell(236237, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetProcTarget(), true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_chaos_cleave::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_chaos_cleave::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
    }
};

class spell_dh_legion_havoc_chaos_nova : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_chaos_nova);

    void HandleApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        AuraEffect* overwhelmingPower = caster->GetAuraEffect(201464, EFFECT_0);
        if (!overwhelmingPower)
            return;

        if (!roll_chance_i(overwhelmingPower->GetAmount()))
            return;

        Unit* target = GetTarget();

        target->CastSpell(caster, roll_chance_i(50) ? 228533 : 237867, true);
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_dh_legion_havoc_chaos_nova::HandleApply, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL);
    }
};

class spell_dh_legion_havoc_demonic_appetite_energize : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_demonic_appetite_energize);

    SpellCastResult CheckTalent()
    {
        return !GetCaster()->HasAura(206478) ? SPELL_FAILED_DONT_REPORT : SPELL_CAST_OK;
    }

    void Register() override
    {
        OnCheckCast += SpellCheckCastFn(spell_dh_legion_havoc_demonic_appetite_energize::CheckTalent);
    }
};

class spell_dh_legion_havoc_fel_mastery : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_fel_mastery);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();
        return spell && spell->Id == SPELL_DH_FEL_RUSH_DMG;
    }

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        GetTarget()->CastSpell(GetTarget(), 234244, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_fel_mastery::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_fel_mastery::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 187727 - Immolation Aura
class spell_dh_legion_vengeance_immolation_aura : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_immolation_aura);

    void HandleInitialDamage(SpellEffIndex /*effIndex*/)
    {
        // Immolation Aura Burst can only spawn 5 Soul fragments per cast
        if (targetCount++ < 5)
        {
            if (GetCaster()->HasAura(227174) && roll_chance_i(60)) // HACK: There is no source about the Proc Chance
            {
                // Not sure if they should remove existing ones and spawn new souls
                Aura* aura = GetCaster()->GetAura(203981);
                if (aura && aura->GetStackAmount() == aura->GetSpellInfo()->StackAmount)
                    return;

                SpellCastTargets targets;
                targets.SetUnitTarget(GetCaster());
                GetHitUnit()->CastSpell(targets, roll_chance_i(50) ? SPELL_DH_SHATTER_SOUL : SPELL_DH_SHATTER_SOUL_1, true, nullptr, GetSpellInfo());
            }
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_immolation_aura::HandleInitialDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
    }

    private:
        int8 targetCount = 0;
};

struct at_dh_vengeance_soul_fragment : public AreaTriggerAI
{
public:
    at_dh_vengeance_soul_fragment(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnCreate() override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        caster->CastSpell(caster, SPELL_DH_SOUL_FRAGMENT_COUNTER, true);
    }

    void OnRemove() override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        caster->RemoveAuraFromStack(SPELL_DH_SOUL_FRAGMENT_COUNTER);
    }

    void OnUnitEnter(Unit* unit) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || caster != unit)
            return;

        if (caster->HasAura(SPELL_DH_MOVE_MARKER))
            return;

        AbsorbSoul(at, caster);
    }

    void OnUpdate(uint32 /*diff*/) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || caster->HasAura(SPELL_DH_MOVE_MARKER))
            return;

        auto itr = at->GetInsideUnits().find(at->GetCasterGuid());
        if (itr != at->GetInsideUnits().end())
            AbsorbSoul(at, caster);
    }
};

// 209795 - Fracture
class spell_dh_legion_vengeance_fracture : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_fracture);

    void HandleSoulFragments(SpellEffIndex /*effIndex*/)
    {
        SpellCastTargets targets;
        targets.SetUnitTarget(GetCaster());
        for (int i = 0; i < 2; ++i)
            GetHitUnit()->CastSpell(targets, roll_chance_i(50) ? SPELL_DH_SHATTER_SOUL : SPELL_DH_SHATTER_SOUL_1, true, nullptr, GetSpellInfo());
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_fracture::HandleSoulFragments, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 208014 - Consume Soul
class spell_dh_legion_vengeance_consume_soul : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_consume_soul);

    enum Values
    {
        FEED_THE_DEMON_CD_REDUCE = 1000,
    };

    void HandleConsumeSoul()
    {
        Unit* caster = GetCaster();
        if (caster->HasAura(SPELL_DH_FEED_THE_DEMON))
            caster->GetSpellHistory()->ReduceChargeTime(sSpellMgr->AssertSpellInfo(SPELL_DH_DEMON_SPIKES_TRIGGER)->ChargeCategoryId, FEED_THE_DEMON_CD_REDUCE);

        if (caster->HasAura(SPELL_DH_PAINBRINGER))
            caster->CastSpell(caster, SPELL_DH_PAINBRINGER_REDUCTION, true);

        if (Aura* soulbarrier = caster->GetAura(SPELL_DH_SOUL_BARRIER))
        {
            // "Consuming a Soul Fragment adds ${$s2/100*$AP*$pctH} to the shield."
            AuraEffect* absorb = soulbarrier->GetEffect(EFFECT_0);
            AuraEffect* dummy = soulbarrier->GetEffect(EFFECT_1);
            int32 amount = absorb->GetAmount() + int32(float(dummy->GetAmount()) / 100.f * caster->GetTotalAttackPowerValue(BASE_ATTACK));
            absorb->ChangeAmount(amount);
        }
    }

    void Register() override
    {
        AfterCast += SpellCastFn(spell_dh_legion_vengeance_consume_soul::HandleConsumeSoul);
    }
};

// 218256 - Empower Wards
class spell_dh_legion_vengeance_empower_wards : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_empower_wards);

    void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
    {
        // Set absorbtion amount to unlimited
        amount = -1;
    }

    void Absorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& /*absorbAmount*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        AuraEffect* siphonPower = caster->GetAuraEffect(SPELL_DH_SIPHON_POWER, EFFECT_0);
        if (!siphonPower)
            return;

        savedAmount += dmgInfo.GetDamage();

        double value = floor((double)savedAmount / (double)caster->GetMaxHealth() * 100);

        uint32 attackPowerAmount = std::min((uint32)value, (uint32)siphonPower->GetAmount());

        caster->CastCustomSpell(SPELL_DH_SIPHONED_POWER, SPELLVALUE_BASE_POINT0, attackPowerAmount, caster, true);
    }

    private:
        uint32 savedAmount = 0;

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dh_legion_vengeance_empower_wards::CalculateAmount, EFFECT_1, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_dh_legion_vengeance_empower_wards::Absorb, EFFECT_1);
    }
};

// 211509 - Solitude
class spell_dh_legion_honor_solitude : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_honor_solitude);

    void CheckTargetsInRange(AuraEffect const* /*aurEff*/)
    {
        Unit* caster = GetTarget();
        if (AnyPlayerInRange(caster))
        {
            caster->RemoveAura(SPELL_DH_SOLITUDE);
            return;
        }

        caster->CastSpell(caster, SPELL_DH_SOLITUDE, true);
    }

    bool AnyPlayerInRange(Unit* caster) const
    {
        float searchRadius = float(GetAura()->GetSpellEffectInfo(EFFECT_1)->CalcValue());

        std::list<Player*> players;
        Trinity::AnyPlayerInObjectRangeCheck checker(caster, searchRadius);
        Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(caster, players, checker);
        Cell::VisitWorldObjects(caster, searcher, searchRadius);

        players.remove_if([caster](Unit* target)
        {
            return caster == target || !caster->IsValidAssistTarget(target);
        });

        return !players.empty();
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_honor_solitude::CheckTargetsInRange, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
    }
};

// 213017 - Fueled by Pain
class spell_dh_legion_vengeance_artifact_fueled_by_pain : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_artifact_fueled_by_pain);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();
        return spell && spell->Id == SPELL_DH_CONSUME_SOUL_HEAL;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
    {
        if (GetTarget()->HasAura(SPELL_DH_METAMORPHOSIS_VENGEANCE_TRANSFORM))
            return;

        GetTarget()->CastCustomSpell(SPELL_DH_METAMORPHOSIS_VENGEANCE_TRANSFORM, SPELLVALUE_AURA_DURATION, aurEff->GetAmount() * IN_MILLISECONDS, GetTarget(), true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_vengeance_artifact_fueled_by_pain::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_artifact_fueled_by_pain::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 212817 - Fiery Demise
class spell_dh_legion_vengeance_artifact_fiery_demise : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_artifact_fiery_demise);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spell = eventInfo.GetSpellInfo();
        return spell && spell->Id == SPELL_DH_FIERY_BRAND;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        GetTarget()->CastCustomSpell(212818, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), eventInfo.GetProcTarget(), true);
    }

    void HandleDuration(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Aura* fieryBrand = GetTarget()->GetAura(SPELL_DH_FIERY_BRAND_DOT);
        if (!fieryBrand)
        {
            fieryBrand = GetTarget()->GetAura(SPELL_DH_FIERY_BRAND_DMG_REDUCTION_DEBUFF);

            if (!fieryBrand)
                return;
        }

        GetAura()->SetDuration(fieryBrand->GetDuration());
    }

    void Register() override
    {
        if (m_scriptSpellId == 212817)
        {
            DoCheckProc += AuraCheckProcFn(spell_dh_legion_vengeance_artifact_fiery_demise::CheckProc);
            OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_artifact_fiery_demise::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
        }
        if (m_scriptSpellId == 212818)
        {
            OnEffectApply += AuraEffectApplyFn(spell_dh_legion_vengeance_artifact_fiery_demise::HandleDuration, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PCT_TAKEN_FROM_CASTER, AURA_EFFECT_HANDLE_REAL);
        }
    }
};

// 213010 - Charred Warblades
class spell_dh_legion_vengeance_artifact_charred_warblades : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_artifact_charred_warblades);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (!eventInfo.GetDamageInfo())
            return false;

        return eventInfo.GetDamageInfo()->GetSchoolMask() & SPELL_SCHOOL_MASK_FIRE;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        uint32 heal = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());

        GetTarget()->CastCustomSpell(213011, SPELLVALUE_BASE_POINT0, heal, GetTarget(), true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_vengeance_artifact_charred_warblades::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_artifact_charred_warblades::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 212871 - Defensive Spikes
class spell_dh_legion_vengeance_artifact_defensive_spikes : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_artifact_defensive_spikes);

    void HandleApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        if (AuraEffect* demonSpikes = GetTarget()->GetAuraEffect(SPELL_DH_DEMON_SPIKES, EFFECT_0))
            demonSpikes->ChangeAmount(demonSpikes->GetAmount() + aurEff->GetAmount());
    }

    void HandleRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        if (AuraEffect* demonSpikes = GetTarget()->GetAuraEffect(SPELL_DH_DEMON_SPIKES, EFFECT_0))
            demonSpikes->ChangeAmount(demonSpikes->GetAmount() - aurEff->GetAmount());
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_dh_legion_vengeance_artifact_defensive_spikes::HandleApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_vengeance_artifact_defensive_spikes::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

struct at_dh_vengeance_sigil_of_flame : public AreaTriggerAI
{
public:
    at_dh_vengeance_sigil_of_flame(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnRemove() override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DH_SIGIL_OF_FLAME_DAMAGE, true);
    }
};

class spell_dh_legion_vengeance_sigil_of_flame : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_sigil_of_flame);

    void HandleApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        const_cast<AuraEffect*>(aurEff)->SetDonePct(caster->SpellDamagePctDone(GetTarget(), GetSpellInfo(), DOT, aurEff->GetSpellEffectInfo()));
        const_cast<AuraEffect*>(aurEff)->SetDamage(int32(caster->SpellDamageBonusDone(GetTarget(), GetSpellInfo(), 0, DOT, GetSpellInfo()->GetEffect(EFFECT_2), GetStackAmount()) * aurEff->GetDonePct()));
        const_cast<AuraEffect*>(aurEff)->SetCritChance(caster->GetUnitSpellCriticalChance(GetTarget(), GetSpellInfo(), GetSpellInfo()->GetSchoolMask()));
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_dh_legion_vengeance_sigil_of_flame::HandleApply, EFFECT_1, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);
    }
};

struct at_dh_vengeance_sigil_of_misery : public AreaTriggerAI
{
public:
    at_dh_vengeance_sigil_of_misery(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnRemove() override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DH_SIGIL_OF_MISERY, true);
    }
};

struct at_dh_vengeance_sigil_of_silence : public AreaTriggerAI
{
public:
    at_dh_vengeance_sigil_of_silence(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnRemove() override
    {
        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        caster->CastSpell(at->GetPositionX(), at->GetPositionY(), at->GetPositionZ(), SPELL_DH_SIGIL_OF_SILENCE, true);
    }
};

// 205478 - Desperate Instincts
class spell_dh_legion_havoc_desperate_instincts : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_desperate_instincts);

    bool Validate(SpellInfo const* /*spellInfo*/)
    {
        return ValidateSpellInfo({ SPELL_DH_BLUR, SPELL_DH_BLUR_TRIGGER });
    }

    void HandlePeriodic(AuraEffect const* /*aurEff*/)
    {
        PreventDefaultAction();

        Unit* target = GetTarget();
        if (!target->GetSpellHistory()->HasCooldown(SPELL_DH_BLUR))
            target->CastSpell(target, SPELL_DH_BLUR_TRIGGER, TriggerCastFlags(TRIGGERED_FULL_MASK & ~TRIGGERED_IGNORE_SPELL_AND_CATEGORY_CD));
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_havoc_desperate_instincts::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
    }
};

// 178940 - Shattered Soul (Havoc Proc)
class spell_dh_legion_havoc_shattered_souls : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_shattered_souls);

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        Unit* caster = eventInfo.GetActor();
        Unit* target = eventInfo.GetProcTarget();

        if (!caster || !target)
            return;

        target->CastSpell(caster, 209651, true);
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_shattered_souls::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 204254 - Shattered Soul (Vengeance Proc)
class spell_dh_legion_vengeance_shattered_souls : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_shattered_souls);

    void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        Unit* caster = eventInfo.GetActor();
        Unit* target = eventInfo.GetProcTarget();

        if (!caster || !target)
            return;

        target->CastSpell(caster, SPELL_DH_SHATTER_SOUL_2, true);
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_shattered_souls::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

// 227225 - Soul Barrier
class spell_dh_legion_vengeance_soul_barrier : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_soul_barrier);

    void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& /*canBeRecalculated*/)
    {
        Unit* owner = GetUnitOwner();
        float ap = owner->GetTotalAttackPowerValue(BASE_ATTACK);

        // "Shield yourself for $d, absorbing ${$s1/100*$AP*$pctH} damage"
        amount = int32(aurEff->GetSpellEffectInfo()->BonusCoefficientFromAP * ap);

        // "Consumes all Soul Fragments within 20 yds."
        for (AreaTrigger* at : owner->GetAreaTriggersByEntry({ AREATRIGGER_DH_SOUL_FRAGMENT_VENGEANCE, AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE, AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE_DEMON }))
            if (owner->GetDistance(at) <= 20.f)
                AbsorbSoul(at, owner);
    }

    void HandleAbsorb(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
    {
        if (!GetEffect(EFFECT_2))
            return;

        int32 minAmount = int32(GetSpellInfo()->GetEffect(EFFECT_2)->BonusCoefficientFromAP * GetUnitOwner()->GetTotalAttackPowerValue(BASE_ATTACK));

        // "Soul Barrier's absorption cannot be reduced below ${$s3/100*$AP*$pctH}."
        if (int32(aurEff->GetAmount() - dmgInfo.GetDamage()) <= minAmount)
        {
            absorbAmount = 0;
            dmgInfo.AbsorbDamage(std::max(minAmount, aurEff->GetAmount()));

            if (aurEff->GetAmount() != minAmount)
                GetEffect(EFFECT_0)->ChangeAmount(minAmount);
        }
    }

    void HandleProc(ProcEventInfo& /*eventInfo*/)
    {
        //// "Consuming a Soul Fragment adds ${$s2/100*$AP*$pctH} to the shield."
        //if (AuraEffect* absorb = GetEffect(EFFECT_0))
        //{
        //    int32 amount = absorb->GetAmount() + int32(GetEffect(EFFECT_1)->GetAmount() / 100.f * GetTarget()->GetTotalAttackPowerValue(BASE_ATTACK));
        //    absorb->ChangeAmount(amount);
        //}
    }

    void Register() override
    {
        DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_dh_legion_vengeance_soul_barrier::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
        OnEffectAbsorb += AuraEffectAbsorbFn(spell_dh_legion_vengeance_soul_barrier::HandleAbsorb, EFFECT_0);
        //OnProc += AuraProcFn(spell_dh_legion_vengeance_soul_barrier_AuraScript::HandleProc);
    }
};

class spell_dh_legion_vengeance_spirit_bomb : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_spirit_bomb);

    SpellCastResult CheckCast()
    {
        for (AreaTrigger* trigger : GetCaster()->GetAreaTriggersByEntry({ AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE_DEMON, AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE, AREATRIGGER_DH_SOUL_FRAGMENT_VENGEANCE }))
            if (GetCaster()->GetDistance(trigger) <= 20.f)
                return SPELL_CAST_OK;

        SetCustomCastResultMessage(SPELL_CUSTOM_ERROR_REQUIRES_NEARBY_SOUL_FRAGMENT);
        return SPELL_FAILED_CUSTOM_ERROR;
    }

    void OnCastEffectHitTarget(SpellEffIndex /*effIndex*/)
    {
        std::vector<AreaTrigger*> areaTriggers = GetCaster()->GetAreaTriggersByEntry({ AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE_DEMON, AREATRIGGER_DH_SHATTERED_SOULS_VENGEANCE, AREATRIGGER_DH_SOUL_FRAGMENT_VENGEANCE });
        if (areaTriggers.empty())
            return;

        AreaTrigger* at = areaTriggers.front();
        SpellCastTargets targets;
        targets.SetUnitTarget(GetCaster());
        targets.SetDst(*at);

        GetHitUnit()->CastSpell(targets, SPELL_DH_SPIRIT_BOMB_VISUAL, true, nullptr, nullptr, GetCaster()->GetGUID());
        at->Remove();
    }

    void OnVisualEffectHitTarget(SpellEffIndex /*effIndex*/)
    {
        GetHitUnit()->CastSpell(GetCaster(), SPELL_DH_SPIRIT_BOMB_DAMAGE, true);
        GetHitUnit()->CastSpell(GetCaster(), SPELL_DH_FRAILTY, true);
    }

    void Register() override
    {
        if (m_scriptSpellId == SPELL_DH_SPIRIT_BOMB_VISUAL)
            OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_spirit_bomb::OnVisualEffectHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
        else
        {
            OnCheckCast += SpellCheckCastFn(spell_dh_legion_vengeance_spirit_bomb::CheckCast);
            OnEffectHitTarget += SpellEffectFn(spell_dh_legion_vengeance_spirit_bomb::OnCastEffectHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
        }
    }
};

// 203704 - Mana Break
class spell_dh_legion_havoc_honor_mana_break : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_honor_mana_break);

    void HandleHit(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();
        Unit* target = GetHitUnit();
        int32 healthPct = GetEffectValue();
        int32 damagePctLimit = GetSpellInfo()->GetEffect(EFFECT_3)->CalcValue(caster);
        int32 currentPctDamage = int32((100.f - target->GetPowerPct(POWER_MANA)) / GetSpellInfo()->GetEffect(EFFECT_2)->CalcValue(caster) * GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(caster));

        // "Deals $m1% of the target's maximum health in Chaos damage. Damage is increased the lower the target's current mana, up to $m4% of the target's maximum health in damage."
        // taken from 7.1.5 patch notes: "Total health in damage is increased by 1% per 2% of the target�s total missing mana"
        if (target->GetMaxPower(POWER_MANA))
            healthPct += std::min(currentPctDamage, damagePctLimit);

        SetHitDamage(int32(target->CountPctFromMaxHealth(healthPct)));
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_havoc_honor_mana_break::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
    }
};

// 203468 - Glimpse
class spell_dh_legion_havoc_honor_glimpse : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_honor_glimpse);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        SpellInfo const* spellInfo = eventInfo.GetSpellInfo();
        return spellInfo && spellInfo->Id == SPELL_DH_VENGEFUL_RETREAT_TRIGGER;
    }

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
    {
        Unit* owner = GetUnitOwner();

        // "You automatically gain Blur for 3 sec after you use Vengeful Retreat."
        if (Aura* blur = owner->GetAura(SPELL_DH_BLUR))
        {
            blur->SetDuration(blur->GetDuration() + aurEff->GetAmount());

            if (blur->GetDuration() > blur->GetMaxDuration())
                blur->SetMaxDuration(blur->GetDuration());
        }
        else
            owner->CastCustomSpell(SPELL_DH_BLUR, SPELLVALUE_AURA_DURATION, aurEff->GetAmount(), owner, true);
    }

    void Register() override
    {
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_havoc_honor_glimpse::CheckProc);
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_honor_glimpse::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

class spell_dh_legion_havoc_honor_awaken_the_demon_within : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_havoc_honor_awaken_the_demon_within);

    void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
    {
        Unit* owner = GetUnitOwner();
        Aura* metamorphosis = owner->GetAura(SPELL_DH_METAMORPHOSIS_TRANSFORM);
        int32 duration = GetSpellInfo()->GetEffect(EFFECT_2)->CalcValue(owner) * IN_MILLISECONDS;
        int32 lowHealthPct = GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(owner);

        // "If you reach 50% health, you automatically gain Metamorphosis for 6 sec."
        if (owner->HealthBelowPct(aurEff->GetAmount()) && owner->HealthAbovePct(lowHealthPct - 1))
        {
            if (!owner->HasAura(SPELL_DH_AWAKEN_THE_DEMON_WITHIN_CD))
            {
                if (!metamorphosis || metamorphosis->GetDuration() < duration)
                {
                    owner->CastSpell(owner, SPELL_DH_AWAKEN_THE_DEMON_WITHIN_CD, true);
                    owner->CastCustomSpell(SPELL_DH_METAMORPHOSIS_TRANSFORM, SPELLVALUE_AURA_DURATION, duration, owner, true);
                }
            }
        }
        // "In addition, the duration of Metamorphosis will be refreshed up to a maximum of 6 sec while at or below 20% health."
        else if (owner->HealthBelowPct(lowHealthPct + 1))
        {
            if (metamorphosis && metamorphosis->GetDuration() < duration)
                metamorphosis->SetDuration(duration);
        }
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_havoc_honor_awaken_the_demon_within::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
    }
};

class spell_dh_legion_vengeance_honor_jagged_spikes : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_honor_jagged_spikes);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        // prevent multiple procs from a single cast
        if (GetUnitOwner()->GetSpellHistory()->HasCooldown(SPELL_DH_JAGGED_SPIKES_DMG))
            return false;

        return eventInfo.GetActor() && eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage();
    }

    void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
    {
        int32 damage = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), sSpellMgr->AssertSpellInfo(SPELL_DH_JAGGED_SPIKES)->GetEffect(EFFECT_0)->CalcValue(GetUnitOwner()));
        GetUnitOwner()->CastCustomSpell(SPELL_DH_JAGGED_SPIKES_DMG, SPELLVALUE_BASE_POINT0, damage, eventInfo.GetActor(), true);
        GetUnitOwner()->GetSpellHistory()->AddCooldown(SPELL_DH_JAGGED_SPIKES_DMG, 0, std::chrono::milliseconds(200));
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_honor_jagged_spikes::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_vengeance_honor_jagged_spikes::CheckProc);
    }
};

struct at_dh_vengeance_honor_demonic_trample : public AreaTriggerAI
{
public:
    at_dh_vengeance_honor_demonic_trample(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitEnter(Unit* unit) override
    {
        Unit* caster = at->GetCaster();
        if (!caster || caster == unit)
            return;

        if (!caster->IsValidAttackTarget(unit))
            return;

        caster->CastSpell(unit, SPELL_DH_DEMONIC_TRAMPLE_DMG, true);
        caster->CastSpell(unit, SPELL_DH_DEMONIC_TRAMPLE_STUN, true);
    }
};

class spell_dh_legion_honor_eye_of_leotheras : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_honor_eye_of_leotheras);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        if (!eventInfo.GetDamageInfo())
            return false;

        SpellInfo const* spellInfo = eventInfo.GetSpellInfo();
        return spellInfo && !spellInfo->IsPositive() && eventInfo.GetDamageInfo()->GetDamageType() != DOT;
    }

    void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
    {
        // "You keep a watchful eye over the enemy. Whenever they cast a harmful spell, they suffer 6% of their health in Shadow damage and refresh the duration of Eye of Leotheras."
        int32 damage = int32(GetUnitOwner()->CountPctFromCurHealth(sSpellMgr->AssertSpellInfo(SPELL_DH_EYE_OF_LEOTHERAS_DMG)->GetEffect(EFFECT_0)->CalcValue(GetCaster())));
        GetCaster()->CastCustomSpell(SPELL_DH_EYE_OF_LEOTHERAS_DMG, SPELLVALUE_BASE_POINT0, damage, GetUnitOwner(), true);
        RefreshDuration();
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_honor_eye_of_leotheras::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_honor_eye_of_leotheras::CheckProc);
    }
};

class spell_dh_legion_honor_reverse_magic : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_honor_reverse_magic);

    void HandleDispel(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();
        Unit* target = GetHitUnit();
        DispelChargesList dispelList;
        uint32 dispelMask = uint32((1 << DISPEL_NONE) | (1 << DISPEL_MAGIC));
        target->GetDispellableAuraList(caster, dispelMask, dispelList);

        for (auto const& itr : dispelList)
        {
            Aura* aura = itr.first;

            // "and sends them back to their original caster if possible."
            if (Unit* initialCaster = aura->GetCaster())
            {
                SpellInfo const* spellInfo = aura->GetSpellInfo();

                if (initialCaster->IsAlive() && target->IsWithinDist(initialCaster, spellInfo->GetMaxRange(true)) && !spellInfo->IsAffectingArea(DIFFICULTY_NONE))
                {
                    CustomSpellValues csv;

                    // copy effect values
                    for (AuraEffect const* aurEff : aura->GetAuraEffects())
                    {
                        if (!aurEff)
                            continue;

                        csv.AddSpellMod(SpellValueMod(SPELLVALUE_BASE_POINT0 + aurEff->GetEffIndex()), aurEff->GetAmount());
                    }

                    // and duration
                    csv.AddSpellMod(SPELLVALUE_AURA_DURATION, aura->GetDuration());
                    target->CastCustomSpell(aura->GetId(), csv, initialCaster, TRIGGERED_FULL_MASK);
                }
            }

            // "Dispels all harmful magical effects from yourself and all nearby allies within 10 yards"
            target->RemoveAurasDueToSpellByDispel(aura->GetId(), GetSpellInfo()->Id, aura->GetCasterGUID(), caster, itr.second);
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_honor_reverse_magic::HandleDispel, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

class spell_dh_legion_vengeance_frailty : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_frailty);

    bool CheckProc(ProcEventInfo& eventInfo)
    {
        return eventInfo.GetDamageInfo() && eventInfo.GetDamageInfo()->GetDamage() && eventInfo.GetActor() && eventInfo.GetActor()->GetGUID() == GetCasterGUID();
    }

    void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
    {
        int32 heal = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount());
        aurEff->GetCaster()->CastCustomSpell(SPELL_DH_SPIRIT_BOMB_HEAL, SPELLVALUE_BASE_POINT0, heal, aurEff->GetCaster(), true);
    }

    void Register() override
    {
        OnEffectProc += AuraEffectProcFn(spell_dh_legion_vengeance_frailty::HandleEffectProc, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
        DoCheckProc += AuraCheckProcFn(spell_dh_legion_vengeance_frailty::CheckProc);
    }
};

struct at_dh_havoc_honor_mana_rift : public AreaTriggerAI
{
public:
    at_dh_havoc_honor_mana_rift(AreaTrigger* areaTrigger) : AreaTriggerAI(areaTrigger) { }

    void OnUnitExit(Unit* unit) override
    {
        if (!at->IsRemoved())
            return;

        // execute when areatrigger is despawning

        Unit* caster = at->GetCaster();
        if (!caster)
            return;

        SpellInfo const* manaRift = sSpellMgr->AssertSpellInfo(SPELL_DH_MANA_RIFT_DMG_POWER_BURN);

        if (!unit->IsHostileTo(caster) || !caster->IsValidAttackTarget(unit))
            return;

        CustomSpellValues csv;
        csv.AddSpellMod(SpellValueMod(SPELLVALUE_BASE_POINT0), int32(unit->CountPctFromMaxHealth(manaRift->GetEffect(EFFECT_0)->CalcValue())));
        csv.AddSpellMod(SpellValueMod(SPELLVALUE_BASE_POINT1), unit->CountPctFromMaxPower(POWER_MANA, manaRift->GetEffect(EFFECT_1)->CalcValue()));

        caster->CastCustomSpell(SPELL_DH_MANA_RIFT_DMG_POWER_BURN, csv, unit, TRIGGERED_FULL_MASK);
    }
};

class spell_dh_legion_vengeance_artifact_soul_carver : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_vengeance_artifact_soul_carver);

    void HandleLaunch(SpellEffIndex /*effIndex*/)
    {
        SpellCastTargets targets;
        targets.SetUnitTarget(GetCaster());

        // "immediately shatters 2 Lesser Soul Fragments from the target"
        for (int8 i = 0; i < GetEffectInfo(EFFECT_3)->CalcValue(); ++i)
            GetHitUnit()->CastSpell(targets, roll_chance_i(50) ? SPELL_DH_SHATTER_SOUL : SPELL_DH_SHATTER_SOUL_1, true, nullptr, GetSpellInfo(), GetCaster()->GetGUID());
    }

    void Register() override
    {
        OnEffectLaunchTarget += SpellEffectFn(spell_dh_legion_vengeance_artifact_soul_carver::HandleLaunch, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
    }
};

class spell_dh_legion_vengeance_artifact_soul_carver_AuraScript : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_vengeance_artifact_soul_carver_AuraScript);

    void HandlePeriodic(AuraEffect const* /*aurEff*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        SpellCastTargets targets;
        targets.SetUnitTarget(caster);

        // "shatters 1 additional Lesser Soul Fragment every 1 sec."
        GetTarget()->CastSpell(targets, roll_chance_i(50) ? SPELL_DH_SHATTER_SOUL : SPELL_DH_SHATTER_SOUL_1, true, nullptr, GetSpellInfo(), caster->GetGUID());
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_dh_legion_vengeance_artifact_soul_carver_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
    }
};

class spell_dh_legion_spectral_sight : public AuraScript
{
    PrepareAuraScript(spell_dh_legion_spectral_sight);

    bool Validate(SpellInfo const* spellInfo) override
    {
        if (!spellInfo->GetEffect(EFFECT_0) || !spellInfo->GetEffect(EFFECT_1))
            return false;

        if (spellInfo->GetEffect(EFFECT_0)->TriggerSpell == 0 || spellInfo->GetEffect(EFFECT_1)->TriggerSpell == 0)
            return false;

        return true;
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        GetTarget()->RemoveAura(GetSpellInfo()->GetEffect(EFFECT_0)->TriggerSpell);
        GetTarget()->RemoveAura(GetSpellInfo()->GetEffect(EFFECT_1)->TriggerSpell);
    }

    void Register() override
    {
        OnEffectRemove += AuraEffectRemoveFn(spell_dh_legion_spectral_sight::OnRemove, EFFECT_2, SPELL_AURA_SCREEN_EFFECT, AURA_EFFECT_HANDLE_REAL);
    }
};

class spell_dh_legion_havoc_honor_fel_lance : public SpellScript
{
    PrepareSpellScript(spell_dh_legion_havoc_honor_fel_lance);

    void HandleDamage(SpellEffIndex /*effIndex*/)
    {
        // "Deals $m2% of the target's total health in Chaos damage."
        SetHitDamage(int32(GetHitUnit()->CountPctFromMaxHealth(GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue(GetCaster()))));
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_dh_legion_havoc_honor_fel_lance::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
    }
};

void AddSC_legion_demon_hunter_spell_scripts()
{
    // Havoc

    /* Spells & Auras */

    RegisterSpellAndAuraScriptPair(spell_dh_legion_havoc_chaos_strike, spell_dh_legion_havoc_chaos_strike_AuraScript);
    RegisterSpellAndAuraScriptPair(spell_dh_legion_havoc_fel_barrage, spell_dh_legion_havoc_fel_barrage_AuraScript);
    RegisterSpellAndAuraScriptPair(spell_dh_legion_havoc_fel_rush, spell_dh_legion_havoc_fel_rush_AuraScript);

    /* Auras */

    RegisterAuraScript(spell_dh_legion_havoc_artifact_rage_of_the_illidari);
    RegisterAuraScript(spell_dh_legion_havoc_artifact_anguish_of_the_deceiver);
    RegisterAuraScript(spell_dh_legion_havoc_artifact_deceivers_fury);
    RegisterAuraScript(spell_dh_legion_havoc_artifact_feast_on_the_souls);
    RegisterAuraScript(spell_dh_legion_havoc_artifact_inner_demons);
    RegisterAuraScript(spell_dh_legion_havoc_bloodlet);
    RegisterAuraScript(spell_dh_legion_havoc_chaos_blades);
    RegisterAuraScript(spell_dh_legion_havoc_chaos_cleave);
    RegisterAuraScript(spell_dh_legion_havoc_chaos_nova);
    RegisterAuraScript(spell_dh_legion_havoc_darkness);
    RegisterAuraScript(spell_dh_legion_havoc_demonic_appetite);
    RegisterAuraScript(spell_dh_legion_havoc_desperate_instincts);
    RegisterAuraScript(spell_dh_legion_havoc_eye_beam);
    RegisterAuraScript(spell_dh_legion_havoc_fel_mastery);
    RegisterAuraScript(spell_dh_legion_havoc_honor_awaken_the_demon_within);
    RegisterAuraScript(spell_dh_legion_havoc_honor_glimpse);
    RegisterAuraScript(spell_dh_legion_havoc_master_of_the_glaives);
    RegisterAuraScript(spell_dh_legion_havoc_momentum);
    RegisterAuraScript(spell_dh_legion_havoc_nemesis);
    RegisterAuraScript(spell_dh_legion_havoc_prepared);
    RegisterAuraScript(spell_dh_legion_havoc_shattered_souls);

    /* AreaTrigger */

    RegisterAreaTriggerAI(at_dh_havoc_artifact_fury_of_the_illidari);
    RegisterAreaTriggerAI(at_dh_havoc_artifact_inner_demons);
    RegisterAreaTriggerAI(at_dh_havoc_darkness);
    RegisterAreaTriggerAI(at_dh_havoc_honor_mana_rift);
    RegisterAreaTriggerAI(at_dh_havoc_shattered_souls);

    /* Spells */

    RegisterSpellScript(spell_dh_legion_havoc_artifact_rage_of_the_illidari_damage);
    RegisterSpellScript(spell_dh_legion_havoc_blur);
    RegisterSpellScript(spell_dh_legion_havoc_demonic_appetite_energize);
    RegisterSpellScript(spell_dh_legion_havoc_honor_mana_break);
    RegisterSpellScript(spell_dh_legion_havoc_metamorphosis);
    RegisterSpellScript(spell_dh_legion_havoc_honor_fel_lance);
    RegisterSpellScript(spell_dh_legion_havoc_shattered_souls_missile);

    // Vengeance

    /* Spells & Auras */

    RegisterSpellAndAuraScriptPair(spell_dh_legion_vengeance_fiery_brand, spell_dh_legion_vengeance_fiery_brand_AuraScript);
    RegisterSpellAndAuraScriptPair(spell_dh_legion_vengeance_artifact_soul_carver, spell_dh_legion_vengeance_artifact_soul_carver_AuraScript);

    /* Auras */

    RegisterAuraScript(spell_dh_legion_vengeance_artifact_charred_warblades);
    RegisterAuraScript(spell_dh_legion_vengeance_artifact_defensive_spikes);
    RegisterAuraScript(spell_dh_legion_vengeance_artifact_fiery_demise);
    RegisterAuraScript(spell_dh_legion_vengeance_artifact_fueled_by_pain);
    RegisterAuraScript(spell_dh_legion_vengeance_demon_spikes_aura);
    RegisterAuraScript(spell_dh_legion_vengeance_empower_wards);
    RegisterAuraScript(spell_dh_legion_vengeance_fel_devastation);
    RegisterAuraScript(spell_dh_legion_vengeance_frailty);
    RegisterAuraScript(spell_dh_legion_vengeance_honor_jagged_spikes);
    RegisterAuraScript(spell_dh_legion_vengeance_last_resort);
    RegisterAuraScript(spell_dh_legion_vengeance_razor_spikes);
    RegisterAuraScript(spell_dh_legion_vengeance_shattered_souls);
    RegisterAuraScript(spell_dh_legion_vengeance_shear);
    RegisterAuraScript(spell_dh_legion_vengeance_sigil_of_flame);
    RegisterAuraScript(spell_dh_legion_vengeance_soul_barrier);

    /* AreaTrigger */

    RegisterAreaTriggerAI(at_dh_vengeance_honor_demonic_trample);
    RegisterAreaTriggerAI(at_dh_vengeance_sigil_of_flame);
    RegisterAreaTriggerAI(at_dh_vengeance_sigil_of_misery);
    RegisterAreaTriggerAI(at_dh_vengeance_sigil_of_silence);
    RegisterAreaTriggerAI(at_dh_vengeance_soul_fragment);

    /* Spells */

    RegisterSpellScript(spell_dh_legion_vengeance_burning_alive);
    RegisterSpellScript(spell_dh_legion_vengeance_consume_soul);
    RegisterSpellScript(spell_dh_legion_vengeance_demon_spikes);
    RegisterSpellScript(spell_dh_legion_vengeance_demonic_infusion);
    RegisterSpellScript(spell_dh_legion_vengeance_fracture);
    RegisterSpellScript(spell_dh_legion_vengeance_immolation_aura);
    RegisterSpellScript(spell_dh_legion_vengeance_infernal_strike);
    RegisterSpellScript(spell_dh_legion_vengeance_shatter_soul);
    RegisterSpellScript(spell_dh_legion_vengeance_soul_cleave);
    RegisterSpellScript(spell_dh_legion_vengeance_spirit_bomb);

    // Vengeance & Havoc

    RegisterSpellScript(spell_dh_legion_blade_dance);
    RegisterSpellScript(spell_dh_legion_blade_dance_damage);
    new spell_dh_legion_first_blood();
    RegisterAuraScript(spell_dh_legion_honor_eye_of_leotheras);
    RegisterAuraScript(spell_dh_legion_honor_solitude);
    RegisterAuraScript(spell_dh_legion_spectral_sight);

    RegisterSpellScript(spell_dh_legion_honor_reverse_magic);

    RegisterSpellScript(spell_dh_legion_felblade);
    RegisterAuraScript(spell_dh_legion_felblade_aura);
    RegisterSpellScript(spell_dh_legion_felblade_charge);
    RegisterSpellAndAuraScriptPair(spell_dh_legion_glide, spell_dh_legion_glide_AuraScript);
}
