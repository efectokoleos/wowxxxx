/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Scripts for spells with SPELLFAMILY_GENERIC spells used by items.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_item_".
 */

#include "DB2Stores.h"
#include "ScriptMgr.h"
#include "SpellAuraEffects.h"
#include "SpellScript.h"

enum SelfieSpells
{
    SPELL_ITEM_SELFIE_CAMERA                            = 182575,
    SPELL_ITEM_SELFIE_CAMERA_SWITCH_TO_CAMERA_VIEW      = 181765,
    SPELL_ITEM_SELFIE_CAMERA_MKII_SWITCH_TO_CAMERA_VIEW = 181884,
    SELFIE_SPELL_VISUAL_KIT                             = 54154
};

// 182575 - S.E.L.F.I.E. Camera
// 182562 - S.E.L.F.I.E. Camera MkII
class spell_item_selfie_camera : public SpellScriptLoader
{
    public:
        spell_item_selfie_camera() : SpellScriptLoader("spell_item_selfie_camera") { }

        class spell_item_selfie_camera_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_item_selfie_camera_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                return ValidateSpellInfo(
                {
                    SPELL_ITEM_SELFIE_CAMERA_SWITCH_TO_CAMERA_VIEW,
                    SPELL_ITEM_SELFIE_CAMERA_MKII_SWITCH_TO_CAMERA_VIEW
                });
            }

            void OnHitTarget(SpellEffIndex /*effIndex*/)
            {
                uint32 triggerId = GetSpellInfo()->Id == SPELL_ITEM_SELFIE_CAMERA ? SPELL_ITEM_SELFIE_CAMERA_SWITCH_TO_CAMERA_VIEW : SPELL_ITEM_SELFIE_CAMERA_MKII_SWITCH_TO_CAMERA_VIEW;
                GetHitUnit()->CastSpell(GetHitUnit(), triggerId, true);

                // TODO: handle Photo B.O.M.B. (182515, basepoints of this effect) when creature 91977 will be in DB
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_item_selfie_camera_SpellScript::OnHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_item_selfie_camera_SpellScript();
        }
};

// 181765 - S.E.L.F.I.E. Camera
// 181884 - S.E.L.F.I.E. Camera MkII (camera view)
class spell_item_selfie_camera_view : public SpellScriptLoader
{
    public:
        spell_item_selfie_camera_view() : SpellScriptLoader("spell_item_selfie_camera_view") { }

        class spell_item_selfie_camera_view_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_item_selfie_camera_view_AuraScript);

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->SendPlaySpellVisualKit(SELFIE_SPELL_VISUAL_KIT, 2, 0);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->SendCancelSpellVisualKit(SELFIE_SPELL_VISUAL_KIT);
            }

            void OnRemoveOverrideSpells(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (OverrideSpellDataEntry const* overrideSpells = sOverrideSpellDataStore.LookupEntry(aurEff->GetSpellEffectInfo()->MiscValue))
                    for (uint8 i = 0; i < MAX_OVERRIDE_SPELL; ++i)
                        if (uint32 spellId = overrideSpells->SpellID[i])
                            GetTarget()->RemoveAurasDueToSpell(spellId);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_item_selfie_camera_view_AuraScript::OnApply, EFFECT_0, SPELL_AURA_SELFIE_CAMERA, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_item_selfie_camera_view_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_SELFIE_CAMERA, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_item_selfie_camera_view_AuraScript::OnRemoveOverrideSpells, EFFECT_1, SPELL_AURA_OVERRIDE_SPELLS, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_item_selfie_camera_view_AuraScript();
        }
};

void AddSC_legion_item_spell_scripts()
{
    new spell_item_selfie_camera();
    new spell_item_selfie_camera_view();
}
