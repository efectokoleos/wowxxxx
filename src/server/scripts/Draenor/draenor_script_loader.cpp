/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

// Auchindoun
void AddSC_instance_auchindoun();

// Blackrock Foundry
void AddSC_instance_blackrock_foundry();

// Bloodmaul Slag Mines
void AddSC_instance_bloodmaul_slag_mines();

// Hellfire Citadel
void AddSC_instance_hellfire_citadel();

// Highmaul
void AddSC_instance_highmaul();

// Grimrail Depot
void AddSC_instance_grimrail_depot();

// Iron Docks
void AddSC_instance_iron_docks();

// Shadowmoon Burial grounds
void AddSC_instance_shadowmoon_burial_grounds();

// Skyreach
void AddSC_instance_skyreach();

// The Everbloom
void AddSC_instance_the_everbloom();

// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddDraenorScripts()
{
    // Auchindoun
    AddSC_instance_auchindoun();

    // Blackrock Foundry
    AddSC_instance_blackrock_foundry();

    // Bloodmaul Slag Mines
    AddSC_instance_bloodmaul_slag_mines();

    // Hellfire Citadel
    AddSC_instance_hellfire_citadel();

    // Highmaul
    AddSC_instance_highmaul();

    // Grimrail Depot
    AddSC_instance_grimrail_depot();

    // Iron Docks
    AddSC_instance_iron_docks();

    // Shadowmoon Burial grounds
    AddSC_instance_shadowmoon_burial_grounds();

    // Skyreach
    AddSC_instance_skyreach();

    // The Everbloom
    AddSC_instance_the_everbloom();
}
