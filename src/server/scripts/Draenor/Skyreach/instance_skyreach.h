/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef SKYREACH_H_
#define SKYREACH_H_

#include "CreatureAIImpl.h"

namespace SkyReach
{
    static char constexpr const ScriptName[]    = "instance_skyreach";
    static char constexpr const DataHeader[]    = "SR";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1209;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum SRDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_RANJIT                     = 0,
    DATA_ARAKNATH                   = 1,
    DATA_RUKHRAN                    = 2,
    DATA_HIGH_SAGE_VIRYX            = 3,

    // Additional data
};

enum SRCreatureIds
{
};

enum SRGameObjectIds
{
};

#endif // SKYREACH_H_
