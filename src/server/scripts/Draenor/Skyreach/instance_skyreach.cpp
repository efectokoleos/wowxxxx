/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "Map.h"
#include "ScriptMgr.h"
#include "instance_skyreach.h"

class instance_skyreach : public InstanceMapScript
{
    public:                                                      
        instance_skyreach() : InstanceMapScript(SkyReach::ScriptName, SkyReach::MapId) { }

        struct instance_skyreach_InstanceMapScript : public InstanceScript
        {
            instance_skyreach_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(SkyReach::DataHeader);
                SetBossNumber(SkyReach::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_skyreach_InstanceMapScript(map);
        }
};

void AddSC_instance_skyreach()
{
    new instance_skyreach();
}
