/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef IRON_DOCKS_H_
#define IRON_DOCKS_H_

#include "CreatureAIImpl.h"

namespace IronDocks
{
    static char constexpr const ScriptName[]    = "instance_iron_docks";
    static char constexpr const DataHeader[]    = "ID";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1195;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum IDDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_GRIMRAIL_ENFORCERS         = 0,
    DATA_FLESHRENDER_NOKGAR         = 1,
    DATA_SKULLOC                    = 2,
    DATA_OSHIR                      = 3,

    // Additional data
};

enum IDCreatureIds
{
};

enum IDGameObjectIds
{
};

#endif // IRON_DOCKS_H_
