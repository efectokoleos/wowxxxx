/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef AUCHINDOUN_H_
#define AUCHINDOUN_H_

#include "CreatureAIImpl.h"

namespace Auchindoun
{
    static char constexpr const ScriptName[]    = "instance_auchindoun";
    static char constexpr const DataHeader[]    = "AUCH";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1182;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum AUCHDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_AZZAKEL                    = 0,
    DATA_SOULBINDER_NYAMI           = 1,
    DATA_VIGILANT_KAATHAR           = 2,
    DATA_TERON_GOR                  = 3,

    // Additional data
};

enum AUCHCreatureIds
{
};

enum AUCHGameObjectIds
{
};

#endif // AUCHINDOUN_H_
