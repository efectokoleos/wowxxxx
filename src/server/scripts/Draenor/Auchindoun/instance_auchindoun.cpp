/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "Map.h"
#include "ScriptMgr.h"
#include "auchindoun.h"

class instance_auchindoun : public InstanceMapScript
{
    public:
        instance_auchindoun() : InstanceMapScript(Auchindoun::ScriptName, Auchindoun::MapId) { }

        struct instance_auchindoun_InstanceMapScript : public InstanceScript
        {
            instance_auchindoun_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(Auchindoun::DataHeader);
                SetBossNumber(Auchindoun::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_auchindoun_InstanceMapScript(map);
        }
};

void AddSC_instance_auchindoun()
{
    new instance_auchindoun();
}
