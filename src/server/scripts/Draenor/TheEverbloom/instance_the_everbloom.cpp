/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "Map.h"
#include "ScriptMgr.h"
#include "the_everbloom.h"

class instance_the_everbloom : public InstanceMapScript
{
    public:
        instance_the_everbloom() : InstanceMapScript(TheEverbloom::ScriptName, TheEverbloom::MapId) { }

        struct instance_the_everbloom_InstanceMapScript : public InstanceScript
        {
            instance_the_everbloom_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(TheEverbloom::DataHeader);
                SetBossNumber(TheEverbloom::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_the_everbloom_InstanceMapScript(map);
        }
};

void AddSC_instance_the_everbloom()
{
    new instance_the_everbloom();
}
