/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef THE_EVERBLOOM_H_
#define THE_EVERBLOOM_H_

#include "CreatureAIImpl.h"

namespace TheEverbloom
{
    static char constexpr const ScriptName[]    = "instance_the_everbloom";
    static char constexpr const DataHeader[]    = "TEB";
    uint32 const EncounterCount                 = 5;
    uint32 const MapId                          = 1279;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum TEBDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_WITHERBARK                 = 0,
    DATA_ARCHMAGE_SOL               = 1,
    DATA_XERI_TAC                   = 2,
    DATA_YALNU                      = 3,
    DATA_ANCIENT_PROTECTORS         = 4,

    // Additional data
};

enum TEBCreatureIds
{
};

enum TEBGameObjectIds
{
};

#endif // THE_EVERBLOOM_H_
