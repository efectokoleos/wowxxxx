/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef HIGHMAUL_H_
#define HIGHMAUL_H_

#include "CreatureAIImpl.h"

namespace Highmaul
{
    static char constexpr const ScriptName[]    = "instance_highmaul";
    static char constexpr const DataHeader[]    = "HM";
    uint32 const EncounterCount                 = 7;
    uint32 const MapId                          = 1228;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum HMDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_IMPERATOR_MAR_GOK          = 0,
    DATA_THE_BUTCHER                = 1,
    DATA_TWIN_OGRON                 = 2,
    DATA_BRACKENSPORE               = 3,
    DATA_KARGATH_BLADEFIST          = 4,
    DATA_TECTUS                     = 5,
    DATA_KO_RAGH                    = 6,

    // Additional data
};

enum HMCreatureIds
{
};

enum HMGameObjectIds
{
};

#endif // HIGHMAUL_H_
