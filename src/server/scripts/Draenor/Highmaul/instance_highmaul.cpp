/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "Map.h"
#include "ScriptMgr.h"
#include "highmaul.h"

class instance_highmaul : public InstanceMapScript
{
    public:                                                      
        instance_highmaul() : InstanceMapScript(Highmaul::ScriptName, Highmaul::MapId) { }

        struct instance_highmaul_InstanceMapScript : public InstanceScript
        {
            instance_highmaul_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(Highmaul::DataHeader);
                SetBossNumber(Highmaul::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_highmaul_InstanceMapScript(map);
        }
};

void AddSC_instance_highmaul()
{
    new instance_highmaul();
}
