/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef BLOODMAUL_SLAG_MINES_H_
#define BLOODMAUL_SLAG_MINES_H_

#include "CreatureAIImpl.h"

namespace BloodmaulSlagMines
{
    static char constexpr const ScriptName[]    = "instance_bloodmaul_slag_mines";
    static char constexpr const DataHeader[]    = "BSM";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1175;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum BSMDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_ROLTALL                    = 0,
    DATA_SLAVE_WATCHER_CRUSHTO      = 1,
    DATA_GUG_ROKK                   = 2,
    DATA_MAGMOLATUS                 = 3,

    // Additional data
};

enum BSMCreatureIds
{
};

enum BSMGameObjectIds
{
};

#endif // BLOODMAUL_SLAG_MINES_H_
