/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "Map.h"
#include "InstanceScript.h"
#include "ScriptMgr.h"
#include "bloodmaul_slag_mines.h"

class instance_bloodmaul_slag_mines : public InstanceMapScript
{
    public:
        instance_bloodmaul_slag_mines() : InstanceMapScript(BloodmaulSlagMines::ScriptName, BloodmaulSlagMines::MapId) { }

        struct instance_bloodmaul_slag_mines_InstanceMapScript : public InstanceScript
        {
            instance_bloodmaul_slag_mines_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(BloodmaulSlagMines::DataHeader);
                SetBossNumber(BloodmaulSlagMines::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_bloodmaul_slag_mines_InstanceMapScript(map);
        }
};

void AddSC_instance_bloodmaul_slag_mines()
{
    new instance_bloodmaul_slag_mines();
}
