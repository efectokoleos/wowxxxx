/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "Map.h"
#include "ScriptMgr.h"
#include "shadowmoon_burial_grounds.h"

class instance_shadowmoon_burial_grounds : public InstanceMapScript
{
    public:                                                      
        instance_shadowmoon_burial_grounds() : InstanceMapScript(ShadowmoonBurialGrounds::ScriptName, ShadowmoonBurialGrounds::MapId) { }

        struct instance_shadowmoon_burial_grounds_InstanceMapScript : public InstanceScript
        {
            instance_shadowmoon_burial_grounds_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(ShadowmoonBurialGrounds::DataHeader);
                SetBossNumber(ShadowmoonBurialGrounds::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_shadowmoon_burial_grounds_InstanceMapScript(map);
        }
};

void AddSC_instance_shadowmoon_burial_grounds()
{
    new instance_shadowmoon_burial_grounds();
}
