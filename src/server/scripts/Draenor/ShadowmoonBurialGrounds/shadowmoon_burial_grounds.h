/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef SHADOWMOON_BURIAL_GROUNDS_H_
#define SHADOWMOON_BURIAL_GROUNDS_H_

#include "CreatureAIImpl.h"

namespace ShadowmoonBurialGrounds
{
    static char constexpr const ScriptName[]    = "instance_shadowmoon_burial_grounds";
    static char constexpr const DataHeader[]    = "SBG";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1176;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum SBGDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_SADANA_BLOODFURY           = 0,
    DATA_BONEMAW                    = 1,
    DATA_NER_ZHUL                   = 2,
    DATA_NHALLISH                   = 3,

    // Additional data
};

enum SBGCreatureIds
{
};

enum SBGGameObjectIds
{
};

#endif // SHADOWMOON_BURIAL_GROUNDS_H_
