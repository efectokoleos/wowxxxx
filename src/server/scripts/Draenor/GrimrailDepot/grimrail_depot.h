/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef GRIMRAIL_DEPOT_H_
#define GRIMRAIL_DEPOT_H_

#include "CreatureAIImpl.h"
 
namespace GrimrailDeport
{
    static char constexpr const ScriptName[]    = "instance_grimrail_depot";
    static char constexpr const DataHeader[]    = "GD";
    uint32 const EncounterCount                 = 3;
    uint32 const MapId                          = 1208;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum GDDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_ROCKETSPARK_AND_BORKA      = 0,
    DATA_NITROGG_THUNDERTOWER       = 1,
    DATA_SKYLORD_TOVRA              = 2,

    // Additional data
};

enum GDCreatureIds
{
};

enum GDGameObjectIds
{
};

#endif // GRIMRAIL_DEPOT_H_
