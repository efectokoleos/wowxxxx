/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef HELLFIRE_CITADEL_H_
#define HELLFIRE_CITADEL_H_

#include "CreatureAIImpl.h"

namespace HellfireCitadel
{
    static char constexpr const ScriptName[]    = "instance_hellfire_citadel";
    static char constexpr const DataHeader[]    = "HFC";
    uint32 const EncounterCount                 = 13;
    uint32 const MapId                          = 1448;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}


enum HFCDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_FEL_LORD_ZAKUUN            = 0,
    DATA_HELLFIRE_ASSAULT           = 1,
    DATA_GOREFIEND                  = 2,
    DATA_TYRANT_VELHARI             = 3,
    DATA_IRON_REAVER                = 4,
    DATA_KILROGG_DEADEYE            = 5,
    DATA_KORMROK                    = 6,
    DATA_SHADOW_LORD_ISKAR          = 7,
    DATA_SOCRETHAR_THE_ETERNAL      = 8,
    DATA_MANNOROTH                  = 9,
    DATA_HELLFIRE_HIGH_COUNCIL      = 10,
    DATA_ARCHIMONDE                 = 11,
    DATA_XHUL_HORAC                 = 12,

    // Additional data
};

enum HFCCreatureIds
{
};

enum HFCGameObjectIds
{
};

#endif // HELLFIRE_CITADEL_H_
