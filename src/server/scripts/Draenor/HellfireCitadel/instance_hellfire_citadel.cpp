/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "hellfire_citadel.h"

class instance_hellfire_citadel : public InstanceMapScript
{
    public:                                                      
        instance_hellfire_citadel() : InstanceMapScript(HellfireCitadel::ScriptName, HellfireCitadel::MapId) { }

        struct instance_hellfire_citadel_InstanceMapScript : public InstanceScript
        {
            instance_hellfire_citadel_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(HellfireCitadel::DataHeader);
                SetBossNumber(HellfireCitadel::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_hellfire_citadel_InstanceMapScript(map);
        }
};

void AddSC_instance_hellfire_citadel()
{
    new instance_hellfire_citadel();
}
