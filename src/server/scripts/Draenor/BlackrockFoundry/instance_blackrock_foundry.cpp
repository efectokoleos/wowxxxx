/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "blackrock_foundry.h"

class instance_blackrock_foundry : public InstanceMapScript
{
    public:
        instance_blackrock_foundry() : InstanceMapScript(BlackrockFoundry::ScriptName, BlackrockFoundry::MapId) { }

        struct instance_blackrock_foundry_InstanceMapScript : public InstanceScript
        {
            instance_blackrock_foundry_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(BlackrockFoundry::DataHeader);
                SetBossNumber(BlackrockFoundry::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_blackrock_foundry_InstanceMapScript(map);
        }
};

void AddSC_instance_blackrock_foundry()
{
    new instance_blackrock_foundry();
}
