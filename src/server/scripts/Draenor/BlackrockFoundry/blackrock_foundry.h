/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef BLACKROCK_FOUNDRY_H_
#define BLACKROCK_FOUNDRY_H_

#include "CreatureAIImpl.h"

namespace BlackrockFoundry
{
    static char constexpr const ScriptName[]    = "instance_blackrock_foundry";
    static char constexpr const DataHeader[]    = "BRF";
    uint32 const EncounterCount                 = 10;
    uint32 const MapId                          = 1205;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum BRFDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_FLAMEBENDER_KA_GRAZ        = 0,
    DATA_BLAST_FURNACE              = 1,
    DATA_GRUUL                      = 2,
    DATA_OPERATOR_THOGAR            = 3,
    DATA_HANS_GAR_AND_FRAN_ZOK      = 4,
    DATA_BEASTLORD_DARMAC           = 5,
    DATA_THE_IRON_MAIDENS           = 6,
    DATA_OREGORGER_THE_DEVOURER     = 7,
    DATA_BLACKHAND                  = 8,
    DATA_KROMOG                     = 9,

    // Additional data
};

enum BDFCreatureIds
{
};

enum BDFGameObjectIds
{
};

#endif // BLACKROCK_FOUNDRY_H_
