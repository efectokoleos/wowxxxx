/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef STORMSTOUT_BREWERY_H_
#define STORMSTOUT_BREWERY_H_

#include "CreatureAIImpl.h"

namespace StormstoutBrewery
{
    static char constexpr const ScriptName[]    = "instance_stormstout_brewery";
    static char constexpr const DataHeader[]    = "SB";
    uint32 const EncounterCount                 = 3;
    uint32 const MapId                          = 961;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum SBDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_OOK_OOK                    = 0,
    DATA_HOPTALLUS                  = 1,
    DATA_YAN_ZHU_THE_UNCASKED       = 2,

    // Additional data
};

enum SBCreatureIds
{
};

enum SBGameObjectIds
{
};

#endif // STORMSTOUT_BREWERY_H_
