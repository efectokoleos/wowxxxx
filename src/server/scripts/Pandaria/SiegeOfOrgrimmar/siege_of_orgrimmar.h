/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef SIEGE_OF_ORGRIMMAR_H_
#define SIEGE_OF_ORGRIMMAR_H_

#include "CreatureAIImpl.h"

namespace SiegeOfOrgrimmar
{
    static char constexpr const ScriptName[]    = "instance_siege_of_orgrimmar";
    static char constexpr const DataHeader[]    = "SOO";
    uint32 const EncounterCount                 = 14;
    uint32 const MapId                          = 1136;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum SOODataTypes
{
    // Encounter States/Boss GUIDs
    DATA_PARAGONS_OF_THE_KLAXXI     = 0,
    DATA_SPOILS_OF_PANDARIA         = 1,
    DATA_MALKOROK                   = 2,
    DATA_FALLEN_PROTECTORS          = 3,
    DATA_THOK_THE_BLOODTHIRSTY      = 4,
    DATA_IRON_JUGGERNAUT            = 5,
    DATA_SIEGECRAFTER_BLACKFUSE     = 6,
    DATA_IMMERSEUS                  = 7,
    DATA_GENERAL_NAZGRIM            = 8,
    DATA_SHA_OF_PRIDE               = 9,
    DATA_KOR_KRON_DARK_SHAMAN       = 10,
    DATA_GALAKRAS                   = 11,
    DATA_GARROSH_HELLSCREAM         = 12,
    DATA_NORUSHEN                   = 13,

    // Additional data
};

enum SOOCreatureIds
{
};

enum SOOGameObjectIds
{
};

#endif // SIEGE_OF_ORGRIMMAR_H_
