/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef THRONE_OF_THUNDER_H
#define THRONE_OF_THUNDER_H

#include "CreatureAIImpl.h"

namespace ThroneOfThunder
{
    static char constexpr const ScriptName[]    = "instance_throne_of_thunder";
    static char constexpr const DataHeader[]    = "TOT";
    uint32 const EncounterCount                 = 13;
    uint32 const MapId                          = 1098;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum TOTDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_IRON_QON                   = 0,
    DATA_TWIN_CONSORTS              = 1,
    DATA_TORTOS                     = 2,
    DATA_COUNCIL_OF_ELDERS          = 3,
    DATA_DURUMU_THE_FORGOTTEN       = 4,
    DATA_JI_KUN                     = 5,
    DATA_PRIMORDIUS                 = 6,
    DATA_HORRIDON                   = 7,
    DATA_DARK_ANIMUS                = 8,
    DATA_JIN_ROKH_THE_BREAKER       = 9,
    DATA_MEGAERA                    = 10,
    DATA_LEI_SHEN                   = 11,
    DATA_RA_DEN                     = 12,

    // Additional data
};

enum TOTCreatureIds
{
};

enum TOTGameObjectIds
{
};

#endif // THRONE_OF_THUNDER_H
