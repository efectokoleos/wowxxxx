/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef GATE_SETTING_SUN_H_
#define GATE_SETTING_SUN_H_

#include "CreatureAIImpl.h"

namespace GateOfTheSettingSun
{
    static char constexpr const ScriptName[]    = "instance_gate_of_the_setting_sun";
    static char constexpr const DataHeader[]    = "GSS";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 962;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum GSSDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_SABOTEUR_KIP_TILAK         = 0,
    DATA_STRIKER_GA_DOK             = 1,
    DATA_COMMANDER_RI_MOK           = 2,
    DATA_RAIGONN                    = 3,

    // Additional data
};

enum GSSCreatureIds
{
};

enum GSSGameObjectIds
{
};

#endif // GATE_SETTING_SUN_H_
