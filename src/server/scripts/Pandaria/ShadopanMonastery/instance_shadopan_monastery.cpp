/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "ScriptMgr.h"
#include "Map.h"
#include "shadopan_monastery.h"

class instance_shadopan_monastery : public InstanceMapScript
{
    public:
        instance_shadopan_monastery() : InstanceMapScript(ShadoPanMonastery::ScriptName, ShadoPanMonastery::MapId) { }

        struct instance_shadopan_monastery_InstanceMapScript : public InstanceScript
        {
            instance_shadopan_monastery_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(ShadoPanMonastery::DataHeader);
                SetBossNumber(ShadoPanMonastery::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_shadopan_monastery_InstanceMapScript(map);
        }
};

void AddSC_instance_shadopan_monastery()
{
    new instance_shadopan_monastery();
}
