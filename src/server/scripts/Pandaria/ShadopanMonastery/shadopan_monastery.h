/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef SHADO_PAN_MONASTERY_H_
#define SHADO_PAN_MONASTERY_H_

#include "CreatureAIImpl.h"

namespace ShadoPanMonastery
{
    static char constexpr const ScriptName[]    = "instance_shado_pan_monastery";
    static char constexpr const DataHeader[]    = "SPM";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 959;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum SPMDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_GU_CLOUDSTRIKE             = 0,
    DATA_MASTER_SNOWDRIFT           = 1,
    DATA_SHA_OF_VIOLENCE            = 2,
    DATA_TARAN_ZHU                  = 3,

    // Additional data
};

enum SPMCreatureIds
{
};

enum SPMGameObjectIds
{
};

#endif // SHADO_PAN_MONASTERY_H_
