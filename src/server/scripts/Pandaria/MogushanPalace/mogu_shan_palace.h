/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef MOGU_SHAN_PALACE_H_
#define MOGU_SHAN_PALACE_H_

#include "CreatureAIImpl.h"

namespace MoguShanPalace
{
    static char constexpr const ScriptName[]    = "instance_mogu_shan_palace";
    static char constexpr const DataHeader[]    = "MSP";
    uint32 const EncounterCount                 = 3;
    uint32 const MapId                          = 994;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum MSPDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_XIN_THE_WEAPONMASTER       = 0,
    DATA_TRIAL_OT_THE_KING          = 1,
    DATA_GEKKAN                     = 2,

    // Additional data
};

enum MSPCreatureIds
{
};

enum MSPGameObjectIds
{
};

#endif // MOGU_SHAN_PALACE_H_
