/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef SIEGE_OF_NIUZAO_H
#define SIEGE_OF_NIUZAO_H

#include "CreatureAIImpl.h"

namespace SiegeOfNiuzaoTemple
{
    static char constexpr const ScriptName[]    = "instance_siege_of_niuzao_temple";
    static char constexpr const DataHeader[]    = "SNT";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1011;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum SNTDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_GENERAL_PA_VALAK           = 0,
    DATA_WING_LEADER_NER_ONOK       = 1,
    DATA_VIZIER_JIN_BAK             = 2,
    DATA_COMMANDER_VO_JAK           = 3,

    // Additional data
};

enum SNTCreatureIds
{
};

enum SNTGameObjectIds
{
};

#endif // SIEGE_OF_NIUZAO_H
