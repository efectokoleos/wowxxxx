/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "ScriptMgr.h"
#include "Map.h"
#include "siege_of_niuzao_temple.h"

class instance_siege_of_niuzao_temple : public InstanceMapScript
{
    public:
        instance_siege_of_niuzao_temple() : InstanceMapScript(SiegeOfNiuzaoTemple::ScriptName, SiegeOfNiuzaoTemple::MapId) { }

        struct instance_siege_of_niuzao_temple_InstanceMapScript : public InstanceScript
        {
            instance_siege_of_niuzao_temple_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(SiegeOfNiuzaoTemple::DataHeader);
                SetBossNumber(SiegeOfNiuzaoTemple::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_siege_of_niuzao_temple_InstanceMapScript(map);
        }
};

void AddSC_instance_siege_of_niuzao_temple()
{
    new instance_siege_of_niuzao_temple();
}
