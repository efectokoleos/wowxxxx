/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptedCreature.h"
#include "ScriptMgr.h"

enum Spells
{
};

enum Creatures
{
    NPC_GALLEON         = 62346,
};

class boss_galleon : public CreatureScript
{
    public:
        boss_galleon() : CreatureScript("boss_galleon") { }

        struct boss_galleon_AI : public WorldBossAI
        {
            boss_galleon_AI(Creature* creature) : WorldBossAI(creature)
            {
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new boss_galleon_AI(creature);
        }
};

void AddSC_boss_galleon()
{
    new boss_galleon();
}
