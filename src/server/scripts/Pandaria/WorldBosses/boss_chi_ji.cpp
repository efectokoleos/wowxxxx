/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptedCreature.h"
#include "ScriptMgr.h"

enum Spells
{
};

enum Creatures
{
    NPC_CHI_JI          = 71952,
};

class boss_chi_ji : public CreatureScript
{
    public:
        boss_chi_ji() : CreatureScript("boss_chi_ji") { }

        struct boss_chi_jiAI : public WorldBossAI
        {
            boss_chi_jiAI(Creature* creature) : WorldBossAI(creature)
            {
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new boss_chi_jiAI(creature);
        }
};

void AddSC_boss_chi_ji()
{
    new boss_chi_ji();
}
