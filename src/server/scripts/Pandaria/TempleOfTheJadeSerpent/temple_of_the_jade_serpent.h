/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef TEMPLE_JADE_SERPENT_H_
#define TEMPLE_JADE_SERPENT_H_

#include "CreatureAIImpl.h"

namespace TempleOfTheJadeSerpent
{
    static char constexpr const ScriptName[]    = "instance_temple_of_the_jade_serpent";
    static char constexpr const DataHeader[]    = "TJS";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 960;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum TJSDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_SHA_OF_DOUBT               = 0,
    DATA_LIU_FLAMEHEART             = 1,
    DATA_LOREWALKER_STONESTEP       = 2,
    DATA_WISE_MARI                  = 3,

    // Additional data
};

enum TJSCreatureIds
{
};

enum TJSGameObjectIds
{
};

#endif // TEMPLE_JADE_SERPENT_H_
