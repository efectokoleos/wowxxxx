/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "ScriptMgr.h"
#include "Map.h"
#include "temple_of_the_jade_serpent.h"

class instance_temple_of_the_jade_serpent : public InstanceMapScript
{
    public:
        instance_temple_of_the_jade_serpent() : InstanceMapScript(TempleOfTheJadeSerpent::ScriptName, TempleOfTheJadeSerpent::MapId) { }

        struct instance_temple_of_the_jade_serpent_InstanceMapScript : public InstanceScript
        {
            instance_temple_of_the_jade_serpent_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(TempleOfTheJadeSerpent::DataHeader);
                SetBossNumber(TempleOfTheJadeSerpent::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_temple_of_the_jade_serpent_InstanceMapScript(map);
        }
};

void AddSC_instance_temple_of_the_jade_serpent()
{
    new instance_temple_of_the_jade_serpent();
}
