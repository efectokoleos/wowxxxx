/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

// Gate of the Setting Sun
void AddSC_instance_gate_of_the_setting_sun();

// Heart of Fear
void AddSC_instance_heart_of_fear();

// Mogu'shan Palace
void AddSC_instance_mogu_shan_palace();

// Mogu'shan Vaults
void AddSC_instance_mogu_shan_vault();

// Shado-Pan Monastery
void AddSC_instance_shadopan_monastery();

// Siege of Niuzao Temple
void AddSC_instance_siege_of_niuzao_temple();

// Siege of Orgrimmar
void AddSC_instance_siege_of_orgrimmar();

// Stormstout Brewery
void AddSC_instance_stormstout_brewery();

// Temple of the Jade Serpent
void AddSC_instance_temple_of_the_jade_serpent();

// Terrace of Endless Spring
void AddSC_instance_terrace_of_the_endless_spring();

// Throne of Thunder
void AddSC_instance_throne_of_thunder();

// Worldbosses
void AddSC_boss_chi_ji();
void AddSC_boss_galleon();
void AddSC_boss_niuzao();
void AddSC_boss_oondasta();
void AddSC_boss_ordos();
void AddSC_boss_sha_of_anger();
void AddSC_boss_xuen();
void AddSC_boss_yu_lon();

// Zones
void AddSC_zone_wandering_isle();

// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddPandariaScripts()
{
    // Gate of the Setting Sun
    AddSC_instance_gate_of_the_setting_sun();

    // Heart of Fear
    AddSC_instance_heart_of_fear();

    // Mogu'shan Palace
    AddSC_instance_mogu_shan_palace();

    // Mogu'shan Vaults
    AddSC_instance_mogu_shan_vault();

    // Shado-Pan Monastery
    AddSC_instance_shadopan_monastery();

    // Siege of Niuzao Temple
    AddSC_instance_siege_of_niuzao_temple();

    // Siege of Orgrimmar
    AddSC_instance_siege_of_orgrimmar();

    // Stormstout Brewery
    AddSC_instance_stormstout_brewery();

    // Temple of the Jade Serpent
    AddSC_instance_temple_of_the_jade_serpent();

    // Terrace of Endless Spring
    AddSC_instance_terrace_of_the_endless_spring();

    // Throne of Thunder
    AddSC_instance_throne_of_thunder();

    // Worldbosses
    AddSC_boss_chi_ji();
    AddSC_boss_galleon();
    AddSC_boss_niuzao();
    AddSC_boss_oondasta();
    AddSC_boss_ordos();
    AddSC_boss_sha_of_anger();
    AddSC_boss_xuen();
    AddSC_boss_yu_lon();

    // Zones
    AddSC_zone_wandering_isle();
}
