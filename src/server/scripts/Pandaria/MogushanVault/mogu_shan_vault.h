/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef MOGUSHAN_VAULT_H_
#define MOGUSHAN_VAULT_H_

#include "CreatureAIImpl.h"

namespace MoguShanVault
{
    static char constexpr const ScriptName[]    = "instance_mogu_shan_vault";
    static char constexpr const DataHeader[]    = "MSV";
    uint32 const EncounterCount                 = 6;
    uint32 const MapId                          = 1008;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum MSVDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_FENG_THE_ACCURSED          = 0,
    DATA_THE_STONE_GUARD            = 1,
    DATA_WILL_OF_THE_EMPEROR        = 2,
    DATA_GARA_JAL_THE_SPIRITBINDER  = 3,
    DATA_THE_SPIRIT_KINGS           = 4,
    DATA_ELEGON                     = 5,

    // Additional data
};

enum MSVCreatureIds
{
};

enum MSVGameObjectIds
{
};

#endif // MOGUSHAN_VAULT_H_
