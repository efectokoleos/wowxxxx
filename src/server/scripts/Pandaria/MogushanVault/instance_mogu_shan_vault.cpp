/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "ScriptMgr.h"
#include "Map.h"
#include "mogu_shan_vault.h"

class instance_mogu_shan_vault : public InstanceMapScript
{
    public:
        instance_mogu_shan_vault() : InstanceMapScript(MoguShanVault::ScriptName, MoguShanVault::MapId) { }

        struct instance_mogu_shan_vault_InstanceMapScript : public InstanceScript
        {
            instance_mogu_shan_vault_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(MoguShanVault::DataHeader);
                SetBossNumber(MoguShanVault::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_mogu_shan_vault_InstanceMapScript(map);
        }
};

void AddSC_instance_mogu_shan_vault()
{
    new instance_mogu_shan_vault();
}
