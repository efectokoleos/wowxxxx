/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "InstanceScript.h"
#include "ScriptMgr.h"
#include "Map.h"
#include "terrace_of_endless_spring.h"

class instance_terrace_of_the_endless_spring : public InstanceMapScript
{
    public:
        instance_terrace_of_the_endless_spring() : InstanceMapScript(TerraceOfEndlessSpring::ScriptName, TerraceOfEndlessSpring::MapId) { }

        struct instance_terrace_of_the_endless_spring_InstanceMapScript : public InstanceScript
        {
            instance_terrace_of_the_endless_spring_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(TerraceOfEndlessSpring::DataHeader);
                SetBossNumber(TerraceOfEndlessSpring::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_terrace_of_the_endless_spring_InstanceMapScript(map);
        }
};

void AddSC_instance_terrace_of_the_endless_spring()
{
    new instance_terrace_of_the_endless_spring();
}
