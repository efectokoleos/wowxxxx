/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef TERRACE_OF_ENDLESS_SPRING_H_
#define TERRACE_OF_ENDLESS_SPRING_H_

#include "CreatureAIImpl.h"

namespace TerraceOfEndlessSpring
{
    static char constexpr const ScriptName[]    = "instance_terrace_of_endless_spring";
    static char constexpr const DataHeader[]    = "TES";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 996;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum TESDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_PROTECTORS_OF_THE_ENDLESS  = 0,
    DATA_SHA_OF_FEAR                = 1,
    DATA_TSULONG                    = 2,
    DATA_LEI_SHI                    = 3,

    // Additional data
};

enum TESCreatureIds
{
};

enum TESGameObjectIds
{
};

#endif // TERRACE_OF_ENDLESS_SPRING_H_
