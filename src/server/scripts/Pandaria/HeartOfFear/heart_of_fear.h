/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef HEART_OF_FEAR_H_
#define HEART_OF_FEAR_H_

#include "CreatureAIImpl.h"

namespace HeartOfFear
{
    static char constexpr const ScriptName[]    = "instance_heart_of_fear";
    static char constexpr const DataHeader[]    = "HOF";
    uint32 const EncounterCount                 = 6;
    uint32 const MapId                          = 1009;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum HOFDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_GARALON                    = 0,
    DATA_WIND_LORD_MELJARAK         = 1,
    DATA_AMBER_SHAPER_UNSOCK        = 2,
    DATA_GRAND_EMPRESS_SHEKZEER     = 3,
    DATA_BLADE_LORD_TAYAK           = 4,
    DATA_IMPERIAL_VIZIER_ZORLOCK    = 5,

    // Additional data
};

enum HOFCreatureIds
{
};

enum HOFGameObjectIds
{
};

#endif // HEART_OF_FEAR_H_
