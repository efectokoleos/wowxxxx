/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef THE_EMERALD_NIGHTMARE_H_
#define THE_EMERALD_NIGHTMARE_H_

#include "CreatureAIImpl.h"

namespace TheEmeraldNightmare
{
    static char constexpr const ScriptName[]    = "instance_the_emerald_nightmare";
    static char constexpr const DataHeader[]    = "TEN";
    uint32 const EncounterCount                 = 7;
    uint32 const MapId                          = 1520;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum TENDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_URSOC                              = 0,
    DATA_NYTHENDRA                          = 1,
    DATA_DRAGONS_OF_NIGHTMARE               = 2,
    DATA_XAVIUS                             = 3,
    DATA_IL_GYNOTH_THE_HEART_OF_CURRUPTION  = 4,
    DATA_ELERETHE_RENFERAL                  = 5,
    DATA_CENARIUS                           = 6,

    // Additional data
};

enum TENCreatureIds
{
};

enum TENGameObjectIds
{
};

#endif // THE_EMERALD_NIGHTMARE_H_
