/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef EYE_OF_AZSHARA_H_
#define EYE_OF_AZSHARA_H_

#include "CreatureAIImpl.h"

namespace EyeOfAzshara
{
    static char constexpr const ScriptName[]    = "instance_eye_of_azshara";
    static char constexpr const DataHeader[]    = "EOA";
    uint32 const EncounterCount                 = 5;
    uint32 const MapId                          = 1456;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum EOADataTypes
{
    // Encounter States/Boss GUIDs
    DATA_WARLORD_PARJESH            = 0,
    DATA_LADY_HATECOIL              = 1,
    DATA_KING_DEEPBEARD             = 2,
    DATA_SERPENTRIX                 = 3,
    DATA_WRATH_OF_AZSHARA           = 4,

    // Additional data
};

enum EOACreatureIds
{
};

enum EOAGameObjectIds
{
};

#endif // EYE_OF_AZSHARA_H_
