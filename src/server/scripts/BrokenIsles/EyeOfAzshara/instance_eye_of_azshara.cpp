/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "eye_of_azshara.h"

class instance_eye_of_azshara : public InstanceMapScript
{
    public:
        instance_eye_of_azshara() : InstanceMapScript(EyeOfAzshara::ScriptName, EyeOfAzshara::MapId) { }

        struct instance_eye_of_azshara_InstanceMapScript : public InstanceScript
        {
            instance_eye_of_azshara_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(EyeOfAzshara::DataHeader);
                SetBossNumber(EyeOfAzshara::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_eye_of_azshara_InstanceMapScript(map);
        }
};

void AddSC_instance_eye_of_azshara()
{
    new instance_eye_of_azshara();
}
