/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef VAULT_OF_THE_WARDENS_H_
#define VAULT_OF_THE_WARDENS_H_

#include "CreatureAIImpl.h"

namespace VaultOfTheWardens
{
    static char constexpr const ScriptName[]    = "instance_vault_of_the_wardens";
    static char constexpr const DataHeader[]    = "VOTW";
    uint32 const EncounterCount                 = 5;
    uint32 const MapId                          = 1493;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum VOTWDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_TIRATHON_SALTHERIL         = 0,
    DATA_ASH_GOLM                   = 1,
    DATA_GLAZER                     = 2,
    DATA_CORDANA_FELSONG            = 3,
    DATA_INQUISITOR_TORMENTORUM     = 4,

    // Additional data
};

enum VOTWCreatureIds
{
};

enum VOTWGameObjectIds
{
};

#endif // VAULT_OF_THE_WARDENS_H_
