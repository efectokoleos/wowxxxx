/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "vault_of_the_wardens.h"

class instance_vault_of_the_wardens : public InstanceMapScript
{
    public:
        instance_vault_of_the_wardens() : InstanceMapScript(VaultOfTheWardens::ScriptName, VaultOfTheWardens::MapId) { }

        struct instance_vault_of_the_wardens_InstanceMapScript : public InstanceScript
        {
            instance_vault_of_the_wardens_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(VaultOfTheWardens::DataHeader);
                SetBossNumber(VaultOfTheWardens::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_vault_of_the_wardens_InstanceMapScript(map);
        }
};

void AddSC_instance_vault_of_the_wardens()
{
    new instance_vault_of_the_wardens();
}
