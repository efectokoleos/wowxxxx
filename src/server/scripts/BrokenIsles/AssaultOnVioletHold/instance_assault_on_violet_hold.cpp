/*
 * Copyright (C) 2008-2016 Frostmourne <http://www.frostmourne.eu/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "assault_on_violet_hold.h"

class instance_assault_on_violet_hold : public InstanceMapScript
{
    public:
        instance_assault_on_violet_hold() : InstanceMapScript(AssaultOnVioletHold::ScriptName, AssaultOnVioletHold::MapId) { }

        struct instance_assault_on_violet_hold_InstanceMapScript : public InstanceScript
        {
            instance_assault_on_violet_hold_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(AssaultOnVioletHold::DataHeader);
                SetBossNumber(AssaultOnVioletHold::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_assault_on_violet_hold_InstanceMapScript(map);
        }
};

void AddSC_instance_assault_on_violet_hold()
{
    new instance_assault_on_violet_hold();
}
