/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef ASSAULT_ON_VIOLET_HOLD_H_
#define ASSAULT_ON_VIOLET_HOLD_H_

#include "CreatureAIImpl.h"

namespace AssaultOnVioletHold
{
    static char constexpr const ScriptName[]    = "instance_assault_on_violet_hold";
    static char constexpr const DataHeader[]    = "AOVH";
    uint32 const EncounterCount                 = 8;
    uint32 const MapId                          = 1544;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum AOVHDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_SHIVERMAW                  = 0,
    DATA_MINDFLAYER_KAAHRJ          = 1,
    DATA_MILLIFICENT_MANASTORM      = 2,
    DATA_FESTERFACE                 = 3,
    DATA_SAEL_ORN                   = 4,
    DATA_ANUB_ESSET                 = 5,
    DATA_BLOOD_PRINCESS_THAL_ENA    = 6,
    DATA_FEL_LORD_BETRUG            = 7,

    // Additional data
};

enum AOVHCreatureIds
{
};

enum AOVHGameObjectIds
{
};

#endif // ASSAULT_ON_VIOLET_HOLD_H_
