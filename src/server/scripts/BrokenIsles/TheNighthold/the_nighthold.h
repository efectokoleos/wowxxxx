/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef THE_NIGHTHOLD_H_
#define THE_NIGHTHOLD_H_

#include "CreatureAIImpl.h"

namespace TheNighthold
{
    static char constexpr const ScriptName[]    = "instance_the_nighthold";
    static char constexpr const DataHeader[]    = "TN";
    uint32 const EncounterCount                 = 10;
    uint32 const MapId                          = 1530;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum TNDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_KROSUS                     = 0,
    DATA_SKORPYRON                  = 1,
    DATA_TICHONDRIUS                = 2,
    DATA_STAR_AUGUR_ETRAEUS         = 3,
    DATA_CHRONOMATIC_ANOMALY        = 4,
    DATA_GUL_DAN                    = 5,
    DATA_TRILLIAX                   = 6,
    DATA_SPELLBLADE_ALURIEL         = 7,
    DATA_GRAND_MAGISTRIX_ELISANDE   = 8,
    DATA_HIGH_BOTANIST_TEL_AM       = 9,

    // Additional data
};

enum TNCreatureIds
{
};

enum TNGameObjectIds
{
};

#endif // THE_NIGHTHOLD_H_
