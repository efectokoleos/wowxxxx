/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef COURT_OF_STARS_H_
#define COURT_OF_STARS_H_

#include "CreatureAIImpl.h"

namespace CourtOfStars
{
    static char constexpr const ScriptName[]    = "instance_court_of_stars";
    static char constexpr const DataHeader[]    = "COS";
    uint32 const EncounterCount                 = 3;
    uint32 const MapId                          = 1571;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum COSDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_PATROL_CAPTAIN_GERDO       = 0,
    DATA_TALIXAE_FLAMEWREATH        = 1,
    DATA_ADVISOR_MELANDRUS          = 2,

    // Additional data
};

enum COSCreatureIds
{
};

enum COSGameObjectIds
{
};

#endif // COURT_OF_STARS_H_
