/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "court_of_stars.h"

class instance_court_of_stars : public InstanceMapScript
{
    public:
        instance_court_of_stars() : InstanceMapScript(CourtOfStars::ScriptName, CourtOfStars::MapId) { }

        struct instance_court_of_stars_InstanceMapScript : public InstanceScript
        {
            instance_court_of_stars_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(CourtOfStars::DataHeader);
                SetBossNumber(CourtOfStars::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_court_of_stars_InstanceMapScript(map);
        }
};

void AddSC_instance_court_of_stars()
{
    new instance_court_of_stars();
}
