/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef BLACK_ROOK_HOLD_H_
#define BLACK_ROOK_HOLD_H_

#include "CreatureAIImpl.h"

namespace BlackRookHold
{
    static char constexpr const ScriptName[]    = "instance_black_rook_hold";
    static char constexpr const DataHeader[]    = "BRH";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1501;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum BRHDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_AMALGAM_OF_SOULS           = 0,
    DATA_ILLYSANNA_RAVENCREST       = 1,
    DATA_SMASHSPITE_THE_HATEFUL     = 2,
    DATA_LORD_KUR_TALOS_RAVENCREST  = 3,

    // Additional data
};

enum BRHCreatureIds
{
};

enum BRHGameObjectIds
{
};

#endif // BLACK_ROOK_HOLD_H_
