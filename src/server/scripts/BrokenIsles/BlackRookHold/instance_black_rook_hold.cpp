/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "black_rook_hold.h"

class instance_black_rook_hold : public InstanceMapScript
{
    public:
        instance_black_rook_hold() : InstanceMapScript(BlackRookHold::ScriptName, BlackRookHold::MapId) { }

        struct instance_black_rook_hold_InstanceMapScript : public InstanceScript
        {
            instance_black_rook_hold_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(BlackRookHold::DataHeader);
                SetBossNumber(BlackRookHold::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_black_rook_hold_InstanceMapScript(map);
        }
};

void AddSC_instance_black_rook_hold()
{
    new instance_black_rook_hold();
}
