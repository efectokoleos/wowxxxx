/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef NELTHARIONS_LAIR_H_
#define NELTHARIONS_LAIR_H_

#include "CreatureAIImpl.h"

namespace NeltharionsLair
{
    static char constexpr const ScriptName[]    = "instance_neltharions_lair";
    static char constexpr const DataHeader[]    = "NL";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1458;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum NLDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_ROKMORA                    = 0,
    DATA_ULAROGG_CRAGSHAPER         = 1,
    DATA_NARAXAS                    = 2,
    DATA_DARGRUL_THE_UNDERKING      = 3,

    // Additional data
};

enum NLCreatureIds
{
};

enum NLGameObjectIds
{
};

#endif // NELTHARIONS_LAIR_H_
