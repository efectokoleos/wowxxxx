/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "neltharions_lair.h"

class instance_neltharions_lair : public InstanceMapScript
{
    public:
        instance_neltharions_lair() : InstanceMapScript(NeltharionsLair::ScriptName, NeltharionsLair::MapId) { }

        struct instance_neltharions_lair_InstanceMapScript : public InstanceScript
        {
            instance_neltharions_lair_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(NeltharionsLair::DataHeader);
                SetBossNumber(NeltharionsLair::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_neltharions_lair_InstanceMapScript(map);
        }
};

void AddSC_instance_neltharions_lair()
{
    new instance_neltharions_lair();
}
