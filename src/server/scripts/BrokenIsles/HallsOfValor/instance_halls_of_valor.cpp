/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "halls_of_valor.h"

class instance_halls_of_valor : public InstanceMapScript
{
    public:
        instance_halls_of_valor() : InstanceMapScript(HallsOfValor::ScriptName, HallsOfValor::MapId) { }

        struct instance_halls_of_valor_InstanceMapScript : public InstanceScript
        {
            instance_halls_of_valor_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(HallsOfValor::DataHeader);
                SetBossNumber(HallsOfValor::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_halls_of_valor_InstanceMapScript(map);
        }
};

void AddSC_instance_halls_of_valor()
{
    new instance_halls_of_valor();
}
