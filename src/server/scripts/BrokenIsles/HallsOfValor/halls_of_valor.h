/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef HALLS_OF_VALOR_H_
#define HALLS_OF_VALOR_H_

#include "CreatureAIImpl.h"

namespace HallsOfValor
{
    static char constexpr const ScriptName[]    = "instance_halls_of_valor";
    static char constexpr const DataHeader[]    = "HOV";
    uint32 const EncounterCount                 = 5;
    uint32 const MapId                          = 1477;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum HOVDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_HYMDALL                    = 0,
    DATA_HYRJA                      = 1,
    DATA_FENRYR                     = 2,
    DATA_GOD_KING_SKOVALD           = 3,
    DATA_ODYN                       = 4,

    // Additional data
};

enum HOVCreatureIds
{
};

enum HOVGameObjectIds
{
};

#endif // HALLS_OF_VALOR_H_
