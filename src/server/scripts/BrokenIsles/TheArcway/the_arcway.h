/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef THE_ARCWAY_H_
#define THE_ARCWAY_H_

#include "CreatureAIImpl.h"

namespace TheArcway
{
    static char constexpr const ScriptName[]    = "instance_the_arcway";
    static char constexpr const DataHeader[]    = "TAW";
    uint32 const EncounterCount                 = 5;
    uint32 const MapId                          = 1516;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum TAWDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_CORSTILAX                  = 0,
    DATA_NAL_TIRA                   = 1,
    DATA_IVANYR                     = 2,
    DATA_GENERAL_XAKAL              = 3,
    DATA_ADVISOR_VANDROS            = 4,

    // Additional data
};

enum TAWCreatureIds
{
};

enum TAWGameObjectIds
{
};

#endif // THE_ARCWAY_H_
