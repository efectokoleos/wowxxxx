/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "the_arcway.h"

class instance_the_arcway : public InstanceMapScript
{
    public:
        instance_the_arcway() : InstanceMapScript(TheArcway::ScriptName, TheArcway::MapId) { }

        struct instance_the_arcway_InstanceMapScript : public InstanceScript
        {
            instance_the_arcway_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(TheArcway::DataHeader);
                SetBossNumber(TheArcway::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_the_arcway_InstanceMapScript(map);
        }
};

void AddSC_instance_the_arcway()
{
    new instance_the_arcway();
}
