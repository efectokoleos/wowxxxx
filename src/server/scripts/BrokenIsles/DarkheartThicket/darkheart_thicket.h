/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef DARKHEART_THICKET_H_
#define DARKHEART_THICKET_H_

#include "CreatureAIImpl.h"

namespace DarkheartThicket
{
    static char constexpr const ScriptName[]    = "instance_darkheart_thicket";
    static char constexpr const DataHeader[]    = "DHT";
    uint32 const EncounterCount                 = 4;
    uint32 const MapId                          = 1466;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum DHTDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_ARCHDRUID_GLAIDALIS        = 0,
    DATA_OAKHEART                   = 1,
    DATA_DRESARON                   = 2,
    DATA_SHADE_OF_XAVIUS            = 3,

    // Additional data
};

enum DHTCreatureIds
{
};

enum DHTGameObjectIds
{
};

#endif // DARKHEART_THICKET_H_
