/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "darkheart_thicket.h"

class instance_darkheart_thicket : public InstanceMapScript
{
    public:
        instance_darkheart_thicket() : InstanceMapScript(DarkheartThicket::ScriptName, DarkheartThicket::MapId) { }

        struct instance_darkheart_thicket_InstanceMapScript : public InstanceScript
        {
            instance_darkheart_thicket_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(DarkheartThicket::DataHeader);
                SetBossNumber(DarkheartThicket::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_darkheart_thicket_InstanceMapScript(map);
        }
};

void AddSC_instance_darkheart_thicket()
{
    new instance_darkheart_thicket();
}
