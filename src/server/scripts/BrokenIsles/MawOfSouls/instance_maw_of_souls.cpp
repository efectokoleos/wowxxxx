/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "maw_of_souls.h"

class instance_maw_of_souls : public InstanceMapScript
{
    public:
        instance_maw_of_souls() : InstanceMapScript(MawOfSouls::ScriptName, MawOfSouls::MapId) { }

        struct instance_maw_of_souls_InstanceMapScript : public InstanceScript
        {
            instance_maw_of_souls_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(MawOfSouls::DataHeader);
                SetBossNumber(MawOfSouls::EncounterCount);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_maw_of_souls_InstanceMapScript(map);
        }
};

void AddSC_instance_maw_of_souls()
{
    new instance_maw_of_souls();
}
