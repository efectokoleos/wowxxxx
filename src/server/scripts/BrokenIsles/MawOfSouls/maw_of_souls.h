/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef MAW_OF_SOULS_H_
#define MAW_OF_SOULS_H_

#include "CreatureAIImpl.h"

namespace MawOfSouls
{
    static char constexpr const ScriptName[]    = "instance_maw_of_souls";
    static char constexpr const DataHeader[]    = "MOS";
    uint32 const EncounterCount                 = 3;
    uint32 const MapId                          = 1492;

    template<class AI, class T>
    inline AI* GetAI(T* obj)
    {
        return GetInstanceAI<AI, T>(obj, ScriptName);
    }
}

enum MOSDataTypes
{
    // Encounter States/Boss GUIDs
    DATA_YMIRON_THE_FALLEN_KING     = 0,
    DATA_HARBARON                   = 1,
    DATA_HELYA                      = 2,

    // Additional data
};

enum MOSCreatureIds
{
};

enum MOSGameObjectIds
{
};

#endif // MAW_OF_SOULS_H_
