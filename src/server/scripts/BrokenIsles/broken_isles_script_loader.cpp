/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

// Assault on Violet Hold
void AddSC_instance_assault_on_violet_hold();

// Black Rook Hold
void AddSC_instance_black_rook_hold();

// Court of Stars
void AddSC_instance_court_of_stars();

// Darkheart Thicket
void AddSC_instance_darkheart_thicket();

// Eye of Azshara
void AddSC_instance_eye_of_azshara();

// Halls of Valor
void AddSC_instance_halls_of_valor();

// Maw of Souls
void AddSC_instance_maw_of_souls();

// Neltharion's Lair
void AddSC_instance_neltharions_lair();

// The Arcway
void AddSC_instance_the_arcway();

// The Emerald Nightmare
void AddSC_instance_the_emerald_nightmare();

// The Nighthold
void AddSC_instance_the_nighthold();

// Vault of the Wardens
void AddSC_instance_vault_of_the_wardens();

// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddBrokenIslesScripts()
{
    // Assault on Violet Hold
    AddSC_instance_assault_on_violet_hold();

    // Black Rook Hold
    AddSC_instance_black_rook_hold();

    // Court of Stars
    AddSC_instance_court_of_stars();

    // Darkheart Thicket
    AddSC_instance_darkheart_thicket();

    // Eye of Azshara
    AddSC_instance_eye_of_azshara();

    // Halls of Valor
    AddSC_instance_halls_of_valor();

    // Maw of Souls
    AddSC_instance_maw_of_souls();

    // Neltharion's Lair
    AddSC_instance_neltharions_lair();

    // The Arcway
    AddSC_instance_the_arcway();

    // The Emerald Nightmare
    AddSC_instance_the_emerald_nightmare();

    // The Nighthold
    AddSC_instance_the_nighthold();

    // Vault of the Wardens
    AddSC_instance_vault_of_the_wardens();
}
