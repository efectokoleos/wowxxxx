/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_pri_".
 */

#include "GridNotifiers.h"
#include "ObjectAccessor.h"
#include "PassiveAI.h"
#include "PetAI.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellHistory.h"
#include "TaskScheduler.h"
#include "TemporarySummon.h"

enum PriestSpells
{
    SPELL_PRIEST_LIGHTWELL_CHARGES          = 59907,
    SPELL_PRIEST_ATONEMENT_PROC_AURA        = 195178,
    SPELL_PRIEST_ATONEMENT_PASSIVE          = 81749,
    SPELL_PRIEST_SHADOWCRAWL                = 63619,
    SPELL_PRIEST_MANA_LEECH_MINDBENDER      = 123050,
    SPELL_PRIEST_MINDBENDER_TALENT          = 123040,
    SPELL_PRIEST_MIND_FLAY                  = 193473,
    SPELL_PRIEST_PSYFLAY                    = 199845,
};

class npc_pet_pri_lightwell : public CreatureScript
{
    public:
        npc_pet_pri_lightwell() : CreatureScript("npc_pet_pri_lightwell") { }

        struct npc_pet_pri_lightwellAI : public PassiveAI
        {
            npc_pet_pri_lightwellAI(Creature* creature) : PassiveAI(creature)
            {
                DoCast(me, SPELL_PRIEST_LIGHTWELL_CHARGES, false);
            }

            void EnterEvadeMode(EvadeReason /*why*/) override
            {
                if (!me->IsAlive())
                    return;

                me->DeleteThreatList();
                me->CombatStop(true);
                me->ResetPlayerDamageReq();
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_pri_lightwellAI(creature);
        }
};

class npc_pet_pri_shadowfiend : public CreatureScript
{
    public:
        npc_pet_pri_shadowfiend() : CreatureScript("npc_pet_pri_shadowfiend") { }

        struct npc_pet_pri_shadowfiendAI : public PetAI
        {
            npc_pet_pri_shadowfiendAI(Creature* creature) : PetAI(creature) { }

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->GetTypeId() != TYPEID_PLAYER)
                    return;

                if (Unit* target = summoner->ToPlayer()->GetSelectedUnit())
                    AttackStart(target);

                if (summoner->HasAura(SPELL_PRIEST_ATONEMENT_PASSIVE))
                    DoCast(me, SPELL_PRIEST_ATONEMENT_PROC_AURA, true);
                PetAI::IsSummonedBy(summoner);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_pri_shadowfiendAI(creature);
        }
};

class npc_pet_pri_mindbender : public CreatureScript
{
    public:
        npc_pet_pri_mindbender() : CreatureScript("npc_pet_pri_mindbender") { }

        struct npc_pet_pri_mindbenderAI : public PetAI
        {
            npc_pet_pri_mindbenderAI(Creature* creature) : PetAI(creature) { }

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->GetTypeId() != TYPEID_PLAYER)
                    return;

                if (Unit* target = summoner->ToPlayer()->GetSelectedUnit())
                    AttackStart(target);

                if (summoner->HasAura(SPELL_PRIEST_ATONEMENT_PASSIVE))
                    DoCast(me, SPELL_PRIEST_ATONEMENT_PROC_AURA, true);

                DoCast(me, SPELL_PRIEST_MANA_LEECH_MINDBENDER, true);

                _scheduler.Schedule(Milliseconds(100), [this](TaskContext task)
                {
                    if (!me->GetSpellHistory()->HasCooldown(SPELL_PRIEST_SHADOWCRAWL))
                    {
                        DoCastVictim(SPELL_PRIEST_SHADOWCRAWL, true);
                        task.Repeat(Seconds(6));
                    }
                    else
                        task.Repeat(Milliseconds(250));
                });

                PetAI::IsSummonedBy(summoner);
            }

            void UpdateAI(uint32 diff) override
            {
                PetAI::UpdateAI(diff);
                _scheduler.Update(diff);
            }

        private:
            TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_pri_mindbenderAI(creature);
        }
};

class npc_pri_summon_call_of_the_void : public CreatureScript
{
    public:
        npc_pri_summon_call_of_the_void() : CreatureScript("npc_pri_summon_call_of_the_void") { }

        struct npc_pri_summon_call_of_the_voidAI : public ScriptedAI
        {
            npc_pri_summon_call_of_the_voidAI(Creature* creature) : ScriptedAI(creature)
            {
                SetCombatMovement(false);
            }

            Unit* SelectNewTarget()
            {
                if (TempSummon* summon = me->ToTempSummon())
                {
                    if (Unit* owner = summon->GetSummoner())
                    {
                        std::list<Unit*> targets;
                        Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(me, me, 40.0f);
                        Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(me, targets, u_check);
                        Cell::VisitAllObjects(me, searcher, 40.0f);
                        for (Unit* target : targets)
                            if (owner->IsInCombatWith(target) && owner->IsValidAttackTarget(target) && me->IsWithinLOSInMap(target))
                                return target;
                    }
                }
                return nullptr;
            }

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->GetTypeId() != TYPEID_PLAYER)
                    return;

                if (Unit* target = summoner->ToPlayer()->GetSelectedUnit())
                {
                    focusTargetGuid = target->GetGUID();
                    AttackStart(target);
                    DoCast(target, SPELL_PRIEST_MIND_FLAY, true);
                }

                _scheduler.Schedule(Milliseconds(500), [this](TaskContext task)
                {
                    if (me->GetCurrentSpell(CURRENT_CHANNELED_SPELL))
                    {
                        task.Repeat(Milliseconds(500));
                        return;
                    }

                    if (focusTargetGuid.IsEmpty())
                        if (Unit* target = SelectNewTarget())
                            focusTargetGuid = target->GetGUID();

                    if (Unit* focusTarget = ObjectAccessor::GetUnit(*me, focusTargetGuid))
                    {
                        if (!focusTarget->IsAlive() || !me->IsWithinLOSInMap(focusTarget))
                        {
                            if (Unit* target = SelectNewTarget())
                            {
                                me->SetInCombatWith(target);
                                DoCast(target, SPELL_PRIEST_MIND_FLAY, true);
                                focusTargetGuid = target->GetGUID();
                            }
                        }
                        else
                        {
                            me->SetInCombatWith(focusTarget);
                            DoCast(focusTarget, SPELL_PRIEST_MIND_FLAY, true);
                        }
                    }
                    task.Repeat(Milliseconds(500));
                });

                ScriptedAI::IsSummonedBy(summoner);
            }

            void UpdateAI(uint32 diff) override
            {
                ScriptedAI::UpdateAI(diff);
                _scheduler.Update(diff);
            }

            void EnterEvadeMode(EvadeReason /*why = EVADE_REASON_OTHER */) override { }
            void AttackStart(Unit* /*target*/) override { }

        private:
            TaskScheduler _scheduler;
            ObjectGuid focusTargetGuid;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pri_summon_call_of_the_voidAI(creature);
        }
};

class npc_pri_summon_psyfiend : public CreatureScript
{
    public:
        npc_pri_summon_psyfiend() : CreatureScript("npc_pri_summon_psyfiend") { }

        struct npc_pri_summon_psyfiendAI : public ScriptedAI
        {
            npc_pri_summon_psyfiendAI(Creature* creature) : ScriptedAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->GetTypeId() != TYPEID_PLAYER)
                    return;

                // From ginny (1,670 � 2�25) am 2017.01.10 (Patch 7.1.5)
                // In patch 7.1.5, Psyfiend is immune to crowd control and damage over time effects. It can also no longer be shielded.
                // ^ seems it's immune against apply aura effect
                me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_APPLY_AURA, true);

                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    if (me->GetCurrentSpell(CURRENT_CHANNELED_SPELL))
                    {
                        task.Repeat(Milliseconds(500));
                        return;
                    }
                    DoCast(me, SPELL_PRIEST_PSYFLAY, false);
                    task.Repeat(Milliseconds(500));
                });

                ScriptedAI::IsSummonedBy(summoner);
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pri_summon_psyfiendAI(creature);
        }
};

void AddSC_priest_pet_scripts()
{
    new npc_pet_pri_lightwell();
    new npc_pet_pri_shadowfiend();
    new npc_pet_pri_mindbender();
    new npc_pri_summon_call_of_the_void();
    new npc_pri_summon_psyfiend();
}
