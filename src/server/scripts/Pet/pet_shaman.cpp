/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_sha_".
 */

#include "CreatureAIImpl.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "PassiveAI.h"
#include "TemporarySummon.h"
#include "PetAI.h"

enum ShamanSpells
{
    // Earth Elemental
    SPELL_SHAMAN_ANGERED_EARTH           = 36213,

    // Fire Elemental
    SPELL_SHAMAN_FIREBLAST               = 57984,

    // Storm Totem
    SPELL_SHAMAN_CALL_LIGHTNING          = 157348,
    SPELL_SHAMAN_ELEMENTAL_TOTEM         = 34967,
    SPELL_SHAMAN_SOOTHING_WINDS          = 157333,
    SPELL_SHAMAN_WIND_GUST               = 157331,
    SPELL_SHAMAN_WIND_GUST_ENERGIZE      = 226180,

    // Spirit Wolf
    SPELL_SHAMAN_GLYPH_OF_SPIRIT_RAPTORS = 147783,
    SPELL_SHAMAN_RAPTOR_TRANSFORM        = 147908,
    SPELL_SHAMAN_FERAL_SPIRIT_DUMMY_AURA = 231723,
    SPELL_SHAMAN_FERAL_SPIRIT_MASTERY_EFFECT = 131073,

    SPELL_SHAMAN_STATIC_CHARGE           = 118905,
    SPELL_SHAMAN_EARTHEN_SHIELD_AREA     = 198839,
    SPELL_SHAMAN_VOODOO_TOTEM_AT         = 196935,
    SPELL_SHAMAN_SKYFURY_TOTEM_AT        = 208964,

    SPELL_SHAMAN_FIERY_JAWS              = 224125,
    SPELL_SHAMAN_FROZEN_BITE             = 224126,
    SPELL_SHAMAN_CRACKLING_SURGE         = 224127,

    SPELL_SHAMAN_ALPHA_WOLF             = 198434,
    SPELL_SHAMAN_ALPHA_WOLF_PERIODIC    = 198486,

    // Lightning Elemental
    SPELL_SHAMAN_LIGHTNING_BLAST        = 191726,
    SPELL_SHAMAN_CHAIN_LIGHTNING        = 191732,

};

enum ShamanActions
{
    // Lightning Elemental
    ACTION_LIGHTNING_BOLT       = 1,
    ACTION_CHAIN_LIGHTNING      = 2,
};

enum WolfForms
{
    WOLF_NORMAL_FORM    = 0,
    WOLF_FIRE_FORM      = 1,
    WOLF_ICE_FORM       = 2,
    WOLF_LIGHTNING_FORM = 3
};

// Greater Earth Elemental : 95072
class npc_pet_shaman_earth_elemental : public CreatureScript
{
    public:
        npc_pet_shaman_earth_elemental() : CreatureScript("npc_pet_shaman_earth_elemental") { }

        struct npc_pet_shaman_earth_elementalAI : public PetAI
        {
            npc_pet_shaman_earth_elementalAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                me->ApplySpellImmune(0, IMMUNITY_SCHOOL, SPELL_SCHOOL_MASK_NATURE, true);
                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    DoCastVictim(SPELL_SHAMAN_ANGERED_EARTH);
                    task.Repeat(Seconds(5));
                });
                PetAI::IsSummonedBy(summoner);
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                PetAI::UpdateAI(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_earth_elementalAI(creature);
        }
};

// Greater Fire Elemental : 95061
class npc_pet_shaman_fire_elemental : public CreatureScript
{
    public:
        npc_pet_shaman_fire_elemental() : CreatureScript("npc_pet_shaman_fire_elemental") { }

        struct npc_pet_shaman_fire_elementalAI : public PetAI
        {
            npc_pet_shaman_fire_elementalAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    DoCastVictim(SPELL_SHAMAN_FIREBLAST, true);
                    task.Repeat(Seconds(3));
                });

                me->ApplySpellImmune(0, IMMUNITY_SCHOOL, SPELL_SCHOOL_MASK_FIRE, true);
                PetAI::IsSummonedBy(summoner);
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                PetAI::UpdateAI(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_fire_elementalAI(creature);
        }
};

// 61245 - Lightning Surge Totem
class npc_pet_shaman_lightning_surge_totem : public CreatureScript
{
    public:
        npc_pet_shaman_lightning_surge_totem() : CreatureScript("npc_pet_shaman_lightning_surge_totem") { }

        struct npc_pet_shaman_lightning_surge_totemAI : public PassiveAI
        {
            npc_pet_shaman_lightning_surge_totemAI(Creature* creature) : PassiveAI(creature) { }

            void InitializeAI() override
            {
                _scheduler.Async([this]
                {
                    DoCastSelf(SPELL_SHAMAN_STATIC_CHARGE);
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }
        private:
            TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_lightning_surge_totemAI(creature);
        }
};

// 100943 - Earthen Shield Totem
class npc_pet_shaman_earthen_shield_totem : public CreatureScript
{
    public:
        npc_pet_shaman_earthen_shield_totem() : CreatureScript("npc_pet_shaman_earthen_shield_totem") { }

        struct npc_pet_shaman_earthen_shield_totemAI : public PassiveAI
        {
            npc_pet_shaman_earthen_shield_totemAI(Creature* creature) : PassiveAI(creature) {}

            void InitializeAI() override
            {
                DoCastSelf(SPELL_SHAMAN_EARTHEN_SHIELD_AREA);
                PassiveAI::InitializeAI();
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_earthen_shield_totemAI(creature);
        }
};

// Greater Storm Elemental : 77936
class npc_pet_shaman_storm_elemental : public CreatureScript
{
    public:
        npc_pet_shaman_storm_elemental() : CreatureScript("npc_pet_shaman_storm_elemental") { }

        struct npc_pet_shaman_storm_elementalAI : public PetAI
        {
            npc_pet_shaman_storm_elementalAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                DoCast(me, SPELL_SHAMAN_ELEMENTAL_TOTEM, true);

                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    if (++castCount == 6)
                    {
                        castCount = 0;
                        DoCastVictim(SPELL_SHAMAN_CALL_LIGHTNING);
                        task.Repeat(Seconds(3));
                    }
                    else
                    {
                        DoCastVictim(SPELL_SHAMAN_WIND_GUST);
                        if (Unit* owner = me->GetOwner())
                            me->CastSpell(owner, SPELL_SHAMAN_WIND_GUST_ENERGIZE, true);
                        task.Repeat(Milliseconds(2500));
                    }
                });
                PetAI::IsSummonedBy(summoner);
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                PetAI::UpdateAI(diff);
            }

            private:
                TaskScheduler _scheduler;
                uint8 castCount = 0;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_storm_elementalAI(creature);
        }
};

// 100099 - Voodoo Totem
class npc_pet_shaman_voodoo_totem : public CreatureScript
{
    public:
        npc_pet_shaman_voodoo_totem() : CreatureScript("npc_pet_shaman_voodoo_totem") { }

        struct npc_pet_shaman_voodoo_totemAI : public PassiveAI
        {
            npc_pet_shaman_voodoo_totemAI(Creature* creature) : PassiveAI(creature)
            {
                Initialize();
            }

            void Initialize()
            {
                DoCast(SPELL_SHAMAN_VOODOO_TOTEM_AT);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_voodoo_totemAI(creature);
        }
};

// 105427 - Skyfury Totem
class npc_pet_shaman_skyfury_totem : public CreatureScript
{
    public:
        npc_pet_shaman_skyfury_totem() : CreatureScript("npc_pet_shaman_skyfury_totem") { }

        struct npc_pet_shaman_skyfury_totemAI : public PassiveAI
        {
            npc_pet_shaman_skyfury_totemAI(Creature* creature) : PassiveAI(creature)
            {
                Initialize();
            }

            void Initialize()
            {
                DoCast(SPELL_SHAMAN_SKYFURY_TOTEM_AT);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_skyfury_totemAI(creature);
        }
};

// Spirit Wolf : 29264
// Spirit Wolf : 100820
class npc_pet_shaman_spirit_wolf : public CreatureScript
{
    public:
        npc_pet_shaman_spirit_wolf() : CreatureScript("npc_pet_shaman_spirit_wolf") { }

        struct npc_pet_shaman_spirit_wolfAI : public PetAI
        {
            npc_pet_shaman_spirit_wolfAI(Creature* creature) : PetAI(creature) { }

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->HasAura(SPELL_SHAMAN_GLYPH_OF_SPIRIT_RAPTORS))
                    DoCast(me, SPELL_SHAMAN_RAPTOR_TRANSFORM, true);

                if (summoner->HasAura(SPELL_SHAMAN_FERAL_SPIRIT_DUMMY_AURA))
                    DoCast(me, SPELL_SHAMAN_FERAL_SPIRIT_MASTERY_EFFECT, true);

                if (summoner->HasAura(SPELL_SHAMAN_ALPHA_WOLF))
                    DoCast(me, SPELL_SHAMAN_ALPHA_WOLF_PERIODIC, true);

                if (me->GetEntry() == ENTRY_SPIRIT_DOOM_WOLF)
                {
                    int32 spellId = RAND(SPELL_SHAMAN_FIERY_JAWS, SPELL_SHAMAN_FROZEN_BITE, SPELL_SHAMAN_CRACKLING_SURGE);

                    // Don't overwrite the glyph displayid
                    if (!summoner->HasAura(SPELL_SHAMAN_GLYPH_OF_SPIRIT_RAPTORS))
                    {
                        switch (spellId)
                        {
                            case SPELL_SHAMAN_FIERY_JAWS:
                                me->SetDisplayId(me->GetCreatureTemplate()->Modelid1);
                                formVersion = WOLF_FIRE_FORM;
                                break;
                            case SPELL_SHAMAN_FROZEN_BITE:
                                me->SetDisplayId(me->GetCreatureTemplate()->Modelid2);
                                formVersion = WOLF_ICE_FORM;
                                break;
                            case SPELL_SHAMAN_CRACKLING_SURGE:
                                me->SetDisplayId(me->GetCreatureTemplate()->Modelid3);
                                formVersion = WOLF_LIGHTNING_FORM;
                                break;
                            default:
                                break;
                        }
                    }

                    _scheduler.Schedule(Milliseconds(), [this, spellId](TaskContext task)
                    {
                        DoCastVictim(spellId);
                        task.Repeat(Seconds(4));
                    });
                }
            }

            void EnterEvadeMode(EvadeReason /*why*/) override { }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                PetAI::UpdateAI(diff);
            }

            uint32 GetData(uint32 /*data*/) const override
            {
                return formVersion;
            }

            private:
                TaskScheduler _scheduler;
                uint32 formVersion = WOLF_NORMAL_FORM;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_spirit_wolfAI(creature);
        }
};

class npc_pet_shaman_lightning_elemental : public CreatureScript
{
    public:
        npc_pet_shaman_lightning_elemental() : CreatureScript("npc_pet_shaman_lightning_elemental") { }

        struct npc_pet_shaman_lightning_elementalAI : public PetAI
        {
            npc_pet_shaman_lightning_elementalAI(Creature* creature) : PetAI(creature) { }

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->getAttackerForHelper())
                    me->SetInCombatWith(summoner->getAttackerForHelper());

                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    DoCastVictim(SPELL_SHAMAN_LIGHTNING_BLAST);
                    task.Repeat(Milliseconds(1500));
                });
            }

            void DoAction(int32 action) override
            {
                if (actionDone)
                    return;

                actionDone = true;

                if (action == ACTION_CHAIN_LIGHTNING)
                {
                    _scheduler.CancelAll();
                    _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                    {
                        DoCastVictim(SPELL_SHAMAN_CHAIN_LIGHTNING);
                        task.Repeat(Milliseconds(1500));
                    });
                }
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                PetAI::UpdateAI(diff);
            }

            private:
                TaskScheduler _scheduler;
                bool actionDone = false;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_shaman_lightning_elementalAI(creature);
        }
};

void AddSC_shaman_pet_scripts()
{
    new npc_pet_shaman_earth_elemental();
    new npc_pet_shaman_fire_elemental();
    new npc_pet_shaman_storm_elemental();
    new npc_pet_shaman_lightning_surge_totem();
    new npc_pet_shaman_earthen_shield_totem();
    new npc_pet_shaman_voodoo_totem();
    new npc_pet_shaman_skyfury_totem();
    new npc_pet_shaman_spirit_wolf();
    new npc_pet_shaman_lightning_elemental();
}
