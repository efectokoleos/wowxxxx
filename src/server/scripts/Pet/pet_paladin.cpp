/*
* Copyright (C) 2008-2017 Frostmourne <http://www.frostmourne.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "Cell.h"
#include "CellImpl.h"
#include "Common.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "PassiveAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"

enum PaladinSummonsSpells
{
    SPELL_PALADIN_LIGHTS_HAMMER_COSMETIC            = 122257,
    SPELL_PALADIN_LIGHTS_HAMMER_PERIODIC            = 114918,
    SPELL_PALADIN_LIGHTS_HAMMER_DAMAGE              = 114919,
    SPELL_PALADIN_LIGHTS_HAMMER_HEAL                = 119952,
    SPELL_PALADIN_DIVINE_SHIELD_CHANNELED           = 228050,
    SPELL_PALADIN_GUARDIAN_OF_THE_FORGOTTEN_QUEEN   = 228049,
};

// 59738 - Light's Hammer
class npc_pal_legion_light_s_hammer : public CreatureScript
{
    public:
        npc_pal_legion_light_s_hammer() : CreatureScript("npc_pal_legion_light_s_hammer") { }

        struct npc_pal_legion_light_s_hammerAI : public ScriptedAI
        {
            npc_pal_legion_light_s_hammerAI(Creature* creature) : ScriptedAI(creature)
            {
                DoCast(me, SPELL_PALADIN_LIGHTS_HAMMER_COSMETIC, true);
                DoCast(me, SPELL_PALADIN_LIGHTS_HAMMER_PERIODIC, true);
                SetCombatMovement(false);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pal_legion_light_s_hammerAI(creature);
        }
};

// 228049 - Guardian of the forgotten queen
class npc_pal_legion_guardian_of_the_forgotten_queen : public CreatureScript
{
    public:
        npc_pal_legion_guardian_of_the_forgotten_queen() : CreatureScript("npc_pal_legion_guardian_of_the_forgotten_queen") { }

        struct npc_pal_legion_guardian_of_the_forgotten_queenAI : public PassiveAI
        {
            npc_pal_legion_guardian_of_the_forgotten_queenAI(Creature* creature) : PassiveAI(creature) {}

            void IsSummonedBy(Unit* /*owner*/) override
            {
                me->SetDisableGravity(true);
                me->SetHover(true);

                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    if (me->GetCurrentSpell(CURRENT_CHANNELED_SPELL))
                    {
                        task.Repeat(Milliseconds(500));
                        return;
                    }

                    DoCast(me, SPELL_PALADIN_DIVINE_SHIELD_CHANNELED, false);
                    task.Repeat(Milliseconds(500));
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pal_legion_guardian_of_the_forgotten_queenAI(creature);
        }
};

// 114918 - Light's Hammer
class spell_pal_legion_light_s_hammer_periodic : public SpellScriptLoader
{
    public:
        spell_pal_legion_light_s_hammer_periodic() : SpellScriptLoader("spell_pal_legion_light_s_hammer_periodic") { }

        class spell_pal_legion_light_s_hammer_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_legion_light_s_hammer_periodic_AuraScript);

            void OnPeriodic(AuraEffect const* aurEff)
            {
                GetTarget()->CastSpell(GetTarget()->GetPositionX(), GetTarget()->GetPositionY(), GetTarget()->GetPositionZ(), SPELL_PALADIN_LIGHTS_HAMMER_DAMAGE, true, nullptr, aurEff, GetTarget()->GetCharmerOrOwnerOrOwnGUID());
                GetTarget()->CastSpell(GetTarget()->GetPositionX(), GetTarget()->GetPositionY(), GetTarget()->GetPositionZ(), SPELL_PALADIN_LIGHTS_HAMMER_HEAL, true, nullptr, aurEff, GetTarget()->GetCharmerOrOwnerOrOwnGUID());
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pal_legion_light_s_hammer_periodic_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pal_legion_light_s_hammer_periodic_AuraScript();
        }
};

// 119952 - Arcing Light (triggered by lights hammer)
class spell_pal_legion_arcing_light_heal : public SpellScriptLoader
{
    public:
        spell_pal_legion_arcing_light_heal() : SpellScriptLoader("spell_pal_legion_arcing_light_heal") { }

        class spell_pal_legion_arcing_light_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_arcing_light_heal_SpellScript);

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.remove(GetCaster());
                if (targets.size() > 6)
                    Trinity::Containers::RandomResize(targets, 6);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_arcing_light_heal_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_arcing_light_heal_SpellScript();
        }
};

// 228050 - Divine Shield
class spell_pal_legion_divine_shield_pet : public SpellScriptLoader
{
    public:
        spell_pal_legion_divine_shield_pet() : SpellScriptLoader("spell_pal_legion_divine_shield_pet") { }

        class spell_pal_legion_divine_shield_pet_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_legion_divine_shield_pet_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::UnitAuraCheck(false, SPELL_PALADIN_GUARDIAN_OF_THE_FORGOTTEN_QUEEN, GetCaster()->GetOwnerGUID()));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_legion_divine_shield_pet_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pal_legion_divine_shield_pet_SpellScript();
        }
};

void AddSC_paladin_pet_scripts()
{
    new npc_pal_legion_light_s_hammer();
    new npc_pal_legion_guardian_of_the_forgotten_queen();
    new spell_pal_legion_light_s_hammer_periodic();
    new spell_pal_legion_arcing_light_heal();
    new spell_pal_legion_divine_shield_pet();
}
