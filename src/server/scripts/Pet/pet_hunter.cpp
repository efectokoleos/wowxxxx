/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_hun_".
 */

#include "ScriptMgr.h"
#include "CreatureAIImpl.h"
#include "ScriptedCreature.h"
#include "TemporarySummon.h"
#include "PassiveAI.h"
#include "PetAI.h"
#include "SpellAuras.h"
#include "MotionMaster.h"

enum HunterSpells
{
    SPELL_HUNTER_CRIPPLING_POISON       = 30981, // Viper
    SPELL_HUNTER_DEADLY_POISON_PASSIVE  = 34657, // Venomous Snake
    SPELL_HUNTER_MIND_NUMBING_POISON    = 25810, // Viper

    // Dire Beast
    SPELL_HUNTER_DIRE_BEAST_ENERGIZE    = 120694,
    SPELL_HUNTER_DIRE_BEAST_DMG_BONUS   = 215778,
    SPELL_HUNTER_DIRE_BEAST             = 120679,
    SPELL_HUNTER_STOMP                  = 199530,
    SPELL_HUNTER_STOMP_DAMAGE           = 201754,
    
    // Stampede
    SPELL_HUNTER_STAMPEDE_PERIODIC      = 201631,

    // Dark Minion
    SPELL_HUNTER_DARK_WHISPER           = 204683,
    SPELL_HUNTER_BLACK_ARROW            = 194599,

    // Spitting Cobra
    SPELL_HUNTER_COBRA_SPIT             = 206685,
};

enum HunterCreatures
{
    NPC_HUNTER_VIPER                    = 19921
};

class npc_pet_hunter_snake_trap : public CreatureScript
{
    public:
        npc_pet_hunter_snake_trap() : CreatureScript("npc_pet_hunter_snake_trap") { }

        struct npc_pet_hunter_snake_trapAI : public ScriptedAI
        {
            npc_pet_hunter_snake_trapAI(Creature* creature) : ScriptedAI(creature)
            {
                Initialize();
            }

            void Initialize()
            {
                _spellTimer = 0;
                _isViper = false;
            }

            void EnterCombat(Unit* /*who*/) override { }

            void Reset() override
            {
                Initialize();

                CreatureTemplate const* Info = me->GetCreatureTemplate();

                _isViper = Info->Entry == NPC_HUNTER_VIPER ? true : false;

                me->SetMaxHealth(uint32(107 * (me->getLevel() - 40) * 0.025f));
                // Add delta to make them not all hit the same time
                uint32 delta = (rand32() % 7) * 100;
                me->SetBaseAttackTime(BASE_ATTACK, Info->BaseAttackTime + delta);
                //me->SetStatFloatValue(UNIT_FIELD_RANGED_ATTACK_POWER, float(Info->attackpower));

                // Start attacking attacker of owner on first ai update after spawn - move in line of sight may choose better target
                if (!me->GetVictim() && me->IsSummon())
                    if (Unit* Owner = me->ToTempSummon()->GetSummoner())
                        if (Owner->getAttackerForHelper())
                            AttackStart(Owner->getAttackerForHelper());

                if (!_isViper)
                    DoCast(me, SPELL_HUNTER_DEADLY_POISON_PASSIVE, true);
            }

            // Redefined for random target selection:
            void MoveInLineOfSight(Unit* who) override
            {
                if (!me->GetVictim() && me->CanCreatureAttack(who))
                {
                    if (me->GetDistanceZ(who) > CREATURE_Z_ATTACK_RANGE)
                        return;

                    float attackRadius = me->GetAttackDistance(who);
                    if (me->IsWithinDistInMap(who, attackRadius) && me->IsWithinLOSInMap(who))
                    {
                        if (!(rand32() % 5))
                        {
                            me->setAttackTimer(BASE_ATTACK, (rand32() % 10) * 100);
                            _spellTimer = (rand32() % 10) * 100;
                            AttackStart(who);
                        }
                    }
                }
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim() || !me->GetVictim())
                    return;

                if (me->EnsureVictim()->HasBreakableByDamageCrowdControlAura(me))
                {
                    me->InterruptNonMeleeSpells(false);
                    return;
                }

                // Viper
                if (_isViper)
                {
                    if (_spellTimer <= diff)
                    {
                        if (urand(0, 2) == 0) // 33% chance to cast
                            DoCastVictim(RAND(SPELL_HUNTER_MIND_NUMBING_POISON, SPELL_HUNTER_CRIPPLING_POISON));

                        _spellTimer = 3000;
                    }
                    else
                        _spellTimer -= diff;
                }

                DoMeleeAttackIfReady();
            }

        private:
            bool _isViper;
            uint32 _spellTimer;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_hunter_snake_trapAI(creature);
        }
};

// Dire Beast - 121118, 122802, 122804, 122806, 122807, 122809, 122811, 126213, 126214, 126215, 126216, 132764, 138574, 138580, 139226, 139227, 
// 149365, 170357, 170358, 170359, 170360, 170361, 170362, 170363, 170364, 204397, 204409, 204410, 204411, 204412, 204413, 204415, 204416, 204417, 
// 204418, 204419, 204420, 204422, 204423, 204424, 204425, 204426, 204427, 204428, 204429, 204430, 204431, 204432, 204433, 204434, 204480, 204494, 
// 204507, 204512, 204514, 204516, 204518, 204520, 204521, 204522, 204523, 204524, 204525, 204526, 204527, 204528, 204530, 204532, 204533, 204534, 
// 204546, 204548, 204550, 204552, 204554, 204555, 204556, 204557, 204558, 204560, 204561, 204565, 204568, 204569, 204571, 204572, 204583, 204584, 
// 204585, 204614, 204619, 204621, 204622, 204623, 204627, 204629, 204630, 204632, 204633, 204633, 212381, 212382, 212386, 212388, 212389, 212396, 
// 212397, 219199, 224573, 224574, 224575, 224576, 224577, 224578
class npc_hun_legion_dire_beast : public CreatureScript
{
    public:
        npc_hun_legion_dire_beast() : CreatureScript("npc_hun_legion_dire_beast") { }

        struct npc_hun_legion_dire_beastAI : public PetAI
        {
            npc_hun_legion_dire_beastAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* owner) override
            {
                me->CastSpell(owner, SPELL_HUNTER_DIRE_BEAST_ENERGIZE, true, nullptr, nullptr, owner->GetGUID());
                me->CastSpell(me, SPELL_HUNTER_DIRE_BEAST_DMG_BONUS, true, nullptr, nullptr, owner->GetGUID());
                if (!owner->GetTargetAuraApplications(SPELL_HUNTER_DIRE_BEAST).empty())
                {
                    AuraApplication* aurApp = owner->GetTargetAuraApplications(SPELL_HUNTER_DIRE_BEAST).front();
                    AttackStart(aurApp->GetTarget());
                    me->GetMotionMaster()->MoveCharge(aurApp->GetTarget()->GetPositionX(), aurApp->GetTarget()->GetPositionY(), aurApp->GetTarget()->GetPositionZ(), SPEED_CHARGE, EVENT_CHARGE, true, aurApp->GetTarget(), nullptr);
                }
            }

            void MovementInform(uint32 /*type*/, uint32 id) override
            {
                if (id == EVENT_CHARGE)
                    if (Unit* owner = me->GetOwner(false))
                        if (owner->HasAura(SPELL_HUNTER_STOMP))
                            DoCast(me, SPELL_HUNTER_STOMP_DAMAGE, true);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_hun_legion_dire_beastAI(creature);
        }
};

// 201430 - Stampede
class npc_hun_legion_stampede : public CreatureScript
{
    public:
        npc_hun_legion_stampede() : CreatureScript("npc_hun_legion_stampede") { }

        struct npc_hun_legion_stampedeAI : public PassiveAI
        {
            npc_hun_legion_stampedeAI(Creature* creature) : PassiveAI(creature) {}

            void IsSummonedBy(Unit* owner) override
            {
                me->CastSpell(me, SPELL_HUNTER_STAMPEDE_PERIODIC, true, nullptr, nullptr, owner->GetGUID());
            }
            
            void EnterEvadeMode(CreatureAI::EvadeReason) override {}
            void AttackStart(Unit* /*attacker*/) override {}
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_hun_legion_stampedeAI(creature);
        }
};

// 94072 - Dark Minion
class npc_hun_legion_dark_minion : public CreatureScript
{
    public:
        npc_hun_legion_dark_minion() : CreatureScript("npc_hun_legion_dark_minion") { }

        struct npc_hun_legion_dark_minionAI : public PetAI
        {
            npc_hun_legion_dark_minionAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* owner) override
            {
                DoCast(me, SPELL_HUNTER_DARK_WHISPER, true);
                
                if (!owner->GetTargetAuraApplications(SPELL_HUNTER_BLACK_ARROW).empty())
                    AttackStart(owner->GetTargetAuraApplications(SPELL_HUNTER_BLACK_ARROW).back()->GetTarget());
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_hun_legion_dark_minionAI(creature);
        }
};

// Spitting Cobra - 104493
class npc_hun_legion_spitting_cobra : public CreatureScript
{
    public:
        npc_hun_legion_spitting_cobra() : CreatureScript("npc_hun_legion_spitting_cobra") { }

        struct npc_hun_legion_spitting_cobraAI : public ScriptedAI
        {
            npc_hun_legion_spitting_cobraAI(Creature* creature) : ScriptedAI(creature) {}

            void IsSummonedBy(Unit* /*owner*/) override
            {
                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    if (Unit* owner = me->GetOwner(false))
                    {
                        if (Unit* target = owner->getAttackerForHelper())
                        {
                            AttackStart(target);
                            me->CastSpell(target, SPELL_HUNTER_COBRA_SPIT, false);
                        }
                        else
                            DoCastVictim(SPELL_HUNTER_COBRA_SPIT);
                    }
                    task.Repeat(Seconds(2));
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                UpdateVictim();
                // no melee attacks
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_hun_legion_spitting_cobraAI(creature);
        }
};

void AddSC_hunter_pet_scripts()
{
    new npc_pet_hunter_snake_trap();
    new npc_hun_legion_dire_beast();
    new npc_hun_legion_stampede();
    new npc_hun_legion_dark_minion();
    new npc_hun_legion_spitting_cobra();
}
