/*
* Copyright (C) 2008-2016 Frostmourne <http://www.frostmourne.eu/>
*
* This file is free software; as a special exception the author gives
* unlimited permission to copy and/or distribute it, with or without
* modifications, as long as this notice is preserved.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
* implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include "DB2Stores.h"
#include "GridNotifiers.h"
#include "Map.h"
#include "PassiveAI.h"
#include "PetAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "TemporarySummon.h"
#include "ObjectAccessor.h"
#include "SpellHistory.h"
#include "MotionMaster.h"
#include "Unit.h"

enum Spells : uint32
{
    // Demonic Gateway
    SPELL_DEMONIC_GATEWAY_AREATRIGGER           = 113904,
    SPELL_DEMONIC_GATEWAY_DUMMY_CAST            = 143251,
    SPELL_DEMONIC_GATEWAY_DEST_DUMMY            = 113900,
    SPELL_DEMONIC_GATEWAY_CASTER_DUMMY          = 113899,
    SPELL_DEMONIC_GATEWAY_CASTER_JUMP           = 120729,
    SPELL_DEMONIC_GATEWAY_DEST_JUMP             = 113896,
    SPELL_DEMONIC_GATEWAY_BLOCKER               = 113942,
    SPELL_WARLOCK_PLANESWALKER                  = 196675,
    SPELL_WARLOCK_PLANESWALKER_BUFF             = 196674,

    // Infernal
    SPELL_AVOIDANCE                             = 32233,
    SPELL_IMMILATION_PERIODIC                   = 19483,
    SPELL_PLAYER_DAMAGE_REDUCTION               = 115043,
    SPELL_PLAYER_DAMAGE_REDUCTION_90            = 142689,

    // Doomguard
    SPELL_RITUAL_ENSLAVEMENT                    = 22987,
    SPELL_DOOM_BOLT                             = 85692,

    // Grimoire Summons
    SPELL_GRIMOIRE_OF_SERVICE                   = 216187,

    // Imp
    SPELL_IMP_SINGE_MAGIC                       = 89808,
    SPELL_IMP_FIREBOLT                          = 3110,

    // Fel hunter
    SPELL_FEL_HUNTER_SPELL_LOCK                 = 19647,
    SPELL_FEL_HUNTER_SHADOW_BITE                = 54049,

    // Succubus
    SPELL_SUCCUBUS_SEDUCTION                    = 6358,
    SPELL_SUCCUBUS_LASH_OF_PAIN                 = 7814,
    SPELL_SUCCUBUS_WHIPLASH                     = 6360,

    // Voidwalker
    SPELL_VOIDWALKER_SUFFERING                  = 17735,
    SPELL_VOIDWALKER_CONSUMING_SHADOWS          = 3716,

    // Felguard
    SPELL_FELGUARD_AXE_TOSS                     = 89766,
    SPELL_FELGUARD_LEGION_STRIKE                = 30213,
    SPELL_FELGUARD_FELSTORM                     = 89751,

    // Wild Imp
    SPELL_FIXATE                                = 226044,
    SPELL_FEL_FIREBOLT                          = 104318,
    SPELL_HAND_OF_GULDAN_JUMP                   = 194227,
    SPELL_THE_EXPENDABLES                       = 211219,
    SPELL_THE_EXPENDABLES_BONUS                 = 211218,
    SPELL_STOLEN_POWER_TRAIT                    = 211530,
    SPELL_STOLEN_POWER_PROC_AURA                = 211592,
    SPELL_WARLOCK_IMPLOSION                     = 196278,

    // Darkglare
    SPELL_DARKGLARE_EYE_LASER                   = 205231,
    SPELL_WARLOCK_DOOM                          = 603,

    // Dreadstalker
    SPELL_WARLOCK_IMPROVED_DREADSTALKERS        = 196272,
    SPELL_WARLOCK_SUMMON_WILD_IMP               = 104317,
    SPELL_WARLOCK_SHARPENED_DREADFANGS_TRAIT    = 211123,
    SPELL_WARLOCK_SHARPENED_DREADFANGS_BONUS    = 215111,
    SPELL_DREADSTALKER_DREADBITE                = 205196,

    // Eye of Kilrogg
    SPELL_EYE_OF_KILROGG_STEALTH                = 2585,

    // Fel Lord
    SPELL_FEL_LORD_FEL_CLEAVE                   = 213688,
    SPELL_FEL_LORD_VISUAL                       = 212460,

    // Dimensional Rift
    SPELL_SHADOWY_TEAR_SHADOW_BARRAGE           = 196659,
    SPELL_UNSTABLE_TEAR_CHAOS_BARRAGE_PERIODIC  = 187385,
    SPELL_CHAOS_TEAR_CHAOS_BOLT                 = 215279,

    // Soul Effigy
    SPELL_WARLOCK_SOUL_EFFIGY                   = 205178,
    SPELL_SOUL_EFFIGY_PERIODIC                  = 205247,
};

enum Misc
{
    NPC_DEMONIC_GATEWAY_DEST    = 59271,
    NPC_DEMONIC_GATEWAY_CASTER  = 59262,

    NPC_IMP                     = 416,
    NPC_FEL_HUNTER              = 417,
    NPC_SUCCUBUS                = 1863,
    NPC_VOIDWALKER              = 1860,
    NPC_FELGUARD                = 17252,
    NPC_WILD_IMP                = 55659,
};

class npc_warl_legion_demonic_portal : public CreatureScript
{
    public:
        npc_warl_legion_demonic_portal() : CreatureScript("npc_warl_legion_demonic_portal") { }

        struct npc_warl_legion_demonic_portalAI : public ScriptedAI
        {
            npc_warl_legion_demonic_portalAI(Creature* creature) : ScriptedAI(creature) {}

            void AttackStart(Unit* /*target*/) override {}

            void Reset() override
            {
                me->SetControlled(true, UNIT_STATE_STUNNED);
                DoCast(me, SPELL_DEMONIC_GATEWAY_AREATRIGGER, true);
                DoCast(me, SPELL_DEMONIC_GATEWAY_DUMMY_CAST, true);
                DoCast(me, me->GetEntry() == NPC_DEMONIC_GATEWAY_DEST ? SPELL_DEMONIC_GATEWAY_DEST_DUMMY : SPELL_DEMONIC_GATEWAY_CASTER_DUMMY, true);
            }

            void OnSpellClick(Unit* clicker, bool& result) override
            {
                if (!result || clicker->HasAura(SPELL_DEMONIC_GATEWAY_BLOCKER))
                    return;

                if (clicker->GetGUID() == me->GetOwnerGUID())
                    if (clicker->HasAura(SPELL_WARLOCK_PLANESWALKER))
                        clicker->CastSpell(clicker, SPELL_WARLOCK_PLANESWALKER_BUFF, true);

                std::list<Creature*> portalList;
                GetCreatureListWithEntryInGrid(portalList, me, me->GetEntry() == NPC_DEMONIC_GATEWAY_DEST ? NPC_DEMONIC_GATEWAY_CASTER : NPC_DEMONIC_GATEWAY_DEST, 50.0f);
                for (Creature* portal : portalList)
                {
                    if (portal->GetOwnerGUID() == me->GetOwnerGUID())
                    {
                        clicker->CastSpell(portal, me->GetEntry() == NPC_DEMONIC_GATEWAY_DEST ?  SPELL_DEMONIC_GATEWAY_CASTER_JUMP : SPELL_DEMONIC_GATEWAY_DEST_JUMP, true);
                        break;
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_demonic_portalAI(creature);
        }
};

class npc_warl_legion_infernal : public CreatureScript
{
    public:
        npc_warl_legion_infernal() : CreatureScript("npc_warl_legion_infernal") { }

        struct npc_warl_legion_infernalAI : public PetAI
        {
            npc_warl_legion_infernalAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                me->CastSpell(me, SPELL_AVOIDANCE, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_IMMILATION_PERIODIC, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_PLAYER_DAMAGE_REDUCTION, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_PLAYER_DAMAGE_REDUCTION_90, true, nullptr, nullptr, summoner->GetGUID());
                PetAI::IsSummonedBy(summoner);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_infernalAI(creature);
        }
};

class npc_warl_legion_doomguard : public CreatureScript
{
    public:
        npc_warl_legion_doomguard() : CreatureScript("npc_warl_legion_doomguard") { }

        struct npc_warl_legion_doomguardAI : public PetAI
        {
            npc_warl_legion_doomguardAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->GetVictim())
                    focusTargetGUID = summoner->GetVictim()->GetGUID();

                DoCast(me, SPELL_RITUAL_ENSLAVEMENT, true);
                me->SetPower(POWER_ENERGY, me->GetMaxPower(POWER_ENERGY));

                _scheduler.Schedule(Milliseconds(100), [this](TaskContext task)
                {
                    if (!me->HasUnitState(UNIT_STATE_CASTING) && me->IsSummon())
                    {
                        Unit* target = ObjectAccessor::GetUnit(*me, focusTargetGUID);
                        if (!target && me->IsSummon())
                        {
                            if (me->ToTempSummon()->GetSummoner()->getAttackerForHelper())
                            {
                                target = me->ToTempSummon()->GetSummoner()->getAttackerForHelper();
                                focusTargetGUID = me->GetOwner()->getAttackerForHelper()->GetGUID();
                            }
                        }

                        if (target)
                            DoCast(target, SPELL_DOOM_BOLT, false);
                    }
                    task.Repeat();
                });
                PetAI::IsSummonedBy(summoner);
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                PetAI::UpdateAI(diff);
            }

            private:
                TaskScheduler _scheduler;
                ObjectGuid focusTargetGUID;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_doomguardAI(creature);
        }
};

class npc_warl_legion_grimoire_summons : public CreatureScript
{
    public:
        npc_warl_legion_grimoire_summons() : CreatureScript("npc_warl_legion_grimoire_summons") { }

        struct npc_warl_legion_grimoire_summonsAI : public PetAI
        {
            npc_warl_legion_grimoire_summonsAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->GetTypeId() != TYPEID_PLAYER)
                    return;

                me->CastSpell(me, SPELL_GRIMOIRE_OF_SERVICE, true);
                me->CastSpell(me, SPELL_AVOIDANCE, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_PLAYER_DAMAGE_REDUCTION, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_PLAYER_DAMAGE_REDUCTION_90, true, nullptr, nullptr, summoner->GetGUID());

                Unit* target = summoner->ToPlayer()->GetSelectedUnit();
                if (target)
                    AttackStart(target);

                if (CharmInfo* charmInfo = me->GetCharmInfo())
                {
                    switch (me->GetEntry())
                    {
                        case NPC_IMP:
                            DoCast(summoner, SPELL_IMP_SINGE_MAGIC, true);
                            charmInfo->AddAutoCastSpell(SPELL_IMP_FIREBOLT, 0);
                            break;
                        case NPC_FEL_HUNTER:
                            DoCast(target, SPELL_FEL_HUNTER_SPELL_LOCK, true);
                            charmInfo->AddAutoCastSpell(SPELL_FEL_HUNTER_SHADOW_BITE, 0);
                            break;
                        case NPC_SUCCUBUS:
                            me->SetPower(POWER_ENERGY, me->GetMaxPower(POWER_ENERGY));
                            DoCast(target, SPELL_SUCCUBUS_SEDUCTION, false);
                            charmInfo->AddAutoCastSpell(SPELL_SUCCUBUS_LASH_OF_PAIN, 0);
                            charmInfo->AddAutoCastSpell(SPELL_SUCCUBUS_WHIPLASH, 1);
                            break;
                        case NPC_VOIDWALKER:
                            DoCast(target, SPELL_VOIDWALKER_SUFFERING, true);
                            charmInfo->AddAutoCastSpell(SPELL_VOIDWALKER_CONSUMING_SHADOWS, 0);
                            break;
                        case NPC_FELGUARD:
                            DoCast(target, SPELL_FELGUARD_AXE_TOSS, true);
                            charmInfo->AddAutoCastSpell(SPELL_FELGUARD_LEGION_STRIKE, 0);
                            charmInfo->AddAutoCastSpell(SPELL_FELGUARD_FELSTORM, 1);
                            break;
                        default:
                            break;
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_grimoire_summonsAI(creature);
        }
};

// Wild Imp - 55659
class npc_warl_legion_wild_imp : public CreatureScript
{
    public:
        npc_warl_legion_wild_imp() : CreatureScript("npc_warl_legion_wild_imp") { }

        struct npc_warl_legion_wild_impAI : public PetAI
        {
            npc_warl_legion_wild_impAI(Creature* creature) : PetAI(creature) {}

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->GetTypeId() != TYPEID_PLAYER)
                    return;

                me->CastSpell(me, SPELL_AVOIDANCE, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_PLAYER_DAMAGE_REDUCTION, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_PLAYER_DAMAGE_REDUCTION_90, true, nullptr, nullptr, summoner->GetGUID());
                me->CastSpell(me, SPELL_FIXATE, true);

                if (!me->GetVehicle())
                {
                    Position jumpPos = me->GetRandomPoint(me->GetPosition(), 6.00f);
                    me->GetMotionMaster()->MoveJump(jumpPos, 10.00f, 10.00f);
                }

                _scheduler.Schedule(Milliseconds(200), [this](TaskContext /*task*/)
                {
                    allowExplosion = true;
                    if (CharmInfo* charmInfo = me->GetCharmInfo())
                        charmInfo->AddAutoCastSpell(SPELL_FEL_FIREBOLT, 0);
                });

                if (summoner->HasAura(SPELL_STOLEN_POWER_TRAIT))
                    me->CastSpell(me, SPELL_STOLEN_POWER_PROC_AURA, true);

                if (summoner->HasAura(SPELL_THE_EXPENDABLES))
                {
                    _scheduler.Schedule(Milliseconds(13900), [this](TaskContext /*task*/)
                    {
                        if (Unit* owner = me->GetOwner())
                            owner->CastSpell(owner, SPELL_THE_EXPENDABLES_BONUS, true);
                    });
                }
            }

            void MovementInform(uint32 /*type*/, uint32 id) override
            {
                if (!allowExplosion)
                    return;

                if (id == EVENT_JUMP)
                {
                    // make sure position is already end destination ...
                    _scheduler.Schedule(Milliseconds(200), [this](TaskContext /*task*/)
                    {
                        if (Unit* owner = me->GetCharmerOrOwner())
                            owner->CastSpell(me, SPELL_WARLOCK_IMPLOSION, true);
                    });
                }
            }

            void JustDied(Unit* /*killer*/) override
            {
                if (Unit* owner = me->GetCharmerOrOwner())
                    if (owner->HasAura(SPELL_THE_EXPENDABLES))
                        owner->CastSpell(owner, SPELL_THE_EXPENDABLES_BONUS, true);
                me->DespawnOrUnsummon();
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);

                PetAI::UpdateAI(diff);
            }

            private:
                TaskScheduler _scheduler;
                bool allowExplosion = false;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_wild_impAI(creature);
        }
};

// 103673 - Darkglare
class npc_warl_legion_darkglare : public CreatureScript
{
    public:
        npc_warl_legion_darkglare() : CreatureScript("npc_warl_legion_darkglare") { }

        struct npc_warl_legion_darkglareAI : public ScriptedAI
        {
            npc_warl_legion_darkglareAI(Creature* creature) : ScriptedAI(creature) { }

            void IsSummonedBy(Unit* /*summoner*/) override
            {
                _scheduler.Schedule(Milliseconds(100), [this](TaskContext task)
                {
                    Unit* target = ObjectAccessor::GetUnit(*me, _focusTargetGUID);
                    if (!target)
                    {
                        std::list<Unit*> unitTargets;
                        Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(me, me, 40.0f);
                        Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(me, unitTargets, u_check);
                        Cell::VisitAllObjects(me, searcher, 40.0f);
                        unitTargets.remove_if(Trinity::UnitAuraCheck(false, SPELL_WARLOCK_DOOM, me->GetCharmerOrOwnerGUID()));

                        if (!unitTargets.empty())
                        {
                            unitTargets.sort(Trinity::ObjectDistanceOrderPred(me));
                            target = unitTargets.front();
                            _focusTargetGUID = unitTargets.front()->GetGUID();
                        }

                    }

                    if (target)
                    {
                        me->SetFacingToObject(target);
                        DoCast(target, SPELL_DARKGLARE_EYE_LASER, false);
                    }

                    task.Repeat(Seconds(2));
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
                ObjectGuid _focusTargetGUID;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_darkglareAI(creature);
        }
};

// 98035 - Dreadstalker
class npc_warl_legion_dreadstalker : public CreatureScript
{
    public:
        npc_warl_legion_dreadstalker() : CreatureScript("npc_warl_legion_dreadstalker") { }

        struct npc_warl_legion_dreadstalkerAI : public PetAI
        {
            npc_warl_legion_dreadstalkerAI(Creature* creature) : PetAI(creature) { }

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->HasAura(SPELL_WARLOCK_IMPROVED_DREADSTALKERS))
                {
                    SummonPropertiesEntry const* properties = sSummonPropertiesStore.LookupEntry(3210);
                    if (TempSummon* imp = me->GetMap()->SummonCreature(NPC_WILD_IMP, me->GetPosition(), properties, 12000, summoner, SPELL_WARLOCK_SUMMON_WILD_IMP, 0))
                        imp->EnterVehicle(me, 0);
                }

                if (AuraEffect* sharpenedDreadfangs = summoner->GetAuraEffect(SPELL_WARLOCK_SHARPENED_DREADFANGS_TRAIT, EFFECT_0))
                    me->CastCustomSpell(SPELL_WARLOCK_SHARPENED_DREADFANGS_BONUS, SPELLVALUE_BASE_POINT0, sharpenedDreadfangs->GetAmount(), me, TRIGGERED_FULL_MASK);

                _scheduler.Schedule(Milliseconds(100), [this](TaskContext task)
                {
                    if (!me->GetVictim())
                    {
                        task.Repeat();
                        return;
                    }

                    if (!me->IsValidAttackTarget(me->GetVictim()))
                    {
                        task.Repeat();
                        return;
                    }

                    me->GetMotionMaster()->MoveCharge(me->GetVictim()->GetPositionX(), me->GetVictim()->GetPositionY(), me->GetVictim()->GetPositionZ());
                });
            }

            void MovementInform(uint32 /*type*/, uint32 id) override
            {
                if (id == EVENT_CHARGE)
                    DoCastVictim(SPELL_DREADSTALKER_DREADBITE);
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
                PetAI::UpdateAI(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_dreadstalkerAI(creature);
        }
};

class npc_warl_legion_eye_of_kilrogg : public CreatureScript
{
    public:
        npc_warl_legion_eye_of_kilrogg() : CreatureScript("npc_warl_legion_eye_of_kilrogg") { }

        struct npc_warl_legion_eye_of_kilroggAI : public PassiveAI
        {
            npc_warl_legion_eye_of_kilroggAI(Creature* creature) : PassiveAI(creature) {}

            void IsSummonedBy(Unit* /*summoner*/) override
            {
                // Don't move this into creature_template_addon, system is called too early for this aura
                DoCast(me, SPELL_EYE_OF_KILROGG_STEALTH, true);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_eye_of_kilroggAI(creature);
        }
};

// 107024 - Fel Lord
class npc_warl_legion_fel_lord : public CreatureScript
{
    public:
        npc_warl_legion_fel_lord() : CreatureScript("npc_warl_legion_fel_lord") { }

        struct npc_warl_legion_fel_lordAI : public PassiveAI
        {
            npc_warl_legion_fel_lordAI(Creature* creature) : PassiveAI(creature) {}

            void IsSummonedBy(Unit* /*summoner*/) override
            {
                me->SetControlled(true, UNIT_STATE_ROOT);
                DoCast(me, SPELL_FEL_LORD_VISUAL, true);

                _scheduler.Schedule(Milliseconds(), [this](TaskContext task)
                {
                    if (me->HasUnitState(UNIT_STATE_CASTING) || me->GetSpellHistory()->HasCooldown(SPELL_FEL_LORD_FEL_CLEAVE))
                    {
                        task.Repeat(Milliseconds(250));
                        return;
                    }

                    std::list<Unit*> unitTargets;
                    Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(me, me, 6.0f);
                    Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(me, unitTargets, u_check);
                    Cell::VisitAllObjects(me, searcher, 6.0f);

                    for (Unit* target : unitTargets)
                    {
                        // AnyUnfriendlyUnitInObjectRangeCheck dont use exact check we need to check it again here
                        if (me->GetExactDist(target) <= 6.00f)
                        {
                            DoCast(me, SPELL_FEL_LORD_FEL_CLEAVE, false);
                            break;
                        }
                    }
                    task.Repeat(Milliseconds(250));
                });
            }

            void DamageTaken(Unit* /*attacker*/, uint32& damage) override
            {
                damage = 0;
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            private:
                TaskScheduler _scheduler;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_fel_lordAI(creature);
        }
};

// 107024 - Fel Lord
class npc_warl_legion_dimensional_rifts : public CreatureScript
{
    public:
        npc_warl_legion_dimensional_rifts() : CreatureScript("npc_warl_legion_dimensional_rifts") { }

        struct npc_warl_legion_dimensional_riftsAI : public PassiveAI
        {
            npc_warl_legion_dimensional_riftsAI(Creature* creature) : PassiveAI(creature) {}

            void IsSummonedBy(Unit* /*summoner*/) override
            {
                _scheduler.Schedule(me->GetEntry() == ENTRY_CHAOS_TEAR ? Seconds(1) : Milliseconds(100), [this](TaskContext task)
                {
                    if (initialTarget.IsEmpty())
                    {
                        task.Repeat(Milliseconds(100));
                        return;
                    }

                    switch (me->GetEntry())
                    {
                        case ENTRY_CHAOS_TEAR:
                            if (Unit* target = ObjectAccessor::GetUnit(*me, initialTarget))
                                DoCast(target, SPELL_CHAOS_TEAR_CHAOS_BOLT, true);

                            me->DespawnOrUnsummon(2000);
                            break;
                        case ENTRY_UNSTABLE_TEAR:
                            DoCastSelf(SPELL_UNSTABLE_TEAR_CHAOS_BARRAGE_PERIODIC);
                            break;
                        case ENTRY_SHADOWY_TEAR:
                            DoCastSelf(SPELL_SHADOWY_TEAR_SHADOW_BARRAGE);
                            break;
                        default:
                            break;
                    }
                });
            }

            void UpdateAI(uint32 diff) override
            {
                _scheduler.Update(diff);
            }

            void SetGUID(ObjectGuid guid, int32 /* = 0 */) override
            {
                initialTarget = guid;
            }

            ObjectGuid GetGUID(int32 /* = 0 */) const override
            {
                return initialTarget;
            }

            private:
                TaskScheduler _scheduler;
                ObjectGuid initialTarget;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_dimensional_riftsAI(creature);
        }
};

// 103679 - Soul Effigy
class npc_warl_legion_soul_effigy : public CreatureScript
{
    public:
        npc_warl_legion_soul_effigy() : CreatureScript("npc_warl_legion_soul_effigy") { }

        struct npc_warl_legion_soul_effigyAI : public PassiveAI
        {
            npc_warl_legion_soul_effigyAI(Creature* creature) : PassiveAI(creature) {}

            void JustDied(Unit* /*killer*/) override
            {
                if (Unit* summoner = ObjectAccessor::GetUnit(*me, me->GetDemonCreatorGUID()))
                {
                    Unit::AuraApplicationVector aurApps = summoner->GetTargetAuraApplications(SPELL_WARLOCK_SOUL_EFFIGY);
                    for (AuraApplication* aurApp : aurApps)
                        aurApp->GetBase()->Remove();
                }
            }

            void UpdateAI(uint32 /*diff*/) override { }
            void EnterEvadeMode(EvadeReason /*reason*/) override { }
            void AttackStart(Unit* /*target*/) override { }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_warl_legion_soul_effigyAI(creature);
        }
};

void AddSC_warlock_pet_scripts()
{
    new npc_warl_legion_demonic_portal();
    new npc_warl_legion_infernal();
    new npc_warl_legion_doomguard();
    new npc_warl_legion_grimoire_summons();
    new npc_warl_legion_wild_imp();
    new npc_warl_legion_darkglare();
    new npc_warl_legion_dreadstalker();
    new npc_warl_legion_eye_of_kilrogg();
    new npc_warl_legion_fel_lord();
    new npc_warl_legion_dimensional_rifts();
    new npc_warl_legion_soul_effigy();
}
